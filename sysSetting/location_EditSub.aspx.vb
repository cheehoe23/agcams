#Region "Information Section"
' ****************************************************************************************************
' Description       : Edit Sub Location 
' Purpose           : Edit Sub Location Information
' Date              : 15/12/2008
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class location_EditSub
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")

                ''Get Pass Value
                hdnCallFrm.Value = Request("CallFrm")
                hdnSLocID.Value = Request("SLocID")

                ''Populate Light Type
                fnPopulateSmartShelfLightType(ddlLightType)

                ''Retrieve Records 
                fnGetRecordsFromDB()

                ''Check Access Right
                fnCheckForUserAccess()
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnGetRecordsFromDB() As Boolean
        Dim objRdr As SqlDataReader
        Try
            objRdr = clsLocation.fnLocationGetAllRecDR("0", hdnSLocID.Value, "fld_LocSubID", "ASC")
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()

                    lblLocCode.Text = CStr(objRdr("fld_LocationCode"))
                    lblLocName.Text = CStr(objRdr("fld_LocationName"))
                    txtSLocCode.Text = CStr(objRdr("fld_LocSubCode"))
                    txtSLocName.Text = CStr(objRdr("fld_LocSubName"))
                    rdActivate.SelectedValue = CStr(objRdr("LocSubActvCode"))
                    rdActivate_SelectedIndexChanged(Nothing, Nothing)
                    txtLightIP.Text = CStr(objRdr("fld_LightIPAdd"))
                    ddlLightType.SelectedValue = CStr(objRdr("fld_LightType"))

                    rdDoorGantryF.SelectedValue = CStr(objRdr("fld_DGF"))
                    rdDoorGantryF_SelectedIndexChanged(Nothing, Nothing)
                    txtDGMvStatus.Text = CStr(objRdr("fld_DGMvStatus"))

                    hdnLocID.Value = CStr(objRdr("fld_LocationID"))
                End If
            End If
            objRdr.Close()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Function

    Public Sub fnCheckForUserAccess()
        ''Case 1: Access right
        If (InStr(Session("AR"), "SysSet|loct|edit") = 0) Then
            txtSLocCode.Enabled = False
            txtSLocName.Enabled = False
            rdActivate.Enabled = False
            txtLightIP.Enabled = False
            ddlLightType.Enabled = False
            'rdActivate_SelectedIndexChanged(Nothing, Nothing)

            rdDoorGantryF.Enabled = False
            txtDGMvStatus.Enabled = False

            butSubmit.Enabled = False
            butReset.Enabled = False
        End If
    End Sub

    Protected Sub ButCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButCancel.Click
        If hdnCallFrm.Value = "Edit" Then
            Server.Transfer("location_edit.aspx?loctID=" + hdnLocID.Value)
        Else
            Server.Transfer("location_view.aspx")
        End If
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            fnGetRecordsFromDB()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim strNewSLocCode As String
            Dim strNewSLocName As String
            Dim strNewActv As String
            Dim strNewLightIPAdd As String = ""
            Dim srtrNewLightType As String = "0"
            Dim strDoorGantryF As String = ""
            Dim strDGMvStatus As String = ""

            ''Get New Records Value
            strNewSLocCode = Trim(txtSLocCode.Text)
            strNewSLocName = Trim(txtSLocName.Text)
            strNewActv = rdActivate.SelectedValue
            If strNewActv = "Y" Then
                strNewLightIPAdd = Trim(txtLightIP.Text)
                srtrNewLightType = ddlLightType.SelectedValue
            End If
            strDoorGantryF = rdDoorGantryF.SelectedValue
            If strDoorGantryF = "Y" Then
                strDGMvStatus = Trim(txtDGMvStatus.Text)
            End If

            Dim dt As DataTable = clsLocation.fnLocationSub_GetByfld_LocSubCode(txtSLocCode.Text.ToString().Trim()).Tables(0)
            If (dt.Rows.Count > 0) Then
                lblErrorMessage.Text = "Duplicate Sub Location Code in the Asset Management System! Please Try Again!"
                lblErrorMessage.Visible = True
                hdnDuplicateStatus.Value = "1"
                txtSLocCode.Focus()
            Else
                lblErrorMessage.Text = ""
                lblErrorMessage.Visible = False
                hdnDuplicateStatus.Value = "0"
            End If

            ''Validate Sub Location Code
            If Not clsLocation.fnLocation_CheckDuplicate4Sub(hdnLocID.Value, hdnSLocID.Value, strNewSLocCode, "E") Then 'Duplicate Sub Location Code
                ''Add New Sub Location in database
                Dim intRetVal As Integer
                Dim strMsg As String
                intRetVal = clsLocation.fnLocation_EditLocation( _
                                hdnLocID.Value, hdnSLocID.Value, strNewSLocCode, strNewSLocName, strNewActv, strNewLightIPAdd, _
                                srtrNewLightType, Session("UsrID"), "S", strDoorGantryF, strDGMvStatus)
                If intRetVal > 0 Then
                    strMsg = "Sub Location update successfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Sub Location Updated : " + strNewSLocName + " (" + strNewSLocCode + ")")

                    ''Message Notification
                    Dim strJavaScript As String
                    If hdnCallFrm.Value = "Edit" Then
                        strJavaScript = "<script language = 'Javascript'>" & _
                                        "alert('" + strMsg + "');" & _
                                        "document.location.href='location_edit.aspx?loctID=" + hdnLocID.Value + "';" & _
                                        "</script>"
                    Else
                        strJavaScript = "<script language = 'Javascript'>" & _
                                        "alert('" + strMsg + "');" & _
                                        "document.location.href='location_view.aspx';" & _
                                        "</script>"
                    End If
                    Response.Write(strJavaScript)
                Else
                    strMsg = "Sub Location update failed. Please contact your Administrator."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If

            Else   '' Error Found --> show error message
                lblErrorMessage.Text = "Sub Location Code already exist. Please choose another Sub Location Code."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub rdActivate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdActivate.SelectedIndexChanged
        Try
            If rdActivate.SelectedValue = "Y" Then
                divLightIP.Visible = True
            Else
                divLightIP.Visible = False
            End If

            txtLightIP.Text = ""
            ddlLightType.SelectedValue = "0"
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub rdDoorGantryF_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdDoorGantryF.SelectedIndexChanged
        Try
            If rdDoorGantryF.SelectedValue = "Y" Then
                divGoorGantry.Visible = True
            Else
                divGoorGantry.Visible = False
            End If

            txtDGMvStatus.Text = ""
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub txtSLocCode_TextChanged(sender As Object, e As EventArgs) Handles txtSLocCode.TextChanged
        Dim dt As DataTable = clsLocation.fnLocationSub_GetByfld_LocSubCode(txtSLocCode.Text.ToString().Trim()).Tables(0)
        If (dt.Rows.Count > 0) Then
            lblErrorMessage.Text = "Duplicate Sub Location Code in the Asset Management System! Please Try Again!"
            lblErrorMessage.Visible = True
            hdnDuplicateStatus.Value = "1"
            txtSLocCode.Focus()
        Else
            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False
            hdnDuplicateStatus.Value = "0"
        End If
    End Sub
End Class