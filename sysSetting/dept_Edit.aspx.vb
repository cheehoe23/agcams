#Region "Information Section"
' ****************************************************************************************************
' Description       : Edit New Department 
' Purpose           : Edit New Department Information
' Author            : See Siew
' Date              : 27/02/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Web.Services

#End Region

Partial Class dept_Edit
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label
    Protected WithEvents rdDeptType As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents lblDeptCodeChar As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            Me.Header.DataBind()

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")

                ''Get Deprtment ID 
                hdnDeptID.Text = Request("DeptID")

                ' ''Get Owner
                cbHOD.DataBind()

                ''Retrieve Records 
                fnGetRecordsFromDB()
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnGetRecordsFromDB()
        Dim objRdr As SqlDataReader
        Try
            objRdr = clsDepartment.fnDeptGetDeptForEdit(hdnDeptID.Text)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()
                    txtDeptCode.Text = CStr(objRdr("fld_DepartmentCode"))
                    txtDeptName.Text = CStr(objRdr("fld_DepartmentName"))
                    cbHOD.Text = CStr(objRdr("fld_HODName")) & " (" & CStr(objRdr("fld_department")) & ")"
                End If
            End If
            objRdr.Close()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Private Sub ButCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButCancel.Click
        Response.Redirect("dept_View.aspx")
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        fnGetRecordsFromDB()
    End Sub

    Private Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim strDeptCode, strDeptName As String
            Dim strHOD As String = ""
            Dim bDupDept As Boolean = False
            Dim intRetVal As Integer
            Dim strMsg As String

            ''** get Fields Value
            strDeptCode = txtDeptCode.Text
            strDeptName = txtDeptName.Text

            ''*** Get HOD
            If Trim(cbHOD.Text) <> "" Then
                Dim strUsrExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbHOD.Text), strHOD, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Head of Department not found. Please select an existing Head of Department."
                    lblErrorMessage.Visible = True
                    cbHOD.Focus()
                    Exit Sub
                End If
            End If

            ''** Check for Duplicate Department Code
            bDupDept = clsDepartment.fnDeptDuplicateDept(hdnDeptID.Text, strDeptCode, "E")

            If Not bDupDept Then ''not duplicate record
                ''Edit record
                intRetVal = clsDepartment.fnDeptInsertUpdateDelete( _
                                    hdnDeptID.Text, strDeptCode, strDeptName, strHOD, Session("UsrID"), "E")

                If intRetVal > 0 Then
                    strMsg = "Department Updated Successfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Department Updated : " + strDeptName + " (" + strDeptCode + ")")

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "document.location.href='dept_View.aspx';" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                    'Server.Transfer("dept_View.aspx")
                Else
                    strMsg = "Department Update Failed."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If

            Else  ''duplicate record
                lblErrorMessage.Text = "Department Code already exist. Please choose another Department Code."
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class
