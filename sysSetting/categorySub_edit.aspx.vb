#Region "Information Section"
' ****************************************************************************************************
' Description       : Edit Sub Category 
' Purpose           : Edit Sub Category Information
' Date              : 28/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
#End Region

Partial Public Class categorySub_edit
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                hdnSCategoryID.Value = Request("SCategoryID")
                hdnCallFrm.Value = Request("CallFrm")
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")
                Me.butDelImage.Attributes.Add("OnClick", "return fnDeleteImages()")

                ''Populate Control
                fnPopulateCtrl()

                ''Populate Records
                fnGetRecordsFromDB()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Validity Year --> 0 - 20
            fnPopulateNumberInDDL(ddlValidYear, "50")

            ''Get Expiring Notification --> 0 - 6
            fnPopulateNumberInDDL(ddlExpNotiB4, "6")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnGetRecordsFromDB() As Boolean
        Dim objRdr As SqlDataReader
        Try
            objRdr = clsCategory.fnCategoryGetAllRecDR(0, hdnSCategoryID.Value, "fld_CatSubID", "ASC")
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()

                    lblCatCode.Text = CStr(objRdr("fld_CategoryCode"))
                    lblCatName.Text = CStr(objRdr("fld_CategoryName"))
                    txtSubCatCode.Text = CStr(objRdr("fld_CatSubCode"))
                    txtSubCatName.Text = CStr(objRdr("fld_CatSubName"))
                    cbAddInfo1.Checked = IIf(CStr(objRdr("fld_CatSubAddInfoM1")) = "Y", True, False)
                    cbAddInfo2.Checked = IIf(CStr(objRdr("fld_CatSubAddInfoM2")) = "Y", True, False)
                    cbAddInfo3.Checked = IIf(CStr(objRdr("fld_CatSubAddInfoM3")) = "Y", True, False)
                    cbAddInfo4.Checked = IIf(CStr(objRdr("fld_CatSubAddInfoM4")) = "Y", True, False)
                    cbAddInfo5.Checked = IIf(CStr(objRdr("fld_CatSubAddInfoM5")) = "Y", True, False)
                    txtAddInfo1.Text = CStr(objRdr("fld_CatSubAddInfo1"))
                    txtAddInfo2.Text = CStr(objRdr("fld_CatSubAddInfo2"))
                    txtAddInfo3.Text = CStr(objRdr("fld_CatSubAddInfo3"))
                    txtAddInfo4.Text = CStr(objRdr("fld_CatSubAddInfo4"))
                    txtAddInfo5.Text = CStr(objRdr("fld_CatSubAddInfo5"))
                    ddlValidYear.SelectedValue = CStr(objRdr("fld_UsefulLifeYear"))
                    txtFloatLevel.Text = IIf(Trim(CStr(objRdr("fld_FloatLevel"))) = "-", "", CStr(objRdr("fld_FloatLevel")))
                    ddlExpNotiB4.SelectedValue = CStr(objRdr("fld_ExpNoticeB4"))

                    cbACTag.Checked = IIf(CStr(objRdr("fld_ACTagF")) = "Y", True, False)
                    If Trim(CStr(objRdr("fld_FileName"))) = "" Then ''no images
                        FUploadImage.Visible = True
                        hdnImageSaveF.Value = "N"
                        butDelImage.Visible = False
                        hylFileName.Visible = False
                    Else ''have images
                        FUploadImage.Visible = False
                        hdnImageSaveF.Value = "Y"
                        butDelImage.Visible = True
                        hylFileName.Visible = True

                        hylFileName.NavigateUrl = gCategoryPath & hdnSCategoryID.Value & "_" & CStr(objRdr("fld_FileName"))
                        hylFileName.Text = CStr(objRdr("fld_FileName"))
                    End If

                    hdnCategoryID.Value = CStr(objRdr("fld_CategoryID"))
                    hdnCatAssign2Asset.Value = CStr(objRdr("CatAssign2Asset"))
                End If
            End If
            objRdr.Close()

            ''Check Access Right
            fnCheckForUserAccess()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Function

    Public Sub fnCheckForUserAccess()
        ''Case 1: Access right
        If (InStr(Session("AR"), "SysSet|catg|edit") = 0) Then
            txtSubCatCode.Enabled = False
            txtSubCatName.Enabled = False
            cbAddInfo1.Enabled = False
            cbAddInfo2.Enabled = False
            cbAddInfo3.Enabled = False
            cbAddInfo4.Enabled = False
            cbAddInfo5.Enabled = False
            txtAddInfo1.Enabled = False
            txtAddInfo2.Enabled = False
            txtAddInfo3.Enabled = False
            txtAddInfo4.Enabled = False
            txtAddInfo5.Enabled = False
            ddlValidYear.Enabled = False
            txtFloatLevel.Enabled = False
            ddlExpNotiB4.Enabled = False

            cbACTag.Enabled = False
            FUploadImage.Disabled = True
            'butDelImage.Enabled = False
            butDelImage.Visible = False

            butSubmit.Enabled = False
            butReset.Enabled = False
        End If

        ''Case 2: If subcategory assign to Asset, Not allow to edit Additional Information
        If hdnCatAssign2Asset.Value = "Y" Then
            cbAddInfo1.Enabled = False
            cbAddInfo2.Enabled = False
            cbAddInfo3.Enabled = False
            cbAddInfo4.Enabled = False
            cbAddInfo5.Enabled = False
            txtAddInfo1.Enabled = False
            txtAddInfo2.Enabled = False
            txtAddInfo3.Enabled = False
            txtAddInfo4.Enabled = False
            txtAddInfo5.Enabled = False
        End If
    End Sub

    Protected Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim strNewSCatCode As String
            Dim strNewSCatName As String
            Dim strNewAddInfo1 As String = ""
            Dim strNewAddInfo2 As String = ""
            Dim strNewAddInfo3 As String = ""
            Dim strNewAddInfo4 As String = ""
            Dim strNewAddInfo5 As String = ""
            Dim strNewAddInfoM1 As String = ""
            Dim strNewAddInfoM2 As String = ""
            Dim strNewAddInfoM3 As String = ""
            Dim strNewAddInfoM4 As String = ""
            Dim strNewAddInfoM5 As String = ""
            Dim strUploadFileF As String = "N"
            Dim strUploadFileName As String = ""

            ''Get New Records Value
            strNewSCatCode = txtSubCatCode.Text
            strNewSCatName = txtSubCatName.Text

            ''** Images format checking
            If hdnImageSaveF.Value = "N" Then  ''imges haven't save
                If Not FUploadImage.PostedFile Is Nothing And FUploadImage.PostedFile.ContentLength > 0 Then '' check whether user upload file
                    If System.IO.Path.GetExtension(FUploadImage.PostedFile.FileName).ToUpper <> ".JPG" Then
                        lblErrorMessage.Text = "Please attach a image with an acceptable file format for uploading. Acceptable file formats are JPG."
                        lblErrorMessage.Visible = True
                        Exit Sub
                    Else
                        '' Get File Name
                        strUploadFileName = System.IO.Path.GetFileName(FUploadImage.PostedFile.FileName)

                        '' Indicate have new images want to upload
                        strUploadFileF = "Y"
                    End If
                End If
            End If

            ''** Get New Additional Information to display
            fnGetSubCatAddInfo( _
                Trim(txtAddInfo1.Text), Trim(txtAddInfo2.Text), Trim(txtAddInfo3.Text), Trim(txtAddInfo4.Text), Trim(txtAddInfo5.Text), _
                IIf(cbAddInfo1.Checked, "Y", "N"), IIf(cbAddInfo2.Checked, "Y", "N"), IIf(cbAddInfo3.Checked, "Y", "N"), IIf(cbAddInfo4.Checked, "Y", "N"), IIf(cbAddInfo5.Checked, "Y", "N"), _
                strNewAddInfo1, strNewAddInfo2, strNewAddInfo3, strNewAddInfo4, strNewAddInfo5, _
                strNewAddInfoM1, strNewAddInfoM2, strNewAddInfoM3, strNewAddInfoM4, strNewAddInfoM5, _
                "")

            ''Validate Sub Category Name
            If Not clsCategory.fnCategoryCheckDuplicate(hdnCategoryID.Value, hdnSCategoryID.Value, "", strNewSCatName, "2", "E") Then '' not Duplicate found --> add new record in datatable
                ''Add New Sub Category in database
                Dim intRetVal As Integer
                Dim strMsg As String
                intRetVal = clsCategory.fnCategoryEditCategory( _
                                hdnCategoryID.Value, hdnSCategoryID.Value, "", "", _
                                strNewSCatCode, strNewSCatName, _
                                strNewAddInfo1, strNewAddInfoM1, _
                                strNewAddInfo2, strNewAddInfoM2, _
                                strNewAddInfo3, strNewAddInfoM3, _
                                strNewAddInfo4, strNewAddInfoM4, _
                                strNewAddInfo5, strNewAddInfoM5, _
                                ddlValidYear.SelectedValue, txtFloatLevel.Text, _
                                ddlExpNotiB4.SelectedValue, Session("UsrID"), "S", _
                                IIf(cbACTag.Checked, "Y", "N"), strUploadFileName)
                If intRetVal > 0 Then
                    strMsg = "Sub Category Update Successfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Sub Category Updated : " + strNewSCatName + IIf(strNewSCatCode = "", "", " (" + strNewSCatCode + ")"))

                    '' ** Save Images in system
                    If strUploadFileF = "Y" Then
                        Dim SaveLocation As String = ""
                        SaveLocation = Server.MapPath(gCategoryPath & hdnSCategoryID.Value & "_" & strUploadFileName)
                        FUploadImage.PostedFile.SaveAs(SaveLocation)
                    End If

                    ''Message Notification
                    Dim strJavaScript As String
                    If hdnCallFrm.Value = "Edit" Then
                        strJavaScript = "<script language = 'Javascript'>" & _
                                        "alert('" + strMsg + "');" & _
                                        "document.location.href='category_edit.aspx?CategoryID=" + hdnCategoryID.Value + "';" & _
                                        "</script>"
                    Else
                        strJavaScript = "<script language = 'Javascript'>" & _
                                        "alert('" + strMsg + "');" & _
                                        "document.location.href='category_view.aspx';" & _
                                        "</script>"
                    End If
                    Response.Write(strJavaScript)
                Else
                    strMsg = "Sub Category Update Failed. Please contact your Administrator."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If

            Else   '' Error Found --> show error message
                lblErrorMessage.Text = "Sub Category Name already exist. Please choose another Sub Category Name."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        If hdnCallFrm.Value = "Edit" Then
            Server.Transfer("category_edit.aspx?CategoryID=" + hdnCategoryID.Value)
        Else
            Server.Transfer("category_view.aspx")
        End If

    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            fnGetRecordsFromDB()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butDelImage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelImage.Click
        Try
            Dim strNewSCatCode As String = txtSubCatCode.Text
            Dim strNewSCatName As String = txtSubCatName.Text

            Dim intRetVal As Integer
            Dim strMsg As String
            intRetVal = clsCategory.fnCategorySub_DeleteImages( _
                            hdnSCategoryID.Value, Session("UsrID"))
            If intRetVal > 0 Then
                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Sub Category Image Deleted : " + strNewSCatName + IIf(strNewSCatCode = "", "", " (" + strNewSCatCode + ")"))

                '' ** Delete Images in server
                Dim strImageFullPath As String = Server.MapPath(hylFileName.NavigateUrl)
                If File.Exists(strImageFullPath) Then
                    File.Delete(strImageFullPath)
                End If

                ''set to "allow upload images"
                FUploadImage.Visible = True
                hdnImageSaveF.Value = "N"
                butDelImage.Visible = False
                hylFileName.Visible = False
                hylFileName.NavigateUrl = ""
                hylFileName.Text = ""

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + "Image deleted successfully." + "');" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "Image delete failed. Please contact your Administrator."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class