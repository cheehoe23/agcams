#Region "Information Section"
' ****************************************************************************************************
' Description       : View Department
' Purpose           : View/Delete Department
' Author            : See Siew
' Date              : 25/02/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class dept_View
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butDelDept.Attributes.Add("OnClick", "return ConfirmDelete()")

                ''Checking For Assign Right
                If (InStr(Session("AR"), "SysSet|dept|add") = 0) Then butAddDept.Visible = False
                If (InStr(Session("AR"), "SysSet|dept|delete") = 0) Then butDelDept.Visible = False

                ''default Sorting
                hdnSortName.Value = "fld_DepartmentCode"
                hdnSortAD.Value = "ASC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
            Throw ex
        End Try

    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Checking whether user not have access right to delete department
            If (InStr(Session("AR"), "SysSet|dept|delete") = 0) Then dgDept.Columns(6).Visible = False

            '' Sort Header Display
            fnSortHeaderDisplay()

            ''Get Department Records
            dgDept.DataSource = clsDepartment.fnDeptGetAllRec(hdnSortName.Value, hdnSortAD.Value)
            dgDept.DataBind()
            If Not dgDept.Items.Count > 0 Then ''Not Department Records found
                dgDept.Visible = False
                ddlPageSize.Enabled = False
                butDelDept.Visible = False
                lblErrorMessage.Text = "There is no department record."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By fld_DepartmentCode (3), fld_DepartmentName (4)
        Dim intSortIndex As Integer
        Dim strSortHeader As String = ""

        ''Set Default Header
        dgDept.Columns(3).HeaderText = "Department Code"
        dgDept.Columns(4).HeaderText = "Department Name"

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fld_DepartmentCode"
                intSortIndex = 3
                strSortHeader = "Department Code"
            Case "fld_DepartmentName"
                intSortIndex = 4
                strSortHeader = "Department Name"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgDept.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgDept.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Function fnShowEditHyperlink(ByVal strDeptID As String) As String
        Dim strRetVal As String = ""
        If (InStr(Session("AR"), "SysSet|dept|edit") > 0) Then  '' Check For Edit Department Access Right
            strRetVal += "<a id=hlEditDept href='dept_Edit.aspx?DeptID=" + strDeptID + "' target=_self >" & _
                        "<img id=imgUpdate src=../images/update.gif alt='Click for Update Department.' border=0 >" & _
                        "</a>"
        End If

        Return (strRetVal)
    End Function

    Private Sub dgDept_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDept.ItemDataBound
        Dim cbDelete As CheckBox
        cbDelete = CType(e.Item.FindControl("DeleteThis"), CheckBox)
        If (InStr(Session("AR"), "SysSet|dept|delete") > 0) Then  '' Check For Delete Department Access Right
            If Not cbDelete Is Nothing Then
                ''If Department assign to lastest asset movement, delete check box invisible
                If e.Item.Cells(1).Text = "Y" Then
                    cbDelete.Enabled = False
                    cbDelete.Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub dgDept_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgDept.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub


    Private Sub dgDept_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgDept.PageIndexChanged
        dgDept.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub


    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgDept.CurrentPageIndex = 0
        dgDept.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub


    Private Sub butAddDept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAddDept.Click
        Response.Redirect("dept_add.aspx")
    End Sub

    Private Sub butDelDept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelDept.Click
        Try
            Dim strMsg As String
            Dim retval As Integer
            Dim strDelDeptNameCode As String = ""

            ''*** Start: Get Department that selected for deleted
            Dim GridItem As DataGridItem
            Dim chkSelectedDel As CheckBox
            Dim strDeptIDForDel As String = ""
            For Each GridItem In dgDept.Items
                chkSelectedDel = CType(GridItem.Cells(6).FindControl("DeleteThis"), CheckBox)
                If chkSelectedDel.Checked Then
                    strDeptIDForDel += GridItem.Cells(0).Text & "^"
                    strDelDeptNameCode += GridItem.Cells(4).Text & " (" & Trim(GridItem.Cells(3).Text) & "),"
                End If
            Next
            If strDelDeptNameCode <> "" Then strDelDeptNameCode = strDelDeptNameCode.Substring(0, strDelDeptNameCode.Length - 1)
            ''*** End  : Get Department that selected for deleted

            ''Start: Delete selected records from database
            retval = clsDepartment.fnDeptInsertUpdateDelete(strDeptIDForDel, "", "", "", Session("UsrID"), "D")
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 Then
                strMsg = "Selected record(s) deleted successfully."
                fnPopulateRecords()

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Delete Department(s) : " + strDelDeptNameCode)
            Else
                strMsg = "Selected record(s) deleted failed."
            End If
            Dim strJavaScript As String
            strJavaScript = "<script language = 'Javascript'>alert('" + strMsg + "');</script>"
            Response.Write(strJavaScript)
            ''End  : Message Notification

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class
