<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="location_Add.aspx.vb" Inherits="AMS.location_Add"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>location_Add</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet" />
        <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
    <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />

		<script language="JavaScript" type="text/javascript" src="../Common/CommonJS.js"></script>
		<script language="javascript">
		function chkFrm() {
			var foundError = false;
			
			//Validate Location Code
			if (!foundError && gfnIsFieldBlank(document.frm_locationAdd.txtLocationCode)) {
				foundError=true;
				document.frm_locationAdd.txtLocationCode.focus();
				alert("Please enter Location Code.");
			}
			if (!foundError && document.frm_locationAdd.txtLocationCode.value.length > 10) {
				foundError=true
				document.frm_locationAdd.txtLocationCode.focus()
				alert("Please make sure the Location Code is less than 10 characters long.")
			}
			
			//Validate Location Name
			if (!foundError && gfnIsFieldBlank(document.frm_locationAdd.txtLocationName)) {
				foundError=true
				document.frm_locationAdd.txtLocationName.focus()
				alert("Please enter Location Name.")
			}
			if (!foundError && document.frm_locationAdd.txtLocationName.value.length > 250) {
				foundError=true
				document.frm_locationAdd.txtLocationName.focus()
				alert("Please make sure the Location Name is less than 250 characters long.")
			}
			
			//Validate at least 1 Sub Location is created 
			if (!foundError && document.frm_locationAdd.hdnSubLocID.value == '0') {
				foundError=true;
				alert("Please create at least one Sub Location.")
			}
			
 			if (!foundError){
 				var flag = false;
 				flag = window.confirm("Are you sure you want to add this Location?");
 				return flag;
 				//return true
 			}
			else
				return false
		}
		function mfnOpenAddSubLocWindow() 
			{
				var foundError = false;
				
				//validate Location Code is not blank 
				if (!foundError && gfnIsFieldBlank(document.frm_locationAdd.txtLocationCode)) {
				    foundError=true;
				    document.frm_locationAdd.txtLocationCode.focus();
				    alert("Please enter Location Code.");
			    }
				
				//validate Location Name is not blank 
				if (!foundError && gfnIsFieldBlank(document.frm_locationAdd.txtLocationName)) {
				    foundError=true
				    document.frm_locationAdd.txtLocationName.focus()
				    alert("Please enter Location Name.")
			    }
			    
				
				if (!foundError){
				    var LC = document.frm_locationAdd.txtLocationCode.value;
					var LN = document.frm_locationAdd.txtLocationName.value;
					window.open('location_AddSub.aspx?CallFrm=Add&LC='+LC+'&LN='+LN, 'Add_SLoc','width=550,height=350,Top=0,left=0,scrollbars=1');
				}
				
				return false;
			
			}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="frm_locationAdd" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Administration : Add New Location</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table id="Table2" height="400" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<!--Start: Main Content-->
						<table id="Table3" height="400" cellSpacing="1" cellPadding="0" width="100%" align="left"
							bgColor="silver" border="0">
							<tr>
								<td>
									<table id="Table4" height="400" cellSpacing="0" cellPadding="0" width="100%" align="left"
										bgColor="white" border="0">
										<tr>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
													<TR>
														<TD vAlign="top" align="center" width="100%">
														    <asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE id="Table6" height="126" cellSpacing="0" cellPadding="3" width="100%" align="left"
																bgColor="white" border="0">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" height="352">
																			<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
																				<TBODY>
																				    <TR>
																						<TD vAlign="middle" width="35%" align=left>
																						    <FONT color="#666666"><font color="red">*</font>Location Code : </FONT>
																						</TD>
																						<TD width="65%">
																						    <asp:textbox id="txtLocationCode" Runat="server" MaxLength=8></asp:textbox>&nbsp;
																							<FONT class="DisplayFieldSize">(Maximum 8 characters)</FONT>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign=top width="35%" align="left">
																							<FONT color="#666666"><font color="red">*</font>Location Name : </FONT>
																						</TD>
																						<TD width="65%">
																						    <asp:textbox id="txtLocationName" Runat="server" maxlength="250" Width="300px"></asp:textbox>
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Sub Location</b></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2" align=right><asp:LinkButton ID=butAddSubLoc runat=server>[Add Sub Location]</asp:LinkButton></TD>
																					</TR>
																				    <TR>
																					    <TD colspan=2>
																					        <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgSubLoc" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="dt_SLocID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn datafield="dt_SLocID" headertext="S/No" ItemStyle-Height="10" Visible=false>
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
										                                                            <asp:boundcolumn datafield="dt_Number" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Sub Location Code" datafield="dt_SLocCode">
												                                                        <itemstyle width="16%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Sub Location Name" datafield="dt_SLocName">
												                                                        <itemstyle width="25%" cssclass="GridText" verticalalign="Middle" ></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn HeaderText="Smart Shelf Rack" ItemStyle-Width="8%" Visible=false ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
												                                                        <itemtemplate>
													                                                        <%#fnShowActivateText(DataBinder.Eval(Container.DataItem, "dt_SLocActv"))%>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
											                                                        <asp:boundcolumn headertext="Light IP Address" datafield="dt_SLocLightIP" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" ></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn HeaderText="Light Type" ItemStyle-Width="10%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" Visible=false>
												                                                        <itemtemplate>
													                                                        <%#fnShowLightTypeText(DataBinder.Eval(Container.DataItem, "dt_SLocLightType"))%>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
											                                                        
											                                                        <asp:templatecolumn HeaderText="Door Gantry" ItemStyle-Width="8%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" Visible=false>
												                                                        <itemtemplate>
													                                                        <%#fnShowActivateText(DataBinder.Eval(Container.DataItem, "dt_DGF"))%>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
											                                                        <asp:boundcolumn headertext="Movement Status" datafield="dt_DGMvStatus" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left ></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        
																									<asp:ButtonColumn ItemStyle-Width="5%" Text="<IMG SRC=../images/delete.gif Border=0>" ItemStyle-HorizontalAlign="Center"
																										HeaderText="<IMG SRC=../images/delete.gif Border=0>" CommandName="Delete"></asp:ButtonColumn>									
										                                                        </Columns>
									                                                        </asp:datagrid>
																					    </TD>
																					</TR>
																					<TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:button id="butSubmit" Runat="Server" Text="Submit"></asp:button>
																							<asp:button id="butReset" Runat="Server" Text="Reset"></asp:button>
																							<asp:button id="ButCancel" Runat="Server" Text="Cancel"></asp:button>
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer>
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnSubLocID" runat="server"> 
			<input type="hidden" id="hdnAddSubLocF" runat="server">
			<input type="hidden" id="hdnAddSubLocCode" runat="server"> 
			<input type="hidden" id="hdnAddSubLocName" runat="server"> 
			<input type="hidden" id="hdnAddSubLocActv" runat="server"> 
			<input type="hidden" id="hdnAddSubLocLightIP" runat="server"> 
			<input type="hidden" id="hdnAddSubLocLightType" runat="server"> 
			<input type="hidden" id="hdnAddSubLocDGF" runat="server"> 
			<input type="hidden" id="hdnAddSubLocDGMvS" runat="server"> 
			<!-- End  : Hidden Fields -->
		</form>
	</body>
</HTML>
