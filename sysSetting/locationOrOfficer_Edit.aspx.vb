#Region "Information Section"
' ****************************************************************************************************
' Description       : Volume Modification Screen  
' Purpose           : Volume Modification Information
' Author            : See Siew
' Date              : 09/04/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Web.Services

#End Region

Partial Class locationOrOfficer_Edit
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim dateculture As IFormatProvider = New System.Globalization.CultureInfo("fr-FR", True)

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            '''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("/common/logout.aspx", False)
                Exit Sub
            End If
            '''********** End  : Check Session Time out ***********

            Me.Header.DataBind()

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then

                ''Get Hidden Data
                hdnCallFrm.Value = clsEncryptDecrypt.DecryptText(Request("CallFrm"))
                hdnLocationOfficerID.Value = clsEncryptDecrypt.DecryptText(Request("fld_LocationOfficerID"))
                'hdnLocOfficerName.Value = clsEncryptDecrypt.DecryptText(Request("fld_LocationOfficerName"))
                'txtLocOfficerName.Text = clsEncryptDecrypt.DecryptText(Request("fld_LocationOfficerName"))

                ''Display File Record
                fnPopulateRecords()

            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub fnPopulateRecords()
        Try
            Dim dt As DataTable = clsLocationOfficer.fnGetDataByfld_LocationOfficerID(hdnLocationOfficerID.Value).Tables(0)
            If dt.Rows.Count > 0 Then
                txtLocOfficerName.Text = dt.Rows(0)("fld_LocationOfficerName").ToString()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click

        Response.Redirect("locationOrOfficer_Search.aspx?CallFrm=" + hdnCallFrm.Value)

    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        txtLocOfficerName.Text = ""
        txtLocOfficerName.Focus()
    End Sub

    Private Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim intRetVal As Integer
            Dim strMsg As String

            If (txtLocOfficerName.Text.Trim() = "") Then
                lblErrorMessage.Text = "Please enter Location/Officer Name!"
                lblErrorMessage.Visible = True
                txtLocOfficerName.Focus()
                Return
            End If

            '''Check for Duplicate locationOfficerName
            Dim dt As DataTable = clsLocationOfficer.fnGetByfld_LocationOfficerName(txtLocOfficerName.Text.Trim().Replace("'", "''")).Tables(0)
            If (dt.Rows.Count > 0) Then
                If (dt.Rows(0)("fld_LocationOfficerID").ToString() <> hdnLocationOfficerID.Value) Then
                    lblErrorMessage.Text = "Duplicate Location/Officer Name in the File Management System! Please Try Again!"
                    lblErrorMessage.Visible = True
                    txtLocOfficerName.Focus()
                    Return
                End If
            Else
                lblErrorMessage.Text = ""
                lblErrorMessage.Visible = False
            End If

            ''Update record
            intRetVal = clsLocationOfficer.fnLocationOfficer_Update(hdnLocationOfficerID.Value, txtLocOfficerName.Text.Trim(), Session("UsrID"))

            If intRetVal > 0 Then
                strMsg = "Updated Successfully."

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "Updated : " + txtLocOfficerName.Text.Trim())

                ''Message Notification
                Dim strJavaScript As String
                Dim strURL As String
                If hdnCallFrm.Value = "Modify" Then
                    'strURL = "file_EditMaster.aspx?FID=" + hdnLocationOfficerID.Value
                Else
                    strURL = "locationOrOfficer_Search.aspx"
                End If
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "document.location.href='" + strURL + "';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "Location/Officer Updated Failed. Please contact your administrator."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub



    'Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
    '    'dgFileMV.CurrentPageIndex = 0
    '    'dgFileMV.PageSize = ddlPageSize.SelectedItem.Value
    '    'fnPopulateFileMvRec()
    'End Sub

    Protected Sub fnCheckAccessRight()
        Try

            If (hdnCallFrm.Value = "Delete") Then
                ''**Remove Asset Type, if access right for creation particular asset types are not given
                If (InStr(Session("AR"), "SysSet|delLoc|Delete") = 0) Then
                    butSubmit.Enabled = False
                    butReset.Enabled = False
                Else
                    butSubmit.Enabled = True
                    butReset.Enabled = True
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("fld_ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class
