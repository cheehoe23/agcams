<%@ Page Language="vb" AutoEventWireup="false" Codebehind="location_Edit.aspx.vb" Inherits="AMS.location_Edit"%>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>location_Edit</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../Common/CommonJS.js"></script>
		<script language="javascript" type="text/javascript">
		function chkFrm() {
			var foundError = false;
			
			//Validate Location Code
			if (!foundError && gfnIsFieldBlank(document.frm_locationEdit.txtLocationCode)) {
				foundError=true;
				document.frm_locationEdit.txtLocationCode.focus();
				alert("Please enter Location Code.");
			}
			if (!foundError && document.frm_locationEdit.txtLocationCode.value.length > 10) {
				foundError=true
				document.frm_locationEdit.txtLocationCode.focus()
				alert("Please make sure the Location Code is less than 10 characters long.")
			}
			
			//Validate Location Name
			if (!foundError && gfnIsFieldBlank(document.frm_locationEdit.txtLocationName)) {
				foundError=true
				document.frm_locationEdit.txtLocationName.focus()
				alert("Please enter Location Name.")
			}
			if (!foundError && document.frm_locationEdit.txtLocationName.value.length > 250) {
				foundError=true
				document.frm_locationEdit.txtLocationName.focus()
				alert("Please make sure the Location Name is less than 250 characters long.")
			}
			
 			if (!foundError){
 				var flag = false;
 				flag = window.confirm("Are you sure want to update this record?");
 				return flag;
 			}
			else
				return false
		}
		function mfnOpenAddSubLocWindow() {
			var foundError = false;
										
			if (!foundError){
			    var LC = document.frm_locationEdit.txtLocationCode.value;
				var LN = document.frm_locationEdit.txtLocationName.value;
				var LID = document.frm_locationEdit.hdnLoctID.value;
				window.open('location_AddSub.aspx?CallFrm=Edit&LC='+LC+'&LN='+LN+'&LocID='+LID+'&SLocID=0&DelF=N', 'Edit_SLoc','width=550,height=350,Top=0,left=0,scrollbars=1');
			}
			
			return false;
		}
		function mfnDeleteSubLoc(SLocID) {	
				var flag;
				flag = false;
				
				//Check whether This is last sub location for location, if last location, then:-
				//--> 1. User need to add another new location.
				if (document.frm_locationEdit.hdnTotRec.value == '1') {
					flag = window.confirm("You have chosen to delete last Sub Location.\nAre you want to add new Sub Location.\nYou cannot undo this action. Continue?");
					if (flag){	
						var LC = document.frm_locationEdit.txtLocationCode.value;
				        var LN = document.frm_locationEdit.txtLocationName.value;
				        var LID = document.frm_locationEdit.hdnLoctID.value;
					    window.open('location_AddSub.aspx?CallFrm=Edit&LC='+LC+'&LN='+LN+'&LocID='+LID+'&SLocID='+ SLocID +'&DelF=Y', 'Edit_SLoc','width=550,height=350,Top=0,left=0,scrollbars=1');
					}
				}
				
				//Not the last sub location --> Normal Deletion 
				else{
					flag = window.confirm("You have chosen to delete this Sub Location.\nYou cannot undo this action. Continue?");
					if (flag){	
						document.frm_locationEdit.hdnSLocID.value = SLocID;
						document.frm_locationEdit.hdnDeleteSubFlag.value = 'Y';
						document.frm_locationEdit.submit();
					}
				}
		    }
		    function mfnDeleteAll() 
		    {
			    var flag = false;
			    flag = window.confirm("You have chosen to delete this Location.\nYou cannot undo this action. Continue?");
			    return flag;
		    }
		</script>
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="frm_locationEdit" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Administration : Edit Location</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table id="Table2" height="400" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<!--Start: Main Content-->
						<table id="Table3" height="400" cellSpacing="1" cellPadding="0" width="100%" align="left"
							bgColor="silver" border="0">
							<tr>
								<td>
									<table id="Table4" height="400" cellSpacing="0" cellPadding="0" width="100%" align="left"
										bgColor="white" border="0">
										<tr>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
													<TR>
														<TD vAlign="top" align="center" width="100%">
														    <asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE id="Table6" height="126" cellSpacing="0" cellPadding="3" width="100%" align="left"
																bgColor="white" border="0">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" height="352">
																			<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
																				<TBODY>
																				    <TR>
																						<TD vAlign="middle" width="35%" align=left>
																						    <FONT color="#666666"><font color="red">*</font>Location Code : </FONT>
																						</TD>
																						<TD width="65%">
																						    <asp:textbox id="txtLocationCode" Runat="server" MaxLength=8></asp:textbox>&nbsp;
																							<FONT class="DisplayFieldSize">(Maximum 8 characters)</FONT></TD>
																					</TR>
																					<TR>
																						<TD vAlign=top width="35%" align="left">
																							<FONT class="DisplayTitle"><font color="red">*</font>Location Name : </FONT>
																						</TD>
																						<TD width="65%"><asp:textbox id="txtLocationName" Runat="server" maxlength="250" Width="300px"></asp:textbox></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Sub Location</b></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2" align=right><asp:LinkButton ID=butAddSubLoc runat=server>[Add Sub Location]</asp:LinkButton></TD>
																					</TR>
																				    <TR>
																					    <TD colspan=2>
																					        <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgSubLoc" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_LocSubID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn datafield="fld_LocationID" headertext="fld_LocationID" ItemStyle-Height="10" Visible=false>
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
										                                                            <asp:boundcolumn datafield="fld_LocSubID" headertext="fld_LocSubID" ItemStyle-Height="10" Visible=false>
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
										                                                            <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Sub Location Code" datafield="fld_LocSubCode">
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Sub Location Name" datafield="fld_LocSubName">
												                                                        <itemstyle width="25%" cssclass="GridText" verticalalign="Middle" ></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <%--<asp:boundcolumn headertext="Smart Shelf Rack" datafield="LocSubActvName">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center ></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Light IP Address" datafield="fld_LightIPAdd">
												                                                        <itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center ></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Light Type" datafield="fld_LightTypeStr">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center ></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        
											                                                        <asp:boundcolumn headertext="Door Gantry" datafield="fld_DGFName">
												                                                        <itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center ></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Movement Status" datafield="fld_DGMvStatus">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center ></itemstyle>
											                                                        </asp:boundcolumn>--%>
											                                                        
																									<asp:hyperlinkcolumn headertext="<img src='../images/update.gif' runat=server id=imgUpdateH>" text="<img src='../images/update.gif' runat=server id=imgUpdateH border=0>"
																										navigateurl="location_EditSub.aspx" datanavigateurlfield="fld_LocSubID" DataNavigateUrlFormatString="location_EditSub.aspx?CallFrm=Edit&SLocID={0}">
																										<itemstyle cssclass="GridText" horizontalalign="Center" width="5%" verticalalign="Middle"></itemstyle>
																									</asp:hyperlinkcolumn>
											                                                       <asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="5%">
																										<headertemplate>
																											<img src="../images/delete.gif" alt="Delete" runat="server" id="imgDelete">
																										</headertemplate>
																										<itemtemplate>
																											<center>
																												<%#fnShowDeleteBut(DataBinder.Eval(Container.DataItem, "fld_LocSubID"), DataBinder.Eval(Container.DataItem, "LoctDeleteF"))%>
																											</center>
																										</itemtemplate>
																									</asp:templatecolumn>
																									<asp:boundcolumn HeaderText="LoctDeleteF" datafield="LoctDeleteF" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn HeaderText="fld_LocationCode" datafield="fld_LocationCode" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn HeaderText="fld_LocationName" datafield="fld_LocationName" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>									
										                                                        </Columns>
									                                                        </asp:datagrid>
																					    </TD>
																					</TR>
																					<TR>
																						<TD align="center" colSpan="2"><BR>
																							<asp:button id="butSubmit" Runat="Server" Text="Submit"></asp:button>
																							<asp:button id="butReset" Runat="Server" Text="Reset"></asp:button>
																							<asp:button id="ButCancel" Runat="Server" Text="Cancel"></asp:button>
																							<asp:Button id="butDelete" Text="Delete" Runat="Server" /></TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start : Hidden Fields -->
			<input type="hidden" id="hdnLoctID" runat="server" />
			<input type="hidden" id="hdnAddSubLocF" runat="server" />
			<input type="hidden" id="hdnDeleteSubFlag"  runat="server" />
			<input type="hidden" id="hdnSLocID" runat="server" />
			<input type="hidden" id="hdnTotRec"  runat="server" />
			<input type="hidden" id="hdnLocUseF"  runat="server" />
			<!-- End   : Hidden Fields -->
			<tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer>
		</form>
	</body>
</html>
