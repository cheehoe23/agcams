#Region "Information Section"
' ****************************************************************************************************
' Description       : View Category 
' Purpose           : View Category   Information
' Date              : 28/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class category_view
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butDelCategory.Attributes.Add("OnClick", "return ConfirmDelete()")

                ''Checking For Assign Right
                If (InStr(Session("AR"), "SysSet|catg|add") = 0) Then butAddCategory.Visible = False
                If (InStr(Session("AR"), "SysSet|catg|delete") = 0) Then butDelCategory.Visible = False

                ''default Sorting
                hdnSortName.Value = "fld_CategoryCode"
                hdnSortAD.Value = "ASC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()

                ''Delete Temporary Sub Category
                fnClearTempCategoryFile(Server.MapPath(gCategoryPath), Session("UsrID"))
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Checking whether user not have access right to deleted
            If (InStr(Session("AR"), "SysSet|catg|delete") = 0) Then dgCategory.Columns(12).Visible = False

            '' Sort Header Display
            fnSortHeaderDisplay()
            
            ''Get User Group Records
            dgCategory.DataSource = clsCategory.fnCategoryGetAllRec(0, 0, hdnSortName.Value, hdnSortAD.Value)
            dgCategory.DataBind()
            If Not dgCategory.Items.Count > 0 Then ''Not Records found
                ddlPageSize.Enabled = False
                butDelCategory.Visible = False
                lblErrorMessage.Text = "There is no record found."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By fld_CategoryCode (3), fld_CategoryName (4), fld_CatSubCode (5), fld_CatSubName (6)
        Dim intSortIndex As Integer
        Dim strSortHeader As String = ""

        ''Set Default Header
        dgCategory.Columns(3).HeaderText = "Category Code"
        dgCategory.Columns(4).HeaderText = "Category Name"
        dgCategory.Columns(5).HeaderText = "Sub Category Code"
        dgCategory.Columns(6).HeaderText = "Sub Category Name"

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fld_CategoryCode"
                intSortIndex = 3
                strSortHeader = "Category Code"
            Case "fld_CategoryName"
                intSortIndex = 4
                strSortHeader = "Category Name"
            Case "fld_CatSubCode"
                intSortIndex = 5
                strSortHeader = "Sub Category Code"
            Case "fld_CatSubName"
                intSortIndex = 6
                strSortHeader = "Sub Category Name"

        End Select

        If hdnSortAD.Value = "ASC" Then
            dgCategory.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgCategory.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Public Function fnGetAddInfo(ByVal strAddInfo1 As String, ByVal strAddInfoM1 As String, _
                                 ByVal strAddInfo2 As String, ByVal strAddInfoM2 As String, _
                                 ByVal strAddInfo3 As String, ByVal strAddInfoM3 As String, _
                                 ByVal strAddInfo4 As String, ByVal strAddInfoM4 As String, _
                                 ByVal strAddInfo5 As String, ByVal strAddInfoM5 As String) As String
        Dim strRetVal As String = ""
        Dim strNewAddInfo As String = ""
        Dim strNewAddInfo1 As String = ""
        Dim strNewAddInfo2 As String = ""
        Dim strNewAddInfo3 As String = ""
        Dim strNewAddInfo4 As String = ""
        Dim strNewAddInfo5 As String = ""
        Dim strNewAddInfoM1 As String = ""
        Dim strNewAddInfoM2 As String = ""
        Dim strNewAddInfoM3 As String = ""
        Dim strNewAddInfoM4 As String = ""
        Dim strNewAddInfoM5 As String = ""

        ''** Get New Additional Information to display
        fnGetSubCatAddInfo( _
            strAddInfo1, strAddInfo2, strAddInfo3, strAddInfo4, strAddInfo5, _
            strAddInfoM1, strAddInfoM2, strAddInfoM3, strAddInfoM4, strAddInfoM5, _
            strNewAddInfo1, strNewAddInfo2, strNewAddInfo3, strNewAddInfo4, strNewAddInfo5, _
            strNewAddInfoM1, strNewAddInfoM2, strNewAddInfoM3, strNewAddInfoM4, strNewAddInfoM5, _
            strNewAddInfo)

        strRetVal = IIf(strNewAddInfo = "", " - ", strNewAddInfo)
        Return (strRetVal)
    End Function

    Protected Sub butAddCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAddCategory.Click
        Server.Transfer("category_add.aspx")
    End Sub

    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgCategory.CurrentPageIndex = 0
        dgCategory.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Private Sub dgCategory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCategory.ItemDataBound
        Dim cbDelete As CheckBox
        cbDelete = CType(e.Item.FindControl("DeleteThis"), CheckBox)
        If (InStr(Session("AR"), "SysSet|catg|delete") > 0) Then  '' Check For Delete Access Right
            If Not cbDelete Is Nothing Then
                If e.Item.Cells(13).Text = "Y" Then
                    cbDelete.Enabled = False
                    cbDelete.Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub dgCategory_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgCategory.PageIndexChanged
        dgCategory.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub dgCategory_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgCategory.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

    Protected Sub butDelCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelCategory.Click
        Try
            Dim strMsg As String
            Dim retval As Integer
            Dim strDelCatNameCode As String = ""

            ''*** Start: Get Cat that selected for deleted
            Dim GridItem As DataGridItem
            Dim chkSelectedDel As CheckBox
            Dim strSCatIDForDel As String = ""
            For Each GridItem In dgCategory.Items
                chkSelectedDel = CType(GridItem.Cells(12).FindControl("DeleteThis"), CheckBox)
                If chkSelectedDel.Checked Then
                    strSCatIDForDel += GridItem.Cells(1).Text & "^"
                    strDelCatNameCode += GridItem.Cells(14).Text & " (" & Trim(GridItem.Cells(3).Text) & ") - " & GridItem.Cells(15).Text & ","
                End If
            Next
            If strDelCatNameCode <> "" Then strDelCatNameCode = strDelCatNameCode.Substring(0, strDelCatNameCode.Length - 1)
            ''*** End  : Get Cat that selected for deleted

            ''Start: Delete selected records from database
            retval = clsCategory.fnCategoryDeleteCategory("", strSCatIDForDel, Session("UsrID"), "V")
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 Then
                strMsg = "Selected record(s) deleted successfully."
                fnPopulateRecords()

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Delete Category - Subcategory : " + strDelCatNameCode)
            Else
                strMsg = "Selected record(s) deleted failed."
            End If
            Dim strJavaScript As String
            strJavaScript = "<script language = 'Javascript'>alert('" + strMsg + "');</script>"
            Response.Write(strJavaScript)
            ''End  : Message Notification

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class