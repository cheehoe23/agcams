#Region "Information Section"
' ****************************************************************************************************
' Description       : Edit Category 
' Purpose           : Edit Category Information
' Date              : 28/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class category_edit
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                hdnCategoryID.Value = Request("CategoryID")
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")
                Me.butDelete.Attributes.Add("OnClick", "return mfnDeleteAll()")
                butAddSubCat.Attributes.Add("OnClick", "return mfnOpenAddSubCatWindow()")

                ''Set Default 
                hdnAddSubCatF.Value = "N"
                hdnDeleteSubFlag.Value = "N"

                ''Populate Records
                fnGetRecordsFromDB()
            End If

            ''New Subcategory have been added
            If hdnAddSubCatF.Value = "Y" Then
                hdnAddSubCatF.Value = "N"
                hdnCategoryID.Value = Request("CategoryID")
                fnGetRecordsFromDB()
            End If

            ''Delete Subcategory 
            If hdnDeleteSubFlag.Value = "Y" Then
                fnDeleteSubcategory()
                hdnDeleteSubFlag.Value = "N"
            End If


        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnGetRecordsFromDB() As Boolean
        Dim objRdr As SqlDataReader
        Try
            ''Get Main Category Record
            objRdr = clsCategory.fnCategoryGetRecordsForMainCategory(hdnCategoryID.Value)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()

                    txtCatCode.Text = CStr(objRdr("fld_CategoryCode"))
                    txtCatName.Text = CStr(objRdr("fld_CategoryName"))
                    hdnTotRec.Value = CStr(objRdr("totRec"))
                    hdnCatAssign2Asset.Value = CStr(objRdr("CatAssign2Asset"))

                End If
            End If
            objRdr.Close()

            ''Get Subcategory Record
            dgSubCategory.DataSource = clsCategory.fnCategoryGetAllRec(hdnCategoryID.Value, 0, "fld_CatSubID", "ASC")
            dgSubCategory.DataBind()
            If Not dgSubCategory.Items.Count > 0 Then ''Not Records found
                lblErrorMessage.Text = "There is no Subcategory record found. Please contact your administrator."
                lblErrorMessage.Visible = True
            End If

            ''Checking For User Access
            fnCheckForUserAccess()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Function

    Public Sub fnCheckForUserAccess()
        ''Case 1: Access right
        If (InStr(Session("AR"), "SysSet|catg|add") = 0) Then butAddSubCat.Visible = False

        If (InStr(Session("AR"), "SysSet|catg|delete") = 0) Then
            butDelete.Visible = False
            dgSubCategory.Columns(12).Visible = False
        End If

        If (InStr(Session("AR"), "SysSet|catg|edit") = 0) Then
            txtCatCode.Enabled = False
            txtCatName.Enabled = False
            dgSubCategory.Columns(11).Visible = False

            butSubmit.Enabled = False
            butReset.Enabled = False
        End If

        ''Case 2: check whether allow to delete whole record (DELETE button) 
        If hdnCatAssign2Asset.Value = "Y" Then
            txtCatCode.Enabled = False
            butDelete.Visible = False
        End If
    End Sub

    Public Function fnGetAddInfo(ByVal strAddInfo1 As String, ByVal strAddInfoM1 As String, _
                                 ByVal strAddInfo2 As String, ByVal strAddInfoM2 As String, _
                                 ByVal strAddInfo3 As String, ByVal strAddInfoM3 As String, _
                                 ByVal strAddInfo4 As String, ByVal strAddInfoM4 As String, _
                                 ByVal strAddInfo5 As String, ByVal strAddInfoM5 As String) As String
        Dim strRetVal As String = ""
        Dim strNewAddInfo As String = ""
        Dim strNewAddInfo1 As String = ""
        Dim strNewAddInfo2 As String = ""
        Dim strNewAddInfo3 As String = ""
        Dim strNewAddInfo4 As String = ""
        Dim strNewAddInfo5 As String = ""
        Dim strNewAddInfoM1 As String = ""
        Dim strNewAddInfoM2 As String = ""
        Dim strNewAddInfoM3 As String = ""
        Dim strNewAddInfoM4 As String = ""
        Dim strNewAddInfoM5 As String = ""

        ''** Get New Additional Information to display
        fnGetSubCatAddInfo( _
            strAddInfo1, strAddInfo2, strAddInfo3, strAddInfo4, strAddInfo5, _
            strAddInfoM1, strAddInfoM2, strAddInfoM3, strAddInfoM4, strAddInfoM5, _
            strNewAddInfo1, strNewAddInfo2, strNewAddInfo3, strNewAddInfo4, strNewAddInfo5, _
            strNewAddInfoM1, strNewAddInfoM2, strNewAddInfoM3, strNewAddInfoM4, strNewAddInfoM5, _
            strNewAddInfo)

        strRetVal = IIf(strNewAddInfo = "", " - ", strNewAddInfo)
        Return (strRetVal)
    End Function

    Public Function fnShowDeleteBut( _
                            ByVal strSCatID As String, _
                            ByVal strAssign2Asset As String) As String
        ''Just allow to delete Sub category, which have following condition:-
        ''--> 1. Not assign to any asset 
        Dim strRetVal As String = ""
        If (InStr(Session("AR"), "SysSet|catg|delete") > 0) Then  '' Check For Delete Sub category Access Right
            If strAssign2Asset = "N" Then
                strRetVal = "<a id=hlDelRemark href='javascript:mfnDeleteSubcat(" + strSCatID + ")'>" & _
                            "<img id=imgUpdate src=../images/delete.gif alt='Click for Delete Subcategory.' border=0 >" & _
                            "</a>"
            End If
        End If

        Return (strRetVal)
    End Function

    Protected Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Server.Transfer("category_view.aspx")
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        fnGetRecordsFromDB()
    End Sub

    Protected Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        Try
            Dim intRetVal As Integer
            Dim strMsg As String
            intRetVal = clsCategory.fnCategoryDeleteCategory(hdnCategoryID.Value, "", Session("UsrID"), "A")
            If intRetVal > 0 Then
                strMsg = "Category have been delete Successfully "

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Category Deleted : " + txtCatName.Text + " (" + txtCatCode.Text + ")")

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "document.location.href='category_View.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "Category have been delete Failed. Please contact your Administrator."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnDeleteSubcategory()
        Try
            Dim intRetVal As Integer
            Dim strMsg As String
            intRetVal = clsCategory.fnCategoryDeleteCategory("", hdnSCatID.Value, Session("UsrID"), "S")
            If intRetVal > 0 Then
                strMsg = "Subcategory have been delete Successfully "

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Subcategory Deleted for Category : " + txtCatName.Text + " (" + txtCatCode.Text + ")")

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "</script>"
                Response.Write(strJavaScript)

                ''Populate again record
                fnGetRecordsFromDB()
            Else
                strMsg = "Subcategory have been delete Failed. Please contact your Administrator."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim intRetVal As Integer
            Dim strMsg As String
            Dim strCatCode As String
            Dim strCatName As String

            ''Get Data
            strCatCode = txtCatCode.Text
            strCatName = txtCatName.Text

            ''Check for duplicate 
            If Not clsCategory.fnCategoryCheckDuplicate(hdnCategoryID.Value, 0, strCatCode, "", "1", "E") Then
                intRetVal = clsCategory.fnCategoryEditCategory( _
                                            hdnCategoryID.Value, 0, strCatCode, strCatName, _
                                            "", "", "", "", "", "", "", "", "", "", "", "", "", "", _
                                            "", Session("UsrID"), "M", "", "")
                If intRetVal > 0 Then
                    strMsg = "Category have been update Successfully "

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Category Updated: " + txtCatName.Text + " (" + txtCatCode.Text + ")")

                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "document.location.href='category_view.aspx';" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                    'fnPopulateRecords()
                Else
                    strMsg = "Category have been update Failed. Please contact your Administrator."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If

            Else 'Duplicate Status Name
                strMsg = "Category Code already exist. Please enter another Category Code."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnGetFileName(ByVal strCatSubID As String, ByVal strFileNameSave As String) As String
        Try
            Dim strRetVal As String = ""

            If Trim(strFileNameSave) <> "" Then
                strRetVal = "<a id=hlFile href='" & gCategoryPath & strCatSubID & "_" & strFileNameSave & "' target=CatEditFile >" & _
                             "<img id=imgView src=../images/audit.gif alt='Click for view image.' border=0 >" & _
                             "</a>"
            Else
                strRetVal = "-"
            End If

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function
End Class