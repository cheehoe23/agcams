#Region "Information Section"
' ****************************************************************************************************
' Description       : Edit New Department 
' Purpose           : Edit New Department Information
' Author            : See Siew
' Date              : 27/02/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Web.Services

#End Region

Partial Class devicetype_Edit
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")

                ''Get Deprtment ID 
                hdnDeviceTypeID.Value = Request("DeviceTypeID")

                ' ''Get Owner

                ''Retrieve Records 
                fnGetRecordsFromDB()
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnGetRecordsFromDB()
        Dim objRdr As SqlDataReader
        Try
            objRdr = clsDeviceType.fnDeviceType_GetDeviceTypeForEdit(hdnDeviceTypeID.Value)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()
                    txtDeviceType.Text = CStr(objRdr("fld_DeviceType"))
                End If
            End If
            objRdr.Close()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Private Sub ButCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButCancel.Click
        Response.Redirect("devicetype_View.aspx")
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        fnGetRecordsFromDB()
    End Sub

    Private Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim strDeviceType As String
            Dim bDupDeviceType As Boolean = False
            Dim intRetVal As Integer
            Dim strMsg As String

            ''** get Fields Value
            strDeviceType = txtDeviceType.Text

            ''** Check for Duplicate Department Code
            bDupDeviceType = clsDeviceType.fnDeviceType_DuplicateDeviceType(hdnDeviceTypeID.Value, strDeviceType, "E")

            If Not bDupDeviceType Then ''not duplicate record
                ''Edit record
                intRetVal = clsDeviceType.fnDeviceType_InsertUpdateDelete( _
                                    hdnDeviceTypeID.Value, strDeviceType, Session("UsrID"), "E")

                If intRetVal > 0 Then
                    strMsg = "Device Type Updated Successfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Device Type Updated : " + strDeviceType + ")")

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "document.location.href='devicetype_View.aspx';" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                    'Server.Transfer("dept_View.aspx")
                Else
                    strMsg = "Device Type Update Failed."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If

            Else  ''duplicate record
                lblErrorMessage.Text = "Device Type already exist. Please choose another Device Type."
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

End Class
