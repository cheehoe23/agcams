#Region "Information Section"
' ****************************************************************************************************
' Description       : Search Result File --> for Modify  
' Purpose           : Search Result File Information
' Author            : WIN
' Date              : 16/05/2016
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class locationOrOfficer_SearchResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            '''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("/common/logout.aspx", False)
                Exit Sub
            End If
            '''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            ClientScript.GetPostBackEventReference(Me, String.Empty)

            If Not Page.IsPostBack Then

                Me.butDelFile.Attributes.Add("OnClick", "return fnConfirmSelectedfile('Delete')")
                Me.btnRFIDPrint.Attributes.Add("OnClick", "return fnConfirmSelectedfile('PRPL')")

                ''Get Hidden Data
                hdnCallFrom.Value = Request("CallFrm")
                hdnLocOfficerID.Value = clsEncryptDecrypt.DecryptText(Request("strLocOfficerID")) '--> Search, Modify, Delete
                hdnLocOfficerName.Value = clsEncryptDecrypt.DecryptText(Request("strLocOfficerName"))

                ''Get Header
                If hdnCallFrom.Value = "Search" Then
                    lblHeader.Text = "File Management : Search Location/Officer"
                ElseIf hdnCallFrom.Value = "Delete" Then
                    lblHeader.Text = "File Management : Delete Location/Officer"
                End If

                ''default Sorting
                hdnSortName.Value = "fld_LocationOfficerBarcodeID"
                hdnSortAD.Value = "ASC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Checking for access right
                fnCheckAccessRight()

                ''default page for page 1
                dgLocOfficer.CurrentPageIndex = 0

                ''Display Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub fnPopulateRecords()
        Try
            ''' Sort Header Display
            fnSortHeaderDisplay()

            dgLocOfficer.DataSource = clsLocationOfficer.fnGetDataByfld_LocationOfficerIDAndName(hdnLocOfficerID.Value, hdnLocOfficerName.Value, hdnSortName.Value, hdnSortAD.Value)
            dgLocOfficer.DataBind()
            If Not dgLocOfficer.Items.Count > 0 Then
                dgLocOfficer.Visible = False
                ddlPageSize.Enabled = False
                lblErrorMessage.Text = "There is no file record(s) found."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            'Throw ex
        End Try
    End Sub

    Private Sub fnCheckAccessRight()
        Try
            Select Case hdnCallFrom.Value '--> Search, Modify, Archive, Restore, Delete, AddKIV
                Case "Search"
                    dgLocOfficer.Columns(5).Visible = True
                    butDelFile.Visible = False
                    btnRFIDPrint.Visible = True
                    If (InStr(Session("AR"), "SysSet|searchLoc|print") = 0) Then
                        btnRFIDPrint.Enabled = False
                    Else
                        btnRFIDPrint.Enabled = True
                    End If

                Case "Delete"
                    dgLocOfficer.Columns(5).Visible = False
                    btnRFIDPrint.Visible = False
                    butDelFile.Visible = True
                    If (InStr(Session("AR"), "SysSet|delLoc|delete") = 0) Then
                        butDelFile.Enabled = False
                    Else
                        butDelFile.Enabled = True
                    End If
            End Select

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnShowEditPrintIcon(ByVal fld_LocationOfficerID As String, _
                                        ByVal fld_LocationOfficerName As String)
        Try
            Dim strRetVal As String = ""

            '' Check For Edit File Access Right
            If (InStr(Session("AR"), "SysSet|searchLoc|edit") > 0) Then
                strRetVal = "<a id=hlEditFileV href='locationOrOfficer_Edit.aspx?CallFrm=" + clsEncryptDecrypt.EncryptText(hdnCallFrom.Value) + "&fld_LocationOfficerID=" + clsEncryptDecrypt.EncryptText(fld_LocationOfficerID) + "&fld_LocationOfficerName=" + clsEncryptDecrypt.EncryptText(fld_LocationOfficerName) + "' target=_self >" & _
                             "<img id=imgUpdate src=../images/update.gif alt='Click for update Record.' border=0 >" & _
                             "</a>"
            End If

            '' Check For Edit File Access Right
            'If (InStr(Session("AR"), "SysSet|searchLoc|print") > 0) Then
            '    strRetVal += "<a id=hlEditPrintALabel href=javascript:gfnprintLocOfficer('" + fld_LocationOfficerID + "')>" & _
            '                     "<img id=imgUpdate src=../images/print.gif alt='Click for print label.' border=0 >" & _
            '                     "</a>"
            'End If

            If (InStr(Session("AR"), "SysSet|searchLoc|print") > 0) Then
                strRetVal += "<a id=hlEditPrintALabel href=# onclick=javascript:ClickPrint('" + fld_LocationOfficerID + "^')>" & _
                             "<img id=imgUpdate src=../images/print.gif alt='Click for print label.' border=0 >" & _
                             "</a>"
            End If

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgLocOfficer.CurrentPageIndex = 0
        dgLocOfficer.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Private Sub dgFile_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgLocOfficer.PageIndexChanged
        dgLocOfficer.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub dgFile_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgLocOfficer.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By FileNum (3), FileNum (4), fld_FileRef (5), fld_VolumeNo (6), 
        ''                fld_FileTitle (7), fld_Keyword (8), OffLocName(9)
        Dim intSortIndex As Integer
        Dim strSortHeader As String

        ''Set Default Header
        dgLocOfficer.Columns(2).HeaderText = "Location/Officer ID"
        dgLocOfficer.Columns(3).HeaderText = "Location/Officer Name"

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fld_LocationOfficerBarcodeID"
                intSortIndex = 2
                strSortHeader = "Location/Officer ID"
            Case "fld_LocationOfficerName"
                intSortIndex = 3
                strSortHeader = "Location/Officer Name"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgLocOfficer.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgLocOfficer.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
        'End If
    End Function

    Protected Sub butDelFile_Click(sender As Object, e As EventArgs) Handles butDelFile.Click
        Try
            Dim intRetVal As Integer
            Dim strMsg As String
            Dim strLocationOfficerID As String

            strLocationOfficerID = fnGetSelectedLocOffRec()
            'If strLocationOfficerID <> "" Then strLocationOfficerID = strLocationOfficerID.Substring(0, strLocationOfficerID.Length - 1)

            ' ''Checking whether Loc/Off selected for Delete
            If strLocationOfficerID <> "" Then
                ''Delete Funtion Going Here
                intRetVal = clsLocationOfficer.fnLocationOfficer_Delete(strLocationOfficerID, Session("UsrID"))

                If intRetVal > 0 Then
                    strMsg = "Record(s) Delete Successfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "Record(s) Deleted : " + strLocationOfficerID)

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)

                    ''Populate Record
                    fnPopulateRecords()
                Else
                    strMsg = "Record(s) Delete Failed."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If

            Else
                lblErrorMessage.Text = "At least one record should be selected for Delete."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub btnRFIDPrint_Click(sender As Object, e As EventArgs) Handles btnRFIDPrint.Click

        Try
            Dim strLocOffID As String = ""

            ''Get Selected Asset IDs
            strLocOffID = fnGetSelectedLocOffRec()

            If Trim(strLocOffID) <> "" Then

                Session("LocOffIDs" + Session("UsrID").ToString()) = strLocOffID
                Session("ModuleName" + Session("UsrID").ToString()) = "FTS"
                Session("File" + Session("UsrID").ToString()) = ""
                Session("AddFile" + Session("UsrID").ToString()) = ""
                Session("LocationOrOfficer" + Session("UsrID").ToString()) = "1"

                ClientScript.RegisterStartupScript(Me.[GetType](), "myalert", "window.open('../common/PrintPopUp.aspx','_blank','left=400, top=200, width=400, height=200, menubar=no, resizable=no, status=no, titlebar=no, toolbar=no');", True)


                'clsRFIDLabel.fnCreateRFIDLabelLocationOfficer(strLocOffID, Session("UsrID"), _
                '                                   Server.MapPath(gRFIDLabelPath), "")

                ' ''Insert Audit Trail
                'clsCommon.fnAuditInsertRec(Session("UsrID"), strLocOffID, "RFID passive label printed.")

                'Dim strJavaScript As String
                'strJavaScript = "<script language = 'Javascript'>" & _
                '                "alert('" + "RFID Tag print succesfully." + "');" & _
                '                "</script>"
                'Response.Write(strJavaScript)

            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try

    End Sub

    Private Function fnGetSelectedLocOffRec() As String
        Try
            Dim strSelectedRec As String = ""

            ''*** Get record that selected 
            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox
            For Each GridItem In dgLocOfficer.Items
                chkSelectedRec = CType(GridItem.Cells(5).FindControl("DeleteThis"), CheckBox)
                If chkSelectedRec.Checked Then
                    strSelectedRec += GridItem.Cells(0).Text & "^"
                End If
            Next

            Return strSelectedRec
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub dgLocOfficer_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgLocOfficer.ItemDataBound

        If (hdnCallFrom.Value = "Delete") Then
            Dim cbDelete As CheckBox
            cbDelete = CType(e.Item.FindControl("DeleteThis"), CheckBox)
            If (InStr(Session("AR"), "SysSet|delLoc|delete") > 0) Then  '' Check For Delete Department Access Right
                If Not cbDelete Is Nothing Then
                    ''If loc/Off assign to lastest file movement, delete check box invisible
                    If e.Item.Cells(6).Text = "Y" Then
                        cbDelete.Enabled = False
                        cbDelete.Visible = False
                    End If
                End If
            End If
        End If
        
    End Sub

    Private Sub hdnclickprint_Click(sender As Object, e As System.EventArgs) Handles hdnclickprint.Click
        Try
            Dim strLocOffIDs As String = hdnLocOffIDs.Value
            Dim strFileNameLabel As String = ""

            If Trim(strLocOffIDs) <> "" Then

                Session("LocOffIDs" + Session("UsrID").ToString()) = strLocOffIDs
                Session("ModuleName" + Session("UsrID").ToString()) = "FTS"
                Session("File" + Session("UsrID").ToString()) = ""
                Session("AddFile" + Session("UsrID").ToString()) = ""
                Session("LocationOrOfficer" + Session("UsrID").ToString()) = "1"

                ClientScript.RegisterStartupScript(Me.[GetType](), "myalert", "window.open('../common/PrintPopUp.aspx','_blank','left=400, top=200, width=400, height=200, menubar=no, resizable=no, status=no, titlebar=no, toolbar=no');", True)

                'clsRFIDLabel.fnCreateRFIDLabelLocationOfficer(strLocOffIDs, Session("UsrID"), _
                '                                   Server.MapPath(gRFIDLabelPath), strFileNameLabel)

                ' ''Insert Audit Trail
                'clsCommon.fnAuditInsertRec(Session("UsrID"), strLocOffIDs, "RFID passive label printed.")

                'Dim strJavaScript As String
                'strJavaScript = "<script language = 'Javascript'>" & _
                '                "alert('" + "RFID Tag print succesfully." + "');" & _
                '                "</script>"
                'Response.Write(strJavaScript)

            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

End Class
