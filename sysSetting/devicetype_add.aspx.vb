#Region "Information Section"
' ****************************************************************************************************
' Description       : Add New Department 
' Purpose           : Add New Department Information
' Author            : See Siew
' Date              : 20/10/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Web.Services

#End Region

Partial Class devicetype_add
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label
    Protected WithEvents rdDeptType As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents txtUDeptCode As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblDeptCodeChar As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                txtDeviceType.Focus()
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")

            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        lblErrorMessage.Visible = False
        lblErrorMessage.Text = ""
        txtDeviceType.Text = ""

    End Sub

    Private Sub ButCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButCancel.Click
        Response.Redirect("devicetype_View.aspx")
    End Sub

    Private Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim strDeviceType As String
            Dim bDupDeviceType As Boolean = False
            Dim intRetVal As Integer
            Dim strMsg As String

            ''** get Fields Value
            strDeviceType = Trim(txtDeviceType.Text)

            ''** Check for Duplicate Department Code
            bDupDeviceType = clsDeviceType.fnDeviceType_DuplicateDeviceType(0, strDeviceType, "I")

            If Not bDupDeviceType Then ''not duplicate record

                intRetVal = clsDeviceType.fnDeviceType_InsertUpdateDelete( _
                                    0, strDeviceType, Session("UsrID"), "I")

                If intRetVal > 0 Then
                    strMsg = "New Device Type Added Successfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "New Device Type created : " + strDeviceType)

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "document.location.href='devicetype_View.aspx';" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                    'Server.Transfer("dept_View.aspx")
                Else
                    strMsg = "New Device Type Added Failed."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If

            Else  ''duplicate record
                lblErrorMessage.Text = "Device Type already exist. Please choose another Device Type."
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

End Class
