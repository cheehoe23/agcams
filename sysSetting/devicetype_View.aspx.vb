#Region "Information Section"
' ****************************************************************************************************
' Description       : View Department
' Purpose           : View/Delete Department
' Author            : See Siew
' Date              : 25/02/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class devicetype_View
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butDelDeviceType.Attributes.Add("OnClick", "return ConfirmDelete()")

                ''Checking For Assign Right
                If (InStr(Session("AR"), "SysSet|devicetype|add") = 0) Then butAddDeviceType.Visible = False
                If (InStr(Session("AR"), "SysSet|devicetype|delete") = 0) Then butDelDeviceType.Visible = False

                ''default Sorting
                hdnSortName.Value = "fld_DeviceType"
                hdnSortAD.Value = "ASC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
            Throw ex
        End Try

    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Checking whether user not have access right to delete department
            If (InStr(Session("AR"), "SysSet|devicetype|delete") = 0) Then dgDeviceType.Columns(6).Visible = False

            '' Sort Header Display
            fnSortHeaderDisplay()

            ''Get Department Records
            dgDeviceType.DataSource = clsDeviceType.fnDeviceTypeGetAllRec(hdnSortName.Value, hdnSortAD.Value)
            dgDeviceType.DataBind()
            If Not dgDeviceType.Items.Count > 0 Then ''Not Department Records found
                dgDeviceType.Visible = False
                ddlPageSize.Enabled = False
                butDelDeviceType.Visible = False
                lblErrorMessage.Text = "There is no Device Type record."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By fld_DeviceType (3), fld_DepartmentName (4)
        Dim intSortIndex As Integer
        Dim strSortHeader As String = ""

        ''Set Default Header
        dgDeviceType.Columns(3).HeaderText = "Device Type"

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fld_DeviceType"
                intSortIndex = 3
                strSortHeader = "Device Type"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgDeviceType.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgDeviceType.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Function fnShowEditHyperlink(ByVal strDeviceTypeID As String) As String
        Dim strRetVal As String = ""
        If (InStr(Session("AR"), "SysSet|dept|edit") > 0) Then  '' Check For Edit Device Type Access Right
            strRetVal += "<a id=hlEditDept href='devicetype_Edit.aspx?DeviceTypeID=" + strDeviceTypeID + "' target=_self >" & _
                        "<img id=imgUpdate src=../images/update.gif alt='Click for Update Device Type.' border=0 >" & _
                        "</a>"
        End If

        Return (strRetVal)
    End Function

    Private Sub dgDeviceType_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDeviceType.ItemDataBound
        Dim cbDelete As CheckBox
        cbDelete = CType(e.Item.FindControl("DeleteThis"), CheckBox)
        If (InStr(Session("AR"), "SysSet|dept|delete") > 0) Then  '' Check For Delete Department Access Right
            If Not cbDelete Is Nothing Then
                ''If Department assign to lastest asset movement, delete check box invisible
                If e.Item.Cells(1).Text = "Y" Then
                    cbDelete.Enabled = False
                    cbDelete.Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub dgDeviceType_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgDeviceType.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub


    Private Sub dgDeviceType_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgDeviceType.PageIndexChanged
        dgDeviceType.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub


    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgDeviceType.CurrentPageIndex = 0
        dgDeviceType.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub


    Private Sub butAddDeviceType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAddDeviceType.Click
        Response.Redirect("devicetype_add.aspx")
    End Sub

    Private Sub butDelDeviceType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelDeviceType.Click
        Try
            Dim strMsg As String
            Dim retval As Integer
            Dim strDelDeviceType As String = ""

            ''*** Start: Get Department that selected for deleted
            Dim GridItem As DataGridItem
            Dim chkSelectedDel As CheckBox
            Dim strDeviceTypeIDForDel As String = ""
            For Each GridItem In dgDeviceType.Items
                chkSelectedDel = CType(GridItem.Cells(4).FindControl("DeleteThis"), CheckBox)
                If chkSelectedDel.Checked Then
                    strDeviceTypeIDForDel += GridItem.Cells(0).Text & "^"
                    strDelDeviceType += GridItem.Cells(3).Text & ","
                End If
            Next
            
            ''Start: Delete selected records from database
            retval = clsDeviceType.fnDeviceType_InsertUpdateDelete(strDeviceTypeIDForDel, "", Session("UsrID"), "D")
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 Then
                strMsg = "Selected record(s) deleted successfully."
                fnPopulateRecords()

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Delete Device Type(s) : " + strDelDeviceType.ToString.TrimEnd(","))
            Else
                strMsg = "Selected record(s) deleted failed."
            End If
            Dim strJavaScript As String
            strJavaScript = "<script language = 'Javascript'>alert('" + strMsg + "');</script>"
            Response.Write(strJavaScript)
            ''End  : Message Notification

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class
