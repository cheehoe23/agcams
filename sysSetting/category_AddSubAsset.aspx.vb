#Region "Information Section"
' ****************************************************************************************************
' Description       : Add New Sub Category 
' Purpose           : Add New Sub Category Information
' Author            : See Siew
' Date              : 23/10/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class category_AddSubAsset
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")

                ''Get Pass Value from Main screen
                hdnCallFrm.Value = Request("CallFrm")   ''Called from "Add" screen OR "Edit" screen
                hdnCatID.Value = Request("CatID")       ''Just for Called From "Edit" screen
                hdnSCatID.Value = Request("SCatID")     ''Just for Called From "Edit" screen -->  Sub Category ID that want to delete
                hdnDeleteF.Value = Request("DeleteF")   ''Just for Called From "Edit" screen  --> "Y"=Delete Sub Status
                lblCatCode.Text = Request("CC")
                lblCatName.Text = Request("CN")

                ''set Add Sub Status Flag to "N"
                hdnAddSubCatF.Value = "N"

                ''Populate Control
                fnPopulateCtrl()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Validity Year --> 0 - 20
            fnPopulateNumberInDDL(ddlValidYear, "50")

            ''Get Expiring Notification --> 0 - 6
            fnPopulateNumberInDDL(ddlExpNotiB4, "6")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        If hdnCallFrm.Value = "Add" Then  '' Called From Add Screen
            Dim strJavaScript As String
            strJavaScript = "<script language = 'Javascript'>" & _
                            "window.close();" & _
                            "</script>"
            Response.Write(strJavaScript)
        Else    ''Called From Modify Screen
            hdnAddSubCatF.Value = "Y"
        End If
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        txtSubCatCode.Text = ""
        txtSubCatName.Text = ""
        cbAddInfo1.Checked = False
        txtAddInfo1.Text = ""
        cbAddInfo2.Checked = False
        txtAddInfo2.Text = ""
        cbAddInfo3.Checked = False
        txtAddInfo3.Text = ""
        cbAddInfo4.Checked = False
        txtAddInfo4.Text = ""
        cbAddInfo5.Checked = False
        txtAddInfo5.Text = ""
        ddlValidYear.SelectedValue = "0"
        txtFloatLevel.Text = ""
        ddlExpNotiB4.SelectedValue = "0"
        cbACTag.Checked = False
    End Sub

    Protected Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim strNewSCatID As String = ""
            Dim strNewSCatCode As String
            Dim strNewSCatName As String
            Dim strNewAddInfo1 As String = ""
            Dim strNewAddInfo2 As String = ""
            Dim strNewAddInfo3 As String = ""
            Dim strNewAddInfo4 As String = ""
            Dim strNewAddInfo5 As String = ""
            Dim strNewAddInfoM1 As String = ""
            Dim strNewAddInfoM2 As String = ""
            Dim strNewAddInfoM3 As String = ""
            Dim strNewAddInfoM4 As String = ""
            Dim strNewAddInfoM5 As String = ""
            Dim strUploadFileF As String = "N"
            Dim strUploadFileName As String = ""

            Dim strGotError As String = "N"
            Dim strErrorMsg As String = ""

            ''Get New Records Value
            strNewSCatCode = Trim(txtSubCatCode.Text)
            strNewSCatName = Trim(txtSubCatName.Text)

            ''** Images format checking
            If Not FUploadImage.PostedFile Is Nothing And FUploadImage.PostedFile.ContentLength > 0 Then '' check whether user upload file
                If System.IO.Path.GetExtension(FUploadImage.PostedFile.FileName).ToUpper <> ".JPG" Then
                    lblErrorMessage.Text = "Please attach a image with an acceptable file format for uploading. Acceptable file formats are JPG."
                    lblErrorMessage.Visible = True
                    Exit Sub
                Else
                    '' Get File Name
                    strUploadFileName = System.IO.Path.GetFileName(FUploadImage.PostedFile.FileName)

                    '' Indicate have new images want to upload
                    strUploadFileF = "Y"
                End If
            End If

            ''** Get New Additional Information to display
            fnGetSubCatAddInfo( _
                Trim(txtAddInfo1.Text), Trim(txtAddInfo2.Text), Trim(txtAddInfo3.Text), Trim(txtAddInfo4.Text), Trim(txtAddInfo5.Text), _
                IIf(cbAddInfo1.Checked, "Y", "N"), IIf(cbAddInfo2.Checked, "Y", "N"), IIf(cbAddInfo3.Checked, "Y", "N"), IIf(cbAddInfo4.Checked, "Y", "N"), IIf(cbAddInfo5.Checked, "Y", "N"), _
                strNewAddInfo1, strNewAddInfo2, strNewAddInfo3, strNewAddInfo4, strNewAddInfo5, _
                strNewAddInfoM1, strNewAddInfoM2, strNewAddInfoM3, strNewAddInfoM4, strNewAddInfoM5, _
                "")

            ''Validate Sub Category Name
            fnValidateSubCategory(strNewSCatName, strGotError, strErrorMsg)

            If strGotError = "N" Then '' not Duplicate found --> add new record in datatable
                If hdnCallFrm.Value = "Add" Then     '' For Called From ADD screen
                    ''Add New Sub Category in database
                    Dim intRetVal As Integer
                    Dim strMsg As String
                    intRetVal = clsCategory.fnCategorySub_AddTempSubCategory( _
                                    strNewSCatCode, strNewSCatName, _
                                    strNewAddInfo1, strNewAddInfoM1, _
                                    strNewAddInfo2, strNewAddInfoM2, _
                                    strNewAddInfo3, strNewAddInfoM3, _
                                    strNewAddInfo4, strNewAddInfoM4, _
                                    strNewAddInfo5, strNewAddInfoM5, _
                                    ddlValidYear.SelectedValue, txtFloatLevel.Text, _
                                    ddlExpNotiB4.SelectedValue, Session("UsrID"), _
                                    IIf(cbACTag.Checked, "Y", "N"), strUploadFileName, strNewSCatID)
                    If intRetVal > 0 Then
                        hdnAddSubCatF.Value = "Y"
                        strMsg = "Sub Category Added Successfully."

                        '' ** Save Images in system
                        If strUploadFileF = "Y" Then
                            Dim SaveLocation As String = ""
                            SaveLocation = Server.MapPath(gCategoryPath & strNewSCatID & "_" & strUploadFileName)
                            FUploadImage.PostedFile.SaveAs(SaveLocation)
                        End If

                        ''Message Notification
                        Dim strJavaScript As String
                        strJavaScript = "<script language = 'Javascript'>" & _
                                        "alert('" + strMsg + "');" & _
                                        "</script>"
                        Response.Write(strJavaScript)
                    Else
                        strMsg = "Sub Category Add Failed. Please contact your Administrator."
                        lblErrorMessage.Text = strMsg
                        lblErrorMessage.Visible = True
                    End If



                Else      '' For Called From EDIT screen
                    ''Add New Sub Category in database
                    Dim intRetVal As Integer
                    Dim strMsg As String
                    intRetVal = clsCategory.fnCategoryAddSubCategoryForModify( _
                                    hdnCatID.Value, IIf(hdnSCatID.Value = "", "0", hdnSCatID.Value), hdnDeleteF.Value, _
                                    strNewSCatCode, strNewSCatName, _
                                    strNewAddInfo1, strNewAddInfoM1, _
                                    strNewAddInfo2, strNewAddInfoM2, _
                                    strNewAddInfo3, strNewAddInfoM3, _
                                    strNewAddInfo4, strNewAddInfoM4, _
                                    strNewAddInfo5, strNewAddInfoM5, _
                                    ddlValidYear.SelectedValue, txtFloatLevel.Text, _
                                    ddlExpNotiB4.SelectedValue, Session("UsrID"), _
                                    IIf(cbACTag.Checked, "Y", "N"), strUploadFileName, strNewSCatID)
                    If intRetVal > 0 Then
                        hdnAddSubCatF.Value = "Y"
                        strMsg = "Sub Category Added Successfully."

                        ''Insert Audit Trail
                        clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Sub Category Added : " + strNewSCatName + IIf(strNewSCatCode = "", "", " (" + strNewSCatCode + ")"))

                        '' ** Save Images in system
                        If strUploadFileF = "Y" Then
                            Dim SaveLocation As String = ""
                            SaveLocation = Server.MapPath(gCategoryPath & strNewSCatID & "_" & strUploadFileName)
                            FUploadImage.PostedFile.SaveAs(SaveLocation)
                        End If

                        ''Message Notification
                        Dim strJavaScript As String
                        strJavaScript = "<script language = 'Javascript'>" & _
                                        "alert('" + strMsg + "');" & _
                                        "</script>"
                        Response.Write(strJavaScript)
                    Else
                        strMsg = "Sub Category Add Failed. Please contact your Administrator."
                        lblErrorMessage.Text = strMsg
                        lblErrorMessage.Visible = True
                    End If

                End If
            Else   '' Error Found --> show error message
                lblErrorMessage.Text = strErrorMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnValidateSubCategory( _
                            ByVal strNewSCatName As String, _
                            ByRef strGotError As String, _
                            ByRef strErrorMsg As String)
        Try
            ''Check whether Sub Category Name already exist in database (just for Called from EDIT screen)
            If strGotError = "N" And hdnCallFrm.Value = "Edit" Then
                If clsCategory.fnCategoryCheckDuplicate(hdnCatID.Value, 0, "", strNewSCatName, "2", "I") Then 'Duplicate Sub Category Name
                    strGotError = "Y"
                    strErrorMsg = "Sub Category Name already exist. Please choose another Sub Category Name."
                End If
            End If


            ''Check whether Sub Status Name already exist in Current Datatable (just for Called from ADD screen)
            If strGotError = "N" And hdnCallFrm.Value = "Add" Then
                If clsCategory.fnCategorySub_CheckDuplicateTempSub(strNewSCatName, Session("UsrID")) Then 'Duplicate Sub Category Name
                    strGotError = "Y"
                    strErrorMsg = "Sub Category Name already exist. Please choose another Sub Category Name."
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class