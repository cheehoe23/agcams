#Region "Information Section"
' ****************************************************************************************************
' Description       : Add New Category 
' Purpose           : Add New Category Information
' Author            : See Siew
' Date              : 23/10/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class category_add
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")
                butAddSubCat.Attributes.Add("OnClick", "return mfnOpenAddSubStatusWindow()")

                ''set default
                hdnSubCatID.Value = 0 ''Total Subcategory reset to '0'
                'hdnAddSubCatF.Value = "N"

                ''Delete Temporary Sub Category
                fnClearTempCategoryFile(Server.MapPath(gCategoryPath), Session("UsrID"))
            End If

            ''Populate Temporary Sub Category
            fnGetRecordsFromDB()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnGetRecordsFromDB() As Boolean
        Try
            ''Get Subcategory Record
            dgSubCategory.DataSource = clsCategory.fnCategorySub_GetTempRec(Session("UsrID"))
            dgSubCategory.DataBind()
            hdnSubCatID.Value = dgSubCategory.Items.Count
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Server.Transfer("category_view.aspx")
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            'fnCreateDataTable()
            'txtCatCode.Text = ""
            'txtCatName.Text = ""
            'hdnSubCatID.Value = 0
            'BindData()

            ' ''Delete Temporary Sub Category
            'fnClearTempCategoryFile(Server.MapPath(gCategoryPath), Session("UsrID"))

            Server.Transfer("category_add.aspx")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub dgSubCategory_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSubCategory.DeleteCommand
        Try
            Dim strSCatIDDel As String = e.Item.Cells(0).Text

            Dim intRetVal As Integer
            Dim strMsg As String
            intRetVal = clsCategory.fnCategorySub_DeleteParticularTempRec(strSCatIDDel)
            If intRetVal > 0 Then
                ''Populate Temporary Sub Category
                fnGetRecordsFromDB()
            Else
                strMsg = "Subcategory have been delete failed. Please contact your Administrator."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim strCatCode As String = ""
            Dim strCatName As String = ""
            Dim strSubCatXML As String = ""

            Dim blnDuplicate As Boolean = False
            Dim intRetVal As Integer
            Dim strMsg As String

            ''** Get fields value
            strCatCode = Trim(txtCatCode.Text)
            strCatName = Trim(txtCatName.Text)

            ''** Check duplicate category
            blnDuplicate = clsCategory.fnCategoryCheckDuplicate(0, 0, strCatCode, "", "1", "I")

            If Not blnDuplicate Then ''Not Error
                ''Add Category and Sub Category to datatbase
                intRetVal = clsCategory.fnCategoryAddNewCategory( _
                                strCatCode, strCatName, strSubCatXML, Session("UsrID"))
                If intRetVal > 0 Then
                    strMsg = "New Category added successfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "New Category created : " + strCatName + " (" + strCatCode + ")")

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "document.location.href='category_View.aspx';" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    strMsg = "New Category Add Failed. Please contact your administrator."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If

            Else ''Got Error
                lblErrorMessage.Text = "Category Code already exist. Please choose another Category Code."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnGetAddInfo(ByVal strAddInfo1 As String, ByVal strAddInfoM1 As String, _
                                 ByVal strAddInfo2 As String, ByVal strAddInfoM2 As String, _
                                 ByVal strAddInfo3 As String, ByVal strAddInfoM3 As String, _
                                 ByVal strAddInfo4 As String, ByVal strAddInfoM4 As String, _
                                 ByVal strAddInfo5 As String, ByVal strAddInfoM5 As String) As String
        Dim strRetVal As String = ""
        Dim strNewAddInfo As String = ""
        Dim strNewAddInfo1 As String = ""
        Dim strNewAddInfo2 As String = ""
        Dim strNewAddInfo3 As String = ""
        Dim strNewAddInfo4 As String = ""
        Dim strNewAddInfo5 As String = ""
        Dim strNewAddInfoM1 As String = ""
        Dim strNewAddInfoM2 As String = ""
        Dim strNewAddInfoM3 As String = ""
        Dim strNewAddInfoM4 As String = ""
        Dim strNewAddInfoM5 As String = ""

        ''** Get New Additional Information to display
        fnGetSubCatAddInfo( _
            strAddInfo1, strAddInfo2, strAddInfo3, strAddInfo4, strAddInfo5, _
            strAddInfoM1, strAddInfoM2, strAddInfoM3, strAddInfoM4, strAddInfoM5, _
            strNewAddInfo1, strNewAddInfo2, strNewAddInfo3, strNewAddInfo4, strNewAddInfo5, _
            strNewAddInfoM1, strNewAddInfoM2, strNewAddInfoM3, strNewAddInfoM4, strNewAddInfoM5, _
            strNewAddInfo)

        strRetVal = IIf(strNewAddInfo = "", " - ", strNewAddInfo)
        Return (strRetVal)
    End Function

    Public Function fnGetFileName(ByVal strCatSubID As String, ByVal strFileNameSave As String) As String
        Try
            Dim strRetVal As String = ""

            If Trim(strFileNameSave) <> "" Then
                strRetVal = "<a id=hlFile href='" & gCategoryPath & strCatSubID & "_" & strFileNameSave & "' target=CatEditFile >" & _
                             "<img id=imgView src=../images/audit.gif alt='Click for view image.' border=0 >" & _
                             "</a>"
            Else
                strRetVal = "-"
            End If

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function
End Class