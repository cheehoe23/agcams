#Region "Information Section"
' ****************************************************************************************************
' Description       : Edit Location 
' Purpose           : Edit Location Information
' Author            : See Siew
' Date              : 01/03/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class location_Edit
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")
                butAddSubLoc.Attributes.Add("OnClick", "return mfnOpenAddSubLocWindow()")
                Me.butDelete.Attributes.Add("OnClick", "return mfnDeleteAll()")

                ''Get Location ID 
                hdnLoctID.Value = Request("LoctID")

                ''Set Default 
                hdnAddSubLocF.Value = "N"
                hdnDeleteSubFlag.Value = "N"

                ''Retrieve Records 
                fnGetRecordsFromDB()

                ''Checking For User Access
                fnCheckForUserAccess()
            End If

            ''New Sub Location have been added
            If hdnAddSubLocF.Value = "Y" Then
                hdnAddSubLocF.Value = "N"
                hdnLoctID.Value = Request("LoctID")
                fnGetRecordsFromDB()
            End If

            ''Delete Sub Location
            If hdnDeleteSubFlag.Value = "Y" Then
                fnDeleteSubLocation()

                hdnDeleteSubFlag.Value = "N"
                hdnSLocID.Value = ""
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnGetRecordsFromDB()
        Dim objRdr As SqlDataReader
        Try
            objRdr = clsLocation.fnLocationGetRecForEdit(hdnLoctID.Value)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()
                    txtLocationCode.Text = CStr(objRdr("fld_LocationCode"))
                    txtLocationName.Text = CStr(objRdr("fld_LocationName"))

                    hdnTotRec.Value = CStr(objRdr("totRec"))
                    hdnLocUseF.Value = CStr(objRdr("LocUseF"))
                End If
            End If
            objRdr.Close()

            ''Get Sub Location Record
            dgSubLoc.DataSource = clsLocation.fnLocationGetAllRec(hdnLoctID.Value, "0", "fld_LocSubID", "ASC")
            dgSubLoc.DataBind()
            If Not dgSubLoc.Items.Count > 0 Then ''Not Records found
                lblErrorMessage.Text = "There is no Sub Location record found. Please contact your administrator."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Public Sub fnCheckForUserAccess()
        ''Case 1: Access right
        If (InStr(Session("AR"), "SysSet|loct|add") = 0) Then butAddSubLoc.Visible = False

        If (InStr(Session("AR"), "SysSet|loct|delete") = 0) Then
            butDelete.Visible = False
            dgSubLoc.Columns(11).Visible = False
        End If

        If (InStr(Session("AR"), "SysSet|loct|edit") = 0) Then
            txtLocationCode.Enabled = False
            txtLocationName.Enabled = False
            dgSubLoc.Columns(10).Visible = False

            butSubmit.Enabled = False
            butReset.Enabled = False
        End If

        ''Case 2: check whether allow to delete whole record (DELETE button) 
        If hdnLocUseF.Value = "Y" Then
            butDelete.Visible = False
        End If
    End Sub

    Private Sub ButCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButCancel.Click
        Response.Redirect("location_View.aspx")
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        lblErrorMessage.Visible = False
        lblErrorMessage.Text = ""
        fnGetRecordsFromDB()
    End Sub

    Private Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim strLocationCode, strLocationName As String
            Dim bDupLocation As Boolean = False
            Dim intRetVal As Integer
            Dim strMsg As String

            ''** get Fields Value
            strLocationCode = Trim(txtLocationCode.Text)
            strLocationName = Trim(txtLocationName.Text)

            ''** Check for Duplicate Location Name
            bDupLocation = clsLocation.fnLocationCheckDuplicate(hdnLoctID.Value, strLocationCode, "E")

            If Not bDupLocation Then ''not duplicate record
                ''update record
                'intRetVal = clsLocation.fnLocationInsertUpdateDelete( _
                '                    hdnLoctID.Value, strLocationCode, strLocationName, Session("UsrID"), "E")
                intRetVal = clsLocation.fnLocation_EditLocation( _
                                hdnLoctID.Value, "0", strLocationCode, strLocationName, "", "", "", _
                                Session("UsrID"), "L", "", "")

                If intRetVal > 0 Then
                    strMsg = "Location updated successfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Location Updated : " + strLocationName + " (" + strLocationCode + ")")

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "document.location.href='location_View.aspx';" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    strMsg = "Location update failed."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If

            Else  ''duplicate record
                lblErrorMessage.Text = "Location Code already exist. Please choose another Location Code."
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnShowDeleteBut( _
                            ByVal strSLocID As String, _
                            ByVal strSLocUseF As String) As String

        ''Just allow to delete Sub Location, which have following condition:-
        ''--> 1. Not assign to any asset 
        Dim strRetVal As String = ""
        If (InStr(Session("AR"), "SysSet|loct|delete") > 0) Then  '' Check For Delete Sub Location Access Right
            If strSLocUseF = "N" Then
                strRetVal = "<a id=hlDelRemark href='javascript:mfnDeleteSubLoc(" + strSLocID + ")'>" & _
                            "<img id=imgUpdate src=../images/delete.gif alt='Click for Delete Sub Location.' border=0 >" & _
                            "</a>"
            End If
        End If

        Return (strRetVal)
    End Function

    Public Sub fnDeleteSubLocation()
        Try
            Dim intRetVal As Integer
            Dim strMsg As String
            intRetVal = clsLocation.fnLocation_DeleteLocation("", hdnSLocID.Value, Session("UsrID"), "S")
            If intRetVal > 0 Then
                strMsg = "Sub Location have been delete Successfully "

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Sub Location deleted for Location : " + txtLocationName.Text + " (" + txtLocationCode.Text + ")")

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "</script>"
                Response.Write(strJavaScript)

                ''Populate again record
                fnGetRecordsFromDB()
            Else
                strMsg = "Sub Location have been delete Failed. Please contact your Administrator."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        Try
            Dim intRetVal As Integer
            Dim strMsg As String
            intRetVal = clsLocation.fnLocation_DeleteLocation(hdnLoctID.Value, "", Session("UsrID"), "A")
            If intRetVal > 0 Then
                strMsg = "Location have been delete successfully "

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Location Deleted : " + txtLocationName.Text + " (" + txtLocationCode.Text + ")")

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "document.location.href='Location_View.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "Location have been delete failed. Please contact your administrator."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class
