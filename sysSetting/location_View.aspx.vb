#Region "Information Section"
' ****************************************************************************************************
' Description       : View Location
' Purpose           : View/Delete Location
' Author            : See Siew
' Date              : 01/03/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
#End Region

Partial Class location_View
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butDelLoct.Attributes.Add("OnClick", "return ConfirmDelete()")
                Me.butPrintRFIDLabel.Attributes.Add("OnClick", "return fnCheckSelect('cdCheckS')")

                ''Checking For Assign Right
                If (InStr(Session("AR"), "SysSet|loct|add") = 0) Then butAddLoct.Visible = False
                If (InStr(Session("AR"), "SysSet|loct|delete") = 0) Then butDelLoct.Visible = False
                If (InStr(Session("AR"), "SysSet|loct|print") = 0) Then
                    butPrintRFIDLabel.Visible = False
                    dgLocation.Columns(17).Visible = False
                End If

                ''default Sorting
                hdnSortName.Value = "fld_LocationCode"
                hdnSortAD.Value = "ASC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Checking whether user not have access right to delete Location
            If (InStr(Session("AR"), "SysSet|loct|delete") = 0) Then dgLocation.Columns(16).Visible = False

            '' Sort Header Display
            fnSortHeaderDisplay()

            ''Get Location Records
            dgLocation.DataSource = clsLocation.fnLocationGetAllRec("0", "0", hdnSortName.Value, hdnSortAD.Value)
            dgLocation.DataBind()
            If Not dgLocation.Items.Count > 0 Then ''Not Location Records found
                'dgLocation.Visible = False
                ddlPageSize.Enabled = False
                butDelLoct.Visible = False
                lblErrorMessage.Text = "There is no location record."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By fld_LocationCode(4), fld_LocationName (5)
        Dim intSortIndex As Integer
        Dim strSortHeader As String = ""

        ''Set Default Header
        dgLocation.Columns(4).HeaderText = "Location Code"
        dgLocation.Columns(5).HeaderText = "Location Name"
        dgLocation.Columns(7).HeaderText = "Sub Location Code"
        dgLocation.Columns(8).HeaderText = "Sub Location Name"
        dgLocation.Columns(10).HeaderText = "Smart Shelf Rack"
        dgLocation.Columns(11).HeaderText = "Light IP Address"
        dgLocation.Columns(12).HeaderText = "Light Type"
        dgLocation.Columns(13).HeaderText = "Door Gantry"
        dgLocation.Columns(14).HeaderText = "Movement Status"

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fld_LocationCode"
                intSortIndex = 4
                strSortHeader = "Location Code"
            Case "fld_LocationName"
                intSortIndex = 5
                strSortHeader = "Location Name"
            Case "fld_LocSubCode"
                intSortIndex = 7
                strSortHeader = "Sub Location Code"
            Case "fld_LocSubName"
                intSortIndex = 8
                strSortHeader = "Sub Location Name"
            Case "fld_Activate"
                intSortIndex = 10
                strSortHeader = "Smart Shelf Rack"
            Case "fld_LightIPAdd"
                intSortIndex = 11
                strSortHeader = "Light IP Address"
            Case "fld_LightTypeStr"
                intSortIndex = 12
                strSortHeader = "Light Type"
            Case "fld_DGF"
                intSortIndex = 13
                strSortHeader = "Door Gantry"
            Case "fld_DGMvStatus"
                intSortIndex = 14
                strSortHeader = "Movement Status"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgLocation.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgLocation.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Function fnShowEditHyperlink(ByVal strloctID As String, _
                                 ByVal strEditable As String) As String
        Dim strRetVal As String = ""

        '' Check For Edit Location Access Right and Editable = "Y"
        If (InStr(Session("AR"), "SysSet|loct|edit") > 0) Then ' And strEditable = "Y" Then
            strRetVal += "<a id=hlEditloct href='location_Edit.aspx?loctID=" + strloctID + "' target=_self >" & _
                        "<img id=imgUpdate src=../images/update.gif alt='Click for Update Location.' border=0 >" & _
                        "</a>"
        End If

        Return (strRetVal)
    End Function

    Private Sub dgLocation_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgLocation.ItemDataBound
        Dim cbDelete As CheckBox
        cbDelete = CType(e.Item.FindControl("DeleteThis"), CheckBox)

        '' Check For Delete Department Access Right
        If (InStr(Session("AR"), "SysSet|loct|delete") > 0) Then
            If Not cbDelete Is Nothing Then
                '' If Location assign to Latest Asset Movement,
                '' delete check box invisible
                If e.Item.Cells(2).Text = "Y" Then
                    cbDelete.Enabled = False
                    cbDelete.Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub dgLocation_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgLocation.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

    Private Sub dgLocation_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgLocation.PageIndexChanged
        dgLocation.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgLocation.CurrentPageIndex = 0
        dgLocation.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Private Sub butAddLoct_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAddLoct.Click
        Response.Redirect("location_add.aspx")
    End Sub

    Private Sub butDelLoct_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelLoct.Click
        Try
            Dim strMsg As String
            Dim retval As Integer
            Dim strDelLocationName As String = ""

            ''*** Start: Get Location that selected for deleted
            Dim GridItem As DataGridItem
            Dim chkSelectedDel As CheckBox
            Dim strLoctIDForDel As String = ""
            For Each GridItem In dgLocation.Items
                chkSelectedDel = CType(GridItem.Cells(16).FindControl("DeleteThis"), CheckBox)
                If chkSelectedDel.Checked Then
                    strLoctIDForDel += GridItem.Cells(1).Text & "^"
                    strDelLocationName += GridItem.Cells(6).Text & " (" & Trim(GridItem.Cells(4).Text) & ") - " & GridItem.Cells(9).Text & " (" & Trim(GridItem.Cells(7).Text) & "),"
                End If
            Next
            If strDelLocationName <> "" Then strDelLocationName = strDelLocationName.Substring(0, strDelLocationName.Length - 1)
            ''*** End  : Get Location that selected for deleted

            ''Start: Delete selected records from database
            'retval = clsLocation.fnLocationInsertUpdateDelete(strLoctIDForDel, "", "", Session("UsrID"), "D")
            retval = clsLocation.fnLocation_DeleteLocation("", strLoctIDForDel, Session("UsrID"), "V")
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 Then
                strMsg = "Selected record(s) deleted successfully."
                fnPopulateRecords()

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Delete Location(s) : " + strDelLocationName)

                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>alert('" + strMsg + "');</script>"
                Response.Write(strJavaScript)
            Else
                lblErrorMessage.Text = "Selected record(s) deleted failed."
                lblErrorMessage.Visible = True
            End If
            ''End  : Message Notification

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butPrintRFIDLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPrintRFIDLabel.Click
        Try
            ''*** Start: Get Location that selected for deleted
            Dim GridItem As DataGridItem
            Dim chkSelectedDel As CheckBox
            Dim strLoctID As String = ""
            For Each GridItem In dgLocation.Items
                chkSelectedDel = CType(GridItem.Cells(17).FindControl("cdCheckS"), CheckBox)
                If chkSelectedDel.Checked Then
                    strLoctID += GridItem.Cells(1).Text & "^"
                End If
            Next
            ''*** End  : Get Location that selected for deleted

            If Trim(strLoctID) <> "" Then

                Session("LoctID" + Session("UsrID").ToString()) = strLoctID
                Session("ModuleName" + Session("UsrID").ToString()) = "AMS"
                Session("Asset" + Session("UsrID").ToString()) = ""
                Session("AddAsset" + Session("UsrID").ToString()) = ""
                Session("Location" + Session("UsrID").ToString()) = "1"

                ClientScript.RegisterStartupScript(Me.[GetType](), "myalert", "window.open('../common/PrintPopUp.aspx','_blank','left=350, top=150, width=400, height=200, menubar=no, resizable=no, status=no, titlebar=no, toolbar=no');", True)

                ''******** ignore --> for save RFID label in table to print at database side...
                '' ''Start: print selected records from database
                ''Dim retval As Integer
                ''retval = clsLocation.fnLocation_PrintLocMarker(strLoctID, gRFIDPrintBatch, Session("UsrID"))
                '' ''End  : print selected records from database

                '' ''Start: Message Notification
                ''If retval > 0 Then
                ''    ''Insert Audit Trail
                ''    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Location Marker printed.")

                ''    Dim strJavaScript As String
                ''    strJavaScript = "<script language = 'Javascript'>alert('" + "Selected record(s) print successfully." + "');</script>"
                ''    Response.Write(strJavaScript)
                ''Else
                ''    lblErrorMessage.Text = "Selected record(s) print failed."
                ''    lblErrorMessage.Visible = True
                ''End If
                '' ''End  : Message Notification

                '*** Create ZPL file
                'Dim strFileNameLabel As String = ""
                ''clsRFIDLabel.fnCreateRFIDLabelFile4Loc(strLoctID, Session("UsrID"), _
                ''                                       Server.MapPath(gAMSRFIDLabelPath), strFileNameLabel)

                ' ''Insert Audit Trail
                'clsCommon.fnAuditInsertRec(Session("UsrID"), strLoctID, "RFID passive label printed.")

                'Dim strJavaScript As String
                'strJavaScript = "<script language = 'Javascript'>" & _
                '                "alert('" + "RFID Tag print succesfully." + "');" & _
                '                "</script>"
                'Response.Write(strJavaScript)

                '*** send to Printing
                'fnPrintRFIDLabelFile(strFileNameLabel)
            Else
                lblErrorMessage.Text = "Selected record(s) print failed. Please try again."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPrintRFIDLabelFile(ByVal strFileNameLabel As String)
        Try
            Dim intCount As Integer = "0"
            Dim strFileFullPath As String = ""

            If gRFIDLabelPrintF = "Y" Then
                ''Get File Full path
                strFileFullPath = Server.MapPath(gRFIDLabelPath) & strFileNameLabel

                ''*** Check whether file exist
                If File.Exists(strFileFullPath) Then
                    '' ''*** Printing RFID Label
                    ''Dim objScriptShell
                    ''objScriptShell = Server.CreateObject("Wscript.Shell")
                    ' ''objScriptShell.Run("copy " & strFileFullPath & " com1")
                    ''objScriptShell.Run("copy " & strFileFullPath & " " & gRFIDPrinterIP)

                    ' ''objScriptShell.Run("ftp " & gRFIDPrinterIP)
                    ' ''objScriptShell.Run("send " & strFileFullPath)
                    ''objScriptShell = Nothing

                    '' ''*** Delete File
                    ' ''File.Delete(strFileFullPath)

                    ''*** Call client side EXE to write tag
                    'Dim startInfo As System.Diagnostics.ProcessStartInfo
                    'Dim p As New Process()
                    'startInfo = New System.Diagnostics.ProcessStartInfo(gRFIDPrintBatch)
                    'p.StartInfo = startInfo
                    'p.Start()
                    'p.WaitForExit()
                    'p.Dispose()

                    Dim myProcess As New Process
                    myProcess.StartInfo.FileName = gRFIDPrintBatch
                    myProcess.Start()
                    myProcess.WaitForExit() 'wait for the scan to complete
                    myProcess.Dispose()
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
