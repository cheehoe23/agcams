#Region "Information Section"
' ****************************************************************************************************
' Description       : Add New Sub Location 
' Purpose           : Add New Sub Location Information
' Author            : See Siew
' Date              : 12/12/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class location_AddSub
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")

                ''Get Pass Value from Main screen
                hdnCallFrm.Value = Request("CallFrm")   ''Called from "Add" screen OR "Edit" screen
                hdnLocID.Value = Request("LocID")       ''Just for Called From "Edit" screen
                hdnSLocID.Value = Request("SLocID")     ''Just for Called From "Edit" screen -->  Sub Location ID that want to delete
                hdnDeleteF.Value = Request("DelF")      ''Just for Called From "Edit" screen  --> "Y"=Delete Sub Location
                lblLocCode.Text = Request("LC")
                lblLocName.Text = Request("LN")

                ''Populate Light Type
                fnPopulateSmartShelfLightType(ddlLightType)

                ''set Add Sub Location Flag to "N"
                hdnAddSubLocF.Value = "N"
                rdActivate_SelectedIndexChanged(Nothing, Nothing)
                rdDoorGantryF_SelectedIndexChanged(Nothing, Nothing)
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        If hdnCallFrm.Value = "Add" Then  '' Called From Add Screen
            Dim strJavaScript As String
            strJavaScript = "<script language = 'Javascript'>" & _
                            "window.close();" & _
                            "</script>"
            Response.Write(strJavaScript)
        Else    ''Called From Modify Screen
            hdnAddSubLocF.Value = "Y"
        End If
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        txtSubLocCode.Text = ""
        txtSubLocName.Text = ""
        rdActivate.SelectedValue = "N"
        rdActivate_SelectedIndexChanged(Nothing, Nothing)
        rdDoorGantryF.SelectedValue = "N"
        rdDoorGantryF_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim strNewSLocCode As String
            Dim strNewSLocName As String

            Dim strGotError As String = "N"
            Dim strErrorMsg As String = ""

            ''Get New Records Value
            strNewSLocCode = Trim(txtSubLocCode.Text)
            strNewSLocName = Trim(txtSubLocName.Text)

            Dim dt As DataTable = clsLocation.fnLocationSub_GetByfld_LocSubCode(txtSubLocCode.Text.ToString().Trim()).Tables(0)
            If (dt.Rows.Count > 0) Then
                lblErrorMessage.Text = "Duplicate Sub Location Code in the Asset Management System! Please Try Again!"
                lblErrorMessage.Visible = True
                hdnDuplicateStatus.Value = "1"
                txtSubLocCode.Focus()
            Else
                lblErrorMessage.Text = ""
                lblErrorMessage.Visible = False
                hdnDuplicateStatus.Value = "0"
            End If

            ''Validate Sub Location Code
            fnValidateSubLoc(strNewSLocCode, strGotError, strErrorMsg)

            If strGotError = "N" Then '' not Duplicate found --> add new record in datatable
                If hdnCallFrm.Value = "Add" Then     '' For Called From ADD screen
                    hdnAddSubLocF.Value = "Y"

                Else      '' For Called From EDIT screen
                    ''Add New Sub Location in database
                    Dim intRetVal As Integer
                    intRetVal = clsLocation.fnLocation_AddSubLocForModify( _
                                    hdnLocID.Value, IIf(hdnSLocID.Value = "", "0", hdnSLocID.Value), hdnDeleteF.Value, _
                                    strNewSLocCode, strNewSLocName, rdActivate.SelectedValue, _
                                    IIf(rdActivate.SelectedValue = "Y", Trim(txtLightIP.Text), ""), _
                                    IIf(rdActivate.SelectedValue = "Y", ddlLightType.SelectedValue, "0"), _
                                    Session("UsrID"), _
                                    rdDoorGantryF.SelectedValue, _
                                    IIf(rdDoorGantryF.SelectedValue = "Y", Trim(txtDGMvStatus.Text), ""))
                    If intRetVal > 0 Then
                        hdnAddSubLocF.Value = "Y"

                        ''Insert Audit Trail
                        clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Sub Location Added : " + strNewSLocName + " (" + strNewSLocCode + ")")

                        ''Message Notification
                        Dim strJavaScript As String
                        strJavaScript = "<script language = 'Javascript'>" & _
                                        "alert('" + "Sub Location Added Successfully." + "');" & _
                                        "</script>"
                        Response.Write(strJavaScript)
                    Else
                        lblErrorMessage.Text = "Sub Location Add Failed. Please contact your Administrator."
                        lblErrorMessage.Visible = True
                    End If

                End If
            Else   '' Error Found --> show error message
                lblErrorMessage.Text = strErrorMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnValidateSubLoc( _
                            ByVal strNewSLocCode As String, _
                            ByRef strGotError As String, _
                            ByRef strErrorMsg As String)
        Try
            ''Check whether Sub Location Code already exist in database (just for Called from EDIT screen)
            If strGotError = "N" And hdnCallFrm.Value = "Edit" Then
                If clsLocation.fnLocation_CheckDuplicate4Sub(hdnLocID.Value, 0, strNewSLocCode, "I") Then 'Duplicate Sub Location Code
                    strGotError = "Y"
                    strErrorMsg = "Sub Location Code already exist. Please choose another Sub Location Code."
                End If
            End If


            ''Check whether Sub Location Code already exist in Current Datatable (just for Called from ADD screen)
            If strGotError = "N" And hdnCallFrm.Value = "Add" Then
                Dim TblSLoc As DataTable = CType(Session("dtSubLoc"), DataTable)
                Dim RowSubLoc As DataRow
                For Each RowSubLoc In TblSLoc.Rows
                    If Trim(CStr(RowSubLoc("dt_SLocCode")).ToUpper) = Trim(strNewSLocCode.ToUpper) Then
                        strGotError = "Y"
                        strErrorMsg = "Sub Location Code already exist. Please choose another Sub Location Code."
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub rdActivate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdActivate.SelectedIndexChanged
        Try
            If rdActivate.SelectedValue = "Y" Then
                divLightIP.Visible = True
            Else
                divLightIP.Visible = False
            End If

            txtLightIP.Text = ""
            ddlLightType.SelectedValue = "0"
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub rdDoorGantryF_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdDoorGantryF.SelectedIndexChanged
        Try
            If rdDoorGantryF.SelectedValue = "Y" Then
                divGoorGantry.Visible = True
            Else
                divGoorGantry.Visible = False
            End If

            txtDGMvStatus.Text = ""
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub txtSubLocCode_TextChanged(sender As Object, e As EventArgs) Handles txtSubLocCode.TextChanged
        Dim dt As DataTable = clsLocation.fnLocationSub_GetByfld_LocSubCode(txtSubLocCode.Text.ToString().Trim()).Tables(0)
        If (dt.Rows.Count > 0) Then
            lblErrorMessage.Text = "Duplicate Sub Location Code in the Asset Management System! Please Try Again!"
            lblErrorMessage.Visible = True
            hdnDuplicateStatus.Value = "1"
            txtSubLocCode.Focus()
        Else
            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False
            hdnDuplicateStatus.Value = "0"
        End If
    End Sub
End Class