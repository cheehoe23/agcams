#Region "Information Section"
' ****************************************************************************************************
' Description       : Create New File 
' Purpose           : Create New File Information
' Author            : See Siew
' Date              : 26/03/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Web.Services

#End Region

Partial Class locationOrOfficer_Add
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label
    Protected WithEvents ddlSubject As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlTopic As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlSTopic As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlS2Topic As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            '''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("/common/logout.aspx", False)
                Exit Sub
            End If
            '''********** End  : Check Session Time out ***********

            Me.Header.DataBind()

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                txtLocOfficerName.Focus()
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")

                fnCheckAccessRight()
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub fnCheckAccessRight()
        Try
            If (InStr(Session("AR"), "SysSet|addLoc|add") = 0) Then
                butSubmit.Enabled = False
                butReset.Enabled = False
            Else
                butSubmit.Enabled = True
                butReset.Enabled = True
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        txtLocOfficerName.Text = ""
        txtLocOfficerName.Focus()
       
    End Sub

    Private Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try

            Dim intRetVal As Integer
            Dim strMsg As String

            '''Check for Duplicate locationOfficerName
            Dim dt As DataTable = clsLocationOfficer.fnGetByfld_LocationOfficerName(txtLocOfficerName.Text.Trim().Replace("'", "''")).Tables(0)
            If (dt.Rows.Count > 0) Then
                lblErrorMessage.Text = "Duplicate Location/Officer Name in the File Management System! Please Try Again!"
                lblErrorMessage.Visible = True
                txtLocOfficerName.Focus()
                Return
            Else
                lblErrorMessage.Text = ""
                lblErrorMessage.Visible = False
            End If

            intRetVal = clsLocationOfficer.fnLocationOfficer_AddNewLocationOfficer(txtLocOfficerName.Text.Trim(), Session("UsrID"))


            If intRetVal > 0 Then
                strMsg = "New Location/Officer Add Successfully."
                txtLocOfficerName.Text = ""
                txtLocOfficerName.Focus()
                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "New LocationOfficer created : " + txtLocOfficerName.Text.Trim().Replace("'", "''"))

                'Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "</script>"
                Response.Write(strJavaScript)

                '        ''Reset the "Creation File"
                '        butReset_Click(Nothing, Nothing)
            Else
                strMsg = "New LocationOfficer Add Failed. Please contact your administrator."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If


        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("fld_ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class
