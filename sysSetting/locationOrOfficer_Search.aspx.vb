#Region "Information Section"
' ****************************************************************************************************
' Description       : Search File --> for Modify File, Archive File, Restore File, Delete File, KIV File 
' Purpose           : Search File Information
' Author            : See Siew
' Date              : 27/03/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Web.Services

#End Region

Partial Class locationOrOfficer_Search
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label
    Protected WithEvents hdnCurrentDate As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            '''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("/common/logout.aspx", False)
                Exit Sub
            End If
            '''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            Me.Header.DataBind()

            If Not Page.IsPostBack Then
                'Me.txtKeywordTitle.Attributes.Add("onkeypress", "return fnEnterProb()")
                Me.ButSearch.Attributes.Add("OnClick", "return chkFrm()")

                ''may call from --> Search, Modify, Archive, Restore, Delete, AddKIV
                hdnCallFrom.Value = Request("CallFrm")

                ''Get Header
                If hdnCallFrom.Value = "Search" Then
                    lblHeader.Text = "File Management : Search Loction/Officer"
                ElseIf hdnCallFrom.Value = "Delete" Then
                    lblHeader.Text = "File Management : Delete Loction/Officer"
                End If

                fnCheckAccessRight()
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        txtLocOfficerID.Text = ""
        txtLocOfficerName.Text = ""
        txtLocOfficerID.Focus()

    End Sub

    Protected Sub fnCheckAccessRight()
        Try

            If (InStr(Session("AR"), "SysSet|searchLoc|Search") = 0) Then
                ButSearch.Enabled = False
                butReset.Enabled = False
            Else
                ButSearch.Enabled = True
                butReset.Enabled = True
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ButSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButSearch.Click
        Try
            Dim strLocOfficerID, strLocOfficerName As String
            Dim dsFile As New DataSet
            Dim strURL As String

            ''Get Value 

            strLocOfficerID = txtLocOfficerID.Text.Trim()
            strLocOfficerName = txtLocOfficerName.Text.Trim()
            strLocOfficerName = strLocOfficerName.Replace("'", "''")
            strLocOfficerName = strLocOfficerName.Replace("[", "[[]")
            strLocOfficerName = strLocOfficerName.Replace("%", "[%]")
            strLocOfficerName = strLocOfficerName.Replace("_", "[_]")

            dsFile = clsLocationOfficer.fnGetDataByfld_LocationOfficerIDAndName(strLocOfficerID, strLocOfficerName, "fld_LocationOfficerBarcodeID", "ASC")

            If dsFile.Tables(0).Rows.Count > 0 Then

                If (hdnCallFrom.Value = "Search") Then
                    strURL = "/sysSetting/locationOrOfficer_SearchResult.aspx?CallFrm=" + hdnCallFrom.Value + "&strLocOfficerID=" + clsEncryptDecrypt.EncryptText(strLocOfficerID) + "&strLocOfficerName=" + clsEncryptDecrypt.EncryptText(strLocOfficerName)
                ElseIf (hdnCallFrom.Value = "Delete") Then
                    strURL = "/sysSetting/locationOrOfficer_SearchResult.aspx?CallFrm=" + hdnCallFrom.Value + "&strLocOfficerID=" + clsEncryptDecrypt.EncryptText(strLocOfficerID) + "&strLocOfficerName=" + clsEncryptDecrypt.EncryptText(strLocOfficerName)
                Else
                    strURL = "/sysSetting/locationOrOfficer_SearchResult.aspx?CallFrm=" + hdnCallFrom.Value + "&strLocOfficerID=" + clsEncryptDecrypt.EncryptText(strLocOfficerID) + "&strLocOfficerName=" + clsEncryptDecrypt.EncryptText(strLocOfficerName)
                End If

                clsCommon.fnAuditInsertRec(Session("UsrID"), "To Search " + strLocOfficerName + " Location/Officer Name")

                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "document.location.href='" + strURL + "';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                lblErrorMessage.Visible = True
                lblErrorMessage.Text = "Record(s) not found."
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("fld_ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class
