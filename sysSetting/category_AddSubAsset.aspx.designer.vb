'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class category_AddSubAsset
    
    '''<summary>
    '''frm_CatAddSub control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents frm_CatAddSub As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''lblErrorMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblErrorMessage As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblCatCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCatCode As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblCatName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCatName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtSubCatCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSubCatCode As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtSubCatName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSubCatName As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''cbAddInfo1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbAddInfo1 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''txtAddInfo1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAddInfo1 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''cbAddInfo2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbAddInfo2 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''txtAddInfo2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAddInfo2 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''cbAddInfo3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbAddInfo3 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''txtAddInfo3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAddInfo3 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''cbAddInfo4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbAddInfo4 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''txtAddInfo4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAddInfo4 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''cbAddInfo5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbAddInfo5 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''txtAddInfo5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAddInfo5 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ddlValidYear control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlValidYear As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''txtFloatLevel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFloatLevel As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ddlExpNotiB4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlExpNotiB4 As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''cbACTag control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbACTag As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''FUploadImage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FUploadImage As Global.System.Web.UI.HtmlControls.HtmlInputFile
    
    '''<summary>
    '''butSubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents butSubmit As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''butReset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents butReset As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''butCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents butCancel As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''hdnAddSubCatF control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnAddSubCatF As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hdnCallFrm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnCallFrm As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hdnCatID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnCatID As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hdnSCatID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnSCatID As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hdnDeleteF control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnDeleteF As Global.System.Web.UI.HtmlControls.HtmlInputHidden
End Class
