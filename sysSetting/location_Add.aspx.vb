#Region "Information Section"
' ****************************************************************************************************
' Description       : Add New Location 
' Purpose           : Add New Location Information
' Author            : See Siew
' Date              : 20/10/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class location_Add
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")
                butAddSubLoc.Attributes.Add("OnClick", "return mfnOpenAddSubLocWindow()")

                ''Create New Data Table
                fnCreateDataTable()
                BindData()

                ''set default
                hdnSubLocID.Value = 0 ''Sub Location Id reset to '0'
                hdnAddSubLocF.Value = "N"
            End If

            '' Check "Add New Sub Location"
            If hdnAddSubLocF.Value = "Y" Then
                fnAddNewSubLoc()

                ''set back to default
                hdnAddSubLocF.Value = "N"
                hdnAddSubLocCode.Value = ""
                hdnAddSubLocName.Value = ""
                hdnAddSubLocActv.Value = ""
                hdnAddSubLocLightIP.Value = ""
                hdnAddSubLocLightType.Value = ""
                hdnAddSubLocDGF.Value = ""
                hdnAddSubLocDGMvS.Value = ""
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub ButCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButCancel.Click
        Response.Redirect("location_View.aspx")
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        'lblErrorMessage.Visible = False
        'lblErrorMessage.Text = ""
        'txtLocationCode.Text = ""
        'txtLocationName.Text = ""

        Response.Redirect("location_Add.aspx")
    End Sub

    Private Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim strLocationCode, strLocationName As String
            Dim strSubLocXML As String = ""
            Dim bDupLocation As Boolean = False
            Dim intRetVal As Integer
            Dim strMsg As String

            ''** get Fields Value
            strLocationCode = Trim(txtLocationCode.Text)
            strLocationName = Trim(txtLocationName.Text)

            ''** Check for Duplicate Location Code
            bDupLocation = clsLocation.fnLocationCheckDuplicate(0, strLocationCode, "I")

            If Not bDupLocation Then ''not duplicate record
                ''Get Sub Location in XML
                Dim TblSLoc As DataTable = CType(Session("dtSubLoc"), DataTable)
                Dim ds As New DataSet
                ds = New DataSet         'creating a dataset
                ds.Tables.Add(TblSLoc)   'assign datatable to dataset
                If Not ds Is Nothing Then
                    Dim loCol As DataColumn
                    'Prepare XML output from the DataSet as a string
                    For Each loCol In ds.Tables(0).Columns
                        loCol.ColumnMapping = System.Data.MappingType.Attribute
                    Next
                    strSubLocXML = ds.GetXml
                End If

                ''** insert record
                'intRetVal = clsLocation.fnLocationInsertUpdateDelete( _
                '                    0, strLocationCode, strLocationName, Session("UsrID"), "I")
                intRetVal = clsLocation.fnLocation_AddNewLocation(strLocationCode, strLocationName, strSubLocXML, Session("UsrID"))

                If intRetVal > 0 Then
                    strMsg = "New Location Added Successfully."

                    ''** Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "New Location created : " + strLocationName + " (" + strLocationCode + ")")

                    ''** Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "document.location.href='location_View.aspx';" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    strMsg = "New Location Add Failed."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If

            Else  ''duplicate record
                lblErrorMessage.Text = "Location Code already exist. Please choose another Location Code."
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnCreateDataTable()
        Try
            ''creating a table named tbl_SubLoc
            Dim TblSLoc As DataTable
            TblSLoc = New DataTable("tbl_SubLoc")

            ''Column 1: Sub Location id
            Dim dt_SLocID As DataColumn = New DataColumn("dt_SLocID")    'declaring a column named Name
            dt_SLocID.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            TblSLoc.Columns.Add(dt_SLocID)                               'adding the column to table

            ''Column 2: Sub Location - S/No
            Dim dt_Number As DataColumn = New DataColumn("dt_Number")
            dt_Number.DataType = System.Type.GetType("System.String")
            TblSLoc.Columns.Add(dt_Number)

            ''Column 3: Sub Location Code
            Dim dt_SLocCode As DataColumn = New DataColumn("dt_SLocCode")
            dt_SLocCode.DataType = System.Type.GetType("System.String")
            TblSLoc.Columns.Add(dt_SLocCode)

            ''Column 4: Sub Location Name
            Dim dt_SLocName As DataColumn = New DataColumn("dt_SLocName")
            dt_SLocName.DataType = System.Type.GetType("System.String")
            TblSLoc.Columns.Add(dt_SLocName)

            ''Column 5: Activate
            Dim dt_SLocActv As DataColumn = New DataColumn("dt_SLocActv")
            dt_SLocActv.DataType = System.Type.GetType("System.String")
            TblSLoc.Columns.Add(dt_SLocActv)

            ''Column 6: Light IP Add
            Dim dt_SLocLightIP As DataColumn = New DataColumn("dt_SLocLightIP")
            dt_SLocLightIP.DataType = System.Type.GetType("System.String")
            TblSLoc.Columns.Add(dt_SLocLightIP)

            ''Column 7: Light IP Add
            Dim dt_SLocLightType As DataColumn = New DataColumn("dt_SLocLightType")
            dt_SLocLightType.DataType = System.Type.GetType("System.String")
            TblSLoc.Columns.Add(dt_SLocLightType)

            ''Column 8: Door Gantry
            Dim dt_DGF As DataColumn = New DataColumn("dt_DGF")
            dt_DGF.DataType = System.Type.GetType("System.String")
            TblSLoc.Columns.Add(dt_DGF)

            ''Column 9: Movement Status
            Dim dt_DGMvStatus As DataColumn = New DataColumn("dt_DGMvStatus")
            dt_DGMvStatus.DataType = System.Type.GetType("System.String")
            TblSLoc.Columns.Add(dt_DGMvStatus)

            ''Set Primary Keys
            '--> Make the ID column the primary key column.
            Dim PrimaryKeyColumns(0) As DataColumn
            PrimaryKeyColumns(0) = TblSLoc.Columns("dt_SLocID")
            TblSLoc.PrimaryKey = PrimaryKeyColumns

            ''Set Datatable record to blank
            Session("dtSubLoc") = ""

            ''Assign Subcategory Table to Session
            Session("dtSubLoc") = TblSLoc
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub BindData()
        Try
            dgSubLoc.DataSource = Session("dtSubLoc")
            dgSubLoc.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub fnAddNewSubLoc()
        Try
            Dim TblSLoc As DataTable = CType(Session("dtSubLoc"), DataTable)
            Dim RowSubLoc As DataRow
            Dim strNewLocID As String

            ''** Get New Sub Location ID 
            hdnSubLocID.Value = hdnSubLocID.Value + 1
            strNewLocID = hdnSubLocID.Value


            ''Add New Row to Datatable
            RowSubLoc = TblSLoc.NewRow()        'declaring a new row
            RowSubLoc.Item("dt_SLocID") = strNewLocID
            RowSubLoc.Item("dt_Number") = strNewLocID
            RowSubLoc.Item("dt_SLocCode") = hdnAddSubLocCode.Value
            RowSubLoc.Item("dt_SLocName") = hdnAddSubLocName.Value
            RowSubLoc.Item("dt_SLocActv") = hdnAddSubLocActv.Value
            RowSubLoc.Item("dt_SLocLightIP") = hdnAddSubLocLightIP.Value
            RowSubLoc.Item("dt_SLocLightType") = hdnAddSubLocLightType.Value
            RowSubLoc.Item("dt_DGF") = hdnAddSubLocDGF.Value
            RowSubLoc.Item("dt_DGMvStatus") = hdnAddSubLocDGMvS.Value
            TblSLoc.Rows.Add(RowSubLoc)

            ''Assign Updated Datatble to Session
            Session("dtSubLoc") = TblSLoc

            ''Bind again the Datatgrid 
            BindData()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub dgSubLoc_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSubLoc.DeleteCommand
        Try
            Dim strSLocIDDel As String = e.Item.Cells(0).Text

            ''Delete Data in Datatable
            Dim TblSLoc As DataTable = CType(Session("dtSubLoc"), DataTable)
            Dim RowSubLoc As DataRow = TblSLoc.Rows.Find(strSLocIDDel)
            If Not RowSubLoc Is Nothing Then
                RowSubLoc.Delete()
                TblSLoc.AcceptChanges()

                ''Reset the Sequence No and sub location ID
                hdnSubLocID.Value = "0"  ''reset ID
                Dim RowSubLocloop As DataRow
                For Each RowSubLocloop In TblSLoc.Rows
                    hdnSubLocID.Value = hdnSubLocID.Value + 1
                    RowSubLocloop.BeginEdit()
                    RowSubLocloop.Item("dt_SLocID") = hdnSubLocID.Value
                    RowSubLocloop.Item("dt_Number") = hdnSubLocID.Value
                    RowSubLocloop.AcceptChanges()
                Next
                TblSLoc.AcceptChanges()

                ''Assign new datatable to session
                Session("dtSubLoc") = TblSLoc
                BindData()
            Else
                lblErrorMessage.Text = "No record found for delete. Please contact your administrator."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Function fnShowActivateText(ByVal strActivateCode As String) As String
        Dim strRetVal As String = ""

        If strActivateCode = "Y" Then
            strRetVal = "Yes"
        Else
            strRetVal = "No"
        End If

        Return (strRetVal)
    End Function

    Function fnShowLightTypeText(ByVal strLightType As String) As String
        Dim strRetVal As String = ""

        If strLightType = "1" Then
            strRetVal = "Red LED"
        ElseIf strLightType = "2" Then
            strRetVal = "Green LED"
        ElseIf strLightType = "3" Then
            strRetVal = "Red LED and Buzzer"
        End If

        Return (strRetVal)
    End Function
End Class
