<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="category_AddSubAsset.aspx.vb" Inherits="AMS.category_AddSubAsset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script language="javascript">
		function chkFrm() {
			var foundError = false;
			
			//Validate Sub category Name
			if (!foundError && gfnIsFieldBlank(document.frm_CatAddSub.txtSubCatName)) {
				foundError=true;
				document.frm_CatAddSub.txtSubCatName.focus();
				alert("Please enter Sub Category Name.");
			}
			if (!foundError && document.frm_CatAddSub.txtSubCatName.value.length > 250) {
				foundError=true
				document.frm_CatAddSub.txtSubCatName.focus()
				alert("Please make sure the Sub Category Name is less than 250 characters long.")
			}
			
			//Validate Sub Category Additional Information 1
			if (!foundError && document.frm_CatAddSub.cbAddInfo1.checked) {
			    if (!foundError && gfnIsFieldBlank(document.frm_CatAddSub.txtAddInfo1)) {
				    foundError=true;
				    document.frm_CatAddSub.txtAddInfo1.focus();
				    alert("Please enter Additional Information 1.");
			    }
			    if (!foundError && document.frm_CatAddSub.txtAddInfo1.value.length > 250) {
				    foundError=true
				    document.frm_CatAddSub.txtAddInfo1.focus()
				    alert("Please make sure the Additional Information 1 is less than 250 characters long.")
			    }
			 }
			 
			 //Validate Sub Category Additional Information 2
			if (!foundError && document.frm_CatAddSub.cbAddInfo2.checked) {
			    if (!foundError && gfnIsFieldBlank(document.frm_CatAddSub.txtAddInfo2)) {
				    foundError=true;
				    document.frm_CatAddSub.txtAddInfo2.focus();
				    alert("Please enter Additional Information 2.");
			    }
			    if (!foundError && document.frm_CatAddSub.txtAddInfo2.value.length > 250) {
				    foundError=true
				    document.frm_CatAddSub.txtAddInfo2.focus()
				    alert("Please make sure the Additional Information 2 is less than 250 characters long.")
			    }
			 }
			
			//Validate Sub Category Additional Information 3
			if (!foundError && document.frm_CatAddSub.cbAddInfo3.checked) {
			    if (!foundError && gfnIsFieldBlank(document.frm_CatAddSub.txtAddInfo3)) {
				    foundError=true;
				    document.frm_CatAddSub.txtAddInfo3.focus();
				    alert("Please enter Additional Information 3.");
			    }
			    if (!foundError && document.frm_CatAddSub.txtAddInfo3.value.length > 250) {
				    foundError=true
				    document.frm_CatAddSub.txtAddInfo3.focus()
				    alert("Please make sure the Additional Information 3 is less than 250 characters long.")
			    }
			 }
		
			//Validate Sub Category Additional Information 4
			if (!foundError && document.frm_CatAddSub.cbAddInfo4.checked) {
			    if (!foundError && gfnIsFieldBlank(document.frm_CatAddSub.txtAddInfo4)) {
				    foundError=true;
				    document.frm_CatAddSub.txtAddInfo4.focus();
				    alert("Please enter Additional Information 4.");
			    }
			    if (!foundError && document.frm_CatAddSub.txtAddInfo4.value.length > 250) {
				    foundError=true
				    document.frm_CatAddSub.txtAddInfo4.focus()
				    alert("Please make sure the Additional Information 4 is less than 250 characters long.")
			    }
			 }
			 
			//Validate Sub Category Additional Information 5
			if (!foundError && document.frm_CatAddSub.cbAddInfo5.checked) {
			    if (!foundError && gfnIsFieldBlank(document.frm_CatAddSub.txtAddInfo5)) {
				    foundError=true;
				    document.frm_CatAddSub.txtAddInfo5.focus();
				    alert("Please enter Additional Information 5.");
			    }
			    if (!foundError && document.frm_CatAddSub.txtAddInfo5.value.length > 250) {
				    foundError=true
				    document.frm_CatAddSub.txtAddInfo5.focus()
				    alert("Please make sure the Additional Information 5 is less than 250 characters long.")
			    }
			}
			 
			//validate Useful Life
//			if (!foundError && document.frm_CatAddSub.ddlValidYear.value == 0) {
//			    foundError=true
//			    document.frm_CatAddSub.ddlValidYear.focus()
//			    alert("Please select Useful Life.")
//			}
			
			//validate Float Level
			if (!foundError && !gfnIsFieldBlank(document.frm_CatAddSub.txtFloatLevel)) {
			    //--> Checking Float Level much numeric
				if (!foundError && gfnCheckNumeric(document.frm_CatAddSub.txtFloatLevel,'')) {
					foundError=true;
					document.frm_CatAddSub.txtFloatLevel.focus();
					alert("Float Level just allow number only.");
				}
			}
			 
 			if (!foundError){
 			    var flag = false;
 				flag = window.confirm("Are you sure you want to add this Sub Category?");
 				return flag;
 			}
			else
				return false;
		}
	</script>
</head>
<body>
    <form id="frm_CatAddSub" method="post" runat="server">
			<TABLE id="tableSubStatusAdd" cellSpacing="1" cellPadding="1" width="90%" align="center"
				bgColor="white" border="0">
				<TBODY>
					<TR>
						<TD colSpan="2"><B>Add Sub Category</B></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></TD>
					</TR>
					<tr>
						<td colSpan="2">
						    <TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																				    <TR>
																					    <TD valign="middle" width="35%">
																					        <FONT class="DisplayTitle">Category Code : </FONT>
																					    </TD>
																					    <TD width="65%"><asp:Label id="lblCatCode" Runat="server"></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Category Name : </FONT>
																					    </TD>
																					    <TD><asp:Label id="lblCatName" Runat="server"></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Sub Category Code : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtSubCatCode" MaxLength="4" Width="40px" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Sub Category Name : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtSubCatName" MaxLength="250" Runat="server" Width="280px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Additional Information 1 : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:CheckBox ID="cbAddInfo1" runat=server Text="" />&nbsp;
																					        <asp:TextBox id="txtAddInfo1" MaxLength="250" Runat="server" Width="250px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Additional Information 2 : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:CheckBox ID="cbAddInfo2" runat=server Text="" />&nbsp;
																					        <asp:TextBox id="txtAddInfo2" MaxLength="250" Runat="server" Width="250px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Additional Information 3 : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:CheckBox ID="cbAddInfo3" runat=server Text="" />&nbsp;
																					        <asp:TextBox id="txtAddInfo3" MaxLength="250" Runat="server" Width="250px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Additional Information 4 : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:CheckBox ID="cbAddInfo4" runat=server Text="" />&nbsp;
																					        <asp:TextBox id="txtAddInfo4" MaxLength="250" Runat="server" Width="250px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Additional Information 5 : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:CheckBox ID="cbAddInfo5" runat=server Text="" />&nbsp;
																					        <asp:TextBox id="txtAddInfo5" MaxLength="250" Runat="server" Width="250px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Useful Life : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlValidYear" Runat="server"></asp:DropDownList> Year
																					    </TD>
																				    </TR>
																			        <TR>
																					    <TD valign="middle" width="35%">
																					        <FONT class="DisplayTitle">Float Level : </FONT>
																					    </TD>
																					    <TD width="65%"><asp:TextBox id="txtFloatLevel" maxlength="5" Width="40px" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Expiring Notification before : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlExpNotiB4" Runat="server"></asp:DropDownList> Month
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">AC Tag : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:CheckBox ID="cbACTag" runat="server" Text="" />
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Upload Image : </FONT>
																					    </TD>
																					    <TD>
																					        <input id="FUploadImage" runat="server" type="file" />
																					    </TD>
																				    </TR>
																					<TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:Button id="butSubmit" Text="Add" Runat="Server" />&nbsp;
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />&nbsp;
																							<asp:Button id="butCancel" Text="Cancel" Runat="Server" />
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
					    </td>
					</tr>
				</TBODY>
			</TABLE>
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnAddSubCatF" runat="server">
			<input type="hidden" id="hdnCallFrm" runat="server"> 
			<input type="hidden" id="hdnCatID" runat="server"> 
			<input type="hidden" id="hdnSCatID" runat="server">
			<input type="hidden" id="hdnDeleteF" runat="server">  
			<!-- End  : Hidden Fields -->
		</form>
		<script language="javascript">
			if (document.frm_CatAddSub.hdnAddSubCatF.value == 'Y')
			{	
				//Call From Add Category Screen
				if (document.frm_CatAddSub.hdnCallFrm.value=='Add')
				{	
					var openerForm = opener.document.forms.Frm_CategoryAdd;
					openerForm.action = "category_add.aspx";
					window.opener.document.forms(0).hdnAddSubCatF.value = document.frm_CatAddSub.hdnAddSubCatF.value;
				}	
				
				//Call From Edit Category Screen
				if (document.frm_CatAddSub.hdnCallFrm.value=='Edit')
				{
					var openerForm = opener.document.forms.Frm_CategoryEdit;
					openerForm.action = "category_edit.aspx?CategoryID=" + document.frm_CatAddSub.hdnCatID.value;
					window.opener.document.forms(0).hdnAddSubCatF.value = document.frm_CatAddSub.hdnAddSubCatF.value;
				}
					openerForm.method = "post";
					openerForm.submit();
					window.close();	
			}
		</script>
</body>
</html>
