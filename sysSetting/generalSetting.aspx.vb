#Region "Information Section"
' ****************************************************************************************************
' Description       : General Settings
' Purpose           : Edit General Settings Information
' Author            : See Siew
' Date              : 22/10/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Web.Services

#End Region

Partial Class generalSetting
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")
                Me.cbAssetNFSSendTo3.Attributes.Add("OnClick", "return fnShowHideOtherEmail('1')")
                Me.cbAssetAMSSend3.Attributes.Add("OnClick", "return fnShowHideOtherEmail('2')")
                Me.cbWarrantyExpSend3.Attributes.Add("OnClick", "return fnShowHideOtherEmail('3')")
                Me.cbContractExpSend3.Attributes.Add("OnClick", "return fnShowHideOtherEmail('4')")
                Me.cbAssetTransferSend3.Attributes.Add("OnClick", "return fnShowHideOtherEmail('5')")
                Me.cbFloatSend2.Attributes.Add("OnClick", "return fnShowHideOtherEmail('6')")
                Me.cbContractExp2Send3.Attributes.Add("OnClick", "return fnShowHideOtherEmail('7')")
                Me.cbContractExpSend5.Attributes.Add("OnClick", "return fnShowHideOtherEmail('8')")
                Me.cbContractExp2Send5.Attributes.Add("OnClick", "return fnShowHideOtherEmail('9')")

                Me.cbCondemnExpSend3.Attributes.Add("OnClick", "return fnShowHideOtherEmail('10')")
                Me.cbLeaseInvExpSend3.Attributes.Add("OnClick", "return fnShowHideOtherEmail('11')")

                'Me.txttest.Attributes.Add("onkeypress", "return fntest()")

                ''Populate Control
                If (InStr(Session("AR"), "SysSet|genrl|update") <> 0) Then
                    fnPopulateCtrl()
                End If

                ''Populate Record from backend
                fnGetRecordsFromDB()

                ''Checking for access right whether user not allow to update
                If (InStr(Session("AR"), "SysSet|genrl|update") = 0) Then
                    ComboInventoryCtrEmail.Enabled = False

                    rdAssetNFSActive.Enabled = False
                    txtAssetNFSSubject.Enabled = False
                    cbAssetNFSSendTo1.Enabled = False
                    cbAssetNFSSendTo2.Enabled = False
                    cbAssetNFSSendTo3.Enabled = False
                    ComboAssetNFSSendTo3.Enabled = False
                    txtAssetNFSContent.Enabled = False

                    rbAssetAMSActive.Enabled = False
                    txtAssetAMSSubject.Enabled = False
                    cbAssetAMSSend1.Enabled = False
                    cbAssetAMSSend2.Enabled = False
                    cbAssetAMSSend3.Enabled = False
                    ComboAssetAMSSend3.Enabled = False
                    txtAssetAMSContent.Enabled = False


                    rbWarrantyExp.Enabled = False
                    txtWarrantyExpSubj.Enabled = False
                    cbWarrantyExpSend1.Enabled = False
                    cbWarrantyExpSend2.Enabled = False
                    cbWarrantyExpSend3.Enabled = False
                    ComboWarrantyExpSend3.Enabled = False
                    txtWarrantyExpContent.Enabled = False

                    rbCondemnExp.Enabled = False
                    txtCondemnExpSubj.Enabled = False
                    cbCondemnExpSend1.Enabled = False
                    cbCondemnExpSend2.Enabled = False
                    cbCondemnExpSend3.Enabled = False
                    ComboCondemnExpSend3.Enabled = False
                    txtCondemnExpContent.Enabled = False

                    rbLeaseInvExp.Enabled = False
                    txtLeaseInvExpSubj.Enabled = False
                    cbLeaseInvExpSend1.Enabled = False
                    cbLeaseInvExpSend2.Enabled = False
                    cbLeaseInvExpSend3.Enabled = False
                    ComboLeaseInvExpSend3.Enabled = False
                    txtLeaseInvExpContent.Enabled = False

                    rbContractExp.Enabled = False
                    txtContractExpSubj.Enabled = False
                    cbContractExpSend2.Enabled = False
                    cbContractExpSend3.Enabled = False
                    ComboContractExpSend3.Enabled = False
                    cbContractExpSend4.Enabled = False
                    cbContractExpSend5.Enabled = False
                    ComboContractExpSend5.Enabled = False
                    txtContractExpContent.Enabled = False

                    rbContractExp2.Enabled = False
                    txtContractExpSubj2.Enabled = False
                    cbContractExp2Send2.Enabled = False
                    cbContractExp2Send3.Enabled = False
                    ComboContractExp2Send3.Enabled = False
                    cbContractExp2Send4.Enabled = False
                    cbContractExp2Send5.Enabled = False
                    ComboContractExp2Send5.Enabled = False
                    txtContractExpContent2.Enabled = False

                    rbAssetTransfer.Enabled = False
                    txtAssetTransferSubj.Enabled = False
                    cbAssetTransferSend1.Enabled = False
                    cbAssetTransferSend2.Enabled = False
                    cbAssetTransferSend3.Enabled = False
                    ComboAssetTransferSend3.Enabled = False
                    txtAssetTransferContent.Enabled = False

                    rbFloatActive.Enabled = False
                    txtFloatSubj.Enabled = False
                    cbFloatSend1.Enabled = False
                    cbFloatSend2.Enabled = False
                    ComboFloatSend2.Enabled = False
                    txtFloatContent.Enabled = False

                    txtRenewalCtrlDays.Enabled = False

                    butSubmit.Enabled = False
                    butReset.Enabled = False
                End If

            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            Dim ds As New DataSet
            ds = fnGetUsrEmailDataSet("")

            ''Populate Inventory Email
            'ComboInventoryCtrEmail.DataSource = ds
            'ComboInventoryCtrEmail.DataBind()

            ' ''Populate new Addition of Asset from NFS
            'ComboAssetNFSSendTo3.DataSource = ds
            'ComboAssetNFSSendTo3.DataBind()

            ' ''Populate new Addition of Asset in AMS
            'ComboAssetAMSSend3.DataSource = ds
            'ComboAssetAMSSend3.DataBind()

            ' ''Populate Warranty Expiry
            'ComboWarrantyExpSend3.DataSource = ds
            'ComboWarrantyExpSend3.DataBind()

            ' ''Populate Contract Expiry - 1st Reminder
            'ComboContractExpSend3.DataSource = ds
            'ComboContractExpSend3.DataBind()

            ' ''Populate Contract Expiry - 1st Reminder --> CC
            'ComboContractExpSend5.DataSource = ds
            'ComboContractExpSend5.DataBind()

            ' ''Populate Contract Expiry - 2nd Reminder
            'ComboContractExp2Send3.DataSource = ds
            'ComboContractExp2Send3.DataBind()

            ' ''Populate Contract Expiry - 2nd Reminder --> CC
            'ComboContractExp2Send5.DataSource = ds
            'ComboContractExp2Send5.DataBind()

            ' ''Populate Asset Transfer
            'ComboAssetTransferSend3.DataSource = ds
            'ComboAssetTransferSend3.DataBind()

            ' ''Populate Float Replacement Level is reached
            'ComboFloatSend2.DataSource = ds
            'ComboFloatSend2.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnGetRecordsFromDB()
        Dim objRdr As SqlDataReader
        Try
            Dim arrSendToSelected() As String
            objRdr = clsGeneralSetting.fnGrlSetGetGeneralSettingInfo("A", "", "1")
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        ''Check for Selected Header
                        Select Case CStr(objRdr("fld_systFieldsName"))
                            Case "Inventory Controller Email"
                                ComboInventoryCtrEmail.Text = CStr(objRdr("fld_systFieldsStr"))

                            Case "New Asset From NFS"
                                rdAssetNFSActive.SelectedValue = CStr(objRdr("fld_systFieldsFlg"))
                                txtAssetNFSSubject.Text = CStr(objRdr("fld_systFieldsEmailSubj"))

                                arrSendToSelected = Split(CStr(objRdr("fld_systFieldsEmailSendTo")), "|")
                                If arrSendToSelected(0) = "Y" Then cbAssetNFSSendTo1.Checked = True
                                If arrSendToSelected(1) = "Y" Then cbAssetNFSSendTo2.Checked = True
                                If arrSendToSelected(2) = "Y" Then
                                    cbAssetNFSSendTo3.Checked = True
                                    ComboAssetNFSSendTo3.Text = CStr(objRdr("fld_systFieldsStr"))
                                End If
                                txtAssetNFSContent.Text = CStr(objRdr("fld_systFieldsEmailContent"))

                            Case "New Asset From AMS"
                                rbAssetAMSActive.SelectedValue = CStr(objRdr("fld_systFieldsFlg"))
                                txtAssetAMSSubject.Text = CStr(objRdr("fld_systFieldsEmailSubj"))

                                arrSendToSelected = Split(CStr(objRdr("fld_systFieldsEmailSendTo")), "|")
                                If arrSendToSelected(0) = "Y" Then cbAssetAMSSend1.Checked = True
                                If arrSendToSelected(1) = "Y" Then cbAssetAMSSend2.Checked = True
                                If arrSendToSelected(2) = "Y" Then
                                    cbAssetAMSSend3.Checked = True
                                    ComboAssetAMSSend3.Text = CStr(objRdr("fld_systFieldsStr"))
                                End If
                                txtAssetAMSContent.Text = CStr(objRdr("fld_systFieldsEmailContent"))

                            Case "Warranty Expiry"
                                rbWarrantyExp.SelectedValue = CStr(objRdr("fld_systFieldsFlg"))
                                txtWarrantyExpSubj.Text = CStr(objRdr("fld_systFieldsEmailSubj"))

                                arrSendToSelected = Split(CStr(objRdr("fld_systFieldsEmailSendTo")), "|")
                                If arrSendToSelected(0) = "Y" Then cbWarrantyExpSend1.Checked = True
                                If arrSendToSelected(1) = "Y" Then cbWarrantyExpSend2.Checked = True
                                If arrSendToSelected(2) = "Y" Then
                                    cbWarrantyExpSend3.Checked = True
                                    ComboWarrantyExpSend3.Text = CStr(objRdr("fld_systFieldsStr"))
                                End If
                                txtWarrantyExpContent.Text = CStr(objRdr("fld_systFieldsEmailContent"))

                            Case "Condemn Expiry"
                                rbCondemnExp.SelectedValue = CStr(objRdr("fld_systFieldsFlg"))
                                txtCondemnExpSubj.Text = CStr(objRdr("fld_systFieldsEmailSubj"))

                                arrSendToSelected = Split(CStr(objRdr("fld_systFieldsEmailSendTo")), "|")
                                If arrSendToSelected(0) = "Y" Then cbCondemnExpSend1.Checked = True
                                If arrSendToSelected(1) = "Y" Then cbCondemnExpSend2.Checked = True
                                If arrSendToSelected(2) = "Y" Then
                                    cbCondemnExpSend3.Checked = True
                                    ComboCondemnExpSend3.Text = CStr(objRdr("fld_systFieldsStr"))
                                End If
                                txtCondemnExpContent.Text = CStr(objRdr("fld_systFieldsEmailContent"))

                            Case "Leased Inventory Expiry"
                                rbLeaseInvExp.SelectedValue = CStr(objRdr("fld_systFieldsFlg"))
                                txtLeaseInvExpSubj.Text = CStr(objRdr("fld_systFieldsEmailSubj"))

                                arrSendToSelected = Split(CStr(objRdr("fld_systFieldsEmailSendTo")), "|")
                                If arrSendToSelected(0) = "Y" Then cbLeaseInvExpSend1.Checked = True
                                If arrSendToSelected(1) = "Y" Then cbLeaseInvExpSend2.Checked = True
                                If arrSendToSelected(2) = "Y" Then
                                    cbLeaseInvExpSend3.Checked = True
                                    ComboLeaseInvExpSend3.Text = CStr(objRdr("fld_systFieldsStr"))
                                End If
                                txtLeaseInvExpContent.Text = CStr(objRdr("fld_systFieldsEmailContent"))

                            Case "Contract Expiry"
                                rbContractExp.SelectedValue = CStr(objRdr("fld_systFieldsFlg"))
                                txtContractExpSubj.Text = CStr(objRdr("fld_systFieldsEmailSubj"))

                                arrSendToSelected = Split(CStr(objRdr("fld_systFieldsEmailSendTo")), "|")
                                If arrSendToSelected(0) = "Y" Then cbContractExpSend2.Checked = True
                                If arrSendToSelected(1) = "Y" Then
                                    cbContractExpSend3.Checked = True
                                    ComboContractExpSend3.Text = CStr(objRdr("fld_systFieldsStr"))
                                End If
                                If arrSendToSelected(2) = "Y" Then cbContractExpSend4.Checked = True
                                If arrSendToSelected(3) = "Y" Then
                                    cbContractExpSend5.Checked = True
                                    ComboContractExpSend5.Text = CStr(objRdr("fld_systFieldsStr2"))
                                End If

                                txtContractExpContent.Text = CStr(objRdr("fld_systFieldsEmailContent"))

                            Case "Contract Expiry 2"
                                rbContractExp2.SelectedValue = CStr(objRdr("fld_systFieldsFlg"))
                                txtContractExpSubj2.Text = CStr(objRdr("fld_systFieldsEmailSubj"))

                                arrSendToSelected = Split(CStr(objRdr("fld_systFieldsEmailSendTo")), "|")
                                If arrSendToSelected(0) = "Y" Then cbContractExp2Send2.Checked = True
                                If arrSendToSelected(1) = "Y" Then
                                    cbContractExp2Send3.Checked = True
                                    ComboContractExp2Send3.Text = CStr(objRdr("fld_systFieldsStr"))
                                End If
                                If arrSendToSelected(2) = "Y" Then cbContractExp2Send4.Checked = True
                                If arrSendToSelected(3) = "Y" Then
                                    cbContractExp2Send5.Checked = True
                                    ComboContractExp2Send5.Text = CStr(objRdr("fld_systFieldsStr2"))
                                End If
                                txtContractExpContent2.Text = CStr(objRdr("fld_systFieldsEmailContent"))

                            Case "Asset Transfer"
                                rbAssetTransfer.SelectedValue = CStr(objRdr("fld_systFieldsFlg"))
                                txtAssetTransferSubj.Text = CStr(objRdr("fld_systFieldsEmailSubj"))

                                arrSendToSelected = Split(CStr(objRdr("fld_systFieldsEmailSendTo")), "|")
                                If arrSendToSelected(0) = "Y" Then cbAssetTransferSend1.Checked = True
                                If arrSendToSelected(1) = "Y" Then cbAssetTransferSend2.Checked = True
                                If arrSendToSelected(2) = "Y" Then
                                    cbAssetTransferSend3.Checked = True
                                    ComboAssetTransferSend3.Text = CStr(objRdr("fld_systFieldsStr"))
                                End If
                                txtAssetTransferContent.Text = CStr(objRdr("fld_systFieldsEmailContent"))

                            Case "Float Replacement"
                                rbFloatActive.SelectedValue = CStr(objRdr("fld_systFieldsFlg"))
                                txtFloatSubj.Text = CStr(objRdr("fld_systFieldsEmailSubj"))

                                arrSendToSelected = Split(CStr(objRdr("fld_systFieldsEmailSendTo")), "|")
                                If arrSendToSelected(0) = "Y" Then cbFloatSend1.Checked = True
                                If arrSendToSelected(1) = "Y" Then
                                    cbFloatSend2.Checked = True
                                    ComboFloatSend2.Text = CStr(objRdr("fld_systFieldsStr"))
                                End If
                                txtFloatContent.Text = CStr(objRdr("fld_systFieldsEmailContent"))

                            Case "Renewal Contarct Days After Expired"
                                txtRenewalCtrlDays.Text = CStr(objRdr("fld_systFieldsStr"))

                            Case "StockTake Action"
                                rdStocktakeActive.SelectedValue = CStr(objRdr("fld_systFieldsFlg"))
                                txtSTFrmDt.Text = CStr(objRdr("fld_systFieldFrmDt"))
                                txtSTToDt.Text = CStr(objRdr("fld_systFieldToDt"))
                        End Select
                    End While
                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub


    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            fnGetRecordsFromDB()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim intRetVal As Integer
            Dim strMsg As String
            Dim strInventoryEmail As String
            Dim strNFSActive, strNFSSubj, strNFSSendTo, strNFSSendToOther, strNFSContent As String
            Dim strAMSActive, strAMSSubj, strAMSSendTo, strAMSSendToOther, strAMSContent As String
            Dim strWarExpActive, strWarExpSubj, strWarExpSendTo, strWarExpSendToOther, strWarExpContent As String
            Dim strCondemnExpActive, strCondemnExpSubj, strCondemnExpSendTo, strCondemnExpSendToOther, strCondemnExpContent As String
            Dim strLeasedInvExpActive, strLeasedInvExpSubj, strLeasedInvExpSendTo, strLeasedInvExpSendToOther, strLeasedInvExpContent As String
            Dim strCtrExpActive, strCtrExpSubj, strCtrExpSendTo, strCtrExpSendToOther, strCtrExpSendCCOther, strCtrExpContent As String
            Dim strCtrExpActive2, strCtrExpSubj2, strCtrExpSendTo2, strCtrExpSendToOther2, strCtrExpSendCCOther2, strCtrExpContent2 As String
            Dim strAssTrfActive, strAssTrfSubj, strAssTrfSendTo, strAssTrfSendToOther, strAssTrfContent As String
            Dim strFloatActive, strFloatSubj, strFloatSendTo, strFloatSendToOther, strFloatContent As String
            Dim strRenewalCtrlDays As String
            Dim strStockTakeActive, strStockTakeFrmDt, strStockTakeToDt As String

            ''** Get Value
            ''1. Inventory Controller Email
            strInventoryEmail = ComboInventoryCtrEmail.Text

            ''2.Checking for New Addtion for NFS
            strNFSActive = rdAssetNFSActive.SelectedValue
            strNFSSubj = txtAssetNFSSubject.Text
            strNFSSendTo = IIf(cbAssetNFSSendTo1.Checked, "Y", "N") & "|"
            strNFSSendTo += IIf(cbAssetNFSSendTo2.Checked, "Y", "N") & "|"
            strNFSSendTo += IIf(cbAssetNFSSendTo3.Checked, "Y", "N")
            strNFSSendToOther = IIf(cbAssetNFSSendTo3.Checked, ComboAssetNFSSendTo3.Text, "")
            strNFSContent = txtAssetNFSContent.Text

            ''3.Checking for New Addtion for AMS 
            strAMSActive = rbAssetAMSActive.SelectedValue
            strAMSSubj = txtAssetAMSSubject.Text
            strAMSSendTo = IIf(cbAssetAMSSend1.Checked, "Y", "N") & "|"
            strAMSSendTo += IIf(cbAssetAMSSend2.Checked, "Y", "N") & "|"
            strAMSSendTo += IIf(cbAssetAMSSend3.Checked, "Y", "N")
            strAMSSendToOther = IIf(cbAssetAMSSend3.Checked, ComboAssetAMSSend3.Text, "")
            strAMSContent = txtAssetAMSContent.Text

            ''4.Checking for Warranty Expiry 
            strWarExpActive = rbWarrantyExp.SelectedValue
            strWarExpSubj = txtWarrantyExpSubj.Text
            strWarExpSendTo = IIf(cbWarrantyExpSend1.Checked, "Y", "N") & "|"
            strWarExpSendTo += IIf(cbWarrantyExpSend2.Checked, "Y", "N") & "|"
            strWarExpSendTo += IIf(cbWarrantyExpSend3.Checked, "Y", "N")
            strWarExpSendToOther = IIf(cbWarrantyExpSend3.Checked, ComboWarrantyExpSend3.Text, "")
            strWarExpContent = txtWarrantyExpContent.Text

            ''10.Checking for Condemn Expiry 
            strCondemnExpActive = rbCondemnExp.SelectedValue
            strCondemnExpSubj = txtCondemnExpSubj.Text
            strCondemnExpSendTo = IIf(cbCondemnExpSend1.Checked, "Y", "N") & "|"
            strCondemnExpSendTo += IIf(cbCondemnExpSend2.Checked, "Y", "N") & "|"
            strCondemnExpSendTo += IIf(cbCondemnExpSend3.Checked, "Y", "N")
            strCondemnExpSendToOther = IIf(cbCondemnExpSend3.Checked, ComboCondemnExpSend3.Text, "")
            strCondemnExpContent = txtCondemnExpContent.Text

            ''11.Checking for Leased Inventory Expiry 
            strLeasedInvExpActive = rbLeaseInvExp.SelectedValue
            strLeasedInvExpSubj = txtLeaseInvExpSubj.Text
            strLeasedInvExpSendTo = IIf(cbLeaseInvExpSend1.Checked, "Y", "N") & "|"
            strLeasedInvExpSendTo += IIf(cbLeaseInvExpSend2.Checked, "Y", "N") & "|"
            strLeasedInvExpSendTo += IIf(cbLeaseInvExpSend3.Checked, "Y", "N")
            strLeasedInvExpSendToOther = IIf(cbLeaseInvExpSend3.Checked, ComboLeaseInvExpSend3.Text, "")
            strLeasedInvExpContent = txtLeaseInvExpContent.Text

            ''5a.Checking for Contract Expiry - 1st Reminder
            strCtrExpActive = rbContractExp.SelectedValue
            strCtrExpSubj = txtContractExpSubj.Text
            strCtrExpSendTo = IIf(cbContractExpSend2.Checked, "Y", "N") & "|"
            strCtrExpSendTo += IIf(cbContractExpSend3.Checked, "Y", "N") & "|"
            strCtrExpSendTo += IIf(cbContractExpSend4.Checked, "Y", "N") & "|"
            strCtrExpSendTo += IIf(cbContractExpSend5.Checked, "Y", "N")
            strCtrExpSendToOther = IIf(cbContractExpSend3.Checked, ComboContractExpSend3.Text, "")
            strCtrExpSendCCOther = IIf(cbContractExpSend5.Checked, ComboContractExpSend5.Text, "")
            strCtrExpContent = txtContractExpContent.Text

            ''5b.Checking for Contract Expiry - 2nd Reminder
            strCtrExpActive2 = rbContractExp2.SelectedValue
            strCtrExpSubj2 = txtContractExpSubj2.Text
            strCtrExpSendTo2 = IIf(cbContractExp2Send2.Checked, "Y", "N") & "|"
            strCtrExpSendTo2 += IIf(cbContractExp2Send3.Checked, "Y", "N") & "|"
            strCtrExpSendTo2 += IIf(cbContractExp2Send4.Checked, "Y", "N") & "|"
            strCtrExpSendTo2 += IIf(cbContractExp2Send5.Checked, "Y", "N")
            strCtrExpSendToOther2 = IIf(cbContractExp2Send3.Checked, ComboContractExp2Send3.Text, "")
            strCtrExpSendCCOther2 = IIf(cbContractExp2Send5.Checked, ComboContractExp2Send5.Text, "")
            strCtrExpContent2 = txtContractExpContent2.Text

            ''6.Checking for Asset Transfer
            strAssTrfActive = rbAssetTransfer.SelectedValue
            strAssTrfSubj = txtAssetTransferSubj.Text
            strAssTrfSendTo = IIf(cbAssetTransferSend1.Checked, "Y", "N") & "|"
            strAssTrfSendTo += IIf(cbAssetTransferSend2.Checked, "Y", "N") & "|"
            strAssTrfSendTo += IIf(cbAssetTransferSend3.Checked, "Y", "N")
            strAssTrfSendToOther = IIf(cbAssetTransferSend3.Checked, ComboAssetTransferSend3.Text, "")
            strAssTrfContent = txtAssetTransferContent.Text

            ''7.Checking for Float Replacement 
            strFloatActive = rbFloatActive.SelectedValue
            strFloatSubj = txtFloatSubj.Text
            strFloatSendTo = IIf(cbFloatSend1.Checked, "Y", "N") & "|"
            strFloatSendTo += IIf(cbFloatSend2.Checked, "Y", "N")
            strFloatSendToOther = IIf(cbFloatSend2.Checked, ComboFloatSend2.Text, "")
            strFloatContent = txtFloatContent.Text

            ''8.checking for Renewal Contract in X Days
            strRenewalCtrlDays = txtRenewalCtrlDays.Text

            ''9.checking for Stocktake action
            strStockTakeActive = rdStocktakeActive.SelectedValue
            strStockTakeFrmDt = txtSTFrmDt.Text
            strStockTakeToDt = txtSTToDt.Text


            ''update general settings
            intRetVal = clsGeneralSetting.fnGnrSetUpdateGeneralSetting( _
                            strInventoryEmail, _
                            strNFSActive, strNFSSubj, strNFSSendTo, strNFSSendToOther, strNFSContent, _
                            strAMSActive, strAMSSubj, strAMSSendTo, strAMSSendToOther, strAMSContent, _
                            strWarExpActive, strWarExpSubj, strWarExpSendTo, strWarExpSendToOther, strWarExpContent, _
                            strCondemnExpActive, strCondemnExpSubj, strCondemnExpSendTo, strCondemnExpSendToOther, strCondemnExpContent, _
                            strLeasedInvExpActive, strLeasedInvExpSubj, strLeasedInvExpSendTo, strLeasedInvExpSendToOther, strLeasedInvExpContent, _
                            strCtrExpActive, strCtrExpSubj, strCtrExpSendTo, strCtrExpSendToOther, strCtrExpContent, _
                            strAssTrfActive, strAssTrfSubj, strAssTrfSendTo, strAssTrfSendToOther, strAssTrfContent, _
                            strFloatActive, strFloatSubj, strFloatSendTo, strFloatSendToOther, strFloatContent, _
                            strRenewalCtrlDays, strStockTakeActive, strStockTakeFrmDt, strStockTakeToDt, _
                            strCtrExpActive2, strCtrExpSubj2, strCtrExpSendTo2, strCtrExpSendToOther2, strCtrExpContent2, _
                            strCtrExpSendCCOther, strCtrExpSendCCOther2, _
                            Session("UsrID"))

            If intRetVal > 0 Then
                strMsg = "General Settings Updated Successfully."

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "General Setting Updated")

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "document.location.href='../common/welcomePg.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "General Setting Update Failed."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If


        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Function fnGetUsrEmailDataSet(ByVal strSearch As String) As DataSet
        Try
            Return ClsUser.fnUsrGetUsrEmailFromAD(strSearch)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod()>
    Public Shared Function GetUsers(prefix As String) As String()

        Dim arrary() As String = prefix.ToString().Split(";")
        Dim dt As DataTable = New DataTable()
        'Dim par As String = ""
        If arrary.Length > 1 Then
            prefix = arrary(arrary.Length - 1).Trim()
        End If

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("EM", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}", row("EmailContent")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function
    
End Class
