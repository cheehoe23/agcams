<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="category_edit.aspx.vb" Inherits="AMS.category_edit" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script language="javascript">
			function mfnOpenAddSubCatWindow() 
			{
				    var CC = document.Frm_CategoryEdit.txtCatCode.value;
					var CN = document.Frm_CategoryEdit.txtCatName.value;
					var CatID = document.Frm_CategoryEdit.hdnCategoryID.value;
					window.open('category_AddSubAsset.aspx?CallFrm=Edit&CN='+CN+'&CC='+CC+'&CatID='+CatID+'&SCatID=0&DeleteF=N', 'Add_Asset','width=550,height=480,Top=0,left=0,scrollbars=1');
			}
			function mfnDeleteSubcat(SCatID) {	
				var flag;
				flag = false;
				
				//Check whether This is last subcategory for category, if last category, then:-
				//--> 1. User need to add another new category.
				if (document.Frm_CategoryEdit.hdnTotRec.value == '1') {
					flag = window.confirm("You have chosen to delete last Subcategory.\nAre you want to add new Subcategory.\nYou cannot undo this action. Continue?");
					if (flag){	
						var CC = document.Frm_CategoryEdit.txtCatCode.value;
					    var CN = document.Frm_CategoryEdit.txtCatName.value;
					    var CatID = document.Frm_CategoryEdit.hdnCategoryID.value;
					    window.open('category_AddSubAsset.aspx?CallFrm=Edit&CN='+CN+'&CC='+CC+'&CatID='+CatID+'&SCatID='+ SCatID +'&DeleteF=Y', 'Add_Asset','width=550,height=480,Top=0,left=0,scrollbars=1');
					}
				}
				
				//Not the last subcategory --> Normal Deletion 
				else{
					flag = window.confirm("You have chosen to delete this Subcategory.\nYou cannot undo this action. Continue?");
					if (flag){	
						document.Frm_CategoryEdit.hdnSCatID.value = SCatID;
						document.Frm_CategoryEdit.hdnDeleteSubFlag.value = 'Y';
						document.Frm_CategoryEdit.submit();
					}
				}
		    }
		    function mfnDeleteAll() 
		    {
			    var flag = false;
			    flag = window.confirm("You have chosen to delete this Category.\nYou cannot undo this action. Continue?");
			    return flag;
		    }
		    function chkFrm() {
				var foundError = false;
				
				//validate Category Code 
				if (!foundError && gfnIsFieldBlank(document.Frm_CategoryEdit.txtCatCode)) {
					foundError=true;
					document.Frm_CategoryEdit.txtCatCode.focus();
					alert("Please enter Category Code.");
				}
				if (!foundError && document.Frm_CategoryEdit.txtCatCode.value.length != 2) {
					foundError=true
					document.Frm_CategoryEdit.txtCatCode.focus();
					alert("Please make sure the Category Code is 2 characters long.");
				}
				
				//validate Category Name 
				if (!foundError && gfnIsFieldBlank(document.Frm_CategoryEdit.txtCatName)) {
					foundError=true;
					document.Frm_CategoryEdit.txtCatName.focus();
					alert("Please enter Category Name.");
				}
				if (!foundError && document.Frm_CategoryEdit.txtCatName.value.length > 250) {
					foundError=true
					document.Frm_CategoryEdit.txtCatName.focus();
					alert("Please make sure the Category Name is less than 250 characters long.");
				}
				
										
 				if (!foundError){
 				    var flag = false;
 					flag = window.confirm("Are you sure you want to update this category?");
 					return flag;
 				}
				else
					return false;
			}
    </script>
</head>
<body>
    <form id="Frm_CategoryEdit" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Administration : Edit Category</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" height="400" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																				    <TR>
																					    <TD valign="middle" width="35%">
																					        <FONT class="DisplayTitle"><font color="red">*</font>Category Code : </FONT>
																					    </TD>
																					    <TD width="65%"><asp:TextBox id="txtCatCode" maxlength="2" Width="40px" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD valign="middle" width="35%">
																					        <FONT class="DisplayTitle"><font color="red">*</font>Category Name : </FONT>
																					    </TD>
																					    <TD width="65%"><asp:TextBox id="txtCatName" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																			        <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Subcategory</b></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2" align=right><asp:LinkButton ID=butAddSubCat runat=server>[Add Subcategory]</asp:LinkButton></TD>
																					</TR>
																				    <TR>
																					    <TD colspan=2>
																					        <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgSubCategory" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_CatSubID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn datafield="fld_CategoryID" headertext="fld_CategoryID" ItemStyle-Height="10" Visible=false>
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_CatSubID" headertext="fld_CatSubID" ItemStyle-Height="10" Visible=false>
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
										                                                            <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Sub Category Code" datafield="fld_CatSubCode">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Sub Category Name" datafield="fld_CatSubName">
												                                                        <itemstyle width="20%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn ItemStyle-Width="25%" HeaderText="Additional Information (Mandatory/Optional)" 
											                                                            ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												                                                        <itemtemplate>
													                                                        <%#fnGetAddInfo(DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfo1"), DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfoM1"), _
													                                                                        DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfo2"), DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfoM2"), _
													                                                                        DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfo3"), DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfoM3"), _
													                                                                        DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfo4"), DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfoM4"), _
													                                                                        DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfo5"), DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfoM5"))%>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
											                                                        <asp:boundcolumn headertext="Useful Life (Year)" datafield="fld_UsefulLifeYear">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>	
											                                                        <asp:boundcolumn headertext="Float Level" datafield="fld_FloatLevel">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>	
											                                                        <asp:boundcolumn headertext="Expiring Notification (Month)" datafield="fld_ExpNoticeB4">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="AC Tag" datafield="fld_ACTagName">
												                                                        <itemstyle width="7%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn ItemStyle-Width="5%" HeaderText="<IMG SRC=../images/audit.gif Border=0 alt='View Images.'>" 
							                                                                            ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Center>
								                                                                        <itemtemplate>
									                                                                        <%#fnGetFileName(DataBinder.Eval(Container.DataItem, "fld_CatSubID"), DataBinder.Eval(Container.DataItem, "fld_FileName"))%>
								                                                                        </itemtemplate>
							                                                                        </asp:templatecolumn>
											                                                        <asp:hyperlinkcolumn headertext="<img src='../images/update.gif' runat=server id=imgUpdateH>" text="<img src='../images/update.gif' runat=server id=imgUpdateH border=0>"
																										navigateurl="categorySub_Edit.aspx" datanavigateurlfield="fld_CatSubID" DataNavigateUrlFormatString="categorySub_Edit.aspx?CallFrm=Edit&SCategoryID={0}">
																										<itemstyle cssclass="GridText" horizontalalign="Center" width="5%" verticalalign="Middle"></itemstyle>
																									</asp:hyperlinkcolumn>
											                                                       <asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="5%">
																										<headertemplate>
																											<img src="../images/delete.gif" alt="Delete" runat="server" id="imgDelete">
																										</headertemplate>
																										<itemtemplate>
																											<center>
																												<%#fnShowDeleteBut(DataBinder.Eval(Container.DataItem, "fld_CatSubID"), DataBinder.Eval(Container.DataItem, "CatAssign2Asset"))%>
																											</center>
																										</itemtemplate>
																									</asp:templatecolumn>
																									<asp:boundcolumn HeaderText="CatAssign2Asset" datafield="CatAssign2Asset" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn HeaderText="fld_CategoryCode" datafield="fld_CategoryCode" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn HeaderText="fld_CategoryName" datafield="fld_CategoryName" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>								
										                                                        </Columns>
									                                                        </asp:datagrid>
																					    </TD>
																					</TR>
																					<TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:Button id="butSubmit" Text="Submit" Runat="Server" />&nbsp;
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />&nbsp;
																							<asp:Button id="butCancel" Text="Cancel" Runat="Server" />&nbsp;
																							<asp:Button id="butDelete" Text="Delete" Runat="Server" />
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnCategoryID" runat="server"> 
			<input type="hidden" id="hdnSCatID" runat="server">
			<input type="hidden" id="hdnTotRec"  runat="server">
			<input type="hidden" id="hdnCatAssign2Asset"  runat="server">
			<input type="hidden" id="hdnAddSubCatF"  runat="server">
			<input type="hidden" id="hdnDeleteSubFlag"  runat="server">
			<!-- End  : Hidden Fields -->
	</form>
</body>
</html>
