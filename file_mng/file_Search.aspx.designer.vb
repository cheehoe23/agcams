'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class file_Search

    '''<summary>
    '''frm_fileSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents frm_fileSearch As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''lblHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblHeader As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblErrorMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblErrorMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtMasterFileRefNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMasterFileRefNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtDetailFileRefNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDetailFileRefNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtDetailFileTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDetailFileTitle As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtLocationOfficerName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtLocationOfficerName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtRemarks control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRemarks As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtRFIDCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRFIDCode As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ButSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButSearch As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''butReset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents butReset As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''hdnCallFrom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnCallFrom As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Footer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Footer As Global.AMS.FTSfooter_cr
End Class
