#Region "Information Section"
' ****************************************************************************************************
' Description       : Volume Modification Screen  
' Purpose           : Volume Modification Information
' Author            : See Siew
' Date              : 09/04/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class File_EditVolume
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim dateculture As IFormatProvider = New System.Globalization.CultureInfo("fr-FR", True)

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            '''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("/common/logout.aspx", False)
                Exit Sub
            End If
            '''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")
                butOffLoc.Attributes.Add("OnClick", "return mfnOpenOfficerLoc()")

                ''Get Hidden Data
                hdnCallFrm.Value = Request("CallFrm")
                hdnFileID.Value = Request("strFileID")
                hdnFileDetailID.Value = Request("strFileDetailID")
                hdnCurrentDate.Value = mfnGetCurrentDate()
                hdnAutoChgLocF.Value = "N"

                ''Populate Control
                'fnPopulateCtrl()

                ''Display File Record
                fnPopulateRecords()

                fnPopulatePagesInDDL(ddlPageSize)
                dgFileMV.CurrentPageIndex = 0
                ''Display File Movement
                fnPopulateFileMvRec()
            End If

            If hdnChgOffLocF.Value = "Y" Then
                dgFileMV.Visible = True
                hdnChgOffLocF.Value = "N"
                lblOffLoc.Text = hdnOffLocName.Value
                fnChekingAutoChgLoc()
                fnPopulateFileMvRec()
                'Else
                '    Session("Status") = ""
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub fnPopulateFileMvRec()
        Try

            dgFileMV.CurrentPageIndex = 0

            Dim dt As DataTable = clsFile.fnFileMvGetRecords(hdnFileDetailID.Value).Tables(0)

            dgFileMV.DataSource = dt
            dgFileMV.DataBind()

            'If (hdnAutoChgLocF.Value <> "Y") Then
            If Not dgFileMV.Items.Count > 0 Then
                dgFileMV.Visible = False
                lblErrorMessage.Text = "There is no file movement record(s) found. Please contact your administrator."
                lblErrorMessage.Visible = True
            End If
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub fnPopulateCtrl()
        Try
            'Populate Security Status 
            'fnPopulateSecurityStatusInDDL(ddlSecurityStatus)

            ''Get Status 
            Dim drStatus As SqlDataReader
            drStatus = clsStatus.fnStatusGetRecForDDLOrByPCode("", "0", "0")
            fnPopulateDropDownList(drStatus, ddlStatus, "fld_StatusID", "fld_StatusName", False)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub fnPopulateRecords()
        Try
            Dim objRdr As SqlDataReader
            Dim strStatusCode As String

            'Get File Master Data
            objRdr = clsFile.fnFileGetRecordForEdit(hdnFileID.Value, hdnFileDetailID.Value)
            If Not objRdr Is Nothing Then
                objRdr.Read()
                lblFileRefNo.Text = CStr(objRdr("fld_FileRefNo"))
                hdnFileID.Value = CStr(objRdr("fld_FileID"))
                hdnFileDetailID.Value = CStr(objRdr("fld_FileDetailID"))

                lblfld_FileDetail_FileRefNo.Text = CStr(objRdr("fld_FileDetail_FileRefNo"))
                lblfld_FileDetail_FileTitle.Text = CStr(objRdr("fld_FileDetail_FileTitle"))
                lblBarcode.Text = CStr(objRdr("fld_FileDetail_RFID_code"))
                lblOffLoc.Text = CStr(objRdr("OffLocName"))

                'If (Request("strFileID").ToString() = "") Then
                If (hdnAutoChgLocF.Value <> "Y") Then
                    hdnOldOffLocName.Value = CStr(objRdr("OffLocName"))
                End If

                hdnOffLocID.Value = CStr(objRdr("OffLocID"))
                hdnOffLocType.Value = CStr(objRdr("fld_FileMvType"))

                If (objRdr("fld_Status").ToString().Trim().ToUpper() = "OPEN") Then
                    ddlStatus.SelectedIndex = 0
                    hdnOldStatus.Value = ddlStatus.SelectedValue
                ElseIf (objRdr("fld_Status").ToString().Trim().ToUpper() = "CLOSE") Then
                    ddlStatus.SelectedIndex = 1
                    hdnOldStatus.Value = ddlStatus.SelectedValue
                End If

                'Dim CloseDt As DateTime? = Nothing
                'CloseDt = objRdr("fld_CloseDate")

                ''txtCloseDt.Text = IIf(CStr(objRdr("fld_CloseDate")) is Nothing , "" : CStr(objRdr("fld_CloseDate")))
                'If (Not CloseDt Is Nothing) Then
                '    txtCloseDt.Text = (CDate(CloseDt).ToString("dd/MM/yyyy"))
                'Else
                '    txtCloseDt.Text = ""
                'End If
                'txtCloseDt.Text = IIf(CStr(objRdr("fld_CloseDate")) is Nothing , "" : CStr(objRdr("fld_CloseDate")))
                'IIf(ddlCaseStatus.SelectedValue = "","0,1,2,3,4,5,6,7",ddlCaseStatus.SelectedIndex.ToString())
                ''Check whether records allow to modify depend on selected Status/Sub Status 
                'fnCheckRecordModify(hdnFieldEditF.Value, strStatusCode)
                txtCloseDt.Text = CStr(objRdr("fld_CloseDate"))
            Else
                lblErrorMessage.Text = "Record not found. Please contact your administrator."
                lblErrorMessage.Visible = True
            End If
            objRdr.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub fnCheckRecordModify(ByVal strFieldEditF As String, ByVal StrStatusCode As String)
        Try
            ''Check whether Field allow to Edit
            ''--> Case 1: Not Allow to Edit
            'If strFieldEditF = "N" Then
            '    txtFileKeyword.Enabled = False
            '    txtFileRemarks.Enabled = False
            '    butOffLoc.Visible = False
            '    ddlSecurityStatus.Enabled = False
            '    divReviewDt.Visible = False
            '    lblReviewDt.Visible = True
            '    hdnReviewDtF.Value = "N"
            '    divCloseDt.Visible = False
            '    lblCloseDt.Visible = True
            '    hdnCloseDtF.Value = "N"
            '    butReset.Visible = False


            '    ''Check whether Status=CLOSED, (not allow to cant Status)
            '    If StrStatusCode = "2" Or StrStatusCode = "3" Or StrStatusCode = "4" Or _
            '        StrStatusCode = "5" Or StrStatusCode = "6" Or StrStatusCode = "7" Or _
            '        StrStatusCode = "8" Then
            '        ddlStatus.Enabled = False
            '    End If
            'Else ''--> Case 2: Allow to Edit
            '    ''Check whether Status=CLOSED, (not allow to cant Status)
            '    If StrStatusCode = "2" Or StrStatusCode = "3" Or StrStatusCode = "4" Or _
            '        StrStatusCode = "5" Or StrStatusCode = "6" Or StrStatusCode = "7" Or _
            '        StrStatusCode = "8" Then
            '        ddlStatus.Enabled = False
            '    End If
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        
        Response.Redirect("file_Search.aspx?CallFrm=" + hdnCallFrm.Value)

    End Sub
    

    Private Sub fnChekingAutoChgLoc()
        Try
            ''Set to default
            hdnAutoChgLocF.Value = "N"

            '''Checking for auto changing location
            '''1. Sub Status is selected and Old Status <> New Status
            If (hdnOldStatus.Value <> hdnStatus.Value) Or (hdnOldOffLocName.Value <> lblOffLoc.Text) Then
                hdnAutoChgLocF.Value = "Y"
            End If
         
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub dgFileMV_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgFileMV.PageIndexChanged
        dgFileMV.CurrentPageIndex = e.NewPageIndex
        fnPopulateFileMvRec()
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        hdnAutoChgLocF.Value = "N"
        fnPopulateRecords()
        dgFileMV.CurrentPageIndex = 0
        fnPopulateFileMvRec()
    End Sub

    Private Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim intRetVal As Integer
            Dim strMsg As String

            Dim CloseDate As Object
            If (txtCloseDt.Text.ToString() <> "") Then
                CloseDate = DateTime.Parse(txtCloseDt.Text.ToString(), dateculture, System.Globalization.DateTimeStyles.AssumeLocal)
            Else
                CloseDate = DBNull.Value
            End If

            'If Trim(hdnOffLocID.Value) = "0" Then
            '    lblErrorMessage.Text = "Officer or Location cannot be empty."
            '    lblErrorMessage.Visible = True
            '    Exit Sub
            'End If

            ''Update record
            intRetVal = clsFile.fnFileModifyFile(hdnAutoChgLocF.Value, hdnFileID.Value, hdnFileDetailID.Value,
            ddlStatus.SelectedValue, CloseDate, hdnOffLocType.Value, hdnOffLocID.Value, Session("LoginID"), Session("UsrID"))

            If intRetVal > 0 Then
                strMsg = "Updated Successfully."

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "Updated : " + lblfld_FileDetail_FileRefNo.Text)

                ''Message Notification
                Dim strJavaScript As String
                Dim strURL As String
                If hdnCallFrm.Value = "Modify" Then
                    strURL = "file_EditMaster.aspx?FID=" + hdnFileID.Value
                Else
                    strURL = "file_Search.aspx"
                End If
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "document.location.href='" + strURL + "';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "Volume Updated Failed. Please contact your administrator."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    
    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        If (ddlStatus.SelectedIndex = 1) Then
            txtCloseDt.Text = mfnGetCurrentDate()
        Else
            txtCloseDt.Text = ""
        End If

        Try
            hdnStatus.Value = ddlStatus.SelectedValue
            fnChekingAutoChgLoc()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try

    End Sub

    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgFileMV.CurrentPageIndex = 0
        dgFileMV.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateFileMvRec()
    End Sub


End Class
