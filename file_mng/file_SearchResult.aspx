<%@ Register TagPrefix="tagFooter" TagName="footer" src="../commonFTS/FTSfooter_cr.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="file_SearchResult.aspx.vb" Inherits="AMS.file_SearchResult"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>file_SearchResult</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../commonFTS/FTSstyle.css" type="text/css" rel="Stylesheet">
		<script language="JavaScript" src="../Common/CommonJS.js"></script>
		<script language="javascript">

		    function ClickPrint(FileDetailIDs) {
		        if (confirm('Confirm to print RFID Tag?')) {
		            document.getElementById("hdnFileDetailIDs").value = FileDetailIDs;
		            __doPostBack('hdnclickprint', 'OnClick');
		        }
		    }

		    function fnConfirmSelectedfile(CallType) {
				var flag, CallFrm, ErrorSelect, confirmAction;
				flag = false;
								
				//Get Error Message for related action
				switch (CallType)    //--> Search, Modify, Archive, Restore, Delete, AddKIV
				{		
					case 'Delete':
						ErrorSelect = "At least one record should be selected for deletion.";
						confirmAction = "You have chosen to delete one or more file(s).\nYou cannot undo this action. Continue?";
						break

		            case 'PRPL':
		                ErrorSelect = "At least one record should be selected for RFID and Barcode printing.";
		                confirmAction = "Are you sure you want to print RFID and Barcode label for this record(s)?";
		                break

		            case 'Gen':
		                ErrorSelect = "At least one record should be selected for File to generate text Files.";
		                confirmAction = "Are you sure you want to generate text files for this record(s)?";
		                break    		
				}
				
				if (gfnCheckSelect(ErrorSelect)){
					flag = window.confirm(confirmAction);
				}
				else  {
					flag = false;
				}
				return flag;
		}
		
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Frm_FileSResult" method="post" runat="server">
			<!-- Start: header -->
            <asp:Button ID="hdnclickprint" runat="server" Visible =false ></asp:button>
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td valign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B><asp:Label ID="lblHeader" Runat="server"></asp:Label></B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td valign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<table id="Table2" height="500" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr>
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td align="center"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td vAlign="top" width="100%">
						
                        <asp:Button ID="btnRFIDPrint" Runat="server" Text="Print RFID and Barcode" />
                        <asp:Button ID="butDelFile" Runat="server" Text="Delete File(s)" />						
						<table height="400" cellSpacing="1" cellPadding="0" width="100%" align="left" border="0">
							<tr>
								<td vAlign="top" align="right" bgColor="#a3a9cc">
									<table cellSpacing="1" cellPadding="1" width="100%" border="0">
										<tr>
											<td width="20%"><asp:dropdownlist id="ddlPageSize" Runat="server" AutoPostBack="True"></asp:dropdownlist>&nbsp;&nbsp;<font color="white">per 
													page</font>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" align="center" height="400">
									<!--Datagrid for display record.-->
                                    <asp:DataGrid id="dgFile" Runat="server" AlternatingItemStyle-BackColor="#e3d9ee" datakeyfield="fld_FileID"
										PagerStyle-BackColor="#a3a9cc" PagerStyle-Position="Top" PagerStyle-Mode="NumericPages" PagerStyle-VerticalAlign="Middle" PagerStyle-HorizontalAlign="Right"
										PageSize="10" AllowPaging="True" PagerStyle-CssClass="DGpageStyle" BorderColor="#ffffff" BorderStyle="None" Width="100%" AutoGenerateColumns="False"
										AllowSorting="True" ItemStyle-Height="25" AlternatingItemStyle-Height="25" PagerStyle-Height="25">
										<headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											Height="25"></headerstyle>
										<Columns>
											<asp:boundcolumn visible="false" datafield="fld_FileID" headertext="File ID" ItemStyle-Height="10">
												<itemstyle  cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>		
                                            <asp:boundcolumn visible="false" datafield="fld_FileDetailID" headertext="File Detail ID" ItemStyle-Height="10">
												<itemstyle  cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>									
											<asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
                                            <asp:boundcolumn datafield="fld_FileDetailBarcodeID" Visible ="false" headertext="File Barcode ID" SortExpression="fld_FileDetailBarcodeID" ItemStyle-Height="10">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
                                            <asp:boundcolumn datafield="fld_FileRefNo" headertext="ELMS File Ref No" SortExpression="fld_FileRefNo" ItemStyle-Height="10">
												<itemstyle width="15%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
                                            <asp:boundcolumn datafield="fld_FileDetail_FileRefNo" SortExpression="fld_FileDetail_FileRefNo" headertext="External File Ref No" ItemStyle-Height="10">
												<itemstyle width="15%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>											
											<asp:boundcolumn datafield="fld_FileDetail_FileTitle" headertext="External File Title" SortExpression="fld_FileDetail_FileTitle" ItemStyle-Height="10">
												<itemstyle width="15%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
                                            <asp:boundcolumn datafield="fld_LocationOfficerName" SortExpression="fld_LocationOfficerName" headertext="Officer/Location" ItemStyle-Height="10">
												<itemstyle width="20%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>                                           	
											<asp:boundcolumn datafield="fld_Status" SortExpression="fld_Status" headertext="Status" ItemStyle-Height="10">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>		

                                            <asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="5%">
												<headertemplate>
													<asp:checkbox id="CheckAll" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
														runat="server" />
												</headertemplate>
												<itemtemplate>
													<center>
														<asp:checkbox id="DeleteThis" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
															runat="server" />
													</center>
												</itemtemplate>
											</asp:templatecolumn>

                                            <asp:templatecolumn HeaderText="Select" ItemStyle-Width="5%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
												<itemtemplate>
													<%# fnShowEditPrintIcon(DataBinder.Eval(Container.DataItem, "fld_FileID"), DataBinder.Eval(Container.DataItem, "fld_FileDetailID"))%>
												</itemtemplate>
											</asp:templatecolumn>		
											
										</Columns>
									</asp:DataGrid>
                                    </td>
							</tr>
						</table>
						<!--End Main Content-->
																							<asp:Button ID="btnGenerateTextFile" runat="server" 
                            Text="Generate Text File for Selected Files" />
																							<asp:Button ID="btnGenerateTextFileAndLocOff" runat="server" 
                            Text="Generate Text File for All Files and Location/Officers" />
																						</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- Start: Hidden Fields -->
            <input type="hidden" id="hdnFileDetailIDs" runat="server"> 
            <input id="hdnMasterFileRefNo" type="hidden" runat="server">
			<input id="hdnDetailFileRefNo" type="hidden" runat="server"> 
            <input id="hdnDetailFileTitle" type="hidden" runat="server">
            <input id="hdnLocOffName" type="hidden" runat="server"> 
			<input id="hdnRemarks" type="hidden" runat="server">             
            <input id="hdnSortName" type="hidden" runat="server">
			<input id="hdnSortAD" type="hidden" runat="server">
            <input id="hdnCallFrom" type="hidden" runat="server">
			<!-- End  : Hidden Fields -->
            <tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer></form>
	</body>
</HTML>
