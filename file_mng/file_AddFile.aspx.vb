#Region "Information Section"
' ****************************************************************************************************
' Description       : Create New File 
' Purpose           : Create New File Information
' Author            : See Siew
' Date              : 26/03/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class file_AddFile
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label
    Protected WithEvents ddlSubject As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlTopic As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlSTopic As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlS2Topic As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            '''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("/common/logout.aspx", False)
                Exit Sub
            End If
            '''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            ClientScript.GetPostBackEventReference(Me, String.Empty)

            If Not Page.IsPostBack Then
                hdnAddSuccessF.Value = "N"
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")

                fnCheckAccessRight()
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub fnCheckAccessRight()
        Try
            If (InStr(Session("AR"), "FileMng|addFile|add") = 0) Then
                butSubmit.Enabled = False
                butReset.Enabled = False
            Else
                butSubmit.Enabled = True
                butReset.Enabled = True
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        txtFileRef.Text = ""
        txtFileTitle.Text = ""
        txtfileDetailRef.Text = ""
        txtRemarks.Text = ""
    End Sub

    Private Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim intFileID As Integer
            Dim strFileRef, strFileDetailRef, strFileTile, strRemarks As String
            Dim strFileNum As String
            Dim intRetVal As Integer
            Dim blnFileDup As Boolean = False
            Dim strMsg As String
            Dim intfiledetailRef As Integer

            ''get Fields Value            
            strFileRef = txtFileRef.Text
            strFileDetailRef = txtfileDetailRef.Text
            strFileTile = txtFileTitle.Text
            strRemarks = txtRemarks.Text

            Dim dt As DataTable = clsFile.fnFile_DetailGetDataByfld_FileDetail_FileRefNoAndfld_FileRefNo(txtFileRef.Text, txtfileDetailRef.Text.ToString().Trim()).Tables(0)
            If (dt.Rows.Count > 0) Then
                lblErrorMessage.Text = "You have enter duplicate ELMS File Reference No and External File Reference No in the File Management System. Please check it!"
                lblErrorMessage.Visible = True
                txtfileDetailRef.Focus()
                hdnDuplicateStatus.Value = "1"
            Else
                lblErrorMessage.Text = ""
                lblErrorMessage.Visible = False
                hdnDuplicateStatus.Value = "0"
            End If

            If Not blnFileDup Then ''not duplicate record
                ''insert record
                intRetVal = clsFile.fnFileAddFile( _
                                strFileRef.Replace("\n", " "), strFileDetailRef.Replace("\n", " "), strFileTile.Replace("\n", " "), strRemarks, _
                                    Session("LoginID"), Session("UsrID"), intfiledetailRef)

                If intRetVal > 0 Then
                    'strMsg = "New File Add Successfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "New File created : " + txtFileRef.Text)

                    hdnFileDetailRefs.Value = intfiledetailRef.ToString() + "^"
                    ''Get Data for 

                    hdnAddSuccessF.Value = "Y"
                    'hdnFBarcode.Value = mfnGetBarcode("F", intRetVal)
                    hdnFBarcode.Value = intfiledetailRef
                    hdnFRef.Value = Replace(Trim(strFileRef), " ", "^")
                    hdnFTitle.Value = strFileTile
                    hdnFRefMsg.Value = strFileRef
                    hdnFDetailRef.Value = strFileDetailRef
                    hdnRemarks.Value = txtRemarks.Text

                    ''Message Notification
                    'Dim strJavaScript As String
                    'strJavaScript = "<script language = 'Javascript'>" & _
                    '                "alert('" + strMsg + "');" & _
                    '                "</script>"
                    'Response.Write(strJavaScript)

                    ''Reset the "Creation File"
                    butReset_Click(Nothing, Nothing)
                Else
                    strMsg = "New File Add Failed. Please contact your administrator."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If

            Else  ''duplicate record
                lblErrorMessage.Text = "File Number already exist. Please choose another File Number."
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub txtfileDetailRef_TextChanged(sender As Object, e As EventArgs) Handles txtfileDetailRef.TextChanged
        Dim dt As DataTable = clsFile.fnFile_DetailGetDataByfld_FileDetail_FileRefNoAndfld_FileRefNo(txtFileRef.Text, txtfileDetailRef.Text.ToString().Trim()).Tables(0)
        If (dt.Rows.Count > 0) Then
            lblErrorMessage.Text = "You have enter duplicate ELMS File Reference No and External File Reference No in the File Management System. Please check it!"
            lblErrorMessage.Visible = True
            hdnDuplicateStatus.Value = "1"
            txtfileDetailRef.Focus()
        Else
            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False
            hdnDuplicateStatus.Value = "0"
            txtFileTitle.Focus()
        End If
        
    End Sub

    Private Sub hdnclickprint_Click(sender As Object, e As System.EventArgs) Handles hdnclickprint.Click
        Try
            Dim strFileDetailIDs As String = hdnFileDetailRefs.Value
            Dim strFileNameLabel As String = ""

            If Trim(strFileDetailIDs) <> "" Then

                Session("FileDetailIDs" + Session("UsrID").ToString()) = strFileDetailIDs
                Session("ModuleName" + Session("UsrID").ToString()) = "FTS"
                Session("File" + Session("UsrID").ToString()) = "1"
                Session("AddFile" + Session("UsrID").ToString()) = "1"
                Session("LocationOrOfficer" + Session("UsrID").ToString()) = ""

                ClientScript.RegisterStartupScript(Me.[GetType](), "myalert", "window.open('../common/PrintPopUp.aspx','_blank','left=400, top=200, width=400, height=200, menubar=no, resizable=no, status=no, titlebar=no, toolbar=no');", True)

                'clsRFIDLabel.fnCreateRFIDLabelFile(strFileDetailIDs, Session("UsrID"), _
                '                                   Server.MapPath(gRFIDLabelPath), strFileNameLabel)

                ' ''Insert Audit Trail
                'clsCommon.fnAuditInsertRec(Session("UsrID"), strFileDetailIDs, "RFID passive label printed.")

                ''Dim strJavaScript As String
                'Dim strJavaScript As String = "<script language = 'Javascript'>" & _
                '                "alert('" + "RFID Tag print succesfully." + "');" & _
                '                "</script>"
                'Response.Write(strJavaScript)

            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub txtFileRef_TextChanged(sender As Object, e As EventArgs) Handles txtFileRef.TextChanged
        Dim dt As DataTable = clsFile.fnFile_DetailGetDataByfld_FileDetail_FileRefNoAndfld_FileRefNo(txtFileRef.Text, txtfileDetailRef.Text.ToString().Trim()).Tables(0)
        If (dt.Rows.Count > 0) Then
            lblErrorMessage.Text = "You have enter duplicate ELMS File Reference No and External File Reference No in the File Management System. Please check it!"
            lblErrorMessage.Visible = True
            hdnDuplicateStatus.Value = "1"
            txtFileRef.Focus()
        Else
            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False
            hdnDuplicateStatus.Value = "0"
            txtfileDetailRef.Focus()
        End If
    End Sub
End Class
