#Region "Information Section"
' ****************************************************************************************************
' Description       : Search Result File --> for Modify  
' Purpose           : Search Result File Information
' Author            : WIN
' Date              : 16/05/2016
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class file_SearchResult
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            '''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("/common/logout.aspx", False)
                Exit Sub
            End If
            '''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then

                Me.butDelFile.Attributes.Add("OnClick", "return fnConfirmSelectedfile('Delete')")
                Me.btnRFIDPrint.Attributes.Add("OnClick", "return fnConfirmSelectedfile('PRPL')")
                Me.btnGenerateTextFile.Attributes.Add("OnClick", "return fnConfirmSelectedfile('Gen')")

                ''Get Hidden Data
                hdnCallFrom.Value = clsEncryptDecrypt.DecryptText(Request("CallFrm"))
                hdnMasterFileRefNo.Value = clsEncryptDecrypt.DecryptText(Request("strMasterFileRefNo")) '--> Search, Modify, Archive, Restore, Delete, AddKIV
                hdnDetailFileRefNo.Value = clsEncryptDecrypt.DecryptText(Request("strDetailFileRefNo"))
                hdnDetailFileTitle.Value = clsEncryptDecrypt.DecryptText(Request("strDetailFileTitle"))
                hdnLocOffName.Value = clsEncryptDecrypt.DecryptText(Request("strLocOfficerName"))
                hdnRemarks.Value = clsEncryptDecrypt.DecryptText(Request("strRemarks"))

                ClientScript.GetPostBackEventReference(Me, String.Empty)

                ''Get Header
                If hdnCallFrom.Value = "Search" Then
                    lblHeader.Text = "File Management : Search File"
                ElseIf hdnCallFrom.Value = "Delete" Then
                    lblHeader.Text = "File Management : Delete File"
                End If

                ''default Sorting
                hdnSortName.Value = "fld_FileRefNo"
                hdnSortAD.Value = "ASC"

                ''Populate Pages
                fnPopulatePagesInDDLForGenerateTextFile(ddlPageSize)

                ''Checking for access right
                fnCheckAccessRight()

                ''default page for page 1
                dgFile.CurrentPageIndex = 0

                ''Display Record
                fnPopulateRecords()
            End If

            ''For Print Label
            'If hdnPrintF.Value = "Y" Then
            '    hdnPrintF.Value = "N"
            '    'lblErrorMessage.Text = "Print Label is going here... Coming soon..."
            '    'lblErrorMessage.Visible = True
            'End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub fnPopulateRecords()
        Try
            ''' Sort Header Display
            fnSortHeaderDisplay()

            dgFile.DataSource = clsFile.fnFileSearchRecord(hdnMasterFileRefNo.Value, hdnDetailFileRefNo.Value,
                                                            hdnDetailFileTitle.Value, hdnLocOffName.Value, hdnRemarks.Value,
                                                           hdnSortName.Value, hdnSortAD.Value)
            dgFile.DataBind()
            If Not dgFile.Items.Count > 0 Then '''Not Department Records found
                dgFile.Visible = False
                ddlPageSize.Enabled = False
                lblErrorMessage.Text = "There is no file record(s) found."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            'Throw ex
        End Try
    End Sub

    Private Sub fnCheckAccessRight()
        Try
            Select Case hdnCallFrom.Value '--> Search, Modify, Archive, Restore, Delete, AddKIV
                Case "Search"
                    dgFile.Columns(10).Visible = True
                    butDelFile.Visible = False
                    btnGenerateTextFile.Visible = True
                    btnRFIDPrint.Visible = True
                    If (InStr(Session("AR"), "FileMng|searchFile|print") = 0) Then
                        btnRFIDPrint.Enabled = False
                    Else
                        btnRFIDPrint.Enabled = True
                    End If

                    If (InStr(Session("AR"), "FileMng|searchFile|generatetextfile") = 0) Then
                        btnGenerateTextFile.Enabled = False
                    Else
                        btnGenerateTextFile.Enabled = True
                    End If

                    If (InStr(Session("AR"), "FileMng|searchFile|generatetextfileLoc") = 0) Then
                        btnGenerateTextFileAndLocOff.Enabled = False
                    Else
                        btnGenerateTextFileAndLocOff.Enabled = True
                    End If

                Case "Delete"
                    btnGenerateTextFile.Visible = False
                    dgFile.Columns(10).Visible = False
                    btnGenerateTextFileAndLocOff.Visible = False
                    btnRFIDPrint.Visible = False
                    butDelFile.Visible = True
                    If (InStr(Session("AR"), "FileMng|delFile|delete") = 0) Then
                        butDelFile.Enabled = False
                    Else
                        butDelFile.Enabled = True
                    End If
            End Select

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnShowEditPrintIcon(ByVal strFileID As String, _
                                        ByVal strFileDetailID As String)
        Try
            Dim strRetVal As String = ""

            '' Check For Edit File Access Right
            If (InStr(Session("AR"), "FileMng|searchFile|edit") > 0) Then
                strRetVal = "<a id=hlEditFileV href='File_EditFile.aspx?CallFrm=" + clsEncryptDecrypt.EncryptText(hdnCallFrom.Value) + "&strFileID=" + clsEncryptDecrypt.EncryptText(strFileID) + "&strFileDetailID=" + clsEncryptDecrypt.EncryptText(strFileDetailID) + "' target=_self >" & _
                             "<img id=imgUpdate src=../images/update.gif alt='Click for update Record.' border=0 >" & _
                             "</a>"
            End If

            '' Check For Edit File Access Right
            'If (InStr(Session("AR"), "FileMng|searchFile|print") > 0) Then
            '    strRetVal += "<a id=hlEditPrintALabel href=javascript:gfnprint('" + strFileDetailID + "')>" & _
            '                     "<img id=imgUpdate src=../images/print.gif alt='Click for print label.' border=0 >" & _
            '                     "</a>"
            'End If

            '' Check For Print Asset Access Right
            If (InStr(Session("AR"), "FileMng|searchFile|print") > 0) Then
                strRetVal += "<a id=hlEditPrintALabel href=# onclick=javascript:ClickPrint('" + strFileDetailID + "^')>" & _
                             "<img id=imgUpdate src=../images/print.gif alt='Click for print label.' border=0 >" & _
                             "</a>"
            End If

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Function fnGetSelectedFileRec(ByRef strSelectedFileID As String) As String
        Try
            Dim strSelectedFileDetailID As String

            Dim GridItem As DataGridItem
            Dim chkSelected As CheckBox
            For Each GridItem In dgFile.Items
                chkSelected = CType(GridItem.Cells(9).FindControl("DeleteThis"), CheckBox)
                If chkSelected.Checked Then
                    strSelectedFileDetailID += GridItem.Cells(1).Text & "^"
                    strSelectedFileID += GridItem.Cells(0).Text & ","
                End If
            Next

            Return strSelectedFileDetailID
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgFile.CurrentPageIndex = 0
        dgFile.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Private Sub dgFile_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgFile.PageIndexChanged
        dgFile.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub dgFile_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgFile.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

    Public Function fnSortHeaderDisplay() As Boolean
       
        Dim intSortIndex As Integer
        Dim strSortHeader As String

        ''Set Default Header
        dgFile.Columns(3).HeaderText = "File Barcode ID"
        dgFile.Columns(4).HeaderText = "ELMS File Ref No"
        dgFile.Columns(5).HeaderText = "External File Ref No"
        dgFile.Columns(6).HeaderText = "External File Title"
        dgFile.Columns(7).HeaderText = "Officer/Location"
        dgFile.Columns(8).HeaderText = "Status"
       

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fld_FileDetailBarcodeID"
                intSortIndex = 3
                strSortHeader = "File Barcode ID"
            Case "fld_FileRefNo"
                intSortIndex = 4
                strSortHeader = "ELMS File Ref No"
            Case "fld_FileDetail_FileRefNo"
                intSortIndex = 5
                strSortHeader = "External File Ref No"
            Case "fld_FileDetail_FileTitle"
                intSortIndex = 6
                strSortHeader = "External File Title"
            Case "LocOrofficer"
                intSortIndex = 7
                strSortHeader = "Officer/Location"
            Case "fld_Status"
                intSortIndex = 8
                strSortHeader = "Status"
        End Select

        'If hdnSortName.Value = "fld_FileRefNo" Then
        '    If hdnSortAD.Value = "ASC" Then
        '        dgFile.Columns(3).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        '        dgFile.Columns(4).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        '    Else
        '        dgFile.Columns(3).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        '        dgFile.Columns(4).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        '    End If
        'Else
        If hdnSortAD.Value = "ASC" Then
            dgFile.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgFile.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
        'End If
    End Function

    Protected Sub butDelFile_Click(sender As Object, e As EventArgs) Handles butDelFile.Click
        Try
            Dim intRetVal As Integer
            Dim strMsg As String
            Dim strSelectedFileID As String
            Dim strSelectedFileDetailID As String

            strSelectedFileDetailID = fnGetSelectedFileRec(strSelectedFileID)
            If strSelectedFileID <> "" Then strSelectedFileID = strSelectedFileID.Substring(0, strSelectedFileID.Length - 1)

            ''Checking whether File Volume selected for Delete
            If strSelectedFileDetailID <> "" Then
                ''Delete Funtion Going Here
                intRetVal = clsFile.fnFileDeleteRecords(strSelectedFileID, strSelectedFileDetailID, Session("UsrID"))

                If intRetVal > 0 Then
                    strMsg = "Record(s) Delete Successfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "Record(s) Deleted : " + strSelectedFileID)

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)

                    ''Populate Record
                    fnPopulateRecords()
                Else
                    strMsg = "Record(s) Delete Failed."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If

            Else
                lblErrorMessage.Text = "At least one record should be selected for Delete."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub btnRFIDPrint_Click(sender As Object, e As EventArgs) Handles btnRFIDPrint.Click

        Try
            Dim strFileDetailIDs As String = ""
            Dim strFileNameLabel As String = ""

            ''Get Selected Asset IDs
            strFileDetailIDs = fnGetSelectedFileRec()

            If Trim(strFileDetailIDs) <> "" Then

                Session("FileDetailIDs" + Session("UsrID").ToString()) = strFileDetailIDs
                Session("ModuleName" + Session("UsrID").ToString()) = "FTS"
                Session("File" + Session("UsrID").ToString()) = "1"
                Session("AddFile" + Session("UsrID").ToString()) = ""
                Session("LocationOrOfficer" + Session("UsrID").ToString()) = ""

                ClientScript.RegisterStartupScript(Me.[GetType](), "myalert", "window.open('../common/PrintPopUp.aspx','_blank','left=400, top=200, width=400, height=200, menubar=no, resizable=no, status=no, titlebar=no, toolbar=no');", True)


                'clsRFIDLabel.fnCreateRFIDLabelFile(strFileDetailIDs, Session("UsrID"), _
                '                                   Server.MapPath(gRFIDLabelPath), strFileNameLabel)

                'clsCommon.fnAuditInsertRec(Session("UsrID"), strFileDetailIDs, "RFID passive label printed.")

                'Dim strJavaScript As String
                'strJavaScript = "<script language = 'Javascript'>" & _
                '                "alert('" + "RFID Tag print succesfully." + "');" & _
                '                "</script>"
                'Response.Write(strJavaScript)

            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
        
    End Sub

    Private Function fnGetSelectedFileRec() As String
        Try
            Dim strSelectedRec As String = ""

            ''*** Get record that selected 
            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox
            For Each GridItem In dgFile.Items
                chkSelectedRec = CType(GridItem.Cells(8).FindControl("DeleteThis"), CheckBox)
                If chkSelectedRec.Checked Then
                    strSelectedRec += GridItem.Cells(1).Text & "^"
                End If
            Next

            Return strSelectedRec
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub btnGenerateTextFile_Click(sender As Object, e As EventArgs) Handles btnGenerateTextFile.Click
        Dim strFileName4FTS As String = "IN_" & Year(Date.Today).ToString & Month(Date.Today).ToString.PadLeft(2, "0") & Day(Date.Today).ToString.PadLeft(2, "0") & Hour(Date.Now).ToString.PadLeft(2, "0") & Minute(Date.Now).ToString.PadLeft(2, "0") & Second(Date.Now).ToString.PadLeft(2, "0") & ".txt"
        fnGenerateFileForFTS(strFileName4FTS)
    End Sub

    Sub DataTable2CSV(ByVal table As DataTable, ByVal filename As String, _
    ByVal sepChar As String)
        Dim writer As System.IO.StreamWriter
        Try
            writer = New System.IO.StreamWriter(filename)

            ' first write a line with the columns name
            Dim sep As String = ""
            Dim builder As New System.Text.StringBuilder
            For Each col As DataColumn In table.Columns
                builder.Append(sep).Append(col.ColumnName)
                sep = sepChar
            Next
            writer.WriteLine(builder.ToString())

            ' then write all the rows
            For Each row As DataRow In table.Rows
                sep = ""
                builder = New System.Text.StringBuilder

                For Each col As DataColumn In table.Columns
                    builder.Append(sep).Append(row(col.ColumnName))
                    sep = sepChar
                Next
                writer.WriteLine(builder.ToString())
            Next
        Finally
            If Not writer Is Nothing Then writer.Close()
        End Try
    End Sub

    Protected Sub fnGenerateFileForFTS(ByVal strFileName4FTS As String)
        Try
            '********** Beginning write text file ***********
            Dim FileFullPath As String = ""
            FileFullPath = Server.MapPath("..\TempFile\FTMInputFile") & "\" & strFileName4FTS
            
            Dim strFileDetailIDs As String = ""
            strFileDetailIDs = fnGetSelectedFileRec()

            Dim dt As DataTable = clsFile.fnFile_File_GetRecordGenerateTxtFile(strFileDetailIDs).Tables(0)
            If (dt.Rows.Count > 0) Then
                'DataTable2CSV(dt, FileFullPath, ",")
                DataTable2CSV(dt, FileFullPath, "`~`")
            End If

            clsCommon.fnAuditInsertRec(Session("UsrID"), "To generate text file for " + strFileDetailIDs.Replace("^", "") + " files with location/officer.")
            clsCommon.fnAuditInsertRec(Session("UsrID"), "To generate text file for all files with location/officer.")

            ClientScript.RegisterStartupScript(Me.GetType(), "myalert", "alert('Text File Successfully generated.');", True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub fnGenerateFileAndLocationOfficerForFTS()
        Try
            '********** Beginning write text file ***********
            Dim FileFullPath As String = ""
            FileFullPath = Server.MapPath("..\TempFile\FMSFile") & "\" & "File.txt"
            Dim dtFile As DataTable = clsFile.fnFileDetail_GetAllRecord().Tables(0)
            If (dtFile.Rows.Count > 0) Then
                'DataTable2CSV(dtFile, FileFullPath, ",")
                DataTable2CSV(dtFile, FileFullPath, Environment.NewLine)
                'DataTable2CSV(dtFile, FileFullPath, "`~`")
            End If

            FileFullPath = Server.MapPath("..\TempFile\FMSFile") & "\" & "LocationOfficer.txt"
            Dim dtLocOfficer As DataTable = clsLocationOfficer.fnGetAllLocationOfficer().Tables(0)
            If (dtLocOfficer.Rows.Count > 0) Then
                'DataTable2CSV(dtLocOfficer, FileFullPath, ",")
                'DataTable2CSV(dtLocOfficer, FileFullPath, "`~`")
                DataTable2CSV(dtLocOfficer, FileFullPath, Environment.NewLine)
            End If

            clsCommon.fnAuditInsertRec(Session("UsrID"), "To generate text file for all files with location/officers.")

            ClientScript.RegisterStartupScript(Me.GetType(), "myalert", "alert('Text file for all Files and Location/Officers Successfully generated.');", True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub hdnclickprint_Click(sender As Object, e As System.EventArgs) Handles hdnclickprint.Click
        Try
            Dim strFileDetailIDs As String = hdnFileDetailIDs.Value
            Dim strFileNameLabel As String = ""

            If Trim(strFileDetailIDs) <> "" Then

                Session("FileDetailIDs" + Session("UsrID").ToString()) = strFileDetailIDs
                Session("ModuleName" + Session("UsrID").ToString()) = "FTS"
                Session("File" + Session("UsrID").ToString()) = "1"
                Session("AddFile" + Session("UsrID").ToString()) = ""
                Session("LocationOrOfficer" + Session("UsrID").ToString()) = ""

                ClientScript.RegisterStartupScript(Me.[GetType](), "myalert", "window.open('../common/PrintPopUp.aspx','_blank','left=400, top=200, width=400, height=200, menubar=no, resizable=no, status=no, titlebar=no, toolbar=no');", True)

                'clsRFIDLabel.fnCreateRFIDLabelFile(strFileDetailIDs, Session("UsrID"), _
                '                                   Server.MapPath(gRFIDLabelPath), strFileNameLabel)

                ' ''Insert Audit Trail
                'clsCommon.fnAuditInsertRec(Session("UsrID"), strFileDetailIDs, "RFID passive label printed.")

                'Dim strJavaScript As String
                'strJavaScript = "<script language = 'Javascript'>" & _
                '                "alert('" + "RFID Tag print succesfully." + "');" & _
                '                "</script>"
                'Response.Write(strJavaScript)

            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    
    Protected Sub btnGenerateTextFileAndLocOff_Click(sender As Object, e As EventArgs) Handles btnGenerateTextFileAndLocOff.Click
        fnGenerateFileAndLocationOfficerForFTS()
    End Sub
End Class
