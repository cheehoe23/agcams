#Region "Information Section"
' ****************************************************************************************************
' Description       : Search File --> for Modify File, Archive File, Restore File, Delete File, KIV File 
' Purpose           : Search File Information
' Author            : See Siew
' Date              : 27/03/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class file_Search
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label
    Protected WithEvents hdnCurrentDate As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        'Put user code to initialize the page here
        Try
            '''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("/common/logout.aspx", False)
                Exit Sub
            End If
            '''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                'Me.txtKeywordTitle.Attributes.Add("onkeypress", "return fnEnterProb()")
                Me.ButSearch.Attributes.Add("OnClick", "return chkFrm()")

                ''may call from --> Search, Modify, Archive, Restore, Delete, AddKIV
                hdnCallFrom.Value = Request("CallFrm")

                ''Get Header
                If hdnCallFrom.Value = "Search" Then
                    lblHeader.Text = "File Management : Search File"
                ElseIf hdnCallFrom.Value = "Delete" Then
                    lblHeader.Text = "File Management : Delete File"
                End If

                fnCheckAccessRight()
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        txtMasterFileRefNo.Text = ""
        txtDetailFileRefNo.Text = ""

        txtDetailFileTitle.Text = ""

    End Sub

    Protected Sub fnCheckAccessRight()
        Try

            If (InStr(Session("AR"), "FileMng|searchFile|Search") = 0) Then
                ButSearch.Enabled = False
                butReset.Enabled = False
            Else
                ButSearch.Enabled = True
                butReset.Enabled = True
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ButSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButSearch.Click
        Try
            Dim strMasterFileRefNo, strDetailFileRefNo, strDetailFileTitle, strLocOfficerName, strRemarks As String
            Dim dsFile As New DataSet
            Dim strURL As String

            ''Get Value 

            strMasterFileRefNo = txtMasterFileRefNo.Text.Trim()
            strMasterFileRefNo = strMasterFileRefNo.Replace("'", "''")
            strMasterFileRefNo = strMasterFileRefNo.Replace("[", "[[]")
            strMasterFileRefNo = strMasterFileRefNo.Replace("%", "[%]")
            strMasterFileRefNo = strMasterFileRefNo.Replace("_", "[_]")

            strDetailFileRefNo = txtDetailFileRefNo.Text.Trim()
            strDetailFileRefNo = strDetailFileRefNo.Replace("'", "''")
            strDetailFileRefNo = strDetailFileRefNo.Replace("[", "[[]")
            strDetailFileRefNo = strDetailFileRefNo.Replace("%", "[%]")
            strDetailFileRefNo = strDetailFileRefNo.Replace("_", "[_]")

            strDetailFileTitle = txtDetailFileTitle.Text.Trim()
            strDetailFileTitle = strDetailFileTitle.Replace("'", "''")
            strDetailFileTitle = strDetailFileTitle.Replace("[", "[[]")
            strDetailFileTitle = strDetailFileTitle.Replace("%", "[%]")
            strDetailFileTitle = strDetailFileTitle.Replace("_", "[_]")

            strRemarks = txtRemarks.Text.Trim()
            strRemarks = strRemarks.Replace("'", "''")
            strRemarks = strRemarks.Replace("[", "[[]")
            strRemarks = strRemarks.Replace("%", "[%]")
            strRemarks = strRemarks.Replace("_", "[_]")

            strLocOfficerName = txtLocationOfficerName.Text.Trim()
            strLocOfficerName = strLocOfficerName.Replace("'", "''")
            strLocOfficerName = strLocOfficerName.Replace("[", "[[]")
            strLocOfficerName = strLocOfficerName.Replace("%", "[%]")
            strLocOfficerName = strLocOfficerName.Replace("_", "[_]")

            dsFile = clsFile.fnFileSearchRecord(strMasterFileRefNo, strDetailFileRefNo, strDetailFileTitle, strLocOfficerName, strRemarks, "fld_FileRefNo", "ASC")

            If dsFile.Tables(0).Rows.Count > 0 Then

                If (hdnCallFrom.Value = "Search") Then
                    strURL = "/file_mng/file_SearchResult.aspx?CallFrm=" + clsEncryptDecrypt.EncryptText(hdnCallFrom.Value) + "&strMasterFileRefNo=" + clsEncryptDecrypt.EncryptText(strMasterFileRefNo) + "&strDetailFileRefNo=" + clsEncryptDecrypt.EncryptText(strDetailFileRefNo) + "&strDetailFileTitle=" + clsEncryptDecrypt.EncryptText(strDetailFileTitle) + "&strLocOfficerName=" + clsEncryptDecrypt.EncryptText(strLocOfficerName) + "&strRemarks=" + clsEncryptDecrypt.EncryptText(strRemarks)
                ElseIf (hdnCallFrom.Value = "Delete") Then
                    strURL = "/file_mng/file_SearchResult.aspx?CallFrm=" + clsEncryptDecrypt.EncryptText(hdnCallFrom.Value) + "&strMasterFileRefNo=" + clsEncryptDecrypt.EncryptText(strMasterFileRefNo) + "&strDetailFileRefNo=" + clsEncryptDecrypt.EncryptText(strDetailFileRefNo) + "&strDetailFileTitle=" + clsEncryptDecrypt.EncryptText(strDetailFileTitle) + "&strLocOfficerName=" + clsEncryptDecrypt.EncryptText(strLocOfficerName) + "&strRemarks=" + clsEncryptDecrypt.EncryptText(strRemarks)
                Else
                    strURL = "/file_mng/file_SearchResult.aspx?CallFrm=Search&strMasterFileRefNo=" + clsEncryptDecrypt.EncryptText(strMasterFileRefNo) + "&strDetailFileRefNo=" + clsEncryptDecrypt.EncryptText(strDetailFileRefNo) + "&strDetailFileTitle=" + clsEncryptDecrypt.EncryptText(strDetailFileTitle) + "&strLocOfficerName=" + clsEncryptDecrypt.EncryptText(strLocOfficerName) + "&strRemarks=" + clsEncryptDecrypt.EncryptText(strRemarks)
                End If

                clsCommon.fnAuditInsertRec(Session("UsrID"), "To Search " + strDetailFileRefNo + " reference No.")

                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "document.location.href='" + strURL + "';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                lblErrorMessage.Visible = True
                lblErrorMessage.Text = "Record(s) not found."
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class
