#Region "Information Section"
' ****************************************************************************************************
' Description       : Volume Modification Screen  
' Purpose           : Volume Modification Information
' Author            : See Siew
' Date              : 09/04/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class File_EditVolume
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim dateculture As IFormatProvider = New System.Globalization.CultureInfo("fr-FR", True)

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        Try
            '''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("/common/logout.aspx", False)
                Exit Sub
            End If
            '''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")
                butOffLoc.Attributes.Add("OnClick", "return mfnOpenOfficerLoc()")

                ''Get Hidden Data
                hdnCallFrm.Value = clsEncryptDecrypt.DecryptText(Request("CallFrm"))
                hdnFileID.Value = clsEncryptDecrypt.DecryptText(Request("strFileID"))
                hdnFileDetailID.Value = clsEncryptDecrypt.DecryptText(Request("strFileDetailID"))
                hdnCurrentDate.Value = mfnGetCurrentDate()
                hdnAutoChgLocF.Value = "N"

                ''Display File Record
                fnPopulateRecords()

                fnPopulatePagesInDDL(ddlPageSize)
                dgFileMV.CurrentPageIndex = 0

                ''Display File Movement
                fnPopulateFileMvRec()
            End If

            If hdnChgOffLocF.Value = "Y" Then
                dgFileMV.Visible = True
                hdnChgOffLocF.Value = "N"
                lblOffLoc.Text = hdnOffLocName.Value
                fnChekingAutoChgLoc()
                fnPopulateFileMvRec()
               
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub fnPopulateFileMvRec()
        Try

            dgFileMV.CurrentPageIndex = 0

            'Dim dt As DataTable = clsFile.fnFileMvGetRecords(hdnFileDetailID.Value, hdnOffLocID.Value, hdnLocSubID.Value).Tables(0)
            Dim dt As DataTable = clsFile.fnFileMvGetRecords(hdnFileDetailID.Value).Tables(0)

            dgFileMV.DataSource = dt
            dgFileMV.DataBind()

            'If (hdnAutoChgLocF.Value <> "Y") Then
            If Not dgFileMV.Items.Count > 0 Then
                dgFileMV.Visible = False
                lblErrorMessage.Text = "There is no file movement record(s) found. Please contact your administrator."
                lblErrorMessage.Visible = True
            End If
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub fnPopulateCtrl()
        Dim drStatus As SqlDataReader
        Try
            'Populate Security Status 
            'fnPopulateSecurityStatusInDDL(ddlSecurityStatus)

            ''Get Status 
            drStatus = clsStatus.fnStatusGetRecForDDLOrByPCode("", "0", "0")
            fnPopulateDropDownList(drStatus, ddlStatus, "fld_StatusID", "fld_StatusName", False)
            drStatus.Close()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(drStatus)
            Throw ex
        End Try
    End Sub

    Private Sub fnPopulateRecords()
        Dim objRdr As SqlDataReader
        Try
            Dim strStatusCode As String

            'Get File Master Data
            objRdr = clsFile.fnFileGetRecordForEdit(hdnFileID.Value, hdnFileDetailID.Value)
            If Not objRdr Is Nothing Then
                objRdr.Read()
                lblFileRefNo.Text = CStr(objRdr("fld_FileRefNo").ToString())
                hdnFileID.Value = CStr(objRdr("fld_FileID").ToString())
                hdnFileDetailID.Value = CStr(objRdr("fld_FileDetailID").ToString())

                lblfld_FileDetail_FileRefNo.Text = CStr(objRdr("fld_FileDetail_FileRefNo").ToString())
                lblfld_FileDetail_FileTitle.Text = CStr(objRdr("fld_FileDetail_FileTitle").ToString())

                If (hdnAutoChgLocF.Value <> "Y") Then
                    hdnOldOffLocName.Value = CStr(objRdr("fld_LocationOfficerName").ToString())
                    lblOffLoc.Text = CStr(objRdr("fld_LocationOfficerName").ToString())
                End If

                txtRemarks.Text = objRdr("fld_Remarks").ToString()

                hdnOffLocID.Value = CStr(objRdr("fld_LocationOfficerID").ToString())

                If (objRdr("fld_Status").ToString().Trim().ToUpper() = "OPEN") Then
                    ddlStatus.SelectedIndex = 0
                    hdnOldStatus.Value = ddlStatus.SelectedValue
                ElseIf (objRdr("fld_Status").ToString().Trim().ToUpper() = "CLOSE") Then
                    ddlStatus.SelectedIndex = 1
                    hdnOldStatus.Value = ddlStatus.SelectedValue
                End If

                txtCloseDt.Text = CStr(objRdr("fld_CloseDate").ToString())
            Else
                lblErrorMessage.Text = "Record not found. Please contact your administrator."
                lblErrorMessage.Visible = True
            End If
            objRdr.Close()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        
        Response.Redirect("file_Search.aspx?CallFrm=" + hdnCallFrm.Value)

    End Sub
    

    Private Sub fnChekingAutoChgLoc()
        Try
            ''Set to default
            hdnAutoChgLocF.Value = "N"

            '''Checking for auto changing location
            '''1. Sub Status is selected and Old Status <> New Status
            If (hdnOldStatus.Value <> hdnStatus.Value) Or (hdnOldOffLocName.Value <> lblOffLoc.Text) Then
                hdnAutoChgLocF.Value = "Y"
            End If
         
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub dgFileMV_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgFileMV.PageIndexChanged
        dgFileMV.CurrentPageIndex = e.NewPageIndex
        fnPopulateFileMvRec()
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        hdnAutoChgLocF.Value = "N"
        fnPopulateRecords()
        dgFileMV.CurrentPageIndex = 0
        fnPopulateFileMvRec()
    End Sub

    Private Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim intRetVal As Integer
            Dim strMsg As String

            Dim CloseDate As Object
            If (txtCloseDt.Text.ToString() <> "") Then
                CloseDate = DateTime.Parse(txtCloseDt.Text.ToString(), dateculture, System.Globalization.DateTimeStyles.AssumeLocal)
            Else
                CloseDate = DBNull.Value
            End If

            'If Trim(hdnOffLocID.Value) = "0" Then
            '    lblErrorMessage.Text = "Officer or Location cannot be empty."
            '    lblErrorMessage.Visible = True
            '    Exit Sub
            'End If

            ''Update record
            intRetVal = clsFile.fnFileModifyFile(hdnAutoChgLocF.Value, hdnFileID.Value, hdnFileDetailID.Value,
            ddlStatus.SelectedValue, CloseDate, txtRemarks.Text, Session("LoginID"), Session("UsrID"))

            If intRetVal > 0 Then
                strMsg = "Updated Successfully."

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "Updated : " + lblfld_FileDetail_FileRefNo.Text)

                ''Message Notification
                Dim strJavaScript As String
                Dim strURL As String
                If hdnCallFrm.Value = "Modify" Then
                    strURL = "file_EditMaster.aspx?FID=" + hdnFileID.Value
                Else
                    strURL = "file_Search.aspx?CallFrm=Search"
                End If
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "document.location.href='" + strURL + "';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "File Updated Failed. Please contact your administrator."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    
    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        If (ddlStatus.SelectedIndex = 1) Then
            txtCloseDt.Text = mfnGetCurrentDate()
        Else
            txtCloseDt.Text = ""
        End If

        Try
            hdnStatus.Value = ddlStatus.SelectedValue
            fnChekingAutoChgLoc()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try

    End Sub

    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgFileMV.CurrentPageIndex = 0
        dgFileMV.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateFileMvRec()
    End Sub

    Protected Sub fnCheckAccessRight()
        Try

            If (hdnCallFrm.Value = "Delete") Then
                ''**Remove Asset Type, if access right for creation particular asset types are not given
                If (InStr(Session("AR"), "FileMng|delFile|Delete") = 0) Then
                    butSubmit.Enabled = False
                    butReset.Enabled = False
                Else
                    butSubmit.Enabled = True
                    butReset.Enabled = True
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

End Class
