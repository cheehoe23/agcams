<%@ Register TagPrefix="tagFooter" TagName="footer" src="~/commonFTS/FTSfooter_cr.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="file_Search.aspx.vb" Inherits="AMS.file_Search"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>File Search</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../commonFTS/FTSstyle.css" type="text/css" rel="Stylesheet">
		<script language="JavaScript" src="../Common/CommonJS.js"></script>
		<script language="javascript">
		function fnEnterProb(){
			//the purpose of this function is to allow the enter key to 
			//point to the correct button to click.
			var key;
	        
			if(window.event)
				key = window.event.keyCode;     //IE
			else
				key = e.which;     //firefox

			if (key == 13){
				if (gfnTrimAll(document.frm_fileSearch.txtKeywordTitle.value) == ''){
					document.frm_fileSearch.txtKeywordTitle.value =  "" ;
					document.frm_fileSearch.txtKeywordTitle.focus();
					event.keyCode = 0;
				}
				else {
					document.frm_fileSearch.txtKeywordTitle.value =  document.frm_fileSearch.txtKeywordTitle.value + "," ;
					document.frm_fileSearch.txtKeywordTitle.focus();
					event.keyCode = 0;
				}
			}
		}
		function chkFrm() {
			var foundError = false;
			
			//validate File Creation from date
			if (!foundError && gfnCheckDate(document.frm_fileSearch.txtFromDt, "File Creation From Date", "O") == false) {
				foundError=true
			}
			
			//validate File Creation to date
			if (!foundError && gfnCheckDate(document.frm_fileSearch.txtToDt, "File Creation To Date", "O") == false) {
				foundError=true
			}

			//Validate File Barcode 
			if (!gfnIsFieldBlank(document.frm_fileSearch.txtBarcode) ) {
				if (!foundError && document.frm_fileSearch.txtBarcode.value.length != '9') {
					foundError=true
					document.frm_fileSearch.txtBarcode.focus()
					alert("Please make sure the File Barcode is nine characters long.")
				}
				
				//Checking whether user input the correct data for "File Barcode"
				var FileBarcode = document.frm_fileSearch.txtBarcode.value;
				if (!foundError && FileBarcode.substr(0,2) != 'FB') {
					foundError=true;
					document.frm_fileSearch.txtBarcode.focus();
					alert("Please enter correct File Barcode.");
				} 
			}
			
 			if (!foundError)
 				return true;
			else
				return false;
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="frm_fileSearch" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td valign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B><asp:Label ID="lblHeader" Runat="server"></asp:Label></B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td valign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table id="Table2" height="500" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td valign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table id="Table3" height="400" cellSpacing="1" cellPadding="0" width="100%" align="left"
							bgColor="silver" border="0">
							<tr>
								<td>
									<table id="Table4" height="400" cellSpacing="0" cellPadding="0" width="100%" align="left"
										bgColor="white" border="0">
										<tr>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
													<TR>
														<TD vAlign="top" align="center" width="100%"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE id="Table6" height="126" cellSpacing="0" cellPadding="3" width="100%" align="left"
																bgColor="white" border="0">
																<TBODY>
																	<TR>
																		<TD vAlign="top" height="352">
																			<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
																				<TBODY>
																					<TR>
																						<TD vAlign="middle" align="right" width="30%"><FONT class="DisplayTitle">ELMS File 
                                                                                            Reference No : </FONT>
																						</TD>
																						<TD width="70%">
                                                                                            <asp:textbox id="txtMasterFileRefNo" Runat="server" MaxLength="25" 
                                                                                                Width="300"></asp:textbox></TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" align="right" width="30%"> <FONT class="DisplayTitle">External 
                                                                                            File Reference No 
																								: </FONT>
																						</TD>
																						<TD width="70%"><asp:textbox id="txtDetailFileRefNo" MaxLength="25" Runat="server" 
                                                                                                Width="300"></asp:textbox></TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" align="right" width="30%"> <FONT class="DisplayTitle">External File 
                                                                                            Title :</FONT></TD>
																						<TD width="70%">
                                                                                            <asp:textbox id="txtDetailFileTitle" MaxLength="50" Runat="server" 
                                                                                                Width="300"></asp:textbox></TD>
																					</TR>
																					<tr>
																						<TD vAlign="middle" align="right" width="30%"> <FONT class="DisplayTitle">
                                                                                            Location/Officer Name :</FONT></TD>
																						<TD width="70%">
                                                                                            <asp:textbox id="txtLocationOfficerName" MaxLength="50" Runat="server" 
                                                                                                Width="300"></asp:textbox></TD>
																					</tr>
																					<TR style="visibility:hidden">
																						<TD vAlign="middle" align="right" width="30%"> <FONT class="DisplayTitle">Remarks :</FONT></TD>
																						<TD width="70%">
                                                                                            <asp:TextBox ID="txtRemarks" Width="600px" MaxLength="2000" Runat="server" 
                                                                                                ></asp:TextBox></TD>
																					</TR>
																					<TR style="visibility:hidden">
																						<TD vAlign="middle" align="right" width="30%"><FONT class="DisplayTitle">RFID Tag 
                                                                                            Number : </FONT>
																						</TD>
																						<TD width="70%">
                                                                                            <asp:textbox id="txtRFIDCode" MaxLength="50" Runat="server" 
                                                                                                Width="300"></asp:textbox></TD>
																					</TR>
																					<TR>
																						<TD align="center" colSpan="2"><BR>
																							<asp:button id="ButSearch" Runat="Server" Text="Search"></asp:button>
																							<asp:button id="butReset" Runat="Server" Text="Reset"></asp:button></TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start : Hidden Fields -->
			<input id="hdnCallFrom" type="hidden" runat="server"> 
			<!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
				src="../common/DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="Z-INDEX:101; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px">
			</iframe>
			<!-- End   : Hidden Fields -->
			<tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer>
		</form>
	</body>
</HTML>
