#Region "Information Section"
' ****************************************************************************************************
' Description       : Create New File 
' Purpose           : Create New File Information
' Author            : See Siew
' Date              : 26/03/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO

#End Region

Partial Class file_GenerateTextFile
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label
    Protected WithEvents ddlSubject As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlTopic As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlSTopic As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlS2Topic As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            '''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("/common/logout.aspx", False)
                Exit Sub
            End If
            '''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                fnCheckAccessRight()
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub fnGenerateFileForFTS(ByVal strFileName4FTS As String)
        Try
            '********** Beginning write text file ***********
            Dim FileFullPath As String = ""
            FileFullPath = Server.MapPath("..\TempFile\FTMInputFile") & "\" & strFileName4FTS
            Dim dt As DataTable = clsFile.fnFileDetail_GetAllRecord().Tables(0)

            If (dt.Rows.Count > 0) Then
                DataTable2CSV(dt, FileFullPath, ",")

            End If
            clsCommon.fnAuditInsertRec(Session("UsrID"), "To generate text file for all files with sub location.")

            ClientScript.RegisterStartupScript(Me.GetType(), "myalert", "alert('Text File Successfully generated.');", True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub DataTable2CSV(ByVal table As DataTable, ByVal filename As String, _
    ByVal sepChar As String)
        Dim writer As System.IO.StreamWriter
        Try
            writer = New System.IO.StreamWriter(filename)

            ' first write a line with the columns name
            Dim sep As String = ""
            Dim builder As New System.Text.StringBuilder
            For Each col As DataColumn In table.Columns
                builder.Append(sep).Append(col.ColumnName)
                sep = sepChar
            Next
            writer.WriteLine(builder.ToString())

            ' then write all the rows
            For Each row As DataRow In table.Rows
                sep = ""
                builder = New System.Text.StringBuilder

                For Each col As DataColumn In table.Columns
                    builder.Append(sep).Append(row(col.ColumnName))
                    sep = sepChar
                Next
                writer.WriteLine(builder.ToString())
            Next
        Finally
            If Not writer Is Nothing Then writer.Close()
        End Try
    End Sub

    Protected Sub btnGenerateTextFile_Click(sender As Object, e As EventArgs) Handles btnGenerateTextFile.Click
        Dim strFileName4FTS As String = "IN_" & Year(Date.Today).ToString & Month(Date.Today).ToString.PadLeft(2, "0") & Day(Date.Today).ToString.PadLeft(2, "0") & Hour(Date.Now).ToString.PadLeft(2, "0") & Minute(Date.Now).ToString.PadLeft(2, "0") & Second(Date.Now).ToString.PadLeft(2, "0") & ".txt"
        fnGenerateFileForFTS(strFileName4FTS)

    End Sub

    Private Sub fnCheckAccessRight()
        Try
            If (InStr(Session("AR"), "FileMng|GenFile|Generate") = 0) Then
                btnGenerateTextFile.Enabled = False
            Else
                btnGenerateTextFile.Enabled = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
