<%@ Page Language="vb" AutoEventWireup="false" Codebehind="File_EditFile.aspx.vb" Inherits="AMS.File_EditVolume"%>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../commonFTS/FTSfooter_cr.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>File_Edit</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../commonFTS/FTSstyle.css" type="text/css" rel="Stylesheet">
		<script language="JavaScript" src="../Common/CommonJS.js"></script>
		<script language="javascript">
		function mfnOpenOfficerLoc() {
		    var FileID = document.frm_fileEditFile.hdnFileID.value;
		    var FileDetailID = document.frm_fileEditFile.hdnFileDetailID.value;
		    var OffLocID = document.frm_fileEditFile.hdnOffLocID.value;
		    var OffLocName = document.frm_fileEditFile.hdnOffLocName.value;
			var CallFrm = document.frm_fileEditFile.hdnCallFrm.value;
			window.open('../commonFTS/selectOfficerLocation.aspx?FID=' + FileID + '&FDID=' + FileDetailID + '&OffLocID=' + OffLocID + '&OffLocName=' + OffLocName + '&CFrm=' + CallFrm, 'Officer_Location_Screen', 'width=700,height=500,Top=0,left=0,scrollbars=1');
		}
		function chkFrm() {
			var foundError = false;
			var flag = false;
			
			//Validate File Status 
			if (!foundError && document.frm_fileEditFile.ddlStatus.value == '-1'){
				foundError=true
				document.frm_fileEditFile.ddlStatus.focus()
				alert("Please choose Status.");
				return false;
			}	
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="frm_fileEditFile" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>File Management : Edit File</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table id="Table2" height="500" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<!--Start: Main Content-->
						<table id="Table3" height="400" cellSpacing="1" cellPadding="0" width="100%" align="left"
							bgColor="silver" border="0">
							<tr>
								<td>
									<table id="Table4" height="400" cellSpacing="0" cellPadding="0" width="100%" align="left"
										bgColor="white" border="0">
										<tr>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
													<TR>
														<TD vAlign="top" align="center" width="100%"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE id="Table6" height="126" cellSpacing="0" cellPadding="3" width="100%" align="left"
																bgColor="white" border="0">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</FONT></B></DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" height="352">
																			<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
																				<TBODY>
																					<TR>
																						<TD vAlign="middle" align="right" width="30%"><FONT color="#666666">ELMS File 
                                                                                            Reference Number : </FONT>
																						</TD>
																						<TD width="70%"><asp:label id="lblFileRefNo" Runat="server"></asp:label></TD>
																					</TR>
																					<TR>
                                                                                        <TD vAlign="middle" align="right"><FONT color="#666666">External File Reference No :</FONT>
																						</TD>
																						<TD width="70%"><asp:label id="lblfld_FileDetail_FileRefNo" Runat="server"></asp:label></TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" align="right"><FONT color="#666666">External File Title : </FONT>
																						</TD>
																						<TD><asp:label id="lblfld_FileDetail_FileTitle" Runat="server"></asp:label></TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" align="right"><FONT color="#666666">Officer/Location : </FONT>
																						</TD>
																						<TD><asp:label id="lblOffLoc" Runat="server"></asp:label>&nbsp;<asp:linkbutton id="butOffLoc" Runat="server">[Change Location/Officer]</asp:linkbutton>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" align="right"><FONT color="#666666">Remarks : </FONT>
																						</TD>
																						<TD>
                                                                                            <asp:TextBox ID="txtRemarks" Width="600px" MaxLength="2000" Runat="server" 
                                                                                                Height="150px" TextMode="MultiLine"></asp:TextBox></TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" align="right"><FONT color="#666666"><font color="red">*</font>File 
																								Status : </FONT>
																						</TD>
																						<TD><asp:dropdownlist id="ddlStatus" Runat="server" AppendDataBoundItems="True" 
                                                                                                AutoPostBack="True">
                                                                                            <asp:ListItem>OPEN</asp:ListItem>
                                                                                            <asp:ListItem>CLOSE</asp:ListItem>
                                                                                            </asp:dropdownlist>&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" align="right"><FONT color="#666666"><font color="red">*</FONT>Closed Date : </FONT>
																						</TD>
																						<TD>
																							<div id="divCloseDt" runat="server">
																								<asp:textbox id="txtCloseDt" Runat="server" Width="120"></asp:textbox>                                                                                                
                                                                                                <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fSingleDtPop(document.frm_fileEditFile.txtCloseDt);return false;"
																							    HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select Date">
																						    </a>
																							</div>
																						</TD>
																					</TR>
																					<tr style="visibility:hidden">
																						<TD vAlign="middle" align="right"><FONT color="#666666">File Barcode : </FONT>
																						</TD>
																						<TD><asp:label id="lblBarcode" Runat="server"></asp:label></TD>
																					</tr>
																					<TR>
																						<TD align="center" colSpan="2"><BR>
																							<asp:button id="butSubmit" Runat="Server" Text="Save"></asp:button><asp:button id="butReset" Runat="Server" Text="Reset"></asp:button><asp:button id="butCancel" Runat="Server" Text="Cancel"></asp:button></TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																			<TABLE id="TableMovement" cellSpacing="1" cellPadding="1" width="100%" border="0">
																				<TBODY>
																					<tr>
																						<td colSpan="2">&nbsp;</td>
																					</tr>
																					<tr>
																						<td colSpan="2">&nbsp;</td>
																					</tr>
																					<TR>
																						<TD align="left" bgColor="#ffff99" colSpan="2"><b>File Movement History</b>

                                                                                            <table cellSpacing="1" cellPadding="0" width="100%" align="left" border="0">
							                                                                    <tr>
								                                                                    <td vAlign="top" align="right" bgColor="#a3a9cc">
									                                                                    <table cellSpacing="1" cellPadding="1" width="100%" border="0">
										                                                                    <tr>
											                                                                    <td width="20%"><asp:dropdownlist id="ddlPageSize" Runat="server" AutoPostBack="True"></asp:dropdownlist>&nbsp;&nbsp;<font color="white">per 
													                                                                    page</font>
											                                                                    </td>
										                                                                    </tr>
									                                                                    </table>
								                                                                    </td>
							                                                                    </tr>
							                                                                    <tr>
								                                                                    <td valign="top" align="center" >
																							<!--Datagrid for display record.-->
																							<asp:datagrid id="dgFileMV" Runat="server" Width="100%" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
																								ItemStyle-Height="25" AutoGenerateColumns="False" BorderStyle="None" BorderColor="#ffffff"
																								PagerStyle-CssClass="DGpageStyle" AllowPaging="true" PageSize="10" PagerStyle-HorizontalAlign="Right"
																								PagerStyle-VerticalAlign="Middle" PagerStyle-Mode="NumericPages" PagerStyle-Position="Top"
																								PagerStyle-BackColor="#a3a9cc" datakeyfield="fld_FileMvID" AlternatingItemStyle-BackColor="#e3d9ee">
																								<headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
																									Height="25"></headerstyle>
																								<Columns>
																									<asp:boundcolumn visible="false" datafield="fld_FileMvID" headertext="Movement ID" ItemStyle-Height="10">
																										<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
																									</asp:boundcolumn>
																									<asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
																										<itemstyle width="15%" cssclass="GridText" verticalalign="Middle"></itemstyle>
																									</asp:boundcolumn>
																									<asp:boundcolumn datafield="fld_LocationOfficerName" headertext="Officer/Location" ItemStyle-Height="10">
																										<itemstyle width="35%" cssclass="GridText" verticalalign="Middle"></itemstyle>
																									</asp:boundcolumn>                                                                                                   
																									<asp:boundcolumn datafield="fld_FileMvDt" headertext="Date Movement" dataformatstring="{0:dd/MM/yyyy hh:mm tt}">
																										<itemstyle width="30%" cssclass="GridText" verticalalign="Middle" HorizontalAlign="Center"></itemstyle>
																									</asp:boundcolumn>
																								</Columns>
																							</asp:datagrid>

                                                                                            </td>
							</tr>
						</table>
																						</td>
																					</tr>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start : Hidden Part -->
			<input id="hdnCallFrm" type="hidden" name="hdnCallFrm" runat="server"> <input id="hdnVolID" type="hidden" name="hdnVolID" runat="server">
			<input id="hdnFileID" type="hidden" name="hdnFileID" runat="server"> <input id="hdnFileNo" type="hidden" name="hdnFileNo" runat="server">
			<input id="hdnAutoChgLocF" type="hidden" name="hdnAutoChgLocF" runat="server"> <input id="hdnAutoChgLocID" type="hidden" name="hdnAutoChgLocID" runat="server">
			<input id="hdnAutoChgLocName" type="hidden" name="hdnAutoChgLocName" runat="server">
			<input id="hdnOffLocID" type="hidden" name="hdnOffLocID" runat="server"> 
			<input id="hdnOffLocName" type="hidden" name="hdnOffLocName" runat="server"> 
			<input id="hdnOldStatus" type="hidden" name="hdnOldStatus" runat="server">
			<input id="hdnStatus" type="hidden" name="hdnStatus" runat="server">
			<input id="hdnCurrentDate" type="hidden" name="hdnCurrentDate" runat="server"> 
			<input id="hdnChgOffLocF" type="hidden" name="hdnChgOffLocF" runat="server">
			<input id="hdnCloseDtF" type="hidden" name="hdnCloseDtF" runat="server"> 
			<input id="hdnCloseDtOld" type="hidden" name="hdnCloseDtOld" runat="server">			
			<input id="hdnFileDetailID" name="hdnFileDetailID" type="hidden" runat="server"> 
			<input id="hdnOldOffLocName" name="hdnOldOffLocName" type="hidden" runat="server"> 
            <!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe id="gToday:contrast:agenda.js" style="Z-INDEX: 101; LEFT: -500px; VISIBILITY: visible; POSITION: absolute; TOP: -500px"
				name="gToday:contrast:agenda.js" src="../common/DateRange/ipopeng.htm" frameBorder="0"
				width="132" scrolling="no" height="142"></iframe>
			<!-- End   : Hidden Part -->
			<tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer></form>
	</body>
</HTML>
