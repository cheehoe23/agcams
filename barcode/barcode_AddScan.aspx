<%@ Page Language="vb" AutoEventWireup="false" Codebehind="barcode_AddScan.aspx.vb" Inherits="AMS.barcode_AddScan"%>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../commonFTS/FTSfooter_cr.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>barcode_AddScan</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../commonFTS/FTSstyle.css" type="text/css" rel="Stylesheet">
        <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
        <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
        <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />
		<script language="JavaScript" src="../Common/CommonJS.js"></script>
		<script language="javascript">

		    $(function () {
		        $('#txtOffLocBarcode').on('keyup', function (e) {
		            if (e.which !== 32) {
		                var value = $(this).val();
		                var noWhitespaceValue = value.replace(/\s+/g, '');
		                var noWhitespaceCount = noWhitespaceValue.length;
		                if (noWhitespaceCount % 10 === 0) {
		                    __doPostBack('hdnclickOffLocBarcode', 'OnClick');
		                }
		                else if (noWhitespaceCount >= 10) {
		                    alert("Cannot find the External Reference No of " + $('#txtOffLocBarcode').val() + " in the File Management Sysetm.");
		                    $('#txtOffLocBarcode').val('')
		                    //__doPostBack('hdnclickOffLocBarcode', 'OnClick');
		                }
		            }
		        });
		    });

            $(function () {
                $('#txtFilesBarcode').on('keyup', function (e) {
                    if (e.which !== 32) {
                        var value = $(this).val();
                        var noWhitespaceValue = value.replace(/\s+/g, '');
                        var noWhitespaceCount = noWhitespaceValue.length;
                        if (noWhitespaceCount % 10 === 0) {
                            __doPostBack('hdnclickFilesBarcode', 'OnClick');
                        }
                        else if (noWhitespaceCount >= 10) {
                            alert("Cannot find the External Reference No of " + $('#txtFilesBarcode').val() + " in the File Management Sysetm.");
                            $('#txtFilesBarcode').val('')
                            //__doPostBack('hdnclickOffLocBarcode', 'OnClick');
                        }
                    }
                });
            });

		function chkFrm() {
			var foundError = false;

			//Validate Officer/Location Barcode
			if (!foundError && gfnIsFieldBlank(document.frm_BarcodeAdd.txtOffLocBarcode)) {
				foundError=true
				document.frm_BarcodeAdd.txtOffLocBarcode.focus()
				alert("Please enter Officer/Location Barcode.")
			}
		
			//Validate File(s) Barcode
//			if (!foundError && gfnIsFieldBlank(document.frm_BarcodeAdd.txtFilesBarcode)) {
//				foundError=true
//				document.frm_BarcodeAdd.txtFilesBarcode.focus()
//				alert("Please enter File(s) Barcode.")
//			}
			
			//Checking whether user input correct data for "File(s) Barcode"
//			if (!foundError) {
//			    var FileBarcodes = document.frm_BarcodeAdd.txtFilesBarcode.value.trim().replace("\n", "");
//				var arrFileBarcodes = new Array();
//				var countNum = 0;
//				var countNum2 = 0;
//				var CurrentFB;
//				arrFileBarcodes = FileBarcodes.split(',');
//				
//				while (countNum < arrFileBarcodes.length){	
//					
//					CurrentFB = gfnTrimAll(arrFileBarcodes[countNum]);

//					CurrentFB = CurrentFB.replace("\n", "");

//					//Check whether Files Barcode is empty
//					if (CurrentFB != '') {
//						//Check for duplicate File Barcode entering
//						countNum2 = countNum + 1
//						if (!foundError) {
//							while (countNum2 < arrFileBarcodes.length) {
//							    if (!foundError && CurrentFB == gfnTrimAll(arrFileBarcodes[countNum2]).replace("\n", "")) {
//									foundError=true;
//									document.frm_BarcodeAdd.txtFilesBarcode.focus();
//									alert("You have enter duplicate File(s) Barcode. Please check it.");
//								}
//								countNum2+=1;
//							}
//						}
//					}
//					
//					if (!foundError){ countNum+=1;}
//					else {countNum = arrFileBarcodes.length;}
//				}
//			}
			
 			if (!foundError){
 				var flag = false;
 				flag = window.confirm("Are you sure want to add this record(s)?");
 				return flag;
 			}
			else
				return false;
        }

//        $(function () {
//            $('#txtFilesBarcode').keyup(function (e) {
//                this.val = this.val.replace("\n","");
//                if ($(this).val().length == $(this).attr('maxlength'))
//                    $(this).next(':input').focus()
//            })
//        })

//		function fnScanOnce(fType){
//			//the purpose of this function is to allow the enter key to 
//			//point to the correct button to click.

//			var key;
//	        
//			if(window.event)
//				key = window.event.keyCode;     //IE
//			else
//				key = e.which;     //firefox

//			if (key == 13){
//				if (fType == 'OL'){ //Officer/Location 
//					document.frm_BarcodeAdd.txtFilesBarcode.focus();
//					event.keyCode = 0;
//				}
//	        else {   //File	            
////				    document.frm_BarcodeAdd.txtFilesBarcode.value = document.frm_BarcodeAdd.txtFilesBarcode.value.replace("\n","") + ",";
////				    document.frm_BarcodeAdd.txtFilesBarcode.value = document.frm_BarcodeAdd.txtFilesBarcode.value.toString().trim().replace("\n", "").trim();
//				    document.frm_BarcodeAdd.txtFilesBarcode.value = document.frm_BarcodeAdd.txtFilesBarcode.value + ",";
//				    //document.frm_BarcodeAdd.txtFilesBarcode.value = document.frm_BarcodeAdd.txtFilesBarcode.value.toString().trim().replace("\n", "").trim();			    
//                    document.frm_BarcodeAdd.txtFilesBarcode.focus();
//					event.keyCode = 0;
//				}
//			}
//		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" >
		<form id="frm_BarcodeAdd" method="post" runat="server">       
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Batch Proccessing : Barcode Scanner</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table id="Table2" height="500" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<!--Start: Main Content-->
						<table id="Table3" height="400" cellSpacing="1" cellPadding="0" width="100%" align="left"
							bgColor="silver" border="0">
							<tr>
								<td>
									<table id="Table4" height="400" cellSpacing="0" cellPadding="0" width="100%" align="left"
										bgColor="white" border="0">
										<tr>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
													<TR>
														<TD vAlign="top" align="center" width="100%"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE id="Table6" height="126" cellSpacing="0" cellPadding="3" width="100%" align="left"
																bgColor="white" border="0">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</FONT></B></DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" height="352">
																			<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
																				<TBODY>
																					<TR>
																						<TD align="right" width="35%"><FONT class="DisplayTitle"><font color="red">*</font>Officer/Location 
																								Barcode : </FONT>
																						</TD>
																						<TD width="65%">
                                                                                            <asp:TextBox ID="txtOffLocBarcode"  runat="server" Width="400px" 
                                                                                                CssClass="UpperCaseTextBox" ></asp:TextBox>
																						</TD>
																					</TR>
																					<TR>
																						<TD align="right" width="35%" valign="top"><FONT class="DisplayTitle"><font color="red">*</font>File(s) 
																								Barcode : </FONT>
																						</TD>
																						<TD width="65%">
                                                                                            <asp:TextBox ID="txtFilesBarcode" Columns="50" Rows="6" 
                                                                                                Runat="server" 
                                                                                                CssClass="UpperCaseTextBox" Width="400px" ></asp:TextBox>
                                                                                            <br />
                                                                                            <asp:ListBox ID="lstFilesBarcode" runat="server" Width="400px" Height="152px"></asp:ListBox>
                                                                                        </TD>
																					</TR>
																					<TR>
																						<TD align="center" colSpan="2"><BR>
																							<asp:button id="butSubmit" Runat="Server" Text="Save"></asp:button>
																							<asp:button id="butReset" Runat="Server" Text="Reset"></asp:button></TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer>
            <input id="hdnOffLocStatus" name="hdnOffLocStatus" type="hidden" runat="server"><input id="hdnLocOff" name="hdnLocOff" type="hidden" runat="server">
            <input id="hdnFilesBarcode" name="hdnFilesBarcode" type="hidden" runat="server">
            <input id="hdnfilebarcodestatus" name="hdnfilebarcodestatus" type="hidden" runat="server">
            <asp:Button ID="hdnclickOffLocBarcode" runat="server" Visible ="false" ></asp:button>
            <asp:Button ID="hdnclickFilesBarcode" runat="server" Visible ="false" ></asp:button>
		</form>
	</body>
</HTML>
