#Region "Information Section"
' ****************************************************************************************************
' Description       : Add Barcode details for scanner
' Purpose           : Add Barcode details for scanner 
' Author            : See Siew
' Date              : 13/04/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class barcode_AddScan
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        Try
            '''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("/common/logout.aspx", False)
                Exit Sub
            End If
            '''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            ClientScript.GetPostBackEventReference(Me, String.Empty)

            If Not Page.IsPostBack Then
                Me.txtOffLocBarcode.Focus()
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")
                'Me.txtOffLocBarcode.Attributes.Add("onkeypress", "return fnScanOnce('OL')")
                'Me.txtFilesBarcode.Attributes.Add("onkeypress", "return fnScanOnce('F')")
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        txtOffLocBarcode.Text = ""
        txtFilesBarcode.Text = ""
        lstFilesBarcode.Items.Clear()
        hdnFilesBarcode.Value = ""
        hdnLocOff.Value = ""
        txtOffLocBarcode.Focus()
    End Sub

    Private Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim intRetVal As Integer
            Dim strMsg As String

            ''get Fields Value         
            hdnFilesBarcode.Value = ""

            For i As Integer = 0 To lstFilesBarcode.Items.Count - 1
                hdnFilesBarcode.Value = hdnFilesBarcode.Value + lstFilesBarcode.Items(i).Value.ToString() + ","
            Next

            hdnFilesBarcode.Value = hdnFilesBarcode.Value.ToString().TrimEnd(",")

            ''insert record
            intRetVal = clsCommon.fnBarcodeAddRecords(
                                hdnOffLocStatus.Value, hdnLocOff.Value.ToString().Trim(), hdnFilesBarcode.Value.ToString().Trim(), Session("UsrID"), Session("LoginID"))

            If intRetVal > 0 Then
                strMsg = "File Movement Add Successfully."

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "File Movement Added : " + hdnLocOff.Value.ToString().Trim() + " (" + hdnFilesBarcode.Value.ToString().Trim() + ")")

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "</script>"
                Response.Write(strJavaScript)

                ''Reset the Form 
                butReset_Click(Nothing, Nothing)
            Else
                strMsg = "Barcodes Add Failed."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Function txtFilesBarcode_Change() As Boolean
        Dim flag As Boolean = False
        Dim objRdr As SqlDataReader

        Try
            objRdr = clsCommon.fnArrayFileDetailBarCodes(txtFilesBarcode.Text.Replace(Environment.NewLine, "").TrimEnd(" "))
            If Not objRdr Is Nothing Then
                objRdr.Read()
                If (CStr(objRdr("FileBarCode")) <> "") Then
                    hdnfilebarcodestatus.Value = CStr(objRdr("FileBarCode"))
                    lblErrorMessage.Text = "File Bar Codes are not found for " + hdnfilebarcodestatus.Value + ". Please contact your administrator."
                    lblErrorMessage.Visible = True
                    flag = True
                End If
                objRdr.Close()
            End If
            Return flag

        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
        End Try
    End Function

    Private Function txtOffLocBarcode_Change() As Boolean

        Try
            Dim flag As Boolean = False

            If (txtOffLocBarcode.Text.Trim() <> "") Then
                Dim dt As DataTable = clsCommon.fnGetDataByfld_LocationOfficerBarcodeID(txtOffLocBarcode.Text.Trim()).Tables(0)

                If dt.Rows.Count <= 0 Then
                    lblErrorMessage.Visible = True
                    lblErrorMessage.Text = "Location or Officer are not found in File Management System. Please contact your administrator!"
                    txtOffLocBarcode.Focus()
                    flag = True
                End If
            Else
                lblErrorMessage.Visible = False
                txtOffLocBarcode.Focus()
                flag = False
            End If

            Return flag
        Catch ex As Exception

        End Try
    End Function

    Protected Sub txtFilesBarcode_TextChanged(sender As Object, e As EventArgs) Handles txtFilesBarcode.TextChanged
        txtFilesBarcode_Change()
    End Sub

    Private Sub hdnclickOffLocBarcode_Click(sender As Object, e As System.EventArgs) Handles hdnclickOffLocBarcode.Click
        Try
            If Trim(txtOffLocBarcode.Text.Trim()) <> "" Then

                If (txtOffLocBarcode.Text.Trim().Length = 10) Then
                    Dim dt As DataTable = clsLocationOfficer.fnGetDataByfld_fld_LocationOfficerBarcodeID(txtOffLocBarcode.Text.Trim().Replace(" ", "").Trim()).Tables(0)

                    If (dt.Rows.Count > 0) Then
                        txtOffLocBarcode.Text = dt.Rows(0)("fld_LocationOfficerName").ToString()
                        hdnLocOff.Value = dt.Rows(0)("fld_LocationOfficerID").ToString()
                        lblErrorMessage.Visible = False
                        lblErrorMessage.Text = ""
                        txtFilesBarcode.Focus()
                    Else
                        lblErrorMessage.Text = "Cannot find the Location/Officer of " & txtOffLocBarcode.Text & " in the File Management Sysetm."
                        txtOffLocBarcode.Text = ""
                        txtOffLocBarcode.Focus()
                        hdnLocOff.Value = ""
                        lblErrorMessage.Visible = True
                    End If
                ElseIf (txtOffLocBarcode.Text.Trim().Length >= 10) Then
                    lblErrorMessage.Text = "Cannot find the Location/Officer of " & txtOffLocBarcode.Text & " in the File Management Sysetm."
                    txtOffLocBarcode.Text = ""
                    txtOffLocBarcode.Focus()
                    hdnLocOff.Value = ""
                    lblErrorMessage.Visible = True
                End If
                
            End If

        Catch ex As Exception
            'lblErrorMessage.Text = "Cannot find the Location/Officer of " & txtOffLocBarcode.Text & " in the File Management Sysetm."
            'txtOffLocBarcode.Text = ""
            'txtOffLocBarcode.Focus()
            'lblErrorMessage.Visible = True
            'lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            'lblErrorMessage.Visible = True
        End Try
    End Sub
    
    Private Sub hdnclickFilesBarcode_Click(sender As Object, e As System.EventArgs) Handles hdnclickFilesBarcode.Click
        Try
            If Trim(txtFilesBarcode.Text.Trim()) <> "" Then

                If (txtFilesBarcode.Text.Trim().Length = 10) Then
                    Dim dt As DataTable = clsFile.fnFile_GetDataByfld_FileDetailBarcodeID(txtFilesBarcode.Text.ToString().Replace(" ", "").Trim()).Tables(0)

                    If (dt.Rows.Count > 0) Then
                        lstFilesBarcode.Items.Add(New ListItem(dt.Rows(0)("fld_FileDetail_FileRefNo").ToString(), dt.Rows(0)("fld_FileDetailID").ToString()))
                        lblErrorMessage.Visible = False
                        lblErrorMessage.Text = ""
                        txtFilesBarcode.Text = ""
                        txtFilesBarcode.Focus()
                    Else
                        lblErrorMessage.Text = "Cannot find the External Reference No of " & txtFilesBarcode.Text & " in the File Management Sysetm."
                        txtFilesBarcode.Text = ""
                        txtFilesBarcode.Focus()
                        lblErrorMessage.Visible = True
                    End If
                ElseIf (txtFilesBarcode.Text.Trim().Length >= 10) Then
                    lblErrorMessage.Text = "Cannot find the External Reference No of " & txtFilesBarcode.Text & " in the File Management Sysetm."
                    txtFilesBarcode.Text = ""
                    txtFilesBarcode.Focus()
                    lblErrorMessage.Visible = True
                End If
            End If

        Catch ex As Exception
            'lblErrorMessage.Text = "Cannot find the External Reference No of " & txtFilesBarcode.Text & " in the File Management Sysetm."
            'txtFilesBarcode.Text = ""
            'txtFilesBarcode.Focus()
            'lblErrorMessage.Visible = True
        End Try
    End Sub
End Class
