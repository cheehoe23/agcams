#Region "Information Section"
' ****************************************************************************************************
' Description       : Add Barcode details for Wireless Barcode scanner
' Purpose           : Add Barcode details for Wireless Barcode scanner 
' Author            : Win
' Date              : 20/02/2017
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class wirelessBarcode_AddScan
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        Try
            '''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("/common/logout.aspx", False)
                Exit Sub
            End If
            '''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            ClientScript.GetPostBackEventReference(Me, String.Empty)

            If Not Page.IsPostBack Then

                Me.txtBarcode.Focus()
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        txtBarcode.Text = ""
        txtBarcode.Focus()
    End Sub

    Private Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim intRetVal As Integer
            Dim strMsg As String
            Dim LocOfficer1 As String
            Dim LocOfficer2 As String
            Dim status As String
            Dim noOfChar As Integer
            Dim BarcodeDateTime As DateTime
            Dim BarcodeID As Integer

            ''get Fields Value         
            If (txtBarcode.Text.Trim() <> "") Then
                Dim Barcode() As String = txtBarcode.Text.Trim().Split(Environment.NewLine)
                
                Dim i As Integer
                For i = 0 To Barcode.Length - 1
                    If (Barcode.Length = 1 And txtBarcode.Text.Trim().Length > 32) Then
                        lblErrorMessage.Text = "Barcode Format is not correct. Please configuration and try again!"
                        lblErrorMessage.Visible = True
                        Exit Sub
                    ElseIf txtBarcode.Text.Trim().Length = 32 Then
                        lblErrorMessage.Text = "Please scan more than one barcode!"
                        lblErrorMessage.Visible = True
                        Exit Sub
                    Else
                        lblErrorMessage.Text = ""
                        lblErrorMessage.Visible = False
                    End If

                    If Barcode(i).Contains("FB") Then
                        status = "FB"
                        noOfChar = Barcode(i).IndexOf("F")
                        Try
                            BarcodeDateTime = Barcode(i).Substring(0, noOfChar - 1)
                        Catch ex As Exception
                            BarcodeDateTime = System.DateTime.Now
                        End Try
                        If (Convert.ToInt32(BarcodeDateTime.ToString("yyyy")) < 2010) Then
                            BarcodeDateTime = System.DateTime.Now
                        End If

                        BarcodeID = Barcode(i).Substring(noOfChar + 2, Barcode(i).Length - ((noOfChar) + 2))
                    ElseIf Barcode(i).Contains("LO") Then
                        status = "LO"
                        noOfChar = Barcode(i).IndexOf("L")
                        Try
                            BarcodeDateTime = Barcode(i).Substring(0, noOfChar - 1)
                        Catch ex As Exception
                            BarcodeDateTime = System.DateTime.Now
                        End Try
                        If (Convert.ToInt32(BarcodeDateTime.ToString("yyyy")) < 2010) Then
                            BarcodeDateTime = System.DateTime.Now
                        End If
                        BarcodeID = Barcode(i).Substring(noOfChar + 2, Barcode(i).Length - ((noOfChar) + 2))
                    End If

                    If Barcode(i).Contains("FB") Or Barcode(i).Contains("LO") Then
                        LocOfficer2 = clsFile.fn_tbl_File_Movement_InsertWirelessBarcode(BarcodeID, BarcodeDateTime, Convert.ToInt32(LocOfficer1), status, Session("LoginID"), Session("UsrID"))
                        If (LocOfficer2.Trim() <> "0" And LocOfficer2.Trim() <> "-1") Then
                            LocOfficer1 = LocOfficer2.Trim()
                        End If
                    End If
                Next
                clsFile.fnShrinkDB()
            End If

            Dim barcodeInfo As String = ""
            If (txtBarcode.Text.Trim().Replace(Environment.NewLine, ", ").Trim().Length > 3900) Then
                barcodeInfo = txtBarcode.Text.Trim().Replace(Environment.NewLine, ", ").Trim().Substring(0, 3900)
            Else
                barcodeInfo = txtBarcode.Text.Trim().Replace(Environment.NewLine, ", ").Trim()
            End If

            If LocOfficer1 <> 0 Or LocOfficer2 <> 0 Then
                strMsg = "File Movement Add Successfully."

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "Wireless Barcode Scanning for File Movement Added : " + barcodeInfo)

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "</script>"
                Response.Write(strJavaScript)

                ''Reset the Form 
                butReset_Click(Nothing, Nothing)
            Else
                strMsg = "Barcodes Add Failed."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

End Class
