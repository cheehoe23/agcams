#Region "Information Section"
' ****************************************************************************************************
' Description       : Offline Mode
' Purpose           : Offline Mode
' Author            : See Siew
' Date              : 27/04/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
#End Region

Partial Class barcode_OfflineMode
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            '''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("/common/logout.aspx", False)
                Exit Sub
            End If
            '''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")
                'txtFileLoc.Attributes.Add("value", "Mobile Device\Application\UFTSData\UFTSD.txt")
                'txtFileLoc.Attributes.Add("value", "C:\fts_db\UFTSD.txt")
                'txtFileLoc.Value = "Mobile Device\Application\UFTSData\UFTSD.txt"
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        txtFileLoc.Value = ""
    End Sub

    Private Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim FolderN As String
            Dim strFileName As String
            Dim strFilePath As String

            'Get URL to save the file
            FolderN = Server.MapPath("..\tempFile\") ' & "\"

            'Get the name of the file that is posted.
            strFileName = txtFileLoc.PostedFile.FileName() ' "C:\fts_db\UFTSD.txt" 
            strFileName = Path.GetFileName(strFileName)

            'Save the uploaded file to the server.
            strFilePath = FolderN + strFileName


            ''****** 1. Upload File to server
            fnUploadFile(strFilePath)


            ''****** 2. after uploading, read the file
            fnSaveOfflineBarcode(strFilePath)

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Sub fnUploadFile(ByVal strFilePath As String)
        Try
            ''Save File 
            txtFileLoc.PostedFile.SaveAs(strFilePath)
        Catch Ex As Exception
            Throw Ex
        End Try
    End Sub

    Sub fnSaveOfflineBarcode(ByVal strFilePath As String)
        Try
            Dim lobjStreamReader As StreamReader
            Dim strEachRowRecord, strMsg As String
            Dim strOffLocBC, strFileBC, strTempMvDt, strMvDt As String
            Dim intRetVal As Integer

            strMsg = ""

            If System.IO.File.Exists(strFilePath) = True Then
                lobjStreamReader = File.OpenText(strFilePath)
                lobjStreamReader.BaseStream.Seek(0, SeekOrigin.Begin)

                While lobjStreamReader.Peek() > -1
                    'lblErrorMessage.Text += "File Path3 - "
                    'lblErrorMessage.Text += lobjStreamReader.ReadLine().ToString + "; "

                    ''get Fields Value
                    strEachRowRecord = Trim(lobjStreamReader.ReadLine().ToString)
                    If strEachRowRecord <> "" Then
                        strFileBC = ""
                        strOffLocBC = ""
                        strMvDt = ""
                        Dim tArray As Array
                        tArray = Split(strEachRowRecord, "|")
                        strOffLocBC = Trim(tArray(0))
                        strFileBC = Trim(tArray(1))
                        strTempMvDt = Trim(tArray(2))
                        strMvDt = Mid(strTempMvDt, 5, 2) + Mid(strTempMvDt, 3, 2) + Left(strTempMvDt, 2) + Right(strTempMvDt, 6)
                        ''checking whether user scan correct data
                        If (Left(strOffLocBC, 2) = "UB" Or Left(strOffLocBC, 2) = "LB") And Left(strFileBC, 2) = "FB" Then
                            intRetVal = clsCommon.fnBarcodeAddRecordsForPortableScan( _
                                        strOffLocBC, strFileBC, strMvDt, Session("UsrID"), Session("LoginID"))

                            strMsg = "File Movement Add Successfully."

                        End If

                        'For i = 1 To UBound(tArray)
                        '    strFileBC = strFileBC & tArray(i) & ","
                        'Next
                        '                        intFirstComma = strEachRowRecord.IndexOf(",")
                        '                       strOffLocBC = Trim(strEachRowRecord.Substring(0, intFirstComma))
                        '                      strFileBC = Trim(strEachRowRecord.Substring(intFirstComma, strEachRowRecord.Length))

                        'If strOffLocBC <> "" And strFileBC <> "" Then
                        '    ''insert record
                        '    intRetVal = clsCommon.fnBarcodeAddRecords( _
                        '                        strOffLocBC, strFileBC, Session("UsrID"), Session("LoginID"))

                        '    strMsg = "File Movement Add Successfully."
                        'End If
                    End If
                End While

                ''Close reader the file
                lobjStreamReader.Close()

                If strMsg <> "" Then
                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                End If

            Else
                strMsg = "File location not found. please contact your administrator."
            End If
        Catch Ex As Exception
            Throw Ex
        End Try
    End Sub
End Class
