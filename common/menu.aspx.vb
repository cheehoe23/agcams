#Region "Information Section"
' ****************************************************************************************************
' Description       : Menu 
' Purpose           : Display Menu
' Author            : See Siew
' Date              : 14/03/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports Microsoft.Web.UI.WebControls
#End Region

Partial Class menu
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                ''Polulate Menu
                PupolateMenu()

                ''**Start :  get total missing owner record and pending for condemnation
                Dim intTotalMissOwner As Integer = "0"
                Dim intTotalPendCondemn As Integer = "0"
                Dim intTotalNFSToday As Integer = "0"
                Dim intTotalPendReturn As Integer = "0"

                ''Check for acces right
                Dim StrGetRecType As String = "3"
                'If (InStr(Session("AR"), "AssetMng|CondmCtrl|condemnAsset") > 0 And InStr(Session("AR"), "AssetMng|CondmCtrl|condemnInven") > 0) Then
                '    StrGetRecType = "3"
                'ElseIf InStr(Session("AR"), "AssetMng|CondmCtrl|condemnAsset") > 0 Then
                '    StrGetRecType = "1"
                'ElseIf InStr(Session("AR"), "AssetMng|CondmCtrl|condemnInven") > 0 Then
                '    StrGetRecType = "2"
                'Else
                '    StrGetRecType = "0"
                'End If
                'clsAsset.fnMissingOwner_CountRec(intTotalMissOwner)
                'clsAsset.fnAsset_GetCondemnExpRecord("fldAssetBarcode", "ASC", intTotalPendCondemn)
                clsCommon.fnMenu_CountRec4Information(Session("AdminF"), Session("UsrID"), StrGetRecType, intTotalMissOwner, intTotalPendCondemn, intTotalNFSToday, intTotalPendReturn)
                lblMenuMissOwner.Text = CStr(intTotalMissOwner)
                lblMenuCondemn.Text = CStr(intTotalPendCondemn)
                lblMenuNFSToday.Text = CStr(intTotalNFSToday)
                lblMenuReturn.Text = CStr(intTotalPendReturn)
                ''**End   :  get total missing owner record  and pending for condemnation
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub PupolateMenu()
        Dim drModule, drSModule As SqlDataReader
        Try

            Dim nodeMod, nodeSMod As TreeNode

            '' 1. Get Module 
            drModule = clsCommon.fnMenuGetModuleFunction("", Session("AR"), "M")
            If Not drModule Is Nothing Then
                If drModule.HasRows Then
                    While drModule.Read  '' LOOP MODULE
                        ''Add Node for Module
                        nodeMod = New TreeNode
                        'nodeMod.Text = "&nbsp;<font class='navhead'>" + CStr(drModule("fld_moduleName")) + "</font>"
                        nodeMod.Text = "&nbsp;" + CStr(drModule("fld_moduleName"))
                        TvMenu.Nodes.Add(nodeMod)

                        ''2. Get Sub-Module that belong to particular Module
                        drSModule = clsCommon.fnMenuGetModuleFunction(drModule("fld_moduleValue"), Session("AR"), "S")
                        If Not drSModule Is Nothing Then
                            If drSModule.HasRows Then
                                While drSModule.Read  '' LOOP SUB MODULE
                                    ''Add Node for particular Module
                                    nodeSMod = New TreeNode

                                    If CStr(drModule("fld_moduleValue")) = "STGUI" And CStr(drSModule("fld_subModuleValue")) = "ST" Then
                                        ''For Stock-Tracking Flash GUI --> pop-up window
                                        nodeSMod.Text = "<A class=""Child"" href=""#"" onClick=""javascript:window.open(' " + CStr(drSModule("fld_functionFile")) + "', 'STGUI', 'width=700,height=500,Top=0,left=0,resizable=1,scrollbars=1')"">"
                                        nodeSMod.Text += CStr(drSModule("fld_subModuleName")) + "</A>"
                                    Else
                                        nodeSMod.Text = "&nbsp;<A href='" + CStr(drSModule("fld_functionFile")) + "' target=content>"
                                        nodeSMod.Text += CStr(drSModule("fld_subModuleName")) + "</A>"
                                    End If


                                    ''**Start : If Module is Asset Management --> Missing Owner, get total missing owner record
                                    'If drModule("fld_moduleValue") = "AssetMng" And drSModule("fld_subModuleValue") = "UsrResign" Then
                                    '    clsAsset.fnMissingOwner_CountRec(intTotalMissOwner)
                                    '    nodeSMod.Text += "&nbsp;<span id='lblMenuTotalMissOwner'>(" & CStr(intTotalMissOwner) & ")</span>"
                                    '    'nodeSMod.Text += "&nbsp;<input name='lblMenuTotalMissOwner' type='text' value='(" & CStr(intTotalMissOwner) & ")' id='lblMenuTotalMissOwner' style='border-color:White;border-style:None;' />"
                                    'End If
                                    ''**End   : If Module is Asset Management --> Missing Owner, get total missing owner record

                                    nodeSMod.ImageUrl = "../images/dot.gif"
                                    'nodeSMod.SelectedStyle.Item = "FONT-SIZE: 10pt; FONT-WEIGHT: bold; COLOR:#62902E"
                                    nodeMod.Nodes.Add(nodeSMod)

                                End While  '' LOOP SUB MODULE
                            End If
                        End If
                        drSModule.Close()

                    End While   '' LOOP MODULE
                End If
            End If
            drModule.Close()

            ''3. Additional Module
            ''Add for Online Help
            'nodeMod = New TreeNode
            'nodeMod.Text = "&nbsp;<A href='../common/UserGuide.pdf' target=_blank>User Manual</A>"
            'nodeMod.ImageUrl = "../images/dot.gif"
            'TvMenu.Nodes.Add(nodeMod)

            ''Add for Logout
            nodeMod = New TreeNode
            nodeMod.Text = "&nbsp;<A href='../common/logout.aspx?CallFrm=logout' target=_parent>Logout</A>"
            nodeMod.ImageUrl = "../images/dot.gif"
            TvMenu.Nodes.Add(nodeMod)

            ''Set Default output for TreeView
            TvMenu.ShowLines = False
            TvMenu.CssClass = "tvMenu"
        Catch ex As Exception
            clsCommon.fnDataReader_Close(drModule)
            clsCommon.fnDataReader_Close(drSModule)
            Throw ex
        End Try
    End Sub

    Protected Sub lbtnFTS_Click(sender As Object, e As EventArgs) Handles lbtnFTS.Click
        Try
            'If Len(Session("UsrID")) = 0 Then
            '    'Response.Redirect("logout.aspx", False)
            '    Dim strJavaScript As String = ""
            '    strJavaScript = "<script language = 'Javascript'>" & _
            '                    "parent.location.href='../common/logout.aspx';" & _
            '                    "</script>"
            '    Response.Write(strJavaScript)
            '    Exit Sub
            'End If

            Dim strJavaScript1 As String = ""
            'strJavaScript1 = "<script language = 'Javascript'>" & _
            '                "parent.location.href='default.aspx?LoginID=" + Session("LoginID") + "';" & _
            '                "</script>"
            strJavaScript1 = "<script language = 'Javascript'>" & _
                            "parent.location.href='/indexFTS.aspx';" & _
                            "</script>"
            Response.Write(strJavaScript1)

        Catch ex As Exception
        End Try
    End Sub
    
End Class
