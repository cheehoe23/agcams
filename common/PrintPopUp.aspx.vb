﻿Public Class PrintPopUp
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Len(Session("UsrID")) = 0 Then
            Dim strJavaScript As String = ""
            strJavaScript = "<script language = 'Javascript'>" & _
                            "parent.location.href='../common/logout.aspx';" & _
                            "</script>"
            Response.Write(strJavaScript)
            Exit Sub
        End If
    End Sub

    'Added by YK 23/06/2014 - Retrieve value from Cookie to set default view to Quarterly or Monthly
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init

        fnPopulatePrinterLocationDDL(ddlPrintLocation)

        If (Session("ModuleName" + Session("UsrID").ToString()) = "AMS") Then
            ddlPrintLocation.SelectedValue = gAMSPrinterLocLevel7
            ddlPrintLocation.Items.Remove(ddlPrintLocation.Items.FindByValue(gFTSPrinterLocLevel9))
            ddlPrintLocation.Items.Remove(ddlPrintLocation.Items.FindByValue(gFTSPrinterLocL9AMS02))
            ddlPrintLocation.Items.Remove(ddlPrintLocation.Items.FindByValue(gFTSPrinterLocLevel10))
        ElseIf (Session("ModuleName" + Session("UsrID").ToString()) = "FTS") Then
            ddlPrintLocation.SelectedValue = gFTSPrinterLocLevel10
            ddlPrintLocation.Items.Remove(ddlPrintLocation.Items.FindByValue(gAMSPrinterLocLevel7))
            ddlPrintLocation.Items.Remove(ddlPrintLocation.Items.FindByValue(gAMSPrinterLocLevel8))
        End If

        If (Request.Cookies("isLoc") IsNot Nothing) Then
            Dim Level As String = ""
            If (Request.Cookies("isLoc")("Level") IsNot Nothing) Then
                Level = Request.Cookies("isLoc")("Level")
                If Level = gAMSPrinterLocLevel7 Then
                    Me.ddlPrintLocation.SelectedValue = Level
                ElseIf Level = gAMSPrinterLocLevel8 Then
                    Me.ddlPrintLocation.SelectedValue = Level
                ElseIf Level = gFTSPrinterLocLevel9 Then
                    Me.ddlPrintLocation.SelectedValue = Level
                ElseIf Level = gFTSPrinterLocL9AMS02 Then
                    Me.ddlPrintLocation.SelectedValue = Level
                ElseIf Level = gFTSPrinterLocLevel10 Then
                    Me.ddlPrintLocation.SelectedValue = Level
                End If
            End If
        End If

    End Sub

    Public Sub fnPrintRFID(ByVal PrinterLocation As String)
        Try
            Dim Ids As String = ""

            Dim Asset As String = ""
            Dim Location As String = ""
            Dim ModuleName As String = ""
            Dim File As String = ""
            Dim LocationOrOfficer As String = ""
            If Not Session("Asset" + Session("UsrID").ToString()) Is Nothing Then
                Asset = Session("Asset" + Session("UsrID").ToString()).ToString()
            End If
            If Not Session("Location" + Session("UsrID").ToString()) Is Nothing Then
                Location = Session("Location" + Session("UsrID").ToString()).ToString()
            End If
            If Not Session("ModuleName" + Session("UsrID").ToString()) Is Nothing Then
                ModuleName = Session("ModuleName" + Session("UsrID").ToString()).ToString()
            End If
            If Not Session("File" + Session("UsrID").ToString()) Is Nothing Then
                File = Session("File" + Session("UsrID").ToString()).ToString()
            End If
            If Not Session("LocationOrOfficer" + Session("UsrID").ToString()) Is Nothing Then
                LocationOrOfficer = Session("LocationOrOfficer" + Session("UsrID").ToString()).ToString()
            End If

            If (Asset = "1" And ModuleName = "AMS") Then
                Ids = Session("AssetIds" + Session("UsrID").ToString()).ToString()

                clsRFIDLabel.fnCreateRFIDLabelAsset(Ids, Session("UsrID").ToString(), _
                                               Server.MapPath(PrinterLocation), "")

            ElseIf (Location = "1" And ModuleName = "AMS") Then
                Ids = Session("LoctID" + Session("UsrID").ToString()).ToString()

                clsRFIDLabel.fnCreateRFIDLabelFile4Loc(Ids, Session("UsrID"), _
                                               Server.MapPath(PrinterLocation), "")

            ElseIf (File = "1" And ModuleName = "FTS") Then
                Ids = Session("FileDetailIDs" + Session("UsrID").ToString()).ToString()

                clsRFIDLabel.fnCreateRFIDLabelFile(Ids, Session("UsrID"), _
                                               Server.MapPath(PrinterLocation), "")

            ElseIf (LocationOrOfficer = "1" And ModuleName = "FTS") Then
                Ids = Session("LocOffIDs" + Session("UsrID").ToString()).ToString()

                clsRFIDLabel.fnCreateRFIDLabelLocationOfficer(Ids, Session("UsrID").ToString(), _
                                               Server.MapPath(PrinterLocation), "")
            End If

            If (Ids <> "") Then
                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), Ids, "RFID label printed.")

                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + "RFID Tag print succesfully." + "');" & _
                                "</script>"
                Response.Write(strJavaScript)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnprint_Click(sender As Object, e As EventArgs) Handles btnprint.Click

        Dim printerLoc As String = ""
        printerLoc = ddlPrintLocation.SelectedValue

        If (printerLoc = gAMSPrinterLocLevel7) Then
            fnPrintRFID(gAMSRFIDLabelLevel7Path)
        ElseIf (printerLoc = gAMSPrinterLocLevel8) Then
            fnPrintRFID(gAMSRFIDLabelLevel8Path)
        ElseIf (printerLoc = gFTSPrinterLocLevel9) Then
            fnPrintRFID(gFTSRFIDLabelLevel9Path)
        ElseIf (printerLoc = gFTSPrinterLocL9AMS02) Then
            fnPrintRFID(gFTSRFIDLabelL9AMS02Path)
        ElseIf (printerLoc = gFTSPrinterLocLevel10) Then
            fnPrintRFID(gRFIDLabelPath)
        End If

        Dim myCookie As HttpCookie = New HttpCookie("isLoc")
        myCookie("Level") = printerLoc
        myCookie.Expires = Now.AddYears(50)
        Response.Cookies.Add(myCookie)

        Dim AddAsset As String = ""
        Dim ModuleName As String = ""
        Dim AddFile As String = ""
        If Not Session("AddAsset" + Session("UsrID").ToString()) Is Nothing Then
            AddAsset = Session("AddAsset" + Session("UsrID").ToString()).ToString()
        ElseIf Not Session("AddFile" + Session("UsrID").ToString()) Is Nothing Then
            AddFile = Session("AddFile" + Session("UsrID").ToString()).ToString()
        End If

        If Not Session("ModuleName" + Session("UsrID").ToString()) Is Nothing Then
            ModuleName = Session("ModuleName" + Session("UsrID").ToString()).ToString()
        End If

        If (AddAsset = "1" And ModuleName = "AMS") Then
            ClientScript.RegisterStartupScript(Me.[GetType](), "myalert", "window.opener.location.href='../AssetMng/asset_addChoosen.aspx'; window.close();", True)
        ElseIf (AddFile = "1" And ModuleName = "FTS") Then
            ClientScript.RegisterStartupScript(Me.[GetType](), "myalert", "window.opener.location.href='../file_mng/file_AddFile.aspx'; window.close();", True)
        ElseIf (AddAsset = "" Or AddFile = "") Then
            ClientScript.RegisterStartupScript(Me.[GetType](), "myalert", "window.close();", True)
        End If

    End Sub

    Protected Sub butSearchCancel_Click(sender As Object, e As EventArgs) Handles butSearchCancel.Click
        ClientScript.RegisterStartupScript(Me.[GetType](), "myalert", "window.close();", True)
    End Sub
End Class