<%@ Page Language="vb" AutoEventWireup="false" Codebehind="TopPage.aspx.vb" Inherits="AMS.TopPage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>TopPage</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="CommonStyle.css" type="text/css" rel="Stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="frm_TopPage" method="post" runat="server">
			<table width="100%" height="110" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td valign="top" align="center">
						<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td width="100%" valign="top" style="height: 100%">
									<table width="100%" height="75%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td valign="top" width="10%" align="left" style="height: 35px"><div align="left"><img src="../images/AGCLogosmall.JPG"></div>
											</td>
											<td valign="middle" align=left style="height: 35px"><h1>File and Asset Management System</h1>
												Version 1.0</td>
											<td align="right" valign="bottom" style="height: 35px">
												Welcome <strong>
													<asp:Label ID="UsrName" Runat="server"></asp:Label></strong>&nbsp;&nbsp;<br>
													<asp:Label ID="lblTodayDt" Runat="server"></asp:Label>&nbsp;&nbsp;</td>
										</tr>
										<tr><td bgcolor="#ff9900" colspan=3 style="height: 1px">&nbsp;</td></tr>
										<tr>
											<td colspan="3" valign="top" height="1"><img src="../images/spacer.gif" height="1"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
