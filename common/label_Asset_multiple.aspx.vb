Imports System.Data.SqlClient
Imports Neodynamic.WebControls.BarcodeProfessional

Partial Public Class label_Asset_multiple
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Len(Session("UsrID")) = 0 Then
            Dim strJavaScript As String = ""
            strJavaScript = "<script language = 'Javascript'>" & _
                            "parent.location.href='../common/logout.aspx';" & _
                            "</script>"
            Response.Write(strJavaScript)
            Exit Sub
        End If

        Neodynamic.WebControls.BarcodeProfessional.BarcodeProfessional.LicenseOwner = "Ng Ban Loo-Standard Edition-Developer License"
        Neodynamic.WebControls.BarcodeProfessional.BarcodeProfessional.LicenseKey = "4U9TAPBCCXK8V9HNCZHD2XTGSFELFH2B4J4NKLNMPAFST8TJ6E6Q"

        ''Get Asset IDs
        If Trim(Request("AssetIDS")) <> "" Then
            hdnAssetIDs.Value = Request("AssetIDS")
        Else
            hdnAssetIDs.Value = Session("LabelPrintAssetID")
            Session("LabelPrintAssetID") = ""
        End If

        ''Get Record from database
        DLBarcode.DataSource = clsAsset.fnBarcode_GetAssetDetails(hdnAssetIDs.Value)
        DLBarcode.DataBind()

        If Trim(hdnAssetIDs.Value) <> "" And Trim(Request("AssetIDS")) = "" Then
            'hdnAssetIDsSelected.Value = Trim(strAssetIDs)
            'hdnPrintRFIDLabelF.Value = "Y"
            ''fnPopulateRecords()

            clsRFIDLabel.fnCreateRFIDLabelAsset(hdnAssetIDs.Value, Session("UsrID"), _
                                               Server.MapPath(gRFIDLabelPath), "")

            ''*** send to Printing
            'fnPrintRFIDLabelFile(strFileNameLabel)

            ''Insert Audit Trail
            clsCommon.fnAuditInsertRec(Session("UsrID"), hdnAssetIDs.Value.ToString().Replace("^", ","), "RFID passive label printed.")

        End If

        ''Just for Let the Barcode Exist (DON'T Delete it.)
        Dim barcodes As String() = {"3215450", "5002451001"}
        DataList1.DataSource = barcodes
        DataList1.DataBind()


        ''*** Start : Update Label Printed Status
        clsAsset.fnBarcode_UpdatePrintLabelStatus( _
                            hdnAssetIDs.Value, Session("UsrID"))

        'Dim intRetVal As Integer = "0"
        'intRetVal = clsAsset.fnBarcode_UpdatePrintLabelStatus( _
        '                    hdnAssetIDs.Value, Session("UsrID"))

        'If Not intRetVal > 0 Then
        '    lblErrorMessage.Text = "Label Printed Status Updated Failed."
        '    lblErrorMessage.Visible = True
        'End If
        ''*** End   : Update Label Printed Status
    End Sub

End Class