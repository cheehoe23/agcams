#Region "Project Information Section"
' ****************************************************************************************************
' Description       : Common Functions.
' Purpose           : This file provides common functions accessible by other programs.
' Date			    : 25.01.2007
' ****************************************************************************************************
#End Region

#Region "Options Section"
Option Strict Off
Option Explicit On 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data             ' Imports the Data class.
Imports System.Data.SqlClient   ' Imports the SQL Client class.
''Imports System.Web.Mail         ' Imports the mail class.
Imports System.IO
Imports System.Net.Mail
#End Region

Module modCommonFunction
    Public Function fnPopulateSmartShelfLightType(ByVal objDDList As DropDownList ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To populate Smart Shelf Light Type in Drop Down List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 29.03.2009
        ' ****************************************************************************************************
        Try
            objDDList.Items.Add(New ListItem("", "0"))
            objDDList.Items.Add(New ListItem("Red LED", "1"))
            objDDList.Items.Add(New ListItem("Green LED", "2"))
            objDDList.Items.Add(New ListItem("Red LED and Buzzer", "3"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateSecurityStatusInDDL( _
                                    ByVal objDropDownList As DropDownList _
                                    ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add Security Status Value in DropDownList.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 26.03.2007
        ' ****************************************************************************************************
        Try
            Dim intPage As Integer
            objDropDownList.Items.Add(New ListItem("", "-1"))
            objDropDownList.Items.Add(New ListItem("Confidential", "2"))
            objDropDownList.Items.Add(New ListItem("Non-Confidential", "3"))
            objDropDownList.Items.Add(New ListItem("Others", "1"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub fnClearTempCategoryFile( _
                            ByVal strFileLoc As String, ByVal intUsrID As Integer)
        ' ****************************************************************************************************
        ' Purpose  		    : To clear temp file for Category File.
        ' Returns		    : Boolean
        ' Date			    : 06.03.2009
        ' ****************************************************************************************************
        Try
            Dim retval As Integer = "0"
            Dim strFileNameDels As String = ""
            Dim strFileLocation As String = strFileLoc
            Dim strFileLocationDel As String = ""
            Dim arrFileName() As String
            Dim intCurrentFileDel As Integer = 0

            ''Start: Delete selected records from database
            retval = clsCategory.fnCategorySub_DeleteTempRec(intUsrID, strFileNameDels)
            ''End  : Delete selected records from database

            ''Start: Message Notification
            'If retval > 0 And Trim(strFileNameDels) <> "" Then
            '    arrFileName = Split(strFileNameDels, "^")
            '    For intCurrentFileDel = 0 To UBound(arrFileName) - 1
            '        strFileLocationDel = strFileLocation + "\" + arrFileName(intCurrentFileDel)

            '        ''Delete File in AssetFile folder
            '        If File.Exists(strFileLocationDel) Then
            '            File.Delete(strFileLocationDel)
            '        End If
            '    Next
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnCheckNull(ByVal strDBVal) As String
        Try
            If IsDBNull(strDBVal) Then
                fnCheckNull = ""
            Else
                fnCheckNull = strDBVal.ToString
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function mfnPopulateTransactionTypeDDL(ByVal objDDList As DropDownList) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add NFS Transaction Type Value in Drop Down List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 03.01.2009
        ' ****************************************************************************************************
        Try
            objDDList.Items.Add(New ListItem("", ""))
            objDDList.Items.Add(New ListItem("ADD", "ADD"))
            objDDList.Items.Add(New ListItem("ADJ", "ADJ"))
            objDDList.Items.Add(New ListItem("RCT", "RCT"))
            objDDList.Items.Add(New ListItem("REI", "REI"))
            objDDList.Items.Add(New ListItem("RET", "RET"))
            objDDList.Items.Add(New ListItem("TRF-IN", "TRF-IN"))
            objDDList.Items.Add(New ListItem("TRF-OUT", "TRF-OUT"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateDisposalMethodInDDL( _
                                                    ByVal objDDList As DropDownList _
                                                ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add Disposal Method Value in Drop Down List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 12.11.2008
        ' ****************************************************************************************************
        Try
            objDDList.Items.Add(New ListItem("", "0"))
            objDDList.Items.Add(New ListItem("Donation to another Department", "1"))
            objDDList.Items.Add(New ListItem("Donation to a non-Government body or Statutory Board", "2"))
            objDDList.Items.Add(New ListItem("Normal Sale", "3"))
            objDDList.Items.Add(New ListItem("Trade-in", "4"))
            objDDList.Items.Add(New ListItem("Write-off", "5"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function mfnGetContractIDFormat( _
                        ByVal strContractID As String) As String
        ' ****************************************************************************************************
        ' Purpose  		    : To Get contract ID Format
        ' Parameters	    : Contract Id
        ' Returns		    : String
        ' Date			    : 05.11.2007
        ' ****************************************************************************************************
        Try
            Dim strContractIDR As String = ""
            strContractIDR = strContractID.PadLeft(6, "0")

            Return strContractIDR
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub fnEmail4AssetTranser(ByVal strAssetIDs As String, _
                                    ByVal strNewDeptName As String, _
                                    ByVal strNewLocName As String, _
                                    ByVal strNewLocSubName As String, _
                                    ByVal strNewOwnerName As String)
        Try
            Dim objRdr As SqlDataReader
            Dim strEmailActiveF As String = "N"
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""
            Dim strEmailSend2F As String = ""
            Dim strEmailSend2InvenCtrl As String = ""
            Dim strEmailSend2Owner As String = ""
            Dim strEmailSend2Others As String = ""
            Dim strAssetBarcode As String = ""

            Dim strEmailTo As String = ""   '"seesiew@raptech.com.sg"
            Dim strMailBody As String = ""
            Dim arrSendTo() As String

            ''*** Get Email Details from database
            objRdr = clsEmailInfo.fnEmail_GetEmailInfo4AssetTransfer(strAssetIDs)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()
                    strEmailActiveF = CStr(objRdr("EmailActiveF"))
                    strMailSubject = CStr(objRdr("EmailSubject"))
                    strEmailContent = objRdr("EmailContent")
                    strEmailSend2F = CStr(objRdr("EmailSend2F"))
                    strEmailSend2InvenCtrl = CStr(objRdr("EmailSend2InvenCtrl"))
                    strEmailSend2Owner = CStr(objRdr("EmailSend2Owner"))
                    strEmailSend2Others = objRdr("EmailSend2Others")
                    strAssetBarcode = CStr(objRdr("AssetBarcode"))
                End If
            End If
            objRdr.Close()

            ''*** If Email Active, checking for send email
            If strEmailActiveF = "Y" Then
                ''1. Get Email Send To
                arrSendTo = Split(strEmailSend2F, "|")

                ''--> Send for Inventory Controller
                If arrSendTo(0) = "Y" Then
                    strEmailTo += Trim(strEmailSend2InvenCtrl) + ";"
                End If

                ''--> Send for Owner
                If arrSendTo(1) = "Y" Then
                    strEmailTo += Trim(strEmailSend2Owner) + ";"
                End If

                ''--> Send for Others
                If arrSendTo(2) = "Y" Then
                    strEmailTo += Trim(strEmailSend2Others)
                End If

                ''--> remove duplicate email ids in the To List.
                strEmailTo = fnRemoveNameFromEmailAdd(strEmailTo)
                strEmailTo = fnRemoveDuplicates(strEmailTo, ";")

                ''2. Get Email Body
                strMailBody = "Dear Sir/Madam,<br><br><br>"
                strMailBody += strEmailContent
                strMailBody += "<BR><br>Following are asset Details that have been transfer :- "
                strMailBody += "<br>Asset ID : " + strAssetBarcode
                strMailBody += "<br>Transfer to "
                strMailBody += "<br>   Department : " + strNewDeptName
                strMailBody += "<br>   Location : " + strNewLocName
                strMailBody += "<br>   Sub Location : " + strNewLocSubName
                strMailBody += "<br>   Owner : " + strNewOwnerName
                strMailBody += "<BR><br><br>Thanks and Regards,"
                strMailBody += "<BR>File and Asset Management System"

                ''3. Send Email
                If Trim(strEmailTo) <> "" Then
                    fnSendEmail(strEmailTo, "", "", strMailSubject, strMailBody, "", "N")
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnEmail4AssetCreationAMS(ByVal strAssetIDs As String)
        Try
            Dim objRdr As SqlDataReader
            Dim strEmailActiveF As String = "N"
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""
            Dim strEmailSend2F As String = ""
            Dim strEmailSend2InvenCtrl As String = ""
            Dim strEmailSend2Owner As String = ""
            Dim strEmailSend2Others As String = ""
            Dim strAssetBarcode As String = ""

            Dim strEmailTo As String = ""   '"seesiew@raptech.com.sg"
            Dim strMailBody As String = ""
            Dim arrSendTo() As String

            ''*** Get Email Details from database
            objRdr = clsEmailInfo.fnEmail_GetEmailInfo4NewAssetAMS(strAssetIDs)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()
                    strEmailActiveF = CStr(objRdr("EmailActiveF"))
                    strMailSubject = CStr(objRdr("EmailSubject"))
                    strEmailContent = objRdr("EmailContent")
                    strEmailSend2F = CStr(objRdr("EmailSend2F"))
                    strEmailSend2InvenCtrl = CStr(objRdr("EmailSend2InvenCtrl"))
                    strEmailSend2Owner = CStr(objRdr("EmailSend2Owner"))
                    strEmailSend2Others = objRdr("EmailSend2Others")
                    strAssetBarcode = CStr(objRdr("AssetBarcode"))
                End If
            End If
            objRdr.Close()

            ''*** If Email Active, checking for send email
            If strEmailActiveF = "Y" Then
                ''1. Get Email Send To
                arrSendTo = Split(strEmailSend2F, "|")

                ''--> Send for Inventory Controller
                If arrSendTo(0) = "Y" Then
                    strEmailTo += Trim(strEmailSend2InvenCtrl) + ";"
                End If

                ''--> Send for Owner
                If arrSendTo(1) = "Y" Then
                    strEmailTo += Trim(strEmailSend2Owner) + ";"
                End If

                ''--> Send for Others
                If arrSendTo(2) = "Y" Then
                    strEmailTo += Trim(strEmailSend2Others) + ";"
                End If

                ''--> remove duplicate email ids in the To List.
                strEmailTo = fnRemoveNameFromEmailAdd(strEmailTo)
                strEmailTo = fnRemoveDuplicates(strEmailTo, ";")

                ''2. Get Email Body
                strMailBody = "Dear Sir/Madam,<br><br><br>"
                strMailBody += strEmailContent
                strMailBody += "<BR><br>New asset(s) created are " + strAssetBarcode
                strMailBody += "<BR><br><br>Thanks and Regards,"
                strMailBody += "<BR>File and Asset Management System"

                ''3. Send Email
                If Trim(strEmailTo) <> "" Then
                    fnSendEmail(strEmailTo, "", "", strMailSubject, strMailBody, "", "N")
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnRemoveNameFromEmailAdd(ByVal strEmailAdd As String) As String
        Try
            Dim arrEmailAdd As Array
            Dim intCount As Integer = "0"
            Dim strNewEmail As String = ""
            Dim strCurEmail As String = ""

            If Trim(strEmailAdd) <> "" Then
                arrEmailAdd = Split(strEmailAdd, ";")
                For intCount = 0 To UBound(arrEmailAdd) - 1
                    If Trim(arrEmailAdd(intCount)) <> "" Then
                        strCurEmail = Trim(arrEmailAdd(intCount))
                        If Not (InStr(1, strCurEmail, "<", CompareMethod.Text) = 0 Or InStr(1, strCurEmail, ">", CompareMethod.Text) = 0) Then
                            strNewEmail = strNewEmail + strCurEmail.Substring(InStr(strCurEmail, "<"), (InStr(strCurEmail, ">") - InStr(strCurEmail, "<") - 1)) + ";"
                        Else
                            strNewEmail = strNewEmail + strCurEmail + ";"
                        End If
                    End If
                Next
            End If

            fnRemoveNameFromEmailAdd = strNewEmail
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnRemoveDuplicates2( _
                    ByVal strRemoveString As String, _
                    ByVal strDelimString As String, _
                    ByVal strDelimiter As String _
                    ) As String
        ' ****************************************************************************************************
        'Purpose   :    Removes duplicate values from a delimited string.
        'Inputs    :    strDelimString              The delimited string to remove the duplicate values from.
        '               strDelimiter                The string delimiter.
        'Outputs   :    Returns the input string "strDelimString" with any duplicate values removed.
        ' ****************************************************************************************************

        Dim arrValues() As String
        Dim intThisValue As Integer
        Dim strExistingValues As String = ""
        Dim blnEndDelim As Boolean

        If Len(strDelimString) > 0 Then
            'Check if there is an end delimiter
            blnEndDelim = Right$(strDelimString, Len(strDelimiter)) = strDelimiter
            'Split the string into an array
            arrValues = Split(strDelimString, strDelimiter)
            'Loop over the items in the string
            For intThisValue = 0 To UBound(arrValues)
                'Check if the item is already in the string
                If InStr(1, strRemoveString & fnRemoveDuplicates2, arrValues(intThisValue) & strDelimiter, CompareMethod.Text) = 0 Then
                    'The value is unique, add it to the results
                    fnRemoveDuplicates2 = fnRemoveDuplicates2 & arrValues(intThisValue) & strDelimiter
                End If
            Next
            If blnEndDelim = False Then
                'Remove final delimiter
                fnRemoveDuplicates2 = Left(fnRemoveDuplicates2, Len(fnRemoveDuplicates2) - Len(strDelimiter))
            End If
        End If
    End Function 'fnRemoveDuplicates

    Public Function fnRemoveDuplicates( _
                    ByVal strDelimString As String, _
                    ByVal strDelimiter As String _
                    ) As String
        ' ****************************************************************************************************
        'Purpose   :    Removes duplicate values from a delimited string.
        'Inputs    :    strDelimString              The delimited string to remove the duplicate values from.
        '               strDelimiter                The string delimiter.
        'Outputs   :    Returns the input string "strDelimString" with any duplicate values removed.
        ' ****************************************************************************************************

        Dim arrValues() As String
        Dim intThisValue As Integer
        Dim strExistingValues As String = ""
        Dim blnEndDelim As Boolean

        If Len(strDelimString) > 0 Then
            'Check if there is an end delimiter
            blnEndDelim = Right$(strDelimString, Len(strDelimiter)) = strDelimiter
            'Split the string into an array
            arrValues = Split(strDelimString, strDelimiter)
            'Loop over the items in the string
            For intThisValue = 0 To UBound(arrValues)
                'Check if the item is already in the string
                If InStr(1, fnRemoveDuplicates, arrValues(intThisValue) & strDelimiter, CompareMethod.Text) = 0 Then
                    'The value is unique, add it to the results
                    fnRemoveDuplicates = fnRemoveDuplicates & arrValues(intThisValue) & strDelimiter
                End If
            Next
            If blnEndDelim = False Then
                'Remove final delimiter
                fnRemoveDuplicates = Left(fnRemoveDuplicates, Len(fnRemoveDuplicates) - Len(strDelimiter))
            End If
        End If
    End Function 'fnRemoveDuplicates

    Public Sub fnClearTempContractFile( _
                            ByVal strFileLoc As String, _
                            ByVal intUsrID As Integer)
        ' ****************************************************************************************************
        ' Purpose  		    : To clear temp file for Contract File.
        ' Returns		    : Boolean
        ' Date			    : 05.11.2007
        ' ****************************************************************************************************
        Try
            Dim retval As Integer = "0"
            Dim strFileNameDels As String = ""
            Dim strFileLocation As String = strFileLoc
            Dim strFileLocationDel As String = ""
            Dim arrFileName() As String
            Dim intCurrentFileDel As Integer = 0

            ''Start: Delete selected records from database
            retval = clsContract.fnContractFile_DeleteRec(intUsrID, 0, "CA", strFileNameDels)
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 And Trim(strFileNameDels) <> "" Then
                arrFileName = Split(strFileNameDels, "*")
                For intCurrentFileDel = 0 To UBound(arrFileName) - 1
                    strFileLocationDel = strFileLocation + "\" + arrFileName(intCurrentFileDel)

                    ''Delete File in AssetFile folder
                    If File.Exists(strFileLocationDel) Then
                        File.Delete(strFileLocationDel)
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnClearTempStatusFile( _
                            ByVal strFileLoc As String, _
                            ByVal intUsrID As Integer)
        ' ****************************************************************************************************
        ' Purpose  		    : To clear temp file for Status File.
        ' Returns		    : Boolean
        ' Date			    : 29.10.2007
        ' ****************************************************************************************************
        Try
            Dim retval As Integer = "0"
            Dim strFileNameDels As String = ""
            Dim strFileLocation As String = strFileLoc
            Dim strFileLocationDel As String = ""
            Dim arrFileName() As String
            Dim intCurrentFileDel As Integer = 0

            ''Start: Delete selected records from database
            retval = clsAsset.fnStatusFileDeleteRec(intUsrID, 0, "CA", strFileNameDels)
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 And Trim(strFileNameDels) <> "" Then
                arrFileName = Split(strFileNameDels, "*")
                For intCurrentFileDel = 0 To UBound(arrFileName) - 1
                    strFileLocationDel = strFileLocation + "\" + arrFileName(intCurrentFileDel)

                    ''Delete File in AssetFile folder
                    If File.Exists(strFileLocationDel) Then
                        File.Delete(strFileLocationDel)
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnClearTempAssetFile( _
                        ByVal strFileLoc As String, _
                        ByVal intUsrID As Integer)
        ' ****************************************************************************************************
        ' Purpose  		    : To clear temp file for Asset File.
        ' Returns		    : Boolean
        ' Date			    : 25.10.2007
        ' ****************************************************************************************************
        Try
            Dim retval As Integer = "0"
            Dim strFileNameDels As String = ""
            Dim strFileLocation As String = strFileLoc
            Dim strFileLocationDel As String = ""
            Dim arrFileName() As String
            Dim intCurrentFileDel As Integer = 0

            ''Start: Delete selected records from database
            retval = clsAsset.fnAssetFileDeleteRec(intUsrID, 0, "CA", strFileNameDels)
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 And Trim(strFileNameDels) <> "" Then
                arrFileName = Split(strFileNameDels, "^")
                For intCurrentFileDel = 0 To UBound(arrFileName) - 1
                    strFileLocationDel = strFileLocation + "\" + arrFileName(intCurrentFileDel)

                    ''Delete File in AssetFile folder
                    If File.Exists(strFileLocationDel) Then
                        File.Delete(strFileLocationDel)
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnGetSubCatAddInfo( _
        ByVal strAInfo1 As String, ByVal strAInfo2 As String, ByVal strAInfo3 As String, ByVal strAInfo4 As String, ByVal strAInfo5 As String, _
        ByVal strAInfoM1 As String, ByVal strAInfoM2 As String, ByVal strAInfoM3 As String, ByVal strAInfoM4 As String, ByVal strAInfoM5 As String, _
        ByRef strNAInfo1 As String, ByRef strNAInfo2 As String, ByRef strNAInfo3 As String, ByRef strNAInfo4 As String, ByRef strNAInfo5 As String, _
        ByRef strNAInfoM1 As String, ByRef strNAInfoM2 As String, ByRef strNAInfoM3 As String, ByRef strNAInfoM4 As String, ByRef strNAInfoM5 As String, _
        ByRef strAddInfos As String) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To get Additional Information for Subcategory
        ' Parameters	    : string
        ' Returns		    : Boolean
        ' Date			    : 23.10.2007
        ' ****************************************************************************************************
        Try
            Dim intCount As Integer = "0"
            Dim strCurrentAddInfo As Integer = "1"

            ''** Get New Additional Information
            For intCount = 1 To 5
                Select Case intCount
                    Case "1"
                        If strAInfo1 <> "" Then
                            strNAInfo1 = strAInfo1
                            strNAInfoM1 = strAInfoM1
                            strCurrentAddInfo = strCurrentAddInfo + 1
                            strAddInfos += strAInfo1 & " (" & IIf(strAInfoM1 = "Y", "M", "O") & "), "
                        End If
                    Case "2"
                        If strAInfo2 <> "" Then
                            If strCurrentAddInfo = "1" Then
                                strNAInfo1 = strAInfo2
                                strNAInfoM1 = strAInfoM2
                            Else
                                strNAInfo2 = strAInfo2
                                strNAInfoM2 = strAInfoM2
                            End If
                            strCurrentAddInfo = strCurrentAddInfo + 1
                            strAddInfos += strAInfo2 & " (" & IIf(strAInfoM2 = "Y", "M", "O") & "), "
                        End If

                    Case "3"
                        If strAInfo3 <> "" Then
                            If strCurrentAddInfo = "1" Then
                                strNAInfo1 = strAInfo3
                                strNAInfoM1 = strAInfoM3
                            ElseIf strCurrentAddInfo = "2" Then
                                strNAInfo2 = strAInfo3
                                strNAInfoM2 = strAInfoM3
                            Else
                                strNAInfo3 = strAInfo3
                                strNAInfoM3 = strAInfoM3
                            End If
                            strCurrentAddInfo = strCurrentAddInfo + 1
                            strAddInfos += strAInfo3 & " (" & IIf(strAInfoM3 = "Y", "M", "O") & "), "
                        End If

                    Case "4"
                        If strAInfo4 <> "" Then
                            If strCurrentAddInfo = "1" Then
                                strNAInfo1 = strAInfo4
                                strNAInfoM1 = strAInfoM4
                            ElseIf strCurrentAddInfo = "2" Then
                                strNAInfo2 = strAInfo4
                                strNAInfoM2 = strAInfoM4
                            ElseIf strCurrentAddInfo = "3" Then
                                strNAInfo3 = strAInfo4
                                strNAInfoM3 = strAInfoM4
                            Else
                                strNAInfo4 = strAInfo4
                                strNAInfoM4 = strAInfoM4
                            End If
                            strCurrentAddInfo = strCurrentAddInfo + 1
                            strAddInfos += strAInfo4 & " (" & IIf(strAInfoM4 = "Y", "M", "O") & "), "
                        End If

                    Case "5"
                        If strAInfo5 <> "" Then
                            If strCurrentAddInfo = "1" Then
                                strNAInfo1 = strAInfo5
                                strNAInfoM1 = strAInfoM5
                            ElseIf strCurrentAddInfo = "2" Then
                                strNAInfo2 = strAInfo5
                                strNAInfoM2 = strAInfoM5
                            ElseIf strCurrentAddInfo = "3" Then
                                strNAInfo3 = strAInfo5
                                strNAInfoM3 = strAInfoM5
                            ElseIf strCurrentAddInfo = "4" Then
                                strNAInfo4 = strAInfo5
                                strNAInfoM4 = strAInfoM5
                            Else
                                strNAInfo5 = strAInfo5
                                strNAInfoM5 = strAInfoM5
                            End If
                            strCurrentAddInfo = strCurrentAddInfo + 1
                            strAddInfos += strAInfo5 & " (" & IIf(strAInfoM5 = "Y", "M", "O") & "), "
                        End If
                End Select
            Next
            If Trim(strAddInfos) <> "" Then strAddInfos = Trim(strAddInfos).Substring(0, Trim(strAddInfos).Length - 1)

            ''Assign "N" for Optional value
            strNAInfoM1 = IIf(strNAInfoM1 = "", "N", strNAInfoM1)
            strNAInfoM2 = IIf(strNAInfoM2 = "", "N", strNAInfoM2)
            strNAInfoM3 = IIf(strNAInfoM3 = "", "N", strNAInfoM3)
            strNAInfoM4 = IIf(strNAInfoM4 = "", "N", strNAInfoM4)
            strNAInfoM5 = IIf(strNAInfoM5 = "", "N", strNAInfoM5)
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateNumericOptionInDDL( _
                                                    ByVal objDDList As DropDownList _
                                                ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add Numeric Option Value in Drop Down List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 18.10.2007
        ' ****************************************************************************************************
        Try
            objDDList.Items.Add(New ListItem(">", "1"))
            objDDList.Items.Add(New ListItem("<", "2"))
            objDDList.Items.Add(New ListItem(">=", "3"))
            objDDList.Items.Add(New ListItem("<=", "4"))
            objDDList.Items.Add(New ListItem("=", "5"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateTextOptionInDDL( _
                                                ByVal objDDList As DropDownList _
                                            ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add Text Option Value in Drop Down List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 18.10.2007
        ' ****************************************************************************************************
        Try
            objDDList.Items.Add(New ListItem("Like", "1"))
            objDDList.Items.Add(New ListItem("Exactly", "2"))
            objDDList.Items.Add(New ListItem("Not Equal", "3"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateAssetChoosenInRD( _
                                                ByVal objRadioButList As RadioButtonList _
                                                ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add Asset Choosen Value in Radio Button List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 26.09.2007
        ' ****************************************************************************************************
        Try
            objRadioButList.Items.Add(New ListItem("Single Addition", "S"))
            objRadioButList.Items.Add(New ListItem("Bulk Addition", "B"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateNumberInDDL( _
                                           ByVal objDDList As DropDownList, _
                                           ByVal intNumber As Integer _
                                           ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To populate number in Drop Down List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 08.05.2007
        ' ****************************************************************************************************
        Try
            Dim intCount As Integer
            For intCount = 0 To intNumber
                objDDList.Items.Add(New ListItem(CStr(intCount), CStr(intCount)))
            Next
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateAssetFromInDDL( _
                                                ByVal objDDList As DropDownList _
                                            ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add Asset Type Value in Drop Down List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 26.09.2007
        ' ****************************************************************************************************
        Try
            objDDList.Items.Add(New ListItem("", ""))
            objDDList.Items.Add(New ListItem("AMS", "A"))
            objDDList.Items.Add(New ListItem("NFS", "N"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateAssetStatusInDDL( _
                                                    ByVal objDDList As DropDownList _
                                                    ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add Asset Status Value in Drop Down List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 26.09.2007
        ' ****************************************************************************************************
        Try
            objDDList.Items.Add(New ListItem("", "0"))
            objDDList.Items.Add(New ListItem("Active", "A"))
            objDDList.Items.Add(New ListItem("InActive", "I"))
            objDDList.Items.Add(New ListItem("Redundancy", "R"))
            objDDList.Items.Add(New ListItem("Condemnation", "C"))
            objDDList.Items.Add(New ListItem("Dispose", "D"))
            objDDList.Items.Add(New ListItem("Returned", "T"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateYesNoInDDL( _
                                                    ByVal objDDList As DropDownList _
                                                    ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add Asset Type Value in Drop Down List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 26.09.2007
        ' ****************************************************************************************************
        Try
            objDDList.Items.Add(New ListItem("", ""))
            objDDList.Items.Add(New ListItem("Yes", "Y"))
            objDDList.Items.Add(New ListItem("No", "N"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateAssetTempTypeInRD( _
                                                ByVal objRadioButList As RadioButtonList _
                                                ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add Asset Type Value in Radio Button List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 26.09.2007
        ' ****************************************************************************************************
        Try
            objRadioButList.Items.Add(New ListItem("Yes", "Y"))
            objRadioButList.Items.Add(New ListItem("No", "N"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateAssetTypeInDDL( _
                                                ByVal objDDList As DropDownList _
                                            ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add Asset Type Value in Drop Down List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 26.09.2007
        ' ****************************************************************************************************
        Try
            objDDList.Items.Add(New ListItem("", ""))
            objDDList.Items.Add(New ListItem("Fixed Asset", "A"))
            'objDDList.Items.Add(New ListItem("Linked Asset", "K"))
            objDDList.Items.Add(New ListItem("Inventory", "I"))
            objDDList.Items.Add(New ListItem("Leased Inventory", "L"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateAssetTypeInRD( _
                                            ByVal objRadioButList As RadioButtonList _
                                            ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add Asset Type Value in Radio Button List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 26.09.2007
        ' ****************************************************************************************************
        Try
            objRadioButList.Items.Add(New ListItem("Fixed Asset", "A"))
            'objRadioButList.Items.Add(New ListItem("Linked Asset", "K"))
            objRadioButList.Items.Add(New ListItem("Inventory", "I"))
            objRadioButList.Items.Add(New ListItem("Leased Inventory", "L"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateUserTypeInDDL( _
                                           ByVal objDDList As DropDownList _
                                           ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add User Type Value in Drop Down List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 08.05.2007
        ' ****************************************************************************************************
        Try
            objDDList.Items.Add(New ListItem("", ""))
            objDDList.Items.Add(New ListItem("AMS User", "U"))
            objDDList.Items.Add(New ListItem("Officer", "O"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateUserTypeInRD( _
                                        ByVal objRadioButList As RadioButtonList _
                                        ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add User Type Value in Radio Button List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 29.04.2007
        ' ****************************************************************************************************
        Try
            objRadioButList.Items.Add(New ListItem("AMS User", "U"))
            objRadioButList.Items.Add(New ListItem("Officer", "O"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function mfnGetBarcode( _
                    ByVal strType As String, _
                    ByVal strID As String) As String
        ' ****************************************************************************************************
        ' Purpose  		    : To Get Barcode for File, Location, User
        ' Parameters	    : Type ((A)sset, (L)ocation, (U)ser)
        ' Returns		    : String
        ' Date			    : 16.04.2007
        ' ****************************************************************************************************
        Try
            Dim strBarcode As String = ""

            If strType = "A" Then
                strBarcode = "CL-SC-" + strID.PadRight(10, "0")

            ElseIf strType = "F" Then
                strBarcode = "FB" + Right("0000000" + strID, 7)

            ElseIf strType = "L" Then
                strBarcode = "LB" + Right("0000000" + strID, 7)

            ElseIf strType = "U" Then
                strBarcode = "UB" + Right("0000000" + strID, 7)

            End If

            Return strBarcode
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateAndOrInDDL( _
                                        ByVal objDropDownList As DropDownList _
                                        ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add AND/OR in DropDownList
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 27.03.2007
        ' ****************************************************************************************************
        Try
            Dim intPage As Integer
            objDropDownList.Items.Add(New ListItem("And", "and"))
            objDropDownList.Items.Add(New ListItem("Or", "or"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function mfnGetCurrentDateForTopPage() As String
        ' ****************************************************************************************************
        ' Purpose  		    : To Get Current Date [such as : 3rd March 2007 (Saturday)]
        ' Parameters	    : -
        ' Returns		    : String
        ' ****************************************************************************************************
        Try
            Dim strCurrentDt As String
            Dim strMonth() As String = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}
            Dim strWeekDay() As String = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"}

            ''get Current date in [such as : 3rd March 2007 (Saturday)] format
            ''1. Get day
            'Select Case CStr(Now.Day)
            '    Case "1"
            '        strCurrentDt = CStr(Now.Day) + "st"
            '    Case "2"
            '        strCurrentDt = CStr(Now.Day) + "nd"
            '    Case "3"
            '        strCurrentDt = CStr(Now.Day) + "rd"
            '    Case Else
            '        strCurrentDt = CStr(Now.Day) + "th"
            'End Select

            strCurrentDt = CStr(Now.Day)

            ''2. Get Month
            strCurrentDt += " " + strMonth(CStr(Now.Month) - 1)

            ''3. Get Year
            strCurrentDt += " " + CStr(Now.Year)

            ''4. Get Week Day
            strCurrentDt += " (" + strWeekDay(CStr(Now.DayOfWeek)) + ")"

            Return strCurrentDt
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function mfnGetCurrentDate() As String
        ' ****************************************************************************************************
        ' Purpose  		    : To Get Current Date
        ' Parameters	    : -
        ' Returns		    : String
        ' ****************************************************************************************************
        Try
            Dim strCurrentDt As String

            ''get Current date in DD/MM/YYYY format
            If CStr(Now.Day).Length = 1 Then
                strCurrentDt = "0"
            End If
            strCurrentDt += Now.Day & "/"

            If CStr(Now.Month).Length = 1 Then
                strCurrentDt += "0"
            End If
            strCurrentDt += Now.Month & "/" & Now.Year

            Return strCurrentDt
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Public Function fnUserEmailInfo( _
    '                            ByVal strUserName As String, _
    '                            ByVal strEmail As String, _
    '                            ByVal strLoginId As String, _
    '                            ByVal strPwd As String, _
    '                            ByVal EmailFrom As String _
    '                            ) As Boolean
    '    ' ****************************************************************************************************
    '    ' Purpose  		    : To send email for user
    '    ' Parameters	    : strUserName, strEmail,strLoginId, strPwd
    '    ' Returns		    : Boolean
    '    ' ****************************************************************************************************
    '    Try
    '        Dim strMailBody As String
    '        Dim strMailSubject As String

    '        ''Email Subject
    '        strMailSubject = "New Account creation in AMS"

    '        ''Email Body
    '        strMailBody = "Dear " & strUserName & ",<br>"
    '        strMailBody += "Your account to Asset Management System had been created. Please login with the following information.<br>"
    '        strMailBody += "Login ID : " & strLoginId & "<br>"
    '        strMailBody += "Password: " & strPwd & "<br><br><br><br><br>"
    '        strMailBody += "Thank you.<br>"

    '        Return fnSendEmail(strEmail, "", "", strMailSubject, strMailBody, EmailFrom)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Public Function fnUserEmailActiveAcc( _
    '                            ByVal strUserName As String, _
    '                            ByVal strEmail As String, _
    '                            ByVal strLoginId As String, _
    '                            ByVal strPwd As String, _
    '                            ByVal EmailFrom As String _
    '                            ) As Boolean
    '    ' ****************************************************************************************************
    '    ' Purpose  		    : To send email for user that account have been active
    '    ' Parameters	    : strUserName, strEmail,strLoginId, strPwd
    '    ' Returns		    : Boolean
    '    ' ****************************************************************************************************
    '    Try
    '        Dim strMailBody As String
    '        Dim strMailSubject As String

    '        ''Email Subject
    '        strMailSubject = "Account had been activated in AMS"

    '        ''Email Body
    '        strMailBody = "Dear " & strUserName & ",<br>"
    '        'strMailBody += "Your account to File Tracking System had been activated. Please login with the following information.<br>"
    '        strMailBody += "You have been change from Officer to AMS User. Please login with the following information.<br>"
    '        strMailBody += "Login ID : " & strLoginId & "<br>"
    '        strMailBody += "Password: " & strPwd & "<br><br><br><br><br>"
    '        strMailBody += "Thank you.<br>"

    '        Return fnSendEmail(strEmail, "", "", strMailSubject, strMailBody, EmailFrom)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Public Function fnUserEmailResetPwd( _
    '                                ByVal strUserName As String, _
    '                                ByVal strEmail As String, _
    '                                ByVal strLoginId As String, _
    '                                ByVal strPwd As String, _
    '                                ByVal EmailFrom As String _
    '                                ) As Boolean
    '    ' ****************************************************************************************************
    '    ' Purpose  		    : To send email for user that Password have been reset.
    '    ' Parameters	    : strUserName, strEmail,strLoginId, strPwd
    '    ' Returns		    : Boolean
    '    ' ****************************************************************************************************
    '    Try
    '        Dim strMailBody As String
    '        Dim strMailSubject As String

    '        ''Email Subject
    '        strMailSubject = "Password had been reseted in AMS"

    '        ''Email Body
    '        strMailBody = "Dear " & strUserName & ",<br>"
    '        strMailBody += "Your password to Asset Management System had been reseted. Please login with the following information.<br>"
    '        strMailBody += "Login ID : " & strLoginId & "<br>"
    '        strMailBody += "Password: " & strPwd & "<br><br><br><br><br>"
    '        strMailBody += "Thank you.<br>"

    '        Return fnSendEmail(strEmail, "", "", strMailSubject, strMailBody, EmailFrom)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Public Function fnUserEmailChangePwd( _
    '                            ByVal strUserName As String, _
    '                            ByVal strEmail As String, _
    '                            ByVal strLoginId As String, _
    '                            ByVal strPwd As String, _
    '                            ByVal EmailFrom As String _
    '                            ) As Boolean
    '    ' ****************************************************************************************************
    '    ' Purpose  		    : To send email for user that Password have been changed.
    '    ' Parameters	    : strUserName, strEmail,strLoginId, strPwd
    '    ' Returns		    : Boolean
    '    ' ****************************************************************************************************
    '    Try
    '        Dim strMailBody As String
    '        Dim strMailSubject As String

    '        ''Email Subject
    '        strMailSubject = "Password had been changed in AMS"

    '        ''Email Body
    '        strMailBody = "Dear " & strUserName & ",<br>"
    '        strMailBody += "Your password to Asset Management System had been changed. Please login with the following information.<br>"
    '        strMailBody += "Login ID : " & strLoginId & "<br>"
    '        strMailBody += "Password: " & strPwd & "<br><br><br><br><br>"
    '        strMailBody += "Thank you.<br>"

    '        Return fnSendEmail(strEmail, "", "", strMailSubject, strMailBody, EmailFrom)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Public Function fnPopulateDropDownListWithDefaultValue( _
                        ByVal objSQLDataReader As SqlDataReader, _
                        ByVal objDropDownList As DropDownList, _
                        ByVal DataValueField As String, _
                        ByVal DataTextField As String, _
                        ByVal blnDefaultSelectedValue As Boolean, _
                        ByVal strDefaultText As String, _
                        ByVal strDefaultValue As String _
                        ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To populate the drop down list with Default Value.
        ' Parameters	    : objSQLDataReader, objDropDownList,
        '                     DataValueField, DataTextField, blnDefaultSelectedValue, strDefaultValue
        ' Returns		    : Boolean
        ' ****************************************************************************************************
        Try
            If objSQLDataReader Is Nothing Then Return False
            If objSQLDataReader.HasRows Then
                With objDropDownList
                    .DataSource = objSQLDataReader
                    .DataTextField = DataTextField
                    .DataValueField = DataValueField
                    .DataBind()
                End With

                ' Add an empty value to the combo if the Default Selected Value is false.
                If Not blnDefaultSelectedValue Then
                    objDropDownList.Items.Insert(0, New ListItem(strDefaultText, strDefaultValue))
                End If
                objSQLDataReader.Close()
                Return True
            Else
                objSQLDataReader.Close()
                Return False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateDropDownList( _
                            ByVal objSQLDataReader As SqlDataReader, _
                            ByVal objDropDownList As DropDownList, _
                            ByVal DataValueField As String, _
                            ByVal DataTextField As String, _
                            ByVal blnDefaultSelectedValue As Boolean _
                            ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To populate the drop down list.
        ' Parameters	    : objSQLDataReader, objDropDownList,
        '                     DataValueField, DataTextField, blnDefaultSelectedValue
        ' Returns		    : Boolean
        ' ****************************************************************************************************
        Try
            If objSQLDataReader Is Nothing Then Return False
            If objSQLDataReader.HasRows Then
                With objDropDownList
                    .DataSource = objSQLDataReader
                    .DataTextField = DataTextField
                    .DataValueField = DataValueField
                    .DataBind()
                End With
                ' Add an empty value to the combo if the Default Selected Value is false.
                If Not blnDefaultSelectedValue Then
                    objDropDownList.Items.Insert(0, New ListItem("", "-1"))
                End If
                objSQLDataReader.Close()
                Return True
            Else
                objSQLDataReader.Close()
                Return False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulatePagesInDDL( _
                                ByVal objDropDownList As DropDownList _
                                ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add the Page size in the DropDownList in 10 pages.
        ' Parameters	    : objDropDownList, blnDefaultSelectedValue 
        ' Returns		    : Boolean
        ' Date			    : 25.01.2007
        ' ****************************************************************************************************
        Try
            Dim intPage As Integer
            'objDropDownList.Items.Add(New ListItem("2", "2"))
            'objDropDownList.Items.Add(New ListItem("5", "5"))

            For intPage = 10 To 50 Step 10
                objDropDownList.Items.Add(New ListItem(CStr(intPage), CStr(intPage)))
            Next
            objDropDownList.Items.Add(New ListItem("100", "100"))
            objDropDownList.Items.Add(New ListItem("All", "1000000"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulatePagesInDDLForGenerateTextFile( _
                                ByVal objDropDownList As DropDownList _
                                ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add the Page size in the DropDownList in 10 pages.
        ' Parameters	    : objDropDownList, blnDefaultSelectedValue 
        ' Returns		    : Boolean
        ' Date			    : 25.01.2007
        ' ****************************************************************************************************
        Try
            Dim intPage As Integer
            'objDropDownList.Items.Add(New ListItem("2", "2"))
            'objDropDownList.Items.Add(New ListItem("5", "5"))

            For intPage = 10 To 50 Step 10
                objDropDownList.Items.Add(New ListItem(CStr(intPage), CStr(intPage)))
            Next
            objDropDownList.Items.Add(New ListItem("100", "100"))
            objDropDownList.Items.Add(New ListItem("150", "150"))
            objDropDownList.Items.Add(New ListItem("200", "200"))
            objDropDownList.Items.Add(New ListItem("250", "250"))
            objDropDownList.Items.Add(New ListItem("300", "300"))
            objDropDownList.Items.Add(New ListItem("350", "350"))
            objDropDownList.Items.Add(New ListItem("400", "400"))
            objDropDownList.Items.Add(New ListItem("450", "450"))
            objDropDownList.Items.Add(New ListItem("500", "500"))
            'objDropDownList.Items.Add(New ListItem("550", "550"))
            'objDropDownList.Items.Add(New ListItem("600", "600"))
            'objDropDownList.Items.Add(New ListItem("650", "650"))
            'objDropDownList.Items.Add(New ListItem("700", "700"))
            'objDropDownList.Items.Add(New ListItem("750", "750"))
            'objDropDownList.Items.Add(New ListItem("800", "800"))
            'objDropDownList.Items.Add(New ListItem("850", "850"))
            'objDropDownList.Items.Add(New ListItem("900", "900"))
            'objDropDownList.Items.Add(New ListItem("950", "950"))
            'objDropDownList.Items.Add(New ListItem("1000", "1000"))
            objDropDownList.Items.Add(New ListItem("All", "1000000"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Public Function fnSendEmail( _
    '                        ByVal EmailTo As String, _
    '                        ByVal EmailCC As String, _
    '                        ByVal EmailBCC As String, _
    '                        ByVal Subject As String, _
    '                        ByVal BodyText As String, _
    '                        ByVal EmailFrom As String, _
    '                        ByVal PriorityType As String) As Boolean
    '    ' ****************************************************************************************************
    '    ' Purpose  		    : To send email to the persons.
    '    ' Returns		    : Boolean
    '    ' Date			    : 25.01.2007
    '    ' ****************************************************************************************************
    '    Try
    '        Dim objMail As New MailMessage
    '        Dim objSMTP As Mail.SmtpMail

    '        ' The SMTP server IP Address.
    '        SmtpMail.SmtpServer = ConfigurationSettings.AppSettings("SmtpServerIP")

    '        ' The email address of the sender.
    '        objMail.From = ConfigurationSettings.AppSettings("EmailUserID") 'Session("uEmail") ' 'EmailFrom

    '        ' The email address of the recipient.
    '        objMail.To = EmailTo   '"seesiew@raptech.com.sg" '

    '        ' The email address of the CC recipient.
    '        objMail.Cc = EmailCC

    '        ' The email address of the BCC recipient.
    '        objMail.Bcc = EmailBCC

    '        ' The format of the message - it can be MailFormat.Text or MailFormat.Html 
    '        objMail.BodyFormat = MailFormat.Html

    '        ' The priority of the message  - it can be High, Normal Or Low.
    '        If PriorityType = "H" Then
    '            objMail.Priority = MailPriority.High
    '        Else
    '            objMail.Priority = MailPriority.Normal
    '        End If

    '        ' The subject of the message 
    '        objMail.Subject = Subject

    '        ' The message text 
    '        objMail.Body = BodyText

    '        ' Send the mail.
    '        SmtpMail.Send(objMail)
    '        Return True

    '    Catch ex As Exception
    '        Return False
    '    End Try
    'End Function

    Public Function fnSendEmail( _
                            ByVal EmailTo As String, _
                            ByVal EmailCC As String, _
                            ByVal EmailBCC As String, _
                            ByVal Subject As String, _
                            ByVal BodyText As String, _
                            ByVal EmailFrom As String, _
                            ByVal PriorityType As String) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To send email to the persons.
        ' Returns		    : Boolean
        ' Date			    : 25.01.2007
        ' ****************************************************************************************************
        ' *********************************************************
        Try
            Dim smtp As New SmtpClient
            Dim msg As MailMessage = New MailMessage()
            Dim configurationAppSettings As New System.Configuration.AppSettingsReader

            'ErrorLog.Insert(msgFrom, 36, 8)
            'ErrorLog.Insert(msgFromName, 36, 8)

            Dim msgFrom As String = configurationAppSettings.GetValue("EmailUserID", System.Type.GetType("System.String"))
            Dim msgFromName As String = configurationAppSettings.GetValue("EmailUserID", System.Type.GetType("System.String"))

            Dim CCEmail() As String
            Dim BCCEmail() As String
            Dim TOEmail() As String

            'ErrorLog.Insert(msgFrom, 36, 8)

            msg.From = New MailAddress(msgFrom, msgFromName)

            If Not String.IsNullOrEmpty(EmailTo) Then
                TOEmail = EmailTo.TrimEnd(";").Split(";"c)
                For Each str As String In TOEmail
                    msg.Bcc.Add(New MailAddress(str))
                Next str
            End If

            If Not String.IsNullOrEmpty(EmailCC) Then
                CCEmail = EmailCC.TrimEnd(";").Split(";"c)
                For Each str As String In CCEmail
                    msg.CC.Add(New MailAddress(str))
                Next str
            End If

            If Not String.IsNullOrEmpty(EmailBCC) Then
                BCCEmail = EmailBCC.TrimEnd(";").Split(";"c)
                For Each str As String In BCCEmail
                    msg.Bcc.Add(New MailAddress(str))
                Next str
            End If

            If PriorityType = "H" Then
                msg.Priority = MailPriority.High
            Else
                msg.Priority = MailPriority.Normal
            End If

            msg.Subject = Subject
            msg.IsBodyHtml = True

            msg.Body = BodyText

            Dim _smtpServerAddress As String = configurationAppSettings.GetValue("SmtpServerIP", System.Type.GetType("System.String"))
            Dim _smtpLogin As String = configurationAppSettings.GetValue("EmailUserID", System.Type.GetType("System.String"))
            Dim _smtpPassword As String = configurationAppSettings.GetValue("EmailPassword", System.Type.GetType("System.String"))
            Dim _port As String = configurationAppSettings.GetValue("MailServerPort", System.Type.GetType("System.Int32"))
            Dim _SSL As String = configurationAppSettings.GetValue("SSLSetting", System.Type.GetType("System.String"))

            smtp.Port = _port
            smtp.Host = _smtpServerAddress
            If _SSL = "1" Then
                smtp.EnableSsl = True
            Else
                smtp.EnableSsl = False
            End If

            '       smtp.UseDefaultCredentials = True

            If _smtpPassword.Trim() <> "" Then
                smtp.Credentials = New System.Net.NetworkCredential(_smtpLogin, _smtpPassword)
            ElseIf _smtpPassword.Trim() = "" Then
                Dim smtpClient As New SmtpClient(configurationAppSettings.GetValue("GatewayName", System.Type.GetType("System.String")), _port) 'for live
                smtpClient.UseDefaultCredentials = False 'for live
            End If

            smtp.Send(msg)

            Return True
        Catch ex As Exception
        End Try
    End Function

    Public Function fnPopulatePrinterLocationDDL( _
                                                    ByVal objDDList As DropDownList _
                                                    ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add Asset Status Value in Drop Down List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 26.09.2007
        ' ****************************************************************************************************
        Try
            objDDList.Items.Add(New ListItem("", "0"))
            objDDList.Items.Add(New ListItem(gAMSPrinterLocLevel7, gAMSPrinterLocLevel7))
            objDDList.Items.Add(New ListItem(gAMSPrinterLocLevel8, gAMSPrinterLocLevel8))
            objDDList.Items.Add(New ListItem(gFTSPrinterLocLevel9, gFTSPrinterLocLevel9))
            objDDList.Items.Add(New ListItem(gFTSPrinterLocL9AMS02, gFTSPrinterLocL9AMS02))
            objDDList.Items.Add(New ListItem(gFTSPrinterLocLevel10, gFTSPrinterLocLevel10))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Module
