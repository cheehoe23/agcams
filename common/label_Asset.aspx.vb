Imports System.Data.SqlClient


Partial Public Class label_Asset
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Len(Session("UsrID")) = 0 Then
            Dim strJavaScript As String = ""
            strJavaScript = "<script language = 'Javascript'>" & _
                            "parent.location.href='../common/logout.aspx';" & _
                            "</script>"
            Response.Write(strJavaScript)
            Exit Sub
        End If

        Neodynamic.WebControls.BarcodeProfessional.BarcodeProfessional.LicenseOwner = "Ng Ban Loo-Standard Edition-Developer License"
        Neodynamic.WebControls.BarcodeProfessional.BarcodeProfessional.LicenseKey = "4U9TAPBCCXK8V9HNCZHD2XTGSFELFH2B4J4NKLNMPAFST8TJ6E6Q"

        ''Get Asset IDs
        hdnAssetIDs.Value = Request("AssetIDS")

        ''Get Record from database
        Dim objRdr As SqlDataReader
        objRdr = clsAsset.fnBarcode_GetAssetDetailsDR(hdnAssetIDs.Value)
        If Not objRdr Is Nothing Then
            If objRdr.HasRows Then
                objRdr.Read()
                lblNFSAssetID.Text = CStr(objRdr("fld_AssetNFSID"))
                lblAMSAssetID.Text = CStr(objRdr("fld_AssetBarcode"))
                lblDeptCode.Text = CStr(objRdr("fld_DeptCode"))
                lblLocCode.Text = CStr(objRdr("fld_LocCode"))
                lblDesc.Text = CStr(objRdr("fld_AssetDesc"))
                'lblDesc.Text = IIf(CStr(objRdr("fld_AssetDesc")).Length > 34, CStr(objRdr("fld_AssetDesc")).Substring(0, 34), CStr(objRdr("fld_AssetDesc")))

                ''** Get Info for barcode
                barcodeCode.Code = CStr(objRdr("fld_AssetBarcode"))

                ''** setting for barcode
                ' ''Resolution
                ''Dim dpi As Single = 300.0F
                ' ''Target size in inches
                ''Dim targetArea As New System.Drawing.SizeF(1.0F, 0.9F)
                ''barcodeCode.GetBarcodeImage(dpi, targetArea)
            End If
        End If
        objRdr.Close()

        'lblNFSAssetID.Text = "00001239"
        'lblAMSAssetID.Text = "CP-0000000001" 'Request("ABarcode")
        'lblDeptCode.Text = "AP"
        'lblLocCode.Text = "6APBR"
        'lblDesc.Text = "This is description of the asset."
        'barcodeCode.Code = Request("ABarcode")
    End Sub

End Class