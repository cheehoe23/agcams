Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Partial Public Class label_AssetRpt
    Inherits System.Web.UI.Page
    Private report As New ReportDocument()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If

            Neodynamic.WebControls.BarcodeProfessional.BarcodeProfessional.LicenseOwner = "Ng Ban Loo-Standard Edition-Developer License"
            Neodynamic.WebControls.BarcodeProfessional.BarcodeProfessional.LicenseKey = "4U9TAPBCCXK8V9HNCZHD2XTGSFELFH2B4J4NKLNMPAFST8TJ6E6Q"


            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                hdnAssetIDs.Value = Request("AssetIDS")

                ''*** Start : Update Label Printed Status
                Dim intRetVal As Integer = "0"
                intRetVal = clsAsset.fnBarcode_UpdatePrintLabelStatus( _
                                    hdnAssetIDs.Value, Session("UsrID"))

                If Not intRetVal > 0 Then
                    lblErrorMessage.Text = "Label Printed Status Updated Failed."
                    lblErrorMessage.Visible = True
                End If
                ''*** End   : Update Label Printed Status
            End If

            'Getting Customers from DB
            'Dim cnn As New SqlConnection(cnFTSDBString)
            'Dim cmd As New SqlCommand()
            'cmd.Connection = cnn
            'cmd.CommandType = CommandType.StoredProcedure
            'cmd.CommandText = "P_Barcode_GetAssetDetails"
            'cmd.Parameters.Add("@strAssetIDs", SqlDbType.VarChar, 4000)
            'cmd.Parameters(0).Value = hdnAssetIDs.Value
            'Dim da As New SqlDataAdapter(cmd)
            'Dim dsAsset As New DataSet()
            'da.Fill(dsAsset)

            ''Get Dataset from DB
            Dim dsAsset As New DataSet()
            dsAsset = clsAsset.fnBarcode_GetAssetDetails(hdnAssetIDs.Value)

            'Create an instance of Barcode Professional
            Dim bcp As New Neodynamic.WebControls.BarcodeProfessional.BarcodeProfessional()
            'Barcode settings
            bcp.Symbology = Neodynamic.WebControls.BarcodeProfessional.Symbology.Code128
            'bcp.Extended = True
            bcp.AddChecksum = False
            bcp.BarHeight = 0.9F
            bcp.QuietZoneWidth = 0
            bcp.DisplayCode = False

            'Resolution
            Dim dpi As Single = 300.0F
            'Target size in inches
            Dim targetArea As New System.Drawing.SizeF(1.0F, 0.9F)

            Dim row As DataRow
            'Update DataTable with barcode image
            For Each row In dsAsset.Tables(0).Rows
                'Set the value to encode
                bcp.Code = row("fld_AssetBarcode").ToString()
                'Generate the barcode image and store it into the Barcode Column (Get the barcode image fitting the target area)
                row("fld_BarcodeImage") = bcp.GetBarcodeImage(System.Drawing.Imaging.ImageFormat.Png, dpi, targetArea)
                'row("fld_BarcodeImage") = bcp.GetBarcodeImage(System.Drawing.Imaging.ImageFormat.Png)
            Next

            'Create a report object
            'and set its data source with the DataSet
            'Dim report As New ReportDocument()
            Dim rptFile As String = Server.MapPath("label_AssetRpt1.rpt")
            report.Load(rptFile)
            'report.SetParameterValue("@strAssetIDs", hdnAssetIDs.Value)
            report.SetDataSource(dsAsset.Tables(0))

            'Show it!
            Me.crvLabelAsset.ReportSource = report

            ''''**** Start :  Set Crystal Report Login Connection ****''''
            'Dim objtbl As CrystalDecisions.CrystalReports.Engine.Table
            'Dim logOnInfo As TableLogOnInfo
            'logOnInfo = New TableLogOnInfo
            'For Each objtbl In report.Database.Tables
            '    '*******************************************
            '    ' For db server configuration.
            '    '*******************************************
            '    logOnInfo.ConnectionInfo.ServerName = ConfigurationSettings.AppSettings("dbServerName")   ' "VIRTUAL"
            '    logOnInfo.ConnectionInfo.UserID = ConfigurationSettings.AppSettings("dbLoginID")       ' "sa"
            '    logOnInfo.ConnectionInfo.Password = ConfigurationSettings.AppSettings("dbLoginPwd")     ' "system"
            '    logOnInfo.ConnectionInfo.DatabaseName = ConfigurationSettings.AppSettings("dbName") '"db_AMS"
            '    objtbl.ApplyLogOnInfo(logOnInfo)
            '    objtbl.Location = ConfigurationSettings.AppSettings("dbName") & ".dbo." & objtbl.Name
            'Next
            ''''**** End  :  Set Crystal Report Login Connection ****''''

            'report.Close()
            'report.Dispose()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            report.Close()
            report.Dispose()

            GC.Collect()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub



End Class