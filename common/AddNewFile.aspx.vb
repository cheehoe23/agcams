#Region "Information Section"
' ****************************************************************************************************
' Description       : Add New File
' Purpose           : Add New File Information
' Author            : See Siew
' Date              : 25/10/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
#End Region

Partial Public Class AddNewFile
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                'Me.butUpload.Attributes.Add("OnClick", "return chkFrm()")

                ''Get Pass Value from Main screen
                hdnCallFrm.Value = Request("CallFrm")            ''Called from "AddAsset" screen OR "EditAsset" screen OR "AddStatusFile" screen
                hdnAssetStatusInfoID.Value = Request("ASIID")    ''Just for Called From "Edit Asset" screen
                hdnABarcode.Value = Request("ABarcode")          ''Just for Called From "Edit Asset" screen
                hdnAssetID.Value = Request("AssetID")          ''Just for Called From "Edit Asset" screen
                hdnAssetStatus.Value = Request("AssetStatus")          ''Just for Called From "Edit Asset" screen
                hdnContractID.Value = Request("ContractID")            ''Just for Called From "Edit Contract" screen

                ''set Add Flag to "N"
                hdnAddFileF.Value = "N"

                divUploadFile.Visible = True
                divUploadSuccess.Visible = False
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Try
            Dim strJavaScript As String
            strJavaScript = "<script language = 'Javascript'>" & _
                            "window.close();" & _
                            "</script>"
            Response.Write(strJavaScript)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            FileUpload.Value = ""
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpload.Click

        Try
            Dim strFileName As String = ""
            Dim strGotError As String = "N"
            Dim strErrorMsg As String = ""
            Dim strSaveLocation As String = ""
            Dim intRetVal As Integer = 0
            Dim strMsg As String = ""
            Dim intFileID As Integer = 0

            ''Validate File
            fnValidateFile(strGotError, strErrorMsg)

            Dim filesize As Decimal = (FileUpload.PostedFile.ContentLength / 1024) / 1024

            If Convert.ToInt32(ConfigurationSettings.AppSettings("filesize")) < filesize Then
                lblErrorMessage.Text = "You cannot upload the file because attachment size is more than " & ConfigurationSettings.AppSettings("filesize").ToString() & " MB."
                lblErrorMessage.Visible = True
                Return
            Else
                lblErrorMessage.Text = ""
                lblErrorMessage.Visible = False
            End If


            If strGotError = "N" Then '' not Duplicate found --> add new record in datatable
                ''Get New File Name
                strFileName = System.IO.Path.GetFileName(FileUpload.PostedFile.FileName)

                If hdnCallFrm.Value = "AddAsset" Then     '' For Called From ADD Asset screen
                    ''Add New File in database
                    intRetVal = clsAsset.fnAssetFileInsertRec( _
                                    Session("UsrID"), 0, strFileName, "Y", intFileID)

                    ''Get Save Location
                    strSaveLocation = Server.MapPath("..\tempFile\AssetFile")

                ElseIf hdnCallFrm.Value = "AddCondemn" Then     '' For Called From Edit Asset screen (Ass Asset File)
                    ''Add New File in database
                    intRetVal = clsDisMng.fnCondemnFileInsertRec( _
                                    Session("UsrID"), 0, strFileName, "Y", intFileID)

                    ''Get Save Location
                    strSaveLocation = Server.MapPath("..\tempFile\CondemnFile")

                ElseIf hdnCallFrm.Value = "AddRedundancy" Then     '' For Called From Edit Asset screen (Ass Asset File)
                    ''Add New File in database
                    intRetVal = clsDisMng.fnRedundancyFileInsertRec( _
                                    Session("UsrID"), 0, strFileName, "Y", intFileID)

                    ''Get Save Location
                    strSaveLocation = Server.MapPath("..\tempFile\RendundancyFile")

                ElseIf hdnCallFrm.Value = "EditAsset" Then     '' For Called From Edit Asset screen (Ass Asset File)
                    ''Add New File in database
                    intRetVal = clsAsset.fnAssetFileInsertRec( _
                                    Session("UsrID"), hdnAssetStatusInfoID.Value, strFileName, "N", intFileID)

                    ''Get Save Location
                    strSaveLocation = Server.MapPath("..\tempFile\AssetFile")

                ElseIf hdnCallFrm.Value = "AddStatusFile" Or hdnCallFrm.Value = "AddStatusRCFileT" Or hdnCallFrm.Value = "AddStatusDFileT" Then     '' For Called From ADD Status (Condemn/Redundancy/Disposed) screen
                    ''Add New File in database
                    intRetVal = clsAsset.fnStatusFileInsertRec( _
                                     Session("UsrID"), 0, strFileName, "Y", intFileID)

                    ''Get Save Location
                    strSaveLocation = Server.MapPath("..\tempFile\AssetStatusFile")

                ElseIf hdnCallFrm.Value = "AddStatusRCFileP" Or hdnCallFrm.Value = "AddStatusDFileP" Then     '' For Called From Edit Asset screen (Add Status File permanently)
                    ''Add New File in database
                    intRetVal = clsAsset.fnStatusFileInsertRec( _
                                     Session("UsrID"), hdnAssetStatusInfoID.Value, strFileName, "N", intFileID)

                    ''Get Save Location
                    strSaveLocation = Server.MapPath("..\tempFile\AssetStatusFile")

                ElseIf hdnCallFrm.Value = "AddContract" Or hdnCallFrm.Value = "RenewalContract" Then     '' For Called From ADD Contract screen
                    ''Add New File in database
                    intRetVal = clsContract.fnContractFile_InsertRec( _
                                    Session("UsrID"), 0, strFileName, "Y", intFileID)

                    ''Get Save Location
                    strSaveLocation = Server.MapPath("..\tempFile\ContractFile")

                ElseIf hdnCallFrm.Value = "EditContract" Then     '' For Called From Edit Contract screen 
                    ''Add New File in database
                    intRetVal = clsContract.fnContractFile_InsertRec( _
                                    Session("UsrID"), hdnContractID.Value, strFileName, "N", intFileID)

                    ''Get Save Location
                    strSaveLocation = Server.MapPath("..\tempFile\ContractFile")
                End If

                If intRetVal > 0 Then
                    ''save file
                    strSaveLocation = strSaveLocation & "\" & CStr(intFileID) & "_" & strFileName
                    FileUpload.PostedFile.SaveAs(strSaveLocation)

                    hdnAddFileF.Value = "Y"
                    divUploadFile.Visible = False
                    divUploadSuccess.Visible = True

                    If hdnCallFrm.Value = "EditAsset" Or hdnCallFrm.Value = "AddStatusRCFileP" Or hdnCallFrm.Value = "AddStatusDFileP" Or hdnCallFrm.Value = "EditContract" Then
                        If hdnCallFrm.Value = "EditAsset" Then
                            ''Insert Audit Trail
                            clsCommon.fnAuditInsertRec(Session("UsrID"), hdnAssetStatusInfoID.Value + "^", "<font class=DisplayAudit>[Asset Images Added]</font> <br>Asset ID : " + hdnABarcode.Value + "<br>File Name : " + strFileName)

                        ElseIf hdnCallFrm.Value = "AddStatusRCFileP" Or hdnCallFrm.Value = "AddStatusDFileP" Then
                            Dim strFileStatus As String = ""
                            If hdnAssetStatus.Value = "R" Then
                                strFileStatus = "Redundancy"
                            ElseIf hdnAssetStatus.Value = "C" Then
                                strFileStatus = "Condemnation"
                            ElseIf hdnAssetStatus.Value = "D" Then
                                strFileStatus = "Disposed"
                            End If

                            ''Insert Audit Trail
                            clsCommon.fnAuditInsertRec(Session("UsrID"), hdnAssetID.Value + "^", "<font class=DisplayAudit>[" + strFileStatus + " File Added]</font> <br>Asset ID : " + hdnABarcode.Value + "<br>File Name : " + strFileName)

                        ElseIf hdnCallFrm.Value = "EditContract" Then
                            ''Insert Audit Trail
                            clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "<font class=DisplayAudit>[Contract File Added]</font> <br>Contract ID : " + mfnGetContractIDFormat(CStr(hdnContractID.Value)) + "<br>File Name : " + strFileName)

                        End If

                        ''Message Notification
                        Dim strJavaScript As String
                        strJavaScript = "<script language = 'Javascript'>" & _
                                        "alert('File add successfully.');" & _
                                        "</script>"
                        Response.Write(strJavaScript)
                    End If
                Else
                    strMsg = "File upload failed. Please contact your Administrator."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If
            Else   '' Error Found --> show error message
                lblErrorMessage.Text = strErrorMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnValidateFile( _
                            ByRef strGotError As String, _
                            ByRef strErrorMsg As String)
        Try
            ''** Checking whether File is browse
            If strGotError = "N" And Not (Not FileUpload.PostedFile Is Nothing And FileUpload.PostedFile.ContentLength > 0) Then
                strGotError = "Y"
                strErrorMsg = "Please select a file to upload."
            End If

            ''Check whether file exist
            'If strGotError = "N" Then
            '    If Not File.Exists(FileUpload.PostedFile.ToString) Then
            '        strGotError = "Y"
            '        strErrorMsg = "File not exist. Please make sure file location is correct."
            '    End If
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub butAddOther_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAddOther.Click
        Try
            hdnAddFileF.Value = "N"
            divUploadFile.Visible = True
            divUploadSuccess.Visible = False
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butCloseWin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCloseWin.Click
        Try
            Dim strJavaScript As String
            strJavaScript = "<script language = 'Javascript'>" & _
                            "window.close();" & _
                            "</script>"
            Response.Write(strJavaScript)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class