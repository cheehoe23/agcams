#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region


Partial Class label_file
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents butClose As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Me.butClose.Attributes.Add("OnClick", "return fnCloseWin()")
        If Len(Session("UsrID")) = 0 Then
            Dim strJavaScript As String = ""
            strJavaScript = "<script language = 'Javascript'>" & _
                            "parent.location.href='../common/logout.aspx';" & _
                            "</script>"
            Response.Write(strJavaScript)
            Exit Sub
        End If

        Neodynamic.WebControls.BarcodeProfessional.BarcodeProfessional.LicenseOwner = "Ng Ban Loo-Standard Edition-Developer License"
        Neodynamic.WebControls.BarcodeProfessional.BarcodeProfessional.LicenseKey = "4U9TAPBCCXK8V9HNCZHD2XTGSFELFH2B4J4NKLNMPAFST8TJ6E6Q"

        ''*** Start : Comment Out by CSS, 17 Aug 2009
        'Dim strVol As String
        'If Request("FVol").Length = "1" Then
        '    strVol = "00" + Request("FVol")
        'ElseIf Request("FVol").Length = "2" Then
        '    strVol = "0" + Request("FVol")
        'Else
        '    strVol = Request("FVol")
        'End If


        'lblFileRefVol.Text = Replace(Replace(Request("FRef"), "^", " "), "*", "'") + " (" + strVol + ")"
        'lblFileTitle.Text = Replace(Replace(Replace(Request("FTitle"), "^", " "), "~", vbCrLf), "*", "'")
        'barcodeCode.Code = Request("FBarcode")
        'lblFileNo.Text = Request("FNo")
        ''*** End   : Comment Out by CSS, 17 Aug 2009

        ''*** Start : Changes by CSS, 17 Aug 2009
        Dim strFileDetailID As String = Request("strFileDetailID").ToString().Trim()
        Dim Status As String = Request("Status").ToString().Trim()

        Dim dt As DataTable = clsFile.fnFile_GetDataByfld_FileDetailID(strFileDetailID).Tables(0)
        'Dim strSQl As String = "SELECT DISTINCT    " & _
        '                                    "m.* , " & _
        '                                    "d.fld_FileDetailID , " & _
        '                                    "ISNULL(d.fld_FileDetail_FileRefNo,'') fld_FileDetail_FileRefNo, " & _
        '                                    "ISNULL(d.fld_FileDetail_FileTitle,'') fld_FileDetail_FileTitle, " & _
        '                                    "d.fld_FileDetal_FileName , " & _
        '                                    "ISNULL(l.fld_LocationCode,'') fld_LocationCode , " & _
        '                                    "ISNULL(ls.fld_LocSubCode,'') fld_LocSubCode, " & _
        '                                    "d.fld_FileDetail_RFID_code , " & _
        '                                    "d.fld_CloseDate , " & _
        '                                    "d.fld_Status " & _
        '                            "FROM    dbo.tbl_File_Master m " & _
        '                                    "LEFT JOIN dbo.tbl_File_Detail d ON d.fld_FileID = m.fld_FileID " & _
        '                                    "left join dbo.tbl_File_Movement mv on mv.fld_FileMvID = d.fld_FileMvID " & _
        '                                    "left join dbo.tbl_Location l on mv.fld_LocationID = l.fld_LocationID " & _
        '                                    "left join dbo.tbl_LocationSub ls on mv.fld_LocSubID = ls.fld_LocSubID " & _
        '                            " where  d.fld_FileDetailID = '" & CStr(strFileDetailID) & "'"


        'Dim objRdr As SqlDataReader
        'objRdr = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBStringForFTS, CommandType.Text, strSQl)
        'If Not objRdr Is Nothing Then
        '    If objRdr.HasRows Then
        '        objRdr.Read()

        '        lblFileRef.Text = objRdr("fld_FileRefNo").ToString()
        '        lblFileTitle.Text = objRdr("fld_FileDetail_FileTitle").ToString()
        '        'barcodeCode.Code = mfnGetBarcode("F", CStr(objRdr("fld_FileDetailID")))
        '        barcodeCode.Code = objRdr("fld_FileDetail_FileRefNo").ToString()
        '        lblbarcode.Text = objRdr("fld_FileDetail_FileRefNo").ToString()
        '        'lblFileDetail.Text = objRdr("fld_FileDetail_FileRefNo")
        '        lblLocSubCode.Text = objRdr("fld_LocSubCode").ToString()

        '    End If
        'End If
        'objRdr.Close()

        If (dt.Rows.Count > 0) Then
            lblFileRef.Text = dt.Rows(0)("fld_FileRefNo").ToString()
            lblFileTitle.Text = dt.Rows(0)("fld_FileDetail_FileTitle").ToString()
            barcodeCode.Code = dt.Rows(0)("fld_FileDetail_FileRefNo").ToString()
            lblbarcode.Text = dt.Rows(0)("fld_FileDetail_FileRefNo").ToString()
            lblLocOffName.Text = dt.Rows(0)("fld_LocationOfficerName").ToString()
        End If

        If Trim(strFileDetailID) <> "" And Status = "File" Then
            'hdnAssetIDsSelected.Value = Trim(strAssetIDs)
            'hdnPrintRFIDLabelF.Value = "Y"
            ''fnPopulateRecords()

            clsRFIDLabel.fnCreateRFIDLabelFile(strFileDetailID + "^", Session("UsrID"), _
                                               Server.MapPath(gRFIDLabelPath), "")

            ''*** send to Printing
            'fnPrintRFIDLabelFile(strFileNameLabel)

            ''Insert Audit Trail
            clsCommon.fnAuditInsertRec(Session("UsrID"), strFileDetailID.ToString().Replace("^", ","), "RFID passive label printed.")

        End If

        ''*** End   : Changes by CSS, 17 Aug 2009
    End Sub
End Class
