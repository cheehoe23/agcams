<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AddNewFile.aspx.vb" Inherits="AMS.AddNewFile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <LINK href="CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="CommonJS.js"></script>
</head>
<body>
    <form id="frm_AddFile" method="post" runat="server">
			<TABLE id="tableSubStatusAdd" cellSpacing="1" cellPadding="1" width="90%" align="center"
				bgColor="white" border="0">
				<TBODY>
					<TR>
						<TD colSpan="2"><B>Add New File</B></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></TD>
					</TR>
					<tr>
						<td colSpan="2">
						    <TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126" width="100%" ID="Table6">
								<TBODY>
									<TR>
										<TD valign="top">
										    <div id=divUploadFile runat=server>
											    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
												    <TBODY align=left>
												        <TR>
										                    <TD height="4" colspan=2>
											                    <DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
										                    </TD>
									                    </TR>
									                    <TR>
										                    <TD height="4">
											                    &nbsp;
										                    </TD>
									                    </TR>
												        <TR>
													        <TD valign="middle" width="35%"><font color="red">*</font>
													            <FONT class="DisplayTitle">File Upload : </FONT>
													        </TD>
													        <TD width="65%"><input id="FileUpload" runat="server" type="file" /></TD>
												        </TR>
													    <TR>
														    <TD align="center" colspan="2"><BR>
															    <asp:Button id="butUpload" Text="Upload File" Runat="Server" />&nbsp;
															    <asp:Button id="butReset" Text="Reset" Runat="Server" Visible=false />&nbsp;
															    <asp:Button id="butCancel" Text="Cancel" Runat="Server" />
														    </TD>
													    </TR>
												    </TBODY>
											    </TABLE>
											</div>
											
											<div id=divUploadSuccess runat=server>
											    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table1">
												    <TBODY align=left>
												        <TR>
										                    <TD height="4">
											                    &nbsp;
										                    </TD>
									                    </TR>
												        <TR>
													        <TD valign="middle" align=center>
                                                                <h1><font class="DisplayTitleHeader">File upload successfully.</font></h1> 
													        </TD>
												        </TR>
													    <TR>
														    <TD align="center"><BR />
															    <asp:Button id="butAddOther" Text="Add Another File" Runat="Server" Width="149px" />&nbsp;
															    <asp:Button id="butCloseWin" Text="Close" Runat="Server" />
														    </TD>
													    </TR>
												    </TBODY>
											    </TABLE>
											</div>
										</TD>
									</TR>
									<TR>
										<TD height="2"></TD>
									</TR>
								</TBODY>
							</TABLE>
					    </td>
					</tr>
				</TBODY>
			</TABLE>
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnAddFileF" runat="server">
			<input type="hidden" id="hdnCallFrm" runat="server">
			<input type="hidden" id="hdnAssetStatusInfoID" runat="server"> 
			<input type="hidden" id="hdnABarcode" runat="server">
			<input type="hidden" id="hdnAssetID" runat="server">
			<input type="hidden" id="hdnAssetStatus" runat="server">
			<input type="hidden" id="hdnContractID" runat="server">
			<!-- End  : Hidden Fields -->
		</form>
		<script language="javascript">
			if (document.frm_AddFile.hdnAddFileF.value == 'Y')
			{	
			    //Call From Add Asset Screen
				if (document.frm_AddFile.hdnCallFrm.value=='AddAsset')
				{	
					var openerForm = opener.document.forms.Frm_AssetAdd;
					openerForm.action = "../AssetMng/asset_add.aspx";
					window.opener.document.forms(0).hdnAddFileF.value = document.frm_AddFile.hdnAddFileF.value;
				}

	            //Call From Add Condemn Screen
	            if (document.frm_AddFile.hdnCallFrm.value == 'AddCondemn') {
	                var openerForm = opener.document.forms.Frm_RedCodemn;
	                openerForm.action = "../DisposeMng/CRAsset_Condemn.aspx";
	                window.opener.document.forms(0).hdnAddFileF.value = document.frm_AddFile.hdnAddFileF.value;
	            }

	            //Call From Add Redundancy Screen
	            if (document.frm_AddFile.hdnCallFrm.value == 'AddRedundancy') {
	                var openerForm = opener.document.forms.Frm_RedCodemn;
	                openerForm.action = "../DisposeMng/CRAsset_Redundancy.aspx";
	                window.opener.document.forms(0).hdnAddFileF.value = document.frm_AddFile.hdnAddFileF.value;
	            }

				//Call From Add Status File Screen
				if (document.frm_AddFile.hdnCallFrm.value=='AddStatusFile')
				{	
					var openerForm = opener.document.forms.Frm_DisRedCodemn;
					openerForm.action = "../AssetMng/asset_DisposeCondemmRedun.aspx";
					window.opener.document.forms(0).hdnAddFileF.value = document.frm_AddFile.hdnAddFileF.value;
				}	
				
				//Call From Edit Asset Screen --> Add Asset File 
				if (document.frm_AddFile.hdnCallFrm.value=='EditAsset')
				{	
					var openerForm = opener.document.forms.Frm_AssetEdit;
					openerForm.action = "../AssetMng/asset_edit.aspx";
					window.opener.document.forms(0).hdnAddAssetFileF.value = document.frm_AddFile.hdnAddFileF.value;
				}
				
				//Call From Edit Asset Screen --> Add Status File (Condemnation/Redundant)
				if (document.frm_AddFile.hdnCallFrm.value=='AddStatusRCFileP' || document.frm_AddFile.hdnCallFrm.value=='AddStatusRCFileT')
				{	
					var openerForm = opener.document.forms.Frm_AssetEdit;
					openerForm.action = "../AssetMng/asset_edit.aspx";
					window.opener.document.forms(0).hdnAddStatusFileRCF.value = document.frm_AddFile.hdnAddFileF.value;
				}
				
				//Call From Edit Asset Screen --> Add Status File (Disposed)
				if (document.frm_AddFile.hdnCallFrm.value=='AddStatusDFileP' || document.frm_AddFile.hdnCallFrm.value=='AddStatusDFileT')
				{	
					var openerForm = opener.document.forms.Frm_AssetEdit;
					openerForm.action = "../AssetMng/asset_edit.aspx";
					window.opener.document.forms(0).hdnAddStatusFileDF.value = document.frm_AddFile.hdnAddFileF.value;
				}
				
				//Call From Add contract Screen
				if (document.frm_AddFile.hdnCallFrm.value=='AddContract')
				{	
					var openerForm = opener.document.forms.Frm_ContractAdd;
					openerForm.action = "../ContractMng/contract_add.aspx";
					window.opener.document.forms(0).hdnAddFileF.value = document.frm_AddFile.hdnAddFileF.value;
				}	
				
				//Call From edit contract Screen
				if (document.frm_AddFile.hdnCallFrm.value=='EditContract')
				{	
					var openerForm = opener.document.forms.Frm_ContractEdit;
					openerForm.action = "../ContractMng/contract_edit.aspx";
					window.opener.document.forms(0).hdnAddFileF.value = document.frm_AddFile.hdnAddFileF.value;
				}	
				
				//Call From Add renewal contract Screen
				if (document.frm_AddFile.hdnCallFrm.value=='RenewalContract')
				{	
					var openerForm = opener.document.forms.Frm_ContractAdd;
					openerForm.action = "../ContractMng/contract_addRenewal.aspx";
					window.opener.document.forms(0).hdnAddFileF.value = document.frm_AddFile.hdnAddFileF.value;
				}
				
				    document.frm_AddFile.hdnAddFileF.value = 'N';
					openerForm.method = "post";
					openerForm.submit();
					//window.close();	
			}
		</script>
</body>
</html>
