#Region "Project Information Section"
' ****************************************************************************************************
' Description       : Create RFID Label.
' Purpose           : This file is to create RFID Label by coding.
' Date			    : 09.03.2009
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Data
Imports System.IO
#End Region

Public Class clsRFIDLabel
    Public Shared Sub fnCreateRFIDLabelFile4Loc( _
                        ByVal strLoctID As String, ByVal strUsrID As String, ByVal strFilePath As String, _
                        ByRef strFileNameLabel As String)
        Try
            Dim FileFullPath As String = ""
            Dim objRdr As SqlDataReader

            ''*** Get File Name
            'strFileNameLabel = strUsrID & "_" & Year(Date.Today).ToString & Month(Date.Today).ToString.PadLeft(2, "0") & Day(Date.Today).ToString.PadLeft(2, "0") & Hour(Date.Now).ToString.PadLeft(2, "0") & Minute(Date.Now).ToString.PadLeft(2, "0") & Second(Date.Now).ToString.PadLeft(2, "0") & ".ZPL" '".txt"

            ' ''*** Get File Full Path
            'FileFullPath = strFilePath & strFileNameLabel

            objRdr = clsLocation.fnLocation_GetLocationDetails(strLoctID)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    '********** Beginning write text file ***********
                    'Dim MyWriter As New StreamWriter(FileFullPath, False)
                    'Dim textstring As String = ""

                    While objRdr.Read()

                        strFileNameLabel = CStr(objRdr("fld_LocSubCode")) & "_" & strUsrID & "_" & Year(Date.Today).ToString & Month(Date.Today).ToString.PadLeft(2, "0") & Day(Date.Today).ToString.PadLeft(2, "0") & Hour(Date.Now).ToString.PadLeft(2, "0") & Minute(Date.Now).ToString.PadLeft(2, "0") & Second(Date.Now).ToString.PadLeft(2, "0") & ".ZPL" '".txt"
                       
                        strFileNameLabel = strFileNameLabel.Replace("\", "")
                        strFileNameLabel = strFileNameLabel.Replace("/", "")
                        strFileNameLabel = strFileNameLabel.Replace(":", "")
                        strFileNameLabel = strFileNameLabel.Replace("*", "")
                        strFileNameLabel = strFileNameLabel.Replace("?", "")
                        strFileNameLabel = strFileNameLabel.Replace("""", "")
                        strFileNameLabel = strFileNameLabel.Replace("<", "")
                        strFileNameLabel = strFileNameLabel.Replace(">", "")
                        strFileNameLabel = strFileNameLabel.Replace("|", "")
                        strFileNameLabel = strFileNameLabel.Replace("\t", "")
                        strFileNameLabel = strFileNameLabel.Replace("\n", "")
                        strFileNameLabel = strFileNameLabel.Replace("\r", "")
                        strFileNameLabel = strFileNameLabel.Replace("\f", "")
                        strFileNameLabel = strFileNameLabel.Replace("~", "")
                        strFileNameLabel = strFileNameLabel.Replace("!", "")
                        strFileNameLabel = strFileNameLabel.Replace("@", "")
                        strFileNameLabel = strFileNameLabel.Replace("#", "")
                        strFileNameLabel = strFileNameLabel.Replace("$", "")
                        strFileNameLabel = strFileNameLabel.Replace("%", "")
                        strFileNameLabel = strFileNameLabel.Replace("^", "")
                        strFileNameLabel = strFileNameLabel.Replace("&", "")
                        strFileNameLabel = strFileNameLabel.Replace("*", "")
                        strFileNameLabel = strFileNameLabel.Replace("(", "")
                        strFileNameLabel = strFileNameLabel.Replace(")", "")
                        strFileNameLabel = strFileNameLabel.Replace("-", "")
                        strFileNameLabel = strFileNameLabel.Replace("+", "")
                        strFileNameLabel = strFileNameLabel.Replace(";", "")
                        strFileNameLabel = strFileNameLabel.Replace("'", "")
                        strFileNameLabel = strFileNameLabel.Replace("[", "")
                        strFileNameLabel = strFileNameLabel.Replace("]", "")
                        strFileNameLabel = strFileNameLabel.Replace("{", "")
                        strFileNameLabel = strFileNameLabel.Replace("}", "")
                        strFileNameLabel = strFileNameLabel.Replace("&", "")
                        strFileNameLabel = strFileNameLabel.Replace("*", "")
                        strFileNameLabel = strFileNameLabel.Replace("(", "")
                        strFileNameLabel = strFileNameLabel.Replace(")", "")
                        strFileNameLabel = strFileNameLabel.Replace("-", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("%", "")
                        strFileNameLabel = strFileNameLabel.Replace("@", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("*", "")
                        strFileNameLabel = strFileNameLabel.Replace("-", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")

                        ''*** Get File Full Path
                        FileFullPath = strFilePath & strFileNameLabel

                        Dim MyWriter As New StreamWriter(FileFullPath, False)
                        Dim textstring As String = ""

                        ''1.**** Get RFID Label Header
                        textstring = "^XA"
                        MyWriter.WriteLine(textstring)

                        textstring = "^RS,,,3,N"
                        MyWriter.WriteLine(textstring)

                        textstring = "^RR3"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XZ"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XA"
                        MyWriter.WriteLine(textstring)

                        textstring = "^SZ2^JMA"
                        MyWriter.WriteLine(textstring)

                        textstring = "^MCY^PMN"
                        MyWriter.WriteLine(textstring)

                        textstring = "^PW931"
                        MyWriter.WriteLine(textstring)

                        textstring = "~JSN"
                        MyWriter.WriteLine(textstring)

                        textstring = "^JZY"
                        MyWriter.WriteLine(textstring)

                        textstring = "^LH0,0^LRN"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XZ"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XA"
                        MyWriter.WriteLine(textstring)

                        textstring = "^FT34,185"
                        MyWriter.WriteLine(textstring)

                        textstring = "^CI0"
                        MyWriter.WriteLine(textstring)

                        ''2.**** Get RFID Label Data
                        textstring = "^A0N,20,20^FO48,37^FD" & CStr(objRdr("fld_LocationCode")) & "^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^A0N,20,20^FO200,37^FD" & CStr(objRdr("fld_LocationName")) & "^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^A0N,20,20^FO48,119^FD" & CStr(objRdr("fld_LocSubCode")) & "^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^A0N,20,20^FO200,119^FD" & CStr(objRdr("fld_LocSubName")) & "^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^^FO48,54"
                        MyWriter.WriteLine(textstring)

                        textstring = "^BY2^BCN,60,N,N^FD" & CStr(objRdr("fld_LocSubCode")) & "^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^RFW,H,1,2,1^FD3400^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^RFW,H,2,12,1^FD" & ("000000000000000000000000" + CStr(objRdr("fld_LocSubID")) + "B").Substring(("000000000000000000000000" + CStr(objRdr("fld_LocSubID")) + "B").Length - 24) & "^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^PQ1,0,1,Y"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XZ"
                        MyWriter.WriteLine(textstring)

                        MyWriter.Close()
                        MyWriter = Nothing
                    End While

                    'MyWriter.Close()
                    'MyWriter = Nothing
                    '********** Ending write text file ***********
                End If
            End If
            objRdr.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Shared Sub fnCreateRFIDLabelAsset( _
                    ByVal strAssetIDs As String, ByVal strUsrID As String, ByVal strFilePath As String, _
                    ByRef strFileNameLabel As String)
        Try
            Dim FileFullPath As String = ""
            Dim objRdr As SqlDataReader

            objRdr = clsCommon.fnRFIDLabel_GetAssetDetails(strAssetIDs)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    '********** Beginning write text file ***********                    

                    While objRdr.Read()

                        ''*** Get File Name
                        strFileNameLabel = CStr(objRdr("fld_AssetBarcode")) & "_" & strUsrID & "_" & Year(Date.Today).ToString & Month(Date.Today).ToString.PadLeft(2, "0") & Day(Date.Today).ToString.PadLeft(2, "0") & Hour(Date.Now).ToString.PadLeft(2, "0") & Minute(Date.Now).ToString.PadLeft(2, "0") & Second(Date.Now).ToString.PadLeft(2, "0") & ".ZPL" '".txt"

                        strFileNameLabel = strFileNameLabel.Replace("\", "")
                        strFileNameLabel = strFileNameLabel.Replace("/", "")
                        strFileNameLabel = strFileNameLabel.Replace(":", "")
                        strFileNameLabel = strFileNameLabel.Replace("*", "")
                        strFileNameLabel = strFileNameLabel.Replace("?", "")
                        strFileNameLabel = strFileNameLabel.Replace("""", "")
                        strFileNameLabel = strFileNameLabel.Replace("<", "")
                        strFileNameLabel = strFileNameLabel.Replace(">", "")
                        strFileNameLabel = strFileNameLabel.Replace("|", "")
                        strFileNameLabel = strFileNameLabel.Replace("\t", "")
                        strFileNameLabel = strFileNameLabel.Replace("\n", "")
                        strFileNameLabel = strFileNameLabel.Replace("\r", "")
                        strFileNameLabel = strFileNameLabel.Replace("\f", "")
                        strFileNameLabel = strFileNameLabel.Replace("~", "")
                        strFileNameLabel = strFileNameLabel.Replace("!", "")
                        strFileNameLabel = strFileNameLabel.Replace("@", "")
                        strFileNameLabel = strFileNameLabel.Replace("#", "")
                        strFileNameLabel = strFileNameLabel.Replace("$", "")
                        strFileNameLabel = strFileNameLabel.Replace("%", "")
                        strFileNameLabel = strFileNameLabel.Replace("^", "")
                        strFileNameLabel = strFileNameLabel.Replace("&", "")
                        strFileNameLabel = strFileNameLabel.Replace("*", "")
                        strFileNameLabel = strFileNameLabel.Replace("(", "")
                        strFileNameLabel = strFileNameLabel.Replace(")", "")
                        strFileNameLabel = strFileNameLabel.Replace("-", "")
                        strFileNameLabel = strFileNameLabel.Replace("+", "")
                        strFileNameLabel = strFileNameLabel.Replace(";", "")
                        strFileNameLabel = strFileNameLabel.Replace("'", "")
                        strFileNameLabel = strFileNameLabel.Replace("[", "")
                        strFileNameLabel = strFileNameLabel.Replace("]", "")
                        strFileNameLabel = strFileNameLabel.Replace("{", "")
                        strFileNameLabel = strFileNameLabel.Replace("}", "")
                        strFileNameLabel = strFileNameLabel.Replace("&", "")
                        strFileNameLabel = strFileNameLabel.Replace("*", "")
                        strFileNameLabel = strFileNameLabel.Replace("(", "")
                        strFileNameLabel = strFileNameLabel.Replace(")", "")
                        strFileNameLabel = strFileNameLabel.Replace("-", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("%", "")
                        strFileNameLabel = strFileNameLabel.Replace("@", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("*", "")
                        strFileNameLabel = strFileNameLabel.Replace("-", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")

                        ''*** Get File Full Path
                        FileFullPath = strFilePath & strFileNameLabel.Replace("/", "")

                        Dim MyWriter As New StreamWriter(FileFullPath, False)
                        Dim textstring As String = ""

                        ''1.**** Get RFID Label Header
                        textstring = "^XA"
                        MyWriter.WriteLine(textstring)

                        textstring = "^RS,,,3,N"
                        MyWriter.WriteLine(textstring)

                        textstring = "^RR3"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XZ"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XA"
                        MyWriter.WriteLine(textstring)

                        textstring = "^SZ2^JMA"
                        MyWriter.WriteLine(textstring)

                        textstring = "^MCY^PMN"
                        MyWriter.WriteLine(textstring)

                        textstring = "^PW931"
                        MyWriter.WriteLine(textstring)

                        textstring = "~JSN"
                        MyWriter.WriteLine(textstring)

                        textstring = "^JZY"
                        MyWriter.WriteLine(textstring)

                        textstring = "^LH0,0^LRN"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XZ"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XA"
                        MyWriter.WriteLine(textstring)

                        textstring = "^FT34,185"
                        MyWriter.WriteLine(textstring)

                        textstring = "^CI0"
                        MyWriter.WriteLine(textstring)

                        ''2.**** Get RFID Label Data
                        textstring = "^A0N,20,20^FO48,37^FD" & CStr(objRdr("fld_LocCode")) & "^FS"
                        MyWriter.WriteLine(textstring)

                        'textstring = "^A0N,20,20^FO310,37^FD" & CStr(objRdr("fld_LocSubCode")) & "^FS"
                        'MyWriter.WriteLine(textstring)

                        textstring = "^A0N,20,20^FO220,37^FD" & CStr(objRdr("fld_AssetDescOrMachineModel")) & "^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^A0N,20,20^FO48,119^FD" & CStr(objRdr("fld_AssetBarcode")) & "^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^A0N,20,20^FO220,119^FD" & CStr(objRdr("fld_DeptCodeOrSerialNo")) & "^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^FO48,54"
                        MyWriter.WriteLine(textstring)

                        textstring = "^BY2^BCN,60,N,N^FD" & CStr(objRdr("fld_AssetBarcode")) & "^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^RFW,H,1,2,1^FD3400^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^RFW,H,2,12,1^FD" & ("000000000000000000000000" + CStr(objRdr("fld_AssetID")) + "A").Substring(("000000000000000000000000" + CStr(objRdr("fld_AssetID")) + "A").Length - 24) & "^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^PQ1,0,1,Y"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XZ"
                        MyWriter.WriteLine(textstring)

                        MyWriter.Close()
                        MyWriter = Nothing
                    End While
                    
                    '********** Ending write text file ***********
                End If
            End If
            objRdr.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Shared Sub fnCreateRFIDLabelFile( _
                    ByVal strFileDetailIDs As String, ByVal strUsrID As String, ByVal strFilePath As String, _
                    ByRef strFileNameLabel As String)
        Try
            Dim FileFullPath As String = ""
            Dim objRdr As SqlDataReader

            objRdr = clsCommon.fnRFIDLabel_GetFileDetails(strFileDetailIDs)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    '********** Beginning write text file ***********                    

                    While objRdr.Read()

                        Dim FileDetail_FileRefNo As String = CStr(objRdr("fld_FileDetail_FileRefNo").ToString())
                        If (FileDetail_FileRefNo.Length > 20) Then
                            FileDetail_FileRefNo = FileDetail_FileRefNo.Substring(0, 20)
                        End If

                        ''*** Get File Name
                        strFileNameLabel = FileDetail_FileRefNo & "_" & strUsrID & "_" & Year(Date.Today).ToString & Month(Date.Today).ToString.PadLeft(2, "0") & Day(Date.Today).ToString.PadLeft(2, "0") & Hour(Date.Now).ToString.PadLeft(2, "0") & Minute(Date.Now).ToString.PadLeft(2, "0") & Second(Date.Now).ToString.PadLeft(2, "0") & ".ZPL" '".txt"

                        strFileNameLabel = strFileNameLabel.Replace("\", "")
                        strFileNameLabel = strFileNameLabel.Replace("/", "")
                        strFileNameLabel = strFileNameLabel.Replace(":", "")
                        strFileNameLabel = strFileNameLabel.Replace("*", "")
                        strFileNameLabel = strFileNameLabel.Replace("?", "")
                        strFileNameLabel = strFileNameLabel.Replace("""", "")
                        strFileNameLabel = strFileNameLabel.Replace("<", "")
                        strFileNameLabel = strFileNameLabel.Replace(">", "")
                        strFileNameLabel = strFileNameLabel.Replace("|", "")
                        strFileNameLabel = strFileNameLabel.Replace("\t", "")
                        strFileNameLabel = strFileNameLabel.Replace("\n", "")
                        strFileNameLabel = strFileNameLabel.Replace("\r", "")
                        strFileNameLabel = strFileNameLabel.Replace("\f", "")
                        strFileNameLabel = strFileNameLabel.Replace("~", "")
                        strFileNameLabel = strFileNameLabel.Replace("!", "")
                        strFileNameLabel = strFileNameLabel.Replace("@", "")
                        strFileNameLabel = strFileNameLabel.Replace("#", "")
                        strFileNameLabel = strFileNameLabel.Replace("$", "")
                        strFileNameLabel = strFileNameLabel.Replace("%", "")
                        strFileNameLabel = strFileNameLabel.Replace("^", "")
                        strFileNameLabel = strFileNameLabel.Replace("&", "")
                        strFileNameLabel = strFileNameLabel.Replace("*", "")
                        strFileNameLabel = strFileNameLabel.Replace("(", "")
                        strFileNameLabel = strFileNameLabel.Replace(")", "")
                        strFileNameLabel = strFileNameLabel.Replace("-", "")
                        strFileNameLabel = strFileNameLabel.Replace("+", "")
                        strFileNameLabel = strFileNameLabel.Replace(";", "")
                        strFileNameLabel = strFileNameLabel.Replace("'", "")
                        strFileNameLabel = strFileNameLabel.Replace("[", "")
                        strFileNameLabel = strFileNameLabel.Replace("]", "")
                        strFileNameLabel = strFileNameLabel.Replace("{", "")
                        strFileNameLabel = strFileNameLabel.Replace("}", "")
                        strFileNameLabel = strFileNameLabel.Replace("&", "")
                        strFileNameLabel = strFileNameLabel.Replace("*", "")
                        strFileNameLabel = strFileNameLabel.Replace("(", "")
                        strFileNameLabel = strFileNameLabel.Replace(")", "")
                        strFileNameLabel = strFileNameLabel.Replace("-", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("%", "")
                        strFileNameLabel = strFileNameLabel.Replace("@", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("*", "")
                        strFileNameLabel = strFileNameLabel.Replace("-", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")

                        ''*** Get File Full Path
                        FileFullPath = strFilePath & strFileNameLabel

                        Dim MyWriter As New StreamWriter(FileFullPath, False)
                        Dim textstring As String = ""

                        ''1.**** Get RFID Label Header
                        textstring = "^XA"
                        MyWriter.WriteLine(textstring)

                        textstring = "^RS,,,3,N"
                        MyWriter.WriteLine(textstring)

                        textstring = "^RR3"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XZ"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XA"
                        MyWriter.WriteLine(textstring)

                        textstring = "^SZ2^JMA"
                        MyWriter.WriteLine(textstring)

                        textstring = "^MCY^PMN"
                        MyWriter.WriteLine(textstring)

                        textstring = "^PW931"
                        MyWriter.WriteLine(textstring)

                        textstring = "~JSN"
                        MyWriter.WriteLine(textstring)

                        textstring = "^JZY"
                        MyWriter.WriteLine(textstring)

                        textstring = "^LH0,0^LRN"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XZ"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XA"
                        MyWriter.WriteLine(textstring)

                        textstring = "^FT34,185"
                        MyWriter.WriteLine(textstring)

                        textstring = "^CI0"
                        MyWriter.WriteLine(textstring)

                        FileDetail_FileRefNo = CStr(objRdr("fld_FileDetail_FileRefNo").ToString())

                        Dim fileRefNo As String = CStr(objRdr("fld_FileRefNo").ToString())
                        'If (fileRefNo.Length > 20) Then
                        '    fileRefNo = fileRefNo.Substring(0, 20)
                        'End If

                        Dim FileDetail_FileTitle As String = CStr(objRdr("fld_FileDetail_FileTitle").ToString())
                        If (FileDetail_FileTitle.Length > 20) Then
                            FileDetail_FileTitle = FileDetail_FileTitle.Substring(0, 20)
                        End If

                        ''2.**** Get RFID Label Data                       
                        textstring = "^A0N,20,20^FO48,37^FD" & fileRefNo.ToString() & "^FS"
                        MyWriter.WriteLine(textstring)

                        'textstring = "^A0N,20,20^FO270,37^FD" & CStr(objRdr("fld_FileDetailBarcodeID").ToString()) & "^FS"
                        'MyWriter.WriteLine(textstring)

                        textstring = "^A0N,20,20^FO48,119^FD" & FileDetail_FileRefNo.ToString() & "^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^A0N,20,20^FO270,119^FD" & FileDetail_FileTitle.ToString() & "^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^FO48,54"
                        MyWriter.WriteLine(textstring)

                        textstring = "^BY2^BCN,60,N,N^FD" & CStr(objRdr("fld_FileDetailBarcodeID").ToString()) & "^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^RFW,H,1,2,1^FD3400^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^RFW,H,2,12,1^FD" & ("000000000000000000000000" + CStr(objRdr("fld_FileDetailID").ToString()) + "C").Substring(("000000000000000000000000" + CStr(objRdr("fld_FileDetailID").ToString()) + "C").Length - 24) & "^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^PQ1,0,1,Y"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XZ"
                        MyWriter.WriteLine(textstring)

                        MyWriter.Close()
                        MyWriter = Nothing
                    End While

                    '********** Ending write text file ***********
                End If
            End If
            objRdr.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Shared Sub fnCreateRFIDLabelLocationOfficer( _
                    ByVal strLocationOfficerID As String, ByVal strUsrID As String, ByVal strFilePath As String, _
                    ByRef strFileNameLabel As String)
        Try
            Dim FileFullPath As String = ""
            Dim objRdr As SqlDataReader

            objRdr = clsLocationOfficer.fnRFIDLabel_GetLocationOfficer(strLocationOfficerID)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    '********** Beginning write text file ***********                    

                    While objRdr.Read()

                        ''*** Get File Name
                        strFileNameLabel = CStr(objRdr("fld_LocationOfficerBarcodeID")) & "_" & strUsrID & "_" & Year(Date.Today).ToString & Month(Date.Today).ToString.PadLeft(2, "0") & Day(Date.Today).ToString.PadLeft(2, "0") & Hour(Date.Now).ToString.PadLeft(2, "0") & Minute(Date.Now).ToString.PadLeft(2, "0") & Second(Date.Now).ToString.PadLeft(2, "0") & ".ZPL" '".txt"

                        strFileNameLabel = strFileNameLabel.Replace("\", "")
                        strFileNameLabel = strFileNameLabel.Replace("/", "")
                        strFileNameLabel = strFileNameLabel.Replace(":", "")
                        strFileNameLabel = strFileNameLabel.Replace("*", "")
                        strFileNameLabel = strFileNameLabel.Replace("?", "")
                        strFileNameLabel = strFileNameLabel.Replace("""", "")
                        strFileNameLabel = strFileNameLabel.Replace("<", "")
                        strFileNameLabel = strFileNameLabel.Replace(">", "")
                        strFileNameLabel = strFileNameLabel.Replace("|", "")
                        strFileNameLabel = strFileNameLabel.Replace("\t", "")
                        strFileNameLabel = strFileNameLabel.Replace("\n", "")
                        strFileNameLabel = strFileNameLabel.Replace("\r", "")
                        strFileNameLabel = strFileNameLabel.Replace("\f", "")
                        strFileNameLabel = strFileNameLabel.Replace("~", "")
                        strFileNameLabel = strFileNameLabel.Replace("!", "")
                        strFileNameLabel = strFileNameLabel.Replace("@", "")
                        strFileNameLabel = strFileNameLabel.Replace("#", "")
                        strFileNameLabel = strFileNameLabel.Replace("$", "")
                        strFileNameLabel = strFileNameLabel.Replace("%", "")
                        strFileNameLabel = strFileNameLabel.Replace("^", "")
                        strFileNameLabel = strFileNameLabel.Replace("&", "")
                        strFileNameLabel = strFileNameLabel.Replace("*", "")
                        strFileNameLabel = strFileNameLabel.Replace("(", "")
                        strFileNameLabel = strFileNameLabel.Replace(")", "")
                        strFileNameLabel = strFileNameLabel.Replace("-", "")
                        strFileNameLabel = strFileNameLabel.Replace("+", "")
                        strFileNameLabel = strFileNameLabel.Replace(";", "")
                        strFileNameLabel = strFileNameLabel.Replace("'", "")
                        strFileNameLabel = strFileNameLabel.Replace("[", "")
                        strFileNameLabel = strFileNameLabel.Replace("]", "")
                        strFileNameLabel = strFileNameLabel.Replace("{", "")
                        strFileNameLabel = strFileNameLabel.Replace("}", "")
                        strFileNameLabel = strFileNameLabel.Replace("&", "")
                        strFileNameLabel = strFileNameLabel.Replace("*", "")
                        strFileNameLabel = strFileNameLabel.Replace("(", "")
                        strFileNameLabel = strFileNameLabel.Replace(")", "")
                        strFileNameLabel = strFileNameLabel.Replace("-", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("%", "")
                        strFileNameLabel = strFileNameLabel.Replace("@", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("*", "")
                        strFileNameLabel = strFileNameLabel.Replace("-", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")
                        strFileNameLabel = strFileNameLabel.Replace("�", "")

                        ''*** Get File Full Path
                        FileFullPath = strFilePath & strFileNameLabel

                        Dim MyWriter As New StreamWriter(FileFullPath, False)
                        Dim textstring As String = ""

                        ''1.**** Get RFID Label Header
                        textstring = "^XA"
                        MyWriter.WriteLine(textstring)

                        textstring = "^RS,,,3,N"
                        MyWriter.WriteLine(textstring)

                        textstring = "^RR3"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XZ"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XA"
                        MyWriter.WriteLine(textstring)

                        textstring = "^SZ2^JMA"
                        MyWriter.WriteLine(textstring)

                        textstring = "^MCY^PMN"
                        MyWriter.WriteLine(textstring)

                        textstring = "^PW931"
                        MyWriter.WriteLine(textstring)

                        textstring = "~JSN"
                        MyWriter.WriteLine(textstring)

                        textstring = "^JZY"
                        MyWriter.WriteLine(textstring)

                        textstring = "^LH0,0^LRN"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XZ"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XA"
                        MyWriter.WriteLine(textstring)

                        textstring = "^FT34,185"
                        MyWriter.WriteLine(textstring)

                        textstring = "^CI0"
                        MyWriter.WriteLine(textstring)

                        ''2.**** Get RFID Label Data

                        textstring = "^A0N,20,20^FO48,37^FD" & CStr(objRdr("fld_LocationOfficerBarcodeID").ToString()) & "^FS"
                        MyWriter.WriteLine(textstring)

                        'textstring = "^A0N,20,20^FO310,37^FD" & CStr(objRdr("fld_LocationOfficerName").ToString()) & "^FS"
                        'MyWriter.WriteLine(textstring)

                        textstring = "^A0N,20,20^FO48,119^FD" & CStr(objRdr("fld_LocationOfficerName").ToString()) & "^FS"
                        MyWriter.WriteLine(textstring)

                        'textstring = "^A0N,20,20^FO310,119^FD" & CStr(objRdr("fld_FileDetail_FileTitle").ToString()) & "^FS"
                        'MyWriter.WriteLine(textstring)

                        textstring = "^FO48,54"
                        MyWriter.WriteLine(textstring)

                        textstring = "^BY2^BCN,60,N,N^FD" & CStr(objRdr("fld_LocationOfficerBarcodeID").ToString()) & "^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^RFW,H,1,2,1^FD3400^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^RFW,H,2,12,1^FD" & ("000000000000000000000000" + CStr(objRdr("fld_LocationOfficerID").ToString()) + "D").Substring(("000000000000000000000000" + CStr(objRdr("fld_LocationOfficerID").ToString()) + "D").Length - 24) & "^FS"
                        MyWriter.WriteLine(textstring)

                        textstring = "^PQ1,0,1,Y"
                        MyWriter.WriteLine(textstring)

                        textstring = "^XZ"
                        MyWriter.WriteLine(textstring)

                        MyWriter.Close()
                        MyWriter = Nothing
                    End While

                    '********** Ending write text file ***********
                End If
            End If
            objRdr.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
