Imports GControllerStrobeLight

Partial Class welcomePg
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        ''********** Start: Check Session Time out ***********
        If Len(Session("UsrID")) = 0 Then
            'Response.Redirect("logout.aspx", False)
            Dim strJavaScript As String = ""
            strJavaScript = "<script language = 'Javascript'>" & _
                            "parent.location.href='logout.aspx';" & _
                            "</script>"
            Response.Write(strJavaScript)
            Exit Sub
        End If
        ''********** End  : Check Session Time out ***********
    End Sub

    'Protected Sub butLight_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butLight.Click
    '    Try
    '        Dim i As Integer = 1
    '        Dim strLightReturn As Boolean
    '        Dim StrobeLight As New GControllerStrobeLight.StrobeLightDevice()
    '        StrobeLight.openConnection("10.4.60.132", 10000)

    '        For i = 0 To 1000

    '        Next

    '        ''Sets the Strobe Light Device to Visual Alert Mode. The Red LED will be activated in this mode. 
    '        strLightReturn = StrobeLight.sendVisualAlert()  ''red light

    '        ''Sets the Strobe Light Device to ON mode. The Green LED will be activated in this mode. 
    '        strLightReturn = StrobeLight.sendOn ''green light

    '        ''Sets the Strobe Light Device to Alert Mode. The Red LED and the Buzzer will be activated in this mode. 
    '        strLightReturn = StrobeLight.sendAlert()  ''sound and red light

    '        ''Sets the Strobe Light Device to OFF mode. All LEDs and Buzzer will be turned off. 
    '        strLightReturn = StrobeLight.sendOff

    '        StrobeLight.closeConnection()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub
End Class
