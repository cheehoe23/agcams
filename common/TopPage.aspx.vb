#Region "Information Section"
' ****************************************************************************************************
' Description       : Top Page
' Purpose           : Top Page
' Author            : See Siew
' Date              : 22/04/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class TopPage
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ''********** Start: Check Session Time out ***********
        If Len(Session("UsrID")) = 0 Then
            'Response.Redirect("~/common/logout.aspx", False)
            Dim strJavaScript As String = ""
            strJavaScript = "<script language = 'Javascript'>" & _
                            "parent.location.href='logout.aspx';" & _
                            "</script>"
            Response.Write(strJavaScript)
            Exit Sub
        End If
        ''********** End  : Check Session Time out ***********

        ''Get User Name
        UsrName.Text = Session("Name")

        ''Get Date 
        lblTodayDt.Text = mfnGetCurrentDateForTopPage()
    End Sub
End Class
