Partial Class footer_cr
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            Else
                Dim strUserKicked As Boolean
                strUserKicked = clsCommon.fnLogin_UserKicked(Session("UsrID"), Session("LoginUsersID"))
                If strUserKicked Then
                    Dim strJavaScript As String = ""
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "parent.location.href='../common/logout.aspx?CallFrm=logout&Status=K';" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                    Response.End()
                    Exit Sub
                End If
            End If
            '********** End  : Check Session Time out ***********

            '' ''Check for acces right
            ''Dim StrGetRecType As String = "3"
            ''If (InStr(Session("AR"), "AssetMng|CondmCtrl|condemnAsset") > 0 And InStr(Session("AR"), "AssetMng|CondmCtrl|condemnInven") > 0) Then
            ''    StrGetRecType = "3"
            ''ElseIf InStr(Session("AR"), "AssetMng|CondmCtrl|condemnAsset") > 0 Then
            ''    StrGetRecType = "1"
            ''ElseIf InStr(Session("AR"), "AssetMng|CondmCtrl|condemnInven") > 0 Then
            ''    StrGetRecType = "2"
            ''Else
            ''    StrGetRecType = "0"
            ''End If

            '' ''**Start : get total missing owner and pending for condemn record
            ''Dim intTotalMissOwner As Integer = "0"
            ''Dim intPendingCondemn As Integer = "0"
            ''Dim intTotalNFSToday As Integer = "0"
            ''Dim intTotalPendReturn As Integer = "0"
            ''Dim strJavaScriptMO As String = ""
            ' ''clsAsset.fnMissingOwner_CountRec(intTotalMissOwner)
            ''clsCommon.fnMenu_CountRec4Information(Session("AdminF"), Session("UsrID"), StrGetRecType, intTotalMissOwner, intPendingCondemn, intTotalNFSToday, intTotalPendReturn)
            ' ''clsAsset.fnAsset_GetCondemnExpRecord("fldAssetBarcode", "ASC", intPendingCondemn)
            ''strJavaScriptMO = "<script language = 'Javascript'>" & _
            ''                  "parent.menu.document.getElementById('lblMenuMissOwner').innerHTML ='" & CStr(intTotalMissOwner) & "';" & _
            ''                  "parent.menu.document.getElementById('lblMenuCondemn').innerHTML ='" & CStr(intPendingCondemn) & "';" & _
            ''                  "parent.menu.document.getElementById('lblMenuNFSToday').innerHTML ='" & CStr(intTotalNFSToday) & "';" & _
            ''                  "parent.menu.document.getElementById('lblMenuReturn').innerHTML ='" & CStr(intTotalPendReturn) & "';" & _
            ''                  "</script>"
            ''Response.Write(strJavaScriptMO)
            '' ''**End   : get total missing owner and pending for condemn record
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
