#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Data
Imports System.IO

Imports System
Imports System.Collections
Imports System.Threading

' Use the GConnect library. 
Imports GConnectAPI
Imports GPortAPI
#End Region

Public Class clsReadWriteActTag
    Public Shared Function fnGetPortNo(ByRef strFindReaderF As Boolean)
        Try
            Dim strPortNo As String = "0"
            Dim frame As Byte()
            Dim strReaderID As String = ""

            'Initialize a GController object. 
            Dim gc As New GController()

            'Get a list of all available serial ports detected on the computer (in string). 
            Dim allPorts As ArrayList = gc.GetAvailablePort()

            For Each strPortNo In allPorts
                Try
                    'Opens the COM port to the reader. 
                    gc.OpenSerialPort(Convert.ToInt16(strPortNo), "115200")

                    'Thread.Sleep(500)
                    gc.SendGetLocalAddress(1)
                    Thread.Sleep(500)
                    gc.SendGetLocalAddress(1)
                    Thread.Sleep(500)
                    gc.SendGetLocalAddress(1)
                    Thread.Sleep(500)

                    'In actual programs, commands should only be sent to the reader when needed. 
                    'Thread.Sleep(2500)

                    'Retreives the data frames that are received by the reader. Each data frame ia a byte array. 
                    'The parameter "1" indicates that serial communication is being used. Depending on your library version, this parameter may not be needed. 
                    Dim dataFrames As ArrayList = gc.GetResponseFrames(1)

                    For Each frame In dataFrames
                        If GController.GetLocalAddress(frame, strReaderID) Then
                            If strReaderID = "47020500033F" Then
                                strFindReaderF = True
                                Exit For
                            End If
                        End If
                    Next

                    'Closes the COM port. 
                    gc.CloseSerialPort()

                    ''*** if reader is find, exit port loop
                    If strFindReaderF Then
                        Exit For
                    End If
                Catch ex As Exception
                    Continue For
                End Try
            Next

            fnGetPortNo = strPortNo
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function fnReadActTag(ByRef strErrorMsg As String)
        Try
            Dim strRFIDTag As String = ""
            Dim strFindReaderF As Boolean = False
            Dim strPortNo As Integer = fnGetPortNo(strFindReaderF)
            Dim rssi As Byte ''to detect signal, our program not use
            Dim strActTagAdd As String = ""
            Dim strBlk As Integer = "0" ''Alwasy is "zero"
            Dim frame As Byte()

            If strFindReaderF Then
                'Initialize a GController object. 
                Dim gc As New GController()

                'Get a list of all available serial ports detected on the computer (in string). 
                'Dim allPorts As ArrayList = gc.GetAvailablePort()

                'Opens the COM port to the reader. 
                gc.OpenSerialPort(strPortNo, "115200")

                'In actual programs, commands should only be sent to the reader when needed. 
                Thread.Sleep(2000)
                Thread.Sleep(500)
                Thread.Sleep(2000)

                'Retreives the data frames that are received by the reader. Each data frame ia a byte array. 
                'The parameter "1" indicates that serial communication is being used. Depending on your library version, this parameter may not be needed. 
                Dim dataFrames As ArrayList = gc.GetResponseFrames(1)

                If dataFrames.Count > 0 Then
                    ''Handles to data frames that were received from "GetResponseFrames()". 
                    For Each frame In dataFrames
                        GController.GetTagData(frame, rssi, strActTagAdd, strBlk, strRFIDTag)

                        If strRFIDTag <> "" Then
                            Exit For
                        End If
                    Next

                Else
                    strErrorMsg = "Please put Active Tag near Reader to read."
                End If

                'Closes the COM port. 
                gc.CloseSerialPort()

            Else ''*** reader is not find
                strErrorMsg = "Please make sure Reader is plug into PC."
            End If

            fnReadActTag = strRFIDTag
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Sub fnWriteActTag(ByVal strRFIDTag As String, ByRef strErrorMsg As String)
        Try
            Dim strFindReaderF As Boolean = False
            Dim strPortNo As Integer = fnGetPortNo(strFindReaderF)
            Dim rssi As Byte ''to detect signal, our program not use
            Dim strActTagAdd As String = ""
            Dim strBlk As Integer = "0" ''Alwasy is "zero"
            Dim frame As Byte()

            If strFindReaderF Then
                'Initialize a GController object. 
                Dim gc As New GController()

                'Get a list of all available serial ports detected on the computer (in string). 
                ' Dim allPorts As ArrayList = gc.GetAvailablePort()

                'Opens the COM port to the reader. 
                gc.OpenSerialPort(strPortNo, "115200")

                'In actual programs, commands should only be sent to the reader when needed. 
                Thread.Sleep(2000)
                Thread.Sleep(500)
                Thread.Sleep(2000)

                'Retreives the data frames that are received by the reader. Each data frame ia a byte array. 
                'The parameter "1" indicates that serial communication is being used. Depending on your library version, this parameter may not be needed. 
                Dim dataFrames As ArrayList = gc.GetResponseFrames(1)

                If dataFrames.Count > 0 Then
                    ''Handles to data frames that were received from "GetResponseFrames()". 
                    For Each frame In dataFrames
                        'GController.GetTagData(dataFrames(0), rssi, strActTagAdd, strBlk, "")
                        GController.GetTagData(frame, rssi, strActTagAdd, strBlk, "")

                        If strActTagAdd <> "" Then
                            gc.SendSetTagData(1, strActTagAdd, "000000000000", "0", strRFIDTag)

                            Exit For
                        End If
                    Next
                Else
                    strErrorMsg = "Active Tag write failed. Please put your Active Tag near Reader or Active Tag just allow to write after switch on the Active Tag within 1 minute."
                End If

                'Closes the COM port. 
                gc.CloseSerialPort()

            Else ''*** reader is not find
                strErrorMsg = "Please make sure Reader is plug into PC."
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
