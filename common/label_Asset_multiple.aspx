<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="label_Asset_multiple.aspx.vb" Inherits="AMS.label_Asset_multiple" %>

<%@ Register Assembly="Neodynamic.WebControls.BarcodeProfessional" Namespace="Neodynamic.WebControls.BarcodeProfessional"
    TagPrefix="neobarcode" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Asset Label</title>
    <LINK href="CommonStyle.css" type="text/css" rel="Stylesheet">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <form id="frm_LabelAsset" method="post" runat="server">
        
            <asp:DataList ID="DLBarcode" Width=100% runat="server">
                <ItemTemplate>
                    <div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; WIDTH: 180px; PADDING-TOP: -25px; TEXT-ALIGN: center">
					    <table cellpadding="0" cellspacing="0" style="width: 100%" align=left border="0">
						    <tr>
							    <%--<td align=left style="height: 13px"><%#Container.DataItem("fld_AssetNFSID")%></td>--%>
                                <td align=left style="height: 6px"><%#Container.DataItem("fld_LocCode")%></td>
							    <td align=right style="height: 6px"><%#Container.DataItem("fld_LocSubCode")%></td>
						    </tr>
						    <tr>
							    <td colspan=2 align=left>
							        <neobarcode:barcodeprofessional id="barcodeCode" runat="server" Symbology="Code128" Font-Underline="False" Font-Strikeout="False"
									    Font-Size="6pt" Font-Names="Arial" Font-Italic="False" Font-Bold="False" BackColor="White" CacheExpiresAtDateTime="" Code='<%# Container.DataItem("fld_AssetBarcode") %>' 
									    ForeColor="Black" Height="35px" IsbnSupplementCode="" Width="170px" QuietZoneWidth="0" DisplayCode=false Dpi="80"
									     ></neobarcode:barcodeprofessional>
							    </td>
						    </tr>
						    <tr>
							    <td align=left style="height: 6px"><%#Container.DataItem("fld_AssetBarcode")%></td>
							    <td align=right style="height: 6px"><%#Container.DataItem("fld_DeptCode")%></td>
						    </tr>
						    <%--<tr>
							    <td colspan=2 align=left><%#Container.DataItem("fld_AssetDesc")%></td>
						    </tr>--%>
					    </table>
				    </div>
                </ItemTemplate>            
            </asp:DataList>
        
        	<input type=hidden id=hdnAssetIDs value="" runat=server />
            <asp:DataList ID="DataList1" runat="server" Visible=false>
                <ItemTemplate>
                    <neobarcode:BarcodeProfessional ID="BarcodeProfessional1" runat="server" BackColor="White"
                        CacheExpiresAtDateTime=""  Font-Bold="False"
                        Font-Italic="False" Font-Names="Arial" Font-Size="8pt" Font-Strikeout="False"
                        Font-Underline="False" ForeColor="Black" Height="50px" IsbnSupplementCode=""
                        Width="183px" Code='<%# Container.DataItem %>' />
                </ItemTemplate>
            </asp:DataList>
        </form>
		<script type="text/javascript">       
         // Print the window            
         window.print();
         
         // Close the window
         //window.close();    
		</script>
</body>
</html>
