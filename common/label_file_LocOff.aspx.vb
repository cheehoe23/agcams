#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region


Partial Class label_file_LocOff
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents butClose As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Me.butClose.Attributes.Add("OnClick", "return fnCloseWin()")
        If Len(Session("UsrID")) = 0 Then
            Dim strJavaScript As String = ""
            strJavaScript = "<script language = 'Javascript'>" & _
                            "parent.location.href='../common/logout.aspx';" & _
                            "</script>"
            Response.Write(strJavaScript)
            Exit Sub
        End If

        Neodynamic.WebControls.BarcodeProfessional.BarcodeProfessional.LicenseOwner = "Ng Ban Loo-Standard Edition-Developer License"
        Neodynamic.WebControls.BarcodeProfessional.BarcodeProfessional.LicenseKey = "4U9TAPBCCXK8V9HNCZHD2XTGSFELFH2B4J4NKLNMPAFST8TJ6E6Q"

        ''*** Start : Changes by CSS, 17 Aug 2009
        Dim strLocOfficerID As String = Request("strLocOfficerID").ToString().Trim()
        Dim dt As DataTable = clsLocationOfficer.fnGetDataByfld_LocationOfficerID(strLocOfficerID).Tables(0)
        
        If dt.Rows.Count > 0 Then
            lblLocOfficerID.Text = dt.Rows(0)("fld_LocationOfficerBarcodeID").ToString()
            lblLocOffName.Text = dt.Rows(0)("fld_LocationOfficerName").ToString()
            barcodeCode.Code = dt.Rows(0)("fld_LocationOfficerBarcodeID").ToString()
        End If

        If Trim(strLocOfficerID) <> "" Then

            clsRFIDLabel.fnCreateRFIDLabelLocationOfficer(strLocOfficerID + "^", Session("UsrID"), _
                                               Server.MapPath(gRFIDLabelPath), "")

            ''Insert Audit Trail
            clsCommon.fnAuditInsertRec(Session("UsrID"), strLocOfficerID.ToString().Replace("^", ","), "RFID passive label printed.")

        End If

    End Sub
End Class
