<%@ Page Language="vb" AutoEventWireup="false" Codebehind="logout.aspx.vb" Inherits="AMS.logout" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>logout</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="CommonStyle.css" type="text/css" rel="Stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<FORM id="frmLogout" method="post" runat="server">
			<a name="top"></a>
			<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td valign="middle" align="center">
						<table width="780" height="380" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="3">
									<table width="100%" cellpadding="0" border="0" cellspacing="0">
										<tr>
											<td valign="top" width="1" height="1"><img src="../images/lTop.jpg"></td>
											<td background="../images/topmid.gif"><img src="../images/spacer.gif" height="18"></td>
											<td valign="top" width="1" height="1"><img src="../images/rTop.jpg"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" width="1" background="../images/leftmid.gif"><img src="../images/spacer.gif" width="12"></td>
								<td height="100%" width="100%">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td valign="middle" align="center" width="40%">
												<img src="../images/AGCLogo.JPG"><br>
												<br>
												<strong>File and Asset Management System</strong>
											</td>
											<td vAlign="middle" align="center">
												<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
												<br>
												<br>
												<asp:Label ID="lblMsg" Runat="server"></asp:Label>
												<br>
												<%--<a href="../login.aspx" target="_parent">Click here</a> for go to Login Page.--%>
                                                <a href="../default.aspx" target="_parent">Click here</a> for go to Login Page.
											</td>
										</tr>
									</table>
								</td>
								<td valign="top" width="1" background="../images/rightmid.gif"><img src="../images/spacer.gif" width="14"></td>
							</tr>
							<tr>
								<td colspan="3">
									<table width="100%" cellpadding="0" border="0" cellspacing="0">
										<tr>
											<td valign="top" width="1" height="1"><img src="../images/lBottom.jpg"></td>
											<td background="../images/bgB.jpg"><img src="../images/spacer.gif" height="18"></td>
											<td valign="top" width="1"><img src="../images/rBottom.jpg"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" align="center" colspan="3">
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="750">
	                                    <tr>
		                                    <td align="center">
			                                    <font color="black" class="Footer">
			                                    <span class="footer">File and Asset Management System<br>
			                                    Copyright � 2016 Government of Singapore . All rights reserved. </span></font>
		                                    </td>
	                                    </tr>
                                    </table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</FORM>
	</body>
</HTML>
