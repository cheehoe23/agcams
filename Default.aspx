<%@ Page Language="vb" AutoEventWireup="false" Codebehind="default.aspx.vb" Inherits="AMS._default" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>File and Asset Management System</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Common/CommonStyle.css" type="text/css" rel="Stylesheet">
		<script language="JavaScript" src="Common/CommonJS.js"></script>
		<script language="javascript">
		function chkFrm() {
			var foundError = false;
			
			//Validate Login ID
			if (!foundError && gfnIsFieldBlank(document.frmLogin.txtUserId)) {
				foundError=true
				document.frmLogin.txtUserId.focus()
				alert("Please enter your User ID.")
			}
			//Validate Login ID
			if (!foundError && gfnIsFieldBlank(document.frmLogin.txtPwd)) {
				foundError=true
				document.frmLogin.txtPwd.focus()
				alert("Please enter your Password.")
			}

 			if (!foundError)
 				return true
			else
				return false
		}
		</script>
	</HEAD>
	<body bgColor="#ffffff" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<FORM id="frmLogin" method="post" runat="server">
			<a name="top"></a>
			<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td valign="middle" align="center">
						<table width="780" height="380" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="3">
									<table width="100%" cellpadding="0" border="0" cellspacing="0">
										<tr>
											<td valign="top" width="1" height="1"><img src="images/lTop.jpg"></td>
											<td background="images/topmid.gif"><img src="images/spacer.gif" height="18"></td>
											<td valign="top" width="1" height="1"><img src="images/rTop.jpg"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" width="1" background="images/leftmid.gif"><img src="images/spacer.gif" width="12"></td>
								<td height="100%" width="100%">
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td valign="middle" align="center" width="40%">
												<img src="images/AGCLogo.JPG">
												<br><br>
												<strong>File and Asset Management System</strong>
												</td>
											<td valign="middle" align="center" width="60%">
												<asp:Label ID="lb" runat="server"></asp:Label>
												<asp:label id="txtErrMsg" Runat="server" cssclass="LabelErrorText"></asp:label>
												<br>
												<br>
												<div id=divDetails runat=server visible=false>
												<table width="100%" cellpadding="2" cellspacing="2" border="0">
													<tr>
														<td valign="middle" align="right" width="20%">UserID :
														</td>
														<td valign="top" width="*"><asp:textbox id="txtUserId" Runat="server" tooltip="Enter User ID" MaxLength="20"></asp:textbox></td>
													</tr>
													<tr>
														<td align="right" valign="middle">Password :
														</td>
														<td valign="top"><asp:textbox id="txtPwd" Runat="server" tooltip="Enter Password" maxLength="20" TextMode="Password"></asp:textbox></td>
													</tr>
													<TR>
														<TD>&nbsp;</TD>
														<TD>&nbsp;</TD>
													</TR>
													<TR>
														<TD>&nbsp;</TD>
														<TD><asp:button id="btnSubmit" runat="server" text="Submit"></asp:button>&nbsp;
															<asp:button id="btnReset" runat="server" text="Reset"></asp:button></TD>
													</TR>
												</table>
												</div>
											</td>
										</tr>
									</table>
								</td>
								<td valign="top" width="1" background="images/rightmid.gif"><img src="images/spacer.gif" width="14"></td>
							</tr>
							<tr>
								<td colspan="3">
									<table width="100%" cellpadding="0" border="0" cellspacing="0">
										<tr>
											<td valign="top" width="1" height="1"><img src="images/lBottom.jpg"></td>
											<td background="images/bgB.jpg"><img src="images/spacer.gif" height="18"></td>
											<td valign="top" width="1"><img src="images/rBottom.jpg"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" align="center" colspan="3">
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="750">
	                                    <tr>
		                                    <td align="center">
			                                    <font color="black" class="Footer">
			                                    <span class="footer">File and Asset Management System<br>
			                                    Copyright � 2016 Government of Singapore . All rights reserved. </span></font>
		                                    </td>
	                                    </tr>
                                    </table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</FORM>
	</body>
</HTML>
