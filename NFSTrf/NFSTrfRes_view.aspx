<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NFSTrfRes_view.aspx.vb" Inherits="AMS.NFSTrfRes_view" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet" />
	<script language="JavaScript" type="text/javascript" src="../Common/CommonJS.js"></script>
	<script language="javascript" type="text/javascript">
	    function chkFrm() {
			var foundError = false;
		    
		    //validate Import from date
			if (!foundError && gfnCheckDate(document.frmNFSTrf.txtImpFrmDt, "Import From Date", "O") == false) {
				foundError=true
			}
			
			//validate Import to date
			if (!foundError && gfnCheckDate(document.frmNFSTrf.txtImpToDt, "Import To Date", "O") == false) {
				foundError=true
			}
			
			//validate Archive from date
			if (!foundError && gfnCheckDate(document.frmNFSTrf.txtArcFrmDt, "Archive From Date", "O") == false) {
				foundError=true
			}
			
			//validate Archive to date
			if (!foundError && gfnCheckDate(document.frmNFSTrf.txtArcToDt, "Archive To Date", "O") == false) {
				foundError=true
			}
			
			 
 			if (!foundError){
 			   return true;
 			}
			else
				return false;
		}
	    function fnCheckSelect(ChkBoxID, CallType) 
			{
				var flag;
				flag = false;
									
				if (gfnCheckSelectByValue(ChkBoxID)){
				
				    if (CallType == 'R') {
				        flag = window.confirm("You have chosen to restore one or more record(s). Continue?");
				    }
				    else{
				        flag = window.confirm("You have chosen to delete one or more record(s).\nYou cannot undo this action. Continue?");
				    }
					
				}
				else  {
				    alert("At least one record should be selected.");
					flag = false;
				}
				return flag;
			}
	</script>
</head>
<body>
    <form id="frmNFSTrf" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>NFS Transaction : Restore NFS Transaction</B></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																				    <TR>
																					    <TD width="35%">
																						    <FONT class="DisplayTitle">Import Date : </FONT>
																					    </TD>
																					    <TD width="65%">
																						    From <asp:TextBox ID="txtImpFrmDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.frmNFSTrf.txtImpFrmDt, document.frmNFSTrf.txtImpToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtImpToDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.frmNFSTrf.txtImpFrmDt, document.frmNFSTrf.txtImpToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Transaction Type : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:DropDownList ID=ddlTransType runat=server></asp:DropDownList>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">NFS ID : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:TextBox ID=txtNFSID runat=server MaxLength=250></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD width="35%">
																						    <FONT class="DisplayTitle">Archive Date : </FONT>
																					    </TD>
																					    <TD width="65%">
																						    From <asp:TextBox ID="txtArcFrmDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.frmNFSTrf.txtArcFrmDt, document.frmNFSTrf.txtArcToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtArcToDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.frmNFSTrf.txtArcFrmDt, document.frmNFSTrf.txtArcToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>    
																				        <TD>&nbsp;</TD>
																						<TD align="left" style="height: 36px"><BR>
																							<asp:Button id="butSearch" Text="Search" Runat="Server" />
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />
																						</TD>
																					</TR>
																				    <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Search Result</b></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><asp:button id="butRestore" Runat="server" Text="Restore Transaction"></asp:button>
																						                <asp:button id="butDelete" Runat="server" Text="Delete Transaction"></asp:button><br />
																						                <font color="red">Note:</font> Only max. of 1,000 records are displayed. For more than 1,000 records, please use the search function.
																						</TD>
																					</TR>
																					<tr>
								                                                        <td vAlign="top" align="left" bgColor="#a3a9cc" colSpan="2">
									                                                        <table cellSpacing="1" cellPadding="1" width="100%" border="0">
										                                                        <tr>
											                                                        <td width="20%"><asp:dropdownlist id="ddlPageSize" Runat="server" AutoPostBack="True"></asp:dropdownlist>
											                                                                        &nbsp;&nbsp;<font color="white">per page</font>
											                                                        </td>
										                                                        </tr>
									                                                        </table>
								                                                        </td>
							                                                        </tr>
																					<TR>
																						<TD colSpan="2">
																						    <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgNFSTrf" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AllowSorting="True" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" AllowPaging="True" PageSize="10" PagerStyle-HorizontalAlign="Right"
										                                                        PagerStyle-VerticalAlign="Middle" PagerStyle-Mode="NumericPages" PagerStyle-Position="Top"
										                                                        PagerStyle-BackColor="#a3a9cc" datakeyfield="fld_cronDetailID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cronDetailID" headertext="fld_cronDetailID" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdTransType" SortExpression="fld_cdTransType" headertext="Transaction Type" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdNFSID" SortExpression="fld_cdNFSID" headertext="NFS ID" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdPurchaseDt" headertext="Purchase Date" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdCatCode" headertext="Category" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdCostCenter" headertext="Cost Center" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdQuantity" headertext="Quantity" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdCost" headertext="Cost(S$)" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdDesc" headertext="Description" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdNFSIDOld" headertext="Old NFS ID" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_CreatedDt" SortExpression="fld_CreatedDt" headertext="Import Date" ItemStyle-Height="10" dataformatstring="{0:dd/MM/yyyy HH:mm:ss}">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="5%">
									                                                                    <headertemplate>
										                                                                    <asp:checkbox id="cdCheckAll" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckS', 'cdCheckAll');" runat="server" />
									                                                                    </headertemplate>
									                                                                    <itemtemplate>
										                                                                    <center>
											                                                                    <asp:checkbox id="cdCheckS" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckS', 'cdCheckAll');" runat="server" />
										                                                                    </center>
									                                                                    </itemtemplate>
								                                                                    </asp:templatecolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
																						</TD>
																					</TR>
																					
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
			<!-- Start: Hidden Fields -->
			<!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
				src="../common/DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="Z-INDEX:101; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px">
			</iframe>
			<input id="hdnSortName" type="hidden" name="hdnSortName" runat="server">
			<input id="hdnSortAD" type="hidden" name="hdnSortAD" runat="server"> 
			<input id="hdnImpFrmDt" type="hidden" name="hdnImpFrmDt" runat="server"> 
			<input id="hdnImpToDt" type="hidden" name="hdnImpToDt" runat="server"> 
			<input id="hdnTrfType" type="hidden" name="hdnTrfType" runat="server"> 
			<input id="hdnNFSID" type="hidden" name="hdnNFSID" runat="server"> 
			<input id="hdnArcFrmDt" type="hidden" name="hdnArcFrmDt" runat="server"> 
			<input id="hdnArcToDt" type="hidden" name="hdnArcToDt" runat="server"> 
			<!-- End  : Hidden Fields -->
	</form>
</body>
</html>
