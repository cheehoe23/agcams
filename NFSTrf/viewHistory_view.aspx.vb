#Region "Information Section"
' ****************************************************************************************************
' Description       : View NFS Transaction History
' Purpose           : To view NFS Transaction History
' Date              : 25/12/20087
' **************************************************************************************************** 
#End Region


Partial Public Class viewHistory_view
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butSearch.Attributes.Add("OnClick", "return chkFrm()")

                ''default
                hdnSortName.Value = "logDate"
                hdnSortAD.Value = "DESC"
                hdnHisFrmDt.Value = ""
                hdnHisToDt.Value = ""

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
        Try
            hdnHisFrmDt.Value = txtHisFrmDt.Text
            hdnHisToDt.Value = txtHisToDt.Text

            ''Display Record
            fnPopulateRecords()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Sort Header Display
            fnSortHeaderDisplay()

            ''Get User Group Records
            dgTrfHis.DataSource = clsNFSTrf.fnNFSTrfHis_GetRecords(hdnHisFrmDt.Value, hdnHisToDt.Value, hdnSortName.Value, hdnSortAD.Value)
            dgTrfHis.DataBind()
            If Not dgTrfHis.Items.Count > 0 Then ''Not Records found
                ddlPageSize.Enabled = False
                lblErrorMessage.Text = "Not Records Found."
                lblErrorMessage.Visible = True
            Else
                ddlPageSize.Enabled = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        Dim intSortIndex As Integer
        Dim strSortHeader As String = ""

        ''Set Default Header
        dgTrfHis.Columns(1).HeaderText = "Date"
        dgTrfHis.Columns(2).HeaderText = "Action"
        dgTrfHis.Columns(3).HeaderText = "Action By"


        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "logDate"
                intSortIndex = 1
                strSortHeader = "Date"
            Case "logDetail"
                intSortIndex = 2
                strSortHeader = "Action"
            Case "ActionBy"
                intSortIndex = 3
                strSortHeader = "Action By"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgTrfHis.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgTrfHis.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        txtHisFrmDt.Text = ""
        txtHisToDt.Text = ""
    End Sub

    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgTrfHis.CurrentPageIndex = 0
        dgTrfHis.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Private Sub dgTrfHis_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgTrfHis.PageIndexChanged
        dgTrfHis.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub dgTrfHis_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgTrfHis.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

End Class