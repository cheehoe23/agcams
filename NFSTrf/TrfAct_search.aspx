<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TrfAct_search.aspx.vb" Inherits="AMS.TrfAct_search" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet" />
	<script language="JavaScript" type="text/javascript" src="../Common/CommonJS.js"></script>
	<script language="javascript" type="text/javascript">
	    function chkSrhFrm() {
			var foundError = false;
		    
		    //validate Import from date
			if (!foundError && gfnCheckDate(document.frmNFSTrf.txtImpFrmDt, "Import From Date", "O") == false) {
				foundError=true
			}
			
			//validate Import to date
			if (!foundError && gfnCheckDate(document.frmNFSTrf.txtImpToDt, "Import To Date", "O") == false) {
				foundError=true
			}
						 
 			if (!foundError){
 			   return true;
 			}
			else
				return false;
		}
	    function fnCheckSelect(TrfType, ChkBoxID) 
			{
				var flag, confirmAction;
				flag = false;
				
				//Get Message for related action
				switch(TrfType)    
				{
					case 'ADD':
						confirmAction = "You have chosen to add one or more record(s).\nYou cannot undo this action. Continue?"; 
						break    
								
					case 'TRF-IN1':
						confirmAction = "You have chosen to add one or more record(s).\nYou cannot undo this action. Continue?"; 
						break		
						
					case 'TRF-IN2':
						confirmAction = "You have chosen to update one or more EXCEPTION record(s).\nYou cannot undo this action. Continue?"; 
						break	
									
					case 'RCT':
						confirmAction = "You have chosen to re-category one or more record(s).\nYou cannot undo this action. Continue?"; 
						break
						
					case 'ADJ':
						confirmAction = "You have chosen to adjust cost for one or more record(s).\nYou cannot undo this action. Continue?"; 
						break
						
					case 'ADDExp':
						confirmAction = "You have chosen to add one or more EXCEPTION record(s).\nYou cannot undo this action. Continue?"; 
						break 
						
					case 'TRF-IN1Exp':
						confirmAction = "You have chosen to add one or more EXCEPTION record(s).\nYou cannot undo this action. Continue?"; 
						break 
						
					case 'TRF-IN2Exp':
						confirmAction = "You have chosen to update one or more EXCEPTION record(s).\nYou cannot undo this action. Continue?"; 
						break	
				}
				
				
				if (gfnCheckSelectByValue(ChkBoxID)){
					flag = window.confirm(confirmAction);
				}
				else  {
				    alert("At least one record should be selected.");
					flag = false;
				}
				return flag;
			}
		function fnQuantityChg(textboxId){
//		    alert("Quantity change, " + textboxId + ", " + document.frmNFSTrf.dgTrfAdd_ctl02_txtQtyADD.value);
//		    var foundError, flag, confirmAction;
//		    foundError = false;
//			flag = false;
//			confirmAction = "You have change the Quantity. Are you sure want to change it?";
//			
//			if (!foundError && gfnCheckNumeric(eval("document.frmNFSTrf."+textboxId),'')) {
//				    foundError=true;
//				    //document.frmNFSTrf.txtCost.focus();
//				    eval("document.frmNFSTrf."+textboxId+".focus();");
//				    alert("Quantity just allow number only.");
//			} 
//			
//			if (!foundError){
//			    flag = window.confirm(confirmAction);
//			    if (!flag){
//			        eval("document.frmNFSTrf."+textboxId+".focus();");
//			        alert("in, " + "document.frmNFSTrf."+textboxId+".focus();");
//			        document.frmNFSTrf.dgTrfAdd_ctl02_txtQtyADD.focus();
//			        document.frmNFSTrf.txtImpFrmDt.value = textboxId;
//			    }
//			}

			alert("You have changed the Quantity.");
			return false;
		}
	</script>
</head>
<body>
    <form id="frmNFSTrf" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>NFS Transaction : Transaction Action</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																				    <TR>
																					    <TD width="35%">
																						    <FONT class="DisplayTitle">Import Date : </FONT>
																					    </TD>
																					    <TD width="65%">
																						    From <asp:TextBox ID="txtImpFrmDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.frmNFSTrf.txtImpFrmDt, document.frmNFSTrf.txtImpToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtImpToDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.frmNFSTrf.txtImpFrmDt, document.frmNFSTrf.txtImpToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>    
																				        <TD>&nbsp;</TD>
																						<TD align="left" style="height: 36px"><BR>
																							<asp:Button id="butSearch" Text="Search" Runat="Server" />
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />
																						</TD>
																					</TR>
																				    <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Search Result</b></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Transaction Type : ADD</font><br />
																						                <asp:Label ID=lblCCtrADD runat="server"></asp:Label> </TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><asp:button id="butTrfAdd" Runat="server" Text="Add Transaction"></asp:button></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																						    <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgTrfAdd" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff"   datakeyfield="fld_cronDetailID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cronDetailID" headertext="fld_cronDetailID" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="CatID" headertext="CatID" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cdTransType" headertext="fld_cdTransType" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cdQuantity" headertext="fld_cdQuantity" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdNFSID" headertext="NFS ID" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn HeaderText="Other Information" ItemStyle-Width="35%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												                                                        <itemtemplate>
													                                                        <%#fnShowAddInfoDetail(DataBinder.Eval(Container.DataItem, "fld_cdPurchaseDt"), DataBinder.Eval(Container.DataItem, "fld_cdCatCode"), DataBinder.Eval(Container.DataItem, "fld_cdCostCenter"), DataBinder.Eval(Container.DataItem, "fld_cdQuantity"), DataBinder.Eval(Container.DataItem, "fld_cdCost"), DataBinder.Eval(Container.DataItem, "fld_cdDesc"))%>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
											                                                        <asp:templatecolumn HeaderText="Fill in following information" ItemStyle-Width="50%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												                                                        <itemtemplate>
												                                                                 <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				                                    <TBODY align=left>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <%--<font color='red'>*</font><FONT class="DisplayTitle">Quantity : </FONT>--%>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:TextBox ID="txtQtyADD" Visible=false runat="server" Width="80px" MaxLength="20" onchange="javascript:fnQuantityChg(this.id);"></asp:TextBox>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Sub Category : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlCatSubADD" runat="server"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Department : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlDeptADD" runat="server" Width="100px"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Location : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlLocADD" runat="server" Width="100px" OnSelectedIndexChanged="ddlLocADD_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Sub Location : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlLocSubADD" runat="server" Width="100px"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <FONT class="DisplayTitle">Assigned Owner : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlOwnerADD" runat="server"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%" valign=top>
																						                                        <FONT class="DisplayTitle">Remarks : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                         <asp:TextBox id="txtRemarkADD" TextMode=MultiLine maxlength="1000" Columns="40" Rows="5" Runat="server"></asp:TextBox>
																					                                        </TD>
																				                                        </TR>
																				                                     </TBODY>
																				                                  </TABLE>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
                                                                                                    <asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="10%">
									                                                                    <headertemplate>
										                                                                    <asp:checkbox id="cdCheckAddAll" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckAddS', 'cdCheckAddAll');" runat="server" />
									                                                                    </headertemplate>
									                                                                    <itemtemplate>
										                                                                    <center>
											                                                                    <asp:checkbox id="cdCheckAddS" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckAddS', 'cdCheckAddAll');" runat="server" />
										                                                                    </center>
									                                                                    </itemtemplate>
								                                                                    </asp:templatecolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Transaction Type : TRF-IN</font> <br />
																						                <asp:Label ID=lblCCtrTrfINAdd runat="server"></asp:Label> </TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><asp:button id="butTrfInAdd" Runat="server" Text="Add Transaction"></asp:button></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																						    <!--Datagrid for display record.-->
																						    <asp:datagrid id="dgTrfInAdd" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff"   datakeyfield="fld_cronDetailID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cronDetailID" headertext="fld_cronDetailID" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="CatID" headertext="CatID" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cdTransType" headertext="fld_cdTransType" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cdQuantity" headertext="fld_cdQuantity" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdNFSID" headertext="NFS ID" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn HeaderText="Other Information" ItemStyle-Width="35%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												                                                        <itemtemplate>
													                                                        <%#fnShowAddInfoDetail(DataBinder.Eval(Container.DataItem, "fld_cdPurchaseDt"), DataBinder.Eval(Container.DataItem, "fld_cdCatCode"), DataBinder.Eval(Container.DataItem, "fld_cdCostCenter"), DataBinder.Eval(Container.DataItem, "fld_cdQuantity"), DataBinder.Eval(Container.DataItem, "fld_cdCost"), DataBinder.Eval(Container.DataItem, "fld_cdDesc"))%>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
											                                                        <asp:templatecolumn HeaderText="Fill in following information" ItemStyle-Width="50%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												                                                        <itemtemplate>
												                                                                 <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				                                    <TBODY align=left>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                       <%-- <font color='red'>*</font><FONT class="DisplayTitle">Quantity : </FONT>--%>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:TextBox ID="txtQtyTIADD" Visible=false runat="server" Width= "80px" MaxLength="20" onchange="javascript:fnQuantityChg(this.id);"></asp:TextBox>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Sub Category : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlCatSubTIADD" runat="server"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Department : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlDeptTIADD" runat="server" Width="100px"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Location : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlLocTIADD" runat="server" Width="100px" OnSelectedIndexChanged="ddlLocTrfInAdd_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Sub Location : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlLocSubTIADD" runat="server" Width="100px"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <FONT class="DisplayTitle">Assigned Owner : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlOwnerTIADD" runat="server"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%" valign=top>
																						                                        <FONT class="DisplayTitle">Remarks : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                         <asp:TextBox id="txtRemarkTIADD" TextMode=MultiLine maxlength="1000" Columns="40" Rows="5" Runat="server"></asp:TextBox>
																					                                        </TD>
																				                                        </TR>
																				                                     </TBODY>
																				                                  </TABLE>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
                                                                                                    <asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="10%">
									                                                                    <headertemplate>
										                                                                    <asp:checkbox id="cdCheckTIAddAll" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckTIAddS', 'cdCheckTIAddAll');" runat="server" />
									                                                                    </headertemplate>
									                                                                    <itemtemplate>
										                                                                    <center>
											                                                                    <asp:checkbox id="cdCheckTIAddS" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckTIAddS', 'cdCheckTIAddAll');" runat="server" />
										                                                                    </center>
									                                                                    </itemtemplate>
								                                                                    </asp:templatecolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Transaction Type : RCT</font></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><asp:button id="butTrfRCT" Runat="server" Text="Update Transaction"></asp:button></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																						    <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgRCTUpd" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff"   datakeyfield="fld_cronDetailID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cronDetailID" headertext="fld_cronDetailID" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="CatID" headertext="CatID" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cdTransType" headertext="fld_cdTransType" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cdQuantity" headertext="fld_cdQuantity" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdNFSID" headertext="NFS ID" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn HeaderText="Existing Record(s)" ItemStyle-Width="40%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												                                                        <itemtemplate>
													                                                        <%#fnShowExistingAMSDetail(DataBinder.Eval(Container.DataItem, "fld_cdNFSID"), "RCT")%>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
											                                                        <asp:templatecolumn HeaderText="Fill in following information" ItemStyle-Width="45%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												                                                        <itemtemplate>
												                                                                 <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				                                    <TBODY align=left>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Category : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <%# DataBinder.Eval(Container.DataItem, "fld_cdCatCode")%>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Sub Category : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlCatSubRCT" runat="server"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                     </TBODY>
																				                                  </TABLE>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
                                                                                                    <asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="5%">
									                                                                    <headertemplate>
										                                                                    <asp:checkbox id="cdCheckRCTAll" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckRCTS', 'cdCheckRCTAll');" runat="server" />
									                                                                    </headertemplate>
									                                                                    <itemtemplate>
										                                                                    <center>
											                                                                    <asp:checkbox id="cdCheckRCTS" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckRCTS', 'cdCheckRCTAll');" runat="server" />
										                                                                    </center>
									                                                                    </itemtemplate>
								                                                                    </asp:templatecolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Transaction Type : ADJ</font></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><asp:button id="butTrfADJ" Runat="server" Text="Update Transaction"></asp:button></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																						    <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgADJUpd" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff"   datakeyfield="fld_cronDetailID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cronDetailID" headertext="fld_cronDetailID" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cdTransType" headertext="fld_cdTransType" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cdQuantity" headertext="fld_cdQuantity" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdNFSID" headertext="NFS ID" ItemStyle-Height="10">
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdCost" headertext="Cost (S$)" ItemStyle-Height="10">
												                                                        <itemstyle width="20%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn HeaderText="Existing Record(s)" ItemStyle-Width="60%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												                                                        <itemtemplate>
													                                                        <%#fnShowExistingAMSDetail(DataBinder.Eval(Container.DataItem, "fld_cdNFSID"), "ADJ")%>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
                                                                                                    <asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="5%">
									                                                                    <headertemplate>
										                                                                    <asp:checkbox id="cdCheckADJAll" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckADJS', 'cdCheckADJAll');" runat="server" />
									                                                                    </headertemplate>
									                                                                    <itemtemplate>
										                                                                    <center>
											                                                                    <asp:checkbox id="cdCheckADJS" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckADJS', 'cdCheckADJAll');" runat="server" />
										                                                                    </center>
									                                                                    </itemtemplate>
								                                                                    </asp:templatecolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Exception NFS Record(s)</b></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Transaction Type : TRF-IN</font> <br />
																						                <asp:Label ID=lblCCtrTrfINUpd runat="server"></asp:Label> </TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><asp:button id="butTrfInUpd" Runat="server" Text="Update Transaction"></asp:button></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																						    <!--Datagrid for display record.-->
																						    <asp:datagrid id="dgTrfInUpd" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff"   datakeyfield="fld_cronDetailID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cronDetailID" headertext="fld_cronDetailID" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="CatID" headertext="CatID" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cdTransType" headertext="fld_cdTransType" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cdQuantity" headertext="fld_cdQuantity" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdNFSID" headertext="NFS ID" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="AMSTrfInDetails" headertext="Asset ID (AMS)" ItemStyle-Height="10">
												                                                        <itemstyle width="12%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn HeaderText="Other Information" ItemStyle-Width="28%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												                                                        <itemtemplate>
													                                                        <%#fnShowTrfInUpdInfoDetail(DataBinder.Eval(Container.DataItem, "fld_cdPurchaseDt"), DataBinder.Eval(Container.DataItem, "fld_cdCatCode"), DataBinder.Eval(Container.DataItem, "fld_cdCostCenter"), DataBinder.Eval(Container.DataItem, "fld_cdNFSIDOld"), DataBinder.Eval(Container.DataItem, "fld_cdCost"), DataBinder.Eval(Container.DataItem, "fld_cdDesc"))%>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
											                                                        <asp:templatecolumn HeaderText="Fill in following information" ItemStyle-Width="45%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												                                                        <itemtemplate>
												                                                                 <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				                                    <TBODY align=left>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Sub Category : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlCatSubTIUpd" runat="server"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Department : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlDeptTIUpd" runat="server" Width="100px"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Location : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlLocTIUpd" runat="server" Width="100px" OnSelectedIndexChanged="ddlLocTrfInUpd_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Sub Location : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlLocSubTIUpd" runat="server" Width="100px"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <FONT class="DisplayTitle">Assigned Owner : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlOwnerTIUpd" runat="server"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%" valign=top>
																						                                        <FONT class="DisplayTitle">Remarks : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                         <asp:TextBox id="txtRemarkTIUpd" TextMode=MultiLine maxlength="1000" Columns="40" Rows="5" Runat="server"></asp:TextBox>
																					                                        </TD>
																				                                        </TR>
																				                                     </TBODY>
																				                                  </TABLE>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
                                                                                                    <asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="10%">
									                                                                    <headertemplate>
										                                                                    <asp:checkbox id="cdCheckTIUpdAll" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckTIUpdS', 'cdCheckTIUpdAll');" runat="server" />
									                                                                    </headertemplate>
									                                                                    <itemtemplate>
										                                                                    <center>
											                                                                    <asp:checkbox id="cdCheckTIUpdS" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckTIUpdS', 'cdCheckTIUpdAll');" runat="server" />
										                                                                    </center>
									                                                                    </itemtemplate>
								                                                                    </asp:templatecolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Transaction Type : ADD</font><br />
																						                <asp:Label ID=lblCCtrADDExp runat="server"></asp:Label> </TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><asp:button id="butTrfAddExp" Runat="server" Text="Add Transaction"></asp:button></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																						    <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgTrfAddExp" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff"   datakeyfield="fld_cronDetailID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cronDetailID" headertext="fld_cronDetailID" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="CatID" headertext="CatID" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cdTransType" headertext="fld_cdTransType" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cdQuantity" headertext="fld_cdQuantity" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdNFSID" headertext="NFS ID" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn HeaderText="Other Information" ItemStyle-Width="35%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												                                                        <itemtemplate>
													                                                        <%#fnShowAddInfoDetail(DataBinder.Eval(Container.DataItem, "fld_cdPurchaseDt"), DataBinder.Eval(Container.DataItem, "fld_cdCatCode"), DataBinder.Eval(Container.DataItem, "fld_cdCostCenter"), DataBinder.Eval(Container.DataItem, "fld_cdQuantity"), DataBinder.Eval(Container.DataItem, "fld_cdCost"), DataBinder.Eval(Container.DataItem, "fld_cdDesc"))%>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
											                                                        <asp:templatecolumn HeaderText="Fill in following information" ItemStyle-Width="50%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												                                                        <itemtemplate>
												                                                                 <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				                                    <TBODY align=left>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <%--<font color='red'>*</font><FONT class="DisplayTitle">Quantity : </FONT>--%>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:TextBox ID="txtQtyADD" Visible=false runat="server" Width="80px" MaxLength="20" onchange="javascript:fnQuantityChg(this.id);"></asp:TextBox>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Sub Category : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlCatSubADD" runat="server"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Department : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlDeptADD" runat="server" Width="100px"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Location : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlLocADD" runat="server" Width="100px" OnSelectedIndexChanged="ddlLocADD_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Sub Location : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlLocSubADD" runat="server" Width="100px"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <FONT class="DisplayTitle">Assigned Owner : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlOwnerADD" runat="server"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%" valign=top>
																						                                        <FONT class="DisplayTitle">Remarks : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                         <asp:TextBox id="txtRemarkADD" TextMode=MultiLine maxlength="1000" Columns="40" Rows="5" Runat="server"></asp:TextBox>
																					                                        </TD>
																				                                        </TR>
																				                                     </TBODY>
																				                                  </TABLE>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
                                                                                                    <asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="10%">
									                                                                    <headertemplate>
										                                                                    <asp:checkbox id="cdCheckAddExpAll" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckAddExpS', 'cdCheckAddExpAll');" runat="server" />
									                                                                    </headertemplate>
									                                                                    <itemtemplate>
										                                                                    <center>
											                                                                    <asp:checkbox id="cdCheckAddExpS" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckAddExpS', 'cdCheckAddExpAll');" runat="server" />
										                                                                    </center>
									                                                                    </itemtemplate>
								                                                                    </asp:templatecolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Transaction Type : TRF-IN</font> <br />
																						                <asp:Label ID=lblCCtrTrfINAddExp runat="server"></asp:Label> </TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><asp:button id="butTrfInAddExp" Runat="server" Text="Add Transaction"></asp:button></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																						    <!--Datagrid for display record.-->
																						    <asp:datagrid id="dgTrfInAddExp" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff"   datakeyfield="fld_cronDetailID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cronDetailID" headertext="fld_cronDetailID" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="CatID" headertext="CatID" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cdTransType" headertext="fld_cdTransType" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cdQuantity" headertext="fld_cdQuantity" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdNFSID" headertext="NFS ID" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn HeaderText="Other Information" ItemStyle-Width="35%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												                                                        <itemtemplate>
													                                                        <%#fnShowAddInfoDetail(DataBinder.Eval(Container.DataItem, "fld_cdPurchaseDt"), DataBinder.Eval(Container.DataItem, "fld_cdCatCode"), DataBinder.Eval(Container.DataItem, "fld_cdCostCenter"), DataBinder.Eval(Container.DataItem, "fld_cdQuantity"), DataBinder.Eval(Container.DataItem, "fld_cdCost"), DataBinder.Eval(Container.DataItem, "fld_cdDesc"))%>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
											                                                        <asp:templatecolumn HeaderText="Fill in following information" ItemStyle-Width="50%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												                                                        <itemtemplate>
												                                                                 <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				                                    <TBODY align=left>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <%--<font color='red'>*</font><FONT class="DisplayTitle">Quantity : </FONT>--%>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:TextBox ID="txtQtyTIADD" Visible=false runat="server" Width= "80px" MaxLength="20" onchange="javascript:fnQuantityChg(this.id);"></asp:TextBox>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Sub Category : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlCatSubTIADD" runat="server"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Department : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlDeptTIADD" runat="server" Width="100px"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Location : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlLocTIADD" runat="server" Width="100px" OnSelectedIndexChanged="ddlLocTrfInAdd_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Sub Location : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlLocSubTIADD" runat="server" Width="100px"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <FONT class="DisplayTitle">Assigned Owner : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlOwnerTIADD" runat="server"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%" valign=top>
																						                                        <FONT class="DisplayTitle">Remarks : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                         <asp:TextBox id="txtRemarkTIADD" TextMode=MultiLine maxlength="1000" Columns="40" Rows="5" Runat="server"></asp:TextBox>
																					                                        </TD>
																				                                        </TR>
																				                                     </TBODY>
																				                                  </TABLE>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
                                                                                                    <asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="10%">
									                                                                    <headertemplate>
										                                                                    <asp:checkbox id="cdCheckTIAddExpAll" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckTIAddExpS', 'cdCheckTIAddExpAll');" runat="server" />
									                                                                    </headertemplate>
									                                                                    <itemtemplate>
										                                                                    <center>
											                                                                    <asp:checkbox id="cdCheckTIAddExpS" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckTIAddExpS', 'cdCheckTIAddExpAll');" runat="server" />
										                                                                    </center>
									                                                                    </itemtemplate>
								                                                                    </asp:templatecolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Transaction Type : TRF-IN</font> <br />
																						                <asp:Label ID=lblCCtrTrfINUpdExp runat="server"></asp:Label> </TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><asp:button id="butTrfInUpdExp" Runat="server" Text="Update Transaction"></asp:button></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																						    <!--Datagrid for display record.-->
																						    <asp:datagrid id="dgTrfInUpdExp" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff"   datakeyfield="fld_cronDetailID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cronDetailID" headertext="fld_cronDetailID" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="CatID" headertext="CatID" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cdTransType" headertext="fld_cdTransType" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cdQuantity" headertext="fld_cdQuantity" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdNFSID" headertext="NFS ID" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="AMSTrfInDetails" headertext="Asset ID (AMS)" ItemStyle-Height="10">
												                                                        <itemstyle width="12%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn HeaderText="Other Information" ItemStyle-Width="28%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												                                                        <itemtemplate>
													                                                        <%#fnShowTrfInUpdInfoDetail(DataBinder.Eval(Container.DataItem, "fld_cdPurchaseDt"), DataBinder.Eval(Container.DataItem, "fld_cdCatCode"), DataBinder.Eval(Container.DataItem, "fld_cdCostCenter"), DataBinder.Eval(Container.DataItem, "fld_cdNFSIDOld"), DataBinder.Eval(Container.DataItem, "fld_cdCost"), DataBinder.Eval(Container.DataItem, "fld_cdDesc"))%>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
											                                                        <asp:templatecolumn HeaderText="Fill in following information" ItemStyle-Width="45%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												                                                        <itemtemplate>
												                                                                 <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				                                    <TBODY align=left>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Sub Category : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlCatSubTIUpd" runat="server"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Department : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlDeptTIUpd" runat="server" Width="100px"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Location : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlLocTIUpd" runat="server" Width="100px" OnSelectedIndexChanged="ddlLocTrfInUpd_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <font color='red'>*</font><FONT class="DisplayTitle">Sub Location : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlLocSubTIUpd" runat="server" Width="100px"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%">
																						                                        <FONT class="DisplayTitle">Assigned Owner : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                        <asp:DropDownList ID="ddlOwnerTIUpd" runat="server"></asp:DropDownList>
																					                                        </TD>
																				                                        </TR>
																				                                        <TR>
																					                                        <TD width="35%" valign=top>
																						                                        <FONT class="DisplayTitle">Remarks : </FONT>
																					                                        </TD>
																					                                        <TD width="65%">
																						                                         <asp:TextBox id="txtRemarkTIUpd" TextMode=MultiLine maxlength="1000" Columns="40" Rows="5" Runat="server"></asp:TextBox>
																					                                        </TD>
																				                                        </TR>
																				                                     </TBODY>
																				                                  </TABLE>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
                                                                                                    <asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="10%">
									                                                                    <headertemplate>
										                                                                    <asp:checkbox id="cdCheckTIUpdExpAll" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckTIUpdExpS', 'cdCheckTIUpdExpAll');" runat="server" />
									                                                                    </headertemplate>
									                                                                    <itemtemplate>
										                                                                    <center>
											                                                                    <asp:checkbox id="cdCheckTIUpdExpS" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckTIUpdExpS', 'cdCheckTIUpdExpAll');" runat="server" />
										                                                                    </center>
									                                                                    </itemtemplate>
								                                                                    </asp:templatecolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Others Transaction Type</font> <br />
																						                <font color='red'>Note:</font> No action is applied to this exception records.</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																						    <!--Datagrid for display record.-->
																						    <asp:datagrid id="dgAllExp" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff"   datakeyfield="fld_cronDetailID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
											                                                        <asp:boundcolumn visible="false" datafield="fld_cronDetailID" headertext="fld_cronDetailID" ItemStyle-Height="10">
												                                                        <itemstyle width="1%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdTransType" headertext="Transaction Type" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdNFSID" headertext="NFS ID" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdPurchaseDt" headertext="Purchase Date" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdCatCode" headertext="Category" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdCostCenter" headertext="Cost Center" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdQuantity" headertext="Quantity" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdCost" headertext="Cost(S$)" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdDesc" headertext="Description" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_cdNFSIDOld" headertext="Old NFS ID" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_CreatedDt" headertext="Import Date" ItemStyle-Height="10" dataformatstring="{0:dd/MM/yyyy HH:mm:ss}">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
			<!-- Start: Hidden Fields -->
			<!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
				src="../common/DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="Z-INDEX:101; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px">
			</iframe>
			<input id="hdnImpFrmDt" type="hidden" name="hdnImpFrmDt" runat="server"> 
			<input id="hdnImpToDt" type="hidden" name="hdnImpToDt" runat="server"> 
			<!-- End  : Hidden Fields -->
	</form>
</body>
</html>
