#Region "Information Section"
' ****************************************************************************************************
' Description       : Restore NFS Transaction 
' Purpose           : To Restore NFS Transaction 
' Date              : 03/01/2009
' **************************************************************************************************** 
#End Region

Partial Public Class NFSTrfRes_view
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butSearch.Attributes.Add("OnClick", "return chkFrm()")
                Me.butRestore.Attributes.Add("OnClick", "return fnCheckSelect('cdCheckS','R')")
                Me.butDelete.Attributes.Add("OnClick", "return fnCheckSelect('cdCheckS','D')")

                ''default
                hdnSortName.Value = "fld_CreatedDt"
                hdnSortAD.Value = "DESC"
                hdnImpFrmDt.Value = ""
                hdnImpToDt.Value = ""
                hdnTrfType.Value = ""
                hdnNFSID.Value = ""
                hdnArcFrmDt.Value = ""
                hdnArcToDt.Value = ""

                ''Populate Transaction Type
                mfnPopulateTransactionTypeDDL(ddlTransType)

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()

                ''Check access right
                If (InStr(Session("AR"), "NFSTrf|Res|Res") = 0) Then
                    butRestore.Visible = False
                End If
                If (InStr(Session("AR"), "NFSTrf|Res|Del") = 0) Then
                    butDelete.Visible = False
                End If
                If (InStr(Session("AR"), "NFSTrf|Res|Res") = 0) And (InStr(Session("AR"), "NFSTrf|Res|Del") = 0) Then
                    dgNFSTrf.Columns(12).Visible = False
                End If
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
        Try
            hdnImpFrmDt.Value = Trim(txtImpFrmDt.Text)
            hdnImpToDt.Value = Trim(txtImpToDt.Text)
            hdnTrfType.Value = ddlTransType.SelectedValue
            hdnNFSID.Value = Trim(txtNFSID.Text)
            hdnArcFrmDt.Value = Trim(txtArcFrmDt.Text)
            hdnArcToDt.Value = Trim(txtArcToDt.Text)

            ''Display Record
            fnPopulateRecords()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Sort Header Display
            fnSortHeaderDisplay()

            ''Get Records
            dgNFSTrf.DataSource = clsNFSTrf.fnNFSTrfRes_GetRecords(hdnImpFrmDt.Value, hdnImpToDt.Value, _
                                                                   hdnTrfType.Value, hdnNFSID.Value, _
                                                                   hdnArcFrmDt.Value, hdnArcToDt.Value, _
                                                                   hdnSortName.Value, hdnSortAD.Value)
            dgNFSTrf.DataBind()
            If Not dgNFSTrf.Items.Count > 0 Then ''Not Records found
                ddlPageSize.Enabled = False
                butRestore.Enabled = False
                butDelete.Enabled = False
                lblErrorMessage.Text = "Not Records Found."
                lblErrorMessage.Visible = True
            Else
                ddlPageSize.Enabled = True
                butRestore.Enabled = True
                butDelete.Enabled = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        Dim intSortIndex As Integer
        Dim strSortHeader As String = ""

        ''Set Default Header
        dgNFSTrf.Columns(2).HeaderText = "Transaction Type"
        dgNFSTrf.Columns(3).HeaderText = "NFS ID"
        dgNFSTrf.Columns(11).HeaderText = "Import Date"


        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fld_cdTransType"
                intSortIndex = 2
                strSortHeader = "Transaction Type"
            Case "fld_cdNFSID"
                intSortIndex = 3
                strSortHeader = "NFS ID"
            Case "fld_CreatedDt"
                intSortIndex = 11
                strSortHeader = "Import Date"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgNFSTrf.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgNFSTrf.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            txtImpFrmDt.Text = ""
            txtImpToDt.Text = ""
            ddlTransType.SelectedValue = ""
            txtNFSID.Text = ""
            txtArcFrmDt.Text = ""
            txtArcToDt.Text = ""
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgNFSTrf.CurrentPageIndex = 0
        dgNFSTrf.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Private Sub dgNFSTrf_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgNFSTrf.PageIndexChanged
        dgNFSTrf.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub dgNFSTrf_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgNFSTrf.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

    Protected Sub butRestore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butRestore.Click
        Try
            Dim strXMLrec As String = ""

            ''Get Records selected in XML
            strXMLrec = fnGetXMLRec()

            ''Update record
            If strXMLrec <> "" Then
                Dim intRetVal As Integer
                intRetVal = clsNFSTrf.fnNFSTrfRes_RestoreRecords(strXMLrec, Session("UsrID"))
                If intRetVal > 0 Then
                    ''Get Records
                    fnPopulateRecords()

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + "Record(s) restore succesfully." + "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    lblErrorMessage.Text = "Record(s) restore failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                End If
            Else
                lblErrorMessage.Text = "Please select at least one record to restore."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnGetXMLRec() As String
        Try
            Dim TblNFSAdd As DataTable = fnCreateDataTable()
            Dim RowNFSAdd As DataRow

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox
            Dim sXMLString As String = ""

            For Each GridItem In dgNFSTrf.Items
                chkSelectedRec = CType(GridItem.Cells(12).FindControl("cdCheckS"), CheckBox)
                If chkSelectedRec.Checked Then
                    ''Add New Row to Datatable
                    RowNFSAdd = TblNFSAdd.NewRow()        'declaring a new row
                    RowNFSAdd.Item("dt_CDetID") = GridItem.Cells(0).Text
                    TblNFSAdd.Rows.Add(RowNFSAdd)
                End If
            Next

            If TblNFSAdd.Rows.Count > 0 Then
                ''Get XML
                Dim ds As New DataSet
                ds = New DataSet            'creating a dataset
                ds.Tables.Add(TblNFSAdd)    'assign datatable to dataset
                If Not ds Is Nothing Then
                    Dim loCol As DataColumn
                    'Prepare XML output from the DataSet as a string
                    For Each loCol In ds.Tables(0).Columns
                        loCol.ColumnMapping = System.Data.MappingType.Attribute
                    Next
                    sXMLString = ds.GetXml
                End If
            End If

            fnGetXMLRec = Trim(sXMLString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnCreateDataTable() As DataTable
        Try
            ''creating a table 
            Dim TblNFS As DataTable
            TblNFS = New DataTable("TblNFS")

            ''Column 1: Cron Details ID
            Dim dt_CDetID As DataColumn = New DataColumn("dt_CDetID")    'declaring a column named Name
            dt_CDetID.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            TblNFS.Columns.Add(dt_CDetID)                               'adding the column to table

            ''Set Primary Keys
            '--> Make the ID column the primary key column.
            Dim PrimaryKeyColumns(0) As DataColumn
            PrimaryKeyColumns(0) = TblNFS.Columns("dt_CDetID")
            TblNFS.PrimaryKey = PrimaryKeyColumns

            fnCreateDataTable = TblNFS
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        Try
            Dim strXMLrec As String = ""

            ''Get Records selected in XML
            strXMLrec = fnGetXMLRec()

            ''Update record
            If strXMLrec <> "" Then
                Dim intRetVal As Integer
                intRetVal = clsNFSTrf.fnNFSTrfRes_DeleteRecords(strXMLrec, Session("UsrID"))
                If intRetVal > 0 Then
                    ''Get Records
                    fnPopulateRecords()

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + "Record(s) delete succesfully." + "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    lblErrorMessage.Text = "Record(s) delete failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                End If
            Else
                lblErrorMessage.Text = "Please select at least one record to delete."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class