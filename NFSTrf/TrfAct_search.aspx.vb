#Region "Information Section"
' ****************************************************************************************************
' Description       : Transaction Action
' Purpose           : Transaction Action
' Date              : 26/12/20087
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class TrfAct_search
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butSearch.Attributes.Add("OnClick", "return chkSrhFrm()")
                Me.butTrfAdd.Attributes.Add("OnClick", "return fnCheckSelect('ADD','cdCheckAddS')")
                Me.butTrfInAdd.Attributes.Add("OnClick", "return fnCheckSelect('TRF-IN1','cdCheckTIAddS')")
                Me.butTrfInUpd.Attributes.Add("OnClick", "return fnCheckSelect('TRF-IN2','cdCheckTIUpdS')")
                Me.butTrfRCT.Attributes.Add("OnClick", "return fnCheckSelect('RCT','cdCheckRCTS')")
                Me.butTrfADJ.Attributes.Add("OnClick", "return fnCheckSelect('ADJ','cdCheckADJS')")
                Me.butTrfAddExp.Attributes.Add("OnClick", "return fnCheckSelect('ADDExp','cdCheckAddExpS')")
                Me.butTrfInAddExp.Attributes.Add("OnClick", "return fnCheckSelect('TRF-IN1Exp','cdCheckTIAddExpS')")
                Me.butTrfInUpdExp.Attributes.Add("OnClick", "return fnCheckSelect('TRF-IN2Exp','cdCheckTIUpdExpS')")

                ''default
                hdnImpFrmDt.Value = ""
                hdnImpToDt.Value = ""

                ''Display Record
                fnPopulateRecords()

                ''Get Cost Center
                subDisplayCostCenter()

                ''Check access right
                If (InStr(Session("AR"), "NFSTrf|TrfA|upd") = 0) Then
                    butTrfAdd.Enabled = False
                    dgTrfAdd.Enabled = False

                    butTrfInAdd.Enabled = False
                    dgTrfInAdd.Enabled = False

                    butTrfInUpd.Enabled = False
                    dgTrfInUpd.Enabled = False

                    butTrfRCT.Enabled = False
                    dgRCTUpd.Enabled = False

                    butTrfADJ.Enabled = False
                    dgADJUpd.Enabled = False

                    butTrfAddExp.Enabled = False
                    dgTrfAddExp.Enabled = False

                    butTrfInAddExp.Enabled = False
                    dgTrfInAddExp.Enabled = False

                    butTrfInUpdExp.Enabled = False
                    dgTrfInUpdExp.Enabled = False
                End If
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        txtImpFrmDt.Text = ""
        txtImpToDt.Text = ""
    End Sub

    Protected Sub butSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
        Try
            hdnImpFrmDt.Value = txtImpFrmDt.Text
            hdnImpToDt.Value = txtImpToDt.Text

            ''Display Record
            fnPopulateRecords()

            ''Get Cost Center
            subDisplayCostCenter()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub subDisplayCostCenter()
        Try
            Dim strCostCenter As String = clsNFSTrf.fnNFSTrfAct_GetCostCenter()

            lblCCtrADD.Text = "<font color='red'>Note:</font> Only for Cost Center is " & """" & strCostCenter & """" & "."

            lblCCtrTrfINAdd.Text = "<font color='red'>Note:</font> Only for Cost Center is " & """" & strCostCenter & """" & ". To add NFS record(s) in AMS, because of old NFS ID and new NFS ID not exist in AMS."

            lblCCtrTrfINUpd.Text = "<font color='red'>Note:</font> Only for Cost Center is " & """" & strCostCenter & """" & ". To update NFS record in AMS with new NFS record, because of old NFS ID exist in AMS."

            lblCCtrADDExp.Text = "<font color='red'>Note:</font> Only for Cost Center which is not " & """" & strCostCenter & """" & "."

            lblCCtrTrfINAddExp.Text = "<font color='red'>Note:</font> Only for Cost Center which is not " & """" & strCostCenter & """" & ". To add NFS record(s) in AMS, because of old NFS ID and new NFS ID not exist in AMS."

            lblCCtrTrfINUpdExp.Text = "<font color='red'>Note:</font> Only for Cost Center which is not " & """" & strCostCenter & """" & ". To update NFS record in AMS with new NFS record, because of old NFS ID exist in AMS."
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            Dim ds As New DataSet
            ds = clsNFSTrf.fnNFSTrfAct_GetRecords(hdnImpFrmDt.Value, hdnImpToDt.Value, "")

            ''Get "ADD" transaction
            dgTrfAdd.DataSource = ds.Tables(0)
            dgTrfAdd.DataBind()
            If Not dgTrfAdd.Items.Count > 0 Then ''Not Records found
                butTrfAdd.Enabled = False
            Else
                butTrfAdd.Enabled = True
            End If

            ''Get "TRF-IN" transaction for ADD
            dgTrfInAdd.DataSource = ds.Tables(1)
            dgTrfInAdd.DataBind()
            If Not dgTrfInAdd.Items.Count > 0 Then ''Not Records found
                butTrfInAdd.Enabled = False
            Else
                butTrfInAdd.Enabled = True
            End If

            ''Get "TRF-IN" transaction for UPDATE
            dgTrfInUpd.DataSource = ds.Tables(2)
            dgTrfInUpd.DataBind()
            If Not dgTrfInUpd.Items.Count > 0 Then ''Not Records found
                butTrfInUpd.Enabled = False
            Else
                butTrfInUpd.Enabled = True
            End If

            ''Get "RCT" transaction for UPDATE
            dgRCTUpd.DataSource = ds.Tables(6)
            dgRCTUpd.DataBind()
            If Not dgRCTUpd.Items.Count > 0 Then ''Not Records found
                butTrfRCT.Enabled = False
            Else
                butTrfRCT.Enabled = True
            End If

            ''Get "ADJ" transaction for UPDATE
            dgADJUpd.DataSource = ds.Tables(7)
            dgADJUpd.DataBind()
            If Not dgADJUpd.Items.Count > 0 Then ''Not Records found
                butTrfADJ.Enabled = False
            Else
                butTrfADJ.Enabled = True
            End If

            ''Get Exception Records transaction
            dgAllExp.DataSource = ds.Tables(8)
            dgAllExp.DataBind()

            ''Get "ADD" transaction for Exception Records
            dgTrfAddExp.DataSource = ds.Tables(3)
            dgTrfAddExp.DataBind()
            If Not dgTrfAddExp.Items.Count > 0 Then ''Not Records found
                butTrfAddExp.Enabled = False
            Else
                butTrfAddExp.Enabled = True
            End If

            ''Get "TRF-IN" transaction for ADD Exception Records
            dgTrfInAddExp.DataSource = ds.Tables(4)
            dgTrfInAddExp.DataBind()
            If Not dgTrfInAddExp.Items.Count > 0 Then ''Not Records found
                butTrfInAddExp.Enabled = False
            Else
                butTrfInAddExp.Enabled = True
            End If

            ''Get "TRF-IN" transaction for UPDATE for Cost Center which is not 345102
            dgTrfInUpdExp.DataSource = ds.Tables(5)
            dgTrfInUpdExp.DataBind()
            If Not dgTrfInUpdExp.Items.Count > 0 Then ''Not Records found
                butTrfInUpdExp.Enabled = False
            Else
                butTrfInUpdExp.Enabled = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub dgTrfAdd_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgTrfAdd.ItemDataBound
        Try
            Dim txtQty As TextBox
            Dim ddlCatSub As DropDownList
            Dim ddlDept As DropDownList
            Dim ddlLoc As DropDownList
            Dim ddlLocSub As DropDownList
            Dim ddlOwner As DropDownList

            txtQty = CType(e.Item.FindControl("txtQtyADD"), TextBox)
            If Not txtQty Is Nothing Then
                Dim strQty As String = e.Item.Cells(3).Text
                txtQty.Text = strQty
            End If

            ddlCatSub = CType(e.Item.FindControl("ddlCatSubADD"), DropDownList)
            If Not ddlCatSub Is Nothing Then
                Dim strCatID As String = e.Item.Cells(1).Text
                If strCatID <> "0" Then
                    fnPopulateDropDownList(clsCategory.fnCategoryGetAllRecDR(strCatID, 0, "fld_CatSubName", "ASC"), ddlCatSub, "fld_CatSubID", "fld_CatSubName", False)
                Else
                    ddlCatSub.Items.Clear()
                    ddlCatSub.Items.Insert(0, New ListItem("", "-1"))
                End If
            End If

            ddlDept = CType(e.Item.FindControl("ddlDeptADD"), DropDownList)
            If Not ddlDept Is Nothing Then
                fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDept, "fld_DepartmentID", "fld_DepartmentCode", False)
                If Session("AdminF") = "N" Then ddlDept.Items.Remove(ddlDept.Items.FindByValue("-1"))
            End If

            ddlLoc = CType(e.Item.FindControl("ddlLocADD"), DropDownList)
            If Not ddlLoc Is Nothing Then
                fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLoc, "fld_LocationID", "fld_LocationCode", False)
            End If

            ddlLocSub = CType(e.Item.FindControl("ddlLocSubADD"), DropDownList)
            If Not ddlLocSub Is Nothing Then
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If

            ddlOwner = CType(e.Item.FindControl("ddlOwnerADD"), DropDownList)
            If Not ddlOwner Is Nothing Then
                fnPopulateDropDownList(ClsUser.fnUsrGetUsrFromADDR("OU", "9999", "", ""), ddlOwner, "fld_ADLoginID", "fld_ADUsrName", False)
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub ddlLocADD_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim ddllist As DropDownList = CType(sender, DropDownList)
            Dim cell As TableCell = CType(ddllist.Parent, TableCell)
            Dim item As DataGridItem = CType(cell.Parent, DataGridItem)
            Dim ddlLoc As DropDownList = CType(item.Cells(7).FindControl("ddlLocADD"), DropDownList)
            Dim ddlLocSub As DropDownList = CType(item.Cells(7).FindControl("ddlLocSubADD"), DropDownList)

            'Dim strddlID As String = sender.clientid.ToString
            'Dim strddlNo As String = strddlID.Substring(Len(strddlID) - 3, 3)
            'Dim ddlLocation As DropDownList = CType(item.Cells(7).FindControl("ddlLocADD" & strddlNo), DropDownList)
            'Dim ddlLocSub As DropDownList = CType(item.Cells(7).FindControl("ddlLocSubADD" & strddlNo), DropDownList)

            If ddlLoc.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLoc.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCode", False)
            Else
                ddlLocSub.Items.Clear()
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Function fnShowAddInfoDetail(ByVal strPurDt As String, ByVal strCatCode As String, ByVal strCostCtr As String, _
                                 ByVal strQty As String, ByVal strCost As String, ByVal strDesc As String) As String
        Try
            Dim strRetVal As String = ""

            strRetVal = "<TABLE border=0 cellPadding=1 cellSpacing=1 width=100%>" & _
                        "<TBODY align=left>" & _
                        "<TR><TD width=35%><b>Quantity :</b></TD><TD width=65%>" & IIf(strQty = "", " - ", strQty) & "</TD></TR>" & _
                        "<TR><TD><b>Category :</b></TD><TD>" & IIf(strCatCode = "", " - ", strCatCode) & "</TD></TR>" & _
                        "<TR><TD><b>Cost (S$) :</b></TD><TD>" & IIf(strCost = "", " - ", strCost) & "</TD></TR>" & _
                        "<TR><TD><b>Cost Center :</b></TD><TD>" & IIf(strCostCtr = "", " - ", strCostCtr) & "</TD></TR>" & _
                        "<TR><TD><b>Purchase Date :</b></TD><TD>" & IIf(strPurDt = "", " - ", strPurDt) & "</TD></TR>" & _
                        "<TR><TD valign=top><b>Description :</b></TD><TD>" & IIf(strDesc = "", " - ", strDesc) & "</TD></TR>" & _
                        "</TBODY>" & _
                        "</TABLE>"

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Protected Sub butTrfAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butTrfAdd.Click
        Try
            Dim strErrorF As String = "N"
            Dim strMsg As String = ""
            Dim strXMLrec As String = ""

            ''validate records
            fnValSelectRecordADD(strErrorF, strMsg)
            If strErrorF = "Y" Then '' error found...
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
                Exit Sub
            End If

            ''Get Records selected in XML
            strXMLrec = fnGetXMLRecADD()

            ''Update record
            If strXMLrec <> "" Then
                Dim intRetVal As Integer
                intRetVal = clsNFSTrf.fnNFSTrfAct_InsertADDRecords(strXMLrec, Session("UsrID"))
                If intRetVal > 0 Then
                    ''Get Records
                    fnPopulateRecords()

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + "Record(s) add succesfully." + "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    lblErrorMessage.Text = "Record(s) add failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                End If
            Else
                lblErrorMessage.Text = "Please select at least one record to add."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnValSelectRecordADD(ByRef strErrorF As String, ByRef strErrorMsg As String)
        Try
            strErrorF = "N"
            strErrorMsg = ""

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox

            Dim txtQty As TextBox
            Dim ddlCatSubID As DropDownList
            Dim ddlDeptID As DropDownList
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim intRow As Integer = 0

            For Each GridItem In dgTrfAdd.Items
                intRow = intRow + 1

                chkSelectedRec = CType(GridItem.Cells(8).FindControl("cdCheckAddS"), CheckBox)
                If chkSelectedRec.Checked Then
                    txtQty = CType(GridItem.Cells(7).FindControl("txtQtyADD"), TextBox)
                    If Trim(txtQty.Text) = "" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please enter Quantity (Record " & intRow & ")."
                        Exit For
                    Else
                        If Not IsNumeric(Trim(txtQty.Text)) Then
                            strErrorF = "Y"
                            strErrorMsg = "Quantity just allow number (Record " & intRow & ")."
                            Exit For
                        ElseIf Trim(txtQty.Text) = "0" Then
                            strErrorF = "Y"
                            strErrorMsg = "Quantity must more than '0' (Record " & intRow & ")."
                            Exit For
                        End If
                    End If

                    ddlCatSubID = CType(GridItem.Cells(7).FindControl("ddlCatSubADD"), DropDownList)
                    If ddlCatSubID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Sub Category (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlDeptID = CType(GridItem.Cells(7).FindControl("ddlDeptADD"), DropDownList)
                    If ddlDeptID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Department (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlLocID = CType(GridItem.Cells(7).FindControl("ddlLocADD"), DropDownList)
                    If ddlLocID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Location (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlLocSubID = CType(GridItem.Cells(7).FindControl("ddlLocSubADD"), DropDownList)
                    If ddlLocSubID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Sub Location (Record " & intRow & ")."
                        Exit For
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnGetXMLRecADD() As String
        Try
            Dim TblNFSAdd As DataTable = fnCreateDataTableADD()
            Dim RowNFSAdd As DataRow

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox

            Dim txtQty As TextBox
            Dim ddlCatSubID As DropDownList
            Dim ddlDeptID As DropDownList
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim ddlAOwnerID As DropDownList
            Dim txtRemarks As TextBox

            Dim sXMLString As String = ""

            For Each GridItem In dgTrfAdd.Items
                chkSelectedRec = CType(GridItem.Cells(8).FindControl("cdCheckAddS"), CheckBox)
                If chkSelectedRec.Checked Then
                    txtQty = CType(GridItem.Cells(7).FindControl("txtQtyADD"), TextBox)
                    ddlCatSubID = CType(GridItem.Cells(7).FindControl("ddlCatSubADD"), DropDownList)
                    ddlDeptID = CType(GridItem.Cells(7).FindControl("ddlDeptADD"), DropDownList)
                    ddlLocID = CType(GridItem.Cells(7).FindControl("ddlLocADD"), DropDownList)
                    ddlLocSubID = CType(GridItem.Cells(7).FindControl("ddlLocSubADD"), DropDownList)
                    ddlAOwnerID = CType(GridItem.Cells(7).FindControl("ddlOwnerADD"), DropDownList)
                    txtRemarks = CType(GridItem.Cells(7).FindControl("txtRemarkADD"), TextBox)

                    ''Add New Row to Datatable
                    RowNFSAdd = TblNFSAdd.NewRow()        'declaring a new row
                    RowNFSAdd.Item("dt_CDetID") = GridItem.Cells(0).Text
                    RowNFSAdd.Item("dt_Qty") = Trim(txtQty.Text)
                    RowNFSAdd.Item("dt_CatSub") = IIf(ddlCatSubID.SelectedValue = "-1", "0", ddlCatSubID.SelectedValue)
                    RowNFSAdd.Item("dt_Dept") = IIf(ddlDeptID.SelectedValue = "-1", "0", ddlDeptID.SelectedValue)
                    RowNFSAdd.Item("dt_Loc") = IIf(ddlLocID.SelectedValue = "-1", "0", ddlLocID.SelectedValue)
                    RowNFSAdd.Item("dt_LocSub") = IIf(ddlLocSubID.SelectedValue = "-1", "0", ddlLocSubID.SelectedValue)
                    RowNFSAdd.Item("dt_AssOwner") = IIf(ddlAOwnerID.SelectedValue = "-1", "", ddlAOwnerID.SelectedValue)
                    RowNFSAdd.Item("dt_Remark") = Trim(txtRemarks.Text)
                    TblNFSAdd.Rows.Add(RowNFSAdd)
                End If
            Next

            If TblNFSAdd.Rows.Count > 0 Then
                ''Get XML
                Dim ds As New DataSet
                ds = New DataSet            'creating a dataset
                ds.Tables.Add(TblNFSAdd)    'assign datatable to dataset
                If Not ds Is Nothing Then
                    Dim loCol As DataColumn
                    'Prepare XML output from the DataSet as a string
                    For Each loCol In ds.Tables(0).Columns
                        loCol.ColumnMapping = System.Data.MappingType.Attribute
                    Next
                    sXMLString = ds.GetXml
                End If
            End If

            fnGetXMLRecADD = Trim(sXMLString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnCreateDataTableADD() As DataTable
        Try
            ''creating a table 
            Dim TblNFSAdd As DataTable
            TblNFSAdd = New DataTable("TblNFSAdd")

            ''Column 1: Cron Details ID
            Dim dt_CDetID As DataColumn = New DataColumn("dt_CDetID")    'declaring a column named Name
            dt_CDetID.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            TblNFSAdd.Columns.Add(dt_CDetID)                               'adding the column to table

            ''Column 2: Quantity 
            Dim dt_Qty As DataColumn = New DataColumn("dt_Qty")      'declaring a column named Name
            dt_Qty.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblNFSAdd.Columns.Add(dt_Qty)

            ''Column 2: Subcategory 
            Dim dt_CatSub As DataColumn = New DataColumn("dt_CatSub")      'declaring a column named Name
            dt_CatSub.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblNFSAdd.Columns.Add(dt_CatSub)                                    'adding the column to table

            ''Column 3: Department
            Dim dt_Dept As DataColumn = New DataColumn("dt_Dept")
            dt_Dept.DataType = System.Type.GetType("System.String")
            TblNFSAdd.Columns.Add(dt_Dept)

            ''Column 4: Location
            Dim dt_Loc As DataColumn = New DataColumn("dt_Loc")      'declaring a column named Name
            dt_Loc.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblNFSAdd.Columns.Add(dt_Loc)                                    'adding the column to table

            ''Column 5: Sub Location
            Dim dt_LocSub As DataColumn = New DataColumn("dt_LocSub")
            dt_LocSub.DataType = System.Type.GetType("System.String")
            TblNFSAdd.Columns.Add(dt_LocSub)

            ''Column 6: Assigned Owner
            Dim dt_AssOwner As DataColumn = New DataColumn("dt_AssOwner")      'declaring a column named Name
            dt_AssOwner.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblNFSAdd.Columns.Add(dt_AssOwner)                                    'adding the column to table

            ''Column 7: Remark
            Dim dt_Remark As DataColumn = New DataColumn("dt_Remark")
            dt_Remark.DataType = System.Type.GetType("System.String")
            TblNFSAdd.Columns.Add(dt_Remark)

            ''Set Primary Keys
            '--> Make the ID column the primary key column.
            Dim PrimaryKeyColumns(0) As DataColumn
            PrimaryKeyColumns(0) = TblNFSAdd.Columns("dt_CDetID")
            TblNFSAdd.PrimaryKey = PrimaryKeyColumns

            fnCreateDataTableADD = TblNFSAdd
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub dgTrfInAdd_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgTrfInAdd.ItemDataBound
        Try
            Dim txtQty As TextBox
            Dim ddlCatSub As DropDownList
            Dim ddlDept As DropDownList
            Dim ddlLoc As DropDownList
            Dim ddlLocSub As DropDownList
            Dim ddlOwner As DropDownList

            txtQty = CType(e.Item.FindControl("txtQtyTIADD"), TextBox)
            If Not txtQty Is Nothing Then
                Dim strQty As String = e.Item.Cells(3).Text
                txtQty.Text = strQty
            End If

            ddlCatSub = CType(e.Item.FindControl("ddlCatSubTIADD"), DropDownList)
            If Not ddlCatSub Is Nothing Then
                Dim strCatID As String = e.Item.Cells(1).Text
                If strCatID <> "0" Then
                    fnPopulateDropDownList(clsCategory.fnCategoryGetAllRecDR(strCatID, 0, "fld_CatSubName", "ASC"), ddlCatSub, "fld_CatSubID", "fld_CatSubName", False)
                Else
                    ddlCatSub.Items.Clear()
                    ddlCatSub.Items.Insert(0, New ListItem("", "-1"))
                End If
            End If

            ddlDept = CType(e.Item.FindControl("ddlDeptTIADD"), DropDownList)
            If Not ddlDept Is Nothing Then
                fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDept, "fld_DepartmentID", "fld_DepartmentCode", False)
                If Session("AdminF") = "N" Then ddlDept.Items.Remove(ddlDept.Items.FindByValue("-1"))
            End If

            ddlLoc = CType(e.Item.FindControl("ddlLocTIADD"), DropDownList)
            If Not ddlLoc Is Nothing Then
                fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLoc, "fld_LocationID", "fld_LocationCode", False)
            End If

            ddlLocSub = CType(e.Item.FindControl("ddlLocSubTIADD"), DropDownList)
            If Not ddlLocSub Is Nothing Then
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If

            ddlOwner = CType(e.Item.FindControl("ddlOwnerTIADD"), DropDownList)
            If Not ddlOwner Is Nothing Then
                fnPopulateDropDownList(ClsUser.fnUsrGetUsrFromADDR("OU", "9999", "", ""), ddlOwner, "fld_ADLoginID", "fld_ADUsrName", False)
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub ddlLocTrfInAdd_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim ddllist As DropDownList = CType(sender, DropDownList)
            Dim cell As TableCell = CType(ddllist.Parent, TableCell)
            Dim item As DataGridItem = CType(cell.Parent, DataGridItem)
            Dim ddlLoc As DropDownList = CType(item.Cells(7).FindControl("ddlLocTIADD"), DropDownList)
            Dim ddlLocSub As DropDownList = CType(item.Cells(7).FindControl("ddlLocSubTIADD"), DropDownList)

            'Dim strddlID As String = sender.clientid.ToString
            'Dim strddlNo As String = strddlID.Substring(Len(strddlID) - 3, 3)
            'Dim ddlLocation As DropDownList = CType(item.Cells(7).FindControl("ddlLocADD" & strddlNo), DropDownList)
            'Dim ddlLocSub As DropDownList = CType(item.Cells(7).FindControl("ddlLocSubADD" & strddlNo), DropDownList)

            If ddlLoc.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLoc.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCode", False)
            Else
                ddlLocSub.Items.Clear()
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butTrfInAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butTrfInAdd.Click
        Try
            Dim strErrorF As String = "N"
            Dim strMsg As String = ""
            Dim strXMLrec As String = ""

            ''validate records
            fnValSelectRecordTrfInADD(strErrorF, strMsg)
            If strErrorF = "Y" Then '' error found...
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
                Exit Sub
            End If

            ''Get Records selected in XML
            strXMLrec = fnGetXMLRecTrfInADD()

            ''Update record
            If strXMLrec <> "" Then
                Dim intRetVal As Integer
                intRetVal = clsNFSTrf.fnNFSTrfAct_InsertTrfINRecords(strXMLrec, Session("UsrID"))
                If intRetVal > 0 Then
                    ''Get Records
                    fnPopulateRecords()

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + "Record(s) add succesfully." + "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    lblErrorMessage.Text = "Record(s) add failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                End If
            Else
                lblErrorMessage.Text = "Please select at least one record to add."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnValSelectRecordTrfInADD(ByRef strErrorF As String, ByRef strErrorMsg As String)
        Try
            strErrorF = "N"
            strErrorMsg = ""

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox

            Dim txtQty As TextBox
            Dim ddlCatSubID As DropDownList
            Dim ddlDeptID As DropDownList
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim intRow As Integer = 0

            For Each GridItem In dgTrfInAdd.Items
                intRow = intRow + 1

                chkSelectedRec = CType(GridItem.Cells(8).FindControl("cdCheckTIAddS"), CheckBox)
                If chkSelectedRec.Checked Then
                    txtQty = CType(GridItem.Cells(7).FindControl("txtQtyTIADD"), TextBox)
                    If Trim(txtQty.Text) = "" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please enter Quantity (Record " & intRow & ")."
                        Exit For
                    Else
                        If Not IsNumeric(Trim(txtQty.Text)) Then
                            strErrorF = "Y"
                            strErrorMsg = "Quantity just allow number (Record " & intRow & ")."
                            Exit For
                        ElseIf Trim(txtQty.Text) = "0" Then
                            strErrorF = "Y"
                            strErrorMsg = "Quantity must more than '0' (Record " & intRow & ")."
                            Exit For
                        End If
                    End If

                    ddlCatSubID = CType(GridItem.Cells(7).FindControl("ddlCatSubTIADD"), DropDownList)
                    If ddlCatSubID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Sub Category (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlDeptID = CType(GridItem.Cells(7).FindControl("ddlDeptTIADD"), DropDownList)
                    If ddlDeptID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Department (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlLocID = CType(GridItem.Cells(7).FindControl("ddlLocTIADD"), DropDownList)
                    If ddlLocID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Location (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlLocSubID = CType(GridItem.Cells(7).FindControl("ddlLocSubTIADD"), DropDownList)
                    If ddlLocSubID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Sub Location (Record " & intRow & ")."
                        Exit For
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnGetXMLRecTrfInADD() As String
        Try
            Dim TblNFSAdd As DataTable = fnCreateDataTableADD()
            Dim RowNFSAdd As DataRow

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox

            Dim txtQty As TextBox
            Dim ddlCatSubID As DropDownList
            Dim ddlDeptID As DropDownList
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim ddlAOwnerID As DropDownList
            Dim txtRemarks As TextBox

            Dim sXMLString As String = ""

            For Each GridItem In dgTrfInAdd.Items
                chkSelectedRec = CType(GridItem.Cells(8).FindControl("cdCheckTIAddS"), CheckBox)
                If chkSelectedRec.Checked Then
                    txtQty = CType(GridItem.Cells(7).FindControl("txtQtyTIADD"), TextBox)
                    ddlCatSubID = CType(GridItem.Cells(7).FindControl("ddlCatSubTIADD"), DropDownList)
                    ddlDeptID = CType(GridItem.Cells(7).FindControl("ddlDeptTIADD"), DropDownList)
                    ddlLocID = CType(GridItem.Cells(7).FindControl("ddlLocTIADD"), DropDownList)
                    ddlLocSubID = CType(GridItem.Cells(7).FindControl("ddlLocSubTIADD"), DropDownList)
                    ddlAOwnerID = CType(GridItem.Cells(7).FindControl("ddlOwnerTIADD"), DropDownList)
                    txtRemarks = CType(GridItem.Cells(7).FindControl("txtRemarkTIADD"), TextBox)

                    ''Add New Row to Datatable
                    RowNFSAdd = TblNFSAdd.NewRow()        'declaring a new row
                    RowNFSAdd.Item("dt_CDetID") = GridItem.Cells(0).Text
                    RowNFSAdd.Item("dt_Qty") = Trim(txtQty.Text)
                    RowNFSAdd.Item("dt_CatSub") = IIf(ddlCatSubID.SelectedValue = "-1", "0", ddlCatSubID.SelectedValue)
                    RowNFSAdd.Item("dt_Dept") = IIf(ddlDeptID.SelectedValue = "-1", "0", ddlDeptID.SelectedValue)
                    RowNFSAdd.Item("dt_Loc") = IIf(ddlLocID.SelectedValue = "-1", "0", ddlLocID.SelectedValue)
                    RowNFSAdd.Item("dt_LocSub") = IIf(ddlLocSubID.SelectedValue = "-1", "0", ddlLocSubID.SelectedValue)
                    RowNFSAdd.Item("dt_AssOwner") = IIf(ddlAOwnerID.SelectedValue = "-1", "", ddlAOwnerID.SelectedValue)
                    RowNFSAdd.Item("dt_Remark") = Trim(txtRemarks.Text)
                    TblNFSAdd.Rows.Add(RowNFSAdd)
                End If
            Next

            If TblNFSAdd.Rows.Count > 0 Then
                ''Get XML
                Dim ds As New DataSet
                ds = New DataSet            'creating a dataset
                ds.Tables.Add(TblNFSAdd)    'assign datatable to dataset
                If Not ds Is Nothing Then
                    Dim loCol As DataColumn
                    'Prepare XML output from the DataSet as a string
                    For Each loCol In ds.Tables(0).Columns
                        loCol.ColumnMapping = System.Data.MappingType.Attribute
                    Next
                    sXMLString = ds.GetXml
                End If
            End If

            fnGetXMLRecTrfInADD = Trim(sXMLString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function fnShowTrfInUpdInfoDetail(ByVal strPurDt As String, ByVal strCatCode As String, ByVal strCostCtr As String, _
                                      ByVal strNFSIDOld As String, ByVal strCost As String, ByVal strDesc As String) As String
        Try
            Dim strRetVal As String = ""

            strRetVal = "<TABLE border=0 cellPadding=1 cellSpacing=1 width=100%>" & _
                        "<TBODY align=left>" & _
                        "<TR><TD width=35%><b>Old NFS ID :</b></TD><TD width=65%>" & IIf(strNFSIDOld = "", " - ", strNFSIDOld) & "</TD></TR>" & _
                        "<TR><TD><b>Category :</b></TD><TD>" & IIf(strCatCode = "", " - ", strCatCode) & "</TD></TR>" & _
                        "<TR><TD><b>Cost (S$) :</b></TD><TD>" & IIf(strCost = "", " - ", strCost) & "</TD></TR>" & _
                        "<TR><TD><b>Cost Center :</b></TD><TD>" & IIf(strCostCtr = "", " - ", strCostCtr) & "</TD></TR>" & _
                        "<TR><TD><b>Purchase Date :</b></TD><TD>" & IIf(strPurDt = "", " - ", strPurDt) & "</TD></TR>" & _
                        "<TR><TD valign=top><b>Description :</b></TD><TD>" & IIf(strDesc = "", " - ", strDesc) & "</TD></TR>" & _
                        "</TBODY>" & _
                        "</TABLE>"

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub dgTrfInUpd_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgTrfInUpd.ItemDataBound
        Try
            Dim ddlCatSub As DropDownList
            Dim ddlDept As DropDownList
            Dim ddlLoc As DropDownList
            Dim ddlLocSub As DropDownList
            Dim ddlOwner As DropDownList

            ddlCatSub = CType(e.Item.FindControl("ddlCatSubTIUpd"), DropDownList)
            If Not ddlCatSub Is Nothing Then
                Dim strCatID As String = e.Item.Cells(1).Text
                If strCatID <> "0" Then
                    fnPopulateDropDownList(clsCategory.fnCategoryGetAllRecDR(strCatID, 0, "fld_CatSubName", "ASC"), ddlCatSub, "fld_CatSubID", "fld_CatSubName", False)
                Else
                    ddlCatSub.Items.Clear()
                    ddlCatSub.Items.Insert(0, New ListItem("", "-1"))
                End If
            End If

            ddlDept = CType(e.Item.FindControl("ddlDeptTIUpd"), DropDownList)
            If Not ddlDept Is Nothing Then
                fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDept, "fld_DepartmentID", "fld_DepartmentCode", False)
                If Session("AdminF") = "N" Then ddlDept.Items.Remove(ddlDept.Items.FindByValue("-1"))
            End If

            ddlLoc = CType(e.Item.FindControl("ddlLocTIUpd"), DropDownList)
            If Not ddlLoc Is Nothing Then
                fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLoc, "fld_LocationID", "fld_LocationCode", False)
            End If

            ddlLocSub = CType(e.Item.FindControl("ddlLocSubTIUpd"), DropDownList)
            If Not ddlLocSub Is Nothing Then
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If

            ddlOwner = CType(e.Item.FindControl("ddlOwnerTIUpd"), DropDownList)
            If Not ddlOwner Is Nothing Then
                fnPopulateDropDownList(ClsUser.fnUsrGetUsrFromADDR("OU", "9999", "", ""), ddlOwner, "fld_ADLoginID", "fld_ADUsrName", False)
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub ddlLocTrfInUpd_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim ddllist As DropDownList = CType(sender, DropDownList)
            Dim cell As TableCell = CType(ddllist.Parent, TableCell)
            Dim item As DataGridItem = CType(cell.Parent, DataGridItem)
            Dim ddlLoc As DropDownList = CType(item.Cells(8).FindControl("ddlLocTIUpd"), DropDownList)
            Dim ddlLocSub As DropDownList = CType(item.Cells(8).FindControl("ddlLocSubTIUpd"), DropDownList)

            'Dim strddlID As String = sender.clientid.ToString
            'Dim strddlNo As String = strddlID.Substring(Len(strddlID) - 3, 3)
            'Dim ddlLocation As DropDownList = CType(item.Cells(7).FindControl("ddlLocADD" & strddlNo), DropDownList)
            'Dim ddlLocSub As DropDownList = CType(item.Cells(7).FindControl("ddlLocSubADD" & strddlNo), DropDownList)

            If ddlLoc.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLoc.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCode", False)
            Else
                ddlLocSub.Items.Clear()
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butTrfInUpd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butTrfInUpd.Click
        Try
            Dim strErrorF As String = "N"
            Dim strMsg As String = ""
            Dim strXMLrec As String = ""

            ''validate records
            fnValSelectRecordTrfInUpd(strErrorF, strMsg)
            If strErrorF = "Y" Then '' error found...
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
                Exit Sub
            End If

            ''Get Records selected in XML
            strXMLrec = fnGetXMLRecTrfInUpd()

            ''Update record
            If strXMLrec <> "" Then
                Dim intRetVal As Integer
                intRetVal = clsNFSTrf.fnNFSTrfAct_UpdateTrfINRecords(strXMLrec, Session("UsrID"))
                If intRetVal > 0 Then
                    ''Get Records
                    fnPopulateRecords()

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + "Record(s) update succesfully." + "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    lblErrorMessage.Text = "Record(s) update failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                End If
            Else
                lblErrorMessage.Text = "Please select at least one record to update."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnValSelectRecordTrfInUpd(ByRef strErrorF As String, ByRef strErrorMsg As String)
        Try
            strErrorF = "N"
            strErrorMsg = ""

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox

            Dim ddlCatSubID As DropDownList
            Dim ddlDeptID As DropDownList
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim intRow As Integer = 0

            For Each GridItem In dgTrfInUpd.Items
                intRow = intRow + 1

                chkSelectedRec = CType(GridItem.Cells(9).FindControl("cdCheckTIUpdS"), CheckBox)
                If chkSelectedRec.Checked Then

                    ddlCatSubID = CType(GridItem.Cells(8).FindControl("ddlCatSubTIUpd"), DropDownList)
                    If ddlCatSubID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Sub Category (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlDeptID = CType(GridItem.Cells(8).FindControl("ddlDeptTIUpd"), DropDownList)
                    If ddlDeptID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Department (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlLocID = CType(GridItem.Cells(8).FindControl("ddlLocTIUpd"), DropDownList)
                    If ddlLocID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Location (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlLocSubID = CType(GridItem.Cells(8).FindControl("ddlLocSubTIUpd"), DropDownList)
                    If ddlLocSubID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Sub Location (Record " & intRow & ")."
                        Exit For
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnGetXMLRecTrfInUpd() As String
        Try
            Dim TblNFSAdd As DataTable = fnCreateDataTableADD()
            Dim RowNFSAdd As DataRow

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox

            Dim ddlCatSubID As DropDownList
            Dim ddlDeptID As DropDownList
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim ddlAOwnerID As DropDownList
            Dim txtRemarks As TextBox

            Dim sXMLString As String = ""

            For Each GridItem In dgTrfInUpd.Items
                chkSelectedRec = CType(GridItem.Cells(9).FindControl("cdCheckTIUpdS"), CheckBox)
                If chkSelectedRec.Checked Then
                    ddlCatSubID = CType(GridItem.Cells(8).FindControl("ddlCatSubTIUpd"), DropDownList)
                    ddlDeptID = CType(GridItem.Cells(8).FindControl("ddlDeptTIUpd"), DropDownList)
                    ddlLocID = CType(GridItem.Cells(8).FindControl("ddlLocTIUpd"), DropDownList)
                    ddlLocSubID = CType(GridItem.Cells(8).FindControl("ddlLocSubTIUpd"), DropDownList)
                    ddlAOwnerID = CType(GridItem.Cells(8).FindControl("ddlOwnerTIUpd"), DropDownList)
                    txtRemarks = CType(GridItem.Cells(8).FindControl("txtRemarkTIUpd"), TextBox)

                    ''Add New Row to Datatable
                    RowNFSAdd = TblNFSAdd.NewRow()        'declaring a new row
                    RowNFSAdd.Item("dt_CDetID") = GridItem.Cells(0).Text
                    RowNFSAdd.Item("dt_Qty") = "0"
                    RowNFSAdd.Item("dt_CatSub") = IIf(ddlCatSubID.SelectedValue = "-1", "0", ddlCatSubID.SelectedValue)
                    RowNFSAdd.Item("dt_Dept") = IIf(ddlDeptID.SelectedValue = "-1", "0", ddlDeptID.SelectedValue)
                    RowNFSAdd.Item("dt_Loc") = IIf(ddlLocID.SelectedValue = "-1", "0", ddlLocID.SelectedValue)
                    RowNFSAdd.Item("dt_LocSub") = IIf(ddlLocSubID.SelectedValue = "-1", "0", ddlLocSubID.SelectedValue)
                    RowNFSAdd.Item("dt_AssOwner") = IIf(ddlAOwnerID.SelectedValue = "-1", "", ddlAOwnerID.SelectedValue)
                    RowNFSAdd.Item("dt_Remark") = Trim(txtRemarks.Text)
                    TblNFSAdd.Rows.Add(RowNFSAdd)
                End If
            Next

            If TblNFSAdd.Rows.Count > 0 Then
                ''Get XML
                Dim ds As New DataSet
                ds = New DataSet            'creating a dataset
                ds.Tables.Add(TblNFSAdd)    'assign datatable to dataset
                If Not ds Is Nothing Then
                    Dim loCol As DataColumn
                    'Prepare XML output from the DataSet as a string
                    For Each loCol In ds.Tables(0).Columns
                        loCol.ColumnMapping = System.Data.MappingType.Attribute
                    Next
                    sXMLString = ds.GetXml
                End If
            End If

            fnGetXMLRecTrfInUpd = Trim(sXMLString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub dgRCTUpd_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRCTUpd.ItemDataBound
        Try
            Dim ddlCatSub As DropDownList

            ddlCatSub = CType(e.Item.FindControl("ddlCatSubRCT"), DropDownList)
            If Not ddlCatSub Is Nothing Then
                Dim strCatID As String = e.Item.Cells(1).Text
                If strCatID <> "0" Then
                    fnPopulateDropDownList(clsCategory.fnCategoryGetAllRecDR(strCatID, 0, "fld_CatSubName", "ASC"), ddlCatSub, "fld_CatSubID", "fld_CatSubName", False)
                Else
                    ddlCatSub.Items.Clear()
                    ddlCatSub.Items.Insert(0, New ListItem("", "-1"))
                End If
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butTrfRCT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butTrfRCT.Click
        Try
            Dim strErrorF As String = "N"
            Dim strMsg As String = ""
            Dim strXMLrec As String = ""

            ''validate records
            fnValSelectRecordRCTUpd(strErrorF, strMsg)
            If strErrorF = "Y" Then '' error found...
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
                Exit Sub
            End If

            ''Get Records selected in XML
            strXMLrec = fnGetXMLRecRCTUpd()

            ''Update record
            If strXMLrec <> "" Then
                Dim intRetVal As Integer
                intRetVal = clsNFSTrf.fnNFSTrfAct_UpdateRCTRecords(strXMLrec, Session("UsrID"))
                If intRetVal > 0 Then
                    ''Get Records
                    fnPopulateRecords()

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + "Record(s) update succesfully." + "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    lblErrorMessage.Text = "Record(s) update failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                End If
            Else
                lblErrorMessage.Text = "Please select at least one record to update."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnValSelectRecordRCTUpd(ByRef strErrorF As String, ByRef strErrorMsg As String)
        Try
            strErrorF = "N"
            strErrorMsg = ""

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox

            Dim ddlCatSubID As DropDownList
            Dim intRow As Integer = 0

            For Each GridItem In dgRCTUpd.Items
                intRow = intRow + 1

                chkSelectedRec = CType(GridItem.Cells(8).FindControl("cdCheckRCTS"), CheckBox)
                If chkSelectedRec.Checked Then
                    ddlCatSubID = CType(GridItem.Cells(7).FindControl("ddlCatSubRCT"), DropDownList)
                    If ddlCatSubID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Sub Category (Record " & intRow & ")."
                        Exit For
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnGetXMLRecRCTUpd() As String
        Try
            Dim TblNFSAdd As DataTable = fnCreateDataTableADD()
            Dim RowNFSAdd As DataRow

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox
            Dim ddlCatSubID As DropDownList
            Dim sXMLString As String = ""

            For Each GridItem In dgRCTUpd.Items
                chkSelectedRec = CType(GridItem.Cells(8).FindControl("cdCheckRCTS"), CheckBox)
                If chkSelectedRec.Checked Then
                    ddlCatSubID = CType(GridItem.Cells(7).FindControl("ddlCatSubRCT"), DropDownList)

                    ''Add New Row to Datatable
                    RowNFSAdd = TblNFSAdd.NewRow()        'declaring a new row
                    RowNFSAdd.Item("dt_CDetID") = GridItem.Cells(0).Text
                    RowNFSAdd.Item("dt_Qty") = "0"
                    RowNFSAdd.Item("dt_CatSub") = IIf(ddlCatSubID.SelectedValue = "-1", "0", ddlCatSubID.SelectedValue)
                    RowNFSAdd.Item("dt_Dept") = "0"
                    RowNFSAdd.Item("dt_Loc") = "0"
                    RowNFSAdd.Item("dt_LocSub") = "0"
                    RowNFSAdd.Item("dt_AssOwner") = ""
                    RowNFSAdd.Item("dt_Remark") = ""
                    TblNFSAdd.Rows.Add(RowNFSAdd)
                End If
            Next

            If TblNFSAdd.Rows.Count > 0 Then
                ''Get XML
                Dim ds As New DataSet
                ds = New DataSet            'creating a dataset
                ds.Tables.Add(TblNFSAdd)    'assign datatable to dataset
                If Not ds Is Nothing Then
                    Dim loCol As DataColumn
                    'Prepare XML output from the DataSet as a string
                    For Each loCol In ds.Tables(0).Columns
                        loCol.ColumnMapping = System.Data.MappingType.Attribute
                    Next
                    sXMLString = ds.GetXml
                End If
            End If

            fnGetXMLRecRCTUpd = Trim(sXMLString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub butTrfADJ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butTrfADJ.Click
        Try
            Dim strXMLrec As String = ""

            ''Get Records selected in XML
            strXMLrec = fnGetXMLRecADJUpd()

            ''Update record
            If strXMLrec <> "" Then
                Dim intRetVal As Integer
                intRetVal = clsNFSTrf.fnNFSTrfAct_UpdateADJRecords(strXMLrec, Session("UsrID"))
                If intRetVal > 0 Then
                    ''Get Records
                    fnPopulateRecords()

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + "Record(s) update succesfully." + "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    lblErrorMessage.Text = "Record(s) update failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                End If
            Else
                lblErrorMessage.Text = "Please select at least one record to update."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnGetXMLRecADJUpd() As String
        Try
            Dim TblNFSAdd As DataTable = fnCreateDataTableADD()
            Dim RowNFSAdd As DataRow

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox
            Dim sXMLString As String = ""

            For Each GridItem In dgADJUpd.Items
                chkSelectedRec = CType(GridItem.Cells(7).FindControl("cdCheckADJS"), CheckBox)
                If chkSelectedRec.Checked Then
                    ''Add New Row to Datatable
                    RowNFSAdd = TblNFSAdd.NewRow()        'declaring a new row
                    RowNFSAdd.Item("dt_CDetID") = GridItem.Cells(0).Text
                    RowNFSAdd.Item("dt_Qty") = "0"
                    RowNFSAdd.Item("dt_CatSub") = "0"
                    RowNFSAdd.Item("dt_Dept") = "0"
                    RowNFSAdd.Item("dt_Loc") = "0"
                    RowNFSAdd.Item("dt_LocSub") = "0"
                    RowNFSAdd.Item("dt_AssOwner") = ""
                    RowNFSAdd.Item("dt_Remark") = ""
                    TblNFSAdd.Rows.Add(RowNFSAdd)
                End If
            Next

            If TblNFSAdd.Rows.Count > 0 Then
                ''Get XML
                Dim ds As New DataSet
                ds = New DataSet            'creating a dataset
                ds.Tables.Add(TblNFSAdd)    'assign datatable to dataset
                If Not ds Is Nothing Then
                    Dim loCol As DataColumn
                    'Prepare XML output from the DataSet as a string
                    For Each loCol In ds.Tables(0).Columns
                        loCol.ColumnMapping = System.Data.MappingType.Attribute
                    Next
                    sXMLString = ds.GetXml
                End If
            End If

            fnGetXMLRecADJUpd = Trim(sXMLString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function fnShowExistingAMSDetail(ByVal strNFSID As String, ByVal strTrfType As String) As String
        Try
            Dim strRetVal As String = ""
            Dim intRecCount As Integer = "0"

            Dim objRdr As SqlDataReader
            objRdr = clsNFSTrf.fnNFSTrfAct_GetAMSDetailsByNFSID(strNFSID)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        intRecCount = intRecCount + 1

                        If strRetVal <> "" Then strRetVal += "<br><br>"
                        If strTrfType = "RCT" Then
                            strRetVal += "<TABLE border=0 cellPadding=1 cellSpacing=1 width=100%>" & _
                                         "<TBODY align=left>" & _
                                         "<TR><TD width=35%><b>Asset ID (AMS) :</b></TD><TD width=65%>" & CStr(objRdr("fldAssetBarcode")) & "</TD></TR>" & _
                                         "<TR><TD><b>Sub Category :</b></TD><TD>" & CStr(objRdr("fld_CatSubName")) & "</TD></TR>" & _
                                         "<TR><TD valign=top><b>Description :</b></TD><TD>" & CStr(objRdr("fld_AssetDesc")) & "</TD></TR>" & _
                                         "</TBODY>" & _
                                         "</TABLE>"
                        ElseIf strTrfType = "ADJ" Then
                            strRetVal += "<TABLE border=0 cellPadding=1 cellSpacing=1 width=100%>" & _
                                         "<TBODY align=left>" & _
                                         "<TR><TD width=35%><b>Asset ID (AMS) :</b></TD><TD width=65%>" & CStr(objRdr("fldAssetBarcode")) & "</TD></TR>" & _
                                         "<TR><TD><b>Cost (S$) :</b></TD><TD>" & CStr(objRdr("fld_AssetCost")) & "</TD></TR>" & _
                                         "</TBODY>" & _
                                         "</TABLE>"
                        End If

                    End While
                End If
            End If
            objRdr.Close()

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub dgTrfAddExp_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgTrfAddExp.ItemDataBound
        Try
            Dim txtQty As TextBox
            Dim ddlCatSub As DropDownList
            Dim ddlDept As DropDownList
            Dim ddlLoc As DropDownList
            Dim ddlLocSub As DropDownList
            Dim ddlOwner As DropDownList

            txtQty = CType(e.Item.FindControl("txtQtyADD"), TextBox)
            If Not txtQty Is Nothing Then
                Dim strQty As String = e.Item.Cells(3).Text
                txtQty.Text = strQty
            End If

            ddlCatSub = CType(e.Item.FindControl("ddlCatSubADD"), DropDownList)
            If Not ddlCatSub Is Nothing Then
                Dim strCatID As String = e.Item.Cells(1).Text
                If strCatID <> "0" Then
                    fnPopulateDropDownList(clsCategory.fnCategoryGetAllRecDR(strCatID, 0, "fld_CatSubName", "ASC"), ddlCatSub, "fld_CatSubID", "fld_CatSubName", False)
                Else
                    ddlCatSub.Items.Clear()
                    ddlCatSub.Items.Insert(0, New ListItem("", "-1"))
                End If
            End If

            ddlDept = CType(e.Item.FindControl("ddlDeptADD"), DropDownList)
            If Not ddlDept Is Nothing Then
                fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDept, "fld_DepartmentID", "fld_DepartmentCode", False)
                If Session("AdminF") = "N" Then ddlDept.Items.Remove(ddlDept.Items.FindByValue("-1"))
            End If

            ddlLoc = CType(e.Item.FindControl("ddlLocADD"), DropDownList)
            If Not ddlLoc Is Nothing Then
                fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLoc, "fld_LocationID", "fld_LocationCode", False)
            End If

            ddlLocSub = CType(e.Item.FindControl("ddlLocSubADD"), DropDownList)
            If Not ddlLocSub Is Nothing Then
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If

            ddlOwner = CType(e.Item.FindControl("ddlOwnerADD"), DropDownList)
            If Not ddlOwner Is Nothing Then
                fnPopulateDropDownList(ClsUser.fnUsrGetUsrFromADDR("OU", "9999", "", ""), ddlOwner, "fld_ADLoginID", "fld_ADUsrName", False)
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butTrfAddExp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butTrfAddExp.Click
        Try
            Dim strErrorF As String = "N"
            Dim strMsg As String = ""
            Dim strXMLrec As String = ""

            ''validate records
            fnValSelectRecordADDExp(strErrorF, strMsg)
            If strErrorF = "Y" Then '' error found...
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
                Exit Sub
            End If

            ''Get Records selected in XML
            strXMLrec = fnGetXMLRecADDExp()

            ''Update record
            If strXMLrec <> "" Then
                Dim intRetVal As Integer
                intRetVal = clsNFSTrf.fnNFSTrfAct_InsertADDRecords(strXMLrec, Session("UsrID"))
                If intRetVal > 0 Then
                    ''Get Records
                    fnPopulateRecords()

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + "Record(s) add succesfully." + "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    lblErrorMessage.Text = "Record(s) add failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                End If
            Else
                lblErrorMessage.Text = "Please select at least one record to add."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnGetXMLRecADDExp() As String
        Try
            Dim TblNFSAdd As DataTable = fnCreateDataTableADD()
            Dim RowNFSAdd As DataRow

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox

            Dim txtQty As TextBox
            Dim ddlCatSubID As DropDownList
            Dim ddlDeptID As DropDownList
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim ddlAOwnerID As DropDownList
            Dim txtRemarks As TextBox

            Dim sXMLString As String = ""

            For Each GridItem In dgTrfAddExp.Items
                chkSelectedRec = CType(GridItem.Cells(8).FindControl("cdCheckAddExpS"), CheckBox)
                If chkSelectedRec.Checked Then
                    txtQty = CType(GridItem.Cells(7).FindControl("txtQtyADD"), TextBox)
                    ddlCatSubID = CType(GridItem.Cells(7).FindControl("ddlCatSubADD"), DropDownList)
                    ddlDeptID = CType(GridItem.Cells(7).FindControl("ddlDeptADD"), DropDownList)
                    ddlLocID = CType(GridItem.Cells(7).FindControl("ddlLocADD"), DropDownList)
                    ddlLocSubID = CType(GridItem.Cells(7).FindControl("ddlLocSubADD"), DropDownList)
                    ddlAOwnerID = CType(GridItem.Cells(7).FindControl("ddlOwnerADD"), DropDownList)
                    txtRemarks = CType(GridItem.Cells(7).FindControl("txtRemarkADD"), TextBox)

                    ''Add New Row to Datatable
                    RowNFSAdd = TblNFSAdd.NewRow()        'declaring a new row
                    RowNFSAdd.Item("dt_CDetID") = GridItem.Cells(0).Text
                    RowNFSAdd.Item("dt_Qty") = Trim(txtQty.Text)
                    RowNFSAdd.Item("dt_CatSub") = IIf(ddlCatSubID.SelectedValue = "-1", "0", ddlCatSubID.SelectedValue)
                    RowNFSAdd.Item("dt_Dept") = IIf(ddlDeptID.SelectedValue = "-1", "0", ddlDeptID.SelectedValue)
                    RowNFSAdd.Item("dt_Loc") = IIf(ddlLocID.SelectedValue = "-1", "0", ddlLocID.SelectedValue)
                    RowNFSAdd.Item("dt_LocSub") = IIf(ddlLocSubID.SelectedValue = "-1", "0", ddlLocSubID.SelectedValue)
                    RowNFSAdd.Item("dt_AssOwner") = IIf(ddlAOwnerID.SelectedValue = "-1", "", ddlAOwnerID.SelectedValue)
                    RowNFSAdd.Item("dt_Remark") = Trim(txtRemarks.Text)
                    TblNFSAdd.Rows.Add(RowNFSAdd)
                End If
            Next

            If TblNFSAdd.Rows.Count > 0 Then
                ''Get XML
                Dim ds As New DataSet
                ds = New DataSet            'creating a dataset
                ds.Tables.Add(TblNFSAdd)    'assign datatable to dataset
                If Not ds Is Nothing Then
                    Dim loCol As DataColumn
                    'Prepare XML output from the DataSet as a string
                    For Each loCol In ds.Tables(0).Columns
                        loCol.ColumnMapping = System.Data.MappingType.Attribute
                    Next
                    sXMLString = ds.GetXml
                End If
            End If

            fnGetXMLRecADDExp = Trim(sXMLString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub fnValSelectRecordADDExp(ByRef strErrorF As String, ByRef strErrorMsg As String)
        Try
            strErrorF = "N"
            strErrorMsg = ""

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox

            Dim txtQty As TextBox
            Dim ddlCatSubID As DropDownList
            Dim ddlDeptID As DropDownList
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim intRow As Integer = 0

            For Each GridItem In dgTrfAddExp.Items
                intRow = intRow + 1

                chkSelectedRec = CType(GridItem.Cells(8).FindControl("cdCheckAddExpS"), CheckBox)
                If chkSelectedRec.Checked Then
                    txtQty = CType(GridItem.Cells(7).FindControl("txtQtyADD"), TextBox)
                    If Trim(txtQty.Text) = "" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please enter Quantity (Record " & intRow & ")."
                        Exit For
                    Else
                        If Not IsNumeric(Trim(txtQty.Text)) Then
                            strErrorF = "Y"
                            strErrorMsg = "Quantity just allow number (Record " & intRow & ")."
                            Exit For
                        ElseIf Trim(txtQty.Text) = "0" Then
                            strErrorF = "Y"
                            strErrorMsg = "Quantity must more than '0' (Record " & intRow & ")."
                            Exit For
                        End If
                    End If

                    ddlCatSubID = CType(GridItem.Cells(7).FindControl("ddlCatSubADD"), DropDownList)
                    If ddlCatSubID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Sub Category (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlDeptID = CType(GridItem.Cells(7).FindControl("ddlDeptADD"), DropDownList)
                    If ddlDeptID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Department (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlLocID = CType(GridItem.Cells(7).FindControl("ddlLocADD"), DropDownList)
                    If ddlLocID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Location (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlLocSubID = CType(GridItem.Cells(7).FindControl("ddlLocSubADD"), DropDownList)
                    If ddlLocSubID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Sub Location (Record " & intRow & ")."
                        Exit For
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgTrfInAddExp_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgTrfInAddExp.ItemDataBound
        Try
            Dim txtQty As TextBox
            Dim ddlCatSub As DropDownList
            Dim ddlDept As DropDownList
            Dim ddlLoc As DropDownList
            Dim ddlLocSub As DropDownList
            Dim ddlOwner As DropDownList

            txtQty = CType(e.Item.FindControl("txtQtyTIADD"), TextBox)
            If Not txtQty Is Nothing Then
                Dim strQty As String = e.Item.Cells(3).Text
                txtQty.Text = strQty
            End If

            ddlCatSub = CType(e.Item.FindControl("ddlCatSubTIADD"), DropDownList)
            If Not ddlCatSub Is Nothing Then
                Dim strCatID As String = e.Item.Cells(1).Text
                If strCatID <> "0" Then
                    fnPopulateDropDownList(clsCategory.fnCategoryGetAllRecDR(strCatID, 0, "fld_CatSubName", "ASC"), ddlCatSub, "fld_CatSubID", "fld_CatSubName", False)
                Else
                    ddlCatSub.Items.Clear()
                    ddlCatSub.Items.Insert(0, New ListItem("", "-1"))
                End If
            End If

            ddlDept = CType(e.Item.FindControl("ddlDeptTIADD"), DropDownList)
            If Not ddlDept Is Nothing Then
                fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDept, "fld_DepartmentID", "fld_DepartmentCode", False)
                If Session("AdminF") = "N" Then ddlDept.Items.Remove(ddlDept.Items.FindByValue("-1"))
            End If

            ddlLoc = CType(e.Item.FindControl("ddlLocTIADD"), DropDownList)
            If Not ddlLoc Is Nothing Then
                fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLoc, "fld_LocationID", "fld_LocationCode", False)
            End If

            ddlLocSub = CType(e.Item.FindControl("ddlLocSubTIADD"), DropDownList)
            If Not ddlLocSub Is Nothing Then
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If

            ddlOwner = CType(e.Item.FindControl("ddlOwnerTIADD"), DropDownList)
            If Not ddlOwner Is Nothing Then
                fnPopulateDropDownList(ClsUser.fnUsrGetUsrFromADDR("OU", "9999", "", ""), ddlOwner, "fld_ADLoginID", "fld_ADUsrName", False)
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butTrfInAddExp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butTrfInAddExp.Click
        Try
            Dim strErrorF As String = "N"
            Dim strMsg As String = ""
            Dim strXMLrec As String = ""

            ''validate records
            fnValSelectRecordTrfInADDExp(strErrorF, strMsg)
            If strErrorF = "Y" Then '' error found...
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
                Exit Sub
            End If

            ''Get Records selected in XML
            strXMLrec = fnGetXMLRecTrfInADDExp()

            ''Update record
            If strXMLrec <> "" Then
                Dim intRetVal As Integer
                intRetVal = clsNFSTrf.fnNFSTrfAct_InsertTrfINRecords(strXMLrec, Session("UsrID"))
                If intRetVal > 0 Then
                    ''Get Records
                    fnPopulateRecords()

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + "Record(s) add succesfully." + "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    lblErrorMessage.Text = "Record(s) add failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                End If
            Else
                lblErrorMessage.Text = "Please select at least one record to add."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnGetXMLRecTrfInADDExp() As String
        Try
            Dim TblNFSAdd As DataTable = fnCreateDataTableADD()
            Dim RowNFSAdd As DataRow

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox

            Dim txtQty As TextBox
            Dim ddlCatSubID As DropDownList
            Dim ddlDeptID As DropDownList
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim ddlAOwnerID As DropDownList
            Dim txtRemarks As TextBox

            Dim sXMLString As String = ""

            For Each GridItem In dgTrfInAddExp.Items
                chkSelectedRec = CType(GridItem.Cells(8).FindControl("cdCheckTIAddExpS"), CheckBox)
                If chkSelectedRec.Checked Then
                    txtQty = CType(GridItem.Cells(7).FindControl("txtQtyTIADD"), TextBox)
                    ddlCatSubID = CType(GridItem.Cells(7).FindControl("ddlCatSubTIADD"), DropDownList)
                    ddlDeptID = CType(GridItem.Cells(7).FindControl("ddlDeptTIADD"), DropDownList)
                    ddlLocID = CType(GridItem.Cells(7).FindControl("ddlLocTIADD"), DropDownList)
                    ddlLocSubID = CType(GridItem.Cells(7).FindControl("ddlLocSubTIADD"), DropDownList)
                    ddlAOwnerID = CType(GridItem.Cells(7).FindControl("ddlOwnerTIADD"), DropDownList)
                    txtRemarks = CType(GridItem.Cells(7).FindControl("txtRemarkTIADD"), TextBox)

                    ''Add New Row to Datatable
                    RowNFSAdd = TblNFSAdd.NewRow()        'declaring a new row
                    RowNFSAdd.Item("dt_CDetID") = GridItem.Cells(0).Text
                    RowNFSAdd.Item("dt_Qty") = Trim(txtQty.Text)
                    RowNFSAdd.Item("dt_CatSub") = IIf(ddlCatSubID.SelectedValue = "-1", "0", ddlCatSubID.SelectedValue)
                    RowNFSAdd.Item("dt_Dept") = IIf(ddlDeptID.SelectedValue = "-1", "0", ddlDeptID.SelectedValue)
                    RowNFSAdd.Item("dt_Loc") = IIf(ddlLocID.SelectedValue = "-1", "0", ddlLocID.SelectedValue)
                    RowNFSAdd.Item("dt_LocSub") = IIf(ddlLocSubID.SelectedValue = "-1", "0", ddlLocSubID.SelectedValue)
                    RowNFSAdd.Item("dt_AssOwner") = IIf(ddlAOwnerID.SelectedValue = "-1", "", ddlAOwnerID.SelectedValue)
                    RowNFSAdd.Item("dt_Remark") = Trim(txtRemarks.Text)
                    TblNFSAdd.Rows.Add(RowNFSAdd)
                End If
            Next

            If TblNFSAdd.Rows.Count > 0 Then
                ''Get XML
                Dim ds As New DataSet
                ds = New DataSet            'creating a dataset
                ds.Tables.Add(TblNFSAdd)    'assign datatable to dataset
                If Not ds Is Nothing Then
                    Dim loCol As DataColumn
                    'Prepare XML output from the DataSet as a string
                    For Each loCol In ds.Tables(0).Columns
                        loCol.ColumnMapping = System.Data.MappingType.Attribute
                    Next
                    sXMLString = ds.GetXml
                End If
            End If

            fnGetXMLRecTrfInADDExp = Trim(sXMLString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub fnValSelectRecordTrfInADDExp(ByRef strErrorF As String, ByRef strErrorMsg As String)
        Try
            strErrorF = "N"
            strErrorMsg = ""

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox

            Dim txtQty As TextBox
            Dim ddlCatSubID As DropDownList
            Dim ddlDeptID As DropDownList
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim intRow As Integer = 0

            For Each GridItem In dgTrfInAddExp.Items
                intRow = intRow + 1

                chkSelectedRec = CType(GridItem.Cells(8).FindControl("cdCheckTIAddExpS"), CheckBox)
                If chkSelectedRec.Checked Then
                    txtQty = CType(GridItem.Cells(7).FindControl("txtQtyTIADD"), TextBox)
                    If Trim(txtQty.Text) = "" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please enter Quantity (Record " & intRow & ")."
                        Exit For
                    Else
                        If Not IsNumeric(Trim(txtQty.Text)) Then
                            strErrorF = "Y"
                            strErrorMsg = "Quantity just allow number (Record " & intRow & ")."
                            Exit For
                        ElseIf Trim(txtQty.Text) = "0" Then
                            strErrorF = "Y"
                            strErrorMsg = "Quantity must more than '0' (Record " & intRow & ")."
                            Exit For
                        End If
                    End If

                    ddlCatSubID = CType(GridItem.Cells(7).FindControl("ddlCatSubTIADD"), DropDownList)
                    If ddlCatSubID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Sub Category (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlDeptID = CType(GridItem.Cells(7).FindControl("ddlDeptTIADD"), DropDownList)
                    If ddlDeptID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Department (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlLocID = CType(GridItem.Cells(7).FindControl("ddlLocTIADD"), DropDownList)
                    If ddlLocID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Location (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlLocSubID = CType(GridItem.Cells(7).FindControl("ddlLocSubTIADD"), DropDownList)
                    If ddlLocSubID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Sub Location (Record " & intRow & ")."
                        Exit For
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgTrfInUpdExp_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgTrfInUpdExp.ItemDataBound
        Try
            Dim ddlCatSub As DropDownList
            Dim ddlDept As DropDownList
            Dim ddlLoc As DropDownList
            Dim ddlLocSub As DropDownList
            Dim ddlOwner As DropDownList

            ddlCatSub = CType(e.Item.FindControl("ddlCatSubTIUpd"), DropDownList)
            If Not ddlCatSub Is Nothing Then
                Dim strCatID As String = e.Item.Cells(1).Text
                If strCatID <> "0" Then
                    fnPopulateDropDownList(clsCategory.fnCategoryGetAllRecDR(strCatID, 0, "fld_CatSubName", "ASC"), ddlCatSub, "fld_CatSubID", "fld_CatSubName", False)
                Else
                    ddlCatSub.Items.Clear()
                    ddlCatSub.Items.Insert(0, New ListItem("", "-1"))
                End If
            End If

            ddlDept = CType(e.Item.FindControl("ddlDeptTIUpd"), DropDownList)
            If Not ddlDept Is Nothing Then
                fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDept, "fld_DepartmentID", "fld_DepartmentCode", False)
                If Session("AdminF") = "N" Then ddlDept.Items.Remove(ddlDept.Items.FindByValue("-1"))
            End If

            ddlLoc = CType(e.Item.FindControl("ddlLocTIUpd"), DropDownList)
            If Not ddlLoc Is Nothing Then
                fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLoc, "fld_LocationID", "fld_LocationCode", False)
            End If

            ddlLocSub = CType(e.Item.FindControl("ddlLocSubTIUpd"), DropDownList)
            If Not ddlLocSub Is Nothing Then
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If

            ddlOwner = CType(e.Item.FindControl("ddlOwnerTIUpd"), DropDownList)
            If Not ddlOwner Is Nothing Then
                fnPopulateDropDownList(ClsUser.fnUsrGetUsrFromADDR("OU", "9999", "", ""), ddlOwner, "fld_ADLoginID", "fld_ADUsrName", False)
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butTrfInUpdExp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butTrfInUpdExp.Click
        Try
            Dim strErrorF As String = "N"
            Dim strMsg As String = ""
            Dim strXMLrec As String = ""

            ''validate records
            fnValSelectRecordTrfInUpdExp(strErrorF, strMsg)
            If strErrorF = "Y" Then '' error found...
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
                Exit Sub
            End If

            ''Get Records selected in XML
            strXMLrec = fnGetXMLRecTrfInUpdExp()

            ''Update record
            If strXMLrec <> "" Then
                Dim intRetVal As Integer
                intRetVal = clsNFSTrf.fnNFSTrfAct_UpdateTrfINRecords(strXMLrec, Session("UsrID"))
                If intRetVal > 0 Then
                    ''Get Records
                    fnPopulateRecords()

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + "Record(s) update succesfully." + "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    lblErrorMessage.Text = "Record(s) update failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                End If
            Else
                lblErrorMessage.Text = "Please select at least one record to update."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnGetXMLRecTrfInUpdExp() As String
        Try
            Dim TblNFSAdd As DataTable = fnCreateDataTableADD()
            Dim RowNFSAdd As DataRow

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox

            Dim ddlCatSubID As DropDownList
            Dim ddlDeptID As DropDownList
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim ddlAOwnerID As DropDownList
            Dim txtRemarks As TextBox

            Dim sXMLString As String = ""

            For Each GridItem In dgTrfInUpdExp.Items
                chkSelectedRec = CType(GridItem.Cells(9).FindControl("cdCheckTIUpdExpS"), CheckBox)
                If chkSelectedRec.Checked Then
                    ddlCatSubID = CType(GridItem.Cells(8).FindControl("ddlCatSubTIUpd"), DropDownList)
                    ddlDeptID = CType(GridItem.Cells(8).FindControl("ddlDeptTIUpd"), DropDownList)
                    ddlLocID = CType(GridItem.Cells(8).FindControl("ddlLocTIUpd"), DropDownList)
                    ddlLocSubID = CType(GridItem.Cells(8).FindControl("ddlLocSubTIUpd"), DropDownList)
                    ddlAOwnerID = CType(GridItem.Cells(8).FindControl("ddlOwnerTIUpd"), DropDownList)
                    txtRemarks = CType(GridItem.Cells(8).FindControl("txtRemarkTIUpd"), TextBox)

                    ''Add New Row to Datatable
                    RowNFSAdd = TblNFSAdd.NewRow()        'declaring a new row
                    RowNFSAdd.Item("dt_CDetID") = GridItem.Cells(0).Text
                    RowNFSAdd.Item("dt_Qty") = "0"
                    RowNFSAdd.Item("dt_CatSub") = IIf(ddlCatSubID.SelectedValue = "-1", "0", ddlCatSubID.SelectedValue)
                    RowNFSAdd.Item("dt_Dept") = IIf(ddlDeptID.SelectedValue = "-1", "0", ddlDeptID.SelectedValue)
                    RowNFSAdd.Item("dt_Loc") = IIf(ddlLocID.SelectedValue = "-1", "0", ddlLocID.SelectedValue)
                    RowNFSAdd.Item("dt_LocSub") = IIf(ddlLocSubID.SelectedValue = "-1", "0", ddlLocSubID.SelectedValue)
                    RowNFSAdd.Item("dt_AssOwner") = IIf(ddlAOwnerID.SelectedValue = "-1", "", ddlAOwnerID.SelectedValue)
                    RowNFSAdd.Item("dt_Remark") = Trim(txtRemarks.Text)
                    TblNFSAdd.Rows.Add(RowNFSAdd)
                End If
            Next

            If TblNFSAdd.Rows.Count > 0 Then
                ''Get XML
                Dim ds As New DataSet
                ds = New DataSet            'creating a dataset
                ds.Tables.Add(TblNFSAdd)    'assign datatable to dataset
                If Not ds Is Nothing Then
                    Dim loCol As DataColumn
                    'Prepare XML output from the DataSet as a string
                    For Each loCol In ds.Tables(0).Columns
                        loCol.ColumnMapping = System.Data.MappingType.Attribute
                    Next
                    sXMLString = ds.GetXml
                End If
            End If

            fnGetXMLRecTrfInUpdExp = Trim(sXMLString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub fnValSelectRecordTrfInUpdExp(ByRef strErrorF As String, ByRef strErrorMsg As String)
        Try
            strErrorF = "N"
            strErrorMsg = ""

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox

            Dim ddlCatSubID As DropDownList
            Dim ddlDeptID As DropDownList
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim intRow As Integer = 0

            For Each GridItem In dgTrfInUpdExp.Items
                intRow = intRow + 1

                chkSelectedRec = CType(GridItem.Cells(9).FindControl("cdCheckTIUpdExpS"), CheckBox)
                If chkSelectedRec.Checked Then

                    ddlCatSubID = CType(GridItem.Cells(8).FindControl("ddlCatSubTIUpd"), DropDownList)
                    If ddlCatSubID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Sub Category (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlDeptID = CType(GridItem.Cells(8).FindControl("ddlDeptTIUpd"), DropDownList)
                    If ddlDeptID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Department (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlLocID = CType(GridItem.Cells(8).FindControl("ddlLocTIUpd"), DropDownList)
                    If ddlLocID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Location (Record " & intRow & ")."
                        Exit For
                    End If

                    ddlLocSubID = CType(GridItem.Cells(8).FindControl("ddlLocSubTIUpd"), DropDownList)
                    If ddlLocSubID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Sub Location (Record " & intRow & ")."
                        Exit For
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class