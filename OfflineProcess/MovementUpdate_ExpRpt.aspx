<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MovementUpdate_ExpRpt.aspx.vb" Inherits="AMS.MovementUpdate_ExpRpt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title> 
</head>
<body>
        <b><asp:Label ID=lblRptTitle Text="Stocktake History" runat=server></asp:Label></b><br />
        Printed Date : <asp:Label ID=lblPrintDate runat=server></asp:Label> <br />
        StockTake For : <asp:Label ID=stocktakefor runat=server></asp:Label><br />
        Stocktake Start Date : <asp:Label ID=stocktakeStart runat=server></asp:Label> <br />
        Stocktake End Date : <asp:Label ID=stocktakeEnd runat=server></asp:Label>  <br />
        Total Asset(s) : <asp:Label ID=TotalAsset runat=server></asp:Label>   <br />
        Total Scan :    <asp:Label ID=totalScan runat=server></asp:Label> <br />
        Total Missing :    <asp:Label ID=totalMissing runat=server></asp:Label>  <br />  <br />
         

       <b> Stocktake Result</b><br />
      <b> Asset Scan (with Changes)</b>
      <asp:datagrid id="dgScanChange" Runat="server" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
                                                                                                    <asp:boundcolumn datafield="fld_AssetDesc" headertext="Description" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Department" datafield="fld_DepartmentName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Location" datafield="fld_LocationName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Sub Location" datafield="fld_LocSubName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Assigned Owner" datafield="fld_OwnerName" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Remarks" datafield="fld_AssetRemarks" Visible=false >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:TemplateColumn HeaderText="(Stocktake Result) Location">
                                                                                                        <ItemTemplate>
                                                                                                           
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:TemplateColumn HeaderText="(Stocktake Result) Sub Location">
                                                                                                        <ItemTemplate>
                                                                                                         </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:TemplateColumn HeaderText="(Stocktake Result) Assigned Owner">
                                                                                                        <ItemTemplate>
                                                                                                         </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:TemplateColumn HeaderText="(Stocktake Result) Remarks">
                                                                                                        <ItemTemplate>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:boundcolumn headertext="(Found) Department" datafield="fld_newDeptId" Visible=false >
									                                                                    <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                                    </asp:boundcolumn>
								                                                                   <asp:boundcolumn headertext="(Found) Location" datafield="fld_newLocID" Visible=false >
									                                                                    <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                                    </asp:boundcolumn>
								                                                                    <asp:boundcolumn headertext="(Found) Assigned Owner" datafield="fld_newOwnerID" Visible=false >
									                                                                    <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                                    </asp:boundcolumn>
								                                                                    <asp:boundcolumn headertext="(Found) Remarks" datafield="fld_NewRemark" Visible=false >
									                                                                    <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                                    </asp:boundcolumn>									                                                                    
								                                                                    <asp:boundcolumn HeaderText="fld_STDetailID" datafield="fld_STDetailID" Visible=false>
									                                                                    <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
								                                                                    </asp:boundcolumn>
                                                                                                    <%--<asp:TemplateColumn HeaderText="">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblSTDetailID" runat="server" Text='<%# Eval("fld_STDetailID") %>'></asp:Label>                                                                                                            
                                                                                                        </ItemTemplate>
                                                                                                        <EditItemTemplate>
                                                                                                            <asp:Label ID="lblSTDetailID" runat="server" Text='<%# Eval("fld_STDetailID") %>'></asp:Label>                                                                                                            
                                                                                                        </EditItemTemplate>
                                                                                                    </asp:TemplateColumn>--%>
								                                                                    <asp:boundcolumn headertext="(Found) Sub Location" datafield="fld_newLocSubID" Visible=false >
									                                                                    <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                                    </asp:boundcolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
	<br /><br /> <b> Asset Scan (Changes Done)</b>
             <asp:datagrid id="dgChangeDone" Runat="server" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
                                                                                                    <asp:boundcolumn datafield="fld_AssetDesc" headertext="Description" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Department" datafield="fld_OrigDept" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Location" datafield="fld_OrigLoc" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Sub Location" datafield="fld_OrigLocSub" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Assigned Owner" datafield="fld_OrigOwner" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Remarks" datafield="fld_OrigRemark"  Visible=false>
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Location" datafield="fld_NewLoc" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Sub Location" datafield="fld_NewLocSub" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Assigned Owner" datafield="fld_NewOwner" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Remarks" datafield="fld_NewRemark" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
										                                                        </Columns>
                                                                                                </asp:datagrid>
						<br /><br />
                        
                        <b>Asset Scan (without Changes)</b>		<br />	   
                                <asp:datagrid id="dgScanNoChange" Runat="server" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
                                                                                                    <asp:boundcolumn datafield="fld_AssetDesc" headertext="Description" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Department" datafield="fld_OrigDept" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Location" datafield="fld_OrigLoc" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Sub Location" datafield="fld_OrigLocSub" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Assigned Owner" datafield="fld_OrigOwner" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Remarks" datafield="fld_OrigRemark"  Visible=false>
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Location" datafield="fld_NewLoc" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Sub Location" datafield="fld_NewLocSub" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Assigned Owner" datafield="fld_NewOwner" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Remarks" datafield="fld_NewRemark" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
                                                                                            <br /><br />
                                                                                            <b>Asset Missing</b>    <br />  
                                                                                                   <asp:datagrid id="dgAssetMiss" Runat="server" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
                                                                                                    <asp:boundcolumn datafield="fld_AssetDesc" headertext="Description" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                         <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Department" datafield="fld_OrigDept" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Location" datafield="fld_OrigLoc" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Sub Location" datafield="fld_OrigLocSub" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Assigned Owner" datafield="fld_OrigOwner" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Remarks" datafield="fld_OrigRemark"  Visible=false>
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>					
										                                                        </Columns>
									                                                        </asp:datagrid>                                               
</body>
</html>
