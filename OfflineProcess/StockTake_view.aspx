<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="StockTake_view.aspx.vb" Inherits="AMS.StockTake_view" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script type="text/javascript">
	function ConfirmUpload() {
		var flag;
		flag = false;
		
		flag = window.confirm("Are you sure you want to upload these record(s) to text file for stocktaking?");
		
		return flag;						
	}
	</script>
</head>
<body>
    <form id="Frm_Stocktake" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Offline Processing : Stocktake Action</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<table id="Table2" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tbody>
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr>
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td align="center"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td vAlign="top" width="100%">
						<!--Start Main Content-->
						<table height="400" cellSpacing="1" cellPadding="0" width="100%" align="left" border="0">
							<tr>
								<td vAlign="top" align="left">
									<asp:button id="butUpload2PDA" Runat="server" 
                                        Text="Generate Stock Take ID for Stocktaking" Width="283px"></asp:button>
								</td>
							</tr>
							<tr>
								<td vAlign="top" align="right" bgColor="#a3a9cc">
									<table cellSpacing="1" cellPadding="1" width="100%" border="0">
										<tr align=left>
											<td width="20%">
											    <asp:dropdownlist id="ddlPageSize" Runat="server" AutoPostBack="True"></asp:dropdownlist>&nbsp;&nbsp;<font color="white">per page</font>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td vAlign="top" align="center" height="400">
									<!--Datagrid for display record.-->
									<asp:datagrid id="dgAsset" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										ItemStyle-Height="25" AllowSorting="True" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" AllowPaging="True" PageSize="10" PagerStyle-HorizontalAlign="Right"
										PagerStyle-VerticalAlign="Middle" PagerStyle-Mode="NumericPages" PagerStyle-Position="Top"
										PagerStyle-BackColor="#a3a9cc" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										<headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											Height="25"></headerstyle>
										<Columns>
										    <asp:boundcolumn HeaderText="fld_AssetID" datafield="fld_AssetID" Visible=false>
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn HeaderText="Asset ID" datafield="fldAssetBarcode" SortExpression="fldAssetBarcode">
												<itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr" SortExpression="fld_AssetTypeStr" >
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Category" datafield="fld_CategoryName" SortExpression="fld_CategoryName">
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Subcategory" datafield="fld_CatSubName" SortExpression="fld_CatSubName">
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Department" datafield="fld_DepartmentName" SortExpression="fld_DepartmentName">
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Location" datafield="fld_LocationName" SortExpression="fld_LocationName">
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Sub Location" datafield="fld_LocSubName" SortExpression="fld_LocSubName">
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Assigned Owner" datafield="fld_OwnerName" SortExpression="fld_OwnerName">
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Status" datafield="fld_AssetStatusStr" SortExpression="fld_AssetStatusStr">
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
										</Columns>
									</asp:datagrid>
								</td>
							</tr>
						</table>
						<!--End Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
				</tbody>
			</table>
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnSortName" runat="server"> 
			<input type="hidden" id="hdnSortAD" runat="server">
			<input type="hidden" id="hdnAssetCatID" runat="server">
			<input type="hidden" id="hdnAssetCatSubID" runat="server">
			<input type="hidden" id="hdnDeptID" runat="server">
			<input type="hidden" id="hdnLocID" runat="server">
			<input type="hidden" id="hdnLocSubID" runat="server">
			<input type="hidden" id="hdnOwnerID" runat="server">
			<input type="hidden" id="hdnMvFDt" runat="server">
			<input type="hidden" id="hdnMvTDt" runat="server">
			<input type="hidden" id="hdnAType" runat="server">
			<!-- End  : Hidden Fields -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
	</form>
</body>
</html>
