#Region "Information Section"
' ****************************************************************************************************
' Description       : View Stocktake Exercise
' Purpose           : View/Delete Stocktake Exercise
' Author            : See Siew
' Date              : 19/11/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class StocktakePeriod_view
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butDelSTPeiod.Attributes.Add("OnClick", "return ConfirmDelete()")

                ''default Sorting
                hdnSortName.Value = "fld_STStartDt"
                hdnSortAD.Value = "DESC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Check Access Right
                fnCheckAccessRight()

                ''Display Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub fnCheckAccessRight()
        Try
            ' ''Checking For Assign Right 
            If (InStr(Session("AR"), "offlineProcess|StockTakeExer|add") = 0) Then butAddSTPeiod.Visible = False

            If (InStr(Session("AR"), "offlineProcess|StockTakeExer|edit") = 0) Then dgStocktake.Columns(8).Visible = False

            If (InStr(Session("AR"), "offlineProcess|StockTakeExer|del") = 0) Then
                butDelSTPeiod.Visible = False
                dgStocktake.Columns(9).Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Sort Header Display
            fnSortHeaderDisplay()

            '' Get Records
            dgStocktake.DataSource = clsOfflineProccess.fnOfflineProPeriod_GetStocktakePeriod(hdnSortName.Value, hdnSortAD.Value)
            dgStocktake.DataBind()
            If Not dgStocktake.Items.Count > 0 Then ''Not Records found
                ddlPageSize.Enabled = False
                butDelSTPeiod.Enabled = False
                lblErrorMessage.Text = "There is no record."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By fld_STStartDt (3), fld_STEndDt (4), fld_usrName(5)
        Dim intSortIndex As Integer
        Dim strSortHeader As String = ""

        ''Set Default Header
        dgStocktake.Columns(3).HeaderText = "Start Date"
        dgStocktake.Columns(4).HeaderText = "End Date"
        dgStocktake.Columns(5).HeaderText = "Created By"
        dgStocktake.Columns(6).HeaderText = "Created Date"

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fld_STStartDt"
                intSortIndex = 3
                strSortHeader = "Start Date"
            Case "fld_STEndDt"
                intSortIndex = 4
                strSortHeader = "End Date"
            Case "fld_usrName"
                intSortIndex = 5
                strSortHeader = "Created By"
            Case "fld_CreatedDt"
                intSortIndex = 6
                strSortHeader = "Created Date"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgStocktake.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgStocktake.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Function fnShowEditHyperlink(ByVal strSTPID As String) As String
        Dim strRetVal As String = ""
        If (InStr(Session("AR"), "offlineProcess|StockTakeExer|edit") > 0) Then  '' Check For Edit Access Right
            strRetVal += "<a id=hlEdit href='StocktakePeriod_AddEdit.aspx?AddEditF=E&STPeriodID=" + strSTPID + "' target=_self >" & _
                        "<img id=imgUpdate src=../images/update.gif alt='Click for Update Stocktake Exercise.' border=0 >" & _
                        "</a>"
        End If

        Return (strRetVal)
    End Function

    Protected Sub butAddSTPeiod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAddSTPeiod.Click
        Response.Redirect("StocktakePeriod_AddEdit.aspx?AddEditF=I")
    End Sub

    Protected Sub butDelSTPeiod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelSTPeiod.Click
        Try
            Dim strMsg As String
            Dim retval As Integer


            ''*** Start: Get selected for deleted
            Dim GridItem As DataGridItem
            Dim chkSelectedDel As CheckBox
            Dim strSTIDForDel As String = ""
            Dim strDelST As String = ""
            For Each GridItem In dgStocktake.Items
                chkSelectedDel = CType(GridItem.Cells(9).FindControl("DeleteThis"), CheckBox)
                If chkSelectedDel.Checked Then
                    strSTIDForDel += GridItem.Cells(0).Text & "^"
                    strDelST += "From " & GridItem.Cells(3).Text & " To " & Trim(GridItem.Cells(4).Text) & "), "
                End If
            Next
            If strDelST <> "" Then strDelST = Trim(strDelST).Substring(0, Trim(strDelST).Length - 1)
            ''*** End  : Get selected for deleted

            ''Start: Delete selected records from database
            retval = clsOfflineProccess.fnOfflineProPeriod_InsertUpdateDelete(strSTIDForDel, "", "", "", Session("UsrID"), "D")
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 Then
                strMsg = "Selected record(s) deleted successfully."
                fnPopulateRecords()

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Delete Stocktake Exercise(s) : " + strDelST)

                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>alert('" + strMsg + "');</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "Selected record(s) deleted failed."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
            ''End  : Message Notification

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub dgStocktake_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgStocktake.ItemDataBound
        Dim cbDelete As CheckBox
        cbDelete = CType(e.Item.FindControl("DeleteThis"), CheckBox)
        If (InStr(Session("AR"), "offlineProcess|StockTakeExer|del") > 0) Then  '' Check For Delete Access Right
            If Not cbDelete Is Nothing Then
                ''If Stocktake exercise have stocktake record, delete check box invisible
                If e.Item.Cells(1).Text <> "1" Then
                    cbDelete.Enabled = False
                    cbDelete.Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub dgStocktake_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgStocktake.PageIndexChanged
        dgStocktake.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub dgStocktake_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgStocktake.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgStocktake.CurrentPageIndex = 0
        dgStocktake.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    'Protected Sub btnRunBatchjobForRFID_Click(sender As Object, e As EventArgs) Handles btnRunBatchjobForRFID.Click
    '    Dim configurationAppSettings As New System.Configuration.AppSettingsReader
    '    Dim psi As New ProcessStartInfo()
    '    psi.FileName = configurationAppSettings.GetValue("BatchJobEXE", System.Type.GetType("System.String"))
    '    Dim p As Process = Process.Start(psi)
    '    p.Close()
    '    lblErrorMessage.Text = "Run Batch job for RFID has been done."
    '    lblErrorMessage.Visible = True
    '    'MessageBox.Show("Files transfere successful.")

    'End Sub
End Class