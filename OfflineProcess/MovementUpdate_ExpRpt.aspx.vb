Imports System.Data.SqlClient

Partial Public Class MovementUpdate_ExpRpt
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If

            Dim strRptField As String = ""
            Dim strRptType As String = ""
            Dim strCallType As String = ""

            ''Get Today Date
            lblPrintDate.Text = mfnGetCurrentDateForTopPage()
            strCallType = Request("CallType")

            stocktakefor.Text = clsEncryptDecrypt.DecryptText(Request("stocktakefor"))
            stocktakeStart.Text = Request("stocktakeStart")
            stocktakeEnd.Text = Request("stocktakeEnd")
            TotalAsset.Text = Request("TotalAsset")
            totalScan.Text = Request("totalScan")
            totalMissing.Text = Request("totalMissing")

            ''Get REport Type
            ''Get REport Title
            'If strRptType = "1" Then
            '    lblRptTitle.Text = "Contract Expiry Report"
            'Else
            '    lblRptTitle.Text = "Tracking Renewal Contract (Vendor)"
            'End If
            ''Get Report Fields
            Dim hdnid As String
            Dim srDeptId As String
            hdnid = Session("hdnSTPID")
            srDeptId = Session("srDeptId")

            ''Dynamic Generate Data grid Column
            'fnGenerateDGColumn(strRptType, strRptField)

            dgScanChange.DataSource = clsOfflineProccess.fnOfflinePro_GetSummaryStocktakeAssetRecords(hdnid, srDeptId, "SC")
            dgScanChange.DataBind()


            ''Get Stocktake Asset Details --> Changes Done
            dgChangeDone.DataSource = clsOfflineProccess.fnOfflinePro_GetSummaryStocktakeAssetRecords(hdnid, srDeptId, "CD")
            dgChangeDone.DataBind()

            ''Get Stocktake Asset Details --> Not Changes
            dgScanNoChange.DataSource = clsOfflineProccess.fnOfflinePro_GetSummaryStocktakeAssetRecords(hdnid, srDeptId, "SN")
            dgScanNoChange.DataBind()


            ''Get Stocktake Asset Details --> with missing
            dgAssetMiss.DataSource = clsOfflineProccess.fnOfflinePro_GetSummaryStocktakeAssetRecords(hdnid, srDeptId, "MI")
            dgAssetMiss.DataBind()

            ''Set Session to blank
            'Session("hdnSTPID") = ""
            'Session("srDeptId") = ""

            ''Populate Report into Excel/Word/Print out
            If strCallType = "EE" Then
                Response.ContentType = "application/ms-excel"
                Response.AddHeader("Content-Disposition", "inline;filename=StocktakeMovementRpt.xls")

            ElseIf strCallType = "EW" Then
                Response.ContentType = "application/ms-word"
                Response.AddHeader("Content-Disposition", "inline;filename=StocktakeMovementRpt.doc")

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

 
End Class