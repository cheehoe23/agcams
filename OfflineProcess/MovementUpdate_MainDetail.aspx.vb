#Region "Information Section"
' ****************************************************************************************************
' Description       : Stocktake Details
' Purpose           : Stocktake Details
' Author            : See Siew
' Date              : 12/11/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
#End Region

Partial Public Class MovementUpdate_MainDetail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butAcceptChange.Attributes.Add("OnClick", "return chkFrm()")

                'ddlDepartment.Attributes.Add("onchange", "ShowProgress();")

                ''Get Stocktake Period ID 
                hdnSTPID.Text = Request("STID")

                ''Get Department
                fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False)
                If Session("AdminF") = "N" Then ddlDepartment.Items.Remove(ddlDepartment.Items.FindByValue("-1"))

                ''Retrieve Records 
                fnGetRecordsFromDB()
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnGetRecordsFromDB()
        Dim objRdr As SqlDataReader
        Try
            Dim srDeptId As String = IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue)

            ''*** Get Stocktake Master Info
            objRdr = clsOfflineProccess.fnOfflinePro_GetSummaryStocktakeMainRec(hdnSTPID.Text, srDeptId) ' (ddlDepartment.SelectedValue)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()
                    lblHeader.Text = "(From " & CStr(objRdr("fld_STStartDt")) & " To " & CStr(objRdr("fld_STEndDt")) & ")"
                    hdnSTDueF.Text = CStr(objRdr("STChangeF"))

                    lblSTSDate.Text = CStr(objRdr("fld_STStartDt"))
                    lblSTTDate.Text = CStr(objRdr("fld_STEndDt"))
                    lblTotRec.Text = CStr(objRdr("TotAsset"))
                    lblTotScan.Text = CStr(objRdr("TotScan"))
                    lblTotMiss.Text = CStr(objRdr("TotMiss"))
                    lblTotFound.Text = CStr(objRdr("TotFound"))
                End If
            End If
            objRdr.Close()

            ''*** Check whether date is due --> not allow changes
            If hdnSTDueF.Text = "3" Then
                butAcceptChange.Enabled = False
                dgScanChange.Columns(18).Visible = False
            End If

            ''populate stock take records...
            If ddlDepartment.SelectedValue = "-1" Then
                ''not department selected... no need populate... clear Datagrid
                lblTotRec.Text = "0"
                lblTotScan.Text = "0"
                lblTotMiss.Text = "0"
                lblTotFound.Text = "0"

                dgScanChange.Visible = False
                dgChangeDone.Visible = False
                dgScanNoChange.Visible = False
                dgAssetMiss.Visible = False
                dgAssetFound.Visible = False

            Else
                ''department selected, populate records...

                dgScanChange.Visible = True
                dgChangeDone.Visible = True
                dgScanNoChange.Visible = True
                dgAssetMiss.Visible = True
                dgAssetFound.Visible = True
                ''Get Stocktake Asset Details --> with Changes
                dgScanChange.DataSource = clsOfflineProccess.fnOfflinePro_GetSummaryStocktakeAssetRecords(hdnSTPID.Text, srDeptId, "SC")
                dgScanChange.DataBind()


                ''Get Stocktake Asset Details --> Changes Done
                dgChangeDone.DataSource = clsOfflineProccess.fnOfflinePro_GetSummaryStocktakeAssetRecords(hdnSTPID.Text, srDeptId, "CD")
                dgChangeDone.DataBind()

                ''Get Stocktake Asset Details --> Not Changes
                dgScanNoChange.DataSource = clsOfflineProccess.fnOfflinePro_GetSummaryStocktakeAssetRecords(hdnSTPID.Text, srDeptId, "SN")
                dgScanNoChange.DataBind()

                ''Get Stocktake Asset Details --> with missing
                dgAssetMiss.DataSource = clsOfflineProccess.fnOfflinePro_GetSummaryStocktakeAssetRecords(hdnSTPID.Text, srDeptId, "MI")
                dgAssetMiss.DataBind()

                ''Get Stocktake Asset Details --> with found
                dgAssetFound.DataSource = clsOfflineProccess.fnOfflinePro_GetSummaryStocktakeAssetRecords(hdnSTPID.Text, srDeptId, "FO")
                dgAssetFound.DataBind()


            End If

        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Private Sub dgScanChange_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgScanChange.ItemDataBound
        Dim ddlLoc As DropDownList
        Dim ddlLocSub As DropDownList
        Dim ddlOwner As DropDownList
        Dim textArea As TextBox
        Dim ChkBox As CheckBox
        Dim fld_STDetailID As String

        ddlLoc = CType(e.Item.FindControl("ddlLocation"), DropDownList)
        If Not ddlLoc Is Nothing Then
            fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLoc, "fld_LocationID", "fld_LocationCode", False)
            'ddlLoc.SelectedValue = e.Item.Cells(14).Text
        End If

        'fld_STDetailID = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "fld_STDetailID").ToString())
        'fld_STDetailID = Convert.ToInt32(IIf(DataBinder.Eval(e.Item.DataItem, "fld_STDetailID").ToString() = "", "0", DataBinder.Eval(e.Item.DataItem, "fld_STDetailID").ToString()))
        'fld_STDetailID = (DataBinder.Eval(DataBinder.GetDataItem(e.Item), "fld_STDetailID").ToString())

        Try
            fld_STDetailID = DataBinder.Eval(e.Item.DataItem, "fld_STDetailID")
        Catch ex As Exception
            fld_STDetailID = "0"
        End Try

        ddlLocSub = CType(e.Item.FindControl("ddlLocSub"), DropDownList)
        If Not ddlLocSub Is Nothing Then
            If e.Item.Cells(14).Text = "0" Then
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            Else
                fnPopulateDropDownList(clsLocation.fnSubLocation_SelectByfld_STDetailID(fld_STDetailID), ddlLocSub, "fld_LocSubID", "fld_LocSubCode", False)
                'fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLoc.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCode", False)
                'If e.Item.Cells(19).Text <> "0" Then
                '    ddlLocSub.SelectedValue = e.Item.Cells(19).Text
                'End If
            End If
        End If

        ddlOwner = CType(e.Item.FindControl("ddlOwner"), DropDownList)
        If Not ddlOwner Is Nothing Then
            fnPopulateDropDownList(ClsUser.fnUsrGetUsrFromADDR("OU", "9999", "", ""), ddlOwner, "fld_ADLoginID", "fld_ADUsrName", False)
            ddlOwner.SelectedValue = e.Item.Cells(15).Text
        End If

        textArea = CType(e.Item.FindControl("txtRemark"), TextBox)
        If Not textArea Is Nothing Then
            If Trim(e.Item.Cells(16).Text) = "&nbsp;" Then
                textArea.Text = ""
            Else
                textArea.Text = Trim(e.Item.Cells(16).Text)
            End If
        End If


        ''Check if Non-Admin -> Not allow update Asset, just allow update Inventory (Location and remark)
        If Session("AdminF") = "N" Then
            If e.Item.Cells(3).Text = "Fixed Asset" Then
                If Not ddlLoc Is Nothing Then ddlLoc.Enabled = False
                If Not ddlLocSub Is Nothing Then ddlLocSub.Enabled = False
                If Not ddlOwner Is Nothing Then ddlOwner.Enabled = False
                If Not textArea Is Nothing Then textArea.Enabled = False

                ChkBox = CType(e.Item.FindControl("DeleteThis"), CheckBox)
                If Not ChkBox Is Nothing Then
                    ChkBox.Enabled = False
                    ChkBox.Visible = False
                End If
            Else
                If Not ddlOwner Is Nothing Then ddlOwner.Enabled = False
            End If
        End If

        ''Check whether Date is due--> not allow changes
        If hdnSTDueF.Text = "3" Then
            If Not ddlLoc Is Nothing Then ddlLoc.Enabled = False
            If Not ddlLocSub Is Nothing Then ddlLocSub.Enabled = False
            If Not ddlOwner Is Nothing Then ddlOwner.Enabled = False
            If Not textArea Is Nothing Then textArea.Enabled = False

            ChkBox = CType(e.Item.FindControl("DeleteThis"), CheckBox)
            If Not ChkBox Is Nothing Then
                ChkBox.Enabled = False
                ChkBox.Visible = False
            End If
        End If
    End Sub

    Protected Sub ddlDepartment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlDepartment.SelectedIndexChanged
        Try
            fnGetRecordsFromDB()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butAcceptChange_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAcceptChange.Click
        Try
            ''validate records
            Dim strErrorF As String = "N"
            Dim strMsg As String = ""
            fnValSelectRecord(strErrorF, strMsg)
            If strErrorF = "Y" Then '' error found...
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
                Exit Sub
            End If

            ''** Create Datatable --> Get Datatable
            Dim TblAsset As DataTable = fnCreateDataTableAsset()
            Dim RowAsset As DataRow

            ''** Get Data
            Dim GridItem As DataGridItem
            Dim chkSelected As CheckBox
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim ddlOwnerID As DropDownList
            Dim tbRemark As TextBox
            For Each GridItem In dgScanChange.Items
                chkSelected = CType(GridItem.Cells(18).FindControl("DeleteThis"), CheckBox)
                If chkSelected.Checked Then
                    
                    ddlLocID = CType(GridItem.Cells(10).FindControl("ddlLocation"), DropDownList)
                    If (ddlLocID.SelectedValue = -1) Then
                        lblErrorMessage.Text = "Please choose one of Location."
                        lblErrorMessage.Visible = True
                        Exit Sub
                    Else
                        lblErrorMessage.Visible = False
                        lblErrorMessage.Text = ""
                    End If

                    ddlLocSubID = CType(GridItem.Cells(11).FindControl("ddlLocSub"), DropDownList)
                    If (ddlLocSubID.SelectedValue = -1) Then
                        lblErrorMessage.Text = "Please choose one of Sub Location."
                        lblErrorMessage.Visible = True
                        Exit Sub
                    Else
                        lblErrorMessage.Visible = False
                        lblErrorMessage.Text = ""
                    End If

                    ddlOwnerID = CType(GridItem.Cells(12).FindControl("ddlOwner"), DropDownList)
                    'If (Trim(ddlOwnerID.SelectedValue) = "-1") Then
                    '    lblErrorMessage.Text = "Please choose one of Assigned Owner."
                    '    lblErrorMessage.Visible = True
                    '    Exit Sub
                    'Else
                    '    lblErrorMessage.Visible = False
                    '    lblErrorMessage.Text = ""
                    'End If

                    tbRemark = CType(GridItem.Cells(13).FindControl("txtRemark"), TextBox)

                    ''Add New Row to Datatable
                    RowAsset = TblAsset.NewRow()        'declaring a new row
                    RowAsset.Item("dt_AssetID") = GridItem.Cells(0).Text
                    RowAsset.Item("dt_DeptID") = GridItem.Cells(14).Text.ToString().Replace("&nbsp;", "")
                    RowAsset.Item("dt_LocID") = IIf(ddlLocID.SelectedValue = "-1", "0", ddlLocID.SelectedValue)
                    RowAsset.Item("dt_LocSubID") = IIf(ddlLocSubID.SelectedValue = "-1", "0", ddlLocSubID.SelectedValue)
                    RowAsset.Item("dt_OwnerID") = IIf(ddlOwnerID.SelectedValue = "-1", "", ddlOwnerID.SelectedValue)
                    RowAsset.Item("dt_ARemarks") = Trim(tbRemark.Text)
                    RowAsset.Item("dt_STDetailID") = GridItem.Cells(19).Text
                    TblAsset.Rows.Add(RowAsset)
                End If
            Next

            ''** Get Asset in XML
            Dim ds As New DataSet
            Dim strAssetXML As String = ""
            ds = New DataSet         'creating a dataset
            ds.Tables.Add(TblAsset)   'assign datatable to dataset
            If Not ds Is Nothing Then
                Dim loCol As DataColumn
                'Prepare XML output from the DataSet as a string
                For Each loCol In ds.Tables(0).Columns
                    loCol.ColumnMapping = System.Data.MappingType.Attribute
                Next
                strAssetXML = ds.GetXml
            End If

            ''Update Record in DB
            If strAssetXML <> "" Then
                Dim intRetVal As Integer
                intRetVal = clsOfflineProccess.fnOfflineSummary_UpdateRec(strAssetXML, Session("UsrID"))
                If intRetVal > 0 Then
                    strMsg = "Stocktake record(s) updated succesfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Stocktake record(s) Updated")

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "document.location.href='MovementUpdate_MainDetail.aspx?STID=" + hdnSTPID.Text + "';" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    strMsg = "Stocktake record(s) update Failed. Please contact your Administrator."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnValSelectRecord(ByRef strErrorF As String, ByRef strErrorMsg As String)
        Try
            strErrorF = "N"
            strErrorMsg = ""

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim intRow As Integer = 0

            For Each GridItem In dgScanChange.Items
                intRow = intRow + 1

                chkSelectedRec = CType(GridItem.Cells(18).FindControl("DeleteThis"), CheckBox)
                If chkSelectedRec.Checked Then
                    ddlLocID = CType(GridItem.Cells(10).FindControl("ddlLocation"), DropDownList)
                    ddlLocSubID = CType(GridItem.Cells(11).FindControl("ddlLocSub"), DropDownList)
                    If ddlLocID.SelectedValue <> "-1" And ddlLocSubID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Sub Location (Record " & intRow & ")."
                        Exit For
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnCreateDataTableAsset() As DataTable
        Try
            ''****1. creating a table named TblAsset
            Dim TblAsset As DataTable
            TblAsset = New DataTable("TblAsset")

            ''Column 1: Asset id
            Dim dt_AssetID As DataColumn = New DataColumn("dt_AssetID")    'declaring a column named Name
            dt_AssetID.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            TblAsset.Columns.Add(dt_AssetID)                               'adding the column to table

            ' ''Column 2: Dept ID 
            Dim dt_DeptID As DataColumn = New DataColumn("dt_DeptID")      'declaring a column named Name
            dt_DeptID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_DeptID)                                    'adding the column to table

            ''Column 3: Loc ID 
            Dim dt_LocID As DataColumn = New DataColumn("dt_LocID")      'declaring a column named Name
            dt_LocID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_LocID)                                    'adding the column to table

            ''Column 3a: Sub Loc ID 
            Dim dt_LocSubID As DataColumn = New DataColumn("dt_LocSubID")      'declaring a column named Name
            dt_LocSubID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_LocSubID)

            ''Column 4: Owner ID 
            Dim dt_OwnerID As DataColumn = New DataColumn("dt_OwnerID")      'declaring a column named Name
            dt_OwnerID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_OwnerID)

            ''Column 5: Asset Remarks
            Dim dt_ARemarks As DataColumn = New DataColumn("dt_ARemarks")      'declaring a column named Name
            dt_ARemarks.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_ARemarks)                                    'adding the column to table

            ''Column 5: STDetailsID
            Dim dt_STDetailID As DataColumn = New DataColumn("dt_STDetailID")      'declaring a column named Name
            dt_STDetailID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_STDetailID)                                    'adding the column to table

            ''Set Primary Keys
            '--> Make the ID column the primary key column.
            Dim PrimaryKeyColumns(0) As DataColumn
            PrimaryKeyColumns(0) = TblAsset.Columns("dt_AssetID")
            TblAsset.PrimaryKey = PrimaryKeyColumns

            ''return dataTable 
            fnCreateDataTableAsset = TblAsset
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
    '    Try
    '        Dim ddllist As DropDownList = CType(sender, DropDownList)
    '        Dim cell As TableCell = CType(ddllist.Parent, TableCell)
    '        Dim item As DataGridItem = CType(cell.Parent, DataGridItem)
    '        Dim ddlLoc As DropDownList = CType(item.Cells(9).FindControl("ddlLocation"), DropDownList)
    '        Dim ddlLocSub As DropDownList = CType(item.Cells(10).FindControl("ddlLocSub"), DropDownList)

    '        'Dim strddlID As String = sender.clientid.ToString
    '        'Dim strddlNo As String = strddlID.Substring(Len(strddlID) - 3, 3)
    '        'Dim ddlLocation As DropDownList = CType(item.Cells(7).FindControl("ddlLocADD" & strddlNo), DropDownList)
    '        'Dim ddlLocSub As DropDownList = CType(item.Cells(7).FindControl("ddlLocSubADD" & strddlNo), DropDownList)

    '        If ddlLoc.SelectedValue <> "-1" Then
    '            fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLoc.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCode", False)
    '        Else
    '            ddlLocSub.Items.Clear()
    '            ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
    '        End If
    '    Catch ex As Exception
    '        lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
    '        lblErrorMessage.Visible = True
    '    End Try
    'End Sub

    Protected Sub ddlLocSub_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim ddllist As DropDownList = CType(sender, DropDownList)
            Dim cell As TableCell = CType(ddllist.Parent, TableCell)
            Dim item As DataGridItem = CType(cell.Parent, DataGridItem)
            Dim ddlLoc As DropDownList = CType(item.Cells(9).FindControl("ddlLocation"), DropDownList)
            Dim ddlLocSub As DropDownList = CType(item.Cells(10).FindControl("ddlLocSub"), DropDownList)

            'Dim strddlID As String = sender.clientid.ToString
            'Dim strddlNo As String = strddlID.Substring(Len(strddlID) - 3, 3)
            'Dim ddlLocation As DropDownList = CType(item.Cells(7).FindControl("ddlLocADD" & strddlNo), DropDownList)
            'Dim ddlLocSub As DropDownList = CType(item.Cells(7).FindControl("ddlLocSubADD" & strddlNo), DropDownList)

            'If ddlLoc.SelectedValue <> "-1" Then
            '    fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLoc.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCode", False)
            'Else
            '    ddlLocSub.Items.Clear()
            '    ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            'End If

            If ddlLocSub.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(0, ddlLocSub.SelectedValue, "S"), ddlLoc, "fld_LocationID", "fld_LocationCode", False)
                If (ddlLoc.Items.Count > 0) Then
                    ddlLoc.SelectedIndex = 1
                End If
            Else
                ddlLoc.Items.Clear()
                ddlLoc.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butPrint_Click(sender As Object, e As System.EventArgs) Handles butPrint.Click
        Dim strJavaScript As String
        Dim stocktakefor As String = Me.ddlDepartment.SelectedItem.Text
        Dim stocktakeStart As String = Me.lblSTSDate.Text
        Dim stocktakeEnd As String = Me.lblSTTDate.Text
        Dim TotalAsset As String = lblTotRec.Text
        Dim totalScan As String = lblTotScan.Text
        Dim totalMissing As String = lblTotMiss.Text

        Session("hdnSTPID") = hdnSTPID.Text
        Session("srDeptId") = Me.ddlDepartment.SelectedValue

        strJavaScript = "<script language = 'Javascript'>" & _
                        "window.open('MovementUpdate_ExpRpt.aspx?stocktakefor=" & CStr(stocktakefor) _
                        & "&stocktakeStart=" & stocktakeStart & "&stocktakeEnd=" & stocktakeEnd _
                        & "&TotalAsset=" & TotalAsset & "&totalScan=" & totalScan & _
                        "&totalMissing=" & totalMissing & "', 'Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1,menubar=1'); " & _
                        "</script>"
        Response.Write(strJavaScript)
    End Sub

    Private Sub butExport_Click(sender As Object, e As System.EventArgs) Handles butExport.Click
        Dim strJavaScript As String
        Dim stocktakefor As String = Me.ddlDepartment.SelectedItem.Text
        Dim stocktakeStart As String = Me.lblSTSDate.Text
        Dim stocktakeEnd As String = Me.lblSTTDate.Text
        Dim TotalAsset As String = lblTotRec.Text
        Dim totalScan As String = lblTotScan.Text
        Dim totalMissing As String = lblTotMiss.Text

        Session("hdnSTPID") = hdnSTPID.Text
        Session("srDeptId") = Me.ddlDepartment.SelectedValue

        strJavaScript = "<script language = 'Javascript'>" & _
                        "window.open('MovementUpdate_ExpRpt.aspx?CallType=EE&stocktakefor=" & clsEncryptDecrypt.EncryptText(CStr(stocktakefor)) _
                        & "&stocktakeStart=" & stocktakeStart & "&stocktakeEnd=" & stocktakeEnd _
                        & "&TotalAsset=" & TotalAsset & "&totalScan=" & totalScan & _
                        "&totalMissing=" & totalMissing & "', 'Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1,menubar=1'); " & _
                        "</script>"
        Response.Write(strJavaScript)
    End Sub

    Private Sub ButExportWord_Click(sender As Object, e As System.EventArgs) Handles ButExportWord.Click
        Dim strJavaScript As String
        Dim stocktakefor As String = Me.ddlDepartment.SelectedItem.Text
        Dim stocktakeStart As String = Me.lblSTSDate.Text
        Dim stocktakeEnd As String = Me.lblSTTDate.Text
        Dim TotalAsset As String = lblTotRec.Text
        Dim totalScan As String = lblTotScan.Text
        Dim totalMissing As String = lblTotMiss.Text

        Session("hdnSTPID") = hdnSTPID.Text
        Session("srDeptId") = Me.ddlDepartment.SelectedValue

        strJavaScript = "<script language = 'Javascript'>" & _
                        "window.open('MovementUpdate_ExpRpt.aspx?CallType=EW&stocktakefor=" & clsEncryptDecrypt.EncryptText(CStr(stocktakefor)) _
                        & "&stocktakeStart=" & stocktakeStart & "&stocktakeEnd=" & stocktakeEnd _
                        & "&TotalAsset=" & TotalAsset & "&totalScan=" & totalScan & _
                        "&totalMissing=" & totalMissing & "', 'Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1,menubar=1'); " & _
                        "</script>"
        Response.Write(strJavaScript)
    End Sub
End Class