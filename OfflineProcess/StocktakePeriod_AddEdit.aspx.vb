#Region "Information Section"
' ****************************************************************************************************
' Description       : Add/Edit Stocktake Period  
' Purpose           : Add/Edit Stocktake Period  
' Author            : See Siew
' Date              : 24/11/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class StocktakePeriod_AddEdit
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")

                ''Get Pass Vlaue from previous screen 
                hdnCurrentDate.Value = mfnGetCurrentDate()
                hdnSTPeriodID.Value = Request("STPeriodID")
                hdnSTAddEditF.Value = Request("AddEditF")   '"A" = Add, "E" = Edit

                ''if Call from "E"dit, retrieve Record
                If hdnSTAddEditF.Value = "E" Then
                    ''Retrieve Records 
                    fnGetRecordsFromDB()

                    ''Assign Header
                    lblSTHeader.Text = "Edit"
                Else
                    ''Assign Header
                    lblSTHeader.Text = "Add New"
                End If

            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnGetRecordsFromDB()
        Dim objRdr As SqlDataReader
        Try
            ''**Get Record
            objRdr = clsOfflineProccess.fnOfflineProPeriod_GetSTPeriodForEdit(hdnSTPeriodID.Value)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()
                    txtSTFrmDt.Text = CStr(objRdr("fld_STStartDt"))
                    txtSTToDt.Text = CStr(objRdr("fld_STEndDt"))
                    txtRemarks.Text = CStr(objRdr("fld_STRemark"))
                    hdnChangeF.Value = CStr(objRdr("STChangeF"))

                    hdnSTFrmDt.Value = CStr(objRdr("fld_STStartDt"))
                    hdnSTToDt.Value = CStr(objRdr("fld_STEndDt"))
                End If
            End If
            objRdr.Close()

            ''**Check whether allow changing From/To Date
            If hdnChangeF.Value = "2" Then
                '2. Have Stocktake Record and Not Overdue     --> Just allow change End Date	
                txtSTFrmDt.Enabled = False
                linkFrmDt.Visible = False

            ElseIf hdnChangeF.Value = "3" Then
                '3. Have Stocktake Record and Overdue         --> Not allow change Start/End Date	
                txtSTFrmDt.Enabled = False
                txtSTToDt.Enabled = False
                linkFrmDt.Visible = False
                linkToDt.Visible = False

            End If

        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Protected Sub ButCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButCancel.Click
        Server.Transfer("StocktakePeriod_view.aspx")
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            Server.Transfer("StocktakePeriod_AddEdit.aspx?AddEditF=" & hdnSTAddEditF.Value & "&STPeriodID=" & hdnSTPeriodID.Value)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim bDupST As Boolean = False
            Dim intRetVal As Integer
            Dim strMsg As String

            ''** Check for Duplicate 
            bDupST = clsOfflineProccess.fnOfflineProPeriod_CheckForDuplicate(hdnSTPeriodID.Value, txtSTFrmDt.Text, txtSTToDt.Text, hdnSTAddEditF.Value)

            If Not bDupST Then ''not duplicate record
                ''Edit record
                intRetVal = clsOfflineProccess.fnOfflineProPeriod_InsertUpdateDelete( _
                                    hdnSTPeriodID.Value, txtSTFrmDt.Text, txtSTToDt.Text, txtRemarks.Text, _
                                    Session("UsrID"), hdnSTAddEditF.Value)

                If intRetVal > 0 Then
                    strMsg = "Stocktake Exercise " & IIf(hdnSTAddEditF.Value = "I", "Add", "Update") & " Successfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Stocktake Exercise " + IIf(hdnSTAddEditF.Value = "I", "created", "updated") + " : From " + txtSTFrmDt.Text + " To " + txtSTToDt.Text)

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "document.location.href='StocktakePeriod_view.aspx';" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    strMsg = "Stocktake Exercise " & IIf(hdnSTAddEditF.Value = "I", "Add", "Update") & " Failed."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If

            Else  ''duplicate record
                lblErrorMessage.Text = "Stocktake Period already exist. Please choose another Stocktake Period."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class