<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="StocktakePeriod_AddEdit.aspx.vb" Inherits="AMS.StocktakePeriod_AddEdit" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<%@ Register TagPrefix="fluent" Namespace="Fluent.MultiLineTextBoxValidator" Assembly="Fluent.MultiLineTextBoxValidator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script type="text/javascript">
	    function chkFrm() {
			var foundError = false;
		    
		    //validate From date
			if (!foundError && gfnCheckDate(document.frm_Stocktake.txtSTFrmDt, "Stocktake Period From Date", "M") == false) {
				foundError=true
			}
			
			if (document.frm_Stocktake.hdnSTAddEditF.value == "I" || (document.frm_Stocktake.hdnSTAddEditF.value == "E" && document.frm_Stocktake.txtSTFrmDt.value != document.frm_Stocktake.hdnSTFrmDt.value)){
			    if (!foundError && gfnCheckFrmToDt(document.frm_Stocktake.hdnCurrentDate,document.frm_Stocktake.txtSTFrmDt,'>') == false) {
				    foundError=true
				    document.frm_Stocktake.txtSTFrmDt.focus()
				    alert("Stocktake Period From Date should be greater than current Date.");
			    }
			}
			
			//validate To date
			if (!foundError && gfnCheckDate(document.frm_Stocktake.txtSTToDt, "Stocktake Period To Date", "M") == false) {
				foundError=true
			}
	        if (document.frm_Stocktake.hdnSTAddEditF.value == "I" || (document.frm_Stocktake.hdnSTAddEditF.value == "E" && document.frm_Stocktake.txtSTToDt.value != document.frm_Stocktake.hdnSTToDt.value)){
			    if (!foundError && gfnCheckFrmToDt(document.frm_Stocktake.hdnCurrentDate,document.frm_Stocktake.txtSTToDt,'>') == false) {
				    foundError=true
				    document.frm_Stocktake.txtSTToDt.focus()
				    alert("Stocktake Period To Date should be greater than current Date.");
			    }
			}
			 
 			if (!foundError){
 			    var flag = false;
 			    if (document.frm_Stocktake.hdnSTAddEditF.value == 'I'){
 			        flag = window.confirm("Are you sure you want to add this Stocktake Exercise?");
 			    }
 			    else {
 			        flag = window.confirm("Are you sure you want to update this Stocktake Exercise?");
 			    }
 				
 				return flag;
 			}
			else
				return false;
		}
	</script>
</head>
<body>
    <form id="frm_Stocktake" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Offline Processing : <asp:label ID="lblSTHeader" runat= server></asp:label> Stocktake Exercise</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table id="Table2" height="400" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<!--Start: Main Content-->
						<table id="Table3" height="400" cellSpacing="1" cellPadding="0" width="100%" align="left"
							bgColor="silver" border="0">
							<tr>
								<td>
									<table id="Table4" height="400" cellSpacing="0" cellPadding="0" width="100%" align="left"
										bgColor="white" border="0">
										<tr>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
													<TR>
														<TD vAlign="top" align="center" width="100%">
														    <asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE id="Table6" height="126" cellSpacing="0" cellPadding="3" width="100%" align="left"
																bgColor="white" border="0">
																<TBODY align=left>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" height="352">
																			<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
																				<TBODY>
																					<TR>
																						<TD vAlign="middle" width="35%" align=left>
																						    <FONT color="#666666"><font color="red">*</font>Stocktake Period : </FONT>
																						</TD>
																						<TD width="65%">
																						    From <asp:TextBox ID="txtSTFrmDt" Width="110" Runat="server"></asp:TextBox>
																				            <a id=linkFrmDt runat=server href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.frm_Stocktake.txtSTFrmDt, document.frm_Stocktake.txtSTToDt);return false;"
								                                                                HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
							                                                                </a>
																						    
									                                                        &nbsp;&nbsp;&nbsp;&nbsp;
									                                                        To <asp:TextBox ID="txtSTToDt" Width="110" Runat="server"></asp:TextBox>
								                                                            <a id=linkToDt runat=server href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.frm_Stocktake.txtSTFrmDt, document.frm_Stocktake.txtSTToDt);return false;"
									                                                            HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
								                                                            </a>
									                                                         									                                                       
																					    </TD>
																					</TR>
																					<TR>
																						<TD vAlign=top width="35%" align=left>
																						    <FONT color="#666666">Remarks : </FONT>
																						</TD>
																						<TD>
																					        <asp:TextBox id="txtRemarks" maxlength="1000" TextMode="MultiLine" Columns="40" Rows="5" Runat="server"></asp:TextBox><br />
																					        <asp:textbox id="txtRemarksWord" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					        <fluent:multilinetextboxvalidator id="MLLValRemarks" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Remarks to 1000 Characters." OutputControl="txtRemarksWord" ControlToValidate="txtRemarks"></fluent:multilinetextboxvalidator>
																					    </TD>
																					</TR>
																					<TR>
																						<TD align="center" colSpan="2"><BR>
																							<asp:button id="butSubmit" Runat="Server" Text="Submit"></asp:button>&nbsp;
																							<asp:button id="butReset" Runat="Server" Text="Reset"></asp:button>&nbsp;
																							<asp:button id="ButCancel" Runat="Server" Text="Cancel"></asp:button></TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start: Hidden Fields -->
			<!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
				src="../common/DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="Z-INDEX:101; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px">
			</iframe>
			<input type="hidden" id="hdnSTAddEditF" runat="server"> 
			<input type="hidden" id="hdnSTPeriodID" runat="server">  
			<input type="hidden" id="hdnSTFrmDt" runat="server"> 
			<input type="hidden" id="hdnSTToDt" runat="server"> 
			<input type="hidden" id="hdnChangeF" runat="server">  
			<input type="hidden" id="hdnCurrentDate" runat="server">
			<!-- End  : Hidden Fields -->
			<tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer>
		</form>
</body>
</html>
