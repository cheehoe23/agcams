<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MovementUpdate_MainDetail.aspx.vb" Inherits="AMS.MovementUpdate_MainDetail" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
    <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
    <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />
        <style type="text/css">
    .modal
    {
        position: absolute; 
        top: 50%; 
        left: 50%;
        z-index : 50%;
        width: 100%; height: 100%; overflow: visible        
        opacity: 0.8;
        filter: alpha(opacity=80);
        -moz-opacity: 0.8;       
    }
    .loading
    {
        font-family: Arial;
        font-size: 10pt;       
        position: fixed;
        background-color: White;
        z-index: 999;
        x-index: 150;
        y-index: 150;
        left: 500;
    }
</style>
	<script type="text/javascript">
	    function ShowProgress() {
	        setTimeout(function () {
	            var modal = $('<div />');
	            modal.addClass("modal");
	            $('body').append(modal);
	            var loading = $(".loading");
	            loading.show();
	            var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
	            var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
	            loading.css({ top: top, left: left });
	        }, 200);
	    }

	    function chkFrm() {
			var foundError = false;
			            			
			//at least 1 asset record selected for Accept Changes
			if (!foundError && !gfnCheckSelect("At least one record should be selected.")){
				foundError=true
			}
											 
 			if (!foundError){
 			    var flag = false;
 				flag = window.confirm("Are you sure you want to update selected Asset(s)?");
 				return flag;
 			}
			else
				return false;
		}
	</script>
</head>
<body>
    <form id="Frm_StockTake" method="post" runat="server">
			<!-- Start: header -->
            <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ScriptMode="Release" />
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Offline Processing : Stocktake History <asp:Label ID=lblHeader runat=server></asp:Label></B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																				    <TR>
																					    <TD valign=top width="35%">
																					        <FONT class="DisplayTitle">Stocktake For : </FONT>
																					    </TD>
																					    <TD width="65%">
                                                              
							<asp:DropDownList ID="ddlDepartment" Runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"></asp:DropDownList>															    
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Stocktake Start Date : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:Label ID=lblSTSDate runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Stocktake End Date : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:Label ID=lblSTTDate runat=server></asp:Label>
																					    </TD>
																				    </TR>
																                    <TR>
																	                    <TD>
																	                        <FONT class="DisplayTitle">Total Asset(s) : </FONT>
																	                    </TD>
																	                    <TD>
																		                    <asp:Label ID=lblTotRec runat=server></asp:Label>
																	                    </TD>
																                    </TR>
																                    <TR>
																	                    <TD>
																	                        <FONT class="DisplayTitle">Total Scan : </FONT>
																	                    </TD>
																	                    <TD>
																		                    <asp:Label ID=lblTotScan runat=server></asp:Label>
																	                    </TD>
																                    </TR>
																                    <TR>
																	                    <TD>
																	                        <FONT class="DisplayTitle">Total Missing : </FONT>
																	                    </TD>
																	                    <TD>
																		                    <asp:Label ID=lblTotMiss runat=server></asp:Label>
																	                    </TD>
																                    </TR>
																                    <TR style="display:none">
																	                    <TD>
																	                        <FONT class="DisplayTitle">Total Found : </FONT>
																	                    </TD>
																	                    <TD>
																		                    <asp:Label ID=lblTotFound runat=server></asp:Label>
																	                    </TD>
																                    </TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Stocktake Result</b><br /><asp:button id="butExport" Runat="server" Text="Export to Excel"></asp:button>
                                                                                        <asp:button id="ButExportWord" Runat="server" Text="Export to Word"></asp:button> </TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Asset Scan (with Changes)</font></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><asp:button id="butAcceptChange" Runat="server" Text="Accept Changes"></asp:button> <asp:button Visible=false id="butPrint" Runat="server" Text="Print"></asp:button> </TD>
																					</TR>
																					<tr>
								                                                        <td vAlign="top" align="center" colspan=2>
									                                                        <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgScanChange" Runat="server" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
                                                                                                    <asp:boundcolumn datafield="fld_AssetDesc" headertext="Description" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Department" datafield="fld_DepartmentName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Location" datafield="fld_LocationName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Sub Location" datafield="fld_LocSubName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Assigned Owner" datafield="fld_OwnerName" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Remarks" datafield="fld_AssetRemarks" Visible=false >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:TemplateColumn HeaderText="(Stocktake Result) Location">
                                                                                                        <ItemTemplate>
                                                                                                            <%--<asp:DropDownList ID="ddlLocation" runat="server" Width="100px" Enabled="false" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>--%>
                                                                                                            <asp:DropDownList ID="ddlLocation" runat="server" Width="100px" Enabled="false" AutoPostBack="True"></asp:DropDownList>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:TemplateColumn HeaderText="(Stocktake Result) Sub Location">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:DropDownList ID="ddlLocSub" runat="server" Width="100px" OnSelectedIndexChanged="ddlLocSub_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:TemplateColumn HeaderText="(Stocktake Result) Assigned Owner">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:DropDownList ID="ddlOwner" runat="server"></asp:DropDownList>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:TemplateColumn HeaderText="(Stocktake Result) Remarks">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox id="txtRemark" TextMode=MultiLine maxlength="1000" Columns="40" Rows="5" Runat="server"></asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:boundcolumn headertext="(Found) Department" datafield="fld_newDeptId" Visible=false >
									                                                                    <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                                    </asp:boundcolumn>
								                                                                   <asp:boundcolumn headertext="(Found) Location" datafield="fld_newLocID" Visible=false >
									                                                                    <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                                    </asp:boundcolumn>
								                                                                    <asp:boundcolumn headertext="(Found) Assigned Owner" datafield="fld_newOwnerID" Visible=false >
									                                                                    <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                                    </asp:boundcolumn>
								                                                                    <asp:boundcolumn headertext="(Found) Remarks" datafield="fld_NewRemark" Visible=false >
									                                                                    <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                                    </asp:boundcolumn>	
								                                                                    <asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="10%">
									                                                                    <headertemplate>
										                                                                    <asp:checkbox id="CheckAll" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
											                                                                    runat="server" />
									                                                                    </headertemplate>
									                                                                    <itemtemplate>
										                                                                    <center>
											                                                                    <asp:checkbox id="DeleteThis" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
												                                                                    runat="server" />
										                                                                    </center>
									                                                                    </itemtemplate>
								                                                                    </asp:templatecolumn>
								                                                                    <asp:boundcolumn HeaderText="fld_STDetailID" datafield="fld_STDetailID" Visible=false>
									                                                                    <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
								                                                                    </asp:boundcolumn>
                                                                                                    <%--<asp:TemplateColumn HeaderText="">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblSTDetailID" runat="server" Text='<%# Eval("fld_STDetailID") %>'></asp:Label>                                                                                                            
                                                                                                        </ItemTemplate>
                                                                                                        <EditItemTemplate>
                                                                                                            <asp:Label ID="lblSTDetailID" runat="server" Text='<%# Eval("fld_STDetailID") %>'></asp:Label>                                                                                                            
                                                                                                        </EditItemTemplate>
                                                                                                    </asp:TemplateColumn>--%>
								                                                                    <asp:boundcolumn headertext="(Found) Sub Location" datafield="fld_newLocSubID" Visible=false >
									                                                                    <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                                    </asp:boundcolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
								                                                        </td>
							                                                        </tr>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Asset Scan (Changes Done)</font></TD>
																					</TR>
																					<tr>
								                                                        <td vAlign="top" align="center" colspan=2>
									                                                        <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgChangeDone" Runat="server" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
                                                                                                    <asp:boundcolumn datafield="fld_AssetDesc" headertext="Description" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Department" datafield="fld_OrigDept" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Location" datafield="fld_OrigLoc" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Sub Location" datafield="fld_OrigLocSub" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Assigned Owner" datafield="fld_OrigOwner" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Remarks" datafield="fld_OrigRemark"  Visible=false>
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Location" datafield="fld_ChgLoc" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Sub Location" datafield="fld_ChgLocSub" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Assigned Owner" datafield="fld_ChgOwner" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Remarks" datafield="fld_ChgRemark" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
								                                                        </td>
							                                                        </tr>
							                                                        <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Asset Scan (without Changes)</font></TD>
																					</TR>
																					<tr>
								                                                        <td vAlign="top" align="center" colspan=2>
									                                                        <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgScanNoChange" Runat="server" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
                                                                                                    <asp:boundcolumn datafield="fld_AssetDesc" headertext="Description" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Department" datafield="fld_OrigDept" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Location" datafield="fld_OrigLoc" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Sub Location" datafield="fld_OrigLocSub" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Assigned Owner" datafield="fld_OrigOwner" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Remarks" datafield="fld_OrigRemark"  Visible=false>
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Location" datafield="fld_NewLoc" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Sub Location" datafield="fld_NewLocSub" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Assigned Owner" datafield="fld_NewOwner" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Remarks" datafield="fld_NewRemark" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
								                                                        </td>
							                                                        </tr>
							                                                        <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Asset Missing</font></TD>
																					</TR>
																					<tr>
								                                                        <td vAlign="top" align="center" colspan=2>
									                                                        <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgAssetMiss" Runat="server" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
                                                                                                    <asp:boundcolumn datafield="fld_AssetDesc" headertext="Description" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                         <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Department" datafield="fld_OrigDept" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Location" datafield="fld_OrigLoc" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Sub Location" datafield="fld_OrigLocSub" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Assigned Owner" datafield="fld_OrigOwner" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Remarks" datafield="fld_OrigRemark"  Visible=false>
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>					
										                                                        </Columns>
									                                                        </asp:datagrid>
								                                                        </td>
							                                                        </tr>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR style="display:none">
																						<TD colSpan="2"><font class="DisplayTitleHeader">Asset Found</font></TD>
																					</TR>
																					<tr style="display:none">
								                                                        <td vAlign="top" align="center" colspan=2>
									                                                        <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgAssetFound" Runat="server" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
                                                                                                    <asp:boundcolumn datafield="fld_AssetDesc" headertext="Description" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                         <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Department" datafield="fld_NewDept" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Location" datafield="fld_NewLoc" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Sub Location" datafield="fld_NewLocSub" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Assigned Owner" datafield="fld_NewOwner" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Remarks" datafield="fld_NewRemark" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>					
										                                                        </Columns>
									                                                        </asp:datagrid>
								                                                        </td>
							                                                        </tr>
							                                                        
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
                        
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start : Hidden Fields -->
			<asp:TextBox Visible="False" ID="hdnSTPID" Runat="server"></asp:TextBox>
			<asp:TextBox Visible="False" ID="hdnSTDueF" Runat="server"></asp:TextBox>
			<!-- End   : Hidden Fields -->
			<tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer>
	</form>
</body>
</html>
