#Region "Information Section"
' ****************************************************************************************************
' Description       : View Missing & Found 
' Purpose           : View Missing & Found 
' Author            : See Siew
' Date              : 22/11/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class MissFound_view
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butAcceptChange.Attributes.Add("OnClick", "return chkFrm()")

                ''Display Record
                fnPopulateRecords()

                ''Check whether is in Stocktake Period
                fnCheckStocktakePeriod()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnCheckStocktakePeriod()
        Try
            Dim strStocktakeF As String = "N"
            strStocktakeF = clsOfflineProccess.fnOfflinePro_CheckStocktakePeriod()

            ''If not in stocktake period, not allow to do any action
            If strStocktakeF = "N" Then
                butAcceptChange.Enabled = False
                dgAssetMissFound.Enabled = False

                lblErrorMessage.Text = "Not in Stocktake Period"
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            ''Get Records
            dgAssetMissFound.DataSource = clsOfflineProccess.fnOfflineProMissFound_GetStocktakeAssetRecords()
            dgAssetMissFound.DataBind()
            If Not dgAssetMissFound.Items.Count > 0 Then ''Not Records found
                butAcceptChange.Enabled = False
                lblErrorMessage.Text = "There is no record."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub dgAssetMissFound_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAssetMissFound.ItemDataBound
        Dim ddlDept As DropDownList
        Dim ddlLoc As DropDownList
        Dim ddlLocSub As DropDownList
        Dim ddlOwner As DropDownList
        Dim textArea As TextBox
        Dim ChkBox As CheckBox

        ddlDept = CType(e.Item.FindControl("ddlDepartment"), DropDownList)
        If Not ddlDept Is Nothing Then
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), "Y"), ddlDept, "fld_DepartmentID", "fld_DepartmentCode", True)
            ddlDept.SelectedValue = e.Item.Cells(14).Text
        End If

        ddlLoc = CType(e.Item.FindControl("ddlLocation"), DropDownList)
        If Not ddlLoc Is Nothing Then
            fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLoc, "fld_LocationID", "fld_LocationCode", False)
            ddlLoc.SelectedValue = e.Item.Cells(15).Text
        End If

        ddlLocSub = CType(e.Item.FindControl("ddlLocSub"), DropDownList)
        If Not ddlLocSub Is Nothing Then
            If e.Item.Cells(15).Text = "0" Then
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            Else
                fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLoc.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCode", False)
                If e.Item.Cells(21).Text <> "0" Then
                    ddlLocSub.SelectedValue = e.Item.Cells(21).Text
                End If
            End If
        End If

        ddlOwner = CType(e.Item.FindControl("ddlOwner"), DropDownList)
        If Not ddlOwner Is Nothing Then
            fnPopulateDropDownList(ClsUser.fnUsrGetUsrFromADDR("OU", "9999", "", ""), ddlOwner, "fld_ADLoginID", "fld_ADUsrName", False)
            ddlOwner.SelectedValue = e.Item.Cells(16).Text
        End If

        textArea = CType(e.Item.FindControl("txtRemark"), TextBox)
        If Not textArea Is Nothing Then
            If Trim(e.Item.Cells(17).Text) = "&nbsp;" Then
                textArea.Text = ""
            Else
                textArea.Text = e.Item.Cells(17).Text
            End If
        End If

            ''Check if Non-Admin -> Not allow update Asset, just allow update Inventory (Location and remark)
            If Session("AdminF") = "N" Then
                If e.Item.Cells(3).Text = "Fixed Asset" Then
                    If Not ddlDept Is Nothing Then ddlDept.Enabled = False
                If Not ddlLoc Is Nothing Then ddlLoc.Enabled = False
                If Not ddlLocSub Is Nothing Then ddlLocSub.Enabled = False
                    If Not ddlOwner Is Nothing Then ddlOwner.Enabled = False
                    If Not textArea Is Nothing Then textArea.Enabled = False

                    ChkBox = CType(e.Item.FindControl("DeleteThis"), CheckBox)
                    If Not ChkBox Is Nothing Then
                        ChkBox.Enabled = False
                        ChkBox.Visible = False
                    End If
                Else
                    If Not ddlDept Is Nothing Then ddlDept.Enabled = False
                    If Not ddlOwner Is Nothing Then ddlOwner.Enabled = False
                End If
            End If

    End Sub

    Protected Sub butAcceptChange_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAcceptChange.Click
        Try
            ''validate records
            Dim strErrorF As String = "N"
            Dim strMsg As String = ""
            fnValSelectRecord(strErrorF, strMsg)
            If strErrorF = "Y" Then '' error found...
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
                Exit Sub
            End If

            ''** Create Datatable --> Get Datatable
            Dim TblAsset As DataTable = fnCreateDataTableAsset()
            Dim RowAsset As DataRow

            ''** Get Data
            Dim GridItem As DataGridItem
            Dim chkSelected As CheckBox
            Dim ddlDeptID As DropDownList
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim ddlOwnerID As DropDownList
            Dim tbRemark As TextBox
            For Each GridItem In dgAssetMissFound.Items
                chkSelected = CType(GridItem.Cells(18).FindControl("DeleteThis"), CheckBox)
                If chkSelected.Checked Then
                    ddlDeptID = CType(GridItem.Cells(9).FindControl("ddlDepartment"), DropDownList)
                    ddlLocID = CType(GridItem.Cells(10).FindControl("ddlLocation"), DropDownList)
                    ddlLocSubID = CType(GridItem.Cells(11).FindControl("ddlLocSub"), DropDownList)
                    ddlOwnerID = CType(GridItem.Cells(12).FindControl("ddlOwner"), DropDownList)
                    tbRemark = CType(GridItem.Cells(13).FindControl("txtRemark"), TextBox)

                    ''Add New Row to Datatable
                    RowAsset = TblAsset.NewRow()        'declaring a new row
                    RowAsset.Item("dt_AssetID") = GridItem.Cells(0).Text
                    RowAsset.Item("dt_DeptID") = IIf(ddlDeptID.SelectedValue = "-1", "0", ddlDeptID.SelectedValue)
                    RowAsset.Item("dt_LocID") = IIf(ddlLocID.SelectedValue = "-1", "0", ddlLocID.SelectedValue)
                    RowAsset.Item("dt_LocSubID") = IIf(ddlLocSubID.SelectedValue = "-1", "0", ddlLocSubID.SelectedValue)
                    RowAsset.Item("dt_OwnerID") = IIf(ddlOwnerID.SelectedValue = "-1", "", ddlOwnerID.SelectedValue)
                    RowAsset.Item("dt_ARemarks") = Trim(tbRemark.Text)
                    RowAsset.Item("dt_MSTDetailID") = GridItem.Cells(19).Text
                    RowAsset.Item("dt_FSTDetailID") = GridItem.Cells(20).Text
                    TblAsset.Rows.Add(RowAsset)
                End If
            Next

            ''** Get Asset in XML
            Dim ds As New DataSet
            Dim strAssetXML As String = ""
            ds = New DataSet         'creating a dataset
            ds.Tables.Add(TblAsset)   'assign datatable to dataset
            If Not ds Is Nothing Then
                Dim loCol As DataColumn
                'Prepare XML output from the DataSet as a string
                For Each loCol In ds.Tables(0).Columns
                    loCol.ColumnMapping = System.Data.MappingType.Attribute
                Next
                strAssetXML = ds.GetXml
            End If

            ''Update Record in DB
            If strAssetXML <> "" Then
                Dim intRetVal As Integer
                intRetVal = clsOfflineProccess.fnOfflineProMissFound_UpdateRecord(strAssetXML, Session("UsrID"))
                If intRetVal > 0 Then
                    strMsg = "Missing & Found Asset(s) updated succesfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Missing & Found Asset Updated")

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "document.location.href='MissFound_view.aspx';" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    strMsg = "Record(s) update Failed. Please contact your Administrator."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnValSelectRecord(ByRef strErrorF As String, ByRef strErrorMsg As String)
        Try
            strErrorF = "N"
            strErrorMsg = ""

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim intRow As Integer = 0

            For Each GridItem In dgAssetMissFound.Items
                intRow = intRow + 1

                chkSelectedRec = CType(GridItem.Cells(18).FindControl("DeleteThis"), CheckBox)
                If chkSelectedRec.Checked Then
                    ddlLocID = CType(GridItem.Cells(10).FindControl("ddlLocation"), DropDownList)
                    ddlLocSubID = CType(GridItem.Cells(11).FindControl("ddlLocSub"), DropDownList)
                    If ddlLocID.SelectedValue <> "-1" And ddlLocSubID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Sub Location (Record " & intRow & ")."
                        Exit For
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnCreateDataTableAsset() As DataTable
        Try
            ''****1. creating a table named TblAsset
            Dim TblAsset As DataTable
            TblAsset = New DataTable("TblAsset")

            ''Column 1: Asset id
            Dim dt_AssetID As DataColumn = New DataColumn("dt_AssetID")    'declaring a column named Name
            dt_AssetID.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            TblAsset.Columns.Add(dt_AssetID)                               'adding the column to table

            ''Column 2: Dept ID 
            Dim dt_DeptID As DataColumn = New DataColumn("dt_DeptID")      'declaring a column named Name
            dt_DeptID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_DeptID)                                    'adding the column to table

            ''Column 3: Loc ID 
            Dim dt_LocID As DataColumn = New DataColumn("dt_LocID")      'declaring a column named Name
            dt_LocID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_LocID)                                    'adding the column to table

            ''Column 3a: Sub Loc ID 
            Dim dt_LocSubID As DataColumn = New DataColumn("dt_LocSubID")      'declaring a column named Name
            dt_LocSubID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_LocSubID)                                    'adding the column to table

            ''Column 4: Owner ID 
            Dim dt_OwnerID As DataColumn = New DataColumn("dt_OwnerID")      'declaring a column named Name
            dt_LocID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_OwnerID)

            ''Column 5: Asset Remarks
            Dim dt_ARemarks As DataColumn = New DataColumn("dt_ARemarks")      'declaring a column named Name
            dt_ARemarks.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_ARemarks)                                    'adding the column to table

            ''Column 5: STDetailsID
            Dim dt_MSTDetailID As DataColumn = New DataColumn("dt_MSTDetailID")      'declaring a column named Name
            dt_MSTDetailID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_MSTDetailID)                                    'adding the column to table

            ''Column 5: STDetailsID
            Dim dt_FSTDetailID As DataColumn = New DataColumn("dt_FSTDetailID")      'declaring a column named Name
            dt_FSTDetailID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_FSTDetailID)                                    'adding the column to table

            ''Set Primary Keys
            '--> Make the ID column the primary key column.
            Dim PrimaryKeyColumns(0) As DataColumn
            PrimaryKeyColumns(0) = TblAsset.Columns("dt_AssetID")
            TblAsset.PrimaryKey = PrimaryKeyColumns

            ''return dataTable 
            fnCreateDataTableAsset = TblAsset
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim ddllist As DropDownList = CType(sender, DropDownList)
            Dim cell As TableCell = CType(ddllist.Parent, TableCell)
            Dim item As DataGridItem = CType(cell.Parent, DataGridItem)
            Dim ddlLoc As DropDownList = CType(item.Cells(10).FindControl("ddlLocation"), DropDownList)
            Dim ddlLocSub As DropDownList = CType(item.Cells(11).FindControl("ddlLocSub"), DropDownList)

            'Dim strddlID As String = sender.clientid.ToString
            'Dim strddlNo As String = strddlID.Substring(Len(strddlID) - 3, 3)
            'Dim ddlLocation As DropDownList = CType(item.Cells(7).FindControl("ddlLocADD" & strddlNo), DropDownList)
            'Dim ddlLocSub As DropDownList = CType(item.Cells(7).FindControl("ddlLocSubADD" & strddlNo), DropDownList)

            If ddlLoc.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLoc.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCode", False)
            Else
                ddlLocSub.Items.Clear()
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class