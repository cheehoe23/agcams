#Region "Information Section"
' ****************************************************************************************************
' Description       : Stock-Taking  
' Purpose           : Stock-Taking Information
' Date              : 12/11/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class MovementUpdate_view
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            If Not Page.IsPostBack Then
                hdnSTPID.Value = Request("STPID")
                lblErrorMessage.Visible = False
                lblErrorMessage.Text = ""

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''get header
                fnGetHeader()

                ''Check Access Right  
                If (InStr(Session("AR"), "offlineProcess|StockTakeHis|uploadMv") = 0) Then dgStocktake.Columns(9).Visible = False

                ''Populate Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnGetHeader()
        Dim objRdr As SqlDataReader
        Try
            ''**Get Record
            objRdr = clsOfflineProccess.fnOfflineProPeriod_GetSTPeriodForEdit(hdnSTPID.Value)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()
                    lblHeader.Text = "(From " & CStr(objRdr("fld_STStartDt")) & " To " & CStr(objRdr("fld_STEndDt")) & ")"
                End If
            End If
            objRdr.Close()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            ''Get Records
            dgStocktake.DataSource = clsOfflineProccess.fnOfflinePro_GeStocktakeHistoryRec(Session("UsrID"), hdnSTPID.Value, Session("AdminF"))
            dgStocktake.DataBind()
            If Not dgStocktake.Items.Count > 0 Then ''Not Records found
                ddlPageSize.Enabled = False
                lblErrorMessage.Text = "There is no record."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function fnShowEditHyperlink(ByVal strSTID As String) As String
        Dim strRetVal As String = ""
        If (InStr(Session("AR"), "offlineProcess|StockTakeHis|uploadMv") > 0) Then  '' Check For Access Right
            strRetVal += "<a id=hlEditDept href='MovementUpdate_Detail.aspx?STID=" + strSTID + "&STPID=" + hdnSTPID.Value + "' target=_self >" & _
                        "<img id=imgUpdate src=../images/update.gif alt='Click for view Stocktake Details.' border=0 >" & _
                        "</a>"
        End If

        Return (strRetVal)
    End Function

    Private Sub dgStocktake_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgStocktake.PageIndexChanged
        dgStocktake.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgStocktake.CurrentPageIndex = 0
        dgStocktake.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub
End Class