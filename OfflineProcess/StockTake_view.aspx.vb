#Region "Information Section"
' ****************************************************************************************************
' Description       : Stock-Taking 
' Purpose           : View Stock-Taking Information
' Date              : 30/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
#End Region

Partial Public Class StockTake_view
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butUpload2PDA.Attributes.Add("OnClick", "return ConfirmUpload()")

                ''Checking For Assign Right
                If (InStr(Session("AR"), "offlineProcess|StockTakeAct|uploadPDA") = 0) Then butUpload2PDA.Visible = False

                ''Get Value pass from Search Screen
                hdnAssetCatID.Value = Request("CatID")
                hdnAssetCatSubID.Value = Request("CatSID")
                hdnDeptID.Value = Request("DeptID")
                hdnLocID.Value = Request("LocID")
                hdnLocSubID.Value = Request("SLocID")
                hdnOwnerID.Value = Request("OwnerID")
                hdnMvFDt.Value = Request("MvFDt")
                hdnMvTDt.Value = Request("MvTDt")
                hdnAType.Value = Request("AType")

                ''default Sorting
                hdnSortName.Value = "fldAssetBarcode"
                hdnSortAD.Value = "ASC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Sort Header Display
            fnSortHeaderDisplay()
            
            ''Get Records
            dgAsset.DataSource = clsOfflineProccess.fnOfflinePro_StocktakeSearchRecord( _
                                 hdnAssetCatID.Value, hdnAssetCatSubID.Value, _
                                 hdnDeptID.Value, hdnLocID.Value, hdnOwnerID.Value, _
                                 hdnMvFDt.Value, hdnMvTDt.Value, _
                                 hdnSortName.Value, hdnSortAD.Value, hdnAType.Value, hdnLocSubID.Value)
            dgAsset.DataBind()
            If Not dgAsset.Items.Count > 0 Then ''Not Records found
                ddlPageSize.Enabled = False
                butUpload2PDA.Enabled = False
                lblErrorMessage.Text = "There is no record."
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By :-
        ''       fldAssetBarcode (2), fld_AssetTypeStr (3), 
        ''       fld_CategoryName (4), fld_CatSubName (5), 
        ''       fld_DepartmentName (6), fld_LocationName (7), 
        ''       fld_OwnerName (8), fld_AssetStatusStr (9)

        Dim intSortIndex As Integer
        Dim strSortHeader As String = ""

        ''Set Default Header
        dgAsset.Columns(2).HeaderText = "Asset ID"
        dgAsset.Columns(3).HeaderText = "Type"
        dgAsset.Columns(4).HeaderText = "Category"
        dgAsset.Columns(5).HeaderText = "Subcategory"
        dgAsset.Columns(6).HeaderText = "Department"
        dgAsset.Columns(7).HeaderText = "Location"
        dgAsset.Columns(8).HeaderText = "Sub Location"
        dgAsset.Columns(9).HeaderText = "Assigned Owner"
        dgAsset.Columns(10).HeaderText = "Status"

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fldAssetBarcode"
                intSortIndex = 2
                strSortHeader = "Asset ID"
            Case "fld_AssetTypeStr"
                intSortIndex = 3
                strSortHeader = "Type"
            Case "fld_CategoryName"
                intSortIndex = 4
                strSortHeader = "Category"
            Case "fld_CatSubName"
                intSortIndex = 5
                strSortHeader = "Subcategory"
            Case "fld_DepartmentName"
                intSortIndex = 6
                strSortHeader = "Department"
            Case "fld_LocationName"
                intSortIndex = 7
                strSortHeader = "Location"
            Case "fld_LocSubName"
                intSortIndex = 8
                strSortHeader = "Sub Location"
            Case "fld_OwnerName"
                intSortIndex = 9
                strSortHeader = "Assigned Owner"
            Case "fld_AssetStatusStr"
                intSortIndex = 10
                strSortHeader = "Status"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgAsset.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgAsset.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Private Sub dgAsset_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgAsset.PageIndexChanged
        dgAsset.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub dgAsset_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgAsset.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgAsset.CurrentPageIndex = 0
        dgAsset.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Protected Sub butUpload2PDA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpload2PDA.Click
        Try
            Dim strFileName4PDA As String = ""
            Dim strStocktakeID As String = ""

            ''*** Get File Name
            strFileName4PDA = "IN_" & Year(Date.Today).ToString & Month(Date.Today).ToString.PadLeft(2, "0") & Day(Date.Today).ToString.PadLeft(2, "0") & Hour(Date.Now).ToString.PadLeft(2, "0") & Minute(Date.Now).ToString.PadLeft(2, "0") & Second(Date.Now).ToString.PadLeft(2, "0") & ".txt"

            ''*** Insert record  
            Dim intRetVal As Integer
            Dim strMsg As String
            intRetVal = clsOfflineProccess.fnOfflinePro_UploadAsset2PDA( _
                                 hdnAssetCatID.Value, hdnAssetCatSubID.Value, _
                                 hdnDeptID.Value, hdnLocID.Value, hdnOwnerID.Value, _
                                 hdnMvFDt.Value, hdnMvTDt.Value, strFileName4PDA, _
                                 Session("UsrID"), strStocktakeID, hdnAType.Value, hdnLocSubID.Value)

            If intRetVal > 0 Then
                strMsg = "Record(s) generated to StockTake ID successfully."

                ''*** Generate File for PDA
                'fnGenerateFile4PDA(strFileName4PDA, strStocktakeID)

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Generate Stocktake Record(s) for stock take ID.")

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "document.location.href='MovementUpdate_Detail.aspx?STID=" & strStocktakeID & "&STPID=0';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "Record(s) generated to StockTake ID failed."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub fnGenerateFile4PDA(ByVal strFileName4PDA As String, ByVal strStocktakeID As String)
        Dim objRdr As SqlDataReader
        Try
            '********** Beginning write text file ***********
            Dim FileFullPath As String = ""
            FileFullPath = Server.MapPath("..\tempFile\PDAInputFile") & "\" & strFileName4PDA


            '********** Beginning write text file ***********
            Dim MyWriter As New StreamWriter(FileFullPath, False)
            Dim textstring As String = ""

            ''0.**** Details for Stocktake
            textstring = ">>>" + strStocktakeID + "|" + Session("LoginID") + "|" + hdnDeptID.Value
            MyWriter.WriteLine(textstring)

            ''1.**** Get User From AD
            ''--> a. Header 
            textstring = "###TBL_USERFROMAD"
            MyWriter.WriteLine(textstring)

            ''--> b. Data
            objRdr = ClsUser.fnUsrGetUsrFromADDR("PU", "0", "", "0")
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        textstring = CStr(objRdr("fld_ADLoginID")) & "|"
                        textstring = textstring + fnReplaceNotAllowVal(CStr(objRdr("fld_ADUsrName")))
                        MyWriter.WriteLine(textstring)
                    End While
                End If
            End If
            objRdr.Close()

            ''2.**** Get Department
            ''--> a. Header 
            textstring = "###TBL_DEPARTMENT"
            MyWriter.WriteLine(textstring)

            ''--> b. Data
            objRdr = clsDepartment.fnDeptGetDeptForEdit(hdnDeptID.Value) 'clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF"))
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        textstring = hdnDeptID.Value & "|"
                        textstring = textstring + fnReplaceNotAllowVal(CStr(objRdr("fld_DepartmentCode"))) & "|"
                        textstring = textstring + fnReplaceNotAllowVal(CStr(objRdr("fld_DepartmentName")))
                        MyWriter.WriteLine(textstring)
                    End While
                End If
            End If
            objRdr.Close()

            ''3.**** Get Location
            ''--> a. Header 
            textstring = "###TBL_LOCATION"
            MyWriter.WriteLine(textstring)

            ''--> b. Data
            objRdr = clsLocation.fnLocationGetAllRecForDDL()
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        textstring = CStr(objRdr("fld_LocationID")) & "|"
                        textstring = textstring + fnReplaceNotAllowVal(CStr(objRdr("fld_LocationCode"))) & "|"
                        textstring = textstring + fnReplaceNotAllowVal(CStr(objRdr("fld_LocationName")))
                        MyWriter.WriteLine(textstring)
                    End While
                End If
            End If
            objRdr.Close()

            ''3a.**** Get Sub Location
            ''--> a. Header 
            textstring = "###TBL_LOCATIONSUB"
            MyWriter.WriteLine(textstring)

            ''--> b. Data
            objRdr = clsLocation.fnLocation_GetLocation("0", "0", "S") 'clsLocation.fnLocationGetAllRecForDDL()
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        textstring = CStr(objRdr("fld_LocSubID")) & "|"
                        textstring = textstring + CStr(objRdr("fld_LocationID")) & "|"
                        textstring = textstring + fnReplaceNotAllowVal(CStr(objRdr("fld_LocSubCode"))) & "|"
                        textstring = textstring + fnReplaceNotAllowVal(CStr(objRdr("fld_LocSubName")))
                        MyWriter.WriteLine(textstring)
                    End While
                End If
            End If
            objRdr.Close()

            ''4.**** Get Asset
            ''--> a. Header 
            textstring = "###TBL_ASSET"
            MyWriter.WriteLine(textstring)

            ''--> b. Data
            objRdr = clsOfflineProccess.fnOfflinePro_StocktakeSearchRecordDR( _
                                 hdnAssetCatID.Value, hdnAssetCatSubID.Value, _
                                 hdnDeptID.Value, hdnLocID.Value, hdnOwnerID.Value, _
                                 hdnMvFDt.Value, hdnMvTDt.Value, _
                                 "fldAssetBarcode", "ASC", hdnAType.Value, hdnLocSubID.Value)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        textstring = CStr(objRdr("fld_AssetID")) & "|"
                        textstring = textstring + CStr(objRdr("fldAssetBarcode")) & "|"
                        textstring = textstring + CStr(objRdr("fld_AssetNFSID")) & "|"
                        textstring = textstring + fnReplaceNotAllowVal(Replace(CStr(objRdr("fld_AssetDesc")), vbCrLf, " ")) & "|"
                        textstring = textstring + CStr(objRdr("fld_AssetMvID")) & "|"
                        'textstring = textstring + Replace(CStr(objRdr("fld_AssetRemarks")), vbCrLf, " ")
                        textstring = textstring + ""
                        MyWriter.WriteLine(textstring)
                    End While
                End If
            End If
            objRdr.Close()

            ''5.**** Get Asset Movement
            ''--> a. Header 
            textstring = "###TBL_ASSETMOVEMENT"
            MyWriter.WriteLine(textstring)

            ''--> b. Data
            objRdr = clsOfflineProccess.fnOfflinePro_StocktakeSearchRecordGetMovement( _
                                 hdnAssetCatID.Value, hdnAssetCatSubID.Value, _
                                 hdnDeptID.Value, hdnLocID.Value, hdnOwnerID.Value, _
                                 hdnMvFDt.Value, hdnMvTDt.Value, hdnAType.Value, hdnLocSubID.Value)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        textstring = CStr(objRdr("fld_AssetMvID")) & "|"
                        textstring = textstring + CStr(objRdr("fld_DeptID")) & "|"
                        textstring = textstring + CStr(objRdr("fld_LocationID")) & "|"
                        textstring = textstring + CStr(objRdr("fld_LocSubID")) & "|"
                        textstring = textstring + CStr(objRdr("fld_OwnerLoginID")) & "|"
                        textstring = textstring + fnReplaceNotAllowVal(CStr(objRdr("fld_OwnerName"))) & "|"
                        textstring = textstring + CStr(objRdr("fld_CreatedDt")) & "|"
                        textstring = textstring + CStr(objRdr("fld_CreatedBy"))
                        MyWriter.WriteLine(textstring)
                    End While
                End If
            End If
            objRdr.Close()

            MyWriter.Close()
            MyWriter = Nothing
            '********** Ending write text file ***********
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Public Function fnReplaceNotAllowVal(ByVal strDataVal As String) As String
        Try
            Dim strRetVal As String = ""
            strRetVal = Replace(Replace(strDataVal, "'", "''"), "|", "")

            fnReplaceNotAllowVal = strRetVal
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class