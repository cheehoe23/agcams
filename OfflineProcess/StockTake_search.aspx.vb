#Region "Information Section"
' ****************************************************************************************************
' Description       : Stock-Taking  
' Purpose           : Stock-Taking Information
' Date              : 30/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Web.Services

#End Region

Partial Public Class StockTake_search
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            Me.Header.DataBind()

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            ''Get Owner (in-case page load again)

            If Not Page.IsPostBack Then
                Me.butSearch.Attributes.Add("OnClick", "return chkFrm()")

                ''Populate Control
                fnPopulateCtrl()

                ''set default
                cbAType1.Checked = True
                cbAType2.Checked = True
                cbAType3.Checked = True
                'cbAType4.Checked = True

                ''Check whether is in Stocktake Period
                fnCheckStocktakePeriod()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnCheckStocktakePeriod()
        Try
            Dim strStocktakeF As String = "N"
            strStocktakeF = clsOfflineProccess.fnOfflinePro_CheckStocktakePeriod()

            ''If not in stocktake period, not allow to do any action
            If strStocktakeF = "N" Then
                cbAType1.Enabled = False
                cbAType2.Enabled = False
                cbAType3.Enabled = False
                ddlDepartment.Enabled = False
                ddlLocation.Enabled = False
                ddlLocSub.Enabled = False
                cbOwner.Enabled = False
                ddlAssetCat.Enabled = False
                ddlAssetSubCat.Enabled = False
                txtMvFrmDt.Enabled = False
                txtMvToDt.Enabled = False
                butSearch.Enabled = False
                butReset.Enabled = False

                lblErrorMessage.Text = "Not in Stocktake Period"
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDepartment, "fld_DepartmentID", "fld_DptCN", True)
            'If Session("AdminF") = "N" Then ddlDepartment.Items.Remove(ddlDepartment.Items.FindByValue("-1"))

            ''Get Location
            fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLocation, "fld_LocationID", "fld_LoctCodeName", False)

            ''Get Sub Location 
            ddlLocation_SelectedIndexChanged(Nothing, Nothing)

            ''Get Category
            fnPopulateDropDownList(clsCategory.fnCategoryGetCategory(), ddlAssetCat, "fld_CategoryID", "fld_CatCodeName", False)

            ''Get SubCategory (set default --> -1)
            ddlAssetSubCat.Items.Insert(0, New ListItem("", "-1"))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ddlAssetCat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAssetCat.SelectedIndexChanged
        Try
            If ddlAssetCat.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsCategory.fnCategoryGetAllRecDR(ddlAssetCat.SelectedValue, 0, "fld_CatSubName", "ASC"), ddlAssetSubCat, "fld_CatSubID", "fld_CatSubName", False)
            Else
                ddlAssetSubCat.Items.Clear()
                ddlAssetSubCat.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            Server.Transfer("StockTake_search.aspx")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
        Try
            Dim dsAsset As New DataSet
            Dim strURL As String = ""

            Dim strAssetType As String = ""
            Dim intAssetCatID As Integer = IIf(ddlAssetCat.SelectedValue = "-1", "0", ddlAssetCat.SelectedValue)
            Dim intAssetCatSubID As Integer = IIf(ddlAssetSubCat.SelectedValue = "-1", "0", ddlAssetSubCat.SelectedValue)
            Dim intDeptID As Integer = IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue)
            Dim intLocID As Integer = IIf(ddlLocation.SelectedValue = "-1", "0", ddlLocation.SelectedValue)
            Dim intLocSubID As Integer = IIf(ddlLocSub.SelectedValue = "-1", "0", ddlLocSub.SelectedValue)
            Dim strOwnerID As String = ""
            Dim strMvFDt As String = Trim(txtMvFrmDt.Text)
            Dim strMvTDt As String = Trim(txtMvToDt.Text)
            Dim StrSortName As String = "fldAssetBarcode"
            Dim strSortOrder As String = "ASC"

            ''*** Get Asset Type 
            strAssetType = IIf(cbAType1.Checked, "A", "N") & "|"
            'strAssetType = IIf(cbAType4.Checked, "K", "N") & "|"
            strAssetType &= IIf(cbAType2.Checked, "I", "N") & "|"
            strAssetType &= IIf(cbAType3.Checked, "L", "N")

            ''*** Get Owner
            If Trim(cbOwner.Text) <> "" Then
                Dim strUsrExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbOwner.Text), strOwnerID, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Owner not found. Please select an existing Owner."
                    lblErrorMessage.Visible = True
                    cbOwner.Focus()
                    Exit Sub
                End If
            End If

            dsAsset = clsOfflineProccess.fnOfflinePro_StocktakeSearchRecord( _
                                 intAssetCatID, intAssetCatSubID, _
                                 intDeptID, intLocID, strOwnerID, _
                                 strMvFDt, strMvTDt, _
                                 StrSortName, strSortOrder, strAssetType, intLocSubID)


            If dsAsset.Tables(0).Rows.Count > 0 Then
                strURL = "StockTake_view.aspx?CatID=" + CStr(intAssetCatID) + "&CatSID=" + CStr(intAssetCatSubID) + _
                         "&DeptID=" + CStr(intDeptID) + "&LocID=" + CStr(intLocID) + "&OwnerID=" + strOwnerID + _
                         "&MvFDt=" + strMvFDt + "&MvTDt=" + strMvTDt + "&AType=" + strAssetType + "&SLocID=" + CStr(intLocSubID)

                Response.Redirect(strURL)
            Else
                lblErrorMessage.Visible = True
                lblErrorMessage.Text = "Record(s) not found."
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLocation.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCodeName", False)
            Else
                ddlLocSub.Items.Clear()
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class