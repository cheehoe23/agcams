#Region "Information Section"
' ****************************************************************************************************
' Description       : View Stocktake History
' Purpose           : View/Delete Stocktake History
' Author            : See Siew
' Date              : 19/11/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class MovementUpdate_view1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            ''Get Records
            dgStocktake.DataSource = clsOfflineProccess.fnOfflineProHistory_GetStocktakePeriod(Session("AdminF"), Session("UsrID"))
            dgStocktake.DataBind()
            If Not dgStocktake.Items.Count > 0 Then ''Not Records found
                ddlPageSize.Enabled = False
                lblErrorMessage.Text = "There is no record."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function fnShowEditHyperlink(ByVal strSTID As String) As String
        Dim strRetVal As String = ""
        strRetVal += "<a id=hlEditDept href='MovementUpdate_view.aspx?STPID=" + strSTID + "' target=_self >" & _
                     "<img id=imgUpdate src=../images/audit.gif alt='Click for View/Upload Stocktake History.' border=0 >" & _
                     "</a>"

        strRetVal += "<a id=hlEditDept href='MovementUpdate_MainDetail.aspx?STID=" + strSTID + "' target=_self >" & _
                     "<img id=imgUpdate src=../images/update.gif alt='Click for View/Update Stocktake History.' border=0 >" & _
                     "</a>"

        Return (strRetVal)
    End Function

    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgStocktake.CurrentPageIndex = 0
        dgStocktake.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Private Sub dgStocktake_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgStocktake.PageIndexChanged
        dgStocktake.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub
End Class