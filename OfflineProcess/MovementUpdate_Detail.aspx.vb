#Region "Information Section"
' ****************************************************************************************************
' Description       : Stocktake Details
' Purpose           : Stocktake Details
' Author            : See Siew
' Date              : 12/11/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
#End Region

Partial Public Class MovementUpdate_Detail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butMvUpdate.Attributes.Add("OnClick", "return chkFrm()")

                ''Get Stocktake ID 
                hdnSTID.Text = Request("STID")
                hdnSTPID.Text = Request("STPID")

                ''GetHeader 
                fnGetHeader()

                ''Retrieve Records 
                fnGetRecordsFromDB()

                ''Check access right                
                If (InStr(Session("AR"), "offlineProcess|StockTakeHis|uploadMv") = 0) Or hdnOverDueF.Text = "3" Then
                    butMvUpdate.Visible = False
                    FUploadFile.Visible = False
                End If

            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnGetHeader()
        Dim objRdr As SqlDataReader
        Try
            ''**Get Record
            objRdr = clsOfflineProccess.fnOfflineProPeriod_GetSTPeriodForEdit(hdnSTPID.Text)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()
                    lblHeader.Text = "(From " & CStr(objRdr("fld_STStartDt")) & " To " & CStr(objRdr("fld_STEndDt")) & ")"
                    hdnOverDueF.Text = CStr(objRdr("STChangeF"))
                End If
            End If
            objRdr.Close()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Public Sub fnGetRecordsFromDB()
        Dim objRdr As SqlDataReader
        Try
            ''Get Stocktake Master Info
            objRdr = clsOfflineProccess.fnOfflinePro_GeStocktakeDetail(hdnSTID.Text)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()
                    lblSTFor.Text = CStr(objRdr("StocktakeFor"))
                    lblSTDate.Text = CStr(objRdr("fld_CreatedDt"))
                    lblSTBy.Text = CStr(objRdr("fld_usrName"))
                    lblSTID.Text = CStr(objRdr("fld_StocktakeID"))

                    hylFileName.NavigateUrl = "../TempFile/PDAInputFile/" & CStr(objRdr("fld_FileName4PDA"))
                    hylFileName.Text = CStr(objRdr("fld_FileName4PDA"))

                    lblTotRec.Text = CStr(objRdr("TotAsset"))
                    lblTotScan.Text = CStr(objRdr("TotScan"))
                    lblTotMiss.Text = CStr(objRdr("TotUnScan"))
                    lblTotFound.Text = CStr(objRdr("TotFound"))
                End If
            End If
            objRdr.Close()

            ''Get Stocktake Asset Details
            dgAssetScan.DataSource = clsOfflineProccess.fnOfflinePro_GetStocktakeAssetRecords(hdnSTID.Text, "S")
            dgAssetScan.DataBind()

            dgAssetMiss.DataSource = clsOfflineProccess.fnOfflinePro_GetStocktakeAssetRecords(hdnSTID.Text, "U")
            dgAssetMiss.DataBind()

            dgAssetFound.DataSource = clsOfflineProccess.fnOfflinePro_GetStocktakeAssetRecords(hdnSTID.Text, "F")
            dgAssetFound.DataBind()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Protected Sub butMvUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butMvUpdate.Click
        Try
            If FUploadFile.PostedFile.ContentLength <= 0 Then
                lblErrorMessage.Text = "Please browse file first."
                lblErrorMessage.Visible = True
                Exit Sub

            Else

                '' ** Save file in system
                Dim SaveLocation As String = ""
                Dim fn As String = ""
                If Not FUploadFile.PostedFile Is Nothing And FUploadFile.PostedFile.ContentLength > 0 Then
                    fn = System.IO.Path.GetFileName(FUploadFile.PostedFile.FileName)
                    SaveLocation = Server.MapPath("..\tempFile\PDAOutputFile") & "\" & fn
                    FUploadFile.PostedFile.SaveAs(SaveLocation)
                End If

                '' ** Loop Information in Text File and get information...
                fnLoopTextFile(SaveLocation)
            End If
        Catch ex As Exception
            FUploadFile.Dispose()
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnLoopTextFile(ByVal SaveLocation As String)
        Try
           ''Create Datatable .. Get Datatable
            Dim TblAsset As DataTable = fnCreateDataTableAsset()
            Dim RowAsset As DataRow

            Dim TblAssetMV As DataTable = fnCreateDataTableAssetMV()
            Dim RowAssetMV As DataRow

            ''Loop Record in File
            Dim MyReader As New StreamReader(SaveLocation)
            Dim strline As String = ""
            Dim strType As String = ""  '' A=Tbl Asset, M=Tbl Asset Movement
            Dim arrAsset As Array
            Dim arrAssetMv As Array
            Dim arrFirstLine As Array
            Do Until strline Is Nothing
                strline = MyReader.ReadLine()
                If Not strline Is Nothing And strline <> "" Then

                    ''*** Get Starting point for table
                    If strline = "###TBL_ASSET" Then
                        strType = "A"
                    ElseIf strline = "###TBL_ASSETMOVEMENT" Then
                        strType = "M"
                    Else
                        ''*** Get data
                        If strType = "A" Then
                            arrAsset = Split(strline, "|")

                            ''Add New Row to Datatable
                            RowAsset = TblAsset.NewRow()        'declaring a new row
                            RowAsset.Item("dt_AssetID") = arrAsset(0)
                            'RowAsset.Item("dt_BarcodeID") = arrAsset(1)
                            RowAsset.Item("dt_AMvID") = arrAsset(3)
                            RowAsset.Item("dt_ARemarks") = Trim(arrAsset(4))
                            RowAsset.Item("dt_ScanF") = arrAsset(5)
                            RowAsset.Item("dt_ModifyDt") = arrAsset(6)
                            TblAsset.Rows.Add(RowAsset)

                        ElseIf strType = "M" Then
                            arrAssetMv = Split(strline, "|")

                            ''Add New Row to Datatable
                            RowAssetMV = TblAssetMV.NewRow()        'declaring a new row
                            RowAssetMV.Item("dt_MAssetID") = arrAssetMv(0)
                            RowAssetMV.Item("dt_MvID") = arrAssetMv(1)
                            RowAssetMV.Item("dt_DeptID") = arrAssetMv(2)
                            RowAssetMV.Item("dt_LocID") = arrAssetMv(3)
                            RowAssetMV.Item("dt_LocSubID") = arrAssetMv(4)
                            RowAssetMV.Item("dt_OwnerID") = arrAssetMv(5)
                            RowAssetMV.Item("dt_OwnerN") = arrAssetMv(6)
                            RowAssetMV.Item("dt_CreateDt") = arrAssetMv(7)
                            TblAssetMV.Rows.Add(RowAssetMV)

                        Else '' First Line
                            Dim strSTID As String = ""
                            arrFirstLine = Split(strline, "|")
                            strSTID = arrFirstLine(0)
                            If CStr(strSTID) <> CStr(">>>" & hdnSTID.Text) Then
                                lblErrorMessage.Text = "Upload File is not belong to this record. Please make sure you upload correct file."
                                lblErrorMessage.Visible = True
                                FUploadFile.PostedFile.InputStream.Flush()
                                FUploadFile.PostedFile.InputStream.Close()
                                FUploadFile.Dispose()
                                MyReader.Close()
                                MyReader.Dispose()
                                Exit Sub
                            End If
                        End If
                    End If

                End If
            Loop
            MyReader.Close()
            MyReader.Dispose()


            ''** Get Asset in XML
            Dim ds As New DataSet
            Dim strAssetXML As String = ""
            ds = New DataSet         'creating a dataset
            ds.Tables.Add(TblAsset)   'assign datatable to dataset
            If Not ds Is Nothing Then
                Dim loCol As DataColumn
                'Prepare XML output from the DataSet as a string
                For Each loCol In ds.Tables(0).Columns
                    loCol.ColumnMapping = System.Data.MappingType.Attribute
                Next
                strAssetXML = ds.GetXml
            End If

            ''** Get Asset Movement in XML
            Dim dsMv As New DataSet
            Dim strAssetMVXML As String = ""
            dsMv = New DataSet         'creating a dataset
            dsMv.Tables.Add(TblAssetMV)   'assign datatable to dataset
            If Not dsMv Is Nothing Then
                Dim loCol2 As DataColumn
                'Prepare XML output from the DataSet as a string
                For Each loCol2 In dsMv.Tables(0).Columns
                    loCol2.ColumnMapping = System.Data.MappingType.Attribute
                Next
                strAssetMVXML = dsMv.GetXml
            End If

            ''Update Record in DB
            If strAssetXML <> "" And strAssetMVXML <> "" Then
                Dim intRetVal As Integer
                Dim strMsg As String
                intRetVal = clsOfflineProccess.fnOfflinePro_UpdateMovement(hdnSTID.Text, strAssetXML, strAssetMVXML, Session("UsrID"))
                If intRetVal > 0 Then
                    strMsg = "File upload successfully "

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Stocktake file uploaded")

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "document.location.href='MovementUpdate_Detail.aspx?STID=" & hdnSTID.Text & "&STPID=" & hdnSTPID.Text & "';" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    strMsg = "File have been upload Failed. Please contact your Administrator."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If
            End If

        Catch ex As Exception
            Throw ex
            'Finally
            '    FUploadFile.PostedFile.InputStream.Flush()
            '    FUploadFile.PostedFile.InputStream.Close()
            '    FUploadFile.Dispose()
        End Try

    End Sub

    Public Function fnCreateDataTableAsset() As DataTable
        Try
            ''****1. creating a table named TblAsset
            Dim TblAsset As DataTable
            TblAsset = New DataTable("TblAsset")

            ''Column 1: Asset id
            Dim dt_AssetID As DataColumn = New DataColumn("dt_AssetID")    'declaring a column named Name
            dt_AssetID.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            TblAsset.Columns.Add(dt_AssetID)                               'adding the column to table

            ' ''Column 1: Asset AMS id
            'Dim dt_BarcodeID As DataColumn = New DataColumn("dt_BarcodeID")    'declaring a column named Name
            'dt_BarcodeID.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            'TblAsset.Columns.Add(dt_BarcodeID)

            ''Column 1: Mv id
            Dim dt_AMvID As DataColumn = New DataColumn("dt_AMvID")    'declaring a column named Name
            dt_AMvID.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            TblAsset.Columns.Add(dt_AMvID)

            ''Column 2: Asset Remarks
            Dim dt_ARemarks As DataColumn = New DataColumn("dt_ARemarks")      'declaring a column named Name
            dt_ARemarks.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_ARemarks)                                    'adding the column to table

            ''Column 3: Scan Indicator
            Dim dt_ScanF As DataColumn = New DataColumn("dt_ScanF")      'declaring a column named Name
            dt_ScanF.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_ScanF)                                    'adding the column to table

            ''Column 4: Modify Date
            Dim dt_ModifyDt As DataColumn = New DataColumn("dt_ModifyDt")      'declaring a column named Name
            dt_ModifyDt.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_ModifyDt)                                    'adding the column to table

            ''Set Primary Keys
            '--> Make the ID column the primary key column.
            Dim PrimaryKeyColumns(0) As DataColumn
            PrimaryKeyColumns(0) = TblAsset.Columns("dt_AssetID")
            TblAsset.PrimaryKey = PrimaryKeyColumns

            ''return dataTable 
            fnCreateDataTableAsset = TblAsset
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnCreateDataTableAssetMV() As DataTable
        Try
            ''****1. creating a table named TblAssetMV
            Dim TblAssetMV As DataTable
            TblAssetMV = New DataTable("TblAssetMV")

            ''Column 1: MAsset id
            Dim dt_MAssetID As DataColumn = New DataColumn("dt_MAssetID")    'declaring a column named Name
            dt_MAssetID.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_MAssetID)                               'adding the column to table

            ''Column 2: movement id
            Dim dt_MvID As DataColumn = New DataColumn("dt_MvID")      'declaring a column named Name
            dt_MvID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_MvID)                                    'adding the column to table

            ''Column 2: Dept id
            Dim dt_DeptID As DataColumn = New DataColumn("dt_DeptID")      'declaring a column named Name
            dt_DeptID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_DeptID)                                    'adding the column to table

            ''Column 3: Loc id
            Dim dt_LocID As DataColumn = New DataColumn("dt_LocID")      'declaring a column named Name
            dt_LocID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_LocID)                                    'adding the column to table

            ''Column 3a: Sub Loc id
            Dim dt_LocSubID As DataColumn = New DataColumn("dt_LocSubID")      'declaring a column named Name
            dt_LocSubID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_LocSubID)                                    'adding the column to table

            ''Column 4: owner ID
            Dim dt_OwnerID As DataColumn = New DataColumn("dt_OwnerID")      'declaring a column named Name
            dt_OwnerID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_OwnerID)                                    'adding the column to table

            ''Column 5: owner ID
            Dim dt_OwnerN As DataColumn = New DataColumn("dt_OwnerN")      'declaring a column named Name
            dt_OwnerN.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_OwnerN)                                    'adding the column to table

            ''Column 6: Created Date 
            Dim dt_CreateDt As DataColumn = New DataColumn("dt_CreateDt")      'declaring a column named Name
            dt_CreateDt.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_CreateDt)                                    'adding the column to table

            ''Column 7: Created By 
            Dim dt_CreateBy As DataColumn = New DataColumn("dt_CreateBy")      'declaring a column named Name
            dt_CreateBy.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_CreateBy)                                    'adding the column to table

            ''Set Primary Keys
            '--> Make the ID column the primary key column.
            Dim PrimaryKeyColumns(0) As DataColumn
            'PrimaryKeyColumns(0) = TblAssetMV.Columns("dt_MAssetID")   
            PrimaryKeyColumns(0) = TblAssetMV.Columns("dt_MvID")
            TblAssetMV.PrimaryKey = PrimaryKeyColumns

            ''return dataTable 
            fnCreateDataTableAssetMV = TblAssetMV

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Function fnShowDropDownList(ByVal strAssetID As String) As String
    '    Dim strRetVal As String = ""
    '    'If (InStr(Session("AR"), "SysSet|dept|edit") > 0) Then  '' Check For Edit Department Access Right
    '    If strAssetID = "1" Then
    '        strRetVal = " - "
    '    ElseIf strAssetID = "2" Then
    '        strRetVal = "Pending Approved"
    '    ElseIf strAssetID = "3" Then
    '        strRetVal = "Rejected"
    '    Else
    '        strRetVal = "<select name='ddlAR' id='ddlAR'>" & _
    '                        "<option value=''></option>" & _
    '                        "<option value='A'>Accept</option>" & _
    '                        "<option value='R'>Reject</option>" & _
    '                    "</select>"
    '    End If

    '    'End If

    '    Return (strRetVal)
    'End Function
End Class