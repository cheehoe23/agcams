<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MovementUpdate_view1.aspx.vb" Inherits="AMS.MovementUpdate_view1" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>AMS</title>
    <LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
</head>
<body>
    <form id="Frm_Stocktake" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Offline Processing : Stocktake History</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<table id="Table2" height="400" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr>
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td align="center"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td vAlign="top" width="100%">
						<!--Start Main Content-->
						<table height="400" cellSpacing="1" cellPadding="0" width="100%" align="left" border="0">
							<tr>
								<td vAlign="top" align=left bgColor="#a3a9cc">
									<table cellSpacing="1" cellPadding="1" width="100%" border="0">
										<tr>
											<td width="20%"><asp:dropdownlist id="ddlPageSize" Runat="server" AutoPostBack="True"></asp:dropdownlist>
												&nbsp;&nbsp;<font color="white">per page</font>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td vAlign="top" align="center" height="400">
									<!--Datagrid for display record.-->
									<asp:datagrid id="dgStocktake" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										ItemStyle-Height="25" AllowSorting="True" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" AllowPaging="True" PageSize="10" PagerStyle-HorizontalAlign="Right"
										PagerStyle-VerticalAlign="Middle" PagerStyle-Mode="NumericPages" PagerStyle-Position="Top"
										PagerStyle-BackColor="#a3a9cc" datakeyfield="fld_STPeriodID" AlternatingItemStyle-BackColor="#e3d9ee">
										<headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											Height="25"></headerstyle>
										<Columns>
											<asp:boundcolumn visible="false" datafield="fld_STPeriodID" headertext="fld_STPeriodID" ItemStyle-Height="10">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:hyperlinkcolumn headertext="Start Date" text="fld_STStartDt"  datatextfield="fld_STStartDt"  Visible=false
												navigateurl="MovementUpdate_view.aspx" datanavigateurlfield="fld_STPeriodID" DataNavigateUrlFormatString="MovementUpdate_view.aspx?STPID={0}">
												<itemstyle cssclass="GridText" horizontalalign="Center" width="15%" verticalalign="Middle"  ></itemstyle>
											</asp:hyperlinkcolumn>
											<asp:boundcolumn datafield="fld_STStartDt" headertext="Start Date" ItemStyle-Height="10" >
												<itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn datafield="fld_STEndDt" headertext="End Date" ItemStyle-Height="10" >
												<itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn datafield="TotAsset" headertext="Total Asset" ItemStyle-Height="10">
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn datafield="TotScan" headertext="Total Scan" ItemStyle-Height="10">
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn datafield="TotMiss" headertext="Total Missing" ItemStyle-Height="10">
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn datafield="TotFound" headertext="Total Found" Visible="false"  ItemStyle-Height="10">
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											</asp:boundcolumn>
											<asp:templatecolumn HeaderText="Select" ItemStyle-Width="10%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
												<itemtemplate>
													<%#fnShowEditHyperlink(DataBinder.Eval(Container.DataItem, "fld_STPeriodID"))%>
												</itemtemplate>
											</asp:templatecolumn>
										</Columns>
									</asp:datagrid></td>
							</tr>
						</table>
						<!--End Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
		</form>
</body>
</html>
