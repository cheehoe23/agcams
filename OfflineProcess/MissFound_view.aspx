<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MissFound_view.aspx.vb" Inherits="AMS.MissFound_view" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script type="text/javascript">
	    function chkFrm() {
			var foundError = false;
			            			
			//at least 1 asset record selected for Accept Changes
			if (!foundError && !gfnCheckSelect("At least one record should be selected.")){
				foundError=true
			}
											 
 			if (!foundError){
 			    var flag = false;
 				flag = window.confirm("Are you sure you want to update selected Asset(s)?");
 				return flag;
 			}
			else
				return false;
		}
	</script>
</head>
<body>
   <form id="Frm_StockTake" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Offline Processing : Missing and Found</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
										<TR valign=top>
											<TD align="center" valign="top" width="100%">
												<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
												<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
													width="100%" ID="Table6">
													<TBODY valign=top align=left>
														<TR>
															<TD height="352" valign="top">
															    <asp:button id="butAcceptChange" Runat="server" Text="Accept Changes"></asp:button>
																<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																	<TBODY align=left>
																	    <tr>
					                                                        <td vAlign="top" align="center" colspan=2>
						                                                        <!--Datagrid for display record.-->
						                                                        <asp:datagrid id="dgAssetMissFound" Runat="server" AlternatingItemStyle-Height="25"
							                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
							                                                        BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
							                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
								                                                        Height="25"></headerstyle>
							                                                        <Columns>
							                                                            <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
									                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
								                                                        </asp:boundcolumn>
								                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
									                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
								                                                        </asp:boundcolumn>
								                                                        <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
									                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle"></itemstyle>
								                                                        </asp:boundcolumn>
								                                                         <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr" >
									                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                        </asp:boundcolumn>
								                                                        <asp:boundcolumn headertext="(Missing) Department" datafield="fld_DepartmentName" >
									                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                        </asp:boundcolumn>
								                                                        <asp:boundcolumn headertext="(Missing) Location" datafield="fld_LocationName" >
									                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                        </asp:boundcolumn>
								                                                        <asp:boundcolumn headertext="(Missing) Sub Location" datafield="fld_LocSubName" >
									                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                        </asp:boundcolumn>
								                                                        <asp:boundcolumn headertext="(Missing) Assigned Owner" datafield="fld_OwnerName" >
									                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                        </asp:boundcolumn>
								                                                        <asp:boundcolumn headertext="(Missing) Remarks" datafield="fld_AssetRemarks" Visible=false >
									                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                        </asp:boundcolumn>	
								                                                        <asp:TemplateColumn HeaderText="<font color='red'>*</font>(Found) Department">
                                                                                            <ItemTemplate>
                                                                                                <asp:DropDownList ID="ddlDepartment" runat="server"></asp:DropDownList>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                        <asp:TemplateColumn HeaderText="(Found) Location">
                                                                                            <ItemTemplate>
                                                                                                <asp:DropDownList ID="ddlLocation" runat="server" Width="100px" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                        <asp:TemplateColumn HeaderText="(Found) Sub Location">
                                                                                            <ItemTemplate>
                                                                                                <asp:DropDownList ID="ddlLocSub" runat="server" Width="100px"></asp:DropDownList>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                        <asp:TemplateColumn HeaderText="(Found) Assigned Owner">
                                                                                            <ItemTemplate>
                                                                                                <asp:DropDownList ID="ddlOwner" runat="server"></asp:DropDownList>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                        <asp:TemplateColumn HeaderText="(Found) Remarks">
                                                                                            <ItemTemplate>
                                                                                                <asp:TextBox id="txtRemark" TextMode=MultiLine maxlength="1000" Columns="40" Rows="5" Runat="server"></asp:TextBox>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateColumn>
								                                                        <asp:boundcolumn headertext="(Found) Department" datafield="fld_newDeptId" Visible=false >
									                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                        </asp:boundcolumn>
								                                                        <asp:boundcolumn headertext="(Found) Location" datafield="fld_newLocID" Visible=false >
									                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                        </asp:boundcolumn>
								                                                        <asp:boundcolumn headertext="(Found) Owner" datafield="fld_newOwnerID" Visible=false >
									                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                        </asp:boundcolumn>
								                                                        <asp:boundcolumn headertext="(Found) Remarks" datafield="fld_NewRemark" Visible=false >
									                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                        </asp:boundcolumn>	
								                                                        <asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="10%">
									                                                        <headertemplate>
										                                                        <asp:checkbox id="CheckAll" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
											                                                        runat="server" />
									                                                        </headertemplate>
									                                                        <itemtemplate>
										                                                        <center>
											                                                        <asp:checkbox id="DeleteThis" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
												                                                        runat="server" />
										                                                        </center>
									                                                        </itemtemplate>
								                                                        </asp:templatecolumn>
								                                                        <asp:boundcolumn HeaderText="MSTDetailID" datafield="MSTDetailID" Visible=false>
									                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
								                                                        </asp:boundcolumn>
								                                                         <asp:boundcolumn HeaderText="FSTDetailID" datafield="FSTDetailID" Visible=false>
									                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
								                                                        </asp:boundcolumn>
								                                                        <asp:boundcolumn headertext="(Found) Sub Location" datafield="fld_newLocSubID" Visible=false >
									                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
								                                                        </asp:boundcolumn>
							                                                        </Columns>
						                                                        </asp:datagrid>
					                                                        </td>
				                                                        </tr>
																	</TBODY>
																</TABLE>
															</TD>
														</TR>
														<TR>
															<TD height="2"></TD>
														</TR>
													</TBODY>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer>
	</form>
</body>
</html>
