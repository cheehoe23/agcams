<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="contract_addRenewal.aspx.vb" Inherits="AMS.contract_addRenewal" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<%@ Register TagPrefix="fluent" Namespace="Fluent.MultiLineTextBoxValidator" Assembly="Fluent.MultiLineTextBoxValidator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>	
	<LINK href="../common/ComboBox/styles/contactsearch/combo.css" type="text/css" rel="Stylesheet">
    <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
    <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />

	<script language="javascript">
	    $(document).ready(function () {
	        SearchText("cbOwner", "hfOwner");
	        SearchText("cbOffSignName", "hfOffSignName");
	    });
	    function SearchText(obj1, obj2) {
	        $("#" + obj1).autocomplete({
	            source: function (request, response) {
	                $.ajax({
	                    url: '<%#ResolveUrl("~/ContractMng/contract_addRenewal.aspx/GetUsersList") %>',
	                    data: "{ 'prefix': '" + request.term + "'}",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    success: function (data) {
	                        response($.map(data.d, function (item) {
	                            return {
	                                label: item.split('~')[0],
	                                val: item.split('~')[1]
	                            }
	                        }))
	                    },
	                    error: function (response) {
	                        alert(response.responseText);
	                    },
	                    failure: function (response) {
	                        alert(response.responseText);
	                    }
	                });
	            },
	            select: function (e, i) {
	                $("#" + obj2).val(i.item.val);
	            },
	            minLength: 0
	        });

	    }

	    function mfnOpenAddFileWindow() 
			{
			    //open pop-up window
		        window.open('../common/AddNewFile.aspx?CallFrm=RenewalContract', 'AddFile','width=450,height=250,Top=0,left=0,scrollbars=1');
				return false;
			}
		function mfnOpenAddAssetWindow() 
			{
				window.open('contract_selectAsset.aspx?CallFrm=RenewalContract', 'Add_Asset','width=700,height=500,Top=0,left=0,scrollbars=1');
				//window.open('contract_AddAsset.aspx?CallFrm=AddContact', 'Add_Asset','width=700,height=500,Top=0,left=0,scrollbars=1');
			}
		
		function chkFrm() {
			var foundError = false;
					
			//Validate Contract Cost
			if (!foundError && gfnIsFieldBlank(document.Frm_ContractAdd.tctContCost)) {
				foundError=true;
				document.Frm_ContractAdd.tctContCost.focus();
				alert("Please enter Contract Cost.");
			}
			if (!foundError && gfnCheckNumeric(document.Frm_ContractAdd.tctContCost,'.')) {
				    foundError=true;
				    document.Frm_ContractAdd.tctContCost.focus();
				    alert("Contract Cost just allow number only.");
			} 
			    
			//validate Contract From date
			if (!foundError && gfnCheckDate(document.Frm_ContractAdd.txtContFrmDt, "Contract From Date", "M") == false) {
				foundError=true
			}
			
			//validate Contract To date
			if (!foundError && gfnCheckDate(document.Frm_ContractAdd.txtContToDt, "Contract To Date", "M") == false) {
				foundError=true
			}
			
			if (!foundError && gfnCheckFrmToDt(document.Frm_ContractAdd.txtContFrmDt,document.Frm_ContractAdd.txtContToDt,'>') == false) {
			    foundError=true
			    document.Frm_ContractAdd.txtContToDt.focus()
			    alert("Contract To Date should be greater than Contract From Date.");
		    }
			
			//validate Contract Expiry Notification (1st)
			if (!foundError && document.Frm_ContractAdd.ddlContExpNoti.value == 0) {
			    foundError=true
			    document.Frm_ContractAdd.ddlContExpNoti.focus()
			    alert("Please select Contract Expiry Period Notification (1st Reminder).")
			}
			
			//validate Contract Expiry Notification (2nd)
			if (!foundError && document.Frm_ContractAdd.ddlContExpNoti2.value == 0) {
			    foundError=true
			    document.Frm_ContractAdd.ddlContExpNoti2.focus()
			    alert("Please select Contract Expiry Period Notification (2nd Reminder).")
			}
			
			//Validate Vendor Name 
			if (!foundError && gfnIsFieldBlank(document.Frm_ContractAdd.txtVendorName)) {
				foundError=true;
				document.Frm_ContractAdd.txtVendorName.focus();
				alert("Please enter Vendor Name.");
			}
			
			//Validate Contract Owner
			if (!foundError && gfnIsFieldBlank(document.Frm_ContractAdd.cbOwner)) {
                foundError=true
                alert("Please enter Contract Owner.")
            }
			
			//Validate Department for Contract Owner
			if (!foundError && document.Frm_ContractAdd.ddlDepartment.value == "-1") {
				foundError=true
				document.Frm_ContractAdd.ddlDepartment.focus()
				alert("Please select the Department.")
			}
			 
			//validate Security Deposit
			if (!foundError && gfnIsFieldBlank(document.Frm_ContractAdd.txtSecurityDeposit)== false) {
			    if (!foundError && gfnCheckNumeric(document.Frm_ContractAdd.txtSecurityDeposit,'.')) {
				        foundError=true;
				        document.Frm_ContractAdd.txtSecurityDeposit.focus();
				        alert("Security Deposit just allow number only.");
			    }   
			 }
			 
			//validate Security Deposit From Date
			if (!foundError && gfnCheckDate(document.Frm_ContractAdd.txtSecurityDepFDt, "Security Deposit From Date", "O") == false) {
				foundError=true
			}
			if (!foundError && !gfnIsFieldBlank(document.Frm_ContractAdd.txtSecurityDepTDt)) {
			    if (!foundError && gfnCheckDate(document.Frm_ContractAdd.txtSecurityDepFDt, "Security Deposit From Date", "M") == false) {
				    foundError=true
			    }
			}
			 
			//validate Security Deposit To Date
			if (!foundError && gfnCheckDate(document.Frm_ContractAdd.txtSecurityDepTDt, "Security Deposit To Date", "O") == false) {
				foundError=true
			}
			if (!foundError && !gfnIsFieldBlank(document.Frm_ContractAdd.txtSecurityDepFDt)) {
			    if (!foundError && gfnCheckDate(document.Frm_ContractAdd.txtSecurityDepTDt, "Security Deposit To Date", "M") == false) {
				    foundError=true
			    }
			}
			
			//validate Security Deposit Expiry Notification
			if (!foundError && !gfnIsFieldBlank(document.Frm_ContractAdd.txtSecurityDepTDt)) {
			    if (!foundError && gfnCheckFrmToDt(document.Frm_ContractAdd.txtSecurityDepFDt,document.Frm_ContractAdd.txtSecurityDepTDt,'>') == false) {
			        foundError=true
			        document.Frm_ContractAdd.txtSecurityDepTDt.focus()
			        alert("Security Deposit To Date should be greater than Security Deposit From Date.");
		        }
			    if (!foundError && document.Frm_ContractAdd.ddlSecDpsNoti.value == 0) {
			        foundError=true
			        document.Frm_ContractAdd.ddlSecDpsNoti.focus()
			        alert("Please select Security Deposit Expiry Period Notification.")
			    }
			}
						    
			//validate Banker's Guarantee Amount
			if (!foundError && gfnIsFieldBlank(document.Frm_ContractAdd.txtBankGuarAmount)== false) {
			    if (!foundError && gfnCheckNumeric(document.Frm_ContractAdd.txtBankGuarAmount,'.')) {
				        foundError=true;
				        document.Frm_ContractAdd.txtBankGuarAmount.focus();
				        alert("Banker's Guarantee Amount just allow number only.");
			    }   
			 }
			    
			//validate Banker's Guarantee From Date
			if (!foundError && gfnCheckDate(document.Frm_ContractAdd.txtBankGuarFDt, "Banker's Guarantee From Date", "O") == false) {
				foundError=true
			}
			if (!foundError && !gfnIsFieldBlank(document.Frm_ContractAdd.txtBankGuarTDt)) {
			    if (!foundError && gfnCheckDate(document.Frm_ContractAdd.txtBankGuarFDt, "Banker's Guarantee From Date", "M") == false) {
				    foundError=true
			    }
			}
			
			//validate Banker's Guarantee To Date
			if (!foundError && gfnCheckDate(document.Frm_ContractAdd.txtBankGuarTDt, "Banker's Guarantee To Date", "O") == false) {
				foundError=true
			}
			if (!foundError && !gfnIsFieldBlank(document.Frm_ContractAdd.txtBankGuarFDt)) {
			    if (!foundError && gfnCheckDate(document.Frm_ContractAdd.txtBankGuarTDt, "Banker's Guarantee To Date", "M") == false) {
				    foundError=true
			    }
			}
			
			//validate Banker's Guarantee Expiry Notification
			if (!foundError && !gfnIsFieldBlank(document.Frm_ContractAdd.txtBankGuarTDt)) {
			    if (!foundError && gfnCheckFrmToDt(document.Frm_ContractAdd.txtBankGuarFDt,document.Frm_ContractAdd.txtBankGuarTDt,'>') == false) {
			        foundError=true
			        document.Frm_ContractAdd.txtBankGuarTDt.focus()
			        alert("Banker's Guarantee To Date should be greater than Banker's Guarantee From Date.");
		        }
			    if (!foundError && document.Frm_ContractAdd.ddlBankGuaNoti.value == 0) {
			        foundError=true
			        document.Frm_ContractAdd.ddlBankGuaNoti.focus()
			        alert("Please select Banker's Guarantee Expiry Period Notification.")
			    }
			}
			
			//validate Name of Officer who signed the contract
			/*if (!foundError && gfnIsFieldBlank(document.Frm_ContractAdd.txtOffSignName)) {
				foundError=true;
				document.Frm_ContractAdd.txtOffSignName.focus();
				alert("Please enter Name of Officer who signed the contract.");
			}*/
			if (!foundError && gfnIsFieldBlank(document.Frm_ContractAdd.cbOffSignName)) {
                foundError=true
                alert("Please enter Name of Officer who signed the contract.")
            }
			
			//validate Designation of Officer who signed the contract 
			if (!foundError && gfnIsFieldBlank(document.Frm_ContractAdd.txtOffSignDes)) {
				foundError=true;
				document.Frm_ContractAdd.txtOffSignDes.focus();
				alert("Please enter Designation of Officer who signed the contract.");
			}
					  
						 
 			if (!foundError){
 			    var flag = false;
 			    var OverlapMsg = "";
 			    
 			    //*** Checking for overlapping of contract period...
// 			    alert("1= " + gfnCheckFrmToDt(document.Frm_ContractAdd.txtContFrmDt,document.Frm_ContractAdd.hdnOldCtrtFrmDt,'>'));
// 			    alert("2= " + gfnCheckFrmToDt(document.Frm_ContractAdd.hdnOldCtrtToDt,document.Frm_ContractAdd.txtContFrmDt,'>') );
// 			    alert("3= " + gfnCheckFrmToDt(document.Frm_ContractAdd.txtContToDt,document.Frm_ContractAdd.hdnOldCtrtFrmDt,'>') );
// 			    alert("4= " + gfnCheckFrmToDt(document.Frm_ContractAdd.hdnOldCtrtToDt,document.Frm_ContractAdd.txtContToDt,'>')  );
// 			    alert("5= " + gfnCheckFrmToDt(document.Frm_ContractAdd.hdnOldCtrtFrmDt,document.Frm_ContractAdd.txtContFrmDt,'>'));
// 			    alert("6= " + gfnCheckFrmToDt(document.Frm_ContractAdd.txtContToDt,document.Frm_ContractAdd.hdnOldCtrtToDt,'>')  );
 			    
 			    if ((gfnCheckFrmToDt(document.Frm_ContractAdd.txtContFrmDt,document.Frm_ContractAdd.hdnOldCtrtFrmDt,'>') == false  &&
 			         gfnCheckFrmToDt(document.Frm_ContractAdd.hdnOldCtrtToDt,document.Frm_ContractAdd.txtContFrmDt,'>')  == false) ||
 			        (gfnCheckFrmToDt(document.Frm_ContractAdd.txtContToDt,document.Frm_ContractAdd.hdnOldCtrtFrmDt,'>')  == false  &&
 			         gfnCheckFrmToDt(document.Frm_ContractAdd.hdnOldCtrtToDt,document.Frm_ContractAdd.txtContToDt,'>')   == false) ||
 			        (gfnCheckFrmToDt(document.Frm_ContractAdd.hdnOldCtrtFrmDt,document.Frm_ContractAdd.txtContFrmDt,'>') == false  &&
 			         gfnCheckFrmToDt(document.Frm_ContractAdd.txtContToDt,document.Frm_ContractAdd.hdnOldCtrtToDt,'>')   == false)) {
			        OverlapMsg = "Contract Period overlapped with previous Contract Period.\n";
		        }
 			    
 				flag = window.confirm(OverlapMsg + "Are you sure you want to add this contract?");
 				return flag;
 			}
			else
				return false;
		}
    </script>
</head>
<body>
    <form id="Frm_ContractAdd" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Contract Management : Renewal Contract</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" height="400" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																				    <TR>
																					    <TD width="35%">
																					        <FONT class="DisplayTitle">Contract ID : </FONT>
																					    </TD>
																					    <TD width="65%"><asp:Label ID=lblContractID runat=server></asp:Label></TD>
																				    </TR>
																					<TR>
																					    <TD width="35%">
																					        <FONT class="DisplayTitle">Contract Title : </FONT>
																					    </TD>
																					    <TD width="65%"><asp:Label ID=lblCtrtTitle runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Contract Cost (S$) : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="tctContCost" maxlength="250" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle"><font color="red">*</font>Contract Period : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtContFrmDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_ContractAdd.txtContFrmDt, document.Frm_ContractAdd.txtContToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtContToDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_ContractAdd.txtContFrmDt, document.Frm_ContractAdd.txtContToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Contract Expiry Period Notification (1st Reminder) : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlContExpNoti" Runat="server"></asp:DropDownList>&nbsp; Months
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Contract Expiry Period Notification (2nd Reminder) : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlContExpNoti2" Runat="server"></asp:DropDownList>&nbsp; Days
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD width="35%">
																					        <FONT class="DisplayTitle"><font color="red">*</font>Vendor Name : </FONT>
																					    </TD>
																					    <TD width="65%"><asp:TextBox id="txtVendorName" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle">Description: </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtContDesc" TextMode=MultiLine maxlength="1000" Columns="40" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtContDescWord" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="MMLValDesc" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Description to 1000 Characters." OutputControl="txtContDescWord" ControlToValidate="txtContDesc"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Name of Contract Owner : </FONT>
																					    </TD>
																					    <TD>																					        
																					        <asp:TextBox ID="cbOwner"  runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Department of Contract Owner : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlDepartment" Runat="server"></asp:DropDownList>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Designation of Contract Owner : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtOwnerDesg" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				     <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Security Deposit (S$) : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtSecurityDeposit" maxlength="250" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Security Deposit Period : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtSecurityDepFDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_ContractAdd.txtSecurityDepFDt, document.Frm_ContractAdd.txtSecurityDepTDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtSecurityDepTDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_ContractAdd.txtSecurityDepFDt, document.Frm_ContractAdd.txtSecurityDepTDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Security Deposit Expiry Period Notification : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlSecDpsNoti" Runat="server"></asp:DropDownList>&nbsp; Months
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Security Deposit Reference No. : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtSecDpsRefNo" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Banker's Guarantee Amount (S$) : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtBankGuarAmount" maxlength="250" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Banker's Guarantee Period : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtBankGuarFDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_ContractAdd.txtBankGuarFDt, document.Frm_ContractAdd.txtBankGuarTDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtBankGuarTDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_ContractAdd.txtBankGuarFDt, document.Frm_ContractAdd.txtBankGuarTDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Banker's Guarantee Expiry Period Notification : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlBankGuaNoti" Runat="server"></asp:DropDownList>&nbsp; Months
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Banker's Guarantee Reference No. : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtBankGuarRefNo" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Name of Officer who signed the contract : </FONT>
																					    </TD>
																					    <TD>
																					        <!--<asp:TextBox id="txtOffSignName" maxlength="250" Runat="server" Width="300px"></asp:TextBox>-->																					           
																					        <asp:TextBox ID="cbOffSignName" runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Designation of Officer who signed the contract : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtOffSignDes" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle">Remark : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtRemark" TextMode=MultiLine maxlength="1000" Columns="40" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtRemarkWord" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="Multilinetextboxvalidator1" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Remark to 1000 Characters." OutputControl="txtRemarkWord" ControlToValidate="txtRemark"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    <TR style="visibility:hidden">
																					    <TD valign=top>
																					        <FONT class="DisplayTitle">Upload Scanned Copy of Contract : </FONT>
																					    </TD>
																					    <TD >
																					        [<asp:LinkButton ID="butAddFile" runat="server">Add New File</asp:LinkButton>]
																						    <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgContractFile" Runat="server" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#CC9966" datakeyfield="fld_FileID" BackColor="White" BorderWidth="1px" CellPadding="4">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="#FFFFCC" Font-Bold="True" HorizontalAlign="Center"
											                                                        ></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn headertext="fld_FileID" datafield="fld_FileID"  Visible=False>
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="fld_FileSaveName" datafield="fld_FileSaveName"  Visible=False>
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                        </asp:boundcolumn>
										                                                            <asp:boundcolumn headertext="S/No" datafield="SNum" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="File Name" datafield="fld_FileName">
												                                                        <itemstyle width="70%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn ItemStyle-Width="10%" HeaderText="<IMG SRC=../images/audit.gif Border=0>" 
							                                                                            ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Center>
								                                                                        <itemtemplate>
									                                                                        <%#fnGetFileName(DataBinder.Eval(Container.DataItem, "fld_DCTMID"))%>
								                                                                        </itemtemplate>
							                                                                        </asp:templatecolumn>
																									<asp:ButtonColumn Text="&lt;IMG SRC=../images/delete.gif Border=0&gt;"
																										HeaderText="&lt;IMG SRC=../images/delete.gif Border=0&gt;" CommandName="Delete">
                                                                                                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                                                    </asp:ButtonColumn>									
										                                                        </Columns>
									                                                        </asp:datagrid>
                                                                                        </TD>
																				    </TR>
																				    <TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle">Asset Included : </FONT>
																					    </TD>
																					    <TD>
																					        [<asp:LinkButton ID=butAddAsset runat=server>Add Asset</asp:LinkButton>]
																						    <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgAsset" Runat="server" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
												                                                        <itemstyle width="25%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr">
												                                                        <itemstyle width="20%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Category" datafield="fld_CategoryName" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Subcategory" datafield="fld_CatSubName" >
												                                                        <itemstyle width="25%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>							
											                                                        <asp:ButtonColumn ItemStyle-Width="10%" Text="<IMG SRC=../images/delete.gif Border=0>" ItemStyle-HorizontalAlign="Center"
																										HeaderText="<IMG SRC=../images/delete.gif Border=0>" CommandName="Delete"></asp:ButtonColumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
									                                                     </TD>
																				    </TR>
																				    <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																				    <TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:Button id="butSubmit" Text="Submit" Runat="Server" />&nbsp;
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																				    <TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Contract History</b></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																						    <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgContract" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" AllowPaging="True" PageSize="10" PagerStyle-HorizontalAlign="Right"
										                                                        PagerStyle-VerticalAlign="Middle" PagerStyle-Mode="NumericPages" PagerStyle-Position="Top"
										                                                        PagerStyle-BackColor="#a3a9cc" datakeyfield="fld_ContractID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="fld_ContractID" datafield="fld_ContractID" Visible=false>
                                                                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
                                                                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="Contract ID" datafield="contractIDStr">
                                                                                                        <itemstyle width="7%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="Contract Title" datafield="fld_ContractTitle">
                                                                                                        <itemstyle width="20%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="Cost (S$)" datafield="fld_ContractCost">
                                                                                                        <itemstyle width="7%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="From Date" datafield="fld_ContractFDt">
                                                                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="To Date" datafield="fld_ContractTDt">
                                                                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="Owner" datafield="fld_OwnerFullname">
                                                                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="Department" datafield="fld_DepartmentCode">
                                                                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="Vendor Name" datafield="fld_ContractVendor">
                                                                                                        <itemstyle width="16%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:templatecolumn HeaderText="Select" ItemStyle-Width="5%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
												                                                        <itemtemplate>
													                                                        <%#fnShowViewIcon(DataBinder.Eval(Container.DataItem, "fld_ContractID"))%>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
			<!-- Start: Hidden Fields -->
			<!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
				src="../common/DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="Z-INDEX:101; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px">
			</iframe>
			<input type="hidden" id="hdnAddFileF" runat="server"> 
			<input type="hidden" id="hdnAddAssetF" runat="server"> 
			<input type="hidden" id="hdnNewAssetIDs" runat="server"> 
			<input type="hidden" id="hdnAssetIds" runat="server"> 
			<input type="hidden" id="hdnContractMstID" runat="server"> 
			<input type="hidden" id="hdnOldCtrtFrmDt" runat="server"> 
			<input type="hidden" id="hdnOldCtrtToDt" runat="server"> 
            <input type="hidden" id="hfOwner" runat="server"> 
            <input type="hidden" id="hfOffSignName" runat="server"> 
			<!-- End  : Hidden Fields -->
		</form>
</body>
</html>
