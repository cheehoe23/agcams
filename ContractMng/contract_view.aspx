<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="contract_view.aspx.vb" Inherits="AMS.contract_view" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script type="text/javascript">
	function fnConfirmDeleteRec() {
			var flag;
			flag = false;
												
			if (gfnCheckSelect("At least one record should be selected for deletion.")){
				flag = window.confirm("You have chosen to delete one or more contract(s).\nYou cannot undo this action. Continue?");
			}
			else  {
				flag = false;
			}
			return flag;
	}
	</script>
</head>
<body>
    <form id="Frm_ContractView" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Contract Management : <asp:Label ID=lblHeader runat=server></asp:Label></B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<table id="Table2" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tbody>
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr>
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td align="center"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td vAlign="top" width="100%">
						<!--Start Main Content-->
						<table height="400" cellSpacing="1" cellPadding="0" width="100%" align="left" border="0">
							<tr>
								<td vAlign="top" align="left">
								    <asp:button id="butPrintResult" Runat="server" Text="Print Search Result" Width="162px"></asp:button>
								    <asp:button id="butDelContract" Runat="server" Text="Delete Contract"></asp:button>
								</td>
							</tr>
							<tr>
								<td vAlign="top" align="right" bgColor="#a3a9cc">
									<table cellSpacing="1" cellPadding="1" width="100%" border="0">
										<tr align=left>
											<td width="20%">
											    <asp:dropdownlist id="ddlPageSize" Runat="server" AutoPostBack="True"></asp:dropdownlist>&nbsp;&nbsp;<font color="white">per page</font>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td vAlign="top" align="center" height="400">
									<!--Datagrid for display record.-->
									<asp:datagrid id="dgContract" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										ItemStyle-Height="25" AllowSorting="True" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" AllowPaging="True" PageSize="10" PagerStyle-HorizontalAlign="Right"
										PagerStyle-VerticalAlign="Middle" PagerStyle-Mode="NumericPages" PagerStyle-Position="Top"
										PagerStyle-BackColor="#a3a9cc" datakeyfield="fld_ContractID" AlternatingItemStyle-BackColor="#e3d9ee">
										<headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											Height="25"></headerstyle>
										<Columns>
										    <asp:boundcolumn HeaderText="fld_ContractID" datafield="fld_ContractID" Visible=false>
                                                <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
                                                <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn HeaderText="Contract ID" datafield="contractIDStr" SortExpression="contractIDStr">
                                                <itemstyle width="7%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn HeaderText="Contract Title" datafield="fld_ContractTitle" SortExpression="fld_ContractTitle">
                                                <itemstyle width="20%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn HeaderText="Cost (S$)" datafield="fld_ContractCost" SortExpression="fld_ContractCost">
                                                <itemstyle width="7%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn HeaderText="From Date" datafield="fld_ContractFDt" SortExpression="ContractFDt">
                                                <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn HeaderText="To Date" datafield="fld_ContractTDt" SortExpression="ContractTDt">
                                                <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn HeaderText="Contract Owner" datafield="fld_OwnerFullname" SortExpression="fld_OwnerFullname">
                                                <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn HeaderText="Department" datafield="fld_DepartmentCode" SortExpression="fld_DepartmentCode">
                                                <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn HeaderText="Vendor Name" datafield="fld_ContractVendor" SortExpression="fld_ContractVendor">
                                                <itemstyle width="16%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:templatecolumn HeaderText="Select" ItemStyle-Width="5%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
												<itemtemplate>
													<%#fnShowRenewalIcon(DataBinder.Eval(Container.DataItem, "fld_ContractMstID"))%>
												</itemtemplate>
											</asp:templatecolumn>
											<asp:templatecolumn HeaderText="Select" ItemStyle-Width="5%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
												<itemtemplate>
													<%#fnShowEditIcon(DataBinder.Eval(Container.DataItem, "fld_ContractID"))%>
												</itemtemplate>
											</asp:templatecolumn>
											<asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="5%">
												<headertemplate>
													<img src="../images/delete.gif" alt="Delete" runat="server" id="imgDelete">
													<asp:checkbox id="CheckAll" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
														runat="server" />
												</headertemplate>
												<itemtemplate>
													<center>
														<asp:checkbox id="DeleteThis" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
															runat="server" />
													</center>
												</itemtemplate>
											</asp:templatecolumn>
											<asp:boundcolumn HeaderText="fld_ContractModDelF" datafield="fld_ContractModDelF" Visible=false>
                                                <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                            </asp:boundcolumn>
										</Columns>
									</asp:datagrid>
								</td>
							</tr>
						</table>
						<!--End Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
				</tbody>
			</table>
			<!-- Start: Hidden Fields -->
			<input id="hdnSortName" type="hidden" runat="server"> 
			<input id="hdnSortAD" type="hidden" runat="server">
			<input id="hdnContID" type="hidden" runat="server"> 
			<input id="hdnContTitle" type="hidden" runat="server">
			<input id="hdnContCost" type="hidden" runat="server">
			<input id="hdnContSFrmDt" type="hidden" runat="server">
			<input id="hdnContSToDt" type="hidden" runat="server">
			<input id="hdnContEFrmDt" type="hidden" runat="server">
			<input id="hdnContEToDt" type="hidden" runat="server">
			<input id="hdnContNoti" type="hidden" runat="server">
			<input id="hdnVendorName" type="hidden" runat="server">
			<input id="hdnContOwner" type="hidden" runat="server">
			<input id="hdnContOwnerDept" type="hidden" runat="server">
			<input id="hdnSecDps" type="hidden" runat="server">
			<input id="hdnSecDpsSFrmDt" type="hidden" runat="server">
			<input id="hdnSecDpsSToDt" type="hidden" runat="server">
			<input id="hdnSecDpsEFrmDt" type="hidden" runat="server">
			<input id="hdnSecDpsEToDt" type="hidden" runat="server">
			<input id="hdnSecDpsNoti" type="hidden" runat="server">
			<input id="hdnSecDpsRefNo" type="hidden" runat="server">
			<input id="hdnBankGuaAmt" type="hidden" runat="server">
			<input id="hdnBankGuaSFrmDt" type="hidden" runat="server">
			<input id="hdnBankGuaSToDt" type="hidden" runat="server">
			<input id="hdnBankGuaEFrmDt" type="hidden" runat="server">
			<input id="hdnBankGuaEToDt" type="hidden" runat="server">
			<input id="hdnBankGuaNoti" type="hidden" runat="server">
			<input id="hdnBankGuaRefNo" type="hidden" runat="server">
			<input id="hdnOffSignName" type="hidden" runat="server">
			<input id="hdnOffSignDesg" type="hidden" runat="server">
			<input id="hdnAssetID" type="hidden" runat="server">
			<input id="hdnstrArchiveF" type="hidden" runat="server">
			<input id="hdnCallType" type="hidden" runat="server">
			<!-- End  : Hidden Fields -->
			<tagFooter:footer id="Footer" runat="server"></tagFooter:footer>
		</form>
</body>
</html>
