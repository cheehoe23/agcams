#Region "Information Section"
' ****************************************************************************************************
' Description       : View Contract 
' Purpose           : View Contract Information
' Date              : 27/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class contract_view
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butDelContract.Attributes.Add("OnClick", "return fnConfirmDeleteRec()")

                ''Get Value pass Search screen
                hdnCallType.Value = clsEncryptDecrypt.DecryptText(Request("CallType"))
                hdnContID.Value = clsEncryptDecrypt.DecryptText(Request("ContID"))
                hdnContTitle.Value = clsEncryptDecrypt.DecryptText(Request("ContTitle"))
                hdnContCost.Value = clsEncryptDecrypt.DecryptText(Request("ContCost"))
                hdnContSFrmDt.Value = clsEncryptDecrypt.DecryptText(Request("ContSFrmDt"))
                hdnContSToDt.Value = clsEncryptDecrypt.DecryptText(Request("ContSToDt"))
                hdnContEFrmDt.Value = clsEncryptDecrypt.DecryptText(Request("ContEFrmDt"))
                hdnContEToDt.Value = clsEncryptDecrypt.DecryptText(Request("ContEToDt"))
                hdnContNoti.Value = clsEncryptDecrypt.DecryptText(Request("ContNoti"))
                hdnVendorName.Value = clsEncryptDecrypt.DecryptText(Request("VendorName"))
                hdnContOwner.Value = clsEncryptDecrypt.DecryptText(Request("ContOwner"))
                hdnContOwnerDept.Value = clsEncryptDecrypt.DecryptText(Request("ContOwnerDept"))
                hdnSecDps.Value = clsEncryptDecrypt.DecryptText(Request("SecDps"))
                hdnSecDpsSFrmDt.Value = clsEncryptDecrypt.DecryptText(Request("SecDpsSFrmDt"))
                hdnSecDpsSToDt.Value = clsEncryptDecrypt.DecryptText(Request("SecDpsSToDt"))
                hdnSecDpsEFrmDt.Value = clsEncryptDecrypt.DecryptText(Request("SecDpsEFrmDt"))
                hdnSecDpsEToDt.Value = clsEncryptDecrypt.DecryptText(Request("SecDpsEToDt"))
                hdnSecDpsNoti.Value = clsEncryptDecrypt.DecryptText(Request("SecDpsNoti"))
                hdnSecDpsRefNo.Value = clsEncryptDecrypt.DecryptText(Request("SecDpsRefNo"))
                hdnBankGuaAmt.Value = clsEncryptDecrypt.DecryptText(Request("BankGuaAmt"))
                hdnBankGuaSFrmDt.Value = clsEncryptDecrypt.DecryptText(Request("BankGuaSFrmDt"))
                hdnBankGuaSToDt.Value = clsEncryptDecrypt.DecryptText(Request("BankGuaSToDt"))
                hdnBankGuaEFrmDt.Value = clsEncryptDecrypt.DecryptText(Request("BankGuaEFrmDt"))
                hdnBankGuaEToDt.Value = clsEncryptDecrypt.DecryptText(Request("BankGuaEToDt"))
                hdnBankGuaNoti.Value = clsEncryptDecrypt.DecryptText(Request("BankGuaNoti"))
                hdnBankGuaRefNo.Value = clsEncryptDecrypt.DecryptText(Request("BankGuaRefNo"))
                hdnOffSignName.Value = clsEncryptDecrypt.DecryptText(Request("OffSignName"))
                hdnOffSignDesg.Value = clsEncryptDecrypt.DecryptText(Request("OffSignDesg"))
                hdnAssetID.Value = clsEncryptDecrypt.DecryptText(Request("AssetID"))
                hdnstrArchiveF.Value = clsEncryptDecrypt.DecryptText(Request("ArchiveF"))

                ''*** Checking Calling From --> Renew = Renew Contract, else Maintain Contract
                If hdnCallType.Value = "Renew" Then
                    lblHeader.Text = "Renewal Contract"
                    butDelContract.Visible = False
                    dgContract.Columns(11).Visible = False
                    dgContract.Columns(12).Visible = False
                Else
                    lblHeader.Text = "Maintain Contract"
                    dgContract.Columns(10).Visible = False
                End If

                ''default Sorting
                hdnSortName.Value = "contractIDStr"
                hdnSortAD.Value = "ASC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            ''Checking For Assign Right 
            fnCheckAcessRight()

            '' Sort Header Display
            fnSortHeaderDisplay()
            
            ''Get Records
            dgContract.DataSource = clsContract.fnContract_SearchRecord( _
                            hdnContID.Value, hdnContTitle.Value, hdnContCost.Value, hdnContSFrmDt.Value, hdnContSToDt.Value, _
                            hdnContEFrmDt.Value, hdnContEToDt.Value, hdnContNoti.Value, hdnVendorName.Value, _
                            hdnContOwner.Value, hdnContOwnerDept.Value, hdnSecDps.Value, hdnSecDpsSFrmDt.Value, hdnSecDpsSToDt.Value, _
                            hdnSecDpsEFrmDt.Value, hdnSecDpsEToDt.Value, hdnSecDpsNoti.Value, hdnSecDpsRefNo.Value, _
                            hdnBankGuaAmt.Value, hdnBankGuaSFrmDt.Value, hdnBankGuaSToDt.Value, hdnBankGuaEFrmDt.Value, hdnBankGuaEToDt.Value, _
                            hdnBankGuaNoti.Value, hdnBankGuaRefNo.Value, hdnOffSignName.Value, hdnOffSignDesg.Value, _
                            hdnAssetID.Value, hdnstrArchiveF.Value, _
                            IIf(hdnCallType.Value = "Renew", "R", "M"), hdnSortName.Value, hdnSortAD.Value)
            dgContract.DataBind()
            If Not dgContract.Items.Count > 0 Then ''Not Records found
                ddlPageSize.Enabled = False
                butDelContract.Enabled = False
                lblErrorMessage.Text = "There is no record."
                lblErrorMessage.Visible = True
            Else
                ddlPageSize.Enabled = True
                If (InStr(Session("AR"), "ContractMng|Contact|delete") > 0 And hdnCallType.Value <> "Renew") Then butDelContract.Enabled = True
                lblErrorMessage.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub fnCheckAcessRight()
        Try
            ''*** For Maintain Contract ***
            If (InStr(Session("AR"), "ContractMng|Contact|edit") = 0) Then
                dgContract.Columns(11).Visible = False
            End If

            If (InStr(Session("AR"), "ContractMng|Contact|delete") = 0) Then
                butDelContract.Visible = False
                dgContract.Columns(12).Visible = False
            End If

            ''*** For Renew Contract ***
            If (InStr(Session("AR"), "ContractMng|RenewCtrt|renew") = 0) Then
                dgContract.Columns(10).Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By contractIDStr (2), fld_ContractTitle (3)
        ''                fld_ContractCost (4), ContractFDt (5)
        ''                ContractTDt (6), fld_OwnerFullname (7), 
        ''                fld_DepartmentCode(8),  fld_ContractVendor(9)
        Dim intSortIndex As Integer
        Dim strSortHeader As String = ""

        ''Set Default Header
        dgContract.Columns(2).HeaderText = "Contract ID"
        dgContract.Columns(3).HeaderText = "Contract Title"
        dgContract.Columns(4).HeaderText = "Cost (S$)"
        dgContract.Columns(5).HeaderText = "From Date"
        dgContract.Columns(6).HeaderText = "To Date"
        dgContract.Columns(7).HeaderText = "Owner"
        dgContract.Columns(8).HeaderText = "Department"
        dgContract.Columns(9).HeaderText = "Vendor Name"

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "contractIDStr"
                intSortIndex = 2
                strSortHeader = "Contract ID"
            Case "fld_ContractTitle"
                intSortIndex = 3
                strSortHeader = "Contract Title"
            Case "fld_ContractCost"
                intSortIndex = 4
                strSortHeader = "Cost (S$)"
            Case "ContractFDt"
                intSortIndex = 5
                strSortHeader = "From Date"
            Case "ContractTDt"
                intSortIndex = 6
                strSortHeader = "To Date"
            Case "fld_OwnerFullname"
                intSortIndex = 7
                strSortHeader = "Owner"
            Case "fld_DepartmentCode"
                intSortIndex = 8
                strSortHeader = "Department"
            Case "fld_ContractVendor"
                intSortIndex = 9
                strSortHeader = "Vendor Name"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgContract.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgContract.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Public Function fnShowRenewalIcon(ByVal strContractMstID As String) As String
        Try
            Dim strRetVal As String = ""

            '' Check For Edit Asset Access Right
            If (InStr(Session("AR"), "ContractMng|RenewCtrt|renew") > 0) Then
                strRetVal = "<a id=hlEditAsset href='contract_addRenewal.aspx?ContractMstID=" + strContractMstID + "' target=_self >" & _
                             "<img id=imgUpdate src=../images/update.gif alt='Click for renewal contract.' border=0 >" & _
                             "</a>"
            End If

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Public Function fnShowEditIcon(ByVal strContractID As String) As String
        Try
            Dim strRetVal As String = ""

            '' Check For Edit Asset Access Right
            If (InStr(Session("AR"), "ContractMng|Contact|edit") > 0) Then
                strRetVal = "<a id=hlEditAsset href='Contract_edit.aspx?ContractID=" + strContractID + "' target=_self >" & _
                             "<img id=imgUpdate src=../images/update.gif alt='Click for update contract.' border=0 >" & _
                             "</a>"
            End If

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub dgContract_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContract.ItemDataBound
        Dim cbDelete As CheckBox
        cbDelete = CType(e.Item.FindControl("DeleteThis"), CheckBox)
        If (InStr(Session("AR"), "ContractMng|Contact|delete") > 0) Then  '' Check For Delete Access Right
            If Not cbDelete Is Nothing Then
                ''If contract have "renwal contract" or "expiry", delete check box invisible
                If e.Item.Cells(13).Text = "N" Then
                    cbDelete.Enabled = False
                    cbDelete.Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub dgContract_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgContract.PageIndexChanged
        dgContract.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub dgContract_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgContract.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgContract.CurrentPageIndex = 0
        dgContract.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Protected Sub butDelContract_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelContract.Click
        Try
            Dim strMsg As String
            Dim retval As Integer

            ''*** Start: Get Contract that selected for deleted
            Dim GridItem As DataGridItem
            Dim chkSelectedDel As CheckBox
            Dim strContIDDel As String = ""
            Dim strContIDNameDel As String = ""
            For Each GridItem In dgContract.Items
                chkSelectedDel = CType(GridItem.Cells(12).FindControl("DeleteThis"), CheckBox)
                If chkSelectedDel.Checked Then
                    strContIDDel += GridItem.Cells(0).Text & "^"
                    strContIDNameDel += GridItem.Cells(2).Text & ", "
                End If
            Next
            If Trim(strContIDNameDel) <> "" Then strContIDNameDel = Trim(strContIDNameDel).Substring(0, Trim(strContIDNameDel).Length - 1)
            ''*** End  : Get Contract that selected for deleted

            ''Start: Delete selected records from database
            retval = clsContract.fnContract_DeleteContract(strContIDDel, Session("UsrID"))
            ''End  : Delete selected records from database

            If retval > 0 Then
                strMsg = "Selected record(s) deleted successfully."
                fnPopulateRecords()

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Delete Contract(s) : " + strContIDNameDel)

                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>alert('" + strMsg + "');</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "Selected record(s) deleted failed."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butPrintResult_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butPrintResult.Click
        Try
            Dim dsContract As New DataSet

            '' Set Session to blank
            Session("ContractRecord") = ""

            '' Get Record
            dsContract = clsContract.fnContract_SearchRecord( _
                            hdnContID.Value, hdnContTitle.Value, hdnContCost.Value, hdnContSFrmDt.Value, hdnContSToDt.Value, _
                            hdnContEFrmDt.Value, hdnContEToDt.Value, hdnContNoti.Value, hdnVendorName.Value, _
                            hdnContOwner.Value, hdnContOwnerDept.Value, hdnSecDps.Value, hdnSecDpsSFrmDt.Value, hdnSecDpsSToDt.Value, _
                            hdnSecDpsEFrmDt.Value, hdnSecDpsEToDt.Value, hdnSecDpsNoti.Value, hdnSecDpsRefNo.Value, _
                            hdnBankGuaAmt.Value, hdnBankGuaSFrmDt.Value, hdnBankGuaSToDt.Value, hdnBankGuaEFrmDt.Value, hdnBankGuaEToDt.Value, _
                            hdnBankGuaNoti.Value, hdnBankGuaRefNo.Value, hdnOffSignName.Value, hdnOffSignDesg.Value, _
                            hdnAssetID.Value, hdnstrArchiveF.Value, _
                            IIf(hdnCallType.Value = "Renew", "R", "M"), "contractIDStr", "ASC")

            '' Assign Dataset to session
            Session("ContractRecord") = dsContract

            ''Pop-up window
            Dim strJavascript As String = ""
            strJavascript = "<script language = 'Javascript'>" & _
                            "window.open('contract_printSearchResult.aspx', 'PrintContractSearchResult_Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1');" & _
                            "</script>"
            Response.Write(strJavascript)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class