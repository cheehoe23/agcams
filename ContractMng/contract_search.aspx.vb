#Region "Information Section"
' ****************************************************************************************************
' Description       : Search Contract 
' Purpose           : Search Contract Information
' Date              : 06/10/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Web.Services

#End Region

Partial Public Class contract_search
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            Me.Header.DataBind()

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            ''Get Owner (in-case page load again)           

            If Not Page.IsPostBack Then
                hdnCallType.Value = Request("CallType")
                Me.butSearch.Attributes.Add("OnClick", "return chkFrm()")

                ''** Populate Control
                fnPopulateCtrl()

                ''*** Checking Calling From --> Renew = Renew Contract, else Maintain Contract
                If hdnCallType.Value = "Renew" Then
                    lblHeader.Text = "Renewal Contract"
                    divArchive.Visible = False
                Else
                    lblHeader.Text = "Maintain Contract"
                    divArchive.Visible = True
                End If
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False)
            If Session("AdminF") = "N" Then ddlDepartment.Items.Remove(ddlDepartment.Items.FindByValue("-1"))

            ''Contract Expiry Notification
            ddlContExpNoti.Items.Add(New ListItem("", "-1"))
            fnPopulateNumberInDDL(ddlContExpNoti, "12")

            ''Security Deposit Expiry Notification
            ddlSecDpsNoti.Items.Add(New ListItem("", "-1"))
            fnPopulateNumberInDDL(ddlSecDpsNoti, "12")

            ''Banker's Guarantee Expiry Notification
            ddlBankGuaNoti.Items.Add(New ListItem("", "-1"))
            fnPopulateNumberInDDL(ddlBankGuaNoti, "12")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            Server.Transfer("contract_search.aspx?CallType=" + hdnCallType.Value)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
        Try
            Dim dsAsset As New DataSet
            Dim strURL As String = ""

            Dim strContID As String = Trim(txtContractID.Text)
            Dim strContTitle As String = Trim(txtContTitle.Text)
            Dim strContCost As String = Trim(tctContCost.Text)
            Dim strContSFrmDt As String = Trim(txtContSFrmDt.Text)
            Dim strContSToDt As String = Trim(txtContSToDt.Text)
            Dim strContEFrmDt As String = Trim(txtContEFrmDt.Text)
            Dim strContEToDt As String = Trim(txtContEToDt.Text)
            Dim intContNoti As Integer = ddlContExpNoti.SelectedValue
            Dim strVendorName As String = Trim(txtVendorName.Text)
            Dim strContOwner As String = ""
            Dim strContOwnerDept As Integer = IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue)

            Dim strSecDps As String = Trim(txtSecurityDeposit.Text)
            Dim strSecDpsSFrmDt As String = Trim(txtSecurityDepSFDt.Text)
            Dim strSecDpsSToDt As String = Trim(txtSecurityDepSTDt.Text)
            Dim strSecDpsEFrmDt As String = Trim(txtSecurityDepEFDt.Text)
            Dim strSecDpsEToDt As String = Trim(txtSecurityDepETDt.Text)
            Dim intSecDpsNoti As Integer = ddlSecDpsNoti.SelectedValue
            Dim strSecDpsRefNo As String = Trim(txtSecDpsRefNo.Text)

            Dim strBankGuaAmt As String = Trim(txtBankGuarAmount.Text)
            Dim strBankGuaSFrmDt As String = Trim(txtBankGuarSFDt.Text)
            Dim strBankGuaSToDt As String = Trim(txtBankGuarSTDt.Text)
            Dim strBankGuaEFrmDt As String = Trim(txtBankGuarEFDt.Text)
            Dim strBankGuaEToDt As String = Trim(txtBankGuarETDt.Text)
            Dim intBankGuaNoti As Integer = ddlBankGuaNoti.SelectedValue
            Dim strBankGuaRefNo As String = Trim(txtBankGuarRefNo.Text)

            Dim strOffSignName As String = "" ' Trim(txtOffSignName.Text)
            Dim strOffSignDesg As String = Trim(txtOffSignDes.Text)
            Dim strAssetID As String = Trim(txtAssetID.Text)
            Dim strArchiveF As String = rdArchiveF.SelectedValue
            Dim strCallFrm As String = IIf(hdnCallType.Value = "Renew", "R", "M")

            ''*** Get Contract Owner
            If Trim(cbOwner.Text) <> "" Then
                Dim strUsrExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbOwner.Text), strContOwner, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Contract Owner not found. Please select an existing Contract Owner."
                    lblErrorMessage.Visible = True
                    cbOwner.Focus()
                    Exit Sub
                End If
            End If

            ''*** Get Name of Officer who signed the contract
            If Trim(cbOffSignName.Text) <> "" Then
                Dim strOffExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbOffSignName.Text), strOffSignName, strOffExistF)
                If strOffExistF = "N" Then
                    lblErrorMessage.Text = "Name of Officer who signed the contract not found. Please select an existing Name of Officer who signed the contract."
                    lblErrorMessage.Visible = True
                    cbOffSignName.Focus()
                    Exit Sub
                End If
            End If

            strContID = strContID.Replace("'", "''")
            strContTitle = strContTitle.Replace("'", "''")
            strContCost = strContCost.Replace("'", "''")
            strContSFrmDt = strContSFrmDt.Replace("'", "''")
            strContSToDt = strContSToDt.Replace("'", "''")
            strContEFrmDt = strContEFrmDt.Replace("'", "''")
            strContEToDt = strContEToDt.Replace("'", "''")

            strVendorName = strVendorName.Replace("'", "''")
            strContOwner = strContOwner.Replace("'", "''")
            strSecDps = strSecDps.Replace("'", "''")

            strSecDpsSFrmDt = strSecDpsSFrmDt.Replace("'", "''")
            strSecDpsSToDt = strSecDpsSToDt.Replace("'", "''")
            strSecDpsEFrmDt = strSecDpsEFrmDt.Replace("'", "''")
            strSecDpsEToDt = strSecDpsEToDt.Replace("'", "''")
            strSecDpsRefNo = strSecDpsRefNo.Replace("'", "''")
            strBankGuaAmt = strBankGuaAmt.Replace("'", "''")

            strBankGuaSFrmDt = strBankGuaSFrmDt.Replace("'", "''")
            strBankGuaSToDt = strBankGuaSToDt.Replace("'", "''")
            strBankGuaEFrmDt = strBankGuaEFrmDt.Replace("'", "''")
            strBankGuaEToDt = strBankGuaEToDt.Replace("'", "''")
            strBankGuaRefNo = strBankGuaRefNo.Replace("'", "''")
            strOffSignName = strOffSignName.Replace("'", "''")

            strOffSignDesg = strOffSignDesg.Replace("'", "''")
            strAssetID = strAssetID.Replace("'", "''")

            dsAsset = clsContract.fnContract_SearchRecord( _
                            strContID, strContTitle, strContCost, strContSFrmDt, strContSToDt, _
                            strContEFrmDt, strContEToDt, intContNoti, strVendorName, _
                            strContOwner, strContOwnerDept, strSecDps, strSecDpsSFrmDt, strSecDpsSToDt, _
                            strSecDpsEFrmDt, strSecDpsEToDt, intSecDpsNoti, strSecDpsRefNo, _
                            strBankGuaAmt, strBankGuaSFrmDt, strBankGuaSToDt, strBankGuaEFrmDt, strBankGuaEToDt, _
                            intBankGuaNoti, strBankGuaRefNo, strOffSignName, strOffSignDesg, _
                            strAssetID, strArchiveF, strCallFrm, "contractIDStr", "ASC")


            If dsAsset.Tables(0).Rows.Count > 0 Then
                strURL = "contract_view.aspx?CallType=" & clsEncryptDecrypt.EncryptText(hdnCallType.Value) & "&ContID=" & clsEncryptDecrypt.EncryptText(strContID) & _
                    "&ContTitle=" & clsEncryptDecrypt.EncryptText(strContTitle) & _
                         "&ContCost=" & clsEncryptDecrypt.EncryptText(strContCost) & "&ContSFrmDt=" & clsEncryptDecrypt.EncryptText(strContSFrmDt) & "&ContSToDt=" & _
                         clsEncryptDecrypt.EncryptText(strContSToDt) & _
                         "&ContEFrmDt=" & clsEncryptDecrypt.EncryptText(strContEFrmDt) & "&ContEToDt=" & clsEncryptDecrypt.EncryptText(strContEToDt) & _
                         "&ContNoti=" & clsEncryptDecrypt.EncryptText(CStr(intContNoti)) & _
                         "&VendorName=" & clsEncryptDecrypt.EncryptText(strVendorName) & "&ContOwner=" & clsEncryptDecrypt.EncryptText(strContOwner) & "&ContOwnerDept=" & clsEncryptDecrypt.EncryptText(strContOwnerDept) & _
                         "&SecDps=" & clsEncryptDecrypt.EncryptText(strSecDps) & "&SecDpsSFrmDt=" & clsEncryptDecrypt.EncryptText(strSecDpsSFrmDt) & "&SecDpsSToDt=" & clsEncryptDecrypt.EncryptText(strSecDpsSToDt) & _
                         "&SecDpsEFrmDt=" & clsEncryptDecrypt.EncryptText(strSecDpsEFrmDt) & "&SecDpsEToDt=" & clsEncryptDecrypt.EncryptText(strSecDpsEToDt) & "&SecDpsNoti=" & clsEncryptDecrypt.EncryptText(CStr(intSecDpsNoti)) & _
                         "&SecDpsRefNo=" & clsEncryptDecrypt.EncryptText(strSecDpsRefNo) & "&BankGuaAmt=" & clsEncryptDecrypt.EncryptText(strBankGuaAmt) & "&BankGuaSFrmDt=" & clsEncryptDecrypt.EncryptText(strBankGuaSFrmDt) & _
                         "&BankGuaSToDt=" & clsEncryptDecrypt.EncryptText(strBankGuaSToDt) & "&BankGuaEFrmDt=" & clsEncryptDecrypt.EncryptText(strBankGuaEFrmDt) & "&BankGuaEToDt=" & clsEncryptDecrypt.EncryptText(strBankGuaEToDt) & _
                         "&BankGuaNoti=" & clsEncryptDecrypt.EncryptText(CStr(intBankGuaNoti)) & "&BankGuaRefNo=" & clsEncryptDecrypt.EncryptText(strBankGuaRefNo) & "&OffSignName=" & clsEncryptDecrypt.EncryptText(strOffSignName) & _
                         "&OffSignDesg=" & clsEncryptDecrypt.EncryptText(strOffSignDesg) & "&AssetID=" & clsEncryptDecrypt.EncryptText(strAssetID) & "&ArchiveF=" & (strArchiveF)

                Response.Redirect(strURL)
            Else
                lblErrorMessage.Visible = True
                lblErrorMessage.Text = "Record(s) not found."
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class