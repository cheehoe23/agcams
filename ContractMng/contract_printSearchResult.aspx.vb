Public Partial Class contract_printSearchResult
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If

            ''Get Today Date
            lblPrintDate.Text = mfnGetCurrentDateForTopPage()

            ''Bind record in datagrid
            dgContract.DataSource = CType(Session("ContractRecord"), DataSet)
            dgContract.DataBind()

            ''Set Session to blank
            Session("ContractRecord") = ""
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class