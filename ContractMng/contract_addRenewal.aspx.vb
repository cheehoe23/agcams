#Region "Information Section"
' ****************************************************************************************************
' Description       : Add renewal contract 
' Purpose           : Add renewal contract Information
' Date              : 17/12/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Services

#End Region

Partial Public Class contract_addRenewal
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********
            Me.Header.DataBind()

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            ''Get Owner (in-case page load again)
            'cbOwner.DataSource = fnGetUserList(cbOwner.List.PageSize, "", "")
            'cbOffSignName.DataSource = fnGetUserList(cbOffSignName.List.PageSize, "", "")

            If Not Page.IsPostBack Then
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")
                butAddAsset.Attributes.Add("OnClick", "return mfnOpenAddAssetWindow()")
                'butAddFile.Attributes.Add("OnClick", "return mfnOpenAddFileWindow()")
                butAddFile.Attributes.Add("OnClick", "return gfnOpenAddContractFileWindow('" & Session("UsrID") & "','0')")

                ''** Set Default 
                hdnContractMstID.Value = Request("ContractMstID")
                hdnAddFileF.Value = "N"
                hdnAddAssetF.Value = "N"
                hdnNewAssetIDs.Value = ""
                hdnAssetIds.Value = ""

                ''** Populate Control
                fnPopulateCtrl()

                ''** Populate Records
                fnPopulateRecord()

                '' Clear Temp File 
                fnClearTempContractFile(Server.MapPath("..\tempFile\ContractFile"), Session("UsrID"))

            End If

            '' Check "Add New File"
            'If hdnAddFileF.Value = "Y" Then
            fnPopulateFile()

            ''set back to default
            'hdnAddFileF.Value = "N"
            'End If

            '' Check "Add New Asset"
            If hdnAddAssetF.Value = "Y" Then
                ''add new asset to existing asset
                fnAddNewAssetIDs()

                ''populate Record
                fnPopulateAssetRecords()

                ''set back to default
                hdnAddAssetF.Value = "N"
                hdnNewAssetIDs.Value = ""
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateRecord()
        Dim objRdr As SqlDataReader
        Try
            ''Get Contract Record 
            objRdr = clsContract.fnContract_GetContractForRenewal(hdnContractMstID.Value)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()
                    lblContractID.Text = CStr(objRdr("contractID"))
                    lblCtrtTitle.Text = CStr(objRdr("fld_ContractTitle"))
                    hdnOldCtrtFrmDt.Value = CStr(objRdr("fld_ContractFDt"))
                    hdnOldCtrtToDt.Value = CStr(objRdr("fld_ContractTDt"))
                End If
            End If
            objRdr.Close()


            ''Get Contract History
            fnPopulateContractHistory()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Public Sub fnPopulateContractHistory()
        Try
            ''Get Contract History
            dgContract.DataSource = clsContract.fnContract_GetContractHistory("0", hdnContractMstID.Value)
            dgContract.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False)
            If Session("AdminF") = "N" Then ddlDepartment.Items.Remove(ddlDepartment.Items.FindByValue("-1"))

            ''Contract Expiry Notification (1st)
            fnPopulateNumberInDDL(ddlContExpNoti, "12")

            ''Contract Expiry Notification (2nd)
            fnPopulateNumberInDDL(ddlContExpNoti2, "30")
            ddlContExpNoti2.SelectedValue = "30"

            ''Security Deposit Expiry Notification
            fnPopulateNumberInDDL(ddlSecDpsNoti, "12")

            ''Banker's Guarantee Expiry Notification
            fnPopulateNumberInDDL(ddlBankGuaNoti, "12")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub fnPopulateFile()
        Try
            ''Get Records
            dgContractFile.DataSource = clsContract.fnContractFile_GetRec(Session("UsrID"), "0", "Y")
            dgContractFile.DataBind()
            If Not dgContractFile.Items.Count > 0 Then ''Not Records found
                dgContractFile.Visible = False
            Else
                dgContractFile.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnGetFileName(ByVal strFileNameSave As String) As String
        Try
            Dim strRetVal As String = ""
            'strRetVal += "<a id=hlFile href='../tempFile/ContractFile/" & strFileNameSave & "' target=_blank >" & _
            '             "<img id=imgView src=../images/audit.gif alt='Click for view file.' border=0 >" & _
            '             "</a>"
            strRetVal += "<a id=hlFile href='" & cnERegistryAddressString() & "download?id=" & strFileNameSave & "' target=ContractAddRNFile >" & _
                         "<img id=imgView src=../images/audit.gif alt='Click for view file.' border=0 >" & _
                         "</a>"

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub dgContractFile_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgContractFile.DeleteCommand
        Try
            Dim retval As Integer = "0"
            Dim strFileIDDel As String = e.Item.Cells(0).Text
            Dim strFileNameDel As String = e.Item.Cells(1).Text
            Dim strFileLocation As String = Server.MapPath("..\tempFile\ContractFile") & "\" & strFileNameDel

            ''Start: Delete selected records from database
            retval = clsContract.fnContractFile_DeleteRec(Session("UsrID"), strFileIDDel, "DP", "")
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 Then
                ''Delete File in AssetFile folder
                If File.Exists(strFileLocation) Then
                    File.Delete(strFileLocation)
                End If

                ''populate File Images
                fnPopulateFile()
            Else
                lblErrorMessage.Text = "File delete failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateAssetRecords() As Boolean
        Try
            ''Get Records
            If hdnAssetIds.Value <> "" Then
                dgAsset.DataSource = clsAsset.fnAssetGetAssetRecords(hdnAssetIds.Value, "fldAssetBarcode", "ASC")
                dgAsset.DataBind()
                If Not dgAsset.Items.Count > 0 Then ''Not Records found
                    dgAsset.Visible = False
                Else
                    dgAsset.Visible = True
                End If
            Else
                dgAsset.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub dgAsset_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAsset.DeleteCommand
        Try
            Dim strAssetIDDel As String = e.Item.Cells(0).Text
            Dim strNewAssetIDs As String = ""
            Dim strOldAssetIDS As String = hdnAssetIds.Value
            Dim arrAssetID As Array = Split(strOldAssetIDS, "^")
            Dim intCurAssetID As Integer = "0"

            For intCurAssetID = "0" To UBound(arrAssetID) - 1
                If Not strAssetIDDel = arrAssetID(intCurAssetID) Then
                    strNewAssetIDs = strNewAssetIDs + arrAssetID(intCurAssetID) + "^"
                End If
            Next

            ''Assign new asset to Hidden fields
            hdnAssetIds.Value = strNewAssetIDs

            ''Populate record
            fnPopulateAssetRecords()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnAddNewAssetIDs()
        Try

            Dim strNewAssetID As String = Trim(hdnNewAssetIDs.Value)
            Dim arrNewAssetID As Array
            Dim intCountNew As Integer = "0"
            Dim strOldAssetID As String = Trim(hdnAssetIds.Value)
            Dim arrOldAssetID As Array
            Dim intCountOld As Integer = "0"
            Dim strCombineAssetID As String = strOldAssetID
            Dim strAssetExist As String = "N"


            If Trim(hdnNewAssetIDs.Value) <> "" Then
                If strOldAssetID = "" Then
                    strCombineAssetID = strNewAssetID

                Else
                    arrNewAssetID = Split(Trim(strNewAssetID), "^")
                    arrOldAssetID = Split(Trim(strOldAssetID), "^")

                    For intCountNew = 0 To UBound(arrNewAssetID) - 1
                        strAssetExist = "N"

                        If Trim(arrNewAssetID(intCountNew)) <> "" Then
                            ''Loop Old AssetIDs to check whether already exist
                            For intCountOld = "0" To UBound(arrOldAssetID) - 1
                                If Trim(arrOldAssetID(intCountOld)) = Trim(arrNewAssetID(intCountNew)) Then
                                    strAssetExist = "Y"
                                End If
                            Next

                            '' If New Asset Id not exist in Old Asset ID, add in new AssetID to old AssetID
                            If strAssetExist = "N" Then
                                strCombineAssetID = strCombineAssetID + arrNewAssetID(intCountNew) + "^"
                            End If
                        End If
                    Next
                End If
            End If

            hdnAssetIds.Value = Trim(strCombineAssetID)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            Server.Transfer("contract_addRenewal.aspx?ContractMstID=" & hdnContractMstID.Value)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim intRetVal As Integer
            Dim strMsg As String

            Dim strContractCost As String = ""
            Dim strSecurityDeposit As String = ""
            Dim strBankGuaAmt As String = ""
            Dim strContractID As String = ""
            Dim strContractOwner As String = ""
            Dim strContractOwnerDept As Integer = IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue)
            Dim strOffSignName As String = ""

            ''*** Get Contract Owner
            If Trim(cbOwner.Text) <> "" Then
                'strOwner = cbOwner.SelectedRowValues(0)
                Dim strUsrExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbOwner.Text), strContractOwner, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Contract Owner not found. Please select an existing Contract Owner."
                    lblErrorMessage.Visible = True
                    cbOwner.Focus()
                    Exit Sub
                End If
            End If

            ''*** Get Name of Officer who signed the contract
            If Trim(cbOffSignName.Text) <> "" Then
                'strOwner = cbOwner.SelectedRowValues(0)
                Dim strOffExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbOffSignName.Text), strOffSignName, strOffExistF)
                If strOffExistF = "N" Then
                    lblErrorMessage.Text = "Name of Officer who signed the contract not found. Please select an existing Name of Officer who signed the contract."
                    lblErrorMessage.Visible = True
                    cbOffSignName.Focus()
                    Exit Sub
                End If
            End If

            ''*** Get Contract Cost
            If Trim(tctContCost.Text) <> "" Then
                strContractCost = Math.Round(CDec(tctContCost.Text), 2, MidpointRounding.AwayFromZero)
            End If

            ''*** Get Security Deposit
            If Trim(txtSecurityDeposit.Text) <> "" Then
                strSecurityDeposit = Math.Round(CDec(txtSecurityDeposit.Text), 2, MidpointRounding.AwayFromZero)
            End If

            ''*** Get Banker Guarantee Amount
            If Trim(txtBankGuarAmount.Text) <> "" Then
                strBankGuaAmt = Math.Round(CDec(txtBankGuarAmount.Text), 2, MidpointRounding.AwayFromZero)
            End If


            ''insert record
            intRetVal = clsContract.fnContract_InsertRenewalRecord( _
                               hdnContractMstID.Value, strContractCost, txtContFrmDt.Text, txtContToDt.Text, _
                               ddlContExpNoti.SelectedValue, txtVendorName.Text, txtContDesc.Text, strContractOwner, strContractOwnerDept, _
                               strSecurityDeposit, txtSecurityDepFDt.Text, txtSecurityDepTDt.Text, ddlSecDpsNoti.SelectedValue, _
                               txtSecDpsRefNo.Text, strBankGuaAmt, txtBankGuarFDt.Text, txtBankGuarTDt.Text, ddlBankGuaNoti.SelectedValue, _
                               txtBankGuarRefNo.Text, strOffSignName, txtOffSignDes.Text, txtRemark.Text, _
                               hdnAssetIds.Value, Session("UsrID"), strContractID, Trim(txtOwnerDesg.Text), ddlContExpNoti2.SelectedValue)

            If intRetVal > 0 Then
                strMsg = "Renewal Contract (" & strContractID & ") Added Successfully."

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Renewal Contract created : " + strContractID)

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "document.location.href='contract_search.aspx?CallType=Renew';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "Renewal Contract Added Failed."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub dgContract_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgContract.PageIndexChanged
        Try
            dgContract.CurrentPageIndex = e.NewPageIndex
            fnPopulateContractHistory()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnShowViewIcon(ByVal strContractID As String) As String
        Try
            Dim strRetVal As String = ""

            strRetVal = "<a id=hlViewCtrtDetails href=javascript:gfnViewContractDetails('" + strContractID + "')>" & _
                        "<img id=imgUpdate src=../images/audit.gif alt='Click for view contract details.' border=0 >" & _
                        "</a>"

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class