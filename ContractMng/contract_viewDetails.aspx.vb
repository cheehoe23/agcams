#Region "Information Section"
' ****************************************************************************************************
' Description       : View Contract Details 
' Purpose           : View Contract Details Information
' Date              : 19/12/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
#End Region

Partial Public Class contract_viewDetails
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("../common/logout.aspx")
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                hdnContractID.Value = Request("ContractID")
                Me.butClose.Attributes.Add("OnClick", "javascript:window.close()")

                ''Populate Records
                fnGetRecordsFromDB()

                ''Contract File
                fnPopulateFile()

                ''Populate Asset Included
                fnPopulateAssetRecords()
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnGetRecordsFromDB()
        Dim objRdr As SqlDataReader
        Try
            Dim strContractExpF As String = ""
            Dim strContractNo As String = ""

            ''Get Contract Record 
            objRdr = clsContract.fnContract_GetContractForEdit(hdnContractID.Value)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()
                    hdnContractMstID.Value = CStr(objRdr("fld_ContractMstID"))
                    lblContractID.Text = CStr(objRdr("contractIDStr"))
                    lblContTitle.Text = CStr(objRdr("fld_ContractTitle"))
                    lblContCost.Text = CStr(objRdr("fld_ContractCost"))
                    lblContFrmDt.Text = CStr(objRdr("fld_ContractFDt"))
                    lblContToDt.Text = CStr(objRdr("fld_ContractTDt"))
                    lblContExpNoti.Text = CStr(objRdr("fld_ContractExpEmailNotiMth"))
                    lblContExpNoti2.Text = CStr(objRdr("fld_ContractExpEmail2NotiDay"))
                    lblVendorName.Text = CStr(objRdr("fld_ContractVendor"))
                    lblContDesc.Text = Replace(CStr(objRdr("fld_ContractDesc")), vbCrLf, "<br>") 'CStr(objRdr("fld_ContractDesc"))

                    lblOwner.Text = CStr(objRdr("fld_OwnerFullname"))
                    lblDepartment.Text = CStr(objRdr("fld_DepartmentName")) & " (" & CStr(objRdr("fld_DepartmentCode")) & ")"
                    lblOwnerDesg.Text = CStr(objRdr("fld_OwnerDesg"))
                    lblSecurityDeposit.Text = CStr(objRdr("fld_SecurityDeposit"))
                    lblSecurityDepFDt.Text = CStr(objRdr("fld_SecurityDepositFrmDt"))
                    lblSecurityDepTDt.Text = CStr(objRdr("fld_SecurityDepositExpDt"))
                    lblSecDpsNoti.Text = CStr(objRdr("fld_SecurityDepositEmaillNotiMth"))
                    lblSecDpsRefNo.Text = CStr(objRdr("fld_SecurityDepositRefNo"))

                    lblBankGuarAmount.Text = CStr(objRdr("fld_BankGuaranteeAmt"))
                    lblBankGuarFDt.Text = CStr(objRdr("fld_BankGuaranteeFrmDt"))
                    lblBankGuarTDt.Text = CStr(objRdr("fld_BankGuaranteeExpDt"))
                    lblBankGuaNoti.Text = CStr(objRdr("fld_BankGuaranteeEmailNotiMth"))
                    lblBankGuarRefNo.Text = CStr(objRdr("fld_BankGuaranteeRefNo"))
                    lblOffSignName.Text = CStr(objRdr("fld_OffSignFullName"))   'CStr(objRdr("fld_OffSignName"))
                    lblOffSignDes.Text = CStr(objRdr("fld_OffSignDesg"))
                    lblRemark.Text = Replace(CStr(objRdr("fld_Remark")), vbCrLf, "<br>") 'CStr(objRdr("fld_Remark"))
                End If
            End If
            objRdr.Close()

            ''Populate Contract History
            fnPopulateContractHistory()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Public Sub fnPopulateContractHistory()
        Try
            ''Get Contract History
            dgContract.DataSource = clsContract.fnContract_GetContractHistory(hdnContractID.Value, hdnContractMstID.Value)
            dgContract.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub fnPopulateFile()
        Try
            ''Get Records
            dgContractFile.DataSource = clsContract.fnContractFile_GetRec("0", hdnContractID.Value, "N")
            dgContractFile.DataBind()
            If Not dgContractFile.Items.Count > 0 Then ''Not Records found
                dgContractFile.Visible = False
            Else
                dgContractFile.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnGetFileName(ByVal strFileNameSave As String) As String
        Try
            Dim strRetVal As String = ""
            'strRetVal += "<a id=hlFile href='../tempFile/ContractFile/" & strFileNameSave & "' target=_blank >" & _
            '             "<img id=imgView src=../images/audit.gif alt='Click for view file.' border=0 >" & _
            '             "</a>"
            strRetVal += "<a id=hlFile href='" & cnERegistryAddressString() & "download?id=" & strFileNameSave & "' target=ContractViewFile >" & _
                         "<img id=imgView src=../images/audit.gif alt='Click for view file.' border=0 >" & _
                         "</a>"

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Public Function fnPopulateAssetRecords() As Boolean
        Try
            ''Get Records
            dgAsset.DataSource = clsContract.fnContract_GetAssetRecords(hdnContractID.Value, "fldAssetBarcode", "ASC")
            dgAsset.DataBind()
            If Not dgAsset.Items.Count > 0 Then ''Not Records found
                dgAsset.Visible = False
            Else
                dgAsset.Visible = True
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class