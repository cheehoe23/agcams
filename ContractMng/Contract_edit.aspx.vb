#Region "Information Section"
' ****************************************************************************************************
' Description       : Modify Contract 
' Purpose           : Modify Contract Information
' Date              : 27/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Services

#End Region

Partial Public Class Contract_edit
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            Me.Header.DataBind()

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                hdnContractID.Value = Request("ContractID")
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")
                butAddAsset.Attributes.Add("OnClick", "return mfnOpenAddAssetWindow()")
                'butAddFile.Attributes.Add("OnClick", "return mfnOpenAddFileWindow()")
                butAddFile.Attributes.Add("OnClick", "return gfnOpenAddContractFileWindow('" & Session("UsrID") & "','" & hdnContractID.Value & "')")

                ''** Set Default 
                hdnAddFileF.Value = "N"
                hdnAddAssetF.Value = "N"
                hdnDelAssetF.Value = "N"
                hdnDelAssetID.Value = ""

                '' Clear Temp File 
                fnClearTempContractFile(Server.MapPath("..\tempFile\ContractFile"), Session("UsrID"))

                ''** Populate Control
                fnPopulateCtrl()

                ''Populate Records
                fnGetRecordsFromDB()

                ''Contract File
                'fnPopulateFile()

                ''Populate Asset Included
                fnPopulateAssetRecords()
            End If

            '' Check "Add New File"
            'If hdnAddFileF.Value = "Y" Then
            fnPopulateFile()

            ''set back to default
            'hdnAddFileF.Value = "N"
            'End If

            '' Check "Add New Asset"
            If hdnAddAssetF.Value = "Y" Then
                ''populate Record
                fnPopulateAssetRecords()

                ''set back to default
                hdnAddAssetF.Value = "N"
            End If

            '' Exclude Asset from Contract
            If hdnDelAssetF.Value = "Y" Then
                fnDelAssetFromContract()

                hdnDelAssetF.Value = "N"
                hdnDelAssetID.Value = ""
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False)
            If Session("AdminF") = "N" Then ddlDepartment.Items.Remove(ddlDepartment.Items.FindByValue("-1"))

            ''Contract Expiry Notification (1st)
            fnPopulateNumberInDDL(ddlContExpNoti, "12")

            ''Contract Expiry Notification (2nd)
            fnPopulateNumberInDDL(ddlContExpNoti2, "30")

            ''Security Deposit Expiry Notification
            fnPopulateNumberInDDL(ddlSecDpsNoti, "12")

            ''Banker's Guarantee Expiry Notification
            fnPopulateNumberInDDL(ddlBankGuaNoti, "12")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnGetRecordsFromDB()
        Dim objRdr As SqlDataReader
        Try
            Dim strContractExpF As String = ""
            Dim strContractNo As String = ""

            ''Get Contract Record 
            objRdr = clsContract.fnContract_GetContractForEdit(hdnContractID.Value)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()
                    hdnContractMstID.Value = CStr(objRdr("fld_ContractMstID").ToString())
                    lblContractID.Text = CStr(objRdr("contractIDStr").ToString())
                    txtContTitle.Text = CStr(objRdr("fld_ContractTitle").ToString())
                    tctContCost.Text = CStr(objRdr("fld_ContractCost").ToString())
                    txtContFrmDt.Text = CStr(objRdr("fld_ContractFDt").ToString())
                    txtContToDt.Text = CStr(objRdr("fld_ContractTDt").ToString())
                    ddlContExpNoti.SelectedValue = CStr(objRdr("fld_ContractExpEmailNotiMth").ToString())
                    ddlContExpNoti2.SelectedValue = CStr(objRdr("fld_ContractExpEmail2NotiDay").ToString())
                    txtVendorName.Text = CStr(objRdr("fld_ContractVendor").ToString())
                    txtContDesc.Text = CStr(objRdr("fld_ContractDesc").ToString())

                    cbOwner.Text = CStr(objRdr("fld_OwnerFullname").ToString()) + " (" + CStr(objRdr("fld_OwnerDepartment").ToString()) + ")"
                    'cbOwner.SelectedText = CStr(objRdr("fld_OwnerFullname"))
                    'cbOwner.SelectedIndexes = 1

                    'cbOwner.SelectedIndex = 1
                    ddlDepartment.SelectedValue = CStr(objRdr("fld_OwnerDept").ToString())
                    txtOwnerDesg.Text = CStr(objRdr("fld_OwnerDesg").ToString())
                    txtSecurityDeposit.Text = CStr(objRdr("fld_SecurityDeposit").ToString())
                    txtSecurityDepFDt.Text = CStr(objRdr("fld_SecurityDepositFrmDt").ToString())
                    txtSecurityDepTDt.Text = CStr(objRdr("fld_SecurityDepositExpDt").ToString())
                    ddlSecDpsNoti.SelectedValue = CStr(objRdr("fld_SecurityDepositEmaillNotiMth").ToString())
                    txtSecDpsRefNo.Text = CStr(objRdr("fld_SecurityDepositRefNo").ToString())

                    txtBankGuarAmount.Text = CStr(objRdr("fld_BankGuaranteeAmt").ToString())
                    txtBankGuarFDt.Text = CStr(objRdr("fld_BankGuaranteeFrmDt").ToString())
                    txtBankGuarTDt.Text = CStr(objRdr("fld_BankGuaranteeExpDt").ToString())
                    ddlBankGuaNoti.SelectedValue = CStr(objRdr("fld_BankGuaranteeEmailNotiMth").ToString())
                    txtBankGuarRefNo.Text = CStr(objRdr("fld_BankGuaranteeRefNo").ToString())
                    'txtOffSignName.Text = CStr(objRdr("fld_OffSignName"))   
                    cbOffSignName.Text = CStr(objRdr("fld_OffSignFullName").ToString()) + " (" + CStr(objRdr("fld_OffSignDepartment").ToString()) + ")"
                    txtOffSignDes.Text = CStr(objRdr("fld_OffSignDesg").ToString())
                    txtRemark.Text = CStr(objRdr("fld_Remark").ToString())

                    strContractExpF = CStr(objRdr("fld_ContractModDelF").ToString())
                    strContractNo = CStr(objRdr("fld_ContractNo").ToString())

                    hdnOldCtrtFrmDt.Value = CStr(objRdr("PrevCtrlFDt").ToString())
                    hdnOldCtrtToDt.Value = CStr(objRdr("PrevCtrlTDt").ToString())
                    hdnCtrtNo.Value = CStr(objRdr("fld_ContractNo").ToString())
                End If
            End If
            objRdr.Close()

            ''For Contract Number = "0", then allow to modify contract,
            ''Else (renewal contract) not allow to modify contract
            If strContractNo <> "0" Then
                txtContTitle.Enabled = False
            End If

            ''For contract already expiry or have renewal contract / no access right, contract not allow to edit
            If strContractExpF = "N" Or (InStr(Session("AR"), "ContractMng|Contact|edit") = 0) Then
                txtContTitle.Enabled = False
                tctContCost.Enabled = False
                txtContFrmDt.Enabled = False
                txtContToDt.Enabled = False
                linkCtrtFDt.Visible = False
                linkCtrtTDt.Visible = False
                ddlContExpNoti.Enabled = False
                ddlContExpNoti2.Enabled = False
                txtVendorName.Enabled = False
                txtContDesc.Enabled = False
                cbOwner.Enabled = False
                ddlDepartment.Enabled = False
                txtOwnerDesg.Enabled = False

                txtSecurityDeposit.Enabled = False
                txtSecurityDepFDt.Enabled = False
                txtSecurityDepTDt.Enabled = False
                linkSecDpsFDt.Visible = False
                linkSecDpsTDt.Visible = False
                ddlSecDpsNoti.Enabled = False
                txtSecDpsRefNo.Enabled = False

                txtBankGuarAmount.Enabled = False
                txtBankGuarFDt.Enabled = False
                txtBankGuarTDt.Enabled = False
                linkBankGuaFDt.Visible = False
                linkBankGuaTDt.Visible = False
                ddlBankGuaNoti.Enabled = False
                txtBankGuarRefNo.Enabled = False

                'txtOffSignName.Enabled = False
                cbOffSignName.Enabled = False
                txtOffSignDes.Enabled = False
                txtRemark.Enabled = False

                butAddFile.Enabled = False
                butAddAsset.Enabled = False
                butSubmit.Enabled = False
                butReset.Enabled = False

                dgContractFile.Columns(5).Visible = False
                dgAsset.Columns(7).Visible = False
            End If


            ''Populate Contract History
            fnPopulateContractHistory()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Public Sub fnPopulateContractHistory()
        Try
            ''Get Contract History
            dgContract.DataSource = clsContract.fnContract_GetContractHistory(hdnContractID.Value, hdnContractMstID.Value)
            dgContract.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnShowViewIcon(ByVal strContractID As String) As String
        Try
            Dim strRetVal As String = ""

            strRetVal = "<a id=hlViewCtrtDetails href=javascript:gfnViewContractDetails('" + strContractID + "')>" & _
                        "<img id=imgUpdate src=../images/audit.gif alt='Click for view contract details.' border=0 >" & _
                        "</a>"

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub fnPopulateFile()
        Try
            ''Get Records
            dgContractFile.DataSource = clsContract.fnContractFile_GetRec("0", hdnContractID.Value, "N")
            dgContractFile.DataBind()
            If Not dgContractFile.Items.Count > 0 Then ''Not Records found
                dgContractFile.Visible = False
            Else
                dgContractFile.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnGetFileName(ByVal strFileNameSave As String) As String
        Try
            Dim strRetVal As String = ""
            'strRetVal += "<a id=hlFile href='../tempFile/ContractFile/" & strFileNameSave & "' target=_blank >" & _
            '             "<img id=imgView src=../images/audit.gif alt='Click for view file.' border=0 >" & _
            '             "</a>"
            strRetVal += "<a id=hlFile href='" & cnERegistryAddressString() & "download?id=" & strFileNameSave & "' target=ContractMDFile >" & _
                         "<img id=imgView src=../images/audit.gif alt='Click for view file.' border=0 >" & _
                         "</a>"

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub dgContractFile_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgContractFile.DeleteCommand
        Try
            Dim retval As Integer = "0"
            Dim strFileIDDel As String = e.Item.Cells(0).Text
            Dim strFileNameDel As String = e.Item.Cells(1).Text
            Dim strFileNameReal As String = e.Item.Cells(3).Text
            Dim strFileLocation As String = Server.MapPath("..\tempFile\ContractFile") & "\" & strFileNameDel

            ''Start: Delete selected records from database
            retval = clsContract.fnContractFile_DeleteRec(Session("UsrID"), strFileIDDel, "DP", "")
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 Then
                ''Delete File in AssetFile folder
                If File.Exists(strFileLocation) Then
                    File.Delete(strFileLocation)
                End If

                ''populate File Images
                fnPopulateFile()

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "<font class=DisplayAudit>[Contract File Deleted]</font> <br>Contract ID : " + mfnGetContractIDFormat(CStr(hdnContractID.Value)) + "<br>File Name : " + strFileNameReal)

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('Contract File delete successfully.');" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                lblErrorMessage.Text = "File delete failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateAssetRecords() As Boolean
        Try
            ''Get Records
            dgAsset.DataSource = clsContract.fnContract_GetAssetRecords(hdnContractID.Value, "fldAssetBarcode", "ASC")
            dgAsset.DataBind()
            If Not dgAsset.Items.Count > 0 Then ''Not Records found
                dgAsset.Visible = False
            Else
                dgAsset.Visible = True
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnShowDeleteBut(ByVal strAssetID As String) As String
        Dim strRetVal As String = ""
        strRetVal = "<a id=hlDelRemark href='javascript:mfnDeleteAsset(" + strAssetID + ")'>" & _
                        "<img id=imgUpdate src=../images/delete.gif alt='Click for Exclude Asset from Contract.' border=0 >" & _
                        "</a>"

        Return (strRetVal)
    End Function

    Public Sub fnDelAssetFromContract()
        Try
            Dim intRetVal As Integer
            Dim strMsg As String
            Dim strDelFailF As String = ""

            intRetVal = clsContract.fnContract_DeleteAssetFromContract(hdnContractID.Value, hdnDelAssetID.Value, Session("UsrID"), strDelFailF)
            If strDelFailF = "Y" Then
                strMsg = "Contract much at least have 1 asset. You not allow to exclude last asset from contract. "
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            Else
                If intRetVal > 0 Then
                    strMsg = "Asset exclude from Contract Successfully "

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Asset exclude from contract (" + mfnGetContractIDFormat(hdnContractID.Value) + ").")

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)

                    ''Populate again record
                    fnPopulateAssetRecords()

                Else
                    strMsg = "Asset exclude from Contract Failed. Please contact your Administrator."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If

            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Sub dgAsset_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAsset.DeleteCommand
    '    Try
    '        'Dim strAssetIDDel As String = e.Item.Cells(0).Text
    '        'Dim strNewAssetIDs As String = ""
    '        'Dim strOldAssetIDS As String = hdnAssetIds.Value
    '        'Dim arrAssetID As Array = Split(strOldAssetIDS, "^")
    '        'Dim intCurAssetID As Integer = "0"

    '        'For intCurAssetID = "0" To UBound(arrAssetID) - 1
    '        '    If Not strAssetIDDel = arrAssetID(intCurAssetID) Then
    '        '        strNewAssetIDs = strNewAssetIDs + arrAssetID(intCurAssetID) + "^"
    '        '    End If
    '        'Next

    '        ' ''Assign new asset to Hidden fields
    '        'hdnAssetIds.Value = strNewAssetIDs

    '        ' ''Populate record
    '        'fnPopulateAssetRecords()
    '    Catch ex As Exception
    '        lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
    '        lblErrorMessage.Visible = True
    '    End Try
    'End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            ''** Set Default 
            'hdnAddFileF.Value = "N"
            'hdnAddAssetF.Value = "N"

            '' Clear Temp File 
            'fnClearTempContractFile(Server.MapPath("..\tempFile\ContractFile"), Session("UsrID"))

            ''Populate Records
            'fnGetRecordsFromDB()

            ' ''Contract File
            'fnPopulateFile()

            ' ''Populate Asset Included
            'fnPopulateAssetRecords()

            Response.Redirect("Contract_edit.aspx?ContractID=" + hdnContractID.Value)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim intRetVal As Integer
            Dim strMsg As String

            Dim strContractCost As String = ""
            Dim strSecurityDeposit As String = ""
            Dim strBankGuaAmt As String = ""
            Dim strContractOwner As String = ""
            Dim strContractOwnerDept As Integer = IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue)
            Dim strOffSignName As String = ""

            ''*** Get Contract Owner
            If Trim(cbOwner.Text) <> "" Then
                'strOwner = cbOwner.SelectedRowValues(0)
                Dim strUsrExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbOwner.Text), strContractOwner, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Contract Owner not found. Please select an existing Contract Owner."
                    lblErrorMessage.Visible = True
                    cbOwner.Focus()
                    Exit Sub
                End If
            End If

            ''*** Get Name of Officer who signed the contract
            If Trim(cbOffSignName.Text) <> "" Then
                'strOwner = cbOwner.SelectedRowValues(0)
                Dim strOffExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbOffSignName.Text), strOffSignName, strOffExistF)
                If strOffExistF = "N" Then
                    lblErrorMessage.Text = "Name of Officer who signed the contract not found. Please select an existing Name of Officer who signed the contract."
                    lblErrorMessage.Visible = True
                    cbOwner.Focus()
                    Exit Sub
                End If
            End If


            ''*** Get Contract Cost
            If Trim(tctContCost.Text) <> "" Then
                strContractCost = Math.Round(CDec(tctContCost.Text), 2, MidpointRounding.AwayFromZero)
            End If

            ''*** Get Security Deposit
            If Trim(txtSecurityDeposit.Text) <> "" Then
                strSecurityDeposit = Math.Round(CDec(txtSecurityDeposit.Text), 2, MidpointRounding.AwayFromZero)
            End If

            ''*** Get Banker Guarantee Amount
            If Trim(txtBankGuarAmount.Text) <> "" Then
                strBankGuaAmt = Math.Round(CDec(txtBankGuarAmount.Text), 2, MidpointRounding.AwayFromZero)
            End If


            ''insert record
            intRetVal = clsContract.fnContract_UpdateRecord( _
                               hdnContractID.Value, txtContTitle.Text, strContractCost, txtContFrmDt.Text, txtContToDt.Text, _
                               ddlContExpNoti.SelectedValue, txtVendorName.Text, txtContDesc.Text, strContractOwner, strContractOwnerDept, _
                               strSecurityDeposit, txtSecurityDepFDt.Text, txtSecurityDepTDt.Text, ddlSecDpsNoti.SelectedValue, _
                               txtSecDpsRefNo.Text, strBankGuaAmt, txtBankGuarFDt.Text, txtBankGuarTDt.Text, ddlBankGuaNoti.SelectedValue, _
                               txtBankGuarRefNo.Text, strOffSignName, txtOffSignDes.Text, txtRemark.Text, _
                               Session("UsrID"), Trim(txtOwnerDesg.Text), ddlContExpNoti2.SelectedValue)

            If intRetVal > 0 Then
                strMsg = "Contract Update Successfully."

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Contract updated : " + lblContractID.Text)

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "document.location.href='contract_search.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "Contract Update Failed."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function
End Class