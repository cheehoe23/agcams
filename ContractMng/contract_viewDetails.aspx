<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="contract_viewDetails.aspx.vb" Inherits="AMS.contract_viewDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
    
</head>
<body>
    <form id="frm_Contract" method="post" runat="server">
			<TABLE id="tableContract" cellSpacing="1" cellPadding="1" width="98%" align="center"
				bgColor="white" border="0">
				<TBODY>
					<TR>
						<TD colSpan="2"><B>Contract Details</B></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></TD>
					</TR>
					<tr>
						<td colSpan="2">
						    <TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
								width="100%" ID="Table6">
								<TBODY>
									                                                <TR>
																					    <TD width="40%">
																					        <FONT class="DisplayTitle">Contract ID : </FONT>
																					    </TD>
																					    <TD width="60%"><asp:Label ID=lblContractID runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Contract Title : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblContTitle runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Contract Cost (S$) : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblContCost runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Contract Period : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:Label ID=lblContFrmDt runat=server></asp:Label>
																						    &nbsp;&nbsp;&nbsp;&nbsp;
																						    To <asp:Label ID=lblContToDt runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Contract Expiry Period Notification (1st Reminder) : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:Label ID=lblContExpNoti runat=server></asp:Label> &nbsp; Months
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Contract Expiry Period Notification (2nd Reminder) : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:Label ID=lblContExpNoti2 runat=server></asp:Label> &nbsp; Days
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD width="35%">
																					        <FONT class="DisplayTitle">Vendor Name : </FONT>
																					    </TD>
																					    <TD width="65%"><asp:Label ID=lblVendorName runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle">Description: </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblContDesc runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Name of Contract Owner : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblOwner runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Department of Contract Owner : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblDepartment runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Designation of Contract Owner : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblOwnerDesg runat=server></asp:Label></TD>
																				    </TR>
																				     <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Security Deposit (S$) : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblSecurityDeposit runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Security Deposit Period : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:Label ID=lblSecurityDepFDt runat=server></asp:Label>
									                                                        &nbsp;&nbsp;&nbsp;&nbsp;
									                                                        To <asp:Label ID=lblSecurityDepTDt runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Security Deposit Expiry Period Notification : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblSecDpsNoti runat=server></asp:Label>&nbsp; Months</TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Security Deposit Reference No. : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblSecDpsRefNo runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Banker's Guarantee Amount (S$) : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblBankGuarAmount runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Banker's Guarantee Period : </FONT>
																					    </TD>
																					    <TD>
																					        From <asp:Label ID=lblBankGuarFDt runat=server></asp:Label>
									                                                        &nbsp;&nbsp;&nbsp;&nbsp;
									                                                        To <asp:Label ID=lblBankGuarTDt runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Banker's Guarantee Expiry Period Notification : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblBankGuaNoti runat=server></asp:Label>&nbsp; Months
																					    </TD>
																				    </TR>
																				     <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Banker's Guarantee Reference No. : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblBankGuarRefNo runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Name of Officer who signed the contract : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblOffSignName runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Designation of Officer who signed the contract : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblOffSignDes runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle">Remark : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblRemark runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle">Upload Scanned Copy of Contract : </FONT>
																					    </TD>
																					    <TD>
																					        <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgContractFile" Runat="server" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#CC9966" datakeyfield="fld_FileID" BackColor="White" BorderWidth="1px" CellPadding="4">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="#FFFFCC" Font-Bold="True" HorizontalAlign="Center"
											                                                        ></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn headertext="fld_FileID" datafield="fld_FileID"  Visible=False>
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="fld_FileSaveName" datafield="fld_FileSaveName"  Visible=False>
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                        </asp:boundcolumn>
										                                                            <asp:boundcolumn headertext="S/No" datafield="SNum" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="File Name" datafield="fld_FileName">
												                                                        <itemstyle width="70%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn ItemStyle-Width="10%" HeaderText="<IMG SRC=../images/audit.gif Border=0>" 
							                                                                            ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Center>
								                                                                        <itemtemplate>
									                                                                        <%#fnGetFileName(DataBinder.Eval(Container.DataItem, "fld_DCTMID"))%>
								                                                                        </itemtemplate>
							                                                                        </asp:templatecolumn>								
										                                                        </Columns>
									                                                        </asp:datagrid>
                                                                                        </TD>
																				    </TR>
																				    
																				    <TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle">Asset Included : </FONT>
																					    </TD>
																					    <TD><!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgAsset" Runat="server" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
												                                                        <itemstyle width="25%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr">
												                                                        <itemstyle width="20%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Category" datafield="fld_CategoryName" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Subcategory" datafield="fld_CatSubName" >
												                                                        <itemstyle width="25%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
									                                                     </TD>
																				    </TR>
																				    <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																				    <TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Contract History</b></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																						    <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgContract" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" AllowPaging="True" PageSize="10" PagerStyle-HorizontalAlign="Right"
										                                                        PagerStyle-VerticalAlign="Middle" PagerStyle-Mode="NumericPages" PagerStyle-Position="Top"
										                                                        PagerStyle-BackColor="#a3a9cc" datakeyfield="fld_ContractID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="fld_ContractID" datafield="fld_ContractID" Visible=false>
                                                                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
                                                                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="Contract ID" datafield="contractIDStr">
                                                                                                        <itemstyle width="7%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="Contract Title" datafield="fld_ContractTitle">
                                                                                                        <itemstyle width="20%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="Cost (S$)" datafield="fld_ContractCost">
                                                                                                        <itemstyle width="7%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="From Date" datafield="fld_ContractFDt">
                                                                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="To Date" datafield="fld_ContractTDt">
                                                                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="Owner" datafield="fld_OwnerFullname">
                                                                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="Department" datafield="fld_DepartmentCode">
                                                                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="Asset Included" datafield="fld_AssetBarcodesShow" Visible=false>
                                                                                                        <itemstyle width="16%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                                                                                                    </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="Vendor Name" datafield="fld_ContractVendor" >
                                                                                                        <itemstyle width="16%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                                                                                                    </asp:boundcolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:Button id="butClose" Text="Close" Runat="Server" />
																						</TD>
																					</TR>
								</TBODY>
							</TABLE>
					    </td>
					</tr>
				</TBODY>
			</TABLE>
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnContractID" runat="server"> 
			<input type="hidden" id="hdnContractMstID" runat="server"> 
			<!-- End  : Hidden Fields -->
		</form>
</body>
</html>
