<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="contract_search.aspx.vb" Inherits="AMS.contract_search" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<LINK href="../common/ComboBox/styles/contactsearch/combo.css" type="text/css" rel="Stylesheet">
    <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
    <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />
	<script type="text/javascript">

	    $(document).ready(function () {
	        SearchText("cbOwner", "hfOwner");
	        SearchText("cbOffSignName", "hfOffSignName");
	    });
	    function SearchText(obj1, obj2) {
	        $("#" + obj1).autocomplete({
	            source: function (request, response) {
	                $.ajax({
	                    url: '<%#ResolveUrl("~/ContractMng/contract_search.aspx/GetUsersList") %>',
	                    data: "{ 'prefix': '" + request.term + "'}",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    success: function (data) {
	                        response($.map(data.d, function (item) {
	                            return {
	                                label: item.split('~')[0],
	                                val: item.split('~')[1]
	                            }
	                        }))
	                    },
	                    error: function (response) {
	                        alert(response.responseText);
	                    },
	                    failure: function (response) {
	                        alert(response.responseText);
	                    }
	                });
	            },
	            select: function (e, i) {
	                $("#" + obj2).val(i.item.val);
	            },
	            minLength: 0
	        });

	    }
        
	    function chkFrm() {
	        var foundError = false;

	        //validate Contract ID
	        //		    if (!foundError && gfnIsFieldBlank(document.Frm_ContractSearch.txtContractID)== false) {
	        //			    if (!foundError && gfnCheckNumeric(document.Frm_ContractSearch.txtContractID,'')) {
	        //				    foundError=true;
	        //				    document.Frm_ContractSearch.txtContractID.focus();
	        //				    alert("Contract ID just allow number only.");
	        //			    } 
	        //		    }

	        //validate Cost
	        if (!foundError && gfnIsFieldBlank(document.Frm_ContractSearch.tctContCost) == false) {
	            if (!foundError && gfnCheckNumeric(document.Frm_ContractSearch.tctContCost, '.')) {
	                foundError = true;
	                document.Frm_ContractSearch.tctContCost.focus();
	                alert("Contract Cost just allow number only.");
	            }
	        }

	        //validate Contract start From date
	        if (!foundError && gfnCheckDate(document.Frm_ContractSearch.txtContSFrmDt, "Contract Start From Date", "O") == false) {
	            foundError = true
	        }

	        //validate Contract start To date
	        if (!foundError && gfnCheckDate(document.Frm_ContractSearch.txtContSToDt, "Contract Start To Date", "O") == false) {
	            foundError = true
	        }

	        //validate Contract End From date
	        if (!foundError && gfnCheckDate(document.Frm_ContractSearch.txtContEFrmDt, "Contract End From Date", "O") == false) {
	            foundError = true
	        }

	        //validate Contract End To date
	        if (!foundError && gfnCheckDate(document.Frm_ContractSearch.txtContEToDt, "Contract End To Date", "O") == false) {
	            foundError = true
	        }

	        //validate Security Deposit
	        if (!foundError && gfnIsFieldBlank(document.Frm_ContractSearch.txtSecurityDeposit) == false) {
	            if (!foundError && gfnCheckNumeric(document.Frm_ContractSearch.txtSecurityDeposit, '.')) {
	                foundError = true;
	                document.Frm_ContractSearch.txtSecurityDeposit.focus();
	                alert("Security Deposit just allow number only.");
	            }
	        }

	        //validate Security Deposit start From Date
	        if (!foundError && gfnCheckDate(document.Frm_ContractSearch.txtSecurityDepSFDt, "Security Deposit Start From Date", "O") == false) {
	            foundError = true
	        }

	        //validate Security Deposit start to Date
	        if (!foundError && gfnCheckDate(document.Frm_ContractSearch.txtSecurityDepSTDt, "Security Deposit Start To Date", "O") == false) {
	            foundError = true
	        }

	        //validate Security Deposit End From Date
	        if (!foundError && gfnCheckDate(document.Frm_ContractSearch.txtSecurityDepEFDt, "Security Deposit End From Date", "O") == false) {
	            foundError = true
	        }

	        //validate Security Deposit End To Date
	        if (!foundError && gfnCheckDate(document.Frm_ContractSearch.txtSecurityDepETDt, "Security Deposit End To Date", "O") == false) {
	            foundError = true
	        }

	        //validate Banker's Guarantee Amount
	        if (!foundError && gfnIsFieldBlank(document.Frm_ContractSearch.txtBankGuarAmount) == false) {
	            if (!foundError && gfnCheckNumeric(document.Frm_ContractSearch.txtBankGuarAmount, '.')) {
	                foundError = true;
	                document.Frm_ContractSearch.txtBankGuarAmount.focus();
	                alert("Banker's Guarantee Amount just allow number only.");
	            }
	        }

	        //validate Banker's Guarantee Start From Date
	        if (!foundError && gfnCheckDate(document.Frm_ContractSearch.txtBankGuarSFDt, "Banker's Guarantee Start From Date", "O") == false) {
	            foundError = true
	        }

	        //validate Banker's Guarantee Start To Date
	        if (!foundError && gfnCheckDate(document.Frm_ContractSearch.txtBankGuarSTDt, "Banker's Guarantee Start To Date", "O") == false) {
	            foundError = true
	        }

	        //validate Banker's Guarantee End From Date
	        if (!foundError && gfnCheckDate(document.Frm_ContractSearch.txtBankGuarEFDt, "Banker's Guarantee End From Date", "O") == false) {
	            foundError = true
	        }

	        //validate Banker's Guarantee End To Date
	        if (!foundError && gfnCheckDate(document.Frm_ContractSearch.txtBankGuarETDt, "Banker's Guarantee End To Date", "O") == false) {
	            foundError = true
	        }

	        if (!foundError) {
	            return true;
	        }
	        else
	            return false;
	    }
	</script>
</head>
<body>
    <form id="Frm_ContractSearch" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Contract Management : <asp:Label ID="lblHeader" runat=server></asp:Label></B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Contract ID : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtContractID" maxlength="20" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD width="35%">
																					        <FONT class="DisplayTitle">Contract Title : </FONT>
																					    </TD>
																					    <TD width="65%"><asp:TextBox id="txtContTitle" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Contract Cost (S$) : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="tctContCost" maxlength="250" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Contract Start Date : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtContSFrmDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_ContractSearch.txtContSFrmDt, document.Frm_ContractSearch.txtContSToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtContSToDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_ContractSearch.txtContSFrmDt, document.Frm_ContractSearch.txtContSToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Contract End Date : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtContEFrmDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_ContractSearch.txtContEFrmDt, document.Frm_ContractSearch.txtContEToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtContEToDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_ContractSearch.txtContEFrmDt, document.Frm_ContractSearch.txtContEToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Contract Expiry Period Notification (1st Reminder) : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlContExpNoti" Runat="server"></asp:DropDownList>&nbsp; Months
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD width="35%">
																					        <FONT class="DisplayTitle">Vendor Name : </FONT>
																					    </TD>
																					    <TD width="65%"><asp:TextBox id="txtVendorName" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Name of Contract Owner : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:TextBox ID="cbOwner"  runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Department of Contract Owner : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlDepartment" Runat="server"></asp:DropDownList>
																					    </TD>
																				    </TR>
																				     <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Security Deposit (S$) : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtSecurityDeposit" maxlength="250" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Security Deposit Start Date : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtSecurityDepSFDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_ContractSearch.txtSecurityDepSFDt, document.Frm_ContractSearch.txtSecurityDepSTDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtSecurityDepSTDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_ContractSearch.txtSecurityDepSFDt, document.Frm_ContractSearch.txtSecurityDepSTDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Security Deposit End Date : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtSecurityDepEFDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_ContractSearch.txtSecurityDepEFDt, document.Frm_ContractSearch.txtSecurityDepETDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtSecurityDepETDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_ContractSearch.txtSecurityDepEFDt, document.Frm_ContractSearch.txtSecurityDepETDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Security Deposit Expiry Period Notification : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlSecDpsNoti" Runat="server"></asp:DropDownList>&nbsp; Months
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Security Deposit Reference No. : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtSecDpsRefNo" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Banker's Guarantee Amount (S$) : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtBankGuarAmount" maxlength="250" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Banker's Guarantee Start Date : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtBankGuarSFDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_ContractSearch.txtBankGuarSFDt, document.Frm_ContractSearch.txtBankGuarSTDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtBankGuarSTDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_ContractSearch.txtBankGuarSFDt, document.Frm_ContractSearch.txtBankGuarSTDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Banker's Guarantee End Date : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtBankGuarEFDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_ContractSearch.txtBankGuarEFDt, document.Frm_ContractSearch.txtBankGuarETDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtBankGuarETDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_ContractSearch.txtBankGuarEFDt, document.Frm_ContractSearch.txtBankGuarETDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Banker's Guarantee Expiry Period Notification : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlBankGuaNoti" Runat="server"></asp:DropDownList>&nbsp; Months
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Banker's Guarantee Reference No. : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtBankGuarRefNo" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Name of Officer who signed the contract : </FONT>
																					    </TD>
																					    <TD>
																					        <!--<asp:TextBox id="txtOffSignName" maxlength="250" Runat="server" Width="300px"></asp:TextBox>-->
																					        <asp:TextBox ID="cbOffSignName" runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Designation of Officer who signed the contract : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtOffSignDes" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Asset ID Included : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:TextBox id="txtAssetID" maxlength="20" Runat="server"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <tr>
																				        <td colspan=2>
																				            <div id=divArchive runat=server>
																				                <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table9">
																				                     <TR>
																					                    <TD width="35%">
																						                    <FONT class="DisplayTitle">Include Archive : </FONT>
																					                    </TD>
																					                    <TD width="65%">
																						                    <asp:radiobuttonlist id="rdArchiveF" Runat="server" RepeatDirection="Horizontal">
																								                <asp:ListItem Value="Y">Yes</asp:ListItem>
																								                <asp:ListItem Value="N" Selected>No</asp:ListItem>
																							                </asp:radiobuttonlist>
																					                    </TD>
																				                    </TR>       
																				                </TABLE>
																				            </div>
																				        </td>
																				    </tr>
																				    <tr>
																				        <td colspan=2>&nbsp;</td>
																				    </tr>
																					<TR>
																						<TD align="center" colspan="2" style="height: 38px"><BR>
																							<asp:Button id="butSearch" Text="Search" Runat="Server" />&nbsp;
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnCallType" runat="server" >
            <input type="hidden" id="hfOwner" runat="server" > 
            <input type="hidden" id="hfOffSignName" runat="server" > 
			<!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
				src="../common/DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="Z-INDEX:101; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px">
			</iframe>
			<!-- End  : Hidden Fields -->
		</form>
</body>
</html>
