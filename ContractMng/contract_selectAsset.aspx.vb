#Region "Information Section"
' ****************************************************************************************************
' Description       : Search Asset 
' Purpose           : Search Asset Information
' Date              : 26/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Web.Services

#End Region

Partial Public Class contract_addAsset
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("~/common/logout.aspx", False)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            Me.Header.DataBind()

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butAdd.Attributes.Add("OnClick", "return ConfirmAddAsset()")

                ''Get pass value from main screen
                hdnCallFrm.Value = Request("CallFrm")         ''Called from which screen
                hdnContractID.Value = Request("ContractID")   ''Just for Called From "Edit contract" screen

                ''set Default 
                hdnAssetAddF.Value = "N"
                hdnNewAssetIDs.Value = ""
                divSearchResult.Visible = False

                ''Populate Control
                fnPopulateCtrl()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try

            ''Get Asset Type --> Fixed Asset, Inventory
            fnPopulateAssetTypeInDDL(ddlAssetType)

            ''Get Category
            fnPopulateDropDownList(clsCategory.fnCategoryGetCategory(), ddlAssetCat, "fld_CategoryID", "fld_CatCodeName", False)

            ''Get SubCategory (set default --> -1)
            ddlAssetSubCat.Items.Insert(0, New ListItem("", "-1"))

            ''Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False)
            If Session("AdminF") = "N" Then ddlDepartment.Items.Remove(ddlDepartment.Items.FindByValue("-1"))

            ''Get Location
            fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLocation, "fld_LocationID", "fld_LoctCodeName", False)

            ''Get Sub Location
            ddlLocation_SelectedIndexChanged(Nothing, Nothing)

      
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ddlAssetCat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAssetCat.SelectedIndexChanged
        Try
            If ddlAssetCat.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsCategory.fnCategoryGetAllRecDR(ddlAssetCat.SelectedValue, 0, "fld_CatSubName", "ASC"), ddlAssetSubCat, "fld_CatSubID", "fld_CatSubName", False)
            Else
                ddlAssetSubCat.Items.Clear()
                ddlAssetSubCat.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSearchCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearchCancel.Click
        Try
            Dim strJavaScript As String
            strJavaScript = "<script language = 'Javascript'>" & _
                            "window.close();" & _
                            "</script>"
            Response.Write(strJavaScript)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSearchReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearchReset.Click
        Try
            ddlAssetType.SelectedValue = ""
            ddlAssetCat.SelectedValue = "-1"
            ddlAssetCat_SelectedIndexChanged(Nothing, Nothing)
            If Session("AdminF") = "Y" Then ddlDepartment.SelectedValue = "-1"
            ddlLocation.SelectedValue = "-1"
            ddlLocation_SelectedIndexChanged(Nothing, Nothing)
            cbOwner.Text = ""

            divSearchResult.Visible = False
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
        Try
            Dim strAssetType As String = ddlAssetType.SelectedValue
            Dim intAssetCatID As Integer = IIf(ddlAssetCat.SelectedValue = "-1", "0", ddlAssetCat.SelectedValue)
            Dim intAssetCatSubID As Integer = IIf(ddlAssetSubCat.SelectedValue = "-1", "0", ddlAssetSubCat.SelectedValue)
            Dim intDeptID As Integer = IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue)
            Dim intLocID As Integer = IIf(ddlLocation.SelectedValue = "-1", "0", ddlLocation.SelectedValue)
            Dim intLocSubID As Integer = IIf(ddlLocSub.SelectedValue = "-1", "0", ddlLocSub.SelectedValue)
            Dim strOwnerID As String = "" 'IIf(ddlOwner.SelectedValue = "-1", "", ddlOwner.SelectedValue)

            ''*** Get Owner
            If Trim(cbOwner.Text) <> "" Then
                'strOwner = cbOwner.SelectedRowValues(0)
                Dim strUsrExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbOwner.Text), strOwnerID, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Owner not found. Please select an existing Owner."
                    lblErrorMessage.Visible = True
                    Exit Sub
                End If
            End If

            dgAsset.DataSource = clsContract.fnContract_SearchAsset( _
                                     strAssetType, intAssetCatID, intAssetCatSubID, _
                                     intDeptID, intLocID, strOwnerID, intLocSubID)
            dgAsset.DataBind()
            If Not dgAsset.Items.Count > 0 Then ''Records found
                divSearchResult.Visible = False
                lblErrorMessage.Text = "Not record found."
                lblErrorMessage.Visible = True
            Else
                divSearchResult.Visible = True
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAdd.Click
        Try

            ''*** Start: Get selected record
            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox
            Dim strSelectAssetID As String = ""
            For Each GridItem In dgAsset.Items
                chkSelectedRec = CType(GridItem.Cells(6).FindControl("DeleteThis"), CheckBox)
                If chkSelectedRec.Checked Then
                    strSelectAssetID += GridItem.Cells(0).Text & "^"
                End If
            Next
            ''*** End  : Get selected record


            If hdnCallFrm.Value = "AddContact" Or hdnCallFrm.Value = "RenewalContract" Then
                hdnAssetAddF.Value = "Y"
                hdnNewAssetIDs.Value = strSelectAssetID
            Else
                ''Check whther have duplicate record selected
                Dim strDuplicateAsset As String = ""
                clsContract.fncontract_SearchAssetDuplicate(hdnContractID.Value, strSelectAssetID, strDuplicateAsset)
                If Trim(strDuplicateAsset) <> "" Then  ' duplicate found
                    lblErrorMessage.Text = "Asset ID (" + strDuplicateAsset + ") have been assigned for contract."
                    lblErrorMessage.Visible = True
                Else
                    ''Not duplicate record, add asset for contract
                    Dim intRetVal As Integer = "0"
                    Dim strAssetBarcode As String = ""
                    Dim strMsg As String = ""

                    ''insert record
                    intRetVal = clsContract.fnContract_AddAsset( _
                                hdnContractID.Value, strSelectAssetID, Session("UsrID"), strAssetBarcode)

                    If intRetVal > 0 Then
                        hdnAssetAddF.Value = "Y"
                        strMsg = "Asset(s) Added to Contract Successfully."

                        ''Insert Audit Trail
                        clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "<font class=DisplayAudit>[Asset Added to Contract]</font> <br>Contract ID : " + mfnGetContractIDFormat(CStr(hdnContractID.Value)) + "<br> Asset ID(s) : " + strAssetBarcode)

                        ''Message Notification
                        Dim strJavaScript As String
                        strJavaScript = "<script language = 'Javascript'>" & _
                                        "alert('" + strMsg + "');" & _
                                        "</script>"
                        Response.Write(strJavaScript)
                    Else
                        strMsg = "Asset(s) Added to Contract Failed."
                        lblErrorMessage.Text = strMsg
                        lblErrorMessage.Visible = True
                    End If
                End If
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLocation.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCodeName", False)
            Else
                ddlLocSub.Items.Clear()
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class