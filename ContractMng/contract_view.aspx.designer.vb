'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class contract_view

    '''<summary>
    '''Frm_ContractView control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Frm_ContractView As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''lblHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblHeader As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblErrorMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblErrorMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''butPrintResult control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents butPrintResult As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''butDelContract control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents butDelContract As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''ddlPageSize control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPageSize As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''dgContract control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dgContract As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''hdnSortName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnSortName As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnSortAD control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnSortAD As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnContID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnContID As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnContTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnContTitle As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnContCost control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnContCost As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnContSFrmDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnContSFrmDt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnContSToDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnContSToDt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnContEFrmDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnContEFrmDt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnContEToDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnContEToDt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnContNoti control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnContNoti As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnVendorName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnVendorName As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnContOwner control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnContOwner As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnContOwnerDept control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnContOwnerDept As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnSecDps control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnSecDps As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnSecDpsSFrmDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnSecDpsSFrmDt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnSecDpsSToDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnSecDpsSToDt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnSecDpsEFrmDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnSecDpsEFrmDt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnSecDpsEToDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnSecDpsEToDt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnSecDpsNoti control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnSecDpsNoti As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnSecDpsRefNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnSecDpsRefNo As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnBankGuaAmt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnBankGuaAmt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnBankGuaSFrmDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnBankGuaSFrmDt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnBankGuaSToDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnBankGuaSToDt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnBankGuaEFrmDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnBankGuaEFrmDt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnBankGuaEToDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnBankGuaEToDt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnBankGuaNoti control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnBankGuaNoti As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnBankGuaRefNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnBankGuaRefNo As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnOffSignName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnOffSignName As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnOffSignDesg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnOffSignDesg As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnAssetID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnAssetID As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnstrArchiveF control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnstrArchiveF As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnCallType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnCallType As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Footer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Footer As Global.AMS.footer_cr
End Class
