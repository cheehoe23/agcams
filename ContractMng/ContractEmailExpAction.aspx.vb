#Region "Information Section"
' ****************************************************************************************************
' Description       : contract expiry action 
' Purpose           : contract expiry action Information
' Date              : 29/07/2009
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
#End Region

Partial Public Class ContractEmailExpAction
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                ''Get Default Value
                hdnContractID.Value = Request("CID")
                hdnContractAct.Value = Request("CA")

                ''get Window Login ID
                Dim arrWin As Array
                arrWin = Request.ServerVariables("logon_user").Split("\")
                Dim strLoginId As String = arrWin(1)

                ''checking for "just contract owner allow to take this action" and update action
                Dim intRetVal As Integer
                Dim strErrorF As String = ""
                intRetVal = clsContract.fnContractRmd_UpdateActionNchecking( _
                                     hdnContractID.Value, strLoginId, hdnContractAct.Value, strErrorF)
                If strErrorF = "3" Then
                    lblNAuthorize.Text = "Contract not longer existed in system."
                    divContractDetail.Visible = False
                    divNotAuthoriszed.Visible = True

                ElseIf strErrorF = "1" Then
                    lblNAuthorize.Text = "You are not authorized to view this page."
                    divContractDetail.Visible = False
                    divNotAuthoriszed.Visible = True

                Else
                    divContractDetail.Visible = False
                    divNotAuthoriszed.Visible = False

                    If intRetVal > 0 Then
                        ''Action Status update successfully
                        If hdnContractAct.Value = "N" Then
                            lblErrorMessage.Text = "You have chosen contract which will not be renewed." '"You have choosing contract not be renewed."
                            lblErrorMessage.Visible = True
                        Else
                            lblErrorMessage.Text = "You have chosen contract which is already in the process of renewal." '"You have choosing contract is already in process of renewal."
                            lblErrorMessage.Visible = True
                        End If
                    Else
                        ''Action Status update failed
                        lblErrorMessage.Text = "Contract Email Reminder Action update failed. Please contact your administrator."
                        lblErrorMessage.Visible = True
                    End If

                    ''Display Record
                    fnGetRecordsFromDB()

                    ''Contract File
                    fnPopulateFile()

                    ''Populate Asset Included
                    fnPopulateAssetRecords()
                End If

            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnGetRecordsFromDB()
        Dim objRdr As SqlDataReader
        Try
            ''Get Contract Record 
            objRdr = clsContract.fnContract_GetContractForEdit(hdnContractID.Value)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()

                    ''Display Contract Details
                    divContractDetail.Visible = True

                    ''Get Contract Details
                    lblContractID.Text = CStr(objRdr("contractIDStr"))
                    lblContTitle.Text = CStr(objRdr("fld_ContractTitle"))
                    lblContCost.Text = CStr(objRdr("fld_ContractCost"))
                    lblContFrmDt.Text = CStr(objRdr("fld_ContractFDt"))
                    lblContToDt.Text = CStr(objRdr("fld_ContractTDt"))
                    lblContExpNoti.Text = CStr(objRdr("fld_ContractExpEmailNotiMth"))
                    lblContExpNoti2.Text = CStr(objRdr("fld_ContractExpEmail2NotiDay"))
                    lblVendorName.Text = CStr(objRdr("fld_ContractVendor"))
                    lblContDesc.Text = Replace(CStr(objRdr("fld_ContractDesc")), vbCrLf, "<br>") 'CStr(objRdr("fld_ContractDesc"))

                    lblOwner.Text = CStr(objRdr("fld_OwnerFullname"))
                    lblDepartment.Text = CStr(objRdr("fld_DepartmentName")) & " (" & CStr(objRdr("fld_DepartmentCode")) & ")"
                    lblOwnerDesg.Text = CStr(objRdr("fld_OwnerDesg"))
                    lblSecurityDeposit.Text = CStr(objRdr("fld_SecurityDeposit"))
                    lblSecurityDepFDt.Text = CStr(objRdr("fld_SecurityDepositFrmDt"))
                    lblSecurityDepTDt.Text = CStr(objRdr("fld_SecurityDepositExpDt"))
                    lblSecDpsNoti.Text = CStr(objRdr("fld_SecurityDepositEmaillNotiMth"))
                    lblSecDpsRefNo.Text = CStr(objRdr("fld_SecurityDepositRefNo"))

                    lblBankGuarAmount.Text = CStr(objRdr("fld_BankGuaranteeAmt"))
                    lblBankGuarFDt.Text = CStr(objRdr("fld_BankGuaranteeFrmDt"))
                    lblBankGuarTDt.Text = CStr(objRdr("fld_BankGuaranteeExpDt"))
                    lblBankGuaNoti.Text = CStr(objRdr("fld_BankGuaranteeEmailNotiMth"))
                    lblBankGuarRefNo.Text = CStr(objRdr("fld_BankGuaranteeRefNo"))
                    lblOffSignName.Text = CStr(objRdr("fld_OffSignFullName"))   'CStr(objRdr("fld_OffSignName"))
                    lblOffSignDes.Text = CStr(objRdr("fld_OffSignDesg"))
                    lblRemark.Text = Replace(CStr(objRdr("fld_Remark")), vbCrLf, "<br>") 'CStr(objRdr("fld_Remark"))
                End If
            End If
            objRdr.Close()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Private Sub fnPopulateFile()
        Try
            ''Get Records
            dgContractFile.DataSource = clsContract.fnContractFile_GetRec("0", hdnContractID.Value, "N")
            dgContractFile.DataBind()
            If Not dgContractFile.Items.Count > 0 Then ''Not Records found
                dgContractFile.Visible = False
            Else
                dgContractFile.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnGetFileName(ByVal strFileNameSave As String) As String
        Try
            Dim strRetVal As String = ""
            'strRetVal += "<a id=hlFile href='../tempFile/ContractFile/" & strFileNameSave & "' target=_blank >" & _
            '             "<img id=imgView src=../images/audit.gif alt='Click for view file.' border=0 >" & _
            '             "</a>"
            strRetVal += "<a id=hlFile href='" & cnERegistryAddressString() & "download?id=" & strFileNameSave & "' target=ContractViewFile >" & _
                         "<img id=imgView src=../images/audit.gif alt='Click for view file.' border=0 >" & _
                         "</a>"

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Public Function fnPopulateAssetRecords() As Boolean
        Try
            ''Get Records
            dgAsset.DataSource = clsContract.fnContract_GetAssetRecords(hdnContractID.Value, "fldAssetBarcode", "ASC")
            dgAsset.DataBind()
            If Not dgAsset.Items.Count > 0 Then ''Not Records found
                dgAsset.Visible = False
            Else
                dgAsset.Visible = True
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class