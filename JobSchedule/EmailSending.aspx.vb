#Region "Information Section"
' ****************************************************************************************************
' Description       : Job Schedule
' Purpose           : Job Schedule
' Author            : See Siew
' Date              : 27/11/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class EmailSending
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If

            ''*** Send Email for New Asset Creation from NFS
            fnEmail4AssetCreationNFS()

            ''*** Send Email for Warranty Expiry
            fnEmail4WarrantyExp()

            ''*** Send Email for Contract Expiry (1st Reminder)
            fnEmail4ContractExp()

            ''*** Send Email for Contract Expiry (2nd Reminder)
            fnEmail4ContractExp2()

            ''*** Send Email for Float Level Reaching
            fnEmail4FloatReplacement()

        Catch ex As Exception

        End Try
    End Sub

    Public Sub fnEmail4AssetCreationNFS()
        Try
            Dim objRdr As SqlDataReader
            Dim strEmailActiveF As String = "N"
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""
            Dim strEmailSend2F As String = ""
            Dim strEmailSend2InvenCtrl As String = ""
            Dim strEmailSend2Owner As String = ""
            Dim strEmailSend2Others As String = ""
            Dim strAssetBarcode As String = ""

            Dim strEmailTo As String = ""
            Dim strMailBody As String = ""
            Dim arrSendTo() As String

            ''*** Get Email Details from database
            objRdr = clsEmailInfo.fnEmail_GetEmailInfo4NewAssetNFS()
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        strEmailActiveF = CStr(objRdr("EmailActiveF"))
                        strMailSubject = CStr(objRdr("EmailSubject"))
                        strEmailContent = objRdr("EmailContent")
                        strEmailSend2F = CStr(objRdr("EmailSend2F"))
                        strEmailSend2InvenCtrl = CStr(objRdr("EmailSend2InvenCtrl"))
                        strEmailSend2Owner = CStr(objRdr("EmailSend2Owner"))
                        strEmailSend2Others = objRdr("EmailSend2Others")
                        strAssetBarcode = CStr(objRdr("AssetBarcode"))

                        ''*** If Email Active, checking for send email
                        If strEmailActiveF = "Y" And Trim(strAssetBarcode) <> "" Then
                            ''1. Get Email Send To
                            arrSendTo = Split(strEmailSend2F, "|")
                            strEmailTo = ""

                            ''--> Send for Inventory Controller
                            If arrSendTo(0) = "Y" Then
                                strEmailTo += Trim(strEmailSend2InvenCtrl) + ";"
                            End If

                            ''--> Send for Owner
                            If arrSendTo(1) = "Y" Then
                                strEmailTo += Trim(strEmailSend2Owner) + ";"
                            End If

                            ''--> Send for Others
                            If arrSendTo(2) = "Y" Then
                                strEmailTo += Trim(strEmailSend2Others) + ";"
                            End If

                            ''--> remove duplicate email ids in the To List.
                            strEmailTo = fnRemoveNameFromEmailAdd(strEmailTo)
                            strEmailTo = fnRemoveDuplicates(strEmailTo, ";")

                            ''2. Get Email Body
                            strMailBody = "Dear Sir/Madam,<br><br><br>"
                            strMailBody += strEmailContent
                            strMailBody += "<BR><br>New asset(s) created from NFS are " + strAssetBarcode
                            strMailBody += "<BR><br><br>Thanks and Regards,"
                            strMailBody += "<BR>Asset Management System"

                            ''3. Send Email
                            If Trim(strEmailTo) <> "" Then
                                fnSendEmail(strEmailTo, "", "", strMailSubject, strMailBody, "", "N")
                            End If
                        Else
                            Exit Sub
                        End If
                    End While
                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnEmail4WarrantyExp()
        Try
            Dim objRdr As SqlDataReader
            Dim strEmailActiveF As String = "N"
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""
            Dim strEmailSend2F As String = ""
            Dim strEmailSend2InvenCtrl As String = ""
            Dim strEmailSend2Owner As String = ""
            Dim strEmailSend2Others As String = ""
            Dim strAssetBarcode As String = ""

            Dim strEmailTo As String = ""
            Dim strMailBody As String = ""
            Dim arrSendTo() As String

            ''*** Get Email Details from database
            objRdr = clsEmailInfo.fnEmail_GetEmailInfo4WarrantyExp()
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        strEmailActiveF = CStr(objRdr("EmailActiveF"))
                        strMailSubject = CStr(objRdr("EmailSubject"))
                        strEmailContent = objRdr("EmailContent")
                        strEmailSend2F = CStr(objRdr("EmailSend2F"))
                        strEmailSend2InvenCtrl = CStr(objRdr("EmailSend2InvenCtrl"))
                        strEmailSend2Owner = CStr(objRdr("EmailSend2Owner"))
                        strEmailSend2Others = objRdr("EmailSend2Others")
                        strAssetBarcode = CStr(objRdr("AssetBarcode"))

                        ''*** If Email Active, checking for send email
                        If strEmailActiveF = "Y" And Trim(strAssetBarcode) <> "" Then
                            ''1. Get Email Send To
                            arrSendTo = Split(strEmailSend2F, "|")
                            strEmailTo = ""

                            ''--> Send for Inventory Controller
                            If arrSendTo(0) = "Y" Then
                                strEmailTo += Trim(strEmailSend2InvenCtrl) + ";"
                            End If

                            ''--> Send for Owner
                            If arrSendTo(1) = "Y" Then
                                strEmailTo += Trim(strEmailSend2Owner) + ";"
                            End If

                            ''--> Send for Others
                            If arrSendTo(2) = "Y" Then
                                strEmailTo += Trim(strEmailSend2Others) + ";"
                            End If

                            ''--> remove duplicate email ids in the To List.
                            strEmailTo = fnRemoveNameFromEmailAdd(strEmailTo)
                            strEmailTo = fnRemoveDuplicates(strEmailTo, ";")

                            ''2. Get Email Body
                            strMailBody = "Dear Sir/Madam,<br><br><br>"
                            strMailBody += strEmailContent
                            strMailBody += "<BR><br>Warranty for Asset (" + strAssetBarcode + ") have been expiry."
                            strMailBody += "<BR><br><br>Thanks and Regards,"
                            strMailBody += "<BR>Asset Management System"

                            ''3. Send Email
                            If Trim(strEmailTo) <> "" Then
                                fnSendEmail(strEmailTo, "", "", strMailSubject, strMailBody, "", "N")
                            End If
                        Else
                            Exit Sub
                        End If
                    End While
                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnEmail4ContractExp()
        Try
            Dim objRdr As SqlDataReader
            Dim strEmailActiveF As String = "N"
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""
            Dim strEmailSend2F As String = ""
            Dim strEmailSend2InvenCtrl As String = ""
            Dim strEmailSend2Owner As String = ""
            Dim strEmailSend2Others As String = ""
            Dim strEmailSendCCOthers As String = ""
            Dim strContractID As String = ""
            Dim strContractTitle As String = ""
            Dim strContractExpDt As String = ""
            Dim strSecDpsExpDt As String = ""
            Dim strBankGuaExpDt As String = ""
            Dim strOwnerName As String = ""
            Dim strVendorName As String = ""
            Dim strContractDesc As String = ""
            Dim intContractID As String = ""
            Dim strContractExpF As String = ""

            Dim strEmailToOwner As String = ""
            Dim strEmailContentAction As String = ""

            Dim strEmailTo As String = ""
            Dim strEmailCC As String = ""
            Dim strMailBody As String = ""
            Dim arrSendTo() As String

            ''*** Get Email Details from database
            objRdr = clsEmailInfo.fnEmail_GetEmailInfo4ContractExp()
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        strEmailActiveF = CStr(objRdr("EmailActiveF"))
                        strMailSubject = CStr(objRdr("EmailSubject"))
                        strEmailContent = objRdr("EmailContent")
                        strEmailSend2F = CStr(objRdr("EmailSend2F"))
                        strEmailSend2InvenCtrl = CStr(objRdr("EmailSend2InvenCtrl"))
                        strEmailSend2Owner = CStr(objRdr("EmailSend2Owner"))
                        strEmailSend2Others = objRdr("EmailSend2Others")
                        strEmailSendCCOthers = objRdr("EmailSendCCOthers")
                        strContractID = mfnGetContractIDFormat(CStr(objRdr("ContractID")))
                        strContractTitle = CStr(objRdr("ContractTitle"))
                        strContractExpDt = CStr(objRdr("ContractExpDt"))
                        strSecDpsExpDt = IIf(Trim(CStr(objRdr("SecDpsExpDt"))) = "", "N.A.", CStr(objRdr("SecDpsExpDt")))
                        strBankGuaExpDt = IIf(Trim(CStr(objRdr("BankGuaExpDt"))) = "", "N.A.", CStr(objRdr("BankGuaExpDt")))
                        strOwnerName = CStr(objRdr("ContractOwner"))
                        strVendorName = CStr(objRdr("ContractVendor"))
                        strContractDesc = Trim(CStr(objRdr("ContractDesc")))
                        'strContractDesc = Replace(IIf(strContractDesc.Length > 200, strContractDesc.Substring(0, 200) & "...", strContractDesc), vbCrLf, "<br>")
                        If strContractDesc.Length > 200 Then
                            strContractDesc = strContractDesc.Substring(0, 200) & "..."
                        End If
                        strContractDesc = Replace(strContractDesc, vbCrLf, "<br>")
                        intContractID = CStr(objRdr("fld_ContractID"))
                        strContractExpF = CStr(objRdr("ContractExpF"))

                        ''*** If Email Active, checking for send email
                        If strEmailActiveF = "Y" And Trim(strContractID) <> "" Then
                            ''1. Get Email Send To
                            arrSendTo = Split(strEmailSend2F, "|")
                            strEmailTo = ""
                            strEmailCC = ""
                            strEmailToOwner = ""
                            strEmailContentAction = ""

                            ''--> Send for Owner
                            If arrSendTo(0) = "Y" Then
                                strEmailTo = Trim(strEmailSend2Owner) + ";"

                                ''Sending Email cause of Contract Expiry
                                If strContractExpF = "Y" Then
                                    strEmailToOwner = Trim(strEmailSend2Owner) + ";"

                                    'strEmailContentAction = "<br><br>"
                                    'strEmailContentAction += "Please click on following link, if you have take the action for the contract:- <br>"
                                    'strEmailContentAction += "1. <a id=hlCtrAct href='" & gContractEmailExpActPage() & intContractID & "&CA=N' target=ContractActionNR >Contract not be renewed.</a><br>"
                                    'strEmailContentAction += "2. <a id=hlCtrAct href='" & gContractEmailExpActPage() & intContractID & "&CA=P' target=ContractActionPR >Contract is already in process of renewal.</a>"

                                    strEmailContentAction = "<br><br>"
                                    strEmailContentAction += "Please click on one of the options, if you have taken the action for the contract:- "
                                    strEmailContentAction += "<ul>"
                                    strEmailContentAction += "<li><a id=hlCtrAct href='" & gContractEmailExpActPage() & intContractID & "&CA=N' target=ContractActionNR >Contract not be renewed.</a></li>"
                                    strEmailContentAction += "<li><a id=hlCtrAct href='" & gContractEmailExpActPage() & intContractID & "&CA=P' target=ContractActionPR >Contract is already in process of renewal.</a></li>"
                                    strEmailContentAction += "</ul>"
                                    strEmailContentAction += "Please note that if the above options are not selected the 2nd Reminder will be activated."
                                End If

                            End If

                            ''--> Send for Others (To)
                            If arrSendTo(1) = "Y" Then
                                strEmailTo += Trim(strEmailSend2Others) + ";"
                            End If

                            ''--> Send for Inventory Controller
                            If arrSendTo(2) = "Y" Then
                                strEmailCC = Trim(strEmailSend2InvenCtrl) + ";"
                            End If

                            ''--> Send for Others (CC)
                            If arrSendTo(3) = "Y" Then
                                strEmailCC += Trim(strEmailSendCCOthers) + ";"
                            End If

                            ''--> remove duplicate email ids in the To List.
                            strEmailTo = fnRemoveNameFromEmailAdd(strEmailTo)
                            strEmailTo = fnRemoveDuplicates2(strEmailToOwner, strEmailTo, ";") 'fnRemoveDuplicates(strEmailTo, ";")
                            strEmailCC = fnRemoveNameFromEmailAdd(strEmailCC)
                            strEmailCC = fnRemoveDuplicates2(strEmailToOwner & strEmailTo, strEmailCC, ";")

                            ''2. Send email to Contract Owner for Contract expiry action
                            If strContractExpF = "Y" And strEmailToOwner <> "" Then
                                ''2a. Get Email Body
                                strMailBody = "Dear Sir/Madam,<br><br><br>"
                                strMailBody += strEmailContent
                                strMailBody += "<BR><br>This is an email notification that the following contract will be expiring soon. "
                                strMailBody += "Pls refer to details for followup action:-<br><br>"
                                strMailBody += "Contract ID : " + strContractID
                                strMailBody += "<br>Contract Title : " + strContractTitle
                                strMailBody += "<br>Contract Expiry Date : " + strContractExpDt
                                strMailBody += "<br>Security Deposit Expiry Date : " + strSecDpsExpDt
                                strMailBody += "<br>Banker's Guarantee Expiry Date : " + strBankGuaExpDt
                                strMailBody += "<br>Contract Owner : " + strOwnerName
                                strMailBody += "<br>Vendor Name : " + strVendorName
                                strMailBody += "<br>Description : " + strContractDesc
                                strMailBody += strEmailContentAction
                                strMailBody += "<BR><br><br>Thanks and Regards,"
                                strMailBody += "<BR>Asset Management System"
                                strMailBody += "<BR><br><br>* This is a computer generated e-mail, please do not reply to it."

                                ''2a. Send Email
                                If Trim(strEmailToOwner) <> "" Then
                                    fnSendEmail(strEmailToOwner, "", "", strMailSubject, strMailBody, "", "H")
                                End If
                            End If

                            ''3. Send Contract Email as normal 
                            If Trim(strEmailTo) <> "" Or Trim(strEmailCC) <> "" Then
                                ''3a. Get Email Body
                                strMailBody = "Dear Sir/Madam,<br><br><br>"
                                strMailBody += strEmailContent
                                strMailBody += "<BR><br>This is an email notification that the following contract will be expiring soon. "
                                strMailBody += "Pls refer to details for followup action:-<br><br>"
                                strMailBody += "Contract ID : " + strContractID
                                strMailBody += "<br>Contract Title : " + strContractTitle
                                strMailBody += "<br>Contract Expiry Date : " + strContractExpDt
                                strMailBody += "<br>Security Deposit Expiry Date : " + strSecDpsExpDt
                                strMailBody += "<br>Banker's Guarantee Expiry Date : " + strBankGuaExpDt
                                strMailBody += "<br>Contract Owner : " + strOwnerName
                                strMailBody += "<br>Vendor Name : " + strVendorName
                                strMailBody += "<br>Description : " + strContractDesc
                                strMailBody += "<BR><br><br>Thanks and Regards,"
                                strMailBody += "<BR>Asset Management System"
                                strMailBody += "<BR><br><br>* This is a computer generated e-mail, please do not reply to it."

                                ''3a. Send Email
                                fnSendEmail(strEmailTo, strEmailCC, "", strMailSubject, strMailBody, "", "H")
                            End If
                        Else
                            Exit Sub
                        End If
                    End While
                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnEmail4ContractExp2()
        Try
            Dim objRdr As SqlDataReader
            Dim strEmailActiveF As String = "N"
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""
            Dim strEmailSend2F As String = ""
            Dim strEmailSend2InvenCtrl As String = ""
            Dim strEmailSend2Owner As String = ""
            Dim strEmailSend2Others As String = ""
            Dim strEmailSendCCOthers As String = ""
            Dim strContractID As String = ""
            Dim strContractTitle As String = ""
            Dim strContractExpDt As String = ""
            Dim strSecDpsExpDt As String = ""
            Dim strBankGuaExpDt As String = ""
            Dim strEmailHOD As String = ""
            Dim strOwnerName As String = ""
            Dim strVendorName As String = ""
            Dim strContractDesc As String = ""

            Dim strEmailTo As String = ""
            Dim strEmailCC As String = ""
            Dim strMailBody As String = ""
            Dim arrSendTo() As String

            ''*** Get Email Details from database
            objRdr = clsEmailInfo.fnEmail_GetEmailInfo4ContractExp2()
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        strEmailActiveF = CStr(objRdr("EmailActiveF"))
                        strMailSubject = CStr(objRdr("EmailSubject"))
                        strEmailContent = objRdr("EmailContent")
                        strEmailSend2F = CStr(objRdr("EmailSend2F"))
                        strEmailSend2InvenCtrl = CStr(objRdr("EmailSend2InvenCtrl"))
                        strEmailSend2Owner = CStr(objRdr("EmailSend2Owner"))
                        strEmailSend2Others = objRdr("EmailSend2Others")
                        strEmailSendCCOthers = objRdr("EmailSendCCOthers")
                        strContractID = mfnGetContractIDFormat(CStr(objRdr("ContractID")))
                        strContractTitle = CStr(objRdr("ContractTitle"))
                        strContractExpDt = CStr(objRdr("ContractExpDt"))
                        strSecDpsExpDt = IIf(Trim(CStr(objRdr("SecDpsExpDt"))) = "", "N.A.", CStr(objRdr("SecDpsExpDt")))
                        strBankGuaExpDt = IIf(Trim(CStr(objRdr("BankGuaExpDt"))) = "", "N.A.", CStr(objRdr("BankGuaExpDt")))
                        strEmailHOD = CStr(objRdr("HODEmail"))
                        strOwnerName = CStr(objRdr("ContractOwner"))
                        strVendorName = CStr(objRdr("ContractVendor"))
                        strContractDesc = Trim(CStr(objRdr("ContractDesc")))
                        'strContractDesc = Replace(IIf(Len(strContractDesc) > 200, strContractDesc.Substring(0, 200) & "...", strContractDesc), vbCrLf, "<br>")
                        If strContractDesc.Length > 200 Then
                            strContractDesc = strContractDesc.Substring(0, 200) & "..."
                        End If
                        strContractDesc = Replace(strContractDesc, vbCrLf, "<br>")

                        ''*** If Email Active, checking for send email
                        If strEmailActiveF = "Y" And Trim(strContractID) <> "" Then
                            ''1. Get Email Send To
                            arrSendTo = Split(strEmailSend2F, "|")
                            strEmailTo = ""
                            strEmailCC = ""
                            strEmailCC = strEmailHOD + ";"

                            ''--> Send for Owner
                            If arrSendTo(0) = "Y" Then
                                strEmailTo = Trim(strEmailSend2Owner) + ";"
                            End If

                            ''--> Send for Others (To)
                            If arrSendTo(1) = "Y" Then
                                strEmailTo += Trim(strEmailSend2Others) + ";"
                            End If

                            ''--> Send for Inventory Controller
                            If arrSendTo(2) = "Y" Then
                                strEmailCC += Trim(strEmailSend2InvenCtrl) + ";"
                            End If

                            ''--> Send for Others (CC)
                            If arrSendTo(3) = "Y" Then
                                strEmailCC += Trim(strEmailSendCCOthers) + ";"
                            End If

                            ''--> remove duplicate email ids in the To List.
                            strEmailTo = fnRemoveNameFromEmailAdd(strEmailTo)
                            strEmailTo = fnRemoveDuplicates(strEmailTo, ";")
                            strEmailCC = fnRemoveNameFromEmailAdd(strEmailCC)
                            strEmailCC = fnRemoveDuplicates2(strEmailTo, strEmailCC, ";")

                            ''2. Get Email Body
                            strMailBody = "Dear Sir/Madam,<br><br><br>"
                            strMailBody += strEmailContent
                            'strMailBody += "<BR><br>This is an email notification that the following contract will be expiring soon. "
                            'strMailBody += "Pls refer to details for followup action:-<br><br>"
                            strMailBody += "<BR><br>CC:HOD, please ensure that the officer renew the contract before expiry, if applicable."
                            strMailBody += "Please refer to the following details:-<br><br>"
                            strMailBody += "Contract ID : " + strContractID
                            strMailBody += "<br>Contract Title : " + strContractTitle
                            strMailBody += "<br>Contract Expiry Date : " + strContractExpDt
                            strMailBody += "<br>Security Deposit Expiry Date : " + strSecDpsExpDt
                            strMailBody += "<br>Banker's Guarantee Expiry Date : " + strBankGuaExpDt
                            strMailBody += "<br>Contract Owner : " + strOwnerName
                            strMailBody += "<br>Vendor Name : " + strVendorName
                            strMailBody += "<br>Description : " + strContractDesc
                            strMailBody += "<BR><br><br>Thanks and Regards,"
                            strMailBody += "<BR>Asset Management System"
                            strMailBody += "<BR><br><br>* This is a computer generated e-mail, please do not reply to it."

                            ''3. Send Email
                            If Trim(strEmailTo) <> "" Or Trim(strEmailCC) <> "" Then
                                fnSendEmail(strEmailTo, strEmailCC, "", strMailSubject, strMailBody, "", "H")
                            End If
                        Else
                            Exit Sub
                        End If
                    End While
                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnEmail4FloatReplacement()
        Try
            Dim objRdr As SqlDataReader
            Dim strEmailActiveF As String = "N"
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""
            Dim strEmailSend2F As String = ""
            Dim strEmailSend2InvenCtrl As String = ""
            Dim strEmailSend2Others As String = ""
            Dim strFloatExist As String = ""
            Dim strFloatDetail As String = ""

            Dim strEmailTo As String = ""
            Dim strMailBody As String = ""
            Dim arrSendTo() As String

            ''*** Get Email Details from database
            objRdr = clsEmailInfo.fnEmail_GetEmailInfo4FloatReplacement()
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        strEmailActiveF = CStr(objRdr("EmailActiveF"))
                        strMailSubject = CStr(objRdr("EmailSubject"))
                        strEmailContent = objRdr("EmailContent")
                        strEmailSend2F = CStr(objRdr("EmailSend2F"))
                        strEmailSend2InvenCtrl = CStr(objRdr("EmailSend2InvenCtrl"))
                        strEmailSend2Others = objRdr("EmailSend2Others")
                        strFloatExist = CStr(objRdr("FloatExist"))
                        strFloatDetail = CStr(objRdr("FloatDetails"))

                        ''*** If Email Active, checking for send email
                        If strEmailActiveF = "Y" And Trim(strFloatExist) = "Y" Then
                            ''1. Get Email Send To
                            arrSendTo = Split(strEmailSend2F, "|")
                            strEmailTo = ""

                            ''--> Send for Inventory Controller
                            If arrSendTo(0) = "Y" Then
                                strEmailTo += Trim(strEmailSend2InvenCtrl) + ";"
                            End If

                            ''--> Send for Others
                            If arrSendTo(1) = "Y" Then
                                strEmailTo += Trim(strEmailSend2Others) + ";"
                            End If

                            ''--> remove duplicate email ids in the To List.
                            strEmailTo = fnRemoveNameFromEmailAdd(strEmailTo)
                            strEmailTo = fnRemoveDuplicates(strEmailTo, ";")

                            ''2. Get Email Body
                            strMailBody = "Dear Sir/Madam,<br><br><br>"
                            strMailBody += strEmailContent
                            strMailBody += "<BR><br>" + strFloatDetail
                            strMailBody += "<BR><br><br>Thanks and Regards,"
                            strMailBody += "<BR>Asset Management System"

                            ''3. Send Email
                            If Trim(strEmailTo) <> "" Then
                                fnSendEmail(strEmailTo, "", "", strMailSubject, strMailBody, "", "N")
                            End If
                        Else
                            Exit Sub
                        End If
                    End While
                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


End Class