#Region "Information Section"
' ****************************************************************************************************
' Description       : Report Class.
' Purpose           : To perform the business logic for Report (such as view, add, delete, edit)
' Author			: See Siew
' Date			    : 08.11.2005
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsReport

    Public Shared Function fnContractRpt_GenListOfContract( _
        ByVal strContractID As String, ByVal strContractTitle As String, ByVal strContractCost As String, _
        ByVal strContractStartFDt As String, ByVal strContractStartTDt As String, _
        ByVal strContractEndFDt As String, ByVal strContractEndTDt As String, ByVal intContNoti As String, _
        ByVal strVendorName As String, ByVal strDesc As String, _
        ByVal strContOwner As String, ByVal strContOwnerDept As String, _
        ByVal strSecDps As String, ByVal strSecDpsSFrmDt As String, ByVal strSecDpsSToDt As String, _
        ByVal strSecDpsEFrmDt As String, ByVal strSecDpsEToDt As String, ByVal intSecDpsNoti As String, _
        ByVal strSecDpsRefNo As String, ByVal strBankGuaAmt As String, ByVal strBankGuaSFrmDt As String, _
        ByVal strBankGuaSToDt As String, ByVal strBankGuaEFrmDt As String, ByVal strBankGuaEToDt As String, _
        ByVal intBankGuaNoti As String, ByVal strBankGuaRefNo As String, ByVal strOffSignName As String, _
        ByVal strOffSignDesg As String, ByVal strAssetBarcode As String, _
        ByVal strArchiveF As String, ByVal strOrderBy1 As String, _
        ByVal strOrderBy2 As String, ByVal strOrderBy3 As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get List of Contract Report
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  14/01/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(32) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strContractID", strContractID)                     '--""=All 
            oParamSQL(1) = New SqlParameter("@strContractTitle", strContractTitle)               '--""=All
            oParamSQL(2) = New SqlParameter("@strContractCost", strContractCost)                 '--""=All
            oParamSQL(3) = New SqlParameter("@strContractStartFDt", strContractStartFDt)         '--""=All
            oParamSQL(4) = New SqlParameter("@strContractStartTDt", strContractStartTDt)         '--""=All
            oParamSQL(5) = New SqlParameter("@strContractEndFDt", strContractEndFDt) '--""=All
            oParamSQL(6) = New SqlParameter("@strContractEndTDt", strContractEndTDt) '--""=All
            oParamSQL(7) = New SqlParameter("@intContNoti", intContNoti) '--"-1"=All
            oParamSQL(8) = New SqlParameter("@strVendorName", strVendorName)        '--""=All
            oParamSQL(9) = New SqlParameter("@strDesc", strDesc)        '--""=All
            oParamSQL(10) = New SqlParameter("@strContOwner", strContOwner)        '--""=All
            oParamSQL(11) = New SqlParameter("@strContOwnerDept", strContOwnerDept)        '--""=All

            oParamSQL(12) = New SqlParameter("@strSecDps", strSecDps)        '--""=All
            oParamSQL(13) = New SqlParameter("@strSecDpsSFrmDt", strSecDpsSFrmDt)        '--""=All
            oParamSQL(14) = New SqlParameter("@strSecDpsSToDt", strSecDpsSToDt)        '--""=All
            oParamSQL(15) = New SqlParameter("@strSecDpsEFrmDt", strSecDpsEFrmDt)        '--""=All
            oParamSQL(16) = New SqlParameter("@strSecDpsEToDt", strSecDpsEToDt)        '--""=All
            oParamSQL(17) = New SqlParameter("@intSecDpsNoti", intSecDpsNoti)        '--""=All
            oParamSQL(18) = New SqlParameter("@strSecDpsRefNo", strSecDpsRefNo)        '--""=All

            oParamSQL(19) = New SqlParameter("@strBankGuaAmt", strBankGuaAmt)        '--""=All
            oParamSQL(20) = New SqlParameter("@strBankGuaSFrmDt", strBankGuaSFrmDt)        '--""=All
            oParamSQL(21) = New SqlParameter("@strBankGuaSToDt", strBankGuaSToDt)        '--""=All
            oParamSQL(22) = New SqlParameter("@strBankGuaEFrmDt", strBankGuaEFrmDt)        '--""=All
            oParamSQL(23) = New SqlParameter("@strBankGuaEToDt", strBankGuaEToDt)        '--""=All
            oParamSQL(24) = New SqlParameter("@intBankGuaNoti", intBankGuaNoti)        '--""=All
            oParamSQL(25) = New SqlParameter("@strBankGuaRefNo", strBankGuaRefNo)        '--""=All

            oParamSQL(26) = New SqlParameter("@strOffSignName", strOffSignName)        '--""=All
            oParamSQL(27) = New SqlParameter("@strOffSignDesg", strOffSignDesg)        '--""=All
            oParamSQL(28) = New SqlParameter("@strAssetBarcode", strAssetBarcode)    '--""=All
            oParamSQL(29) = New SqlParameter("@strArchiveF", strArchiveF)        '--""=All
            oParamSQL(30) = New SqlParameter("@strOrderBy1", strOrderBy1)        '--""=All
            oParamSQL(31) = New SqlParameter("@strOrderBy2", strOrderBy2)            '--""=All
            oParamSQL(32) = New SqlParameter("@strOrderBy3", strOrderBy3)          '--""=All

            fnContractRpt_GenListOfContract = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_ContractRpt_GenListOfContract", oParamSQL)
        Catch ex As Exception
            fnContractRpt_GenListOfContract = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnContractRpt_GenTrackRenewalCtrtVendorRpt( _
        ByVal strContractID As String, ByVal strContractTitle As String, ByVal strContractCost As String, _
        ByVal strContractStartFDt As String, ByVal strContractStartTDt As String, _
        ByVal strContractEndFDt As String, ByVal strContractEndTDt As String, ByVal intContNoti As String, _
        ByVal strVendorName As String, ByVal strDesc As String, _
        ByVal strContOwner As String, ByVal strContOwnerDept As String, _
        ByVal strSecDps As String, ByVal strSecDpsSFrmDt As String, ByVal strSecDpsSToDt As String, _
        ByVal strSecDpsEFrmDt As String, ByVal strSecDpsEToDt As String, ByVal intSecDpsNoti As String, _
        ByVal strSecDpsRefNo As String, ByVal strBankGuaAmt As String, ByVal strBankGuaSFrmDt As String, _
        ByVal strBankGuaSToDt As String, ByVal strBankGuaEFrmDt As String, ByVal strBankGuaEToDt As String, _
        ByVal intBankGuaNoti As String, ByVal strBankGuaRefNo As String, ByVal strOffSignName As String, _
        ByVal strOffSignDesg As String, ByVal strAssetBarcode As String, _
        ByVal strArchiveF As String, ByVal strOrderBy1 As String, _
        ByVal strOrderBy2 As String, ByVal strOrderBy3 As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Tracking Renewal Contract Report
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  02/01/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(32) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strContractID", strContractID)                     '--""=All 
            oParamSQL(1) = New SqlParameter("@strContractTitle", strContractTitle)               '--""=All
            oParamSQL(2) = New SqlParameter("@strContractCost", strContractCost)                 '--""=All
            oParamSQL(3) = New SqlParameter("@strContractStartFDt", strContractStartFDt)         '--""=All
            oParamSQL(4) = New SqlParameter("@strContractStartTDt", strContractStartTDt)         '--""=All
            oParamSQL(5) = New SqlParameter("@strContractEndFDt", strContractEndFDt) '--""=All
            oParamSQL(6) = New SqlParameter("@strContractEndTDt", strContractEndTDt) '--""=All
            oParamSQL(7) = New SqlParameter("@intContNoti", intContNoti) '--"-1"=All
            oParamSQL(8) = New SqlParameter("@strVendorName", strVendorName)        '--""=All
            oParamSQL(9) = New SqlParameter("@strDesc", strDesc)        '--""=All
            oParamSQL(10) = New SqlParameter("@strContOwner", strContOwner)        '--""=All
            oParamSQL(11) = New SqlParameter("@strContOwnerDept", strContOwnerDept)        '--""=All

            oParamSQL(12) = New SqlParameter("@strSecDps", strSecDps)        '--""=All
            oParamSQL(13) = New SqlParameter("@strSecDpsSFrmDt", strSecDpsSFrmDt)        '--""=All
            oParamSQL(14) = New SqlParameter("@strSecDpsSToDt", strSecDpsSToDt)        '--""=All
            oParamSQL(15) = New SqlParameter("@strSecDpsEFrmDt", strSecDpsEFrmDt)        '--""=All
            oParamSQL(16) = New SqlParameter("@strSecDpsEToDt", strSecDpsEToDt)        '--""=All
            oParamSQL(17) = New SqlParameter("@intSecDpsNoti", intSecDpsNoti)        '--""=All
            oParamSQL(18) = New SqlParameter("@strSecDpsRefNo", strSecDpsRefNo)        '--""=All

            oParamSQL(19) = New SqlParameter("@strBankGuaAmt", strBankGuaAmt)        '--""=All
            oParamSQL(20) = New SqlParameter("@strBankGuaSFrmDt", strBankGuaSFrmDt)        '--""=All
            oParamSQL(21) = New SqlParameter("@strBankGuaSToDt", strBankGuaSToDt)        '--""=All
            oParamSQL(22) = New SqlParameter("@strBankGuaEFrmDt", strBankGuaEFrmDt)        '--""=All
            oParamSQL(23) = New SqlParameter("@strBankGuaEToDt", strBankGuaEToDt)        '--""=All
            oParamSQL(24) = New SqlParameter("@intBankGuaNoti", intBankGuaNoti)        '--""=All
            oParamSQL(25) = New SqlParameter("@strBankGuaRefNo", strBankGuaRefNo)        '--""=All

            oParamSQL(26) = New SqlParameter("@strOffSignName", strOffSignName)        '--""=All
            oParamSQL(27) = New SqlParameter("@strOffSignDesg", strOffSignDesg)        '--""=All
            oParamSQL(28) = New SqlParameter("@strAssetBarcode", strAssetBarcode)    '--""=All
            oParamSQL(29) = New SqlParameter("@strArchiveF", strArchiveF)        '--""=All
            oParamSQL(30) = New SqlParameter("@strOrderBy1", strOrderBy1)        '--""=All
            oParamSQL(31) = New SqlParameter("@strOrderBy2", strOrderBy2)            '--""=All
            oParamSQL(32) = New SqlParameter("@strOrderBy3", strOrderBy3)          '--""=All

            fnContractRpt_GenTrackRenewalCtrtVendorRpt = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_ContractRpt_GenTrackRenewalCtrtVendorRpt", oParamSQL)
        Catch ex As Exception
            fnContractRpt_GenTrackRenewalCtrtVendorRpt = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnContractRpt_GenContractExpRpt( _
        ByVal strContractID As String, ByVal strContractTitle As String, ByVal strContractCost As String, _
        ByVal strContractStartFDt As String, ByVal strContractStartTDt As String, _
        ByVal strContractEndFDt As String, ByVal strContractEndTDt As String, ByVal intContNoti As String, _
        ByVal strVendorName As String, ByVal strDesc As String, _
        ByVal strContOwner As String, ByVal strContOwnerDept As String, _
        ByVal strSecDps As String, ByVal strSecDpsSFrmDt As String, ByVal strSecDpsSToDt As String, _
        ByVal strSecDpsEFrmDt As String, ByVal strSecDpsEToDt As String, ByVal intSecDpsNoti As String, _
        ByVal strSecDpsRefNo As String, ByVal strBankGuaAmt As String, ByVal strBankGuaSFrmDt As String, _
        ByVal strBankGuaSToDt As String, ByVal strBankGuaEFrmDt As String, ByVal strBankGuaEToDt As String, _
        ByVal intBankGuaNoti As String, ByVal strBankGuaRefNo As String, ByVal strOffSignName As String, _
        ByVal strOffSignDesg As String, ByVal strAssetBarcode As String, _
        ByVal strArchiveF As String, ByVal strOrderBy1 As String, _
        ByVal strOrderBy2 As String, ByVal strOrderBy3 As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Contract Expiry Report
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  02/01/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(32) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strContractID", strContractID)                     '--""=All 
            oParamSQL(1) = New SqlParameter("@strContractTitle", strContractTitle)               '--""=All
            oParamSQL(2) = New SqlParameter("@strContractCost", strContractCost)                 '--""=All
            oParamSQL(3) = New SqlParameter("@strContractStartFDt", strContractStartFDt)         '--""=All
            oParamSQL(4) = New SqlParameter("@strContractStartTDt", strContractStartTDt)         '--""=All
            oParamSQL(5) = New SqlParameter("@strContractEndFDt", strContractEndFDt) '--""=All
            oParamSQL(6) = New SqlParameter("@strContractEndTDt", strContractEndTDt) '--""=All
            oParamSQL(7) = New SqlParameter("@intContNoti", intContNoti) '--"-1"=All
            oParamSQL(8) = New SqlParameter("@strVendorName", strVendorName)        '--""=All
            oParamSQL(9) = New SqlParameter("@strDesc", strDesc)        '--""=All
            oParamSQL(10) = New SqlParameter("@strContOwner", strContOwner)        '--""=All
            oParamSQL(11) = New SqlParameter("@strContOwnerDept", strContOwnerDept)        '--""=All

            oParamSQL(12) = New SqlParameter("@strSecDps", strSecDps)        '--""=All
            oParamSQL(13) = New SqlParameter("@strSecDpsSFrmDt", strSecDpsSFrmDt)        '--""=All
            oParamSQL(14) = New SqlParameter("@strSecDpsSToDt", strSecDpsSToDt)        '--""=All
            oParamSQL(15) = New SqlParameter("@strSecDpsEFrmDt", strSecDpsEFrmDt)        '--""=All
            oParamSQL(16) = New SqlParameter("@strSecDpsEToDt", strSecDpsEToDt)        '--""=All
            oParamSQL(17) = New SqlParameter("@intSecDpsNoti", intSecDpsNoti)        '--""=All
            oParamSQL(18) = New SqlParameter("@strSecDpsRefNo", strSecDpsRefNo)        '--""=All

            oParamSQL(19) = New SqlParameter("@strBankGuaAmt", strBankGuaAmt)        '--""=All
            oParamSQL(20) = New SqlParameter("@strBankGuaSFrmDt", strBankGuaSFrmDt)        '--""=All
            oParamSQL(21) = New SqlParameter("@strBankGuaSToDt", strBankGuaSToDt)        '--""=All
            oParamSQL(22) = New SqlParameter("@strBankGuaEFrmDt", strBankGuaEFrmDt)        '--""=All
            oParamSQL(23) = New SqlParameter("@strBankGuaEToDt", strBankGuaEToDt)        '--""=All
            oParamSQL(24) = New SqlParameter("@intBankGuaNoti", intBankGuaNoti)        '--""=All
            oParamSQL(25) = New SqlParameter("@strBankGuaRefNo", strBankGuaRefNo)        '--""=All

            oParamSQL(26) = New SqlParameter("@strOffSignName", strOffSignName)        '--""=All
            oParamSQL(27) = New SqlParameter("@strOffSignDesg", strOffSignDesg)        '--""=All
            oParamSQL(28) = New SqlParameter("@strAssetBarcode", strAssetBarcode)    '--""=All
            oParamSQL(29) = New SqlParameter("@strArchiveF", strArchiveF)        '--""=All
            oParamSQL(30) = New SqlParameter("@strOrderBy1", strOrderBy1)        '--""=All
            oParamSQL(31) = New SqlParameter("@strOrderBy2", strOrderBy2)            '--""=All
            oParamSQL(32) = New SqlParameter("@strOrderBy3", strOrderBy3)          '--""=All

            fnContractRpt_GenContractExpRpt = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_ContractRpt_GenContractExpRpt", oParamSQL)
        Catch ex As Exception
            fnContractRpt_GenContractExpRpt = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnContractRpt_GetField2Display( _
                                   ByVal strCallType As String, _
                                   ByVal intRptType As String, _
                                   ByVal strRptDisplayFields As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Display Fiellds
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  28/12/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strCallType", strCallType)   '--DF=Display Field, AF = Not Display Field, GR=Generate Report
            oParamSQL(1) = New SqlParameter("@intRptType", intRptType)     '--1 = Contract Expiry Report, 2 = Tracking Renewal Contract (Vendor) 
            oParamSQL(2) = New SqlParameter("@strRptDisplayFields", strRptDisplayFields) '--Report Field to display (Format = 12^34^)
            fnContractRpt_GetField2Display = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_ContractRpt_GetField2Display", oParamSQL)
        Catch ex As Exception
            fnContractRpt_GetField2Display = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnABVReport_SearchRecord( _
                ByVal strAssetIDAMS As String, ByVal strAssetIDNFS As String, _
                ByVal strAssetType As String, ByVal intAssetCatID As Integer, ByVal intAssetCatSubID As Integer, _
                ByVal strAssetStatus As String, ByVal strPurchaseFDt As String, ByVal strPurchaseTDt As String, _
                ByVal strWarExpFDt As String, ByVal strWarExpTDt As String, _
                ByVal intDeptID As Integer, ByVal intLocID As Integer, ByVal strOwnerID As String, _
                ByVal strDeprecFDt As String, ByVal strDeprecTDt As String, _
                ByVal strOrderBy As String, ByVal intLocSubID As String, ByVal strCtrlItemF As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Asset Book Value report Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  10/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(17) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDAMS", strAssetIDAMS)       '--""=All
            oParamSQL(1) = New SqlParameter("@strAssetIDNFS", strAssetIDNFS)       '--""=All
            oParamSQL(2) = New SqlParameter("@strAssetType", strAssetType)         '--""=All, A=Fixed Asset, I=Inventory
            oParamSQL(3) = New SqlParameter("@intAssetCatID", intAssetCatID)       '--"0"=All
            oParamSQL(4) = New SqlParameter("@intAssetCatSubID", intAssetCatSubID) '--"0"=All
            oParamSQL(5) = New SqlParameter("@strAssetStatus", strAssetStatus)   '--"0"=All, (A)ctive, (I)nactive, (R)edundancy, (C)ondenmation, (D)isposed
            oParamSQL(6) = New SqlParameter("@strPurchaseFDt", strPurchaseFDt)   '--""=All
            oParamSQL(7) = New SqlParameter("@strPurchaseTDt", strPurchaseTDt)   '--""=All
            oParamSQL(8) = New SqlParameter("@strWarExpFDt", strWarExpFDt)       '--""=All
            oParamSQL(9) = New SqlParameter("@strWarExpTDt", strWarExpTDt)       '--""=All
            oParamSQL(10) = New SqlParameter("@intDeptID", intDeptID)            '--"0"=All
            oParamSQL(11) = New SqlParameter("@intLocID", intLocID)              '--"0"=All
            oParamSQL(12) = New SqlParameter("@strOwnerID", strOwnerID)          '--""=All
            oParamSQL(13) = New SqlParameter("@strDeprecFDt", strDeprecFDt)      '--""=All
            oParamSQL(14) = New SqlParameter("@strDeprecTDt", strDeprecTDt)      '--""=All
            oParamSQL(15) = New SqlParameter("@strOrderBy", strOrderBy)
            oParamSQL(16) = New SqlParameter("@intLocSubID", intLocSubID)              '--"0"=All
            oParamSQL(17) = New SqlParameter("@strCtrlItemF", strCtrlItemF)              '--""=All

            fnABVReport_SearchRecord = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_ABVReport_SearchRecord", oParamSQL)
        Catch ex As Exception
            fnABVReport_SearchRecord = Nothing
            Throw ex
        End Try
    End Function


    Public Shared Function fnDynamicRpt_GenerateRecord( _
                ByVal strAssetIDAMS As String, ByVal strAssetIDNFS As String, _
                ByVal strAssetType As String, ByVal intAssetCatID As String, ByVal intAssetCatSubID As String, _
                ByVal strAssetStatus As String, ByVal strBrand As String, ByVal strDescription As String, _
                ByVal strCost As String, ByVal strCostCenter As String, _
                ByVal strPurchaseFDt As String, ByVal strPurchaseTDt As String, _
                ByVal strWarExpFDt As String, ByVal strWarExpTDt As String, _
                ByVal intDeptID As String, ByVal intLocID As String, ByVal strOwnerID As String, _
                ByVal strOptionAMS As String, ByVal strOptionNFS As String, _
                ByVal strOptionBrand As String, ByVal strOptionDesc As String, ByVal strOptionCost As String, _
                ByVal strOptionCostCtr As String, _
                ByVal strOrderBy1 As String, ByVal strOrderBy2 As String, ByVal strOrderBy3 As String, _
                ByVal intLocSubID As String, ByVal strCtrlItemF As String, _
                ByVal ShortDescription As String, ByVal OtherInformation As String, _
                ByVal intDeviceTypeID As String, ByVal strSerialNo As String, _
                ByVal strMacAddress As String, _
                ByVal strNewHostname As String, ByVal strStatus As String, ByVal strMachineModel As String _
) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Asset Record for Dynamic Report
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  10/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(35) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDAMS", strAssetIDAMS)       '--""=All
            oParamSQL(1) = New SqlParameter("@strAssetIDNFS", strAssetIDNFS)       '--""=All
            oParamSQL(2) = New SqlParameter("@strAssetType", strAssetType)         '--""=All, A=Fixed Asset, I=Inventory
            oParamSQL(3) = New SqlParameter("@intAssetCatID", intAssetCatID)       '--"0"=All
            oParamSQL(4) = New SqlParameter("@intAssetCatSubID", intAssetCatSubID) '--"0"=All
            oParamSQL(5) = New SqlParameter("@strAssetStatus", strAssetStatus)     '--"0"=All, (A)ctive, (I)nactive, (R)edundancy, (C)ondenmation, (D)isposed
            oParamSQL(6) = New SqlParameter("@strBrand", strBrand)                '--""=All
            oParamSQL(7) = New SqlParameter("@strDescription", strDescription)    '--""=All
            oParamSQL(8) = New SqlParameter("@strCost", strCost)                '--""=All
            oParamSQL(9) = New SqlParameter("@strCostCenter", strCostCenter)    '--""=All
            oParamSQL(10) = New SqlParameter("@strPurchaseFDt", strPurchaseFDt)   '--""=All
            oParamSQL(11) = New SqlParameter("@strPurchaseTDt", strPurchaseTDt)   '--""=All
            oParamSQL(12) = New SqlParameter("@strWarExpFDt", strWarExpFDt)       '--""=All
            oParamSQL(13) = New SqlParameter("@strWarExpTDt", strWarExpTDt)       '--""=All
            oParamSQL(14) = New SqlParameter("@intDeptID", intDeptID)            '--"0"=All
            oParamSQL(15) = New SqlParameter("@intLocID", intLocID)              '--"0"=All
            oParamSQL(16) = New SqlParameter("@strOwnerID", strOwnerID)          '--""=All
            oParamSQL(17) = New SqlParameter("@strOptionAMS", strOptionAMS)      '--"1"=like, "2"=Exactly, "3"=Not Equal
            oParamSQL(18) = New SqlParameter("@strOptionNFS", strOptionNFS)
            oParamSQL(19) = New SqlParameter("@strOptionBrand", strOptionBrand)
            oParamSQL(20) = New SqlParameter("@strOptionDesc", strOptionDesc)
            oParamSQL(21) = New SqlParameter("@strOptionCost", strOptionCost)    '--"1"='>', "2"='<', "3"='>=', "4"='<=', "5"='='
            oParamSQL(22) = New SqlParameter("@strOptionCostCtr", strOptionCostCtr)
            oParamSQL(23) = New SqlParameter("@strOrderBy1", strOrderBy1)
            oParamSQL(24) = New SqlParameter("@strOrderBy2", strOrderBy2)
            oParamSQL(25) = New SqlParameter("@strOrderBy3", strOrderBy3)
            oParamSQL(26) = New SqlParameter("@intLocSubID", intLocSubID)
            oParamSQL(27) = New SqlParameter("@strCtrlItemF", strCtrlItemF)
            oParamSQL(28) = New SqlParameter("@ShortDescription", ShortDescription)
            oParamSQL(29) = New SqlParameter("@OtherInformation", OtherInformation)

            oParamSQL(30) = New SqlParameter("@intDeviceTypeID", intDeviceTypeID)
            oParamSQL(31) = New SqlParameter("@strSerialNo", strSerialNo)
            oParamSQL(32) = New SqlParameter("@strMacAddress", strMacAddress)
            oParamSQL(33) = New SqlParameter("@strNewHostname", strNewHostname)
            oParamSQL(34) = New SqlParameter("@strStatus", strStatus)
            oParamSQL(35) = New SqlParameter("@strMachineModel", strMachineModel)

            fnDynamicRpt_GenerateRecord = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_DynamicRpt_GenerateRecord", oParamSQL)
        Catch ex As Exception
            fnDynamicRpt_GenerateRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDynamicRpt_InsertUpdateRptTemplate( _
                                        ByVal intReportID As String, _
                                        ByVal strReportTitle As String, _
                                        ByVal strFieldDisplay As String, _
                                        ByVal intUsrID As String, _
                                        ByVal strType As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Insert/Update REport TEmplate
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  09/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intReportID", intReportID)
            oParamSQL(1) = New SqlParameter("@strReportTitle", strReportTitle)
            oParamSQL(2) = New SqlParameter("@strFieldDisplay", strFieldDisplay)
            oParamSQL(3) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(4) = New SqlParameter("@strType", strType)

            fnDynamicRpt_InsertUpdateRptTemplate = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_DynamicRpt_InsertUpdateRptTemplate", oParamSQL)
        Catch ex As Exception
            fnDynamicRpt_InsertUpdateRptTemplate = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDynamicRpt_GetRptTemplateData( _
                                      ByVal intRptID As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get report data Fields
        'Returns		    :  sqldatareader
        'Author			    :  See Siew
        'Date			    :  08/11/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intRptID", intRptID)

            fnDynamicRpt_GetRptTemplateData = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_DynamicRpt_GetRptTemplateData", oParamSQL)
        Catch ex As Exception
            fnDynamicRpt_GetRptTemplateData = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDynamicRpt_GetField2Display( _
                                   ByVal strCallType As String, _
                                   ByVal strRptFields As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Display Fiellds
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  08/11/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strCallType", strCallType)   '--NR=New Report, ED=Edit Report template (Display Field), EN = Edit Report template (Not Display Field) 
            oParamSQL(1) = New SqlParameter("@strRptFields", strRptFields) '--Report Field to display (Format = 12^34^)
            fnDynamicRpt_GetField2Display = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_DynamicRpt_GetField2Display", oParamSQL)
        Catch ex As Exception
            fnDynamicRpt_GetField2Display = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDynamicRpt_DeleteRec( _
                                    ByVal strRptID As String, _
                                    ByVal intUsrID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To check for duplicate login ID 
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  19/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strRptID", strRptID)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)

            fnDynamicRpt_DeleteRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_DynamicRpt_DeleteRec", oParamSQL)
        Catch ex As Exception
            fnDynamicRpt_DeleteRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDynamicRpt_GetRptTemplate( _
                                ByVal StrSortName As String, _
                                ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get All Report Template Records 
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  08/11/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(1) = New SqlParameter("@strSortOrder", strSortOrder)
            fnDynamicRpt_GetRptTemplate = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_DynamicRpt_GetRptTemplate", oParamSQL)
        Catch ex As Exception
            fnDynamicRpt_GetRptTemplate = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnRep_GetRecForDynamicRep( _
                                                   ByVal strFileNum As String, _
                                                   ByVal strFileRef As String, _
                                                   ByVal strOffLocType As String, _
                                                   ByVal strOffLocIDs As String, _
                                                   ByVal strCreateFD As String, _
                                                   ByVal strCreateTD As String, _
                                                   ByVal strSecStatus As String, _
                                                   ByVal strFileStatus As String, _
                                                   ByVal strReviewFD As String, _
                                                   ByVal strReviewTD As String, _
                                                   ByVal strCloseFD As String, _
                                                   ByVal strCloseTD As String, _
                                                   ByVal strFileDisplay As String, _
                                                   ByVal StrSortName As String, _
                                                   ByVal strSortOrder As String, _
                                                   ByVal strLoginID As String, _
                                                   ByRef intRunNo As String _
                                                   ) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To get run no and search result
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  19/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(16) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strFileNum", strFileNum)
            oParamSQL(1) = New SqlParameter("@strFileRef", strFileRef)
            oParamSQL(2) = New SqlParameter("@strOffLocType", strOffLocType)
            oParamSQL(3) = New SqlParameter("@strOffLocIDs", strOffLocIDs)
            oParamSQL(4) = New SqlParameter("@strCreateFD", strCreateFD)
            oParamSQL(5) = New SqlParameter("@strCreateTD", strCreateTD)
            oParamSQL(6) = New SqlParameter("@strSecStatus", strSecStatus)
            oParamSQL(7) = New SqlParameter("@strFileStatus", strFileStatus)
            'oParamSQL(8) = New SqlParameter("@strDelinquency", strDelinquency)
            oParamSQL(8) = New SqlParameter("@strReviewFD", strReviewFD)
            oParamSQL(9) = New SqlParameter("@strReviewTD", strReviewTD)
            oParamSQL(10) = New SqlParameter("@strCloseFD", strCloseFD)
            oParamSQL(11) = New SqlParameter("@strCloseTD", strCloseTD)
            oParamSQL(12) = New SqlParameter("@strFileDisplay", strFileDisplay)
            oParamSQL(13) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(14) = New SqlParameter("@strSortOrder", strSortOrder)
            oParamSQL(15) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(16) = New SqlParameter("@intRunNo", SqlDbType.Int, 4)
            oParamSQL(16).Direction = ParameterDirection.Output

            fnRep_GetRecForDynamicRep = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Rep_GetRecForDynamicRep", oParamSQL)
            intRunNo = oParamSQL(16).Value
        Catch ex As Exception
            fnRep_GetRecForDynamicRep = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnRep_GetRecForDynamicRepForFTS( _
                                                   ByVal strFileRef As String, _
                                                   ByVal strFileDetailRef As String, _
                                                   ByVal strFileTitle As String, _
                                                   ByVal strOffLocType As String, _
                                                   ByVal strOffLocIDs As String, _
                                                   ByVal strFileStatus As String, _
                                                   ByVal strCreateFD As String, _
                                                   ByVal strCreateTD As String, _
                                                   ByVal strCloseFD As String, _
                                                   ByVal strCloseTD As String, _
                                                   ByVal strFileDisplay As String, _
                                                   ByVal StrSortName As String, _
                                                   ByVal strSortOrder As String, _
                                                   ByVal strLoginID As String, _
                                                   ByRef intRunNo As String _
                                                   ) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To get run no and search result
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  19/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(13) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strFileRef", strFileRef)
            oParamSQL(1) = New SqlParameter("@strFileDetailRef", strFileDetailRef)
            oParamSQL(2) = New SqlParameter("@strFileTitle", strFileTitle)
            oParamSQL(3) = New SqlParameter("@strOffLocType", strOffLocType)
            oParamSQL(4) = New SqlParameter("@strOffLocIDs", strOffLocIDs)
            oParamSQL(5) = New SqlParameter("@strFileStatus", strFileStatus)
            oParamSQL(6) = New SqlParameter("@strCreateFD", strCreateFD)
            oParamSQL(7) = New SqlParameter("@strCreateTD", strCreateTD)
            oParamSQL(8) = New SqlParameter("@strCloseFD", strCloseFD)
            oParamSQL(9) = New SqlParameter("@strCloseTD", strCloseTD)
            oParamSQL(10) = New SqlParameter("@strFileDisplay", strFileDisplay)
            oParamSQL(11) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(12) = New SqlParameter("@strSortOrder", strSortOrder)
            oParamSQL(13) = New SqlParameter("@strLoginID", strLoginID)
            'oParamSQL(14) = New SqlParameter("@intRunNo", SqlDbType.Int, 4)
            'oParamSQL(16).Direction = ParameterDirection.Output

            fnRep_GetRecForDynamicRepForFTS = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_Rep_GetRecForDynamicRep", oParamSQL)
            'intRunNo = oParamSQL(16).Value
        Catch ex As Exception
            fnRep_GetRecForDynamicRepForFTS = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnRep_GetRecForMstFileList( _
                                               ByVal strDeptID As String, _
                                               ByVal StrSortName As String, _
                                               ByVal strSortOrder As String _
                                               ) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To general Master File List
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  21/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strDeptID", strDeptID)
            oParamSQL(1) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(2) = New SqlParameter("@strSortOrder", strSortOrder)

            fnRep_GetRecForMstFileList = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_Rep_GetRecForMstFileList", oParamSQL)
        Catch ex As Exception
            fnRep_GetRecForMstFileList = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDeptGetAllRecDDL(ByVal strDeptType As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get All Department Records to populate in DropDownList  
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  25/01/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strDeptType", strDeptType)
            oParamSQL(1) = New SqlParameter("@StrSortName", "fld_DepartmentCode")
            oParamSQL(2) = New SqlParameter("@strSortOrder", "ASC")
            fnDeptGetAllRecDDL = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_Dept_GetAllRec", oParamSQL)
        Catch ex As Exception
            fnDeptGetAllRecDDL = Nothing
            Throw ex
        End Try
    End Function



End Class
