﻿Imports System
Imports System.Collections
Imports System.Linq
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Xml.Linq
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
' (c) Copyright Microsoft Corporation.
' This source is subject to the Microsoft Public License.
' See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
' All other rights reserved.
'*-------------------------------*
'*                               *
'*      Mahsa Hassankashi        *
'*     info@mahsakashi.com       *
'*   http://www.mahsakashi.com   * 
'*     kashi_mahsa@yahoo.com     * 
'*                               *
'*                               *
'*-------------------------------*
' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
Public Class AutoComplete
    Inherits System.Web.Services.WebService
    'Dim cn As New SqlClient.SqlConnection()
    'Dim ds As New DataSet
    'Dim dt As New DataTable
    <WebMethod()> _
    Public Function GetCompletionList(ByVal prefixText As String, ByVal count As Integer) As String()

        Dim dt As DataTable
        Dim items As List(Of String)

        dt = ClsUser.fnUsrGetUsrEmailFromAD(prefixText.ToString()).Tables(0)
        'dt = GetDataBySenderName(prefixText.ToString())
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(row("EmailContent").ToString())
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class
