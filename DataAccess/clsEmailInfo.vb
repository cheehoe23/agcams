#Region "Information Section"
' ****************************************************************************************************
' Description       : Email Class.
' Purpose           : To perform the business logic for email 
' Author			: See Siew
' Date			    : 04.11.2007
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsEmailInfo

    Public Shared Function fnEmail_GetEmailInfo4ContractExp2() As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get email info for contract expiry (2nd Reminder)
        'Returns		    :  sqldatareader
        'Author			    :  See Siew
        'Date			    :  07/03/2008
        '****************************************************************************************************
        Try
            fnEmail_GetEmailInfo4ContractExp2 = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Email_GetEmailInfo4ContractExp2")
        Catch ex As Exception
            fnEmail_GetEmailInfo4ContractExp2 = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnEmail_GetEmailInfo4FloatReplacement() As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get email info for contract expiry
        'Returns		    :  sqldatareader
        'Author			    :  See Siew
        'Date			    :  27/11/2007
        '****************************************************************************************************
        Try
            fnEmail_GetEmailInfo4FloatReplacement = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Email_GetEmailInfo4FloatReplacement")
        Catch ex As Exception
            fnEmail_GetEmailInfo4FloatReplacement = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnEmail_GetEmailInfo4ContractExp() As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get email info for contract expiry
        'Returns		    :  sqldatareader
        'Author			    :  See Siew
        'Date			    :  27/11/2007
        '****************************************************************************************************
        Try
            fnEmail_GetEmailInfo4ContractExp = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Email_GetEmailInfo4ContractExp")
        Catch ex As Exception
            fnEmail_GetEmailInfo4ContractExp = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnEmail_GetEmailInfo4WarrantyExp() As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get email info for warranty expiry
        'Returns		    :  sqldatareader
        'Author			    :  See Siew
        'Date			    :  27/11/2007
        '****************************************************************************************************
        Try
            fnEmail_GetEmailInfo4WarrantyExp = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Email_GetEmailInfo4WarrantyExp")
        Catch ex As Exception
            fnEmail_GetEmailInfo4WarrantyExp = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnEmail_GetEmailInfo4NewAssetNFS() As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get email info for new asset creation in NFS
        'Returns		    :  sqldatareader
        'Author			    :  See Siew
        'Date			    :  27/11/2007
        '****************************************************************************************************
        Try
            fnEmail_GetEmailInfo4NewAssetNFS = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Email_GetEmailInfo4NewAssetNFS")
        Catch ex As Exception
            fnEmail_GetEmailInfo4NewAssetNFS = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnEmail_GetEmailInfo4AssetTransfer( _
                                        ByVal strAssetIDs As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get email info for new asset creation in AMS
        'Returns		    :  sqldatareader
        'Author			    :  See Siew
        'Date			    :  04/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strAssetIDs", strAssetIDs)  '--Asset ID format -> 12^45^

            fnEmail_GetEmailInfo4AssetTransfer = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Email_GetEmailInfo4AssetTransfer", oParamSQL)
        Catch ex As Exception
            fnEmail_GetEmailInfo4AssetTransfer = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnEmail_GetEmailInfo4NewAssetAMS( _
                                    ByVal strAssetIDs As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get email info for new asset creation in AMS
        'Returns		    :  sqldatareader
        'Author			    :  See Siew
        'Date			    :  04/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strAssetIDs", strAssetIDs)  '--Asset ID format -> 12^45^

            fnEmail_GetEmailInfo4NewAssetAMS = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Email_GetEmailInfo4NewAssetAMS", oParamSQL)
        Catch ex As Exception
            fnEmail_GetEmailInfo4NewAssetAMS = Nothing
            Throw ex
        End Try
    End Function


End Class
