#Region "Information Section"
' ****************************************************************************************************
' Description       : Status Class.
' Purpose           : To perform the business logic for Status (such as view, add, delete, edit)
' Author			: See Siew
' Date			    : 23.03.2007
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsStatus
    Public Shared Function fnStatusGetRecForDDLOrByPCode( _
                                        ByVal strProgCode As String, _
                                        ByVal intStatusID As Integer, _
                                        ByVal intRemarkID As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get status Record for Dropdownlist or get particular status by ID/Program Code
        'Returns		    :  sqldatareader
        'Author			    :  See Siew
        'Date			    :  07/04/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strProgCode", strProgCode)
            oParamSQL(1) = New SqlParameter("@intStatusID", intStatusID)
            oParamSQL(2) = New SqlParameter("@intRemarkID", intRemarkID)

            fnStatusGetRecForDDLOrByPCode = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_Status_GetRecForDDLOrByPCode", oParamSQL)
        Catch ex As Exception
            fnStatusGetRecForDDLOrByPCode = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnStatusEditStatus( _
                                            ByVal intStatusID As Integer, _
                                            ByVal intRemarkID As Integer, _
                                            ByVal strStatusName As String, _
                                            ByVal strLocChangeF As String, _
                                            ByVal intLocID As Integer, _
                                            ByVal strFieldEdit As String, _
                                            ByVal RemarkExist As String, _
                                            ByVal strLoginID As String, _
                                            ByVal strType As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To update status/Sub Status 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  05/04/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(8) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intStatusID", intStatusID)
            oParamSQL(1) = New SqlParameter("@intRemarkID", intRemarkID)
            oParamSQL(2) = New SqlParameter("@strStatusName", strStatusName)
            oParamSQL(3) = New SqlParameter("@strLocChangeF", strLocChangeF)
            oParamSQL(4) = New SqlParameter("@intLocID", intLocID)
            oParamSQL(5) = New SqlParameter("@strFieldEdit", strFieldEdit)
            oParamSQL(6) = New SqlParameter("@RemarkExist", RemarkExist)
            oParamSQL(7) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(8) = New SqlParameter("@strType", strType)

            fnStatusEditStatus = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Status_EditStatus", oParamSQL)
        Catch ex As Exception
            fnStatusEditStatus = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnStatusAddDefaultSubStatusForModify( _
                                        ByVal intStatusID As Integer, _
                                        ByVal strSStatusLocChangeF As String, _
                                        ByVal intStatusLocID As Integer, _
                                        ByVal strStatusFieldEdit As String, _
                                        ByVal strLoginID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Add Default Sub Status for Status and delete all current sub status
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  04/04/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intStatusID", intStatusID)
            oParamSQL(1) = New SqlParameter("@strSStatusLocChangeF", strSStatusLocChangeF)
            oParamSQL(2) = New SqlParameter("@intStatusLocID", intStatusLocID)
            oParamSQL(3) = New SqlParameter("@strStatusFieldEdit", strStatusFieldEdit)
            oParamSQL(4) = New SqlParameter("@strLoginID", strLoginID)

            fnStatusAddDefaultSubStatusForModify = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Status_AddDefaultSubStatusForModify", oParamSQL)
        Catch ex As Exception
            fnStatusAddDefaultSubStatusForModify = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnStatusGetRecordsForMainStatus( _
                                        ByVal strStatusID As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To get record for status
        'Returns		    :  sqldatareader
        'Author			    :  See Siew
        'Date			    :  04/04/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strStatusID", strStatusID)

            fnStatusGetRecordsForMainStatus = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Status_GetRecordsForMainStatus", oParamSQL)
        Catch ex As Exception
            fnStatusGetRecordsForMainStatus = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnStatusDeleteStatus( _
                                    ByVal strStatus As String, _
                                    ByVal strRemark As String, _
                                    ByVal strLoginID As String, _
                                    ByVal strType As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To delete status/sub status 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  03/04/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strStatus", strStatus)
            oParamSQL(1) = New SqlParameter("@strRemark", strRemark)
            oParamSQL(2) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(3) = New SqlParameter("@strType", strType)

            fnStatusDeleteStatus = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Status_DeleteStatus", oParamSQL)
        Catch ex As Exception
            fnStatusDeleteStatus = Nothing
            Throw ex
        End Try
    End Function


    Public Shared Function fnStatusGetRecord( _
                                        ByVal strStatusID As Integer, _
                                        ByVal strRemarkID As Integer, _
                                        ByVal StrSortName As String, _
                                        ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Status/Sub status record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  03/04/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strStatusID", strStatusID)
            oParamSQL(1) = New SqlParameter("@strRemarkID", strRemarkID)
            oParamSQL(2) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(3) = New SqlParameter("@strSortOrder", strSortOrder)

            fnStatusGetRecord = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Status_GetRecords", oParamSQL)
        Catch ex As Exception
            fnStatusGetRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnStatusGetRecordDR( _
                                    ByVal strStatusID As Integer, _
                                    ByVal strRemarkID As Integer, _
                                    ByVal StrSortName As String, _
                                    ByVal strSortOrder As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Status/Sub status record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  03/04/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strStatusID", strStatusID)
            oParamSQL(1) = New SqlParameter("@strRemarkID", strRemarkID)
            oParamSQL(2) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(3) = New SqlParameter("@strSortOrder", strSortOrder)

            fnStatusGetRecordDR = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Status_GetRecords", oParamSQL)
        Catch ex As Exception
            fnStatusGetRecordDR = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnStatusAddNewSubStatusForModify( _
                                    ByVal intStatusID As Integer, _
                                    ByVal intSStatusID As Integer, _
                                    ByVal strReplace As String, _
                                    ByVal strSStatusName As String, _
                                    ByVal strSStatusLocChangeF As String, _
                                    ByVal intStatusLocID As Integer, _
                                    ByVal strStatusFieldEdit As String, _
                                    ByVal strLoginID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To add new sub status in database for Modify Screen
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  02/04/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(7) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intStatusID", intStatusID)
            oParamSQL(1) = New SqlParameter("@intSStatusID", intSStatusID)
            oParamSQL(2) = New SqlParameter("@strReplace", strReplace)
            oParamSQL(3) = New SqlParameter("@strSStatusName", strSStatusName)
            oParamSQL(4) = New SqlParameter("@strSStatusLocChangeF", strSStatusLocChangeF)
            oParamSQL(5) = New SqlParameter("@intStatusLocID", intStatusLocID)
            oParamSQL(6) = New SqlParameter("@strStatusFieldEdit", strStatusFieldEdit)
            oParamSQL(7) = New SqlParameter("@strLoginID", strLoginID)

            fnStatusAddNewSubStatusForModify = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Status_AddNewSubStatusForModify", oParamSQL)
        Catch ex As Exception
            fnStatusAddNewSubStatusForModify = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnStatusAddNewStatus( _
                                ByVal strStatusName As String, _
                                ByVal intStatusLocChangeF As String, _
                                ByVal intStatusLocID As Integer, _
                                ByVal strStatusFieldEdit As String, _
                                ByVal strHaveSubStatus As String, _
                                ByVal strSubStatusXML As String, _
                                ByVal strLoginID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To add new status and sub status (if applicable) in database
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  23/03/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(6) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strStatusName", strStatusName)
            oParamSQL(1) = New SqlParameter("@intStatusLocChangeF", intStatusLocChangeF)
            oParamSQL(2) = New SqlParameter("@intStatusLocID", intStatusLocID)
            oParamSQL(3) = New SqlParameter("@strStatusFieldEdit", strStatusFieldEdit)
            oParamSQL(4) = New SqlParameter("@strHaveSubStatus", strHaveSubStatus)
            oParamSQL(5) = New SqlParameter("@strSubStatusXML", strSubStatusXML)
            oParamSQL(6) = New SqlParameter("@strLoginID", strLoginID)

            fnStatusAddNewStatus = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Status_AddNewStatus", oParamSQL)
        Catch ex As Exception
            fnStatusAddNewStatus = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnStatusCheckDuplicate( _
                            ByVal intStatusID As Integer, _
                            ByVal strStatusName As String, _
                            ByVal strStatusType As String, _
                            ByVal strType As String, _
                            ByRef strCheckDupF As String) As Boolean
        '****************************************************************************************************
        'Purpose  		    :  To check for duplicate Status Name/Sub Status Name
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  23/03/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intStatusID", intStatusID)
            oParamSQL(1) = New SqlParameter("@strStatusName", strStatusName)
            oParamSQL(2) = New SqlParameter("@strStatusType", strStatusType)
            oParamSQL(3) = New SqlParameter("@strType", strType)
            oParamSQL(4) = New SqlParameter("@strRetVal", SqlDbType.Char, 1)
            oParamSQL(4).Direction = ParameterDirection.Output
            SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Status_CheckDuplicate", oParamSQL)
            strCheckDupF = oParamSQL(4).Value
        Catch ex As Exception
            fnStatusCheckDuplicate = Nothing
            Throw ex
        End Try
    End Function
End Class
