#Region "Information Section"
' ****************************************************************************************************
' Description       : Location Class.
' Purpose           : To perform the business logic for General Settings
' Author			: See Siew
' Date			    : 22.10.2007
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsGeneralSetting

    Public Shared Function fnGnrSetUpdateGeneralSetting( _
        ByVal strInventoryEmail As String, _
        ByVal strNFSActive As String, ByVal strNFSSubj As String, ByVal strNFSSendTo As String, ByVal strNFSSendToOther As String, ByVal strNFSContent As String, _
        ByVal strAMSActive As String, ByVal strAMSSubj As String, ByVal strAMSSendTo As String, ByVal strAMSSendToOther As String, ByVal strAMSContent As String, _
        ByVal strWarExpActive As String, ByVal strWarExpSubj As String, ByVal strWarExpSendTo As String, ByVal strWarExpSendToOther As String, ByVal strWarExpContent As String, _
        ByVal strCondemnExpActive As String, ByVal strCondemnExpSubj As String, ByVal strCondemnExpSendTo As String, ByVal strCondemnExpSendToOther As String, ByVal strCondemnExpContent As String, _
        ByVal strLeasedInvExpActive As String, ByVal strLeasedInvExpSubj As String, ByVal strLeasedInvExpSendTo As String, ByVal strLeasedInvExpSendToOther As String, ByVal strLeasedInvExpContent As String, _
        ByVal strCtrExpActive As String, ByVal strCtrExpSubj As String, ByVal strCtrExpSendTo As String, ByVal strCtrExpSendToOther As String, ByVal strCtrExpContent As String, _
        ByVal strAssTrfActive As String, ByVal strAssTrfSubj As String, ByVal strAssTrfSendTo As String, ByVal strAssTrfSendToOther As String, ByVal strAssTrfContent As String, _
        ByVal strFloatActive As String, ByVal strFloatSubj As String, ByVal strFloatSendTo As String, ByVal strFloatSendToOther As String, ByVal strFloatContent As String, _
        ByVal strRenewalCtrlDays As String, _
        ByVal strStockTakeActive As String, ByVal strStockTakeFrmDt As String, ByVal strStockTakeToDt As String, _
        ByVal strCtrExpActive2 As String, ByVal strCtrExpSubj2 As String, ByVal strCtrExpSendTo2 As String, ByVal strCtrExpSendToOther2 As String, ByVal strCtrExpContent2 As String, _
        ByVal strCtrExpSendCCOther As String, ByVal strCtrExpSendCCOther2 As String, _
        ByVal strLoginID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To update general setting information
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  23/10/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(52) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strInventoryEmail", strInventoryEmail)
            oParamSQL(1) = New SqlParameter("@strNFSActive", strNFSActive)
            oParamSQL(2) = New SqlParameter("@strNFSSubj", strNFSSubj)
            oParamSQL(3) = New SqlParameter("@strNFSSendTo", strNFSSendTo)
            oParamSQL(4) = New SqlParameter("@strNFSSendToOther", strNFSSendToOther)
            oParamSQL(5) = New SqlParameter("@strNFSContent", strNFSContent)
            oParamSQL(6) = New SqlParameter("@strAMSActive", strAMSActive)
            oParamSQL(7) = New SqlParameter("@strAMSSubj", strAMSSubj)
            oParamSQL(8) = New SqlParameter("@strAMSSendTo", strAMSSendTo)
            oParamSQL(9) = New SqlParameter("@strAMSSendToOther", strAMSSendToOther)
            oParamSQL(10) = New SqlParameter("@strAMSContent", strAMSContent)
            oParamSQL(11) = New SqlParameter("@strWarExpActive", strWarExpActive)
            oParamSQL(12) = New SqlParameter("@strWarExpSubj", strWarExpSubj)
            oParamSQL(13) = New SqlParameter("@strWarExpSendTo", strWarExpSendTo)
            oParamSQL(14) = New SqlParameter("@strWarExpSendToOther", strWarExpSendToOther)
            oParamSQL(15) = New SqlParameter("@strWarExpContent", strWarExpContent)
            oParamSQL(16) = New SqlParameter("@strCtrExpActive", strCtrExpActive)
            oParamSQL(17) = New SqlParameter("@strCtrExpSubj", strCtrExpSubj)
            oParamSQL(18) = New SqlParameter("@strCtrExpSendTo", strCtrExpSendTo)
            oParamSQL(19) = New SqlParameter("@strCtrExpSendToOther", strCtrExpSendToOther)
            oParamSQL(20) = New SqlParameter("@strCtrExpContent", strCtrExpContent)
            oParamSQL(21) = New SqlParameter("@strAssTrfActive", strAssTrfActive)
            oParamSQL(22) = New SqlParameter("@strAssTrfSubj", strAssTrfSubj)
            oParamSQL(23) = New SqlParameter("@strAssTrfSendTo", strAssTrfSendTo)
            oParamSQL(24) = New SqlParameter("@strAssTrfSendToOther", strAssTrfSendToOther)
            oParamSQL(25) = New SqlParameter("@strAssTrfContent", strAssTrfContent)
            oParamSQL(26) = New SqlParameter("@strFloatActive", strFloatActive)
            oParamSQL(27) = New SqlParameter("@strFloatSubj", strFloatSubj)
            oParamSQL(28) = New SqlParameter("@strFloatSendTo", strFloatSendTo)
            oParamSQL(29) = New SqlParameter("@strFloatSendToOther", strFloatSendToOther)
            oParamSQL(30) = New SqlParameter("@strFloatContent", strFloatContent)
            oParamSQL(31) = New SqlParameter("@strRenewalCtrlDays", strRenewalCtrlDays)
            oParamSQL(32) = New SqlParameter("@strStockTakeActive", strStockTakeActive)
            oParamSQL(33) = New SqlParameter("@strStockTakeFrmDt", strStockTakeFrmDt)
            oParamSQL(34) = New SqlParameter("@strStockTakeToDt", strStockTakeToDt)
            oParamSQL(35) = New SqlParameter("@strCtrExpActive2", strCtrExpActive2)
            oParamSQL(36) = New SqlParameter("@strCtrExpSubj2", strCtrExpSubj2)
            oParamSQL(37) = New SqlParameter("@strCtrExpSendTo2", strCtrExpSendTo2)
            oParamSQL(38) = New SqlParameter("@strCtrExpSendToOther2", strCtrExpSendToOther2)
            oParamSQL(39) = New SqlParameter("@strCtrExpContent2", strCtrExpContent2)
            oParamSQL(40) = New SqlParameter("@strCtrExpSendCCOther", strCtrExpSendCCOther)
            oParamSQL(41) = New SqlParameter("@strCtrExpSendCCOther2", strCtrExpSendCCOther2)
            oParamSQL(42) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(43) = New SqlParameter("@strCondemnExpActive", strCondemnExpActive)
            oParamSQL(44) = New SqlParameter("@strCondemnExpSubj", strCondemnExpSubj)
            oParamSQL(45) = New SqlParameter("@strCondemnExpSendTo", strCondemnExpSendTo)
            oParamSQL(46) = New SqlParameter("@strCondemnExpSendToOther", strCondemnExpSendToOther)
            oParamSQL(47) = New SqlParameter("@strCondemnExpContent", strCondemnExpContent)
            oParamSQL(48) = New SqlParameter("@strLeasedInvExpActive", strLeasedInvExpActive)
            oParamSQL(49) = New SqlParameter("@strLeasedInvExpSubj", strLeasedInvExpSubj)
            oParamSQL(50) = New SqlParameter("@strLeasedInvExpSendTo", strLeasedInvExpSendTo)
            oParamSQL(51) = New SqlParameter("@strLeasedInvExpSendToOther", strLeasedInvExpSendToOther)
            oParamSQL(52) = New SqlParameter("@strLeasedInvExpContent", strLeasedInvExpContent)

            fnGnrSetUpdateGeneralSetting = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_GnrSet_UpdateGeneralSetting", oParamSQL)
        Catch ex As Exception
            fnGnrSetUpdateGeneralSetting = Nothing
            Throw ex
        End Try
    End Function


    Public Shared Function fnGrlSetGetGeneralSettingInfo( _
                                    ByVal strCallType As String, _
                                    ByVal strFieldName As String, _
                                    ByVal strSystForF As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get general settings information 
        'Returns		    :  SQLDataReader
        'Author			    :  See Siew
        'Date			    :  27/02/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strCallType", strCallType)
            oParamSQL(1) = New SqlParameter("@strFieldName", strFieldName)
            oParamSQL(2) = New SqlParameter("@strSystForF", strSystForF)
            fnGrlSetGetGeneralSettingInfo = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_GnrSet_GetGeneralSettingInfo", oParamSQL)
        Catch ex As Exception
            fnGrlSetGetGeneralSettingInfo = Nothing
            Throw ex
        End Try
    End Function
End Class
