#Region "Project Information Section"
' ****************************************************************************************************
' Description       : Connection String
' Purpose           : The module is to get connection string
' **************************************************************************************************** 
#End Region

Module modConnectionSetting
    Public ReadOnly Property cnFTSDBString()
        Get
            'Return _cnString
            Return ConfigurationSettings.AppSettings("dbconn")
        End Get
    End Property

    Public ReadOnly Property cnFTSDBStringForFTS()
        Get
            'Return _cnString
            Return ConfigurationSettings.AppSettings("dbconnFTS")
        End Get
    End Property

    Public ReadOnly Property cnERegistryAddressString()
        Get
            'Return E-Registry Address 
            Return ConfigurationSettings.AppSettings("eRegistryAdd")
        End Get
    End Property

    Public ReadOnly Property gCondemnPath()
        Get
            'Return Condemn File Path 
            Return ConfigurationSettings.AppSettings("CondemnPath")
        End Get
    End Property

    Public ReadOnly Property gCondemnAppPage()
        Get
            'Return Condemn File Path 
            Return ConfigurationSettings.AppSettings("CondemnAppPage")
        End Get
    End Property

    Public ReadOnly Property gCondemnEReg()
        Get
            'Return Condemn File eRegistry Address
            Return ConfigurationSettings.AppSettings("CondemnEReg")
        End Get
    End Property

    Public ReadOnly Property gRedunPath()
        Get
            'Return Redundancy File Path
            Return ConfigurationSettings.AppSettings("RedunPath")
        End Get
    End Property

    Public ReadOnly Property gRedunAppPage()
        Get
            'Return Redundancy File Path
            Return ConfigurationSettings.AppSettings("RedunAppPage")
        End Get
    End Property

    Public ReadOnly Property gRedunEReg()
        Get
            'Return Redundancy File E-Registry Address 
            Return ConfigurationSettings.AppSettings("RedunEReg")
        End Get
    End Property

    Public ReadOnly Property gAssetPath()
        Get
            'Return Asset File Path 
            Return ConfigurationSettings.AppSettings("AssetPath")
        End Get
    End Property

    Public ReadOnly Property gCategoryPath()
        Get
            'Return Category File Path 
            Return ConfigurationSettings.AppSettings("CategoryPath")
        End Get
    End Property

    Public ReadOnly Property gRFIDLabelPrintF()
        Get
            'Return RFID Label File Path 
            Return ConfigurationSettings.AppSettings("RFIDLabelPrintF")
        End Get
    End Property

    Public ReadOnly Property gRFIDPrinterIP()
        Get
            'Return RFID Label File Path 
            Return ConfigurationSettings.AppSettings("RFIDPrinterIP")
        End Get
    End Property

    Public ReadOnly Property gRFIDWriteEXE()
        Get
            'Return RFID Label File Path 
            Return ConfigurationSettings.AppSettings("RFIDWriteEXE")
        End Get
    End Property

    Public ReadOnly Property gRFIDPrintBatch()
        Get
            'Return RFID Label File Path 
            Return ConfigurationSettings.AppSettings("RFIDPrintBatch")
        End Get
    End Property

    Public ReadOnly Property gSmartShelfLight()
        Get
            'Return RFID Label File Path 
            Return ConfigurationSettings.AppSettings("SmartShelfLight")
        End Get
    End Property

    Public ReadOnly Property gContractEmailExpActPage()
        Get
            'Return Contract Email Expiry Action Page 
            Return ConfigurationSettings.AppSettings("ContractEmailExpActPage")
        End Get
    End Property

    'Printer Location
    Public ReadOnly Property gAMSPrinterLocLevel7()
        Get
            'Return Contract Email Expiry Action Page 
            Return ConfigurationSettings.AppSettings("AMSPrinterLocLevel7")
        End Get
    End Property
    Public ReadOnly Property gAMSPrinterLocLevel8()
        Get
            'Return Contract Email Expiry Action Page 
            Return ConfigurationSettings.AppSettings("AMSPrinterLocLevel8")
        End Get
    End Property
    Public ReadOnly Property gFTSPrinterLocLevel9()
        Get
            'Return Contract Email Expiry Action Page 
            Return ConfigurationSettings.AppSettings("FTSPrinterLocLevel9")
        End Get
    End Property

    Public ReadOnly Property gFTSPrinterLocL9AMS02()
        Get
            'Return Contract Email Expiry Action Page 
            Return ConfigurationSettings.AppSettings("FTSPrinterLocL9AMS02")
        End Get
    End Property

    Public ReadOnly Property gFTSPrinterLocLevel10()
        Get
            'Return Contract Email Expiry Action Page 
            Return ConfigurationSettings.AppSettings("FTSPrinterLocLevel10")
        End Get
    End Property

    'RFID Loc Path
    Public ReadOnly Property gRFIDLabelPath()
        Get
            'Return RFID Label File Path 
            Return ConfigurationSettings.AppSettings("RFIDLabelPath")
        End Get
    End Property

    Public ReadOnly Property gAMSRFIDLabelLevel7Path()
        Get
            'Return RFID Label File Path 
            Return ConfigurationSettings.AppSettings("AMSRFIDLabelLevel7Path")
        End Get
    End Property

    Public ReadOnly Property gAMSRFIDLabelLevel8Path()
        Get
            'Return RFID Label File Path 
            Return ConfigurationSettings.AppSettings("AMSRFIDLabelLevel8Path")
        End Get
    End Property

    Public ReadOnly Property gFTSRFIDLabelLevel9Path()
        Get
            'Return RFID Label File Path 
            Return ConfigurationSettings.AppSettings("FTSRFIDLabelLevel9Path")
        End Get
    End Property

    Public ReadOnly Property gFTSRFIDLabelL9AMS02Path()
        Get
            'Return Contract Email Expiry Action Page 
            Return ConfigurationSettings.AppSettings("FTSRFIDLabelL9AMS02Path")
        End Get
    End Property
End Module
