#Region "Information Section"
' ****************************************************************************************************
' Description       : Issuance Class.
' Purpose           : To perform the business logic for issuance Management (such as view, add, delete, edit)
' Author			: See Siew
' Date			    : 15.02.2008
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsIssuance

    Public Shared Function fnIssuancePDA_UpdateRec( _
                                        ByVal strAssetXML As String, _
                                        ByVal intusrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To update Issuance record(s)
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  24/06/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetXML", strAssetXML)
            oParamSQL(1) = New SqlParameter("@intusrID", intusrID)

            fnIssuancePDA_UpdateRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_IssuancePDA_UpdateRec", oParamSQL)
        Catch ex As Exception
            fnIssuancePDA_UpdateRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnIssuancePDA_DeleteRec( _
                                    ByVal strAssetXML As String, _
                                    ByVal intusrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To delete Issuance record(s)
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  24/06/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetXML", strAssetXML)
            oParamSQL(1) = New SqlParameter("@intusrID", intusrID)

            fnIssuancePDA_DeleteRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_IssuancePDA_DeleteRec", oParamSQL)
        Catch ex As Exception
            fnIssuancePDA_DeleteRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnIssuancePDA_GetRecords( _
                                ByVal intIssNo As Integer) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Issuance Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  23/06/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intIssNo", intIssNo)

            fnIssuancePDA_GetRecords = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_IssuancePDA_GetRecords", oParamSQL)
        Catch ex As Exception
            fnIssuancePDA_GetRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnIssuancePDAUpload_UploadRecord( _
                             ByVal strAssetXML As String, ByVal strAssetMvXML As String, _
                             ByVal intusrID As Integer, ByRef intIssNo As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To insert Issuance Records from PDA OUT-File (download from PDA)
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  21/06/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetXML", strAssetXML)
            oParamSQL(1) = New SqlParameter("@strAssetMvXML", strAssetMvXML)
            oParamSQL(2) = New SqlParameter("@intusrID", intusrID)
            oParamSQL(3) = New SqlParameter("@intIssNo", SqlDbType.Int, 4)
            oParamSQL(3).Direction = ParameterDirection.Output

            fnIssuancePDAUpload_UploadRecord = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_IssuancePDAUpload_UploadRecord", oParamSQL)
            intIssNo = oParamSQL(3).Value

        Catch ex As Exception
            fnIssuancePDAUpload_UploadRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnIssuanceUploadExcel_UpdateAllAsset( _
            ByVal xmlAsset As String, ByVal strLoginID As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Asset Result for Excel File
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  01/03/2008
        '****************************************************************************************************
        Try
            Dim DBconn As New SqlConnection(cnFTSDBString)
            Dim cmd As New SqlCommand
            Dim ds As New DataSet
            Dim da As SqlDataAdapter

            DBconn.Open()
            cmd.Connection = DBconn
            cmd.CommandTimeout = "3000" '"0" 'according to BL, changed by CSS, on 31 OCT 2008
            cmd.CommandText = "P_IssuanceUploadExcel_UpdateAllAsset"
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@xmlAsset", SqlDbType.Xml)
            cmd.Parameters(0).Value = xmlAsset
            cmd.Parameters.Add("@strLoginID", SqlDbType.VarChar)
            cmd.Parameters(1).Value = strLoginID

            'create the DataAdapter & DataSet
            da = New SqlDataAdapter(cmd)

            'fill the DataSet using default values for DataTable names, etc.
            da.Fill(ds)

            fnIssuanceUploadExcel_UpdateAllAsset = ds

        Catch ex As Exception
            fnIssuanceUploadExcel_UpdateAllAsset = Nothing
            Throw ex
        End Try
    End Function

    'Public Shared Function fnIssuanceUploadExcel_UpdateAllAsset( _
    '                   ByVal xmlAsset As String, _
    '                   ByVal strLoginID As String) As DataSet
    '    '****************************************************************************************************
    '    'Purpose  		    :  To Get Asset Result for Excel File
    '    'Returns		    :  Dataset
    '    'Author			    :  See Siew
    '    'Date			    :  01/03/2008
    '    '****************************************************************************************************
    '    Try
    '        Dim oParamSQL(1) As SqlParameter
    '        oParamSQL(0) = New SqlParameter("@xmlAsset", xmlAsset)
    '        oParamSQL(1) = New SqlParameter("@strLoginID", strLoginID)

    '        fnIssuanceUploadExcel_UpdateAllAsset = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_IssuanceUploadExcel_UpdateAllAsset", oParamSQL)
    '    Catch ex As Exception
    '        fnIssuanceUploadExcel_UpdateAllAsset = Nothing
    '        Throw ex
    '    End Try
    'End Function

    'Public Shared Function fnIssuanceUploadExcel_UpdateAsset( _
    '                    ByVal strAssetID As String, ByVal strDesc As String, _
    '                    ByVal strDept As String, ByVal strLoc As String, ByVal strOwner As String, _
    '                    ByRef strUpdateF As String, ByRef strReason As String, ByVal strLoginID As String) As Integer
    '    '****************************************************************************************************
    '    'Purpose  		    :  To Update Asset Record for Upload Excel File
    '    'Returns		    :  integer
    '    'Author			    :  See Siew
    '    'Date			    :  18/02/2008
    '    '****************************************************************************************************
    '    Try

    '        Dim DBconn As New SqlConnection(cnFTSDBString)
    '        Dim cmd As New SqlCommand

    '        DBconn.Open()
    '        cmd.Connection = DBconn
    '        cmd.CommandTimeout = "60" '"3000"
    '        cmd.CommandText = "P_IssuanceUploadExcel_UpdateAsset"
    '        cmd.CommandType = CommandType.StoredProcedure

    '        cmd.Parameters.Add("@strAssetID", SqlDbType.VarChar)
    '        cmd.Parameters(0).Value = strAssetID
    '        cmd.Parameters.Add("@strDesc", SqlDbType.VarChar)
    '        cmd.Parameters(1).Value = strDesc
    '        cmd.Parameters.Add("@strDept", SqlDbType.VarChar)
    '        cmd.Parameters(2).Value = strDept
    '        cmd.Parameters.Add("@strLoc", SqlDbType.VarChar)
    '        cmd.Parameters(3).Value = strLoc
    '        cmd.Parameters.Add("@strOwner", SqlDbType.VarChar)
    '        cmd.Parameters(4).Value = strOwner
    '        cmd.Parameters.Add("@strUpdateF", SqlDbType.Char, 1)
    '        cmd.Parameters(5).Direction = ParameterDirection.Output
    '        cmd.Parameters.Add("@strReason", SqlDbType.VarChar, 2000)
    '        cmd.Parameters(6).Direction = ParameterDirection.Output
    '        cmd.Parameters.Add("@strLoginID", SqlDbType.VarChar)
    '        cmd.Parameters(7).Value = strLoginID

    '        cmd.ExecuteNonQuery()

    '        strUpdateF = cmd.Parameters(5).Value
    '        strReason = cmd.Parameters(6).Value

    '        DBconn.Close()
    '    Catch ex As Exception
    '        fnIssuanceUploadExcel_UpdateAsset = Nothing
    '        Throw ex
    '    End Try
    'End Function


    'Public Shared Function fnIssuanceUploadExcel_UpdateAsset( _
    '                ByVal strAssetID As String, ByVal strDesc As String, _
    '                ByVal strDept As String, ByVal strLoc As String, ByVal strOwner As String, _
    '                ByRef strUpdateF As String, ByRef strReason As String, ByVal strLoginID As String) As Integer
    '    '****************************************************************************************************
    '    'Purpose  		    :  To Update Asset Record for Upload Excel File
    '    'Returns		    :  integer
    '    'Author			    :  See Siew
    '    'Date			    :  18/02/2008
    '    '****************************************************************************************************
    '    Try
    '        Dim oParamSQL(7) As SqlParameter
    '        oParamSQL(0) = New SqlParameter("@strAssetID", strAssetID)
    '        oParamSQL(1) = New SqlParameter("@strDesc", strDesc)
    '        oParamSQL(2) = New SqlParameter("@strDept", strDept)
    '        oParamSQL(3) = New SqlParameter("@strLoc", strLoc)
    '        oParamSQL(4) = New SqlParameter("@strOwner", strOwner)
    '        oParamSQL(5) = New SqlParameter("@strUpdateF", SqlDbType.Char, 1)
    '        oParamSQL(5).Direction = ParameterDirection.Output
    '        oParamSQL(6) = New SqlParameter("@strReason", SqlDbType.VarChar, 2000)
    '        oParamSQL(6).Direction = ParameterDirection.Output
    '        oParamSQL(7) = New SqlParameter("@strLoginID", strLoginID)

    '        fnIssuanceUploadExcel_UpdateAsset = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_IssuanceUploadExcel_UpdateAsset", oParamSQL)

    '        strUpdateF = oParamSQL(5).Value
    '        strReason = oParamSQL(6).Value
    '    Catch ex As Exception
    '        fnIssuanceUploadExcel_UpdateAsset = Nothing
    '        Throw ex
    '    End Try
    'End Function

    Public Shared Function fnIssuanceGenExcel_GetAssetRec( _
                    ByVal strAssetIDs As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Asset Record for Excel File
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  16/02/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strAssetIDs", strAssetIDs)       '--""=All

            fnIssuanceGenExcel_GetAssetRec = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_IssuanceGenExcel_GetAssetRec", oParamSQL)
        Catch ex As Exception
            fnIssuanceGenExcel_GetAssetRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnIssuanceGenExcel_SearchRecord( _
                ByVal strAssetIDAMS As String, ByVal strAssetIDNFS As String, _
                ByVal strAssetType As String, ByVal intAssetCatID As Integer, ByVal intAssetCatSubID As Integer, _
                ByVal strAssetStatus As String, ByVal strPurchaseFDt As String, ByVal strPurchaseTDt As String, _
                ByVal strWarExpFDt As String, ByVal strWarExpTDt As String, _
                ByVal intDeptID As Integer, ByVal intLocID As Integer, ByVal strOwnerID As String, _
                ByVal strTempAsset As String, ByVal StrSortName As String, ByVal strSortOrder As String, _
                ByVal intLocSubID As String, ByVal strCtrlItemF As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Seacrh Asset Record for Issuance Management
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  15/02/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(17) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDAMS", strAssetIDAMS)       '--""=All
            oParamSQL(1) = New SqlParameter("@strAssetIDNFS", strAssetIDNFS)       '--""=All
            oParamSQL(2) = New SqlParameter("@strAssetType", strAssetType)         '--""=All, A=Fixed Asset, I=Inventory
            oParamSQL(3) = New SqlParameter("@intAssetCatID", intAssetCatID)       '--"0"=All
            oParamSQL(4) = New SqlParameter("@intAssetCatSubID", intAssetCatSubID) '--"0"=All
            oParamSQL(5) = New SqlParameter("@strAssetStatus", strAssetStatus)   '--"0"=All, (A)ctive, (I)nactive, (R)edundancy, (C)ondenmation, (D)isposed
            oParamSQL(6) = New SqlParameter("@strPurchaseFDt", strPurchaseFDt)   '--""=All
            oParamSQL(7) = New SqlParameter("@strPurchaseTDt", strPurchaseTDt)   '--""=All
            oParamSQL(8) = New SqlParameter("@strWarExpFDt", strWarExpFDt)       '--""=All
            oParamSQL(9) = New SqlParameter("@strWarExpTDt", strWarExpTDt)       '--""=All
            oParamSQL(10) = New SqlParameter("@intDeptID", intDeptID)            '--"0"=All
            oParamSQL(11) = New SqlParameter("@intLocID", intLocID)              '--"0"=All
            oParamSQL(12) = New SqlParameter("@strOwnerID", strOwnerID)          '--""=All
            oParamSQL(13) = New SqlParameter("@strTempAsset", strTempAsset)      '--""=All, "Y"=Yes, "N"=No
            oParamSQL(14) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(15) = New SqlParameter("@strSortOrder", strSortOrder)
            oParamSQL(16) = New SqlParameter("@intLocSubID", intLocSubID)
            oParamSQL(17) = New SqlParameter("@strCtrlItemF", strCtrlItemF)

            fnIssuanceGenExcel_SearchRecord = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_IssuanceGenExcel_SearchRecord", oParamSQL)
        Catch ex As Exception
            fnIssuanceGenExcel_SearchRecord = Nothing
            Throw ex
        End Try
    End Function
End Class
