#Region "Information Section"
' ****************************************************************************************************
' Description       : Offline Proccessing Class.
' Purpose           : To perform the business logic for Offline Proccessing (such as view, add, delete, edit)
' Author			: See Siew
' Date			    : 11.11.2005
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsOfflineProccess

    Public Shared Function fnOfflineProPeriod_CheckForDuplicate( _
                              ByVal strSTPeriodIDs As String, _
                              ByVal strSTFrmDt As String, _
                              ByVal strSTToDt As String, _
                              ByVal strType As String) As Boolean
        '****************************************************************************************************
        'Purpose  		    :  To check for duplicate  Stocktake Period
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  24/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strSTPeriodIDs", strSTPeriodIDs)
            oParamSQL(1) = New SqlParameter("@strSTFrmDt", strSTFrmDt)
            oParamSQL(2) = New SqlParameter("@strSTToDt", strSTToDt)
            oParamSQL(3) = New SqlParameter("@strType", strType)
            oParamSQL(4) = New SqlParameter("@strRetVal", SqlDbType.VarChar, 10)
            oParamSQL(4).Direction = ParameterDirection.Output

            SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_OfflineProPeriod_CheckForDuplicate", oParamSQL)
            If oParamSQL(4).Value = "Y" Then  'Duplicate Records
                fnOfflineProPeriod_CheckForDuplicate = True
            Else                                  'Not Duplicate Records
                fnOfflineProPeriod_CheckForDuplicate = False
            End If

        Catch ex As Exception
            fnOfflineProPeriod_CheckForDuplicate = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOfflineProPeriod_InsertUpdateDelete( _
                                        ByVal strSTPeriodIDs As String, _
                                        ByVal strSTFrmDt As String, _
                                        ByVal strSTToDt As String, _
                                        ByVal strSTRemarks As String, _
                                        ByVal strLoginID As String, _
                                        ByVal strType As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Add, Edit, Delete particular Stocktake Period
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  24/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(5) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strSTPeriodIDs", strSTPeriodIDs)
            oParamSQL(1) = New SqlParameter("@strSTFrmDt", strSTFrmDt)
            oParamSQL(2) = New SqlParameter("@strSTToDt", strSTToDt)
            oParamSQL(3) = New SqlParameter("@strSTRemarks", strSTRemarks)
            oParamSQL(4) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(5) = New SqlParameter("@strType", strType)

            fnOfflineProPeriod_InsertUpdateDelete = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_OfflineProPeriod_InsertUpdateDelete", oParamSQL)
        Catch ex As Exception
            fnOfflineProPeriod_InsertUpdateDelete = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOfflineProPeriod_GetSTPeriodForEdit( _
                                       ByVal intSTPeriodID As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Stocktake Period Record
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  24/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intSTPeriodID", intSTPeriodID)

            fnOfflineProPeriod_GetSTPeriodForEdit = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_OfflineProPeriod_GetSTPeriodForEdit", oParamSQL)
        Catch ex As Exception
            fnOfflineProPeriod_GetSTPeriodForEdit = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOfflineSummary_UpdateRec( _
                                ByVal strAssetXML As String, _
                                ByVal intusrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To insert Stocktake History (download from PDA)
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  22/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetXML", strAssetXML)
            oParamSQL(1) = New SqlParameter("@intusrID", intusrID)

            fnOfflineSummary_UpdateRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_OfflineSummary_UpdateRec_", oParamSQL)
        Catch ex As Exception
            fnOfflineSummary_UpdateRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOfflinePro_GetSummaryStocktakeMainRec( _
                                   ByVal intSTPeriodID As Integer, _
                                   ByVal intDeptID As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Stocktake Details Record
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  12/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intSTPeriodID", intSTPeriodID)
            oParamSQL(1) = New SqlParameter("@intDeptID", intDeptID)

            fnOfflinePro_GetSummaryStocktakeMainRec = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_OfflinePro_GetSummaryStocktakeMainRec", oParamSQL)
        Catch ex As Exception
            fnOfflinePro_GetSummaryStocktakeMainRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOfflinePro_GetSummaryStocktakeAssetRecords( _
                                ByVal intSTPeriodID As Integer, _
                                ByVal intDeptID As Integer, _
                                ByVal strScanF As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Stocktake History Asset Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  22/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intSTPeriodID", intSTPeriodID)
            oParamSQL(1) = New SqlParameter("@intDeptID", intDeptID)
            oParamSQL(2) = New SqlParameter("@strScanF", strScanF)

            fnOfflinePro_GetSummaryStocktakeAssetRecords = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_OfflinePro_GetSummaryStocktakeAssetRecords", oParamSQL)
        Catch ex As Exception
            fnOfflinePro_GetSummaryStocktakeAssetRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOfflineProMissFound_UpdateRecord( _
                            ByVal strAssetXML As String, _
                            ByVal intusrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To insert Stocktake History (download from PDA)
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  22/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetXML", strAssetXML)
            oParamSQL(1) = New SqlParameter("@intusrID", intusrID)

            fnOfflineProMissFound_UpdateRecord = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_OfflineProMissFound_UpdateRecord", oParamSQL)
        Catch ex As Exception
            fnOfflineProMissFound_UpdateRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOfflineProMissFound_GetStocktakeAssetRecords() As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Missing and Found Asset Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  22/11/2007
        '****************************************************************************************************
        Try

            fnOfflineProMissFound_GetStocktakeAssetRecords = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_OfflineProMissFound_GetStocktakeAssetRecords")
        Catch ex As Exception
            fnOfflineProMissFound_GetStocktakeAssetRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOfflineProHistory_GetStocktakePeriod( _
                            ByVal strAdminF As String, _
                            ByVal intUsrID As Integer) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Stocktake History Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  22/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAdminF", strAdminF)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)

            fnOfflineProHistory_GetStocktakePeriod = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_OfflineProHistory_GetStocktakePeriod", oParamSQL)
        Catch ex As Exception
            fnOfflineProHistory_GetStocktakePeriod = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOfflineProPeriod_GetStocktakePeriod( _
                        ByVal StrSortName As String, _
                        ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Stocktake Exercise Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  11/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(1) = New SqlParameter("@strSortOrder", strSortOrder)

            fnOfflineProPeriod_GetStocktakePeriod = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_OfflineProPeriod_GetStocktakePeriod", oParamSQL)
        Catch ex As Exception
            fnOfflineProPeriod_GetStocktakePeriod = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOfflinePro_UpdateMovement( _
                        ByVal intStockTakeID As Integer, ByVal strAssetXML As String, _
                        ByVal strAssetMvXML As String, ByVal intusrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To insert Stocktake History (download from PDA)
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  14/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intStockTakeID", intStockTakeID)
            oParamSQL(1) = New SqlParameter("@strAssetXML", strAssetXML)
            oParamSQL(2) = New SqlParameter("@strAssetMvXML", strAssetMvXML)
            oParamSQL(3) = New SqlParameter("@intusrID", intusrID)

            fnOfflinePro_UpdateMovement = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_OfflinePro_UpdateMovement", oParamSQL)
        Catch ex As Exception
            fnOfflinePro_UpdateMovement = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOfflinePro_GetStocktakeAssetRecords( _
                                ByVal intStocktakeID As Integer, _
                                ByVal strScanF As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Stocktake History Asset Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  12/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intStocktakeID", intStocktakeID)
            oParamSQL(1) = New SqlParameter("@strScanF", strScanF)

            fnOfflinePro_GetStocktakeAssetRecords = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_OfflinePro_GetStocktakeAssetRecords", oParamSQL)
        Catch ex As Exception
            fnOfflinePro_GetStocktakeAssetRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOfflinePro_GeStocktakeDetail( _
                                ByVal intStocktakeID As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Stocktake Details Record
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  12/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intStocktakeID", intStocktakeID)

            fnOfflinePro_GeStocktakeDetail = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_OfflinePro_GeStocktakeDetail", oParamSQL)
        Catch ex As Exception
            fnOfflinePro_GeStocktakeDetail = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOfflinePro_GeStocktakeHistoryRec( _
        ByVal strLoginID As String, ByVal strStpckTPeriodID As String, _
        ByVal strAdminF As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Stocktake History Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  12/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(1) = New SqlParameter("@strStpckTPeriodID", strStpckTPeriodID)
            oParamSQL(2) = New SqlParameter("@strAdminF", strAdminF)

            fnOfflinePro_GeStocktakeHistoryRec = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_OfflinePro_GeStocktakeHistoryRec", oParamSQL)
        Catch ex As Exception
            fnOfflinePro_GeStocktakeHistoryRec = Nothing
            Throw ex
        End Try
    End Function


    Public Shared Function fnOfflinePro_UploadAsset2PDA( _
                    ByVal intAssetCatID As Integer, ByVal intAssetCatSubID As Integer, _
                    ByVal intDeptID As Integer, ByVal intLocID As Integer, ByVal strOwnerID As String, _
                    ByVal strMvFDt As String, ByVal strMvTDt As String, ByVal strFile4PDA As String, _
                    ByVal strLoginID As String, ByRef intStocktakeID As String, _
                    ByVal strAssetType As String, ByVal intLocSubID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To insert Stocktake Action Asset Record (upload to PDA)
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  11/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(11) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intAssetCatID", intAssetCatID)       '--"0"=All
            oParamSQL(1) = New SqlParameter("@intAssetCatSubID", intAssetCatSubID) '--"0"=All
            oParamSQL(2) = New SqlParameter("@intDeptID", intDeptID)            '--"0"=All
            oParamSQL(3) = New SqlParameter("@intLocID", intLocID)              '--"0"=All
            oParamSQL(4) = New SqlParameter("@strOwnerID", strOwnerID)          '--""=All
            oParamSQL(5) = New SqlParameter("@strMvFDt", strMvFDt)              '--""=All
            oParamSQL(6) = New SqlParameter("@strMvTDt", strMvTDt)              '--""=All
            oParamSQL(7) = New SqlParameter("@strFile4PDA", strFile4PDA)
            oParamSQL(8) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(9) = New SqlParameter("@intStocktakeID", SqlDbType.Int, 20)
            oParamSQL(9).Direction = ParameterDirection.Output
            oParamSQL(10) = New SqlParameter("@strAssetType", strAssetType)
            oParamSQL(11) = New SqlParameter("@intLocSubID", intLocSubID)

            fnOfflinePro_UploadAsset2PDA = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_OfflinePro_UploadAsset2PDA", oParamSQL)
            intStocktakeID = oParamSQL(9).Value

        Catch ex As Exception
            fnOfflinePro_UploadAsset2PDA = Nothing
            Throw ex
        End Try
    End Function


    Public Shared Function fnOfflinePro_StocktakeSearchRecordGetMovement( _
                        ByVal intAssetCatID As Integer, ByVal intAssetCatSubID As Integer, _
                        ByVal intDeptID As Integer, ByVal intLocID As Integer, ByVal strOwnerID As String, _
                        ByVal strMvFDt As String, ByVal strMvTDt As String, _
                        ByVal strAssetType As String, ByVal intLocSubID As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Stocktake Action Seacrh Asset Movement Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  14/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(8) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intAssetCatID", intAssetCatID)       '--"0"=All
            oParamSQL(1) = New SqlParameter("@intAssetCatSubID", intAssetCatSubID) '--"0"=All
            oParamSQL(2) = New SqlParameter("@intDeptID", intDeptID)            '--"0"=All
            oParamSQL(3) = New SqlParameter("@intLocID", intLocID)              '--"0"=All
            oParamSQL(4) = New SqlParameter("@strOwnerID", strOwnerID)          '--""=All
            oParamSQL(5) = New SqlParameter("@strMvFDt", strMvFDt)              '--""=All
            oParamSQL(6) = New SqlParameter("@strMvTDt", strMvTDt)              '--""=All
            oParamSQL(7) = New SqlParameter("@strAssetType", strAssetType)
            oParamSQL(8) = New SqlParameter("@intLocSubID", intLocSubID)

            fnOfflinePro_StocktakeSearchRecordGetMovement = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_OfflinePro_StocktakeSearchRecordGetMovement", oParamSQL)
        Catch ex As Exception
            fnOfflinePro_StocktakeSearchRecordGetMovement = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOfflinePro_StocktakeSearchRecordDR( _
                    ByVal intAssetCatID As Integer, ByVal intAssetCatSubID As Integer, _
                    ByVal intDeptID As Integer, ByVal intLocID As Integer, ByVal strOwnerID As String, _
                    ByVal strMvFDt As String, ByVal strMvTDt As String, _
                    ByVal StrSortName As String, ByVal strSortOrder As String, _
                    ByVal strAssetType As String, ByVal intLocSubID As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Stocktake Action Seacrh Asset Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  14/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(10) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intAssetCatID", intAssetCatID)       '--"0"=All
            oParamSQL(1) = New SqlParameter("@intAssetCatSubID", intAssetCatSubID) '--"0"=All
            oParamSQL(2) = New SqlParameter("@intDeptID", intDeptID)            '--"0"=All
            oParamSQL(3) = New SqlParameter("@intLocID", intLocID)              '--"0"=All
            oParamSQL(4) = New SqlParameter("@strOwnerID", strOwnerID)          '--""=All
            oParamSQL(5) = New SqlParameter("@strMvFDt", strMvFDt)              '--""=All
            oParamSQL(6) = New SqlParameter("@strMvTDt", strMvTDt)              '--""=All
            oParamSQL(7) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(8) = New SqlParameter("@strSortOrder", strSortOrder)
            oParamSQL(9) = New SqlParameter("@strAssetType", strAssetType)
            oParamSQL(10) = New SqlParameter("@intLocSubID", intLocSubID)

            fnOfflinePro_StocktakeSearchRecordDR = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_OfflinePro_StocktakeSearchRecord", oParamSQL)
        Catch ex As Exception
            fnOfflinePro_StocktakeSearchRecordDR = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOfflinePro_StocktakeSearchRecord( _
                ByVal intAssetCatID As Integer, ByVal intAssetCatSubID As Integer, _
                ByVal intDeptID As Integer, ByVal intLocID As Integer, ByVal strOwnerID As String, _
                ByVal strMvFDt As String, ByVal strMvTDt As String, _
                ByVal StrSortName As String, ByVal strSortOrder As String, _
                ByVal strAssetType As String, ByVal intLocSubID As Integer) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Stocktake Action Seacrh Asset Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  11/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(10) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intAssetCatID", intAssetCatID)       '--"0"=All
            oParamSQL(1) = New SqlParameter("@intAssetCatSubID", intAssetCatSubID) '--"0"=All
            oParamSQL(2) = New SqlParameter("@intDeptID", intDeptID)            '--"0"=All
            oParamSQL(3) = New SqlParameter("@intLocID", intLocID)              '--"0"=All
            oParamSQL(4) = New SqlParameter("@strOwnerID", strOwnerID)          '--""=All
            oParamSQL(5) = New SqlParameter("@strMvFDt", strMvFDt)              '--""=All
            oParamSQL(6) = New SqlParameter("@strMvTDt", strMvTDt)              '--""=All
            oParamSQL(7) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(8) = New SqlParameter("@strSortOrder", strSortOrder)
            oParamSQL(9) = New SqlParameter("@strAssetType", strAssetType)
            oParamSQL(10) = New SqlParameter("@intLocSubID", intLocSubID)

            fnOfflinePro_StocktakeSearchRecord = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_OfflinePro_StocktakeSearchRecord", oParamSQL)
        Catch ex As Exception
            fnOfflinePro_StocktakeSearchRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOfflinePro_CheckStocktakePeriod() As String
        '****************************************************************************************************
        'Purpose  		    :  To check whether is in Stocktake Period 
        'Returns		    :  string
        'Author			    :  See Siew
        'Date			    :  11/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strRetVal", SqlDbType.Char, 1)
            oParamSQL.Direction = ParameterDirection.Output

            SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_OfflinePro_CheckStocktakePeriod", oParamSQL)
            fnOfflinePro_CheckStocktakePeriod = oParamSQL.Value

        Catch ex As Exception
            fnOfflinePro_CheckStocktakePeriod = Nothing
            Throw ex
        End Try
    End Function

End Class
