#Region "Information Section"
' ****************************************************************************************************
' Description       : NFS Tranaction Class.
' Purpose           : To perform the business logic for NFS Tranaction (such as view, add, delete, edit)
' Author			: See Siew
' Date			    : 25.12.2008
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsNFSTrf

    Public Shared Function fnNFSTrfAct_GetCostCenter() As String
        '****************************************************************************************************
        'Purpose  		    :  To Get Cost Center.  
        'Returns		    :  String
        'Author			    :  See Siew
        'Date			    :  13/02/2009
        '****************************************************************************************************
        Try
            Dim objRdr As SqlDataReader
            Dim strSQL As String = "select fld_cronCostCenter from dbo.tbl_cronCostCtr where fld_cronCCActive = 'Y' and fld_DelStatus = 'N'"
            Dim strCostCenter As String = ""

            objRdr = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.Text, strSQL)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        If strCostCenter <> "" Then
                            strCostCenter = strCostCenter & ", "
                        End If
                        strCostCenter = strCostCenter & Trim(objRdr("fld_cronCostCenter"))
                    End While
                End If
            End If
            objRdr.Close()

            fnNFSTrfAct_GetCostCenter = strCostCenter
        Catch ex As Exception
            fnNFSTrfAct_GetCostCenter = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnNFSTrfRes_DeleteRecords( _
                                            ByVal strNFSXML As String, _
                                            ByVal intUsrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To delete transaction 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  9/1/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strNFSXML", strNFSXML)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)

            fnNFSTrfRes_DeleteRecords = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_NFSTrfRes_DeleteRecords", oParamSQL)
        Catch ex As Exception
            fnNFSTrfRes_DeleteRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnNFSTrfAct_GetAMSDetailsByNFSID(ByVal strNFSIDID As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get AMS Details By NFS ID.  
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  09/01/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strNFSIDID", strNFSIDID)

            fnNFSTrfAct_GetAMSDetailsByNFSID = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_NFSTrfAct_GetAMSDetailsByNFSID", oParamSQL)
        Catch ex As Exception
            fnNFSTrfAct_GetAMSDetailsByNFSID = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnNFSTrfRes_RestoreRecords( _
                                        ByVal strNFSXML As String, _
                                        ByVal intUsrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To restore transaction 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  3/1/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strNFSXML", strNFSXML)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)

            fnNFSTrfRes_RestoreRecords = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_NFSTrfRes_RestoreRecords", oParamSQL)
        Catch ex As Exception
            fnNFSTrfRes_RestoreRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnNFSTrfRes_GetRecords( _
                        ByVal strImpFrmDt As String, ByVal strImpToDt As String, _
                        ByVal strTrfType As String, ByVal strNFSID As String, _
                        ByVal strArcFrmDt As String, ByVal strArcToDt As String, _
                        ByVal StrSortName As String, ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Transaction for Restore
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  03/01/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(7) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strImpFrmDt", strImpFrmDt)
            oParamSQL(1) = New SqlParameter("@strImpToDt", strImpToDt)
            oParamSQL(2) = New SqlParameter("@strTrfType", strTrfType)
            oParamSQL(3) = New SqlParameter("@strNFSID", strNFSID)
            oParamSQL(4) = New SqlParameter("@strArcFrmDt", strArcFrmDt)
            oParamSQL(5) = New SqlParameter("@strArcToDt", strArcToDt)
            oParamSQL(6) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(7) = New SqlParameter("@strSortOrder", strSortOrder)
            fnNFSTrfRes_GetRecords = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_NFSTrfRes_GetRecords", oParamSQL)
        Catch ex As Exception
            fnNFSTrfRes_GetRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnNFSTrfArc_ArchiveRecords( _
                                    ByVal strNFSXML As String, _
                                    ByVal intUsrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To archive transaction 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  3/1/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strNFSXML", strNFSXML)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)

            fnNFSTrfArc_ArchiveRecords = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_NFSTrfArc_ArchiveRecords", oParamSQL)
        Catch ex As Exception
            fnNFSTrfArc_ArchiveRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnNFSTrfArc_GetRecords( _
                    ByVal strImpFrmDt As String, ByVal strImpToDt As String, _
                    ByVal strTrfType As String, ByVal strNFSID As String, _
                    ByVal StrSortName As String, ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Transaction for Archive
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  03/01/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(5) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strImpFrmDt", strImpFrmDt)
            oParamSQL(1) = New SqlParameter("@strImpToDt", strImpToDt)
            oParamSQL(2) = New SqlParameter("@strTrfType", strTrfType)
            oParamSQL(3) = New SqlParameter("@strNFSID", strNFSID)
            oParamSQL(4) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(5) = New SqlParameter("@strSortOrder", strSortOrder)
            fnNFSTrfArc_GetRecords = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_NFSTrfArc_GetRecords", oParamSQL)
        Catch ex As Exception
            fnNFSTrfArc_GetRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnNFSTrfAct_UpdateADJRecords( _
                                ByVal strNFSXML As String, _
                                ByVal intUsrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To update ADJ transaction 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  3/1/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strNFSXML", strNFSXML)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)

            fnNFSTrfAct_UpdateADJRecords = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_NFSTrfAct_UpdateADJRecords", oParamSQL)
        Catch ex As Exception
            fnNFSTrfAct_UpdateADJRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnNFSTrfAct_UpdateRCTRecords( _
                                                        ByVal strNFSXML As String, _
                                                        ByVal intUsrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To update RCT transaction 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  2/1/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strNFSXML", strNFSXML)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)

            fnNFSTrfAct_UpdateRCTRecords = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_NFSTrfAct_UpdateRCTRecords", oParamSQL)
        Catch ex As Exception
            fnNFSTrfAct_UpdateRCTRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnNFSTrfAct_UpdateTrfINRecords( _
                                                    ByVal strNFSXML As String, _
                                                    ByVal intUsrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To update TRF-IN transaction 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  2/1/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strNFSXML", strNFSXML)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)

            fnNFSTrfAct_UpdateTrfINRecords = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_NFSTrfAct_UpdateTrfINRecords", oParamSQL)
        Catch ex As Exception
            fnNFSTrfAct_UpdateTrfINRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnNFSTrfAct_InsertTrfINRecords( _
                                                ByVal strNFSXML As String, _
                                                ByVal intUsrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To insert TRF-IN transaction 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  1/1/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strNFSXML", strNFSXML)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)

            fnNFSTrfAct_InsertTrfINRecords = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_NFSTrfAct_InsertTrfINRecords", oParamSQL)
        Catch ex As Exception
            fnNFSTrfAct_InsertTrfINRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnNFSTrfAct_InsertADDRecords( _
                                            ByVal strNFSXML As String, _
                                            ByVal intUsrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To insert ADD transaction 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  31/12/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strNFSXML", strNFSXML)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)

            fnNFSTrfAct_InsertADDRecords = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_NFSTrfAct_InsertADDRecords", oParamSQL)
        Catch ex As Exception
            fnNFSTrfAct_InsertADDRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnNFSTrfAct_GetRecords( _
                    ByVal strImpFrmDt As String, ByVal strImpToDt As String, _
                    ByVal strTrfType As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Transaction Records
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  26/12/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strImpFrmDt", strImpFrmDt)
            oParamSQL(1) = New SqlParameter("@strImpToDt", strImpToDt)
            oParamSQL(2) = New SqlParameter("@strTrfType", strTrfType)
            fnNFSTrfAct_GetRecords = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_NFSTrfAct_GetRecords", oParamSQL)
        Catch ex As Exception
            fnNFSTrfAct_GetRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnNFSTrfHis_GetRecords( _
                ByVal strHisFrmDt As String, ByVal strHisToDt As String, _
                ByVal StrSortName As String, ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Transaction History
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  25/12/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strHisFrmDt", strHisFrmDt)
            oParamSQL(1) = New SqlParameter("@strHisToDt", strHisToDt)
            oParamSQL(2) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(3) = New SqlParameter("@strSortOrder", strSortOrder)
            fnNFSTrfHis_GetRecords = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_NFSTrfHis_GetRecords", oParamSQL)
        Catch ex As Exception
            fnNFSTrfHis_GetRecords = Nothing
            Throw ex
        End Try
    End Function
End Class
