#Region "Information Section"
' ****************************************************************************************************
' Description       : KIV Class.
' Purpose           : To perform the business logic for KIV (such as search, view, add, delete, edit)
' Author			: See Siew
' Date			    : 12.04.2005
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsKIV

    Public Shared Function fnKIVGetInfo4Ticker() As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get KIV Record for Ticker
        'Returns		    :  SlqDataReader
        'Author			    :  See Siew
        'Date			    :  22/04/2006
        '****************************************************************************************************
        Try
            
            fnKIVGetInfo4Ticker = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_KIV_GetInfo4Ticker")
        Catch ex As Exception
            fnKIVGetInfo4Ticker = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnKIVEditKIVrecord( _
                                ByVal intKIVID As Integer, _
                                ByVal intUsrID As Integer, _
                                ByVal strKIVdt As String, _
                                ByVal strKIVremark As String, _
                                ByVal strLoginID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Edit KIV Records
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  13/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intKIVID", intKIVID)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(2) = New SqlParameter("@strKIVdt", strKIVdt)
            oParamSQL(3) = New SqlParameter("@strKIVremark", strKIVremark)
            oParamSQL(4) = New SqlParameter("@strLoginID", strLoginID)

            fnKIVEditKIVrecord = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_KIV_EditKIVrecord", oParamSQL)
        Catch ex As Exception
            fnKIVEditKIVrecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnKIVGetRecordForEdit( _
                                               ByVal intKIVID As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get KIV Record for Edit KIV Screen
        'Returns		    :  SlqDataReader
        'Author			    :  See Siew
        'Date			    :  13/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intKIVID", intKIVID)

            fnKIVGetRecordForEdit = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_KIV_GetRecordForEdit", oParamSQL)
        Catch ex As Exception
            fnKIVGetRecordForEdit = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnKIVDeletePassKIV( _
                                ByVal strKIVIDs As String, _
                                ByVal strLoginID As String, _
                                ByVal intUsrID As Integer, _
                                ByVal strType As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Delete/Passing KIV Records
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  13/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strKIVIDs", strKIVIDs)
            oParamSQL(1) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(2) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(3) = New SqlParameter("@strType", strType)

            fnKIVDeletePassKIV = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_KIV_DeletePassKIV", oParamSQL)
        Catch ex As Exception
            fnKIVDeletePassKIV = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnKIVViewKIV( _
                                        ByVal strFrmDt As String, _
                                        ByVal strToDt As String, _
                                        ByVal strKIVPass As String, _
                                        ByVal StrSortName As String, _
                                        ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get KIV Records for View KIV screen
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  13/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strFrmDt", strFrmDt)
            oParamSQL(1) = New SqlParameter("@strToDt", strToDt)
            oParamSQL(2) = New SqlParameter("@strKIVPass", strKIVPass)
            oParamSQL(3) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(4) = New SqlParameter("@strSortOrder", strSortOrder)

            fnKIVViewKIV = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_KIV_ViewKIV", oParamSQL)
        Catch ex As Exception
            fnKIVViewKIV = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnKIVAddKIVRecord( _
                            ByVal strVolumeIDs As String, _
                            ByVal intKIVBy As Integer, _
                            ByVal strKIVDate As String, _
                            ByVal strKIVRemark As String, _
                            ByVal strLoginID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Add KIV Records
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  12/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strVolumeIDs", strVolumeIDs)
            oParamSQL(1) = New SqlParameter("@intKIVBy", intKIVBy)
            oParamSQL(2) = New SqlParameter("@strKIVDate", strKIVDate)
            oParamSQL(3) = New SqlParameter("@strKIVRemark", strKIVRemark)
            oParamSQL(4) = New SqlParameter("@strLoginID", strLoginID)

            fnKIVAddKIVRecord = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_KIV_AddKIVRecord", oParamSQL)
        Catch ex As Exception
            fnKIVAddKIVRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnKIVCheckDuplicateFileForKIV( _
                            ByVal strVolumeIDs As String, _
                            ByVal strKIVDate As String, _
                            ByVal strKIVIId As Integer, _
                            ByVal strType As String, _
                            ByRef strErrMsg As String, _
                            ByRef strDupKIV As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Check duplicate KIV record
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  12/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(5) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strVolumeIDs", strVolumeIDs)
            oParamSQL(1) = New SqlParameter("@strKIVDate", strKIVDate)
            oParamSQL(2) = New SqlParameter("@strKIVIId", strKIVIId)
            oParamSQL(3) = New SqlParameter("@strType", strType)
            oParamSQL(4) = New SqlParameter("@strErrMsg", SqlDbType.NVarChar, 2000)
            oParamSQL(4).Direction = ParameterDirection.Output
            oParamSQL(5) = New SqlParameter("@strDupKIV", SqlDbType.Char, 1)
            oParamSQL(5).Direction = ParameterDirection.Output

            fnKIVCheckDuplicateFileForKIV = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_KIV_CheckDuplicateFileForKIV", oParamSQL)
            strErrMsg = oParamSQL(4).Value
            strDupKIV = oParamSQL(5).Value
        Catch ex As Exception
            fnKIVCheckDuplicateFileForKIV = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnKIVGetFile2KIV( _
                                            ByVal strVolumeIDs As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get File Record for Add KIV Screen
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  12/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strVolumeIDs", strVolumeIDs)

            fnKIVGetFile2KIV = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_KIV_GetFile2KIV", oParamSQL)
        Catch ex As Exception
            fnKIVGetFile2KIV = Nothing
            Throw ex
        End Try
    End Function
End Class
