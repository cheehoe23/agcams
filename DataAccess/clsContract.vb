#Region "Information Section"
' ****************************************************************************************************
' Description       : Contract Class.
' Purpose           : To perform the business logic for Contract (such as view, add, delete, edit)
' Author			: See Siew
' Date			    : 05.11.2005
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsContract
    Public Shared Function fnContractRmd_UpdateActionNchecking( _
                ByVal intContractID As String, ByVal strLoginID As String, _
                ByVal strContractAct As String, ByRef strErrorF As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Checking Contract Reminder Action and Update  
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  29/07/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intContractID", intContractID)
            oParamSQL(1) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(2) = New SqlParameter("@strContractAct", strContractAct)
            oParamSQL(3) = New SqlParameter("@strErrorF", SqlDbType.Char, 1)
            oParamSQL(3).Direction = ParameterDirection.Output

            fnContractRmd_UpdateActionNchecking = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_ContractRmd_UpdateActionNchecking", oParamSQL)
            strErrorF = oParamSQL(3).Value
        Catch ex As Exception
            fnContractRmd_UpdateActionNchecking = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnContract_InsertRenewalRecord( _
                ByVal intContractMstID As Integer, ByVal strContractCost As String, _
                ByVal strContractFDt As String, ByVal strContractTDt As String, _
                ByVal intContractNoti As String, _
                ByVal strContractVendor As String, ByVal strContractDesc As String, _
                ByVal strOwnerName As String, ByVal strOwnerDept As String, _
                ByVal strSecurityDeposit As String, _
                ByVal strSecurityDepositFDt As String, ByVal strSecurityDepositExpDt As String, _
                ByVal intSecurityDepositNoti As String, ByVal strSecurityDepositRefNo As String, _
                ByVal strBankGuaAmt As String, ByVal strBankGuaFDt As String, _
                ByVal strBankGuaExpDt As String, ByVal intBankGuaNoti As String, _
                ByVal strBankGuarRefNo As String, ByVal strOffSignName As String, _
                ByVal strOffSignDesg As String, ByVal strRemarks As String, _
                ByVal strAssetIncluded As String, ByVal intUsrID As String, _
                ByRef strContractID As String, ByVal strOwnerDesg As String, ByVal intContExpNoti2 As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Insert Renewal Contract  
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  05/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(26) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intContractMstID", intContractMstID)
            oParamSQL(1) = New SqlParameter("@strContractCost", strContractCost)
            oParamSQL(2) = New SqlParameter("@strContractFDt", strContractFDt)
            oParamSQL(3) = New SqlParameter("@strContractTDt", strContractTDt)
            oParamSQL(4) = New SqlParameter("@intContractNoti", intContractNoti)
            oParamSQL(5) = New SqlParameter("@strContractVendor", strContractVendor)
            oParamSQL(6) = New SqlParameter("@strContractDesc", strContractDesc)
            oParamSQL(7) = New SqlParameter("@strOwnerName", strOwnerName)
            oParamSQL(8) = New SqlParameter("@strOwnerDept", strOwnerDept)
            oParamSQL(9) = New SqlParameter("@strSecurityDeposit", strSecurityDeposit)
            oParamSQL(10) = New SqlParameter("@strSecurityDepositFDt", strSecurityDepositFDt)
            oParamSQL(11) = New SqlParameter("@strSecurityDepositExpDt", strSecurityDepositExpDt)
            oParamSQL(12) = New SqlParameter("@intSecurityDepositNoti", intSecurityDepositNoti)
            oParamSQL(13) = New SqlParameter("@strSecurityDepositRefNo", strSecurityDepositRefNo)
            oParamSQL(14) = New SqlParameter("@strBankGuaAmt", strBankGuaAmt)
            oParamSQL(15) = New SqlParameter("@strBankGuaFDt", strBankGuaFDt)
            oParamSQL(16) = New SqlParameter("@strBankGuaExpDt", strBankGuaExpDt)
            oParamSQL(17) = New SqlParameter("@intBankGuaNoti", intBankGuaNoti)
            oParamSQL(18) = New SqlParameter("@strBankGuarRefNo", strBankGuarRefNo)
            oParamSQL(19) = New SqlParameter("@strOffSignName", strOffSignName)
            oParamSQL(20) = New SqlParameter("@strOffSignDesg", strOffSignDesg)
            oParamSQL(21) = New SqlParameter("@strRemarks", strRemarks)
            oParamSQL(22) = New SqlParameter("@strAssetIncluded", strAssetIncluded)
            oParamSQL(23) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(24) = New SqlParameter("@strContractID", SqlDbType.VarChar, 20)
            oParamSQL(24).Direction = ParameterDirection.Output
            oParamSQL(25) = New SqlParameter("@strOwnerDesg", strOwnerDesg)
            oParamSQL(26) = New SqlParameter("@intContExpNoti2", intContExpNoti2)
            fnContract_InsertRenewalRecord = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Contract_InsertRenewalRecord", oParamSQL)
            strContractID = oParamSQL(24).Value
        Catch ex As Exception
            fnContract_InsertRenewalRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnContract_GetContractHistory( _
                    ByVal intContractID As Integer, _
                    ByVal intContractMstID As Integer) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get contract history  
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  17/12/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intContractID", intContractID)
            oParamSQL(1) = New SqlParameter("@intContractMstID", intContractMstID)
            fnContract_GetContractHistory = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Contract_GetContractHistory", oParamSQL)
        Catch ex As Exception
            fnContract_GetContractHistory = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnContract_GetContractForRenewal(ByVal intContractMstId As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get contract Details For renewal Screen  
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  17/12/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intContractMstId", intContractMstId)
            fnContract_GetContractForRenewal = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Contract_GetContractForRenewal", oParamSQL)
        Catch ex As Exception
            fnContract_GetContractForRenewal = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnContract_UpdateRecord( _
                    ByVal intContractID As String, ByVal strContractTitle As String, ByVal strContractCost As String, _
                    ByVal strContractFDt As String, ByVal strContractTDt As String, _
                    ByVal intContractNoti As String, _
                    ByVal strContractVendor As String, ByVal strContractDesc As String, _
                    ByVal strOwnerName As String, ByVal strOwnerDept As String, _
                    ByVal strSecurityDeposit As String, _
                    ByVal strSecurityDepositFDt As String, ByVal strSecurityDepositExpDt As String, _
                    ByVal intSecurityDepositNoti As String, ByVal strSecurityDepositRefNo As String, _
                    ByVal strBankGuaAmt As String, ByVal strBankGuaFDt As String, _
                    ByVal strBankGuaExpDt As String, ByVal intBankGuaNoti As String, _
                    ByVal strBankGuarRefNo As String, ByVal strOffSignName As String, _
                    ByVal strOffSignDesg As String, ByVal strRemarks As String, _
                    ByVal intUsrID As String, ByVal strOwnerDesg As String, ByVal intContractNoti2 As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To update Contract  
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  07/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(25) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intContractID", intContractID)
            oParamSQL(1) = New SqlParameter("@strContractTitle", strContractTitle)
            oParamSQL(2) = New SqlParameter("@strContractCost", strContractCost)
            oParamSQL(3) = New SqlParameter("@strContractFDt", strContractFDt)
            oParamSQL(4) = New SqlParameter("@strContractTDt", strContractTDt)
            oParamSQL(5) = New SqlParameter("@intContractNoti", intContractNoti)
            oParamSQL(6) = New SqlParameter("@strContractVendor", strContractVendor)
            oParamSQL(7) = New SqlParameter("@strContractDesc", strContractDesc)
            oParamSQL(8) = New SqlParameter("@strOwnerName", strOwnerName)
            oParamSQL(9) = New SqlParameter("@strOwnerDept", strOwnerDept)
            oParamSQL(10) = New SqlParameter("@strSecurityDeposit", strSecurityDeposit)
            oParamSQL(11) = New SqlParameter("@strSecurityDepositFDt", strSecurityDepositFDt)
            oParamSQL(12) = New SqlParameter("@strSecurityDepositExpDt", strSecurityDepositExpDt)
            oParamSQL(13) = New SqlParameter("@intSecurityDepositNoti", intSecurityDepositNoti)
            oParamSQL(14) = New SqlParameter("@strSecurityDepositRefNo", strSecurityDepositRefNo)
            oParamSQL(15) = New SqlParameter("@strBankGuaAmt", strBankGuaAmt)
            oParamSQL(16) = New SqlParameter("@strBankGuaFDt", strBankGuaFDt)
            oParamSQL(17) = New SqlParameter("@strBankGuaExpDt", strBankGuaExpDt)
            oParamSQL(18) = New SqlParameter("@intBankGuaNoti", intBankGuaNoti)
            oParamSQL(19) = New SqlParameter("@strBankGuarRefNo", strBankGuarRefNo)
            oParamSQL(20) = New SqlParameter("@strOffSignName", strOffSignName)
            oParamSQL(21) = New SqlParameter("@strOffSignDesg", strOffSignDesg)
            oParamSQL(22) = New SqlParameter("@strRemarks", strRemarks)
            oParamSQL(23) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(24) = New SqlParameter("@strOwnerDesg", strOwnerDesg)
            oParamSQL(25) = New SqlParameter("@intContractNoti2", intContractNoti2)

            fnContract_UpdateRecord = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Contract_UpdateRecord", oParamSQL)

        Catch ex As Exception
            fnContract_UpdateRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnContract_DeleteAssetFromContract( _
                            ByVal intContractID As String, ByVal intAssetIDDel As String, _
                            ByVal strLoginID As String, ByRef strDeleteFailF As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To delete asset from Contract 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  06/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intContractID", intContractID)
            oParamSQL(1) = New SqlParameter("@intAssetIDDel", intAssetIDDel)
            oParamSQL(2) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(3) = New SqlParameter("@strDeleteFailF", SqlDbType.Char, 1)   '--Y=if asset is last asset to delete in contract, it not allow to delete
            oParamSQL(3).Direction = ParameterDirection.Output

            fnContract_DeleteAssetFromContract = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Contract_DeleteAssetFromContract", oParamSQL)
            strDeleteFailF = oParamSQL(3).Value
        Catch ex As Exception
            fnContract_DeleteAssetFromContract = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnContract_GetAssetRecords( _
                                                ByVal strContractID As String, _
                                                ByVal StrSortName As String, _
                                                ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Asset Record for contract
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  07/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strContractID", strContractID)    '' --Asset Ids format = 12^13^14^
            oParamSQL(1) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(2) = New SqlParameter("@strSortOrder", strSortOrder)

            fnContract_GetAssetRecords = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Contract_GetAssetRecords", oParamSQL)
        Catch ex As Exception
            fnContract_GetAssetRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnContract_GetContractForEdit(ByVal intContractId As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get contract Details For Edit Screen  
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  06/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intContractId", intContractId)
            fnContract_GetContractForEdit = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Contract_GetContractForEdit", oParamSQL)
        Catch ex As Exception
            fnContract_GetContractForEdit = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnContract_DeleteContract( _
                                                    ByVal strContractIDs As String, _
                                                    ByVal strLoginID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To delete Contract 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  06/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strContractIDs", strContractIDs)
            oParamSQL(1) = New SqlParameter("@strLoginID", strLoginID)

            fnContract_DeleteContract = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Contract_DeleteContract", oParamSQL)
        Catch ex As Exception
            fnContract_DeleteContract = Nothing
            Throw ex
        End Try
    End Function


    Public Shared Function fnContract_SearchRecord( _
        ByVal strContractID As String, ByVal strContractTitle As String, ByVal strContractCost As String, _
        ByVal strContractStartFDt As String, ByVal strContractStartTDt As String, _
        ByVal strContractEndFDt As String, ByVal strContractEndTDt As String, ByVal intContNoti As String, _
        ByVal strVendorName As String, ByVal strContOwner As String, ByVal strContOwnerDept As String, _
        ByVal strSecDps As String, ByVal strSecDpsSFrmDt As String, ByVal strSecDpsSToDt As String, _
        ByVal strSecDpsEFrmDt As String, ByVal strSecDpsEToDt As String, ByVal intSecDpsNoti As String, _
        ByVal strSecDpsRefNo As String, ByVal strBankGuaAmt As String, ByVal strBankGuaSFrmDt As String, _
        ByVal strBankGuaSToDt As String, ByVal strBankGuaEFrmDt As String, ByVal strBankGuaEToDt As String, _
        ByVal intBankGuaNoti As String, ByVal strBankGuaRefNo As String, ByVal strOffSignName As String, _
        ByVal strOffSignDesg As String, ByVal strAssetBarcode As String, _
        ByVal strArchiveF As String, ByVal strCallFrm As String, _
        ByVal StrSortName As String, ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Seacrh Contract Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  06/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(31) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strContractID", strContractID)                     '--""=All 
            oParamSQL(1) = New SqlParameter("@strContractTitle", strContractTitle)               '--""=All
            oParamSQL(2) = New SqlParameter("@strContractCost", strContractCost)                 '--""=All
            oParamSQL(3) = New SqlParameter("@strContractStartFDt", strContractStartFDt)         '--""=All
            oParamSQL(4) = New SqlParameter("@strContractStartTDt", strContractStartTDt)         '--""=All
            oParamSQL(5) = New SqlParameter("@strContractEndFDt", strContractEndFDt) '--""=All
            oParamSQL(6) = New SqlParameter("@strContractEndTDt", strContractEndTDt) '--""=All
            oParamSQL(7) = New SqlParameter("@intContNoti", intContNoti) '--"-1"=All
            oParamSQL(8) = New SqlParameter("@strVendorName", strVendorName)        '--""=All
            oParamSQL(9) = New SqlParameter("@strContOwner", strContOwner)        '--""=All
            oParamSQL(10) = New SqlParameter("@strContOwnerDept", strContOwnerDept)        '--""=All

            oParamSQL(11) = New SqlParameter("@strSecDps", strSecDps)        '--""=All
            oParamSQL(12) = New SqlParameter("@strSecDpsSFrmDt", strSecDpsSFrmDt)        '--""=All
            oParamSQL(13) = New SqlParameter("@strSecDpsSToDt", strSecDpsSToDt)        '--""=All
            oParamSQL(14) = New SqlParameter("@strSecDpsEFrmDt", strSecDpsEFrmDt)        '--""=All
            oParamSQL(15) = New SqlParameter("@strSecDpsEToDt", strSecDpsEToDt)        '--""=All
            oParamSQL(16) = New SqlParameter("@intSecDpsNoti", intSecDpsNoti)        '--""=All
            oParamSQL(17) = New SqlParameter("@strSecDpsRefNo", strSecDpsRefNo)        '--""=All

            oParamSQL(18) = New SqlParameter("@strBankGuaAmt", strBankGuaAmt)        '--""=All
            oParamSQL(19) = New SqlParameter("@strBankGuaSFrmDt", strBankGuaSFrmDt)        '--""=All
            oParamSQL(20) = New SqlParameter("@strBankGuaSToDt", strBankGuaSToDt)        '--""=All
            oParamSQL(21) = New SqlParameter("@strBankGuaEFrmDt", strBankGuaEFrmDt)        '--""=All
            oParamSQL(22) = New SqlParameter("@strBankGuaEToDt", strBankGuaEToDt)        '--""=All
            oParamSQL(23) = New SqlParameter("@intBankGuaNoti", intBankGuaNoti)        '--""=All
            oParamSQL(24) = New SqlParameter("@strBankGuaRefNo", strBankGuaRefNo)        '--""=All

            oParamSQL(25) = New SqlParameter("@strOffSignName", strOffSignName)        '--""=All
            oParamSQL(26) = New SqlParameter("@strOffSignDesg", strOffSignDesg)        '--""=All
            oParamSQL(27) = New SqlParameter("@strAssetBarcode", strAssetBarcode)    '--""=All
            oParamSQL(28) = New SqlParameter("@strArchiveF", strArchiveF)        '--""=All
            oParamSQL(29) = New SqlParameter("@strCallFrm", strCallFrm)        '--""=All
            oParamSQL(30) = New SqlParameter("@StrSortName", StrSortName)            '--""=All
            oParamSQL(31) = New SqlParameter("@strSortOrder", strSortOrder)          '--""=All

            fnContract_SearchRecord = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Contract_SearchRecord", oParamSQL)
        Catch ex As Exception
            fnContract_SearchRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnContract_AddAsset( _
                                                ByVal intContractID As Integer, _
                                                ByVal strAssetIDs As String, _
                                                ByVal intUsrID As Integer, _
                                                ByRef strAssetBarcode As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Insert Contract Asset
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  06/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intContractID", intContractID)
            oParamSQL(1) = New SqlParameter("@strAssetIDs", strAssetIDs)
            oParamSQL(2) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(3) = New SqlParameter("@strAssetBarcode", SqlDbType.VarChar, 4000)
            oParamSQL(3).Direction = ParameterDirection.Output
            fnContract_AddAsset = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_contract_AddAsset", oParamSQL)
            strAssetBarcode = oParamSQL(3).Value
        Catch ex As Exception
            fnContract_AddAsset = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fncontract_SearchAssetDuplicate( _
                                                   ByVal intContractID As Integer, _
                                                   ByVal strAssetIDs As String, _
                                                   ByRef strAssetDuplicate As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To check for duplicate Conract Asset
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  06/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intContractID", intContractID)
            oParamSQL(1) = New SqlParameter("@strAssetIDs", strAssetIDs)
            oParamSQL(2) = New SqlParameter("@strAssetDuplicate", SqlDbType.VarChar, 4000)
            oParamSQL(2).Direction = ParameterDirection.Output
            SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_contract_SearchAssetDuplicate", oParamSQL)
            strAssetDuplicate = oParamSQL(2).Value

        Catch ex As Exception
            fncontract_SearchAssetDuplicate = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnContract_SearchAsset( _
               ByVal strAssetType As String, ByVal intAssetCatID As Integer, ByVal intAssetCatSubID As Integer, _
                ByVal intDeptID As Integer, ByVal intLocID As Integer, ByVal strOwnerID As String, _
                ByVal intLocSubID As Integer) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Seacrh Contract Asset Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  06/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(6) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetType", strAssetType)         '--""=All, A=Fixed Asset, I=Inventory
            oParamSQL(1) = New SqlParameter("@intAssetCatID", intAssetCatID)       '--"0"=All
            oParamSQL(2) = New SqlParameter("@intAssetCatSubID", intAssetCatSubID) '--"0"=All
            oParamSQL(3) = New SqlParameter("@intDeptID", intDeptID)            '--"0"=All
            oParamSQL(4) = New SqlParameter("@intLocID", intLocID)              '--"0"=All
            oParamSQL(5) = New SqlParameter("@strOwnerID", strOwnerID)          '--""=All
            oParamSQL(6) = New SqlParameter("@intLocSubID", intLocSubID)              '--"0"=All

            fnContract_SearchAsset = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Contract_SearchAsset", oParamSQL)
        Catch ex As Exception
            fnContract_SearchAsset = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnContract_InsertRecord( _
                ByVal strContractTitle As String, ByVal strContractCost As String, _
                ByVal strContractFDt As String, ByVal strContractTDt As String, _
                ByVal intContractNoti As String, _
                ByVal strContractVendor As String, ByVal strContractDesc As String, _
                ByVal strOwnerName As String, ByVal strOwnerDept As String, _
                ByVal strSecurityDeposit As String, _
                ByVal strSecurityDepositFDt As String, ByVal strSecurityDepositExpDt As String, _
                ByVal intSecurityDepositNoti As String, ByVal strSecurityDepositRefNo As String, _
                ByVal strBankGuaAmt As String, ByVal strBankGuaFDt As String, _
                ByVal strBankGuaExpDt As String, ByVal intBankGuaNoti As String, _
                ByVal strBankGuarRefNo As String, ByVal strOffSignName As String, _
                ByVal strOffSignDesg As String, ByVal strRemarks As String, _
                ByVal strAssetIncluded As String, ByVal intUsrID As String, _
                ByRef strContractID As String, ByVal strOwnerDesg As String, ByVal intContractNoti2 As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Insert Contract  
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  05/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(26) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strContractTitle", strContractTitle)
            oParamSQL(1) = New SqlParameter("@strContractCost", strContractCost)
            oParamSQL(2) = New SqlParameter("@strContractFDt", strContractFDt)
            oParamSQL(3) = New SqlParameter("@strContractTDt", strContractTDt)
            oParamSQL(4) = New SqlParameter("@intContractNoti", intContractNoti)
            oParamSQL(5) = New SqlParameter("@strContractVendor", strContractVendor)
            oParamSQL(6) = New SqlParameter("@strContractDesc", strContractDesc)
            oParamSQL(7) = New SqlParameter("@strOwnerName", strOwnerName)
            oParamSQL(8) = New SqlParameter("@strOwnerDept", strOwnerDept)
            oParamSQL(9) = New SqlParameter("@strSecurityDeposit", strSecurityDeposit)
            oParamSQL(10) = New SqlParameter("@strSecurityDepositFDt", strSecurityDepositFDt)
            oParamSQL(11) = New SqlParameter("@strSecurityDepositExpDt", strSecurityDepositExpDt)
            oParamSQL(12) = New SqlParameter("@intSecurityDepositNoti", intSecurityDepositNoti)
            oParamSQL(13) = New SqlParameter("@strSecurityDepositRefNo", strSecurityDepositRefNo)
            oParamSQL(14) = New SqlParameter("@strBankGuaAmt", strBankGuaAmt)
            oParamSQL(15) = New SqlParameter("@strBankGuaFDt", strBankGuaFDt)
            oParamSQL(16) = New SqlParameter("@strBankGuaExpDt", strBankGuaExpDt)
            oParamSQL(17) = New SqlParameter("@intBankGuaNoti", intBankGuaNoti)
            oParamSQL(18) = New SqlParameter("@strBankGuarRefNo", strBankGuarRefNo)
            oParamSQL(19) = New SqlParameter("@strOffSignName", strOffSignName)
            oParamSQL(20) = New SqlParameter("@strOffSignDesg", strOffSignDesg)
            oParamSQL(21) = New SqlParameter("@strRemarks", strRemarks)
            oParamSQL(22) = New SqlParameter("@strAssetIncluded", strAssetIncluded)
            oParamSQL(23) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(24) = New SqlParameter("@strContractID", SqlDbType.VarChar, 20)
            oParamSQL(24).Direction = ParameterDirection.Output
            oParamSQL(25) = New SqlParameter("@strOwnerDesg", strOwnerDesg)
            oParamSQL(26) = New SqlParameter("@intContractNoti2", intContractNoti2)
            fnContract_InsertRecord = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Contract_InsertRecord", oParamSQL)
            strContractID = oParamSQL(24).Value
        Catch ex As Exception
            fnContract_InsertRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnContractFile_GetRec( _
                                            ByVal intUsrID As Integer, _
                                            ByVal intContractID As Integer, _
                                            ByVal strTempSaveF As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get contract File Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  05/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)               'Created By (For Add contract)
            oParamSQL(1) = New SqlParameter("@intContractID", intContractID)     'contract ID (For Modify contract)
            oParamSQL(2) = New SqlParameter("@strTempSaveF", strTempSaveF)       'Y=Temp Save, N=Not Temp Save

            fnContractFile_GetRec = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_ContractFile_GetRec", oParamSQL)
        Catch ex As Exception
            fnContractFile_GetRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnContractFile_DeleteRec( _
                                                ByVal intUsrID As Integer, _
                                                ByVal intContractFileID As Integer, _
                                                ByVal strType As String, _
                                                ByRef strFile2Delete As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To delete Contract File
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  05/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(1) = New SqlParameter("@intContractFileID", intContractFileID)
            oParamSQL(2) = New SqlParameter("@strType", strType)
            oParamSQL(3) = New SqlParameter("@strFile2Delete", SqlDbType.VarChar, 4000)
            oParamSQL(3).Direction = ParameterDirection.Output
            fnContractFile_DeleteRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_ContractFile_DeleteRec", oParamSQL)
            strFile2Delete = oParamSQL(3).Value
        Catch ex As Exception
            fnContractFile_DeleteRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnContractFile_InsertRec( _
                                            ByVal intUsrID As Integer, _
                                            ByVal intContractID As Integer, _
                                            ByVal strFileName As String, _
                                            ByVal strTempSaveF As String, _
                                            ByRef intFileID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Insert Contract File 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  05/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(1) = New SqlParameter("@intContractID", intContractID)
            oParamSQL(2) = New SqlParameter("@strFileName", strFileName)
            oParamSQL(3) = New SqlParameter("@strTempSaveF", strTempSaveF)
            oParamSQL(4) = New SqlParameter("@intContractFileID", SqlDbType.Int, 5)
            oParamSQL(4).Direction = ParameterDirection.Output
            fnContractFile_InsertRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_ContractFile_InsertRec", oParamSQL)
            intFileID = oParamSQL(4).Value
        Catch ex As Exception
            fnContractFile_InsertRec = Nothing
            Throw ex
        End Try
    End Function
End Class
