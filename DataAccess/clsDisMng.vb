#Region "Information Section"
' ****************************************************************************************************
' Description       : Dispose Management Class.
' Purpose           : To perform the business logic for Dispose Management -> Settings, ...
' Author			: CSS
' Date			    : 10.11.2008
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsDisMng
    Public Shared Function fnDisMng_GetEmailInfo4Redund5(ByVal intRedundID As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get disposal Email Info for redundancy Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  20/1/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intRedundID", intRedundID)

            fnDisMng_GetEmailInfo4Redund5 = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_GetEmailInfo4Redund5", oParamSQL)
        Catch ex As Exception
            fnDisMng_GetEmailInfo4Redund5 = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_RedundCertDispose( _
                ByVal intCertID As String, ByVal strCDispLID As String, ByVal strCDispDesg As String, _
                ByVal strRevName As String, ByVal strRevDesg As String, _
                ByVal strRevDt As String, ByVal strRevEmail As String, ByVal intUsrID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To dispose redundancy cert 
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  19/01/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(7) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intCertID", intCertID)
            oParamSQL(1) = New SqlParameter("@strCDispLID", strCDispLID)
            oParamSQL(2) = New SqlParameter("@strCDispDesg", strCDispDesg)
            oParamSQL(3) = New SqlParameter("@strRevName", strRevName)
            oParamSQL(4) = New SqlParameter("@strRevDesg", strRevDesg)
            oParamSQL(5) = New SqlParameter("@strRevDt", strRevDt)
            oParamSQL(6) = New SqlParameter("@strRevEmail", strRevEmail)
            oParamSQL(7) = New SqlParameter("@intUsrID", intUsrID)

            fnDisMng_RedundCertDispose = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_RedundCertDispose", oParamSQL)

        Catch ex As Exception
            fnDisMng_RedundCertDispose = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_RedundCheckAppOff( _
                                    ByVal intUsrID As String, ByVal strLoginID As String, _
                                    ByVal intCertID As String, ByVal strType As String) As Boolean
        '****************************************************************************************************
        'Purpose  		    :  To check for correct Approving Officer  
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  19/1/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(1) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(2) = New SqlParameter("@intCertID", intCertID)
            oParamSQL(3) = New SqlParameter("@strType", strType)
            oParamSQL(4) = New SqlParameter("@strAppOffF", SqlDbType.Char, 1)
            oParamSQL(4).Direction = ParameterDirection.Output
            SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_RedundCheckAppOff", oParamSQL)

            If oParamSQL(4).Value = "Y" Then  'Y=Correct Approving Officer
                fnDisMng_RedundCheckAppOff = True
            Else                              'Not Correct Approving Officer
                fnDisMng_RedundCheckAppOff = False
            End If

        Catch ex As Exception
            fnDisMng_RedundCheckAppOff = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_GetEmailInfo4Redund4(ByVal intRedundID As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get MOF Email Info for redundancy Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  19/1/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intRedundID", intRedundID)

            fnDisMng_GetEmailInfo4Redund4 = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_GetEmailInfo4Redund4", oParamSQL)
        Catch ex As Exception
            fnDisMng_GetEmailInfo4Redund4 = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_CondemnCheckAppOff( _
                                ByVal intUsrID As String, ByVal strLoginID As String, _
                                ByVal intCertID As String, ByVal strType As String) As Boolean
        '****************************************************************************************************
        'Purpose  		    :  To check for correct Approving Officer  
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  15/1/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(1) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(2) = New SqlParameter("@intCertID", intCertID)
            oParamSQL(3) = New SqlParameter("@strType", strType)
            oParamSQL(4) = New SqlParameter("@strAppOffF", SqlDbType.Char, 1)
            oParamSQL(4).Direction = ParameterDirection.Output
            SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_CondemnCheckAppOff", oParamSQL)

            If oParamSQL(4).Value = "Y" Then  'Y=Correct Approving Officer
                fnDisMng_CondemnCheckAppOff = True
            Else                              'Not Correct Approving Officer
                fnDisMng_CondemnCheckAppOff = False
            End If

        Catch ex As Exception
            fnDisMng_CondemnCheckAppOff = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_CondemnCertRejectEmail( _
                       ByVal intCertID As String, ByVal intUsrID As String, ByVal strRejReason As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To reject condemn cert 
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  13/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intCertID", intCertID)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(2) = New SqlParameter("@strRejReason", strRejReason)

            fnDisMng_CondemnCertRejectEmail = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_CondemnCertRejectEmail", oParamSQL)

        Catch ex As Exception
            fnDisMng_CondemnCertRejectEmail = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_CondemnCertApproveEmail( _
                    ByVal intCertID As String, ByVal intUsrID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To approve condemn cert 
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  13/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intCertID", intCertID)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)

            fnDisMng_CondemnCertApproveEmail = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_CondemnCertApproveEmail", oParamSQL)

        Catch ex As Exception
            fnDisMng_CondemnCertApproveEmail = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_CondemnCertDispose( _
                    ByVal intCertID As String, ByVal strCDispLID As String, _
                    ByVal strCDispDesg As String, ByVal intUsrID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To dispose condemn cert 
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  08/01/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intCertID", intCertID)
            oParamSQL(1) = New SqlParameter("@strCDispLID", strCDispLID)
            oParamSQL(2) = New SqlParameter("@strCDispDesg", strCDispDesg)
            oParamSQL(3) = New SqlParameter("@intUsrID", intUsrID)

            fnDisMng_CondemnCertDispose = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_CondemnCertDispose", oParamSQL)

        Catch ex As Exception
            fnDisMng_CondemnCertDispose = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMngSrhAsset_SearchRecord( _
                ByVal strAssetIDAMS As String, ByVal strAssetIDNFS As String, _
                ByVal strAssetType As String, ByVal intAssetCatID As Integer, ByVal intAssetCatSubID As Integer, _
                ByVal strAssetStatus As String, ByVal strPurchaseFDt As String, ByVal strPurchaseTDt As String, _
                ByVal strWarExpFDt As String, ByVal strWarExpTDt As String, _
                ByVal intDeptID As Integer, ByVal intLocID As Integer, ByVal strOwnerID As String, _
                ByVal strTempAsset As String, _
                ByVal strCallFrm As String, ByVal StrSortName As String, ByVal strSortOrder As String, _
                ByVal intLocSubID As String, ByVal strCtrlItemF As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Seacrh Asset Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  07/01/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(18) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDAMS", strAssetIDAMS)       '--""=All
            oParamSQL(1) = New SqlParameter("@strAssetIDNFS", strAssetIDNFS)       '--""=All
            oParamSQL(2) = New SqlParameter("@strAssetType", strAssetType)         '--""=All, A=Fixed Asset, I=Inventory
            oParamSQL(3) = New SqlParameter("@intAssetCatID", intAssetCatID)       '--"0"=All
            oParamSQL(4) = New SqlParameter("@intAssetCatSubID", intAssetCatSubID) '--"0"=All
            oParamSQL(5) = New SqlParameter("@strAssetStatus", strAssetStatus)   '--"0"=All, (A)ctive, (I)nactive, (R)edundancy, (C)ondenmation, (D)isposed
            oParamSQL(6) = New SqlParameter("@strPurchaseFDt", strPurchaseFDt)   '--""=All
            oParamSQL(7) = New SqlParameter("@strPurchaseTDt", strPurchaseTDt)   '--""=All
            oParamSQL(8) = New SqlParameter("@strWarExpFDt", strWarExpFDt)       '--""=All
            oParamSQL(9) = New SqlParameter("@strWarExpTDt", strWarExpTDt)       '--""=All
            oParamSQL(10) = New SqlParameter("@intDeptID", intDeptID)            '--"0"=All
            oParamSQL(11) = New SqlParameter("@intLocID", intLocID)              '--"0"=All
            oParamSQL(12) = New SqlParameter("@strOwnerID", strOwnerID)          '--""=All
            oParamSQL(13) = New SqlParameter("@strTempAsset", strTempAsset)      '--""=All, "Y"=Yes, "N"=No
            oParamSQL(14) = New SqlParameter("@strCallFrm", strCallFrm)          '--(C)ondemnation, (R)Redundancy 
            oParamSQL(15) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(16) = New SqlParameter("@strSortOrder", strSortOrder)
            oParamSQL(17) = New SqlParameter("@intLocSubID", intLocSubID)
            oParamSQL(18) = New SqlParameter("@strCtrlItemF", strCtrlItemF)

            fnDisMngSrhAsset_SearchRecord = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_DisMngSrhAsset_SearchRecord", oParamSQL)
        Catch ex As Exception
            fnDisMngSrhAsset_SearchRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_GetEmailInfo4Redund3(ByVal intRedundID As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get receiver Email Info for redundancy Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  13/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intRedundID", intRedundID)

            fnDisMng_GetEmailInfo4Redund3 = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_GetEmailInfo4Redund3", oParamSQL)
        Catch ex As Exception
            fnDisMng_GetEmailInfo4Redund3 = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_GetEmailInfo4Redund2(ByVal intRedundID As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get CPF-Finance Email Info for redundancy Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  13/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intRedundID", intRedundID)

            fnDisMng_GetEmailInfo4Redund2 = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_GetEmailInfo4Redund2", oParamSQL)
        Catch ex As Exception
            fnDisMng_GetEmailInfo4Redund2 = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_RedundCertReject( _
                        ByVal intCertID As String, ByVal strRejReason As String, _
                        ByVal intUsrID As String, ByVal strType As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To reject redundancy cert 
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  13/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intCertID", intCertID)
            oParamSQL(1) = New SqlParameter("@strRejReason", strRejReason)
            oParamSQL(2) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(3) = New SqlParameter("@strType", strType)

            fnDisMng_RedundCertReject = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_RedundCertReject", oParamSQL)

        Catch ex As Exception
            fnDisMng_RedundCertReject = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_RedundCertApprove( _
                    ByVal intCertID As String, ByVal intUsrID As String, ByVal strType As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To approve redundancy cert 
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  13/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intCertID", intCertID)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(2) = New SqlParameter("@strType", strType)

            fnDisMng_RedundCertApprove = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_RedundCertApprove", oParamSQL)

        Catch ex As Exception
            fnDisMng_RedundCertApprove = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_GetRedundRecords(ByVal intCertMstID As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get redundancy Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  13/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intCertMstID", intCertMstID)

            fnDisMng_GetRedundRecords = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_GetRedundRecords", oParamSQL)
        Catch ex As Exception
            fnDisMng_GetRedundRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_GetEmailInfo4Condemn2(ByVal intCondemnID As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get CPF-Finance Email Info for Condemn Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  13/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intCondemnID", intCondemnID)

            fnDisMng_GetEmailInfo4Condemn2 = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_GetEmailInfo4Condemn2", oParamSQL)
        Catch ex As Exception
            fnDisMng_GetEmailInfo4Condemn2 = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_CondemnCertReject( _
                    ByVal intCertID As String, ByVal intUsrID As String, ByVal strRejReason As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To reject condemn cert 
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  13/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intCertID", intCertID)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(2) = New SqlParameter("@strRejReason", strRejReason)

            fnDisMng_CondemnCertReject = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_CondemnCertReject", oParamSQL)

        Catch ex As Exception
            fnDisMng_CondemnCertReject = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_CondemnCertApprove( _
                ByVal intCertID As String, ByVal intUsrID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To approve condemn cert 
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  13/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intCertID", intCertID)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)

            fnDisMng_CondemnCertApprove = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_CondemnCertApprove", oParamSQL)

        Catch ex As Exception
            fnDisMng_CondemnCertApprove = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_GetCondemnRecordsDR(ByVal intCondemnMstID As String, ByVal strType As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Condemn Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  12/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intCondemnMstID", intCondemnMstID)
            oParamSQL(1) = New SqlParameter("@strType", strType)

            fnDisMng_GetCondemnRecordsDR = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_GetCondemnRecords", oParamSQL)
        Catch ex As Exception
            fnDisMng_GetCondemnRecordsDR = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_SearchCertRecord( _
                        ByVal strCertSNo As String, ByVal strCertCFDt As String, ByVal strCertCTDt As String, _
                        ByVal strCertStatus As String, ByVal strCertType As String, _
                        ByVal strAssetIDAMS As String, ByVal strAssetIDNFS As String, _
                        ByVal StrSortName As String, ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Search Cert Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  13/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(8) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strCertSNo", strCertSNo)
            oParamSQL(1) = New SqlParameter("@strCertCFDt", strCertCFDt)
            oParamSQL(2) = New SqlParameter("@strCertCTDt", strCertCTDt)
            oParamSQL(3) = New SqlParameter("@strCertStatus", strCertStatus)
            oParamSQL(4) = New SqlParameter("@strCertType", strCertType)
            oParamSQL(5) = New SqlParameter("@strAssetIDAMS", strAssetIDAMS)
            oParamSQL(6) = New SqlParameter("@strAssetIDNFS", strAssetIDNFS)
            oParamSQL(7) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(8) = New SqlParameter("@strSortOrder", strSortOrder)

            fnDisMng_SearchCertRecord = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_SearchCertRecord", oParamSQL)

        Catch ex As Exception
            fnDisMng_SearchCertRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_GetEmailInfo4Redund1(ByVal intRedundID As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Approving Officer Email Info for redundancy Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  12/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intRedundID", intRedundID)

            fnDisMng_GetEmailInfo4Redund1 = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_GetEmailInfo4Redund1", oParamSQL)
        Catch ex As Exception
            fnDisMng_GetEmailInfo4Redund1 = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_GetRedundRecords4Rpt(ByVal intRedundMstID As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Redundancy Record for report
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  12/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intRedundMstID", intRedundMstID)

            fnDisMng_GetRedundRecords4Rpt = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_GetRedundRecords4Rpt", oParamSQL)
        Catch ex As Exception
            fnDisMng_GetRedundRecords4Rpt = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_RedundAsset( _
                ByVal strAssetIDs As String, ByVal strDeptID As String, ByVal strRevParty As String, _
                ByVal strDisMethodID As String, ByVal strReasonRedun As String, _
                ByVal strAppOffLID As String, ByVal strAppOffDesg As String, _
                ByVal strReqLID As String, ByVal strReqDesg As String, ByVal strReqDt As String, _
                ByVal strMOFName As String, ByVal strMOFDesg As String, ByVal strMOFEmail As String, _
                ByVal strMore5hkF As String, ByVal intUsrID As String, _
                ByRef intRedundID As String, ByRef strSNum As String, ByRef strAssetBarcode As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To insert redundancy information
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  12/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(17) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDs", strAssetIDs)
            oParamSQL(1) = New SqlParameter("@strDeptID", strDeptID)
            oParamSQL(2) = New SqlParameter("@strRevParty", strRevParty)
            oParamSQL(3) = New SqlParameter("@strDisMethodID", strDisMethodID)
            oParamSQL(4) = New SqlParameter("@strReasonRedun", strReasonRedun)
            oParamSQL(5) = New SqlParameter("@strAppOffLID", strAppOffLID)
            oParamSQL(6) = New SqlParameter("@strAppOffDesg", strAppOffDesg)
            oParamSQL(7) = New SqlParameter("@strReqLID", strReqLID)
            oParamSQL(8) = New SqlParameter("@strReqDesg", strReqDesg)
            oParamSQL(9) = New SqlParameter("@strReqDt", strReqDt)
            oParamSQL(10) = New SqlParameter("@strMOFName", strMOFName)
            oParamSQL(11) = New SqlParameter("@strMOFDesg", strMOFDesg)
            oParamSQL(12) = New SqlParameter("@strMOFEmail", strMOFEmail)
            oParamSQL(13) = New SqlParameter("@strMore5hkF", strMore5hkF)
            oParamSQL(14) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(15) = New SqlParameter("@intRedundID", SqlDbType.Int, 20)
            oParamSQL(15).Direction = ParameterDirection.Output
            oParamSQL(16) = New SqlParameter("@strSNum", SqlDbType.VarChar, 10)
            oParamSQL(16).Direction = ParameterDirection.Output
            oParamSQL(17) = New SqlParameter("@strAssetBarcode", SqlDbType.VarChar, 8000)
            oParamSQL(17).Direction = ParameterDirection.Output

            fnDisMng_RedundAsset = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_RedundAsset", oParamSQL)

            intRedundID = oParamSQL(15).Value
            strSNum = oParamSQL(16).Value
            strAssetBarcode = oParamSQL(17).Value
        Catch ex As Exception
            fnDisMng_RedundAsset = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_GetCondemnRecords4Rpt(ByVal intCondemnMstID As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Condemn Record for report
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  12/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intCondemnMstID", intCondemnMstID)

            fnDisMng_GetCondemnRecords4Rpt = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_GetCondemnRecords4Rpt", oParamSQL)
        Catch ex As Exception
            fnDisMng_GetCondemnRecords4Rpt = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_GetCondemnRecords(ByVal intCondemnMstID As String, ByVal strType As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Condemn Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  12/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intCondemnMstID", intCondemnMstID)
            oParamSQL(1) = New SqlParameter("@strType", strType)

            fnDisMng_GetCondemnRecords = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_GetCondemnRecords", oParamSQL)
        Catch ex As Exception
            fnDisMng_GetCondemnRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_GetEmailInfo4Condemn1(ByVal intCondemnID As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Approving Officer Email Info for Condemn Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  11/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intCondemnID", intCondemnID)

            fnDisMng_GetEmailInfo4Condemn1 = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_GetEmailInfo4Condemn1", oParamSQL)
        Catch ex As Exception
            fnDisMng_GetEmailInfo4Condemn1 = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_CondemnAsset( _
            ByVal strAssetIDs As String, ByVal strDeptID As String, _
            ByVal strCondemnReason As String, ByVal strDisposalMethod As String, ByVal strTotProceed As String, ByVal strTotRemovalC As String, _
            ByVal strAppOffLID As String, ByVal strAppOffDesg As String, _
            ByVal strReqLID As String, ByVal strReqDesg As String, ByVal strReqDt As String, _
            ByVal intUsrID As String, ByRef intCondemnID As String, ByRef strSNum As String, _
            ByRef strAssetBarcode As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To insert condemn information
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  11/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(14) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDs", strAssetIDs)
            oParamSQL(1) = New SqlParameter("@strDeptID", strDeptID)
            oParamSQL(2) = New SqlParameter("@strCondemnReason", strCondemnReason)
            oParamSQL(3) = New SqlParameter("@strDisposalMethod", strDisposalMethod)
            oParamSQL(4) = New SqlParameter("@strTotProceed", strTotProceed)
            oParamSQL(5) = New SqlParameter("@strTotRemovalC", strTotRemovalC)
            oParamSQL(6) = New SqlParameter("@strAppOffLID", strAppOffLID)
            oParamSQL(7) = New SqlParameter("@strAppOffDesg", strAppOffDesg)
            oParamSQL(8) = New SqlParameter("@strReqLID", strReqLID)
            oParamSQL(9) = New SqlParameter("@strReqDesg", strReqDesg)
            oParamSQL(10) = New SqlParameter("@strReqDt", strReqDt)
            oParamSQL(11) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(12) = New SqlParameter("@intCondemnID", SqlDbType.Int, 20)
            oParamSQL(12).Direction = ParameterDirection.Output
            oParamSQL(13) = New SqlParameter("@strSNum", SqlDbType.VarChar, 10)
            oParamSQL(13).Direction = ParameterDirection.Output
            oParamSQL(14) = New SqlParameter("@strAssetBarcode", SqlDbType.VarChar, 8000)
            oParamSQL(14).Direction = ParameterDirection.Output

            fnDisMng_CondemnAsset = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_CondemnAsset", oParamSQL)

            intCondemnID = oParamSQL(12).Value
            strSNum = oParamSQL(13).Value
            strAssetBarcode = oParamSQL(14).Value
        Catch ex As Exception
            fnDisMng_CondemnAsset = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_GetAssetRecords( _
                            ByVal strAssetIDs As String, _
                            ByVal StrSortName As String, _
                            ByVal strSortOrder As String, _
                            ByRef strTotNBV As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Selected Asset Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  11/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDs", strAssetIDs)    '' --Asset Ids format = 12^13^14^
            oParamSQL(1) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(2) = New SqlParameter("@strSortOrder", strSortOrder)
            oParamSQL(3) = New SqlParameter("@strTotNBV", SqlDbType.VarChar, 200)
            oParamSQL(3).Direction = ParameterDirection.Output

            fnDisMng_GetAssetRecords = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_GetAssetRecords", oParamSQL)
            strTotNBV = oParamSQL(3).Value
        Catch ex As Exception
            fnDisMng_GetAssetRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDisMng_UpdateSettings( _
            ByVal strCDAppOffLID As String, ByVal strCDCPDFinanceEmail As String, ByVal strCDDept As String, _
            ByVal strCDAOActive As String, ByVal strCDAOSubj As String, ByVal strCDAOContent As String, _
            ByVal strCDCFActive As String, ByVal strCDCFSubj As String, ByVal strCDCFContent As String, _
            ByVal strRDAppOffLID As String, ByVal strRDCPDFinanceEmail As String, ByVal strRDDept As String, _
            ByVal strRDAOActive As String, ByVal strRDAOSubj As String, ByVal strRDAOContent As String, _
            ByVal strRDCFActive As String, ByVal strRDCFSubj As String, ByVal strRDCFContent As String, _
            ByVal strRDRevActive As String, ByVal strRDRevSubj As String, ByVal strRDRevContent As String, _
            ByVal strRDMOFActive As String, ByVal strRDMOFSubj As String, ByVal strRDMOFContent As String, _
            ByVal strRDDispActive As String, ByVal strRDDispSubj As String, ByVal strRDDispContent As String, _
            ByVal strLoginID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To update Settings information
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  10/11/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(27) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strCDAppOffLID", strCDAppOffLID)
            oParamSQL(1) = New SqlParameter("@strCDCPDFinanceEmail", strCDCPDFinanceEmail)
            oParamSQL(2) = New SqlParameter("@strCDDept", strCDDept)
            oParamSQL(3) = New SqlParameter("@strCDAOActive", strCDAOActive)
            oParamSQL(4) = New SqlParameter("@strCDAOSubj", strCDAOSubj)
            oParamSQL(5) = New SqlParameter("@strCDAOContent", strCDAOContent)
            oParamSQL(6) = New SqlParameter("@strCDCFActive", strCDCFActive)
            oParamSQL(7) = New SqlParameter("@strCDCFSubj", strCDCFSubj)
            oParamSQL(8) = New SqlParameter("@strCDCFContent", strCDCFContent)
            oParamSQL(9) = New SqlParameter("@strRDAppOffLID", strRDAppOffLID)
            oParamSQL(10) = New SqlParameter("@strRDCPDFinanceEmail", strRDCPDFinanceEmail)
            oParamSQL(11) = New SqlParameter("@strRDDept", strRDDept)
            oParamSQL(12) = New SqlParameter("@strRDAOActive", strRDAOActive)
            oParamSQL(13) = New SqlParameter("@strRDAOSubj", strRDAOSubj)
            oParamSQL(14) = New SqlParameter("@strRDAOContent", strRDAOContent)
            oParamSQL(15) = New SqlParameter("@strRDCFActive", strRDCFActive)
            oParamSQL(16) = New SqlParameter("@strRDCFSubj", strRDCFSubj)
            oParamSQL(17) = New SqlParameter("@strRDCFContent", strRDCFContent)
            oParamSQL(18) = New SqlParameter("@strRDRevActive", strRDRevActive)
            oParamSQL(19) = New SqlParameter("@strRDRevSubj", strRDRevSubj)
            oParamSQL(20) = New SqlParameter("@strRDRevContent", strRDRevContent)

            oParamSQL(21) = New SqlParameter("@strRDMOFActive", strRDMOFActive)
            oParamSQL(22) = New SqlParameter("@strRDMOFSubj", strRDMOFSubj)
            oParamSQL(23) = New SqlParameter("@strRDMOFContent", strRDMOFContent)
            oParamSQL(24) = New SqlParameter("@strRDDispActive", strRDDispActive)
            oParamSQL(25) = New SqlParameter("@strRDDispSubj", strRDDispSubj)
            oParamSQL(26) = New SqlParameter("@strRDDispContent", strRDDispContent)

            oParamSQL(27) = New SqlParameter("@strLoginID", strLoginID)

            fnDisMng_UpdateSettings = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_UpdateSettings", oParamSQL)
        Catch ex As Exception
            fnDisMng_UpdateSettings = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnCondemnFileInsertRec( _
                                            ByVal intUsrID As Integer, _
                                            ByVal intCDMstID As Integer, _
                                            ByVal strFileName As String, _
                                            ByVal strTempSaveF As String, _
                                            ByRef intFileID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Insert Asset File 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  25/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(1) = New SqlParameter("@intCDMstID", intCDMstID)
            oParamSQL(2) = New SqlParameter("@strFileName", strFileName)
            oParamSQL(3) = New SqlParameter("@strTempSaveF", strTempSaveF)
            oParamSQL(4) = New SqlParameter("@intCDFileID", SqlDbType.Int, 5)
            oParamSQL(4).Direction = ParameterDirection.Output
            fnCondemnFileInsertRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_CondemnFile_InsertRec", oParamSQL)
            intFileID = oParamSQL(4).Value
        Catch ex As Exception
            fnCondemnFileInsertRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnCondemnFileGetRec( _
                                        ByVal intUsrID As Integer, _
                                        ByVal intCDMstID As Integer, _
                                        ByVal strTempSaveF As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Asset File Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  25/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)         'Created By (For Add Asset)
            oParamSQL(1) = New SqlParameter("@intCDMstID", intCDMstID)     'Condemn ID (For Modify Asset)
            oParamSQL(2) = New SqlParameter("@strTempSaveF", strTempSaveF) 'Y=Temp Save, N=Not Temp Save

            fnCondemnFileGetRec = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_CondemnFile_GetRec", oParamSQL)
        Catch ex As Exception
            fnCondemnFileGetRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnCondemnFileDeleteRec( _
                                                ByVal intUsrID As Integer, _
                                                ByVal intCondemnFileID As Integer, _
                                                ByVal strType As String, _
                                                ByRef strFile2Delete As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To delete Asset File
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  25/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(1) = New SqlParameter("@intCDFileID", intCondemnFileID)
            oParamSQL(2) = New SqlParameter("@strType", strType)
            oParamSQL(3) = New SqlParameter("@strFile2Delete", SqlDbType.VarChar, 4000)
            oParamSQL(3).Direction = ParameterDirection.Output
            fnCondemnFileDeleteRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_CondemnFile_DeleteRec", oParamSQL)
            strFile2Delete = oParamSQL(3).Value
        Catch ex As Exception
            fnCondemnFileDeleteRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnRedundancyFileInsertRec( _
                                            ByVal intUsrID As Integer, _
                                            ByVal intRDMstID As Integer, _
                                            ByVal strFileName As String, _
                                            ByVal strTempSaveF As String, _
                                            ByRef intFileID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Insert Asset File 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  25/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(1) = New SqlParameter("@intRDMstID", intRDMstID)
            oParamSQL(2) = New SqlParameter("@strFileName", strFileName)
            oParamSQL(3) = New SqlParameter("@strTempSaveF", strTempSaveF)
            oParamSQL(4) = New SqlParameter("@intRDFileID", SqlDbType.Int, 5)
            oParamSQL(4).Direction = ParameterDirection.Output
            fnRedundancyFileInsertRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_RedundancyFile_InsertRec", oParamSQL)
            intFileID = oParamSQL(4).Value
        Catch ex As Exception
            fnRedundancyFileInsertRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnRedundancyFileGetRec( _
                                        ByVal intUsrID As Integer, _
                                        ByVal intRDMstID As Integer, _
                                        ByVal strTempSaveF As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Asset File Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  25/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)         'Created By (For Add Asset)
            oParamSQL(1) = New SqlParameter("@intRDMstID", intRDMstID)     'Condemn ID (For Modify Asset)
            oParamSQL(2) = New SqlParameter("@strTempSaveF", strTempSaveF) 'Y=Temp Save, N=Not Temp Save

            fnRedundancyFileGetRec = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_RedundancyFile_GetRec", oParamSQL)
        Catch ex As Exception
            fnRedundancyFileGetRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnRedundancyFileDeleteRec( _
                                                ByVal intUsrID As Integer, _
                                                ByVal intRDFileID As Integer, _
                                                ByVal strType As String, _
                                                ByRef strFile2Delete As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To delete Asset File
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  25/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(1) = New SqlParameter("@intRDFileID", intRDFileID)
            oParamSQL(2) = New SqlParameter("@strType", strType)
            oParamSQL(3) = New SqlParameter("@strFile2Delete", SqlDbType.VarChar, 4000)
            oParamSQL(3).Direction = ParameterDirection.Output
            fnRedundancyFileDeleteRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_RedundancyFile_DeleteRec", oParamSQL)
            strFile2Delete = oParamSQL(3).Value
        Catch ex As Exception
            fnRedundancyFileDeleteRec = Nothing
            Throw ex
        End Try
    End Function


    Public Shared Function fnDisMng_GetCondemnRequesterEmail(ByVal intCondemnID As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get receiver Email Info for Condemnation Record
        'Returns		    :  Dataset
        'Author			    :  YK
        'Date			    :  06/10/2016
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intCondemnMstID", intCondemnID)

            fnDisMng_GetCondemnRequesterEmail = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_GetCondemnRequesterEmail", oParamSQL)
        Catch ex As Exception
            fnDisMng_GetCondemnRequesterEmail = Nothing
            Throw ex
        End Try
    End Function


    Public Shared Function fnDisMng_GetRedundRequesterEmail(ByVal intRedundID As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get receiver Email Info for redundancy Record
        'Returns		    :  Dataset
        'Author			    :  YK
        'Date			    :  06/10/2016
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intRedundMstID", intRedundID)

            fnDisMng_GetRedundRequesterEmail = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_DisMng_GetRedundRequesterEmail", oParamSQL)
        Catch ex As Exception
            fnDisMng_GetRedundRequesterEmail = Nothing
            Throw ex
        End Try
    End Function
End Class
