#Region "Information Section"
' ****************************************************************************************************
' Description       : Category Class.
' Purpose           : To perform the business logic for Category (such as view, add, delete, edit)
' Author			: See Siew
' Date			    : 23.10.2005
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsCategory
    Public Shared Function fnCategorySub_CheckDuplicateTempSub( _
                                            ByVal strCatSubName As String, _
                                            ByVal strLoginID As String) As Boolean
        '****************************************************************************************************
        'Purpose  		    :  To check for duplicate temporary subcategory
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  06/03/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strCatSubName", strCatSubName)
            oParamSQL(1) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(2) = New SqlParameter("@strRetVal", SqlDbType.Char, 1)
            oParamSQL(2).Direction = ParameterDirection.Output
            SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_CategorySub_CheckDuplicateTempSub", oParamSQL)
            If oParamSQL(2).Value = "Y" Then  'Duplicate Records
                fnCategorySub_CheckDuplicateTempSub = True
            Else                              'Not Duplicate Records
                fnCategorySub_CheckDuplicateTempSub = False
            End If

        Catch ex As Exception
            fnCategorySub_CheckDuplicateTempSub = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnCategorySub_AddTempSubCategory( _
            ByVal strSCatCode As String, ByVal strSCatName As String, _
            ByVal strAddInfo1 As String, ByVal strAddInfoM1 As String, _
            ByVal strAddInfo2 As String, ByVal strAddInfoM2 As String, _
            ByVal strAddInfo3 As String, ByVal strAddInfoM3 As String, _
            ByVal strAddInfo4 As String, ByVal strAddInfoM4 As String, _
            ByVal strAddInfo5 As String, ByVal strAddInfoM5 As String, _
            ByVal intUsefulLife As String, ByVal strFloatLevel As String, _
            ByVal intExpNotiB4 As String, ByVal strLoginID As String, _
            ByVal strACTagF As String, ByVal strUploadFileName As String, ByRef strNewSCatID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To add new sub Category in database for Modify Screen
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  23/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(18) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strSCatCode", strSCatCode)
            oParamSQL(1) = New SqlParameter("@strSCatName", strSCatName)
            oParamSQL(2) = New SqlParameter("@strAddInfo1", strAddInfo1)
            oParamSQL(3) = New SqlParameter("@strAddInfoM1", strAddInfoM1)
            oParamSQL(4) = New SqlParameter("@strAddInfo2", strAddInfo2)
            oParamSQL(5) = New SqlParameter("@strAddInfoM2", strAddInfoM2)
            oParamSQL(6) = New SqlParameter("@strAddInfo3", strAddInfo3)
            oParamSQL(7) = New SqlParameter("@strAddInfoM3", strAddInfoM3)
            oParamSQL(8) = New SqlParameter("@strAddInfo4", strAddInfo4)
            oParamSQL(9) = New SqlParameter("@strAddInfoM4", strAddInfoM4)
            oParamSQL(10) = New SqlParameter("@strAddInfo5", strAddInfo5)
            oParamSQL(11) = New SqlParameter("@strAddInfoM5", strAddInfoM5)
            oParamSQL(12) = New SqlParameter("@intUsefulLife", intUsefulLife)
            oParamSQL(13) = New SqlParameter("@strFloatLevel", strFloatLevel)
            oParamSQL(14) = New SqlParameter("@intExpNotiB4", intExpNotiB4)
            oParamSQL(15) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(16) = New SqlParameter("@strACTagF", strACTagF)
            oParamSQL(17) = New SqlParameter("@strUploadFileName", strUploadFileName)
            oParamSQL(18) = New SqlParameter("@strNewSCatID", SqlDbType.Int)
            oParamSQL(18).Direction = ParameterDirection.Output

            fnCategorySub_AddTempSubCategory = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_CategorySub_AddTempSubCategory", oParamSQL)

            strNewSCatID = oParamSQL(18).Value
        Catch ex As Exception
            fnCategorySub_AddTempSubCategory = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnCategorySub_DeleteParticularTempRec( _
                                           ByVal intCatSubID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To delete particular temp sub categoty
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  06/03/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intCatSubID", intCatSubID)

            fnCategorySub_DeleteParticularTempRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_CategorySub_DeleteParticularTempRec", oParamSQL)
        Catch ex As Exception
            fnCategorySub_DeleteParticularTempRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnCategorySub_GetTempRec( _
                                            ByVal strLoginID As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get temporary subcategory.
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  06/03/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strLoginID", strLoginID)

            fnCategorySub_GetTempRec = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_CategorySub_GetTempRec", oParamSQL)
        Catch ex As Exception
            fnCategorySub_GetTempRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnCategorySub_DeleteTempRec( _
                ByVal strLoginID As String, ByRef strFile2Delete As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Delete Temporary Sub Category
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  06/03/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(1) = New SqlParameter("@strFile2Delete", SqlDbType.VarChar, 8000)
            oParamSQL(1).Direction = ParameterDirection.Output

            fnCategorySub_DeleteTempRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_CategorySub_DeleteTempRec", oParamSQL)

            strFile2Delete = oParamSQL(1).Value
        Catch ex As Exception
            fnCategorySub_DeleteTempRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnCategorySub_DeleteImages( _
                ByVal intSubCatID As String, ByVal strLoginID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Delete Sub Category Images
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  06/03/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intSubCatID", intSubCatID)
            oParamSQL(1) = New SqlParameter("@strLoginID", strLoginID)

            fnCategorySub_DeleteImages = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_CategorySub_DeleteImages", oParamSQL)
        Catch ex As Exception
            fnCategorySub_DeleteImages = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnCategoryGetCategory() As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Category record in Data Reader
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  24/10/2007
        '****************************************************************************************************
        Try
            fnCategoryGetCategory = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Category_GetCategory")
        Catch ex As Exception
            fnCategoryGetCategory = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnCategoryEditCategory( _
                ByVal intCatID As Integer, ByVal intSubCatID As String, _
                ByVal strCatCode As String, ByVal strCatName As String, _
                ByVal strSCatCode As String, ByVal strSCatName As String, _
                ByVal strAddInfo1 As String, ByVal strAddInfoM1 As String, _
                ByVal strAddInfo2 As String, ByVal strAddInfoM2 As String, _
                ByVal strAddInfo3 As String, ByVal strAddInfoM3 As String, _
                ByVal strAddInfo4 As String, ByVal strAddInfoM4 As String, _
                ByVal strAddInfo5 As String, ByVal strAddInfoM5 As String, _
                ByVal intUsefulLife As String, ByVal strFloatLevel As String, _
                ByVal intExpNoti As String, ByVal strLoginID As String, ByVal strType As String, _
                ByVal strACTagF As String, ByVal strUploadFileName As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To edit category/sub Category in database for Modify Screen
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  24/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(22) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intCatID", intCatID)
            oParamSQL(1) = New SqlParameter("@intSubCatID", intSubCatID)
            oParamSQL(2) = New SqlParameter("@strCatCode", strCatCode)
            oParamSQL(3) = New SqlParameter("@strCatName", strCatName)
            oParamSQL(4) = New SqlParameter("@strSCatCode", strSCatCode)
            oParamSQL(5) = New SqlParameter("@strSCatName", strSCatName)
            oParamSQL(6) = New SqlParameter("@strAddInfo1", strAddInfo1)
            oParamSQL(7) = New SqlParameter("@strAddInfoM1", strAddInfoM1)
            oParamSQL(8) = New SqlParameter("@strAddInfo2", strAddInfo2)
            oParamSQL(9) = New SqlParameter("@strAddInfoM2", strAddInfoM2)
            oParamSQL(10) = New SqlParameter("@strAddInfo3", strAddInfo3)
            oParamSQL(11) = New SqlParameter("@strAddInfoM3", strAddInfoM3)
            oParamSQL(12) = New SqlParameter("@strAddInfo4", strAddInfo4)
            oParamSQL(13) = New SqlParameter("@strAddInfoM4", strAddInfoM4)
            oParamSQL(14) = New SqlParameter("@strAddInfo5", strAddInfo5)
            oParamSQL(15) = New SqlParameter("@strAddInfoM5", strAddInfoM5)
            oParamSQL(16) = New SqlParameter("@intUsefulLife", intUsefulLife)
            oParamSQL(17) = New SqlParameter("@strFloatLevel", strFloatLevel)
            oParamSQL(18) = New SqlParameter("@intExpNoti", intExpNoti)
            oParamSQL(19) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(20) = New SqlParameter("@strType", strType)
            oParamSQL(21) = New SqlParameter("@strACTagF", strACTagF)
            oParamSQL(22) = New SqlParameter("@strUploadFileName", strUploadFileName)


            fnCategoryEditCategory = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Category_EditCategory", oParamSQL)
        Catch ex As Exception
            fnCategoryEditCategory = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnCategoryGetRecordsForMainCategory( _
                                            ByVal strCatID As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To get record for ategory
        'Returns		    :  sqldatareader
        'Author			    :  See Siew
        'Date			    :  24/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strCatID", strCatID)

            fnCategoryGetRecordsForMainCategory = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Category_GetRecordsForMainCategory", oParamSQL)
        Catch ex As Exception
            fnCategoryGetRecordsForMainCategory = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnCategoryDeleteCategory( _
                                        ByVal strCatIds As String, _
                                        ByVal strSubCatIDs As String, _
                                        ByVal strLoginID As String, _
                                        ByVal strType As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To delete status/sub status 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  03/04/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strCatIds", strCatIds)
            oParamSQL(1) = New SqlParameter("@strSubCatIDs", strSubCatIDs)
            oParamSQL(2) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(3) = New SqlParameter("@strType", strType)

            fnCategoryDeleteCategory = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Category_DeleteCategory", oParamSQL)
        Catch ex As Exception
            fnCategoryDeleteCategory = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnCategoryGetAllRec( _
                                        ByVal intCatID As Integer, _
                                        ByVal intSubCatID As Integer, _
                                        ByVal StrSortName As String, _
                                        ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Category/Sub Category record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  24/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intCatID", intCatID)
            oParamSQL(1) = New SqlParameter("@intSubCatID", intSubCatID)
            oParamSQL(2) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(3) = New SqlParameter("@strSortOrder", strSortOrder)

            fnCategoryGetAllRec = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Category_GetAllRec", oParamSQL)
        Catch ex As Exception
            fnCategoryGetAllRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnCategoryGetAllRecDR( _
                                        ByVal intCatID As Integer, _
                                        ByVal intSubCatID As Integer, _
                                        ByVal StrSortName As String, _
                                        ByVal strSortOrder As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Category/Sub Category record in Data Reader
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  24/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intCatID", intCatID)
            oParamSQL(1) = New SqlParameter("@intSubCatID", intSubCatID)
            oParamSQL(2) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(3) = New SqlParameter("@strSortOrder", strSortOrder)

            fnCategoryGetAllRecDR = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Category_GetAllRec", oParamSQL)
        Catch ex As Exception
            fnCategoryGetAllRecDR = Nothing
            Throw ex
        End Try
    End Function


    Public Shared Function fnCategoryAddSubCategoryForModify( _
            ByVal intCatID As Integer, ByVal intSCatID As String, ByVal strDeleteF As String, _
            ByVal strSCatCode As String, ByVal strSCatName As String, _
            ByVal strAddInfo1 As String, ByVal strAddInfoM1 As String, _
            ByVal strAddInfo2 As String, ByVal strAddInfoM2 As String, _
            ByVal strAddInfo3 As String, ByVal strAddInfoM3 As String, _
            ByVal strAddInfo4 As String, ByVal strAddInfoM4 As String, _
            ByVal strAddInfo5 As String, ByVal strAddInfoM5 As String, _
            ByVal intUsefulLife As String, ByVal strFloatLevel As String, _
            ByVal intExpNotiB4 As String, ByVal strLoginID As String, _
            ByVal strACTagF As String, ByVal strUploadFileName As String, ByRef strNewSCatID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To add new sub Category in database for Modify Screen
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  23/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(21) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intCatID", intCatID)
            oParamSQL(1) = New SqlParameter("@intSCatID", intSCatID)
            oParamSQL(2) = New SqlParameter("@strDeleteF", strDeleteF)
            oParamSQL(3) = New SqlParameter("@strSCatCode", strSCatCode)
            oParamSQL(4) = New SqlParameter("@strSCatName", strSCatName)
            oParamSQL(5) = New SqlParameter("@strAddInfo1", strAddInfo1)
            oParamSQL(6) = New SqlParameter("@strAddInfoM1", strAddInfoM1)
            oParamSQL(7) = New SqlParameter("@strAddInfo2", strAddInfo2)
            oParamSQL(8) = New SqlParameter("@strAddInfoM2", strAddInfoM2)
            oParamSQL(9) = New SqlParameter("@strAddInfo3", strAddInfo3)
            oParamSQL(10) = New SqlParameter("@strAddInfoM3", strAddInfoM3)
            oParamSQL(11) = New SqlParameter("@strAddInfo4", strAddInfo4)
            oParamSQL(12) = New SqlParameter("@strAddInfoM4", strAddInfoM4)
            oParamSQL(13) = New SqlParameter("@strAddInfo5", strAddInfo5)
            oParamSQL(14) = New SqlParameter("@strAddInfoM5", strAddInfoM5)
            oParamSQL(15) = New SqlParameter("@intUsefulLife", intUsefulLife)
            oParamSQL(16) = New SqlParameter("@strFloatLevel", strFloatLevel)
            oParamSQL(17) = New SqlParameter("@intExpNotiB4", intExpNotiB4)
            oParamSQL(18) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(19) = New SqlParameter("@strACTagF", strACTagF)
            oParamSQL(20) = New SqlParameter("@strUploadFileName", strUploadFileName)
            oParamSQL(21) = New SqlParameter("@strNewSCatID", SqlDbType.Int)
            oParamSQL(21).Direction = ParameterDirection.Output

            fnCategoryAddSubCategoryForModify = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Category_AddSubCategoryForModify", oParamSQL)

            strNewSCatID = oParamSQL(21).Value
        Catch ex As Exception
            fnCategoryAddSubCategoryForModify = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnCategoryAddNewCategory( _
                                    ByVal strCatCode As String, _
                                    ByVal strCatName As String, _
                                    ByVal strSubCatXML As String, _
                                    ByVal strLoginID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To add new Category and sub Category in database
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  23/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strCatCode", strCatCode)
            oParamSQL(1) = New SqlParameter("@strCatName", strCatName)
            oParamSQL(2) = New SqlParameter("@strSubCatXML", strSubCatXML)
            oParamSQL(3) = New SqlParameter("@strLoginID", strLoginID)

            fnCategoryAddNewCategory = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Category_AddNewCategory", oParamSQL)
        Catch ex As Exception
            fnCategoryAddNewCategory = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnCategoryCheckDuplicate( _
                                        ByVal intCatID As Integer, _
                                        ByVal intSubCatID As Integer, _
                                        ByVal strCatCode As String, _
                                        ByVal strCatName As String, _
                                        ByVal strCatType As String, _
                                        ByVal strType As String) As Boolean
        '****************************************************************************************************
        'Purpose  		    :  To check for duplicate category/subcategory
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  23/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(6) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intCatID", intCatID)
            oParamSQL(1) = New SqlParameter("@intSubCatID", intSubCatID)
            oParamSQL(2) = New SqlParameter("@strCatCode", strCatCode)
            oParamSQL(3) = New SqlParameter("@strCatName", strCatName)
            oParamSQL(4) = New SqlParameter("@strCatType", strCatType)
            oParamSQL(5) = New SqlParameter("@strType", strType)
            oParamSQL(6) = New SqlParameter("@strRetVal", SqlDbType.VarChar, 1)
            oParamSQL(6).Direction = ParameterDirection.Output
            SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Category_CheckDuplicate", oParamSQL)
            If oParamSQL(6).Value = "Y" Then  'Duplicate Records
                fnCategoryCheckDuplicate = True
            Else                              'Not Duplicate Records
                fnCategoryCheckDuplicate = False
            End If

        Catch ex As Exception
            fnCategoryCheckDuplicate = Nothing
            Throw ex
        End Try
    End Function
End Class
