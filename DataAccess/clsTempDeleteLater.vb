#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsTempDeleteLater
    Public Shared Function fnTemporarySP_DeleteLaterDR( _
                                                ByVal strGetRecordType As String, _
                                                ByVal strAssetID As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  Temporary --> Delete Later
        'Returns		    :  DataSet
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strGetRecordType", strGetRecordType)
            oParamSQL(1) = New SqlParameter("@strAssetID", strAssetID)
            fnTemporarySP_DeleteLaterDR = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_TemporarySP_DeleteLater", oParamSQL)
        Catch ex As Exception
            fnTemporarySP_DeleteLaterDR = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnTemporarySP_DeleteLater( _
                                                ByVal strGetRecordType As String, _
                                                ByVal strAssetID As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  Temporary --> Delete Later
        'Returns		    :  DataSet
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strGetRecordType", strGetRecordType)
            oParamSQL(1) = New SqlParameter("@strAssetID", strAssetID)
            fnTemporarySP_DeleteLater = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_TemporarySP_DeleteLater", oParamSQL)
        Catch ex As Exception
            fnTemporarySP_DeleteLater = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnTemporarySP_DeleteLaterCt( _
                                            ByVal strGetRecordType As String, _
                                            ByVal strAssetID As String, _
                                            ByVal strContractID As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  Temporary --> Delete Later
        'Returns		    :  DataSet
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strGetRecordType", strGetRecordType)
            oParamSQL(1) = New SqlParameter("@strAssetID", strAssetID)
            oParamSQL(2) = New SqlParameter("@strContractID", strContractID)
            fnTemporarySP_DeleteLaterCt = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_TemporarySP_DeleteLaterCt", oParamSQL)
        Catch ex As Exception
            fnTemporarySP_DeleteLaterCt = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnTemporarySP_DeleteLaterCtDR( _
                                        ByVal strGetRecordType As String, _
                                        ByVal strAssetID As String, _
                                        ByVal strContractID As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  Temporary --> Delete Later
        'Returns		    :  DataSet
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strGetRecordType", strGetRecordType)
            oParamSQL(1) = New SqlParameter("@strAssetID", strAssetID)
            oParamSQL(2) = New SqlParameter("@strContractID", strContractID)
            fnTemporarySP_DeleteLaterCtDR = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_TemporarySP_DeleteLaterCt", oParamSQL)
        Catch ex As Exception
            fnTemporarySP_DeleteLaterCtDR = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnTemporarySP_DeleteLaterMv( _
                                        ByVal strGetRecordType As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  Temporary --> Delete Later
        'Returns		    :  DataSet
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strGetRecordType", strGetRecordType)
            fnTemporarySP_DeleteLaterMv = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_TemporarySP_DeleteLaterMv", oParamSQL)
        Catch ex As Exception
            fnTemporarySP_DeleteLaterMv = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnTemporarySP_DeleteLaterCat( _
                                            ByVal strGetRecordType As String, _
                                            ByVal strCtglID As String, _
                                            ByVal strCtglSubID As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  Temporary --> Delete Later
        'Returns		    :  DataSet
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strGetRecordType", strGetRecordType)
            oParamSQL(1) = New SqlParameter("@strCtglID", strCtglID)
            oParamSQL(2) = New SqlParameter("@strCtglSubID", strCtglSubID)
            fnTemporarySP_DeleteLaterCat = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_TemporarySP_DeleteLaterCat", oParamSQL)
        Catch ex As Exception
            fnTemporarySP_DeleteLaterCat = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnTemporarySP_DeleteLaterCatDR( _
                                            ByVal strGetRecordType As String, _
                                            ByVal strCtglID As String, _
                                            ByVal strCtglSubID As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  Temporary --> Delete Later
        'Returns		    :  DataSet
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strGetRecordType", strGetRecordType)
            oParamSQL(1) = New SqlParameter("@strCtglID", strCtglID)
            oParamSQL(2) = New SqlParameter("@strCtglSubID", strCtglSubID)
            fnTemporarySP_DeleteLaterCatDR = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_TemporarySP_DeleteLaterCat", oParamSQL)
        Catch ex As Exception
            fnTemporarySP_DeleteLaterCatDR = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnTemporarySP_DeleteLaterAT( _
                                                ByVal strGetRecordType As String, _
                                                ByVal strAssetID As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  Temporary --> Delete Later
        'Returns		    :  DataSet
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strGetRecordType", strGetRecordType)
            oParamSQL(1) = New SqlParameter("@strAssetID", strAssetID)
            fnTemporarySP_DeleteLaterAT = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_TemporarySP_DeleteLaterAT", oParamSQL)
        Catch ex As Exception
            fnTemporarySP_DeleteLaterAT = Nothing
            Throw ex
        End Try
    End Function

End Class
