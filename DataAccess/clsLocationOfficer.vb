#Region "Information Section"
' ****************************************************************************************************
' Description       : Location Class.
' Purpose           : To perform the business logic for Location (such as view, add, delete, edit)
' Author			: Win
' Date			    : 16.10.2016
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsLocationOfficer
    Public Shared Function fnGetByfld_LocationOfficerName( _
                                            ByVal fld_LocationOfficerName As String) As DataSet
       
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@fld_LocationOfficerName", fld_LocationOfficerName)

            fnGetByfld_LocationOfficerName = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_tbl_LocationOfficer_GetByfld_LocationOfficerName", oParamSQL)

        Catch ex As Exception
            fnGetByfld_LocationOfficerName = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnGetDataByfld_LocationOfficerIDAndName( _
                                            ByVal fld_LocationOfficerID As String, _
                                            ByVal fld_LocationOfficerName As String, _
                                            ByVal StrSortName As String, _
                                            ByVal strSortOrder As String) As DataSet

        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@fld_LocationOfficerID", fld_LocationOfficerID)
            oParamSQL(1) = New SqlParameter("@fld_LocationOfficerName", fld_LocationOfficerName)
            oParamSQL(2) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(3) = New SqlParameter("@strSortOrder", strSortOrder)

            fnGetDataByfld_LocationOfficerIDAndName = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_tbl_LocationOfficer_GetDataByfld_LocationOfficerIDAndName", oParamSQL)

        Catch ex As Exception
            fnGetDataByfld_LocationOfficerIDAndName = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnGetDataByfld_LocationOfficerID( _
                                            ByVal fld_LocationOfficerID As String) As DataSet
     
        Try
            Dim oParamSQL(0) As SqlParameter
            oParamSQL(0) = New SqlParameter("@fld_LocationOfficerID", fld_LocationOfficerID)

            fnGetDataByfld_LocationOfficerID = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_tbl_LocationOfficer_GetDataByfld_LocationOfficerID", oParamSQL)

        Catch ex As Exception
            fnGetDataByfld_LocationOfficerID = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLocationOfficer_AddNewLocationOfficer( _
                    ByVal fld_LocationOfficerName As String, _
                    ByVal strLoginID As String) As Integer
       
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@fld_LocationOfficerName", fld_LocationOfficerName)
            oParamSQL(1) = New SqlParameter("@strLoginID", strLoginID)

            fnLocationOfficer_AddNewLocationOfficer = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_tbl_LocationOfficer_Insert", oParamSQL)
        Catch ex As Exception
            fnLocationOfficer_AddNewLocationOfficer = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLocationOfficer_Update( _
                                        ByVal LocationOfficerID As String, _
                                        ByVal LocationOfficerName As String, _
                                        ByVal strLoginID As String) As Integer
        
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@fld_LocationOfficerID", LocationOfficerID)
            oParamSQL(1) = New SqlParameter("@fld_LocationOfficerName", LocationOfficerName)
            oParamSQL(2) = New SqlParameter("@strLoginID", strLoginID)
            fnLocationOfficer_Update = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_tbl_LocationOfficer_Update", oParamSQL)
        Catch ex As Exception
            fnLocationOfficer_Update = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLocationOfficer_Delete( _
                                        ByVal LocationOfficerID As String, _
                                        ByVal strLoginID As String) As Integer

        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@fld_LocationOfficerID", LocationOfficerID)
            oParamSQL(1) = New SqlParameter("@strLoginID", strLoginID)
            fnLocationOfficer_Delete = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_tbl_LocationOfficer_Delete", oParamSQL)
        Catch ex As Exception
            fnLocationOfficer_Delete = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnRFIDLabel_GetLocationOfficer( _
                                        ByVal strLocationOfficerID As String) As SqlDataReader
      
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@fld_LocationOfficerID", strLocationOfficerID)

            fnRFIDLabel_GetLocationOfficer = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_RFIDLabel_GetLocationOfficer", oParamSQL)
        Catch ex As Exception
            fnRFIDLabel_GetLocationOfficer = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnGetAllLocationOfficer( _
                                        ) As DataSet
        Try

            fnGetAllLocationOfficer = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_LocationOfficerGetALL", Nothing)

        Catch ex As Exception
            fnGetAllLocationOfficer = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnGetLocationOfficer( _
                                        ) As SqlDataReader
        Try

            fnGetLocationOfficer = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_LocationOfficerGetALL", Nothing)

        Catch ex As Exception
            fnGetLocationOfficer = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnGetDataByfld_fld_LocationOfficerBarcodeID( _
                                            ByVal fld_LocationOfficerBarcodeID As String) As DataSet

        Try
            Dim oParamSQL(0) As SqlParameter
            oParamSQL(0) = New SqlParameter("@fld_LocationOfficerBarcodeID", fld_LocationOfficerBarcodeID)

            fnGetDataByfld_fld_LocationOfficerBarcodeID = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_tbl_LocationOfficer_GetDataByfld_fld_LocationOfficerBarcodeID", oParamSQL)

        Catch ex As Exception
            fnGetDataByfld_fld_LocationOfficerBarcodeID = Nothing
            Throw ex
        End Try
    End Function

End Class
