Public Class clsFlash
    Public Shared Function fnGetRFID() As DataSet
        Try
            'Dim oParamSQL As SqlParameter
            ' Dim id As Integer = Integer.Parse(BodyPartID)
            ' oParamSQL = New SqlParameter("@BodyPartID", id)
            Dim SqlStr As String



            SqlStr = "select fld_RFIDSTID, fld_CtrlItemBarcode, fld_OwnerBarcode, fld_OwnerName, fld_CtrlItemSubCat, fld_CtrlItemDesc, fld_CtrlItemFileName, fld_OwnerFileName, fld_ShowRowF, convert(varchar, fld_RFIDMvDt, 106) + ' ' + convert(varchar, fld_RFIDMvDt, 108) as fld_RFIDMvDt " & _
                    "from tbl_RFIDStockTrackGUI " & _
                    "where(Convert(varchar, fld_RFIDMvDt, 106) = Convert(varchar, getdate(), 106)) " & _
                    "order by fld_RFIDMvDt DESC "


            fnGetRFID = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.Text, SqlStr)
        Catch ex As Exception
            fnGetRFID = Nothing
            Throw ex
        End Try
    End Function

End Class
