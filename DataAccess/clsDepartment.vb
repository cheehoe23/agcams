#Region "Information Section"
' ****************************************************************************************************
' Description       : Department Class.
' Purpose           : To perform the business logic for Department (such as view, add, delete, edit)
' Author			: See Siew
' Date			    : 25.01.2007
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsDepartment
    Public Shared Function fnDeptGetDeptForEdit(ByVal intDeptId As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Deprtment Details For Edit Screen  
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  20/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intDeptId", intDeptId)
            fnDeptGetDeptForEdit = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Dept_GetDeptForEdit", oParamSQL)
        Catch ex As Exception
            fnDeptGetDeptForEdit = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDeptDuplicateDept( _
                                                   ByVal intDeptId As Integer, _
                                                   ByVal strDeptCode As String, _
                                                   ByVal strType As String) As Boolean
        '****************************************************************************************************
        'Purpose  		    :  To check for duplicate Department 
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  20/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intDeptID", intDeptId)
            oParamSQL(1) = New SqlParameter("@strDeptCode", strDeptCode)
            oParamSQL(2) = New SqlParameter("@strType", strType)
            oParamSQL(3) = New SqlParameter("@strRetVal", SqlDbType.VarChar, 10)
            oParamSQL(3).Direction = ParameterDirection.Output
            SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Dept_DuplicateDept", oParamSQL)
            If oParamSQL(3).Value = "Y" Then  'Duplicate Records
                fnDeptDuplicateDept = True
            Else                                  'Not Duplicate Records
                fnDeptDuplicateDept = False
            End If

        Catch ex As Exception
            fnDeptDuplicateDept = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDeptInsertUpdateDelete( _
                                        ByVal strDeptIds As String, _
                                        ByVal strDeptCode As String, _
                                        ByVal strDeptName As String, _
                                        ByVal strHOD As String, _
                                        ByVal strLoginID As String, _
                                        ByVal strType As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Add, Edit, Delete particular Department
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  20/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(5) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intDeptIDs", strDeptIds)
            oParamSQL(1) = New SqlParameter("@strDeptCode", strDeptCode)
            oParamSQL(2) = New SqlParameter("@strDeptName", strDeptName)
            oParamSQL(3) = New SqlParameter("@strHOD", strHOD)
            oParamSQL(4) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(5) = New SqlParameter("@strType", strType)
            fnDeptInsertUpdateDelete = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Dept_InsertUpdateDelete", oParamSQL)
        Catch ex As Exception
            fnDeptInsertUpdateDelete = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDeptGetAllRec( _
                                        ByVal StrSortName As String, _
                                        ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get All Department Records to populate in View Department screen 
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  25/02/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(1) = New SqlParameter("@strSortOrder", strSortOrder)
            fnDeptGetAllRec = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Dept_GetAllRec", oParamSQL)
        Catch ex As Exception
            fnDeptGetAllRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDeptGetAllRecDDL( _
                            ByVal intUsrID As Integer, _
                            ByVal strAdminF As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get All Department Records to populate in DropDownList  
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  20/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(1) = New SqlParameter("@strAdminF", strAdminF)

            fnDeptGetAllRecDDL = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Dept_GetAllRecInList", oParamSQL)

        Catch ex As Exception
            fnDeptGetAllRecDDL = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDeptGetAllRecDDL(ByVal strDeptType As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get All Department Records to populate in DropDownList  
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  25/01/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strDeptType", strDeptType)
            oParamSQL(1) = New SqlParameter("@StrSortName", "fld_DepartmentCode")
            oParamSQL(2) = New SqlParameter("@strSortOrder", "ASC")
            fnDeptGetAllRecDDL = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Dept_GetAllRec", oParamSQL)
        Catch ex As Exception
            fnDeptGetAllRecDDL = Nothing
            Throw ex
        End Try
    End Function

End Class
