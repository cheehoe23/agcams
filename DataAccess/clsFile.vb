#Region "Information Section"
' ****************************************************************************************************
' Description       : File Class.
' Purpose           : To perform the business logic for File (such as search, view, add, delete, edit)
' Author			: See Siew
' Date			    : 22.01.2005
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsFile

    Public Shared Function fnExp2Excel_AllRecord( _
                                    ByVal strFromDate As String, _
                                    ByVal strToDate As String, _
                                    ByVal strFileNum As String, _
                                    ByVal strFileRef As String, _
                                    ByVal strFileTitleKW As String, _
                                    ByVal strBarcode As String, _
                                    ByVal strAndOr1 As String, _
                                    ByVal strAndOr2 As String, _
                                    ByVal strAndOr3 As String, _
                                    ByVal strCallFrm As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get File Record for Export to Excel
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  17/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(9) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strFromDate", strFromDate)
            oParamSQL(1) = New SqlParameter("@strToDate", strToDate)
            oParamSQL(2) = New SqlParameter("@strFileNum", strFileNum)
            oParamSQL(3) = New SqlParameter("@strFileRef", strFileRef)
            oParamSQL(4) = New SqlParameter("@strFileTitleKW", strFileTitleKW)
            oParamSQL(5) = New SqlParameter("@strBarcode", strBarcode)
            oParamSQL(6) = New SqlParameter("@strAndOr1", strAndOr1)
            oParamSQL(7) = New SqlParameter("@strAndOr2", strAndOr2)
            oParamSQL(8) = New SqlParameter("@strAndOr3", strAndOr3)
            oParamSQL(9) = New SqlParameter("@strCallFrm", strCallFrm)

            fnExp2Excel_AllRecord = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Exp2Excel_AllRecord", oParamSQL)
        Catch ex As Exception
            fnExp2Excel_AllRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnExp2Excel_SelectedRecord( _
                                        ByVal strVolIds As String, _
                                        ByVal strCallFrm As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get File Record for Export to Excel
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  17/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strVolIds", strVolIds)
            oParamSQL(1) = New SqlParameter("@strCallFrm", strCallFrm)

            fnExp2Excel_SelectedRecord = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Exp2Excel_SelectedRecord", oParamSQL)
        Catch ex As Exception
            fnExp2Excel_SelectedRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnFileDeleteRecords( _
                                                ByVal FileID As String, _
                                                ByVal FileDetailID As String, _
                                                ByVal intUsrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Delete Record
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  16/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@FileID", FileID)
            oParamSQL(1) = New SqlParameter("@FileDetailID", FileDetailID)
            oParamSQL(2) = New SqlParameter("@intUsrID", intUsrID)

            fnFileDeleteRecords = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_File_DeleteRecords", oParamSQL)
        Catch ex As Exception
            fnFileDeleteRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnFileDetail_GetByfld_FileDetail_FileRefNo( _
                                            ByVal fld_FileDetail_FileRefNo As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Restore Record
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  16/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@fld_FileDetail_FileRefNo", fld_FileDetail_FileRefNo)

            fnFileDetail_GetByfld_FileDetail_FileRefNo = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_FileDetail_GetByfld_FileDetail_FileRefNo", oParamSQL)

        Catch ex As Exception
            fnFileDetail_GetByfld_FileDetail_FileRefNo = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnFileRestoreRecords( _
                                            ByVal strVolIDs As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Restore Record
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  16/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strVolIDs", strVolIDs)

            fnFileRestoreRecords = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_File_RestoreRecords", oParamSQL)
        Catch ex As Exception
            fnFileRestoreRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnFileArchiveRecords( _
                                            ByVal strVolIDs As String, _
                                            ByVal intUsrID As Integer, _
                                            ByVal strLoginID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Archive Record
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  16/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strVolIDs", strVolIDs)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(2) = New SqlParameter("@strLoginID", strLoginID)

            fnFileArchiveRecords = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_File_ArchiveRecords", oParamSQL)
        Catch ex As Exception
            fnFileArchiveRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnFileMvGetRecords( _
                                            ByVal strFileDetailID As Integer _
                                            ) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get File Record for Modification Screen
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  08/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(0) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strFileDetailID", strFileDetailID)

            fnFileMvGetRecords = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_FileMv_GetRecords", oParamSQL)
        Catch ex As Exception
            fnFileMvGetRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnFileAddVolume( _
                                            ByVal intFileID As Integer, _
                                            ByVal strFileKeyword As String, _
                                            ByVal strFileRemarks As String, _
                                            ByVal strFileSecStatus As String, _
                                            ByVal strLoginID As String, _
                                            ByVal intUsrID As Integer, _
                                            ByRef intVolID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Add Volume
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  08/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(6) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intFileID", intFileID)
            oParamSQL(1) = New SqlParameter("@strFileKeyword", strFileKeyword)
            oParamSQL(2) = New SqlParameter("@strFileRemarks", strFileRemarks)
            oParamSQL(3) = New SqlParameter("@strFileSecStatus", strFileSecStatus)
            oParamSQL(4) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(5) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(6) = New SqlParameter("@intVolID", SqlDbType.Int, 4)
            oParamSQL(6).Direction = ParameterDirection.Output

            fnFileAddVolume = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_File_AddVolume", oParamSQL)
            intVolID = oParamSQL(6).Value
        Catch ex As Exception
            fnFileAddVolume = Nothing
            Throw ex
        End Try
    End Function


    'Public Shared Function fnFileModifyFile( _
    '                                        ByVal intFileID As Integer, _
    '                                        ByVal intVolumeID As Integer, _
    '                                        ByVal strFileTitle As String, _
    '                                        ByVal strFileRef As String, _
    '                                        ByVal strFileKeyword As String, _
    '                                        ByVal strFileRemarks As String, _
    '                                        ByVal strFileSecStatus As String, _
    '                                        ByVal strFileStatusID As Integer, _
    '                                        ByVal strFileReviewDt As String, _
    '                                        ByVal strChgReviewDt As String, _
    '                                        ByVal strFileCloseDt As String, _
    '                                        ByVal strChgCloseDt As String, _
    '                                        ByVal strAutoChgLocF As String, _
    '                                        ByVal strAutoChgLocID As Integer, _
    '                                        ByVal strLoginID As String, _
    '                                        ByVal intUsrID As Integer, _
    '                                        ByVal strType As String) As Integer
    '    '****************************************************************************************************
    '    'Purpose  		    :  To Modify Master File Modification/File Volume 
    '    'Returns		    :  integer
    '    'Author			    :  See Siew
    '    'Date			    :  08/04/2006
    '    '****************************************************************************************************
    '    Try
    '        Dim oParamSQL(16) As SqlParameter
    '        oParamSQL(0) = New SqlParameter("@intFileID", intFileID)
    '        oParamSQL(1) = New SqlParameter("@intVolumeID", intVolumeID)
    '        oParamSQL(2) = New SqlParameter("@strFileTitle", strFileTitle)
    '        oParamSQL(3) = New SqlParameter("@strFileRef", strFileRef)
    '        oParamSQL(4) = New SqlParameter("@strFileKeyword", strFileKeyword)
    '        oParamSQL(5) = New SqlParameter("@strFileRemarks", strFileRemarks)
    '        oParamSQL(6) = New SqlParameter("@strFileSecStatus", strFileSecStatus)
    '        oParamSQL(7) = New SqlParameter("@strFileStatusID", strFileStatusID)
    '        oParamSQL(8) = New SqlParameter("@strFileReviewDt", strFileReviewDt)
    '        oParamSQL(9) = New SqlParameter("@strChgReviewDt", strChgReviewDt)
    '        oParamSQL(10) = New SqlParameter("@strFileCloseDt", strFileCloseDt)
    '        oParamSQL(11) = New SqlParameter("@strChgCloseDt", strChgCloseDt)
    '        oParamSQL(12) = New SqlParameter("@strAutoChgLocF", strAutoChgLocF)
    '        oParamSQL(13) = New SqlParameter("@strAutoChgLocID", strAutoChgLocID)
    '        oParamSQL(14) = New SqlParameter("@strLoginID", strLoginID)
    '        oParamSQL(15) = New SqlParameter("@intUsrID", intUsrID)
    '        oParamSQL(16) = New SqlParameter("@strType", strType)

    '        fnFileModifyFile = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_File_ModifyFile", oParamSQL)
    '    Catch ex As Exception
    '        fnFileModifyFile = Nothing
    '        Throw ex
    '    End Try
    'End Function

    Public Shared Function fnFileModifyFile(ByVal strAutoChgLocF As String, _
                                            ByVal intFileID As Integer, _
                                            ByVal intFileDetailID As Integer, _
                                            ByVal strStatus As String, _
                                            ByVal FileCloseDt As Object, _
                                            ByVal Remarks As String, _
                                            ByVal strLoginID As String, _
                                            ByVal intUsrID As String
                                            ) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Modify Master File Modification/File Volume 
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  08/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(6) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intFileID", intFileID)
            oParamSQL(1) = New SqlParameter("@intFileDetailID", intFileDetailID)
            oParamSQL(2) = New SqlParameter("@strStatus", strStatus)
            oParamSQL(3) = New SqlParameter("@FileCloseDt", FileCloseDt)
            oParamSQL(4) = New SqlParameter("@Remarks", Remarks)
            oParamSQL(5) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(6) = New SqlParameter("@intUsrID", intUsrID)

            fnFileModifyFile = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_File_ModifyFile", oParamSQL)
        Catch ex As Exception
            fnFileModifyFile = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnFileGetRecordForEditDS( _
                                        ByVal strFileID As Integer, _
                                        ByVal strVolumeID As Integer, _
                                        ByVal strRecType As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get File Record for Modification Screen
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  08/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strFileID", strFileID)
            oParamSQL(1) = New SqlParameter("@strVolumeID", strVolumeID)
            oParamSQL(2) = New SqlParameter("@strRecType", strRecType)

            fnFileGetRecordForEditDS = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_File_GetRecordForEdit", oParamSQL)
        Catch ex As Exception
            fnFileGetRecordForEditDS = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnFileGetRecordForEdit( _
                                    ByVal strFileID As Integer, _
                                    ByVal strFileDetailID As Integer
                                    ) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get File Record for Modification Screen
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  08/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strFileID", strFileID)
            oParamSQL(1) = New SqlParameter("@strFileDetailID", strFileDetailID)

            fnFileGetRecordForEdit = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_File_GetRecordForEdit", oParamSQL)
        Catch ex As Exception
            fnFileGetRecordForEdit = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnFileSearchRecord( _
                                    ByVal strMasterFileRefNo As String, _
                                    ByVal strDetailFileRefNo As String, _
                                    ByVal strDetailFileTitle As String, _
                                    ByVal strLocOfficerName As String, _
                                    ByVal strRemarks As String, _
                                    ByVal StrSortName As String, _
                                    ByVal strSortOrder As String
                                    ) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get File Record through Search Screen
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  07/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(6) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strMasterFileRefNo", strMasterFileRefNo)
            oParamSQL(1) = New SqlParameter("@strDetailFileRefNo", strDetailFileRefNo)
            oParamSQL(2) = New SqlParameter("@strDetailFileTitle", strDetailFileTitle)
            oParamSQL(3) = New SqlParameter("@strLocationOfficerName", strLocOfficerName)
            oParamSQL(4) = New SqlParameter("@strRemarks", strRemarks)
            oParamSQL(5) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(6) = New SqlParameter("@strSortOrder", strSortOrder)

            fnFileSearchRecord = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_File_SearchRecord", oParamSQL)
        Catch ex As Exception
            fnFileSearchRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnFileCheckDupFileNo( _
                                ByVal intFileNo As Integer, _
                                ByVal intDeptID As Integer, _
                                ByVal strFileNum1 As String, _
                                ByVal strFileNum2 As String, _
                                ByVal strFileNum3 As String, _
                                ByVal strFileNum4 As String, _
                                ByVal strType As String) As Boolean
        '****************************************************************************************************
        'Purpose  		    :  To Check Duplicate File Number
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  26/03/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(7) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intFileNo", intFileNo)
            oParamSQL(1) = New SqlParameter("@intDeptID", intDeptID)
            oParamSQL(2) = New SqlParameter("@strFileNum1", strFileNum1)
            oParamSQL(3) = New SqlParameter("@strFileNum2", strFileNum2)
            oParamSQL(4) = New SqlParameter("@strFileNum3", strFileNum3)
            oParamSQL(5) = New SqlParameter("@strFileNum4", strFileNum4)
            oParamSQL(6) = New SqlParameter("@strType", strType)
            oParamSQL(7) = New SqlParameter("@strRetVal", SqlDbType.Char, 1)
            oParamSQL(7).Direction = ParameterDirection.Output

            fnFileCheckDupFileNo = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_File_CheckDuplicateFileNo", oParamSQL)
            If oParamSQL(7).Value = "Y" Then      'Duplicate Records
                fnFileCheckDupFileNo = True
            Else                                  'Not Duplicate Records
                fnFileCheckDupFileNo = False
            End If
        Catch ex As Exception
            fnFileCheckDupFileNo = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnFileAddFile( _
                                        ByVal fld_FileRefNo As String, _
                                        ByVal fld_FileDetail_FileRefNo As String, _
                                        ByVal fld_FileDetail_FileTitle As String, _
                                        ByVal fld_Remarks As String, _
                                        ByRef strLoginID As String, _
                                        ByVal intUsrID As String,
                                        ByRef intFileDetailID As Integer
                                       ) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Add New Master File 
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  26/03/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(6) As SqlParameter
            oParamSQL(0) = New SqlParameter("@fld_FileRefNo", fld_FileRefNo)
            oParamSQL(1) = New SqlParameter("@fld_FileDetail_FileRefNo", fld_FileDetail_FileRefNo)
            oParamSQL(2) = New SqlParameter("@fld_FileDetail_FileTitle", fld_FileDetail_FileTitle)
            oParamSQL(3) = New SqlParameter("@fld_Remarks", fld_Remarks)
            oParamSQL(4) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(5) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(6) = New SqlParameter("@fld_FileDetailID", SqlDbType.Int, 4)
            oParamSQL(6).Direction = ParameterDirection.Output

            fnFileAddFile = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_File_AddFile", oParamSQL)
            intFileDetailID = oParamSQL(6).Value

        Catch ex As Exception
            fnFileAddFile = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnFileAddMasterFile( _
                                        ByVal intDeptID As Integer, _
                                        ByVal strFileNum1 As String, _
                                        ByVal strFileNum2 As String, _
                                        ByVal strFileNum3 As String, _
                                        ByVal strFileNum4 As String, _
                                        ByVal strFileTitle As String, _
                                        ByVal strFileRef As String, _
                                        ByVal strFileKeyword As String, _
                                        ByVal strFileRemarks As String, _
                                        ByVal strFileSecStatus As String, _
                                        ByVal strLoginID As String, _
                                        ByVal intUsrID As Integer, _
                                        ByRef intFileID As Integer, _
                                        ByRef intVolID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Add New Master File 
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  26/03/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(13) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intDeptID", intDeptID)
            oParamSQL(1) = New SqlParameter("@strFileNum1", strFileNum1)
            oParamSQL(2) = New SqlParameter("@strFileNum2", strFileNum2)
            oParamSQL(3) = New SqlParameter("@strFileNum3", strFileNum3)
            oParamSQL(4) = New SqlParameter("@strFileNum4", strFileNum4)
            oParamSQL(5) = New SqlParameter("@strFileTitle", strFileTitle)
            oParamSQL(6) = New SqlParameter("@strFileRef", strFileRef)
            oParamSQL(7) = New SqlParameter("@strFileKeyword", strFileKeyword)
            oParamSQL(8) = New SqlParameter("@strFileRemarks", strFileRemarks)
            oParamSQL(9) = New SqlParameter("@strFileSecStatus", strFileSecStatus)
            oParamSQL(10) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(11) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(12) = New SqlParameter("@intFileID", SqlDbType.Int)
            oParamSQL(12).Direction = ParameterDirection.Output
            oParamSQL(13) = New SqlParameter("@intVolID", SqlDbType.Int)
            oParamSQL(13).Direction = ParameterDirection.Output

            fnFileAddMasterFile = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_File_AddMasterFile", oParamSQL)
            intFileID = oParamSQL(12).Value
            intVolID = oParamSQL(13).Value
        Catch ex As Exception
            fnFileAddMasterFile = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnFileDetail_GetAllRecord() As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Restore Record
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  16/04/2006
        '****************************************************************************************************
        Try

            fnFileDetail_GetAllRecord = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_File_GetALLRecord", Nothing)

        Catch ex As Exception
            fnFileDetail_GetAllRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnFile_GetDataByfld_FileDetailID(ByVal fld_FileDetailID As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Restore Record
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  16/04/2006
        '****************************************************************************************************
        Try

            Dim oParamSQL(0) As SqlParameter
            oParamSQL(0) = New SqlParameter("@fld_FileDetailID", fld_FileDetailID)

            fnFile_GetDataByfld_FileDetailID = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_File_GetDataByfld_FileDetailID", oParamSQL)

        Catch ex As Exception
            fnFile_GetDataByfld_FileDetailID = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnFile_DetailGetDataByfld_FileDetail_FileRefNoAndfld_FileRefNo(ByVal strMasterFileRefNo As String, ByVal strDetailFileRefNo As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Restore Record
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  16/04/2006
        '****************************************************************************************************
        Try

            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strMasterFileRefNo", strMasterFileRefNo)
            oParamSQL(1) = New SqlParameter("@strDetailFileRefNo", strDetailFileRefNo)

            fnFile_DetailGetDataByfld_FileDetail_FileRefNoAndfld_FileRefNo = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_File_DetailGetDataByfld_FileDetail_FileRefNoAndfld_FileRefNo", oParamSQL)

        Catch ex As Exception
            fnFile_DetailGetDataByfld_FileDetail_FileRefNoAndfld_FileRefNo = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnFile_File_GetRecordGenerateTxtFile(ByVal strFileDetailIDs As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Restore Record
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  16/04/2006
        '****************************************************************************************************
        Try

            Dim oParamSQL(0) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strFileDetailIDs", strFileDetailIDs)

            fnFile_File_GetRecordGenerateTxtFile = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_File_GetRecordGenerateTxtFile", oParamSQL)

        Catch ex As Exception
            fnFile_File_GetRecordGenerateTxtFile = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnFile_GetDataByfld_FileDetailBarcodeID(ByVal fld_FileDetailBarcodeID As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Restore Record
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  16/04/2006
        '****************************************************************************************************
        Try

            Dim oParamSQL(0) As SqlParameter
            oParamSQL(0) = New SqlParameter("@fld_FileDetailBarcodeID", fld_FileDetailBarcodeID)

            fnFile_GetDataByfld_FileDetailBarcodeID = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_File_GetDataByfld_FileDetailBarcodeID", oParamSQL)

        Catch ex As Exception
            fnFile_GetDataByfld_FileDetailBarcodeID = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fn_tbl_File_Movement_InsertWirelessBarcode( _
                                        ByVal Barcode As Integer, _
                                        ByVal BarcodeDateTime As DateTime, _
                                        ByVal BarcodeLocOfficer As Integer, _
                                        ByVal status As String, _
                                        ByVal strLoginID As String, _
                                        ByVal intUsrID As String
                                       ) As String
        '****************************************************************************************************
        'Purpose  		    :  To Add New Master File 
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  26/03/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(6) As SqlParameter
            oParamSQL(0) = New SqlParameter("@Barcode", Barcode)
            oParamSQL(1) = New SqlParameter("@BarcodeDateTime", BarcodeDateTime)
            oParamSQL(2) = New SqlParameter("@BarcodeLocOfficer", BarcodeLocOfficer)
            oParamSQL(3) = New SqlParameter("@status", status)
            oParamSQL(4) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(5) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(6) = New SqlParameter("@LocOfficer", SqlDbType.Int, 4)
            oParamSQL(6).Direction = ParameterDirection.Output

            fn_tbl_File_Movement_InsertWirelessBarcode = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_tbl_File_Movement_InsertWirelessBarcode", oParamSQL)
            fn_tbl_File_Movement_InsertWirelessBarcode = oParamSQL(6).Value

        Catch ex As Exception
            fn_tbl_File_Movement_InsertWirelessBarcode = -1
        End Try
    End Function


    Public Shared Function fnShrinkDB()
        Try
            SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBStringForFTS, CommandType.StoredProcedure, "sproc_ShrinkDB")
        Catch ex As Exception
        End Try
    End Function
End Class
