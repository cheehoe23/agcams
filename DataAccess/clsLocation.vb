#Region "Information Section"
' ****************************************************************************************************
' Description       : Location Class.
' Purpose           : To perform the business logic for Location (such as view, add, delete, edit)
' Author			: See Siew
' Date			    : 01.03.2007
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsLocation
    Public Shared Function fnLocation_PrintLocMarker( _
                        ByVal strLoctSubIDs As String, ByVal strBatchPath As String, _
                        ByVal intLoginUsrID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Print Location Marker
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  29/03/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strLoctSubIDs", strLoctSubIDs)
            oParamSQL(1) = New SqlParameter("@strBatchPath", strBatchPath)
            oParamSQL(2) = New SqlParameter("@intLoginUsrID", intLoginUsrID)

            fnLocation_PrintLocMarker = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Location_PrintLocMarker", oParamSQL)
        Catch ex As Exception
            fnLocation_PrintLocMarker = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLocation_GetLocationDetails( _
                        ByVal strLoctSubIDs As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Location/Sub Location Records
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  163/03/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strLoctSubIDs", strLoctSubIDs)

            fnLocation_GetLocationDetails = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Location_GetLocationDetails", oParamSQL)
        Catch ex As Exception
            fnLocation_GetLocationDetails = Nothing
            Throw ex
        End Try
    End Function


    Public Shared Function fnLocation_GetLocation( _
                    ByVal intLocID As String, ByVal intSLocID As String, ByVal strType As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get All Location/Sub Location Records to populate in Drop Down List
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  16/12/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intLocID", intLocID)
            oParamSQL(1) = New SqlParameter("@intSLocID", intSLocID)
            oParamSQL(2) = New SqlParameter("@strType", strType)
            fnLocation_GetLocation = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Location_GetLocation", oParamSQL)
        Catch ex As Exception
            fnLocation_GetLocation = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnSubLocation_SelectByfld_STDetailID( _
                    ByVal fld_STDetailID As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get All Location/Sub Location Records to populate in Drop Down List
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  16/12/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(0) As SqlParameter
            oParamSQL(0) = New SqlParameter("@fld_STDetailID", fld_STDetailID)

            fnSubLocation_SelectByfld_STDetailID = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_SubLocation_SelectByfld_STDetailID", oParamSQL)
        Catch ex As Exception
            fnSubLocation_SelectByfld_STDetailID = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLocation_EditLocation( _
                        ByVal strLocID As String, ByVal strSLocID As String, _
                        ByVal strLocCode As String, ByVal strLocName As String, _
                        ByVal strActivate As String, ByVal strNewLightIPAdd As String, ByVal srtrNewLightType As String, _
                        ByVal strLoginID As String, ByVal strType As String, _
                        ByVal strDoorGantryF As String, ByVal strDGMvStatus As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Edit Location
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  15/12/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(10) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strLocID", strLocID)
            oParamSQL(1) = New SqlParameter("@strSLocID", strSLocID)
            oParamSQL(2) = New SqlParameter("@strLocCode", strLocCode)
            oParamSQL(3) = New SqlParameter("@strLocName", strLocName)
            oParamSQL(4) = New SqlParameter("@strActivate", strActivate)
            oParamSQL(5) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(6) = New SqlParameter("@strType", strType)
            oParamSQL(7) = New SqlParameter("@strNewLightIPAdd", strNewLightIPAdd)
            oParamSQL(8) = New SqlParameter("@srtrNewLightType", srtrNewLightType)
            oParamSQL(9) = New SqlParameter("@strDoorGantryF", strDoorGantryF)
            oParamSQL(10) = New SqlParameter("@strDGMvStatus", strDGMvStatus)


            fnLocation_EditLocation = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Location_EditLocation", oParamSQL)
        Catch ex As Exception
            fnLocation_EditLocation = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLocation_DeleteLocation( _
                        ByVal strLocIds As String, ByVal strSubLocIDs As String, _
                        ByVal strLoginID As String, ByVal strType As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Delete Location
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  14/12/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strLocIds", strLocIds)
            oParamSQL(1) = New SqlParameter("@strSubLocIDs", strSubLocIDs)
            oParamSQL(2) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(3) = New SqlParameter("@strType", strType)

            fnLocation_DeleteLocation = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Location_DeleteLocation", oParamSQL)
        Catch ex As Exception
            fnLocation_DeleteLocation = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLocation_AddNewLocation( _
                    ByVal strLocCode As String, ByVal strLocName As String, _
                    ByVal strSubLocXML As String, ByVal strLoginID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Add Location
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  12/12/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strLocCode", strLocCode)
            oParamSQL(1) = New SqlParameter("@strLocName", strLocName)
            oParamSQL(2) = New SqlParameter("@strSubLocXML", strSubLocXML)
            oParamSQL(3) = New SqlParameter("@strLoginID", strLoginID)

            fnLocation_AddNewLocation = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Location_AddNewLocation", oParamSQL)
        Catch ex As Exception
            fnLocation_AddNewLocation = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLocation_AddSubLocForModify( _
                    ByVal intLocID As String, ByVal intSLocID As String, ByVal strDeleteF As String, _
                    ByVal strSLocCode As String, ByVal strSLocName As String, _
                    ByVal strActivate As String, ByVal strLightIPAdd As String, ByVal strLightType As String, _
                    ByVal strLoginID As String, _
                    ByVal strDoorGantryF As String, ByVal strDGMvStatus As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Add New Sub Location
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  12/12/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(10) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intLocID", intLocID)
            oParamSQL(1) = New SqlParameter("@intSLocID", intSLocID)
            oParamSQL(2) = New SqlParameter("@strDeleteF", strDeleteF)
            oParamSQL(3) = New SqlParameter("@strSLocCode", strSLocCode)
            oParamSQL(4) = New SqlParameter("@strSLocName", strSLocName)
            oParamSQL(5) = New SqlParameter("@strActivate", strActivate)
            oParamSQL(6) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(7) = New SqlParameter("@strLightIPAdd", strLightIPAdd)
            oParamSQL(8) = New SqlParameter("@strLightType", strLightType)
            oParamSQL(9) = New SqlParameter("@strDoorGantryF", strDoorGantryF)
            oParamSQL(10) = New SqlParameter("@strDGMvStatus", strDGMvStatus)


            fnLocation_AddSubLocForModify = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Location_AddSubLocForModify", oParamSQL)
        Catch ex As Exception
            fnLocation_AddSubLocForModify = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLocation_CheckDuplicate4Sub( _
                               ByVal intLocID As Integer, ByVal intSubLocID As Integer, _
                               ByVal strLocCode As String, ByVal strType As String) As Boolean
        '****************************************************************************************************
        'Purpose  		    :  To check for duplicate Sub Location 
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  12/12/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intLocID", intLocID)
            oParamSQL(1) = New SqlParameter("@intSubLocID", intSubLocID)
            oParamSQL(2) = New SqlParameter("@strLocCode", strLocCode)
            oParamSQL(3) = New SqlParameter("@strType", strType)
            oParamSQL(4) = New SqlParameter("@strRetVal", SqlDbType.Char, 1)
            oParamSQL(4).Direction = ParameterDirection.Output

            SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Location_CheckDuplicate4Sub", oParamSQL)

            If oParamSQL(4).Value = "Y" Then  'Duplicate Records
                fnLocation_CheckDuplicate4Sub = True
            Else                                  'Not Duplicate Records
                fnLocation_CheckDuplicate4Sub = False
            End If

        Catch ex As Exception
            fnLocation_CheckDuplicate4Sub = Nothing
            Throw ex
        End Try
    End Function

    'Public Shared Function fnLocationGetAllRecForDDL() As SqlDataReader
    '    '****************************************************************************************************
    '    'Purpose  		    :  To Get All Location Records to populate in Drop Down List
    '    'Returns		    :  SqlDataReader
    '    'Author			    :  See Siew
    '    'Date			    :  20/10/2007
    '    '****************************************************************************************************
    '    Try
    '        Dim oParamSQL(1) As SqlParameter
    '        oParamSQL(0) = New SqlParameter("@StrSortName", "fld_LocationName")
    '        oParamSQL(1) = New SqlParameter("@strSortOrder", "ASC")
    '        fnLocationGetAllRecForDDL = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Location_GetAllRec", oParamSQL)
    '    Catch ex As Exception
    '        fnLocationGetAllRecForDDL = Nothing
    '        Throw ex
    '    End Try
    'End Function

    Public Shared Function fnLocationGetAllRecForDDL() As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get All Location Records to populate in Drop Down List
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  16/12/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intLocID", "0")
            oParamSQL(1) = New SqlParameter("@intSLocID", "0")
            oParamSQL(2) = New SqlParameter("@strType", "L")
            fnLocationGetAllRecForDDL = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Location_GetLocation", oParamSQL)
        Catch ex As Exception
            fnLocationGetAllRecForDDL = Nothing
            Throw ex
        End Try
    End Function


    Public Shared Function fnLocationGetRecForEdit(ByVal intLoctId As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Location Details For Edit Screen  
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  01/03/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intLoctId", intLoctId)
            fnLocationGetRecForEdit = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Location_GetRecForEdit", oParamSQL)
        Catch ex As Exception
            fnLocationGetRecForEdit = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLocationGetAllRec( _
                                        ByVal intLocID As String, ByVal intSLocID As String, _
                                        ByVal StrSortName As String, _
                                        ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get All Location Records to populate in View Location screen 
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  20/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intLocID", intLocID)
            oParamSQL(1) = New SqlParameter("@intSLocID", intSLocID)
            oParamSQL(2) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(3) = New SqlParameter("@strSortOrder", strSortOrder)
            fnLocationGetAllRec = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Location_GetAllRec", oParamSQL)
        Catch ex As Exception
            fnLocationGetAllRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLocationGetAllRecDR( _
                                        ByVal intLocID As String, ByVal intSLocID As String, _
                                        ByVal StrSortName As String, _
                                        ByVal strSortOrder As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get All Location Records to populate in View Location screen 
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  15/12/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intLocID", intLocID)
            oParamSQL(1) = New SqlParameter("@intSLocID", intSLocID)
            oParamSQL(2) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(3) = New SqlParameter("@strSortOrder", strSortOrder)
            fnLocationGetAllRecDR = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Location_GetAllRec", oParamSQL)
        Catch ex As Exception
            fnLocationGetAllRecDR = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLocationCheckDuplicate( _
                                                       ByVal intLoctID As Integer, _
                                                       ByVal strLoctCode As String, _
                                                       ByVal strType As String) As Boolean
        '****************************************************************************************************
        'Purpose  		    :  To check for duplicate Location 
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  01/03/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intLoctID", intLoctID)
            oParamSQL(1) = New SqlParameter("@strLoctCode", strLoctCode)
            oParamSQL(2) = New SqlParameter("@strType", strType)
            oParamSQL(3) = New SqlParameter("@strRetVal", SqlDbType.VarChar, 10)
            oParamSQL(3).Direction = ParameterDirection.Output
            SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Location_checkDuplicate", oParamSQL)
            If oParamSQL(3).Value = "Y" Then  'Duplicate Records
                fnLocationCheckDuplicate = True
            Else                                  'Not Duplicate Records
                fnLocationCheckDuplicate = False
            End If

        Catch ex As Exception
            fnLocationCheckDuplicate = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLocationGetAllRecForDDL( _
                                            ByVal StrSortName As String, _
                                            ByVal strSortOrder As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get All Location Records to populate in Drop Down List
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  22/03/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(1) = New SqlParameter("@strSortOrder", strSortOrder)
            fnLocationGetAllRecForDDL = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Location_GetAllRec", oParamSQL)
        Catch ex As Exception
            fnLocationGetAllRecForDDL = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLocationGetAllRecForDDLForFTS( _
                                            ByVal StrSortName As String, _
                                            ByVal strSortOrder As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get All Location Records to populate in Drop Down List
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  22/03/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(1) = New SqlParameter("@strSortOrder", strSortOrder)
            fnLocationGetAllRecForDDLForFTS = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_Location_GetAllRec", oParamSQL)
        Catch ex As Exception
            fnLocationGetAllRecForDDLForFTS = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLocationSubGetAllRecForDDLForFTS( _
                                            ByVal StrSortName As String, _
                                            ByVal strSortOrder As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get All Location Records to populate in Drop Down List
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  22/03/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(1) = New SqlParameter("@strSortOrder", strSortOrder)
            fnLocationSubGetAllRecForDDLForFTS = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_LocationSub_GetAllRec", oParamSQL)
        Catch ex As Exception
            fnLocationSubGetAllRecForDDLForFTS = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLocationSub_GetByfld_LocSubCode( _
                                            ByVal fld_LocSubCode As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Restore Record
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  16/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@fld_LocSubCode", fld_LocSubCode)

            fnLocationSub_GetByfld_LocSubCode = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_tbl_LocationSub_GetByfld_LocSubCode", oParamSQL)

        Catch ex As Exception
            fnLocationSub_GetByfld_LocSubCode = Nothing
            Throw ex
        End Try
    End Function

    'Public Shared Function fnLocationInsertUpdateDelete( _
    '                                        ByVal strLoctIDs As String, _
    '                                        ByVal strLoctCode As String, _
    '                                        ByVal strLoctName As String, _
    '                                        ByVal strLoginID As String, _
    '                                        ByVal strType As String) As Integer
    '    '****************************************************************************************************
    '    'Purpose  		    :  To Add, Edit, Delete particular Location
    '    'Returns		    :  integer
    '    'Author			    :  See Siew
    '    'Date			    :  20/10/2007
    '    '****************************************************************************************************
    '    Try
    '        Dim oParamSQL(4) As SqlParameter
    '        oParamSQL(0) = New SqlParameter("@strLoctIDs", strLoctIDs)
    '        oParamSQL(1) = New SqlParameter("@strLoctCode", strLoctCode)
    '        oParamSQL(2) = New SqlParameter("@strLoctName", strLoctName)
    '        oParamSQL(3) = New SqlParameter("@strLoginID", strLoginID)
    '        oParamSQL(4) = New SqlParameter("@strType", strType)

    '        fnLocationInsertUpdateDelete = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Location_InsertUpdateDelete", oParamSQL)
    '    Catch ex As Exception
    '        fnLocationInsertUpdateDelete = Nothing
    '        Throw ex
    '    End Try
    'End Function
End Class
