#Region "Information Section"
' ****************************************************************************************************
' Description       : Category Class.
' Purpose           : To perform the business logic for Category (such as view, add, delete, edit)
' Author			: See Siew
' Date			    : 23.10.2005
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsDeviceType
    
    Public Shared Function fnDeviceType_DuplicateDeviceType( _
                                                   ByVal intDeviceTypeID As Integer, _
                                                   ByVal strDeviceType As String, _
                                                   ByVal strType As String) As Boolean
        '****************************************************************************************************
        'Purpose  		    :  To check for duplicate Department 
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  20/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intDeviceTypeID", intDeviceTypeID)
            oParamSQL(1) = New SqlParameter("@strDeviceType", strDeviceType)
            oParamSQL(2) = New SqlParameter("@strType", strType)
            oParamSQL(3) = New SqlParameter("@strRetVal", SqlDbType.VarChar, 10)
            oParamSQL(3).Direction = ParameterDirection.Output
            SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_DeviceType_DuplicateDeviceType", oParamSQL)
            If oParamSQL(3).Value = "Y" Then  'Duplicate Records
                fnDeviceType_DuplicateDeviceType = True
            Else                                  'Not Duplicate Records
                fnDeviceType_DuplicateDeviceType = False
            End If

        Catch ex As Exception
            fnDeviceType_DuplicateDeviceType = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDeviceType_InsertUpdateDelete( _
                                        ByVal intDeviceTypeIDs As String, _
                                        ByVal strDeviceType As String, _
                                        ByVal strLoginID As String, _
                                        ByVal strType As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Add, Edit, Delete particular Department
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  20/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intDeviceTypeIDs", intDeviceTypeIDs)
            oParamSQL(1) = New SqlParameter("@strDeviceType", strDeviceType)
            oParamSQL(2) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(3) = New SqlParameter("@strType", strType)
            fnDeviceType_InsertUpdateDelete = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_DeviceType_InsertUpdateDelete", oParamSQL)
        Catch ex As Exception
            fnDeviceType_InsertUpdateDelete = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDeviceTypeGetAllRec( _
                                        ByVal StrSortName As String, _
                                        ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get All Department Records to populate in View Department screen 
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  25/02/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(1) = New SqlParameter("@strSortOrder", strSortOrder)
            fnDeviceTypeGetAllRec = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_DeviceType_GetAllRec", oParamSQL)
        Catch ex As Exception
            fnDeviceTypeGetAllRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnDeviceType_GetDeviceTypeForEdit(ByVal intDeviceTypeId As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Deprtment Details For Edit Screen  
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  20/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intDeviceTypeId", intDeviceTypeId)
            fnDeviceType_GetDeviceTypeForEdit = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_DeviceType_GetDeviceTypeForEdit", oParamSQL)
        Catch ex As Exception
            fnDeviceType_GetDeviceTypeForEdit = Nothing
            Throw ex
        End Try
    End Function

End Class
