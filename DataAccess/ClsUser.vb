#Region "Information Section"
' ****************************************************************************************************
' Description       : User Class.
' Purpose           : To perform the business logic for User (such as view, add, delete, edit)
' Author			: See Siew
' Date			    : 25.01.2005
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class ClsUser

    Public Shared Function fnUsr_CheckUserExistAD( _
                                       ByVal strUserName As String, _
                                       ByRef strLoginID As String, _
                                       ByRef strUsrExistF As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To check for existing user
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  21/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strUserName", strUserName)
            oParamSQL(1) = New SqlParameter("@strLoginID", SqlDbType.VarChar, 20)
            oParamSQL(1).Direction = ParameterDirection.Output
            oParamSQL(2) = New SqlParameter("@strUsrExistF", SqlDbType.Char, 1)
            oParamSQL(2).Direction = ParameterDirection.Output

            fnUsr_CheckUserExistAD = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Usr_CheckUserExistAD", oParamSQL)
            strLoginID = oParamSQL(1).Value
            strUsrExistF = oParamSQL(2).Value
        Catch ex As Exception
            fnUsr_CheckUserExistAD = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnUsr_CheckUserExistADFORDDL( _
                                       ByVal strUserName As String, _
                                       ByRef strLoginID As String, _
                                       ByRef strUsrExistF As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To check for existing user
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  21/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strUserName", strUserName)
            oParamSQL(1) = New SqlParameter("@strLoginID", SqlDbType.VarChar, 20)
            oParamSQL(1).Direction = ParameterDirection.Output
            oParamSQL(2) = New SqlParameter("@strUsrExistF", SqlDbType.Char, 1)
            oParamSQL(2).Direction = ParameterDirection.Output

            fnUsr_CheckUserExistADFORDDL = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Usr_CheckUserExistADForDDL", oParamSQL)
            strLoginID = oParamSQL(1).Value
            strUsrExistF = oParamSQL(2).Value
        Catch ex As Exception
            fnUsr_CheckUserExistADFORDDL = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnUsrGetUsrEmailFromAD(ByVal strSearch As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get All User Email Records from Active Directory  
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  22/10/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strSearch", strSearch)
            fnUsrGetUsrEmailFromAD = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Usr_GetUsrEmailFromAD", oParamSQL)
        Catch ex As Exception
            fnUsrGetUsrEmailFromAD = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnUsrGetUsrFromAD( _
                                ByVal strCallType As String, _
                                ByVal intPageSize As Integer, _
                                ByVal strSearchString As String, _
                                ByVal strLastString As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get User from Active Directory (for Add Login User or show OWNER in dropdownlist)
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  19/10/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strCallType", strCallType)
            oParamSQL(1) = New SqlParameter("@intPageSize", intPageSize)
            oParamSQL(2) = New SqlParameter("@strSearchString", strSearchString)
            oParamSQL(3) = New SqlParameter("@strLastString", strLastString)
            fnUsrGetUsrFromAD = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Usr_GetUsrFromAD", oParamSQL)
        Catch ex As Exception
            fnUsrGetUsrFromAD = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnUsrGetUsrFromADForDropDownList( _
                                ByVal strCallType As String, _
                                ByVal intPageSize As Integer, _
                                ByVal strSearchString As String, _
                                ByVal strLastString As String, _
                                ByVal startOffset As String
                                ) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get User from Active Directory (for Add Login User or show OWNER in dropdownlist)
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  19/10/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strCallType", strCallType)
            oParamSQL(1) = New SqlParameter("@intPageSize", intPageSize)
            oParamSQL(2) = New SqlParameter("@strSearchString", strSearchString)
            oParamSQL(3) = New SqlParameter("@strLastString", strLastString)
            oParamSQL(4) = New SqlParameter("@startOffset", startOffset)
            fnUsrGetUsrFromADForDropDownList = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Usr_GetUsrFromADForDDL", oParamSQL)
        Catch ex As Exception
            fnUsrGetUsrFromADForDropDownList = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnUsrGetUsrFromADDR( _
                            ByVal strCallType As String, _
                            ByVal intPageSize As Integer, _
                            ByVal strSearchString As String, _
                            ByVal strLastString As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get User from Active Directory (for Add Login User or show OWNER in dropdownlist)
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  19/10/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strCallType", strCallType)
            oParamSQL(1) = New SqlParameter("@intPageSize", intPageSize)
            oParamSQL(2) = New SqlParameter("@strSearchString", strSearchString)
            oParamSQL(3) = New SqlParameter("@strLastString", strLastString)
            fnUsrGetUsrFromADDR = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Usr_GetUsrFromAD", oParamSQL)
        Catch ex As Exception
            fnUsrGetUsrFromADDR = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnUsrGetUsrForEdit(ByVal intUsrId As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get User Details For Edit Screen  
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  19/10/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intUsrId", intUsrId)
            fnUsrGetUsrForEdit = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Usr_GetUsrForEdit", oParamSQL)
        Catch ex As Exception
            fnUsrGetUsrForEdit = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnUsrGetAllUsr( _
                                                ByVal StrSortName As String, _
                                                ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get All User Records which is Active  
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  19/10/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(1) = New SqlParameter("@strSortOrder", strSortOrder)
            fnUsrGetAllUsr = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Usr_GetUsrRec", oParamSQL)
        Catch ex As Exception
            fnUsrGetAllUsr = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnUsrInsertUpdateDelete( _
                                           ByVal intUsrIDs As String, _
                                           ByVal strLoginId As String, _
                                           ByVal intUsrGrp As Integer, _
                                           ByVal strRemarks As String, _
                                           ByVal intDeptID As Integer, _
                                           ByVal strUserID As String, _
                                           ByVal strType As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Add, Edit, Delete User
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  19/10/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(6) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrIDs", intUsrIDs)
            oParamSQL(1) = New SqlParameter("@strLoginId", strLoginId)
            oParamSQL(2) = New SqlParameter("@intUsrGrp", intUsrGrp)
            oParamSQL(3) = New SqlParameter("@strRemarks", strRemarks)
            oParamSQL(4) = New SqlParameter("@intDeptID", intDeptID)
            oParamSQL(5) = New SqlParameter("@strUserID", strUserID)
            oParamSQL(6) = New SqlParameter("@strType", strType)

            fnUsrInsertUpdateDelete = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Usr_InsertUpdateDelete", oParamSQL)
        Catch ex As Exception
            fnUsrInsertUpdateDelete = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnUsrDuplicateLoginID( _
                                    ByVal strLoginID As String) As Boolean
        '****************************************************************************************************
        'Purpose  		    :  To check for duplicate login ID 
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  19/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(1) = New SqlParameter("@strRetVal", SqlDbType.VarChar, 10)
            oParamSQL(1).Direction = ParameterDirection.Output
            SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Usr_DuplicateLoginID", oParamSQL)
            If oParamSQL(1).Value = "Y" Then  'Duplicate Records
                fnUsrDuplicateLoginID = True
            Else                              'Not Duplicate Records
                fnUsrDuplicateLoginID = False
            End If

        Catch ex As Exception
            fnUsrDuplicateLoginID = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnUsrGetAllUsrDR( _
                                            ByVal StrSortName As String, _
                                            ByVal strSortOrder As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get All User Records which is Active  
        'Returns		    :  SqldataReader
        'Author			    :  See Siew
        'Date			    :  26/01/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(1) = New SqlParameter("@strSortOrder", strSortOrder)
            oParamSQL(2) = New SqlParameter("@strUsrName", "")
            oParamSQL(3) = New SqlParameter("@strUsrType", "")
            oParamSQL(4) = New SqlParameter("@intDept", "-1")
            fnUsrGetAllUsrDR = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Usr_GetUsrRec", oParamSQL)
        Catch ex As Exception
            fnUsrGetAllUsrDR = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnUsrGetAllUsrDRFTS( _
                                            ByVal StrSortName As String, _
                                            ByVal strSortOrder As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get All User Records which is Active  
        'Returns		    :  SqldataReader
        'Author			    :  See Siew
        'Date			    :  26/01/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(1) = New SqlParameter("@strSortOrder", strSortOrder)
            oParamSQL(2) = New SqlParameter("@strUsrName", "")
            oParamSQL(3) = New SqlParameter("@strUsrType", "")
            oParamSQL(4) = New SqlParameter("@intDept", "-1")
            fnUsrGetAllUsrDRFTS = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_Usr_GetUsrRec", oParamSQL)
        Catch ex As Exception
            fnUsrGetAllUsrDRFTS = Nothing
            Throw ex
        End Try
    End Function

End Class
