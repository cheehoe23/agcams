#Region "Information Section"
' ****************************************************************************************************
' Description       : Asset Class.
' Purpose           : To perform the business logic for Asset (such as view, add, delete, edit)
' Author			: See Siew
' Date			    : 25.10.2005
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsAsset
    Public Shared Function fnAsset_GetAssetBarcode(ByVal intAssetID As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Asset Barcode  
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  12/03/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intAssetID", intAssetID)
            fnAsset_GetAssetBarcode = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Asset_GetAssetBarcode", oParamSQL)
        Catch ex As Exception
            fnAsset_GetAssetBarcode = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAssetModify_GetRFIGMvHistory( _
                                    ByVal intAssetID As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get RFID Movement record for asset
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  24/02/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intAssetID ", intAssetID)

            fnAssetModify_GetRFIGMvHistory = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_AssetModify_GetRFIGMvHistory", oParamSQL)
        Catch ex As Exception
            fnAssetModify_GetRFIGMvHistory = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnReturnedAsset_UpdatePendingNotice( _
                                        ByVal strXMLRec As String, _
                                        ByVal intUsrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To update Peding Notice for condemnation
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  01/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strXMLRec", strXMLRec)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)
            fnReturnedAsset_UpdatePendingNotice = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_ReturnedAsset_UpdatePendingNotice", oParamSQL)
        Catch ex As Exception
            fnReturnedAsset_UpdatePendingNotice = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnReturnedAsset_ReturnAsset( _
                                ByVal strAssetIDs As String, _
                                ByVal intUsrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To update Asset Status (Return)
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  29/07/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDs", strAssetIDs)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)

            fnReturnedAsset_ReturnAsset = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_ReturnedAsset_ReturnAsset", oParamSQL)
        Catch ex As Exception
            fnReturnedAsset_ReturnAsset = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnReturnedAsset_SearchRecord( _
                ByVal strAssetIDAMS As String, _
                ByVal strAssetType As String, ByVal intAssetCatID As Integer, ByVal intAssetCatSubID As Integer, _
                ByVal strAssetStatus As String, ByVal strPurchaseFDt As String, ByVal strPurchaseTDt As String, _
                ByVal strWarExpFDt As String, ByVal strWarExpTDt As String, _
                ByVal intDeptID As Integer, ByVal intLocID As Integer, ByVal strOwnerID As String, _
                ByVal StrSortName As String, ByVal strSortOrder As String, ByRef intTotRec As String, _
                ByVal strAdminF As String, ByVal intUsrID As String, _
                ByVal intLocSubID As String, ByVal strCtrlItemF As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Seacrh Asset Record For Returned (Leased Inventory)
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  29/07/2008
        '****************************************************************************************************
        Try
            Dim oParamSQL(18) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDAMS", strAssetIDAMS)       '--""=All
            oParamSQL(1) = New SqlParameter("@strAssetType", strAssetType)         '--""=All, A=Fixed Asset, I=Inventory
            oParamSQL(2) = New SqlParameter("@intAssetCatID", intAssetCatID)       '--"0"=All
            oParamSQL(3) = New SqlParameter("@intAssetCatSubID", intAssetCatSubID) '--"0"=All
            oParamSQL(4) = New SqlParameter("@strAssetStatus", strAssetStatus)   '--"0"=All, (A)ctive, (I)nactive, (R)edundancy, (C)ondenmation, (D)isposed
            oParamSQL(5) = New SqlParameter("@strPurchaseFDt", strPurchaseFDt)   '--""=All
            oParamSQL(6) = New SqlParameter("@strPurchaseTDt", strPurchaseTDt)   '--""=All
            oParamSQL(7) = New SqlParameter("@strWarExpFDt", strWarExpFDt)       '--""=All
            oParamSQL(8) = New SqlParameter("@strWarExpTDt", strWarExpTDt)       '--""=All
            oParamSQL(9) = New SqlParameter("@intDeptID", intDeptID)            '--"0"=All
            oParamSQL(10) = New SqlParameter("@intLocID", intLocID)              '--"0"=All
            oParamSQL(11) = New SqlParameter("@strOwnerID", strOwnerID)          '--""=All
            oParamSQL(12) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(13) = New SqlParameter("@strSortOrder", strSortOrder)
            oParamSQL(14) = New SqlParameter("@intTotRec", SqlDbType.Int, 10)
            oParamSQL(14).Direction = ParameterDirection.Output
            oParamSQL(15) = New SqlParameter("@strAdminF", strAdminF)
            oParamSQL(16) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(17) = New SqlParameter("@intLocSubID", intLocSubID)
            oParamSQL(18) = New SqlParameter("@strCtrlItemF", strCtrlItemF)

            fnReturnedAsset_SearchRecord = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_ReturnedAsset_SearchRecord", oParamSQL)
            intTotRec = oParamSQL(14).Value
        Catch ex As Exception
            fnReturnedAsset_SearchRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAssetFloatCtrl_GetRecord( _
                                                    ByVal StrSortName As String, _
                                                    ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Float Control record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  04/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(1) = New SqlParameter("@strSortOrder", strSortOrder)

            fnAssetFloatCtrl_GetRecord = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_AssetFloatCtrl_GetRecord", oParamSQL)
        Catch ex As Exception
            fnAssetFloatCtrl_GetRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAsset_updateAsset( _
                ByVal intAssetID As Integer, _
                ByVal strCatAddInfo1 As String, ByVal strCatAddInfo2 As String, ByVal strCatAddInfo3 As String, _
                ByVal strCatAddInfo4 As String, ByVal strCatAddInfo5 As String, _
                ByVal strAssetStatus As String, ByVal strAssetBrand As String, ByVal strAssetDesc As String, _
                ByVal strCost As String, ByVal strCostCenter As String, _
                ByVal strPurchaseDt As String, ByVal strWarExpDt As String, _
                ByVal strAssetRemark As String, ByVal strTempAsset As String, _
                ByVal intDeptID As Integer, ByVal intLocationID As Integer, ByVal strOwnerID As String, _
                ByVal strRedunCondemnReason As String, ByVal strDisposedReason As String, _
                ByVal intUsrID As Integer, ByVal intCatSubID As Integer, ByRef intStatusInfoID As String, _
                ByVal intLocSubID As Integer, ByVal strCtrlItemF As String, _
                ByVal strShortDescription As String, ByVal strOtherInformation As String, ByVal strAssetIdNFS As String,
                ByVal intDeviceTypeID As Integer, ByVal strSerialNo As String, ByVal strMacAddress As String,
            ByVal strNewHostname As String, ByVal strStatus As String, ByVal strMachineModel As String
        ) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To update Asset 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  03/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(33) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intAssetID", intAssetID)
            oParamSQL(1) = New SqlParameter("@strCatAddInfo1", strCatAddInfo1)
            oParamSQL(2) = New SqlParameter("@strCatAddInfo2", strCatAddInfo2)
            oParamSQL(3) = New SqlParameter("@strCatAddInfo3", strCatAddInfo3)
            oParamSQL(4) = New SqlParameter("@strCatAddInfo4", strCatAddInfo4)
            oParamSQL(5) = New SqlParameter("@strCatAddInfo5", strCatAddInfo5)
            oParamSQL(6) = New SqlParameter("@strAssetStatus", strAssetStatus)
            oParamSQL(7) = New SqlParameter("@strAssetBrand", strAssetBrand)
            oParamSQL(8) = New SqlParameter("@strAssetDesc", strAssetDesc)
            oParamSQL(9) = New SqlParameter("@strCost", strCost)
            oParamSQL(10) = New SqlParameter("@strCostCenter", strCostCenter)
            oParamSQL(11) = New SqlParameter("@strPurchaseDt", strPurchaseDt)
            oParamSQL(12) = New SqlParameter("@strWarExpDt", strWarExpDt)
            oParamSQL(13) = New SqlParameter("@strAssetRemark", strAssetRemark)
            oParamSQL(14) = New SqlParameter("@strTempAsset", strTempAsset)
            oParamSQL(15) = New SqlParameter("@intDeptID", intDeptID)
            oParamSQL(16) = New SqlParameter("@intLocationID", intLocationID)
            oParamSQL(17) = New SqlParameter("@strOwnerID", strOwnerID)
            oParamSQL(18) = New SqlParameter("@strRedunCondemnReason", strRedunCondemnReason)
            oParamSQL(19) = New SqlParameter("@strDisposedReason", strDisposedReason)
            oParamSQL(20) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(21) = New SqlParameter("@intCatSubID", intCatSubID)
            oParamSQL(22) = New SqlParameter("@intStatusInfoID", SqlDbType.Int, 20)
            oParamSQL(22).Direction = ParameterDirection.Output
            oParamSQL(23) = New SqlParameter("@intLocSubID", intLocSubID)
            oParamSQL(24) = New SqlParameter("@strCtrlItemF", strCtrlItemF)
            oParamSQL(25) = New SqlParameter("@strShortDescription", strShortDescription)
            oParamSQL(26) = New SqlParameter("@strOtherInformation", strOtherInformation)
            oParamSQL(27) = New SqlParameter("@strAssetIdNFS", strAssetIdNFS)
            oParamSQL(28) = New SqlParameter("@intDeviceTypeID", intDeviceTypeID)
            oParamSQL(29) = New SqlParameter("@strSerialNo", strSerialNo)
            oParamSQL(30) = New SqlParameter("@strMacAddress", strMacAddress)
            oParamSQL(31) = New SqlParameter("@strNewHostname", strNewHostname)
            oParamSQL(32) = New SqlParameter("@strStatus", strStatus)
            oParamSQL(33) = New SqlParameter("@strMachineModel", strMachineModel)

            fnAsset_updateAsset = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Asset_updateAsset", oParamSQL)
            intStatusInfoID = oParamSQL(22).Value
        Catch ex As Exception
            fnAsset_updateAsset = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAssetModify_GetMovementHistory( _
                                                        ByVal strAssetID As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get contract record for asset
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  03/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strAssetID", strAssetID)

            fnAssetModify_GetMovementHistory = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_AssetModify_GetMovementHistory", oParamSQL)
        Catch ex As Exception
            fnAssetModify_GetMovementHistory = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAsset_GetContractInfo( _
                                                    ByVal strAssetID As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get contract record for asset
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  03/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strAssetID", strAssetID)

            fnAsset_GetContractInfo = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Asset_GetContractInfo", oParamSQL)
        Catch ex As Exception
            fnAsset_GetContractInfo = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAssetModify_GetAssetRecords( _
                                                    ByVal strAssetID As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get record for Modify Asset
        'Returns		    :  sqldatareader
        'Author			    :  See Siew
        'Date			    :  02/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strAssetID", strAssetID)

            fnAssetModify_GetAssetRecords = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_AssetModify_GetAssetRecords", oParamSQL)
        Catch ex As Exception
            fnAssetModify_GetAssetRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnStatusInfo_GetRecForCertificate( _
                                                ByVal intStatusInfoID As Integer) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get record fro certificate
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  01/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intStatusInfoID", intStatusInfoID)

            fnStatusInfo_GetRecForCertificate = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_StatusInfo_GetRecForCertificate", oParamSQL)
        Catch ex As Exception
            fnStatusInfo_GetRecForCertificate = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnCondemn_UpdatePendingNotice( _
                                    ByVal strXMLRec As String, _
                                    ByVal intUsrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To update Peding Notice for condemnation
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  01/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strXMLRec", strXMLRec)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)
            fnCondemn_UpdatePendingNotice = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Condemn_UpdatePendingNotice", oParamSQL)
        Catch ex As Exception
            fnCondemn_UpdatePendingNotice = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAsset_GetCondemnExpRecord( _
                                                ByVal strAdminF As String, _
                                                ByVal intUsrID As Integer, _
                                                ByVal strGetRecType As String, _
                                                ByVal StrSortName As String, _
                                                ByVal strSortOrder As String, _
                                                ByRef intTotRec As Integer) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Asset Record which pending to condemn Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  01/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(5) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAdminF", strAdminF)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(2) = New SqlParameter("@strGetRecType", strGetRecType)
            oParamSQL(3) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(4) = New SqlParameter("@strSortOrder", strSortOrder)
            oParamSQL(5) = New SqlParameter("@intTotRec", SqlDbType.Int, 10)
            oParamSQL(5).Direction = ParameterDirection.Output

            fnAsset_GetCondemnExpRecord = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Asset_GetCondemnExpRecord", oParamSQL)
            intTotRec = oParamSQL(5).Value
        Catch ex As Exception
            fnAsset_GetCondemnExpRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnTempAsset_UpdateRecord( _
                                ByVal strXMLRec As String, _
                                ByVal intUsrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To update Temporary asset with NFS
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  31/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strXMLRec", strXMLRec)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)
            fnTempAsset_UpdateRecord = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_TempAsset_UpdateRecord", oParamSQL)
        Catch ex As Exception
            fnTempAsset_UpdateRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnTempAsset_GetNFSAssetRecords(ByVal intAssetID As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get NFS Record for Temporary Asset record
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  31/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intAssetID", intAssetID)

            fnTempAsset_GetNFSAssetRecords = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_TempAsset_GetNFSAssetRecords", oParamSQL)
        Catch ex As Exception
            fnTempAsset_GetNFSAssetRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnTempAsset_GetAssetRecords( _
                                                ByVal StrSortName As String, _
                                                ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Temporary Asset record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  31/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(1) = New SqlParameter("@strSortOrder", strSortOrder)

            fnTempAsset_GetAssetRecords = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_TempAsset_GetAssetRecords", oParamSQL)
        Catch ex As Exception
            fnTempAsset_GetAssetRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnBarcode_UpdatePrintLabelStatus( _
                            ByVal strAssetIDs As String, _
                            ByVal intUsrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To update Label Printed Status 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  31/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDs", strAssetIDs)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)
            fnBarcode_UpdatePrintLabelStatus = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Barcode_UpdatePrintLabelStatus", oParamSQL)
        Catch ex As Exception
            fnBarcode_UpdatePrintLabelStatus = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnBarcode_GetAssetDetailsDR( _
                                ByVal strAssetIDs As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Asset Barcode Details
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  05/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strAssetIDs", strAssetIDs)

            fnBarcode_GetAssetDetailsDR = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Barcode_GetAssetDetails", oParamSQL)
        Catch ex As Exception
            fnBarcode_GetAssetDetailsDR = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnBarcode_GetAssetDetails( _
                                                    ByVal strAssetIDs As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Asset Barcode Details
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  31/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strAssetIDs", strAssetIDs)

            fnBarcode_GetAssetDetails = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Barcode_GetAssetDetails", oParamSQL)
        Catch ex As Exception
            fnBarcode_GetAssetDetails = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnMissingOwner_CountRec( _
                                ByRef intTotalMissOwner As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To get total asset which is missing owner
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  30/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intTotalMissOwner", SqlDbType.Int, 10)
            oParamSQL.Direction = ParameterDirection.Output
            fnMissingOwner_CountRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_MissingOwner_CountRec", oParamSQL)
            intTotalMissOwner = oParamSQL.Value
        Catch ex As Exception
            fnMissingOwner_CountRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnMissingOwner_GetAssetRecords( _
                                                ByVal StrSortName As String, _
                                                ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Missing Owner Asset Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  30/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(1) = New SqlParameter("@strSortOrder", strSortOrder)

            fnMissingOwner_GetAssetRecords = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_MissingOwner_GetAssetRecords", oParamSQL)
        Catch ex As Exception
            fnMissingOwner_GetAssetRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAssetUpdateAssetStatus( _
                            ByVal strAssetIDs As String, _
                            ByVal strNewStatus As String, _
                            ByVal strReason As String, _
                            ByVal intUsrID As Integer, _
                            ByRef intStatusInfoID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To update Asset Status (Condemantion/Redundancy/Disposed)
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  30/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(5) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDs", strAssetIDs)
            oParamSQL(1) = New SqlParameter("@strNewStatus", strNewStatus)
            oParamSQL(2) = New SqlParameter("@strReason", strReason)
            oParamSQL(3) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(4) = New SqlParameter("@strInsertAuditF", "Y")
            oParamSQL(5) = New SqlParameter("@intStatusInfoID", SqlDbType.Int, 10)
            oParamSQL(5).Direction = ParameterDirection.Output
            fnAssetUpdateAssetStatus = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Asset_UpdateAssetStatus", oParamSQL)
            intStatusInfoID = oParamSQL(5).Value
        Catch ex As Exception
            fnAssetUpdateAssetStatus = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnStatusFileDeleteRec( _
                                                    ByVal intUsrID As Integer, _
                                                    ByVal intStatusFileID As Integer, _
                                                    ByVal strType As String, _
                                                    ByRef strFile2Delete As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To delete Status File
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  29/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(1) = New SqlParameter("@intStatusFileID", intStatusFileID)
            oParamSQL(2) = New SqlParameter("@strType", strType)
            oParamSQL(3) = New SqlParameter("@strFile2Delete", SqlDbType.VarChar, 4000)
            oParamSQL(3).Direction = ParameterDirection.Output
            fnStatusFileDeleteRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_StatusFile_DeleteRec", oParamSQL)
            strFile2Delete = oParamSQL(3).Value
        Catch ex As Exception
            fnStatusFileDeleteRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnStatusFileGetRec( _
                                            ByVal intUsrID As Integer, _
                                            ByVal intStatusInfoID As Integer, _
                                            ByVal strTempSaveF As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Sttaus File Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  29/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)                   'Created By (For Add Asset)
            oParamSQL(1) = New SqlParameter("@intStatusInfoID", intStatusInfoID)     'Status File ID (For Modify Asset)
            oParamSQL(2) = New SqlParameter("@strTempSaveF", strTempSaveF)           'Y=Temp Save, N=Not Temp Save

            fnStatusFileGetRec = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_StatusFile_GetRec", oParamSQL)
        Catch ex As Exception
            fnStatusFileGetRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnStatusFileInsertRec( _
                                                ByVal intUsrID As Integer, _
                                                ByVal intStatusInfoID As Integer, _
                                                ByVal strFileName As String, _
                                                ByVal strTempSaveF As String, _
                                                ByRef intStatusFileID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Insert Status File 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  29/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(1) = New SqlParameter("@intStatusInfoID", intStatusInfoID)
            oParamSQL(2) = New SqlParameter("@strFileName", strFileName)
            oParamSQL(3) = New SqlParameter("@strTempSaveF", strTempSaveF)
            oParamSQL(4) = New SqlParameter("@intStatusFileID", SqlDbType.Int, 5)
            oParamSQL(4).Direction = ParameterDirection.Output
            fnStatusFileInsertRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_StatusFile_InsertRec", oParamSQL)
            intStatusFileID = oParamSQL(4).Value
        Catch ex As Exception
            fnStatusFileInsertRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAssetTransferUpdateMovement( _
                                                ByVal strAssetIDs As String, _
                                                ByVal intDeptID As Integer, _
                                                ByVal intLocationID As Integer, _
                                                ByVal strOwnerID As String, _
                                                ByVal intUsrID As Integer, _
                                                ByVal intLocSubID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Update Asset Movement
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  29/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(5) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDs", strAssetIDs)
            oParamSQL(1) = New SqlParameter("@intDeptID", intDeptID)
            oParamSQL(2) = New SqlParameter("@intLocationID", intLocationID)
            oParamSQL(3) = New SqlParameter("@strOwnerID", strOwnerID)
            oParamSQL(4) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(5) = New SqlParameter("@intLocSubID", intLocSubID)
            fnAssetTransferUpdateMovement = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_AssetTransfer_UpdateMovement", oParamSQL)
        Catch ex As Exception
            fnAssetTransferUpdateMovement = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAssetTransferUpdateMovement( _
                                                ByVal strAssetIDs As String, _
                                                ByVal intDeptID As Integer, _
                                                ByVal intLocationID As Integer, _
                                                ByVal strOwnerID As String, _
                                                ByVal intUsrID As Integer, _
                                                ByVal intLocSubID As Integer, _
                                                ByVal TransDate As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Update Asset Movement
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  29/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(6) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDs", strAssetIDs)
            oParamSQL(1) = New SqlParameter("@intDeptID", intDeptID)
            oParamSQL(2) = New SqlParameter("@intLocationID", intLocationID)
            oParamSQL(3) = New SqlParameter("@strOwnerID", strOwnerID)
            oParamSQL(4) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(5) = New SqlParameter("@intLocSubID", intLocSubID)
            oParamSQL(6) = New SqlParameter("@TransDate", TransDate)
            fnAssetTransferUpdateMovement = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_AssetTransfer_UpdateMovementForTransdate", oParamSQL)
        Catch ex As Exception
            fnAssetTransferUpdateMovement = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAssetGetAssetRecords( _
                                            ByVal strAssetIDs As String, _
                                            ByVal StrSortName As String, _
                                            ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Selected Asset Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  29/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDs", strAssetIDs)    '' --Asset Ids format = 12^13^14^
            oParamSQL(1) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(2) = New SqlParameter("@strSortOrder", strSortOrder)

            fnAssetGetAssetRecords = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Asset_GetAssetRecords", oParamSQL)
        Catch ex As Exception
            fnAssetGetAssetRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAssetViewAuditTrail( _
                                        ByVal strAssetIDs As String, _
                                        ByVal StrSortName As String, _
                                        ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Asset Audit Trail Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  29/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDs", strAssetIDs)
            oParamSQL(1) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(2) = New SqlParameter("@strSortOrder", strSortOrder)

            fnAssetViewAuditTrail = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Asset_ViewAuditTrail", oParamSQL)
        Catch ex As Exception
            fnAssetViewAuditTrail = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAssetDeleteAsset( _
                                            ByVal strAssetIDs As String, _
                                            ByVal strLoginID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Insert Asset File 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  25/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDs", strAssetIDs)
            oParamSQL(1) = New SqlParameter("@strLoginID", strLoginID)
            fnAssetDeleteAsset = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Asset_DeleteAsset", oParamSQL)
        Catch ex As Exception
            fnAssetDeleteAsset = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAssetSearchRecord( _
            ByVal strAssetIDAMS As String, ByVal strAssetIDNFS As String, _
            ByVal strAssetType As String, ByVal intAssetCatID As Integer, ByVal intAssetCatSubID As Integer, _
            ByVal strAssetStatus As String, ByVal strPurchaseFDt As String, ByVal strPurchaseTDt As String, _
            ByVal strWarExpFDt As String, ByVal strWarExpTDt As String, _
            ByVal intDeptID As Integer, ByVal intLocID As Integer, ByVal strOwnerID As String, _
            ByVal strTempAsset As String, ByVal strLabelPrintF As String, _
            ByVal strCallFrm As String, ByVal StrSortName As String, ByVal strSortOrder As String, _
            ByVal intLocSubID As String, ByVal strCtrlItemF As String, _
            ByVal ShortDescription As String, ByVal OtherInformation As String, _
            ByVal intDeviceTypeID As Integer, ByVal strSerialNo As String, ByVal strMacAddress As String,
            ByVal strNewHostname As String, ByVal strStatus As String, ByVal strMachineModel As String
) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Seacrh Asset Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  28/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(27) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDAMS", strAssetIDAMS)       '--""=All
            oParamSQL(1) = New SqlParameter("@strAssetIDNFS", strAssetIDNFS)       '--""=All
            oParamSQL(2) = New SqlParameter("@strAssetType", strAssetType)         '--""=All, A=Fixed Asset, I=Inventory
            oParamSQL(3) = New SqlParameter("@intAssetCatID", intAssetCatID)       '--"0"=All
            oParamSQL(4) = New SqlParameter("@intAssetCatSubID", intAssetCatSubID) '--"0"=All
            oParamSQL(5) = New SqlParameter("@strAssetStatus", strAssetStatus)   '--"0"=All, (A)ctive, (I)nactive, (R)edundancy, (C)ondenmation, (D)isposed
            oParamSQL(6) = New SqlParameter("@strPurchaseFDt", strPurchaseFDt)   '--""=All
            oParamSQL(7) = New SqlParameter("@strPurchaseTDt", strPurchaseTDt)   '--""=All
            oParamSQL(8) = New SqlParameter("@strWarExpFDt", strWarExpFDt)       '--""=All
            oParamSQL(9) = New SqlParameter("@strWarExpTDt", strWarExpTDt)       '--""=All
            oParamSQL(10) = New SqlParameter("@intDeptID", intDeptID)            '--"0"=All
            oParamSQL(11) = New SqlParameter("@intLocID", intLocID)              '--"0"=All
            oParamSQL(12) = New SqlParameter("@strOwnerID", strOwnerID)          '--""=All
            oParamSQL(13) = New SqlParameter("@strTempAsset", strTempAsset)      '--""=All, "Y"=Yes, "N"=No
            oParamSQL(14) = New SqlParameter("@strLabelPrintF", strLabelPrintF)  '--""=All, "Y"=Yes, "N"=No
            oParamSQL(15) = New SqlParameter("@strCallFrm", strCallFrm)          '--(MA)Maintain Asset, (PD)Pending Disposed, (DiA)Disposed Asset, (RC)Redundancy Control, (DA)Delete Asset
            oParamSQL(16) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(17) = New SqlParameter("@strSortOrder", strSortOrder)
            oParamSQL(18) = New SqlParameter("@intLocSubID", intLocSubID)
            oParamSQL(19) = New SqlParameter("@strCtrlItemF", strCtrlItemF)
            oParamSQL(20) = New SqlParameter("@ShortDescription", ShortDescription)
            oParamSQL(21) = New SqlParameter("@OtherInformation", OtherInformation)
            oParamSQL(22) = New SqlParameter("@intDeviceTypeID", intDeviceTypeID)
            oParamSQL(23) = New SqlParameter("@strSerialNo", strSerialNo)
            oParamSQL(24) = New SqlParameter("@strMacAddress", strMacAddress)
            oParamSQL(25) = New SqlParameter("@strNewHostname", strNewHostname)
            oParamSQL(26) = New SqlParameter("@strStatus", strStatus)
            oParamSQL(27) = New SqlParameter("@strMachineModel", strMachineModel)


            fnAssetSearchRecord = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Asset_SearchRecord", oParamSQL)
        Catch ex As Exception
            fnAssetSearchRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAssetInsertRecBulkAddition( _
                    ByVal strAssetType As String, ByVal intCatID As Integer, ByVal intCatSubID As Integer, _
                    ByVal strCatAddInfoF As String, ByVal strCatAddInfoXML As String, _
                    ByVal strAssetBrand As String, ByVal strAssetDesc As String, _
                    ByVal strCost As String, ByVal strCostCenter As String, _
                    ByVal strPurchaseDt As String, ByVal strWarExpDt As String, _
                    ByVal strAssetRemark As String, ByVal strTempAsset As String, _
                    ByVal intDeptID As Integer, ByVal intLocationID As Integer, _
                    ByVal strOwnerID As String, ByVal intQuantity As Integer, ByVal intUsrID As Integer, _
                    ByRef strAssetID As String, ByRef strAssetFiles As String, _
                    ByVal intLocSubID As String, ByVal strCtrlItemF As String, _
                    ByVal strShortDescription As String, ByVal strOtherInformation As String, ByVal strAssetIdNFS As String,
                    ByVal intDeviceTypeID As Integer, ByVal strSerialNo As String, ByVal strMacAddress As String,
                    ByVal strNewHostname As String, ByVal strStatus As String, ByVal strMachineModel As String
) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To insert asset (Bulk Addtion)
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  27/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(30) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetType", strAssetType)
            oParamSQL(1) = New SqlParameter("@intCatID", intCatID)
            oParamSQL(2) = New SqlParameter("@intCatSubID", intCatSubID)
            oParamSQL(3) = New SqlParameter("@strCatAddInfoF", strCatAddInfoF)
            oParamSQL(4) = New SqlParameter("@strCatAddInfoXML", strCatAddInfoXML)
            oParamSQL(5) = New SqlParameter("@strAssetBrand", strAssetBrand)
            oParamSQL(6) = New SqlParameter("@strAssetDesc", strAssetDesc)
            oParamSQL(7) = New SqlParameter("@strCost", strCost)
            oParamSQL(8) = New SqlParameter("@strCostCenter", strCostCenter)
            oParamSQL(9) = New SqlParameter("@strPurchaseDt", strPurchaseDt)
            oParamSQL(10) = New SqlParameter("@strWarExpDt", strWarExpDt)
            oParamSQL(11) = New SqlParameter("@strAssetRemark", strAssetRemark)
            oParamSQL(12) = New SqlParameter("@strTempAsset", strTempAsset)
            oParamSQL(13) = New SqlParameter("@intDeptID", intDeptID)
            oParamSQL(14) = New SqlParameter("@intLocationID", intLocationID)
            oParamSQL(15) = New SqlParameter("@strOwnerID", strOwnerID)
            oParamSQL(16) = New SqlParameter("@intQuantity", intQuantity)
            oParamSQL(17) = New SqlParameter("@intUsrID", intUsrID)
            'oParamSQL(18) = New SqlParameter("@strAssetID", SqlDbType.NVarChar, 4000)
            oParamSQL(18) = New SqlParameter("@strAssetID", SqlDbType.VarChar, 8000)
            oParamSQL(18).Direction = ParameterDirection.Output
            oParamSQL(19) = New SqlParameter("@strAssetFiles", SqlDbType.NVarChar, 4000)
            oParamSQL(19).Direction = ParameterDirection.Output
            oParamSQL(20) = New SqlParameter("@intLocSubID", intLocSubID)
            oParamSQL(21) = New SqlParameter("@strCtrlItemF", strCtrlItemF)
            oParamSQL(22) = New SqlParameter("@strShortDescription", strShortDescription)
            oParamSQL(23) = New SqlParameter("@strOtherInformation", strOtherInformation)
            oParamSQL(24) = New SqlParameter("@strAssetIdNFS", strAssetIdNFS)

            oParamSQL(25) = New SqlParameter("@intDeviceTypeID", intDeviceTypeID)
            oParamSQL(26) = New SqlParameter("@strSerialNo", strSerialNo)
            oParamSQL(27) = New SqlParameter("@strMacAddress", strMacAddress)
            oParamSQL(28) = New SqlParameter("@strNewHostname", strNewHostname)
            oParamSQL(29) = New SqlParameter("@strStatus", strStatus)
            oParamSQL(30) = New SqlParameter("@strMachineModel", strMachineModel)

            fnAssetInsertRecBulkAddition = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Asset_InsertRecBulkAddition", oParamSQL)
            strAssetID = oParamSQL(18).Value
            strAssetFiles = oParamSQL(19).Value
        Catch ex As Exception
            fnAssetInsertRecBulkAddition = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAssetInsertRecSingleAddition( _
                ByVal strAssetType As String, ByVal intCatID As Integer, ByVal intCatSubID As Integer, _
                ByVal strCatAddInfo1 As String, ByVal strCatAddInfo2 As String, ByVal strCatAddInfo3 As String, _
                ByVal strCatAddInfo4 As String, ByVal strCatAddInfo5 As String, _
                ByVal strAssetBrand As String, ByVal strAssetDesc As String, _
                ByVal strCost As String, ByVal strCostCenter As String, _
                ByVal strPurchaseDt As String, ByVal strWarExpDt As String, _
                ByVal strAssetRemark As String, ByVal strTempAsset As String, _
                ByVal intDeptID As Integer, ByVal intLocationID As Integer, _
                ByVal strOwnerID As String, ByVal intUsrID As Integer, _
                ByRef strAssetID As String, ByVal intLocSubID As String, ByVal strCtrlItemF As String,
                ByVal strShortDescription As String, ByVal strOtherInformation As String, ByVal strAssetIdNFS As String,
                ByVal intDeviceTypeID As Integer, ByVal strSerialNo As String, ByVal strMacAddress As String,
                ByVal strNewHostname As String, ByVal strStatus As String, ByVal strMachineModel As String
    ) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To delete Asset File
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  25/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(31) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetType", strAssetType)
            oParamSQL(1) = New SqlParameter("@intCatID", intCatID)
            oParamSQL(2) = New SqlParameter("@intCatSubID", intCatSubID)
            oParamSQL(3) = New SqlParameter("@strCatAddInfo1", strCatAddInfo1)
            oParamSQL(4) = New SqlParameter("@strCatAddInfo2", strCatAddInfo2)
            oParamSQL(5) = New SqlParameter("@strCatAddInfo3", strCatAddInfo3)
            oParamSQL(6) = New SqlParameter("@strCatAddInfo4", strCatAddInfo4)
            oParamSQL(7) = New SqlParameter("@strCatAddInfo5", strCatAddInfo5)
            oParamSQL(8) = New SqlParameter("@strAssetBrand", strAssetBrand)
            oParamSQL(9) = New SqlParameter("@strAssetDesc", strAssetDesc)
            oParamSQL(10) = New SqlParameter("@strCost", strCost)
            oParamSQL(11) = New SqlParameter("@strCostCenter", strCostCenter)
            oParamSQL(12) = New SqlParameter("@strPurchaseDt", strPurchaseDt)
            oParamSQL(13) = New SqlParameter("@strWarExpDt", strWarExpDt)
            oParamSQL(14) = New SqlParameter("@strAssetRemark", strAssetRemark)
            oParamSQL(15) = New SqlParameter("@strTempAsset", strTempAsset)
            oParamSQL(16) = New SqlParameter("@intDeptID", intDeptID)
            oParamSQL(17) = New SqlParameter("@intLocationID", intLocationID)
            oParamSQL(18) = New SqlParameter("@strOwnerID", strOwnerID)
            oParamSQL(19) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(20) = New SqlParameter("@strAssetID", SqlDbType.VarChar, 20)
            oParamSQL(20).Direction = ParameterDirection.Output
            oParamSQL(21) = New SqlParameter("@intLocSubID", intLocSubID)
            oParamSQL(22) = New SqlParameter("@strCtrlItemF", strCtrlItemF)
            oParamSQL(23) = New SqlParameter("@strShortDescription", strShortDescription)
            oParamSQL(24) = New SqlParameter("@strOtherInformation", strOtherInformation)
            oParamSQL(25) = New SqlParameter("@strAssetIdNFS", strAssetIdNFS)

            oParamSQL(26) = New SqlParameter("@intDeviceTypeID", intDeviceTypeID)
            oParamSQL(27) = New SqlParameter("@strSerialNo", strSerialNo)
            oParamSQL(28) = New SqlParameter("@strMacAddress", strMacAddress)
            oParamSQL(29) = New SqlParameter("@strNewHostname", strNewHostname)
            oParamSQL(30) = New SqlParameter("@strStatus", strStatus)
            oParamSQL(31) = New SqlParameter("@strMachineModel", strMachineModel)

            fnAssetInsertRecSingleAddition = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Asset_InsertRecSingleAddition", oParamSQL)
            strAssetID = oParamSQL(20).Value
        Catch ex As Exception
            fnAssetInsertRecSingleAddition = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAssetFileGetRec( _
                                        ByVal intUsrID As Integer, _
                                        ByVal intAssetID As Integer, _
                                        ByVal strTempSaveF As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get Asset File Record
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  25/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)         'Created By (For Add Asset)
            oParamSQL(1) = New SqlParameter("@intAssetID", intAssetID)     'Asset ID (For Modify Asset)
            oParamSQL(2) = New SqlParameter("@strTempSaveF", strTempSaveF) 'Y=Temp Save, N=Not Temp Save

            fnAssetFileGetRec = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_AssetFile_GetRec", oParamSQL)
        Catch ex As Exception
            fnAssetFileGetRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAssetFileDeleteRec( _
                                                ByVal intUsrID As Integer, _
                                                ByVal intAssetFileID As Integer, _
                                                ByVal strType As String, _
                                                ByRef strFile2Delete As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To delete Asset File
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  25/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(1) = New SqlParameter("@intAssetFileID", intAssetFileID)
            oParamSQL(2) = New SqlParameter("@strType", strType)
            oParamSQL(3) = New SqlParameter("@strFile2Delete", SqlDbType.VarChar, 4000)
            oParamSQL(3).Direction = ParameterDirection.Output
            fnAssetFileDeleteRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_AssetFile_DeleteRec", oParamSQL)
            strFile2Delete = oParamSQL(3).Value
        Catch ex As Exception
            fnAssetFileDeleteRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAssetFileInsertRec( _
                                            ByVal intUsrID As Integer, _
                                            ByVal intAssetID As Integer, _
                                            ByVal strFileName As String, _
                                            ByVal strTempSaveF As String, _
                                            ByRef intFileID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Insert Asset File 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  25/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(1) = New SqlParameter("@intAssetID", intAssetID)
            oParamSQL(2) = New SqlParameter("@strFileName", strFileName)
            oParamSQL(3) = New SqlParameter("@strTempSaveF", strTempSaveF)
            oParamSQL(4) = New SqlParameter("@intAssetFileID", SqlDbType.Int, 5)
            oParamSQL(4).Direction = ParameterDirection.Output
            fnAssetFileInsertRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_AssetFile_InsertRec", oParamSQL)
            intFileID = oParamSQL(4).Value
        Catch ex As Exception
            fnAssetFileInsertRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnGetNFSID(ByVal strSearchString As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Insert Asset File 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  25/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(0) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strSearchString", strSearchString)
            fnGetNFSID = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Asset_GetNFSID", oParamSQL)

        Catch ex As Exception
            fnGetNFSID = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnSelectRFIDRecord(ByVal fromDate As Date, ByVal toDate As Date) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get contract record for asset
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  03/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@fromfld_CreatedDate", fromDate)
            oParamSQL(1) = New SqlParameter("@tofld_CreatedDate", toDate)

            fnSelectRFIDRecord = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_SelectAllRFIDRecord", oParamSQL)
        Catch ex As Exception
            fnSelectRFIDRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnSelectByfld_RFID_Stock_Take_ID(ByVal fld_RFID_Stock_Take_ID As Integer) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get contract record for asset
        'Returns		    :  Dataset
        'Author			    :  See Siew
        'Date			    :  03/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@fld_RFID_Stock_Take_ID", fld_RFID_Stock_Take_ID)

            fnSelectByfld_RFID_Stock_Take_ID = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_SubLocation_SelectByfld_RFID_Stock_Take_ID", oParamSQL)
        Catch ex As Exception
            fnSelectByfld_RFID_Stock_Take_ID = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnRFIDInsertLocation( _
                                            ByVal intAssetID As Integer, _
                                            ByVal intLocSubID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Insert Asset File 
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  25/10/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intAssetID", intAssetID)
            oParamSQL(1) = New SqlParameter("@intLocSubID", intLocSubID)
            fnRFIDInsertLocation = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_RFID_Insert_location", oParamSQL)

        Catch ex As Exception
            fnRFIDInsertLocation = Nothing
            Throw ex
        End Try
    End Function

End Class
