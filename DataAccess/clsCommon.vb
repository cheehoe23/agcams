#Region "Information Section"
' ****************************************************************************************************
' Description       : Common Class.
' Purpose           : To perform the business logic for Common Function (Login/Logout/Audit/...)
' Author			: See Siew
' Date			    : 29.01.2005
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region


Public Class clsCommon

    Public Shared Function fnDataReader_Close(ByVal objRdr As SqlDataReader)
        If (objRdr IsNot Nothing) Then
            objRdr.Close()
        End If
    End Function

    Public Shared Function fnLogin_UserKicked( _
                                       ByVal intUsrID As Integer, ByVal intLoginUsersID As Integer) As Boolean
        '****************************************************************************************************
        'Purpose  		    :  check user has been kicked out
        'Returns		    :  integer
        'Author			    :  Raptech
        'Date			    :  08/01/2010
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            Dim strUserStatus As String = ""

            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(1) = New SqlParameter("@intLoginUsersID", intLoginUsersID)
            oParamSQL(2) = New SqlParameter("@strUserStatus", strUserStatus)
            oParamSQL(2).Direction = ParameterDirection.Output

            Dim objRdr As SqlDataReader
            objRdr = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Login_CheckUserStatus", oParamSQL)

            strUserStatus = oParamSQL(2).Value
            objRdr.Close()
            If strUserStatus = "A" Then
                fnLogin_UserKicked = False
            Else
                fnLogin_UserKicked = True
            End If

        Catch ex As Exception
            fnLogin_UserKicked = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOffLocGetRecord( _
                                       ByVal strOffLocName As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To get Officer/Location record
        'Returns		    :  dataset
        'Author			    :  See Siew
        'Date			    :  10/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(0) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strOffLocName", strOffLocName)
            fnOffLocGetRecord = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_OffLoc_GetRecordByLocOffName", oParamSQL)
        Catch ex As Exception
            fnOffLocGetRecord = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnBarcodeAddRecords( _
                                    ByVal status As String,
                                    ByVal strOffLocBC As String, _
                                    ByVal strFilesBC As String, _
                                    ByVal intUsrID As Integer, _
                                    ByVal strLoginID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Add Barcode details
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  16/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strOffLocBC", strOffLocBC)
            oParamSQL(1) = New SqlParameter("@strFilesBC", strFilesBC)
            oParamSQL(2) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(3) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(4) = New SqlParameter("@status", status)

            fnBarcodeAddRecords = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_Barcode_AddRecords", oParamSQL)
        Catch ex As Exception
            fnBarcodeAddRecords = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnOffLocAddMovement( _
                                ByVal intFileDetailID As Integer, _
                                ByVal strOffLocID As Integer, _
                                ByVal strLoginID As String, _
                                ByVal intUsrID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To add new movement for volume
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  10/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intFileDetailID", intFileDetailID)
            oParamSQL(1) = New SqlParameter("@strOffLocID", strOffLocID)
            oParamSQL(2) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(3) = New SqlParameter("@intUsrID", intUsrID)

            fnOffLocAddMovement = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_OffLoc_AddMovement", oParamSQL)
        Catch ex As Exception
            fnOffLocAddMovement = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnBarcodeAddRecordsForPortableScan( _
                                ByVal strOffLocBC As String, _
                                ByVal strFilesBC As String, _
                                ByVal strMvDt As String, _
                                ByVal intUsrID As Integer, _
                                ByVal strLoginID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Add Barcode details
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  16/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strOffLocBC", strOffLocBC)
            oParamSQL(1) = New SqlParameter("@strFilesBC", strFilesBC)
            oParamSQL(2) = New SqlParameter("@strMvDt", strMvDt)
            oParamSQL(3) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(4) = New SqlParameter("@strLoginID", strLoginID)

            fnBarcodeAddRecordsForPortableScan = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_Barcode_AddRecordsForPortableScan", oParamSQL)
        Catch ex As Exception
            fnBarcodeAddRecordsForPortableScan = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnRFID_SmartShelfLight( _
                        ByVal strAssetIDAMS As String, ByVal intLocSubID As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Sub Location details for light
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  16/03/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAssetIDAMS", strAssetIDAMS)
            oParamSQL(1) = New SqlParameter("@intLocSubID", intLocSubID)

            fnRFID_SmartShelfLight = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_RFID_SmartShelfLight", oParamSQL)
        Catch ex As Exception
            fnRFID_SmartShelfLight = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnRFIDLabel_GetAssetDetails( _
                                        ByVal strAssetIDs As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Asset Details for RFID Label  
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  09/03/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strAssetIDs", strAssetIDs)

            fnRFIDLabel_GetAssetDetails = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_RFIDLabel_GetAssetDetails", oParamSQL)
        Catch ex As Exception
            fnRFIDLabel_GetAssetDetails = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnRFIDLabel_GetFileDetails( _
                                        ByVal strFileDetailIDs As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Asset Details for RFID Label  
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  09/03/2009
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@strFileDetailIDs", strFileDetailIDs)

            fnRFIDLabel_GetFileDetails = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_RFIDLabel_GetFileDetails", oParamSQL)
        Catch ex As Exception
            fnRFIDLabel_GetFileDetails = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnMenu_CountRec4Information( _
                                ByVal strAdminF As String, _
                                ByVal intUsrID As Integer, _
                                ByVal strGetRecType As String, _
                                ByRef intTotalMissOwner As Integer, _
                                ByRef intTotalPendCondemn As Integer, _
                                ByRef intTotalNFSToday As Integer, _
                                ByRef intTotalPendReturn As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To get total asset which is missing owner and pending for condemnation
        'Returns		    :  Integer
        'Author			    :  See Siew
        'Date			    :  01/11/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(6) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strAdminF", strAdminF)
            oParamSQL(1) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(2) = New SqlParameter("@strGetRecType", strGetRecType)
            oParamSQL(3) = New SqlParameter("@intTotalMissOwner", SqlDbType.Int, 10)
            oParamSQL(3).Direction = ParameterDirection.Output
            oParamSQL(4) = New SqlParameter("@intTotalPendCondemn", SqlDbType.Int, 10)
            oParamSQL(4).Direction = ParameterDirection.Output
            oParamSQL(5) = New SqlParameter("@intTotalNFSToday", SqlDbType.Int, 10)
            oParamSQL(5).Direction = ParameterDirection.Output
            oParamSQL(6) = New SqlParameter("@intTotalPendReturn", SqlDbType.Int, 10)
            oParamSQL(6).Direction = ParameterDirection.Output

            fnMenu_CountRec4Information = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Menu_CountRec4Information", oParamSQL)
            intTotalMissOwner = oParamSQL(3).Value
            intTotalPendCondemn = oParamSQL(4).Value
            intTotalNFSToday = oParamSQL(5).Value
            intTotalPendReturn = oParamSQL(6).Value
        Catch ex As Exception
            fnMenu_CountRec4Information = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLogout_UpdateUser( _
                                    ByVal intUsrID As Integer) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Update Logout Time
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  17/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@intUsrID", intUsrID)

            fnLogout_UpdateUser = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Logout_UpdateUser", oParamSQL)
        Catch ex As Exception
            fnLogout_UpdateUser = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnMenuGetModuleFunction( _
                                    ByVal strModVal As String, _
                                    ByVal strAccess As String, _
                                    ByVal strType As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Module and Sub-Module for Menu   
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  14/03/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strModVal", strModVal)
            oParamSQL(1) = New SqlParameter("@strAccess", strAccess)
            oParamSQL(2) = New SqlParameter("@strType", strType)
            fnMenuGetModuleFunction = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_Menu_GetModuleFunction", oParamSQL)
        Catch ex As Exception
            fnMenuGetModuleFunction = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAuditViewAuditTrail( _
                                    ByVal intUsrID As Integer, _
                                    ByVal StrSortName As String, _
                                    ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To get audit trail records
        'Returns		    :  dataset
        'Author			    :  See Siew
        'Date			    :  06/02/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(1) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(2) = New SqlParameter("@strSortOrder", strSortOrder)
            fnAuditViewAuditTrail = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_Audit_ViewAuditTrail", oParamSQL)
        Catch ex As Exception
            fnAuditViewAuditTrail = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnGetByfld_locSubID( _
                                    ByVal fld_LocSubID As Integer) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To get audit trail records
        'Returns		    :  dataset
        'Author			    :  See Siew
        'Date			    :  06/02/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(0) As SqlParameter
            oParamSQL(0) = New SqlParameter("@fld_LocSubID", fld_LocSubID)

            fnGetByfld_locSubID = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_tbl_LocationSub_GetByfld_locSubID", oParamSQL)
        Catch ex As Exception
            fnGetByfld_locSubID = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnLoginValidation( _
                                    ByVal strLoginID As String, _
                                    ByVal strPwd As String, _
                                    ByRef intUsrId As Integer, _
                                    ByRef strUsrName As String, _
                                    ByRef strAccess As String, _
                                    ByRef strLSuccessF As String, _
                                    ByRef strAdminF As String, _
                                    ByRef intLoginUsrID As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  Login Validation and Information
        'Returns		    :  output value
        'Author			    :  See Siew
        'Date			    :  04/11/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(7) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(1) = New SqlParameter("@strPwd", strPwd)
            oParamSQL(2) = New SqlParameter("@intUsrId", SqlDbType.Int, 4)
            oParamSQL(2).Direction = ParameterDirection.Output
            oParamSQL(3) = New SqlParameter("@strUsrName", SqlDbType.VarChar, 255)
            oParamSQL(3).Direction = ParameterDirection.Output
            oParamSQL(4) = New SqlParameter("@strAccess", SqlDbType.VarChar, 8000)
            oParamSQL(4).Direction = ParameterDirection.Output                      '--output: Y=Login Success, N=Login Failed
            oParamSQL(5) = New SqlParameter("@strLSuccessF", SqlDbType.Char, 1)
            oParamSQL(5).Direction = ParameterDirection.Output
            oParamSQL(6) = New SqlParameter("@strAdminF", SqlDbType.Char, 1)
            oParamSQL(6).Direction = ParameterDirection.Output
            oParamSQL(7) = New SqlParameter("@intLoginUsrID", SqlDbType.Int)
            oParamSQL(7).Direction = ParameterDirection.Output

            fnLoginValidation = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_LoginChecking", oParamSQL)

            ''Get output value
            intUsrId = oParamSQL(2).Value
            strUsrName = oParamSQL(3).Value
            strAccess = oParamSQL(4).Value
            strLSuccessF = oParamSQL(5).Value
            strAdminF = oParamSQL(6).Value
            intLoginUsrID = oParamSQL(7).Value
        Catch ex As Exception
            fnLoginValidation = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAuditInsertRec( _
                                ByVal intUsrID As Integer, _
                                ByVal strAssetID As String, _
                                ByVal strAuditAction As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To insert audit trail records
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  29/01/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(4) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(1) = New SqlParameter("@strAssetID", strAssetID)
            oParamSQL(2) = New SqlParameter("@strAuditAction", strAuditAction)
            oParamSQL(3) = New SqlParameter("@intAssetMvID", "0")
            oParamSQL(4) = New SqlParameter("@intStatusInfoID", "0")
            fnAuditInsertRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_Audit_InsertRec", oParamSQL)
        Catch ex As Exception
            fnAuditInsertRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnAuditInsertRec( _
                                ByVal intUsrID As Integer, _
                                ByVal strAuditAction As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To insert audit trail records
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  29/01/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intUsrID", intUsrID)
            oParamSQL(1) = New SqlParameter("@strAuditAction", strAuditAction)
            fnAuditInsertRec = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_Audit_InsertRec", oParamSQL)
        Catch ex As Exception
            fnAuditInsertRec = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function FTSfnMenuGetModuleFunction( _
                                    ByVal strModVal As String, _
                                    ByVal strAccess As String, _
                                    ByVal strType As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get Module and Sub-Module for Menu   
        'Returns		    :  SqlDataReader
        'Author			    :  See Siew
        'Date			    :  14/03/2007
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strModVal", strModVal)
            oParamSQL(1) = New SqlParameter("@strAccess", strAccess)
            oParamSQL(2) = New SqlParameter("@strType", strType)
            FTSfnMenuGetModuleFunction = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_Menu_GetModuleFunction", oParamSQL)
        Catch ex As Exception
            FTSfnMenuGetModuleFunction = Nothing

            Throw ex
        End Try
    End Function

    Public Shared Function fnArrayFileDetailBarCodes( _
                                    ByVal ArrayFileDetailBarCodes As String
                                    ) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get File Record for Modification Screen
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  08/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(0) As SqlParameter
            oParamSQL(0) = New SqlParameter("@ArrayFileDetailBarCodes", ArrayFileDetailBarCodes)

            fnArrayFileDetailBarCodes = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_Barcode_GetStatusForFileDetail", oParamSQL)
        Catch ex As Exception
            fnArrayFileDetailBarCodes = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnGetDataByfld_LocationOfficerBarcodeID( _
                                    ByVal fld_LocationOfficerBarcodeID As String _
                                    ) As DataSet

        Try
            Dim oParamSQL(0) As SqlParameter
            oParamSQL(0) = New SqlParameter("@fld_LocationOfficerBarcodeID", fld_LocationOfficerBarcodeID)

            fnGetDataByfld_LocationOfficerBarcodeID = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_LocationOfficer_GetDataByfld_LocationOfficerBarcodeID", oParamSQL)
        Catch ex As Exception
            fnGetDataByfld_LocationOfficerBarcodeID = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnGetStatusForFileDetail( _
                                    ByVal OfficerOrLocationRFIDCode As String
                                    ) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get File Record for Modification Screen
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  08/04/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(0) As SqlParameter
            oParamSQL(0) = New SqlParameter("@OfficerOrLocationRFIDCode", OfficerOrLocationRFIDCode)

            fnGetStatusForFileDetail = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_Barcode_GetStatusForOfficerOrLocation", oParamSQL)
        Catch ex As Exception
            fnGetStatusForFileDetail = Nothing
            Throw ex
        End Try
    End Function

End Class
