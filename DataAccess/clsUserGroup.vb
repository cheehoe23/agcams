#Region "Information Section"
' ****************************************************************************************************
' Description       : User Group Class.
' Purpose           : To perform the business logic for User Group (such as view, add, delete, edit)
' Author			: See Siew
' Date			    : 22.01.2005
' ****************************************************************************************************
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data
Imports System.Data.SqlClient
#End Region

Public Class clsUserGroup
    Public Shared Function fnUsrGrpGetAllUsrGrpRdr() As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To Get All User Group Records which is Active  
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  23/01/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@StrSortName", "fld_groupName")
            oParamSQL(1) = New SqlParameter("@strSortOrder", "ASC")
            fnUsrGrpGetAllUsrGrpRdr = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_UsrGrp_GetUsrGrpRec", oParamSQL)
        Catch ex As Exception
            fnUsrGrpGetAllUsrGrpRdr = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnUsrGrpGetAllUsrGrp( _
                                                ByVal StrSortName As String, _
                                                ByVal strSortOrder As String) As DataSet
        '****************************************************************************************************
        'Purpose  		    :  To Get All User Group Records which is Active  
        'Returns		    :  DataSet
        'Author			    :  See Siew
        'Date			    :  23/01/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(1) As SqlParameter
            oParamSQL(0) = New SqlParameter("@StrSortName", StrSortName)
            oParamSQL(1) = New SqlParameter("@strSortOrder", strSortOrder)
            fnUsrGrpGetAllUsrGrp = SqlDataAccess.clsDataAccess.ExecuteDataset(cnFTSDBString, CommandType.StoredProcedure, "P_UsrGrp_GetUsrGrpRec", oParamSQL)
        Catch ex As Exception
            fnUsrGrpGetAllUsrGrp = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnUsrGrpDuplicateUsrGrp( _
                                            ByVal intGrpId As Integer, _
                                            ByVal strGrpName As String, _
                                            ByVal strType As String) As Boolean
        '****************************************************************************************************
        'Purpose  		    :  To check for duplicate User Group name 
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  22/01/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intGrpID", intGrpId)
            oParamSQL(1) = New SqlParameter("@strGrpName", strGrpName)
            oParamSQL(2) = New SqlParameter("@strType", strType)
            oParamSQL(3) = New SqlParameter("@strRetVal", SqlDbType.VarChar, 10)
            oParamSQL(3).Direction = ParameterDirection.Output
            SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_UsrGrp_DuplicateUsrGrp", oParamSQL)
            If oParamSQL(3).Value = "Y" Then  'Duplicate Records
                fnUsrGrpDuplicateUsrGrp = True
            Else                                  'Not Duplicate Records
                fnUsrGrpDuplicateUsrGrp = False
            End If

        Catch ex As Exception
            fnUsrGrpDuplicateUsrGrp = Nothing
            Throw ex
        End Try
    End Function


    Public Shared Function fnUsrGrpInsertUpdateDelete( _
                                        ByVal intGrpId As Integer, _
                                        ByVal strGrpName As String, _
                                        ByVal strAdminF As String, _
                                        ByVal strAccess As String, _
                                        ByVal strGrpIds As String, _
                                        ByVal strLoginID As String, _
                                        ByVal strType As String) As Integer
        '****************************************************************************************************
        'Purpose  		    :  To Add, Edit, Delete particular User Group
        'Returns		    :  integer
        'Author			    :  See Siew
        'Date			    :  22/01/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(6) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intGrpID", intGrpId)
            oParamSQL(1) = New SqlParameter("@strGrpName", strGrpName)
            oParamSQL(2) = New SqlParameter("@strAdminF", strAdminF)
            oParamSQL(3) = New SqlParameter("@strAccess", strAccess)
            oParamSQL(4) = New SqlParameter("@strGrpIDs", strGrpIds)
            oParamSQL(5) = New SqlParameter("@strLoginID", strLoginID)
            oParamSQL(6) = New SqlParameter("@strType", strType)
            fnUsrGrpInsertUpdateDelete = SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBString, CommandType.StoredProcedure, "P_UsrGrp_InsertUpdateDelete", oParamSQL)
        Catch ex As Exception
            fnUsrGrpInsertUpdateDelete = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnUsrGrpGetAccessRight( _
                                    ByVal UsrGrpId As Integer) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To get Access Right for particular User Group
        'Returns		    :  SQL Reader with data
        'Author			    :  See Siew
        'Date			    :  22/01/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL As SqlParameter
            oParamSQL = New SqlParameter("@UsrGrpId", UsrGrpId)
            fnUsrGrpGetAccessRight = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_UsrGrp_GetAccessRight", oParamSQL)
        Catch ex As Exception
            fnUsrGrpGetAccessRight = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnUsrGrpGetModuleFunction( _
                                    ByVal strModVal As String, _
                                    ByVal strSubModVal As String, _
                                    ByVal strType As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To get Module, Sub-module, function for User Group
        'Returns		    :  SQL Reader with data
        'Author			    :  See Siew
        'Date			    :  22/01/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strModVal", strModVal)
            oParamSQL(1) = New SqlParameter("@strSubModVal", strSubModVal)
            oParamSQL(2) = New SqlParameter("@strType", strType)
            fnUsrGrpGetModuleFunction = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.StoredProcedure, "P_UsrGrp_GetModuleFunction", oParamSQL)
        Catch ex As Exception
            fnUsrGrpGetModuleFunction = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnUsrGrpGetModuleForFTSFunction( _
                                    ByVal strModVal As String, _
                                    ByVal strSubModVal As String, _
                                    ByVal strType As String) As SqlDataReader
        '****************************************************************************************************
        'Purpose  		    :  To get Module, Sub-module, function for User Group
        'Returns		    :  SQL Reader with data
        'Author			    :  See Siew
        'Date			    :  22/01/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(2) As SqlParameter
            oParamSQL(0) = New SqlParameter("@strModVal", strModVal)
            oParamSQL(1) = New SqlParameter("@strSubModVal", strSubModVal)
            oParamSQL(2) = New SqlParameter("@strType", strType)
            fnUsrGrpGetModuleForFTSFunction = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_UsrGrp_GetModuleFunction", oParamSQL)
        Catch ex As Exception
            fnUsrGrpGetModuleForFTSFunction = Nothing
            Throw ex
        End Try
    End Function

    Public Shared Function fnUsrGrpDuplicateUsrGrpForFTS( _
                                            ByVal intGrpId As Integer, _
                                            ByVal strGrpName As String, _
                                            ByVal strType As String) As Boolean
        '****************************************************************************************************
        'Purpose  		    :  To check for duplicate User Group name 
        'Returns		    :  Boolean
        'Author			    :  See Siew
        'Date			    :  22/01/2006
        '****************************************************************************************************
        Try
            Dim oParamSQL(3) As SqlParameter
            oParamSQL(0) = New SqlParameter("@intGrpID", intGrpId)
            oParamSQL(1) = New SqlParameter("@strGrpName", strGrpName)
            oParamSQL(2) = New SqlParameter("@strType", strType)
            oParamSQL(3) = New SqlParameter("@strRetVal", SqlDbType.VarChar, 10)
            oParamSQL(3).Direction = ParameterDirection.Output
            SqlDataAccess.clsDataAccess.ExecuteNonQuery(cnFTSDBStringForFTS, CommandType.StoredProcedure, "P_UsrGrp_DuplicateUsrGrp", oParamSQL)
            If oParamSQL(3).Value = "Y" Then  'Duplicate Records
                fnUsrGrpDuplicateUsrGrpForFTS = True
            Else                                  'Not Duplicate Records
                fnUsrGrpDuplicateUsrGrpForFTS = False
            End If

        Catch ex As Exception
            fnUsrGrpDuplicateUsrGrpForFTS = Nothing
            Throw ex
        End Try
    End Function

End Class


