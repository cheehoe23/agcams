Public Partial Class getFolder
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Len(Session("UsrID")) = 0 Then
            Dim strJavaScript As String = ""
            strJavaScript = "<script language = 'Javascript'>" & _
                            "parent.location.href='../common/logout.aspx';" & _
                            "</script>"
            Response.Write(strJavaScript)
            Exit Sub
        End If

        Response.Write("&hLink=" & gAssetPath())
        Response.Write("&aLink=" & gCategoryPath())

    End Sub

End Class