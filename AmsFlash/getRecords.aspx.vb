#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml

#End Region

Partial Public Class getReconds
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Len(Session("UsrID")) = 0 Then
            Dim strJavaScript As String = ""
            strJavaScript = "<script language = 'Javascript'>" & _
                            "parent.location.href='../common/logout.aspx';" & _
                            "</script>"
            Response.Write(strJavaScript)
            Exit Sub
        End If

        Response.Write(GetXML())

        Response.ContentType = "text/xml"

    End Sub


    Private Function GetXML() As String

        Dim strSubDXML As String
        Dim Xdoc As XmlDocument
        Xdoc = New XmlDocument

        Dim dsInjury As New DataSet
        dsInjury = New DataSet

        dsInjury = clsFlash.fnGetRFID()

        If Not dsInjury.Tables(0) Is Nothing Then
            If Not dsInjury.Tables(0).Rows.Count() = "0" Then
                strSubDXML = dsInjury.GetXml
                Xdoc.LoadXml(strSubDXML)
            End If

        End If

        Return strSubDXML
    End Function

End Class