#Region "Information Section"
' ****************************************************************************************************
' Description       : Add Asset 
' Purpose           : Add Asset Information
' Date              : 26/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Services

#End Region

Partial Public Class asset_add
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********
            Me.Header.DataBind()

            'tr1.Visible = False

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            ''Get Owner (in-case page load again)
            'cbOwner.DataSource = fnGetUserList(cbOwner.List.PageSize, "", "")

            If Not Page.IsPostBack Then
                hdnAddType.Value = Request("AddType")
                hdnQuantity.Value = Request("Quantity")
                hdnCurrentDate.Value = mfnGetCurrentDate()
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")
                butAddImages.Attributes.Add("OnClick", "return mfnOpenAddImagesWindow()")

                fnDDLDeviceType()

                ''** Populate Control
                fnPopulateCtrl()

                ''** Set Default 
                hdnAddSuccessF.Value = "N"
                'rdAssetType.SelectedValue = "I"
                'rdAssetType_SelectedIndexChanged(Nothing, Nothing)
                ddlLocation.SelectedValue = "1"
                ddlLocation_SelectedIndexChanged(Nothing, Nothing)
                hdnAddFileF.Value = "N"
                If hdnAddType.Value = "B" Then
                    fnCreateDataTableForSubCatAddInfo()
                    fnAddSubCatAddinfoRow()
                End If
                fnDefaultSubcategoryAddInfo() '' set default for subcategory additional information
                hdnAssetIds.Value = ""

                ''** Check access Right
                fnCheckAccessRight()
            End If

            '' Check "Add New File Images"
            If hdnAddFileF.Value = "Y" Then
                fnPopulateFileImages()

                ''set back to default
                hdnAddFileF.Value = "N"
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub fnCheckAccessRight()
        Try  ''AssetMng|AddAsset|AddAsset,AssetMng|AddAsset|AddInvety, AssetMng|AddAsset|AddLInvtry

            ''**Remove Asset Type, if access right for creation particular asset types are not given
            If (InStr(Session("AR"), "AssetMng|AddAsset|AddAsset") = 0) Then
                rdAssetType.Items.Remove(rdAssetType.Items.FindByValue("A"))
            End If

            If (InStr(Session("AR"), "AssetMng|AddAsset|AddInvety") = 0) Then
                rdAssetType.Items.Remove(rdAssetType.Items.FindByValue("I"))
            End If

            If (InStr(Session("AR"), "AssetMng|AddAsset|AddLInvtry") = 0) Then
                rdAssetType.Items.Remove(rdAssetType.Items.FindByValue("L"))
            End If

            If (InStr(Session("AR"), "AssetMng|AddAsset|AddLinkAsset") = 0) Then
                rdAssetType.Items.Remove(rdAssetType.Items.FindByValue("K"))
            End If

            ''**Default selection for "Asset Type" 
            ''---> 1st Choose "Inventory"
            ''---> 2nd Choose "Leased Inventory"
            ''---> 3rd Choose "Fixed Asset"
            If (InStr(Session("AR"), "AssetMng|AddAsset|AddInvety") > 0) Then
                rdAssetType.SelectedValue = "I"
               
            ElseIf (InStr(Session("AR"), "AssetMng|AddAsset|AddLInvtry") > 0) Then
                rdAssetType.SelectedValue = "L"
                
            ElseIf (InStr(Session("AR"), "AssetMng|AddAsset|AddAsset") > 0) Then
                rdAssetType.SelectedValue = "A"
               
            ElseIf (InStr(Session("AR"), "AssetMng|AddAsset|AddLinkAsset") > 0) Then
                rdAssetType.SelectedValue = "K"
            End If
            rdAssetType_SelectedIndexChanged(Nothing, Nothing)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Asset Type --> Fixed Asset, Inventory, Leased Inventory
            fnPopulateAssetTypeInRD(rdAssetType)

            ''Get Category
            fnPopulateDropDownList(clsCategory.fnCategoryGetCategory(), ddlAssetCat, "fld_CategoryID", "fld_CatCodeName", False)

            ''Get SubCategory (set default --> -1)
            ddlAssetSubCat.Items.Insert(0, New ListItem("", "-1"))

            ''Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False)
            If Session("AdminF") = "N" Then ddlDepartment.Items.Remove(ddlDepartment.Items.FindByValue("-1"))

            ''Get Location
            fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLocation, "fld_LocationID", "fld_LoctCodeName", False)

            ''Get Sub Location 
            ddlLocSub.Items.Insert(0, New ListItem("", "-1"))

            ' ''Get Owner
            'cbOwner.DataSource = fnGetUserList(cbOwner.List.PageSize, "", "")


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnDDLDeviceType()
        Try

            Dim dt As DataTable = clsDeviceType.fnDeviceTypeGetAllRec("fld_DeviceType", "ASC").Tables(0)
            If (dt.Rows.Count > 0) Then
                ddlDeviceType.DataSource = dt
                ddlDeviceType.DataTextField = "fld_DeviceType"
                ddlDeviceType.DataValueField = "fld_DeviceTypeID"
                ddlDeviceType.DataBind()
                ddlDeviceType.Items.Insert(0, New ListItem("", "-1"))
            End If

        Catch ex As Exception

        End Try
    End Sub

    Public Sub fnDefaultSubcategoryAddInfo()
        Try
            '' set default for subcategory additional information
            hdnSCatAddInfo1.Value = ""
            hdnSCatAddInfo2.Value = ""
            hdnSCatAddInfo3.Value = ""
            hdnSCatAddInfo4.Value = ""
            hdnSCatAddInfo5.Value = ""
            hdnSCatAddInfoMO1.Value = ""
            hdnSCatAddInfoMO2.Value = ""
            hdnSCatAddInfoMO3.Value = ""
            hdnSCatAddInfoMO4.Value = ""
            hdnSCatAddInfoMO5.Value = ""
            divAddInfo1.Visible = False
            lblAddInfo1.Text = ""
            txtAddInfo1.Text = ""
            divAddInfo2.Visible = False
            lblAddInfo2.Text = ""
            txtAddInfo2.Text = ""
            divAddInfo3.Visible = False
            lblAddInfo3.Text = ""
            txtAddInfo3.Text = ""
            divAddInfo4.Visible = False
            lblAddInfo4.Text = ""
            txtAddInfo4.Text = ""
            divAddInfo5.Visible = False
            lblAddInfo5.Text = ""
            txtAddInfo5.Text = ""
            divAddInfoB.Visible = False
            dgSubCatAddInfo.Columns(1).Visible = False
            dgSubCatAddInfo.Columns(2).Visible = False
            dgSubCatAddInfo.Columns(3).Visible = False
            dgSubCatAddInfo.Columns(4).Visible = False
            dgSubCatAddInfo.Columns(5).Visible = False
            BindSubCatAddInfoData() ''Bind again the Datatgrid 
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ddlAssetCat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAssetCat.SelectedIndexChanged
        Try
            If ddlAssetCat.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsCategory.fnCategoryGetAllRecDR(ddlAssetCat.SelectedValue, 0, "fld_CatSubName", "ASC"), ddlAssetSubCat, "fld_CatSubID", "fld_CatSubName", False)
            Else
                ddlAssetSubCat.Items.Clear()
                ddlAssetSubCat.Items.Insert(0, New ListItem("", "-1"))
            End If

            '' set default for subcategory additional information
            fnDefaultSubcategoryAddInfo()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub ddlAssetSubCat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAssetSubCat.SelectedIndexChanged
        Dim objRdr As SqlDataReader
        Try
            '' set default for subcategory additional information
            fnDefaultSubcategoryAddInfo()
            ''  If subcategory seleceted... get additional information
            If ddlAssetSubCat.SelectedValue <> "-1" Then

                objRdr = clsCategory.fnCategoryGetAllRecDR(0, ddlAssetSubCat.SelectedValue, "fld_CatSubID", "ASC")
                If Not objRdr Is Nothing Then
                    If objRdr.HasRows Then
                        objRdr.Read()

                        ''Check whether have subcategory
                        If CStr(objRdr("fld_CatSubAddInfo1")) <> "" Then '' Got subcategory
                            hdnSCatAddInfo1.Value = Trim(CStr(objRdr("fld_CatSubAddInfo1")))
                            hdnSCatAddInfo2.Value = Trim(CStr(objRdr("fld_CatSubAddInfo2")))
                            hdnSCatAddInfo3.Value = Trim(CStr(objRdr("fld_CatSubAddInfo3")))
                            hdnSCatAddInfo4.Value = Trim(CStr(objRdr("fld_CatSubAddInfo4")))
                            hdnSCatAddInfo5.Value = Trim(CStr(objRdr("fld_CatSubAddInfo5")))
                            hdnSCatAddInfoMO1.Value = CStr(objRdr("fld_CatSubAddInfoM1"))
                            hdnSCatAddInfoMO2.Value = CStr(objRdr("fld_CatSubAddInfoM2"))
                            hdnSCatAddInfoMO3.Value = CStr(objRdr("fld_CatSubAddInfoM3"))
                            hdnSCatAddInfoMO4.Value = CStr(objRdr("fld_CatSubAddInfoM4"))
                            hdnSCatAddInfoMO5.Value = CStr(objRdr("fld_CatSubAddInfoM5"))

                            If hdnAddType.Value = "S" Then ''(S) Single Addition
                                DisplayAddInfoTB4SubcategorySingle()
                            Else ''(B) Bulk Addtion
                                DisplayAddInfoTB4SubcategoryBulkAdd()
                            End If
                        End If
                    End If
                End If
                objRdr.Close()
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
            clsCommon.fnDataReader_Close(objRdr)
        End Try
    End Sub

    Public Sub DisplayAddInfoTB4SubcategorySingle()
        Try
            Dim strOutput As String = ""
            Dim intCurrentAddInfo As Integer = 0
            Dim strCurrentAddInfoName As String = ""
            Dim strCurentAddInfoMO As String = ""

            For intCurrentAddInfo = 1 To 5
                strCurrentAddInfoName = ""
                strCurentAddInfoMO = ""

                Select Case intCurrentAddInfo
                    Case "1"
                        strCurrentAddInfoName = Trim(hdnSCatAddInfo1.Value)
                        strCurentAddInfoMO = IIf(hdnSCatAddInfoMO1.Value = "Y", "<font color='red'>*</font>", "")
                        If strCurrentAddInfoName <> "" Then
                            divAddInfo1.Visible = True
                            lblAddInfo1.Text = strCurentAddInfoMO + strCurrentAddInfoName
                        End If
                    Case "2"
                        strCurrentAddInfoName = Trim(hdnSCatAddInfo2.Value)
                        strCurentAddInfoMO = IIf(hdnSCatAddInfoMO2.Value = "Y", "<font color='red'>*</font>", "")
                        If strCurrentAddInfoName <> "" Then
                            divAddInfo2.Visible = True
                            lblAddInfo2.Text = strCurentAddInfoMO + strCurrentAddInfoName
                        End If
                    Case "3"
                        strCurrentAddInfoName = Trim(hdnSCatAddInfo3.Value)
                        strCurentAddInfoMO = IIf(hdnSCatAddInfoMO3.Value = "Y", "<font color='red'>*</font>", "")
                        If strCurrentAddInfoName <> "" Then
                            divAddInfo3.Visible = True
                            lblAddInfo3.Text = strCurentAddInfoMO + strCurrentAddInfoName
                        End If
                    Case "4"
                        strCurrentAddInfoName = Trim(hdnSCatAddInfo4.Value)
                        strCurentAddInfoMO = IIf(hdnSCatAddInfoMO4.Value = "Y", "<font color='red'>*</font>", "")
                        If strCurrentAddInfoName <> "" Then
                            divAddInfo4.Visible = True
                            lblAddInfo4.Text = strCurentAddInfoMO + strCurrentAddInfoName
                        End If
                    Case "5"
                        strCurrentAddInfoName = Trim(hdnSCatAddInfo5.Value)
                        strCurentAddInfoMO = IIf(hdnSCatAddInfoMO5.Value = "Y", "<font color='red'>*</font>", "")
                        If strCurrentAddInfoName <> "" Then
                            divAddInfo5.Visible = True
                            lblAddInfo5.Text = strCurentAddInfoMO + strCurrentAddInfoName
                        End If
                End Select

                If strCurrentAddInfoName = "" Then
                    Exit For
                End If
            Next
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub DisplayAddInfoTB4SubcategoryBulkAdd()
        Try
            Dim intCurrentAddInfo As Integer = 0
            Dim strCurrentAddInfoName As String = ""
            Dim strCurentAddInfoMO As String = ""

            ''*** Start: Addition Info Part
            '*1. Get Header
            For intCurrentAddInfo = 1 To 5
                strCurrentAddInfoName = ""
                strCurentAddInfoMO = ""

                Select Case intCurrentAddInfo
                    Case "1"
                        strCurrentAddInfoName = Trim(hdnSCatAddInfo1.Value)
                        strCurentAddInfoMO = IIf(hdnSCatAddInfoMO1.Value = "Y", "<font color='red'>*</font>", "")
                        If strCurrentAddInfoName <> "" Then
                            divAddInfoB.Visible = True
                            dgSubCatAddInfo.Columns(1).HeaderText = strCurentAddInfoMO + strCurrentAddInfoName
                            dgSubCatAddInfo.Columns(1).Visible = True
                        End If

                    Case "2"
                        strCurrentAddInfoName = Trim(hdnSCatAddInfo2.Value)
                        strCurentAddInfoMO = IIf(hdnSCatAddInfoMO2.Value = "Y", "<font color='red'>*</font>", "")
                        If strCurrentAddInfoName <> "" Then
                            dgSubCatAddInfo.Columns(2).HeaderText = strCurentAddInfoMO + strCurrentAddInfoName
                            dgSubCatAddInfo.Columns(2).Visible = True
                        End If
                    Case "3"
                        strCurrentAddInfoName = Trim(hdnSCatAddInfo3.Value)
                        strCurentAddInfoMO = IIf(hdnSCatAddInfoMO3.Value = "Y", "<font color='red'>*</font>", "")
                        If strCurrentAddInfoName <> "" Then
                            dgSubCatAddInfo.Columns(3).HeaderText = strCurentAddInfoMO + strCurrentAddInfoName
                            dgSubCatAddInfo.Columns(3).Visible = True
                        End If
                    Case "4"
                        strCurrentAddInfoName = Trim(hdnSCatAddInfo4.Value)
                        strCurentAddInfoMO = IIf(hdnSCatAddInfoMO4.Value = "Y", "<font color='red'>*</font>", "")
                        If strCurrentAddInfoName <> "" Then
                            dgSubCatAddInfo.Columns(4).HeaderText = strCurentAddInfoMO + strCurrentAddInfoName
                            dgSubCatAddInfo.Columns(4).Visible = True
                        End If
                    Case "5"
                        strCurrentAddInfoName = Trim(hdnSCatAddInfo5.Value)
                        strCurentAddInfoMO = IIf(hdnSCatAddInfoMO5.Value = "Y", "<font color='red'>*</font>", "")
                        If strCurrentAddInfoName <> "" Then
                            dgSubCatAddInfo.Columns(5).HeaderText = strCurentAddInfoMO + strCurrentAddInfoName
                            dgSubCatAddInfo.Columns(5).Visible = True
                        End If
                End Select

                If strCurrentAddInfoName = "" Then
                    Exit For
                End If
            Next

            ''Bind again the Datatgrid 
            BindSubCatAddInfoData()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnCreateDataTableForSubCatAddInfo()
        Try
            ''creating a table named TblAddInfo
            Dim TblAddInfo As DataTable
            TblAddInfo = New DataTable("TblAddInfo")

            ''Column 1: S/No
            Dim SNum As DataColumn = New DataColumn("SNum")    'declaring a column named Name
            SNum.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            TblAddInfo.Columns.Add(SNum)                               'adding the column to table

            ''Column 2: Additional Information 1
            Dim AddInfo1 As DataColumn = New DataColumn("AddInfo1")
            AddInfo1.DataType = System.Type.GetType("System.String")
            TblAddInfo.Columns.Add(AddInfo1)

            ''Column 3: Additional Information 1
            Dim AddInfo2 As DataColumn = New DataColumn("AddInfo2")
            AddInfo2.DataType = System.Type.GetType("System.String")
            TblAddInfo.Columns.Add(AddInfo2)

            ''Column 4: Additional Information 1
            Dim AddInfo3 As DataColumn = New DataColumn("AddInfo3")
            AddInfo3.DataType = System.Type.GetType("System.String")
            TblAddInfo.Columns.Add(AddInfo3)

            ''Column 5: Additional Information 1
            Dim AddInfo4 As DataColumn = New DataColumn("AddInfo4")
            AddInfo4.DataType = System.Type.GetType("System.String")
            TblAddInfo.Columns.Add(AddInfo4)

            ''Column 6: Additional Information 1
            Dim AddInfo5 As DataColumn = New DataColumn("AddInfo5")
            AddInfo5.DataType = System.Type.GetType("System.String")
            TblAddInfo.Columns.Add(AddInfo5)

            ''Set Primary Keys
            '--> Make the ID column the primary key column.
            Dim PrimaryKeyColumns(0) As DataColumn
            PrimaryKeyColumns(0) = TblAddInfo.Columns("SNum")
            TblAddInfo.PrimaryKey = PrimaryKeyColumns

            ''Set Datatable record to blank
            Session("dtAddInfo") = ""

            ''Assign File Table to Session
            Session("dtAddInfo") = TblAddInfo
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub fnAddSubCatAddinfoRow()
        Try
            Dim TblAddInfo As DataTable = CType(Session("dtAddInfo"), DataTable)
            Dim RowAddInfo As DataRow
            Dim intCurrentRec As Integer = 0

            For intCurrentRec = 1 To hdnQuantity.Value
                ''Add New Row to Datatable
                RowAddInfo = TblAddInfo.NewRow()        'declaring a new row
                RowAddInfo.Item("SNum") = intCurrentRec
                RowAddInfo.Item("AddInfo1") = ""
                RowAddInfo.Item("AddInfo2") = ""
                RowAddInfo.Item("AddInfo3") = ""
                RowAddInfo.Item("AddInfo4") = ""
                RowAddInfo.Item("AddInfo5") = ""
                TblAddInfo.Rows.Add(RowAddInfo)
            Next

            ''Assign Updated Datatble to Session
            Session("dtAddInfo") = TblAddInfo

            ''Bind again the Datatgrid 
            BindSubCatAddInfoData()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub BindSubCatAddInfoData()
        Try
            dgSubCatAddInfo.DataSource = Session("dtAddInfo")
            dgSubCatAddInfo.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub fnPopulateFileImages()
        Try
            ''Get Records
            dgAssetFileImages.DataSource = clsAsset.fnAssetFileGetRec(Session("UsrID"), "0", "Y")
            dgAssetFileImages.DataBind()
            If Not dgAssetFileImages.Items.Count > 0 Then ''Not Records found
                dgAssetFileImages.Visible = False
            Else
                dgAssetFileImages.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnGetFileName(ByVal strFileNameSave As String) As String
        Try
            Dim strRetVal As String = ""
            strRetVal += "<a id=hlFile href='../tempFile/AssetFile/" & strFileNameSave & "' target=AssetFile >" & _
                         "<img id=imgView src=../images/audit.gif alt='Click for view file.' border=0 >" & _
                         "</a>"

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub dgAssetFileImages_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAssetFileImages.DeleteCommand
        Try
            Dim retval As Integer = "0"
            Dim strFileIDDel As String = e.Item.Cells(0).Text
            Dim strFileNameDel As String = e.Item.Cells(1).Text
            Dim strFileLocation As String = Server.MapPath("..\tempFile\AssetFile") & "\" & strFileNameDel

            ''Start: Delete selected records from database
            retval = clsAsset.fnAssetFileDeleteRec(Session("UsrID"), strFileIDDel, "DP", "")
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 Then
                ''Delete File in AssetFile folder
                If File.Exists(strFileLocation) Then
                    File.Delete(strFileLocation)
                End If

                ''populate File Images
                fnPopulateFileImages()
            Else
                lblErrorMessage.Text = "File delete failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Try
            '' Clear Temp Asset File before clear new Asset
            fnClearTempAssetFile(Server.MapPath("..\tempFile\AssetFile"), Session("UsrID"))

            '' Redirect to Asset Addition Type page
            Server.Transfer("asset_addChoosen.aspx")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            '' Clear Temp Asset File before clear new Asset
            fnClearTempAssetFile(Server.MapPath("..\tempFile\AssetFile"), Session("UsrID"))

            Response.Redirect("asset_add.aspx?AddType=" & hdnAddType.Value & "&Quantity=" & hdnQuantity.Value)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim intRetVal As Integer = 0
            Dim strMsg As String = ""
            Dim strAssetIDs As String = ""
            Dim strAssetFiles As String = ""
            Dim strAddInfoXMLF As String = "N"
            Dim strAddInfoXML As String = ""
            Dim strCost As String = ""
            Dim strOwner As String = ""
            'Dim strStatus As String = ""

            If (txtPurchaseDt.Text.ToString() <> "") Then

                Try
                    Dim culture As IFormatProvider = New System.Globalization.CultureInfo("fr-FR", True)
                    Dim dt As DateTime = DateTime.Parse(txtPurchaseDt.Text, culture, System.Globalization.DateTimeStyles.AssumeLocal)

                    If (dt > System.DateTime.Now) Then
                        'lblPurchaseDtError.Text = "Purchase date Cannot be greater than today date!"
                        lblErrorMessage.Text = "Purchase date Cannot be greater than today date!"
                        lblErrorMessage.Visible = True
                        txtPurchaseDt.Focus()
                        Return
                    Else
                        lblErrorMessage.Text = ""
                        lblErrorMessage.Visible = False
                    End If
                Catch ex As Exception

                End Try

            End If

            ''*** Get Cost
            If Trim(txtCost.Text) <> "" Then
                strCost = Math.Round(CDec(txtCost.Text), 2, MidpointRounding.AwayFromZero)
            End If

            ''*** Get Owner
            'If Trim(hfOwner.Value) <> "" Then
            '    'strOwner = cbOwner.SelectedRowValues(0)
            '    Dim strUsrExistF As String = "N"
            '    ClsUser.fnUsr_CheckUserExistAD(Trim(hfOwner.Value), strOwner, strUsrExistF)
            '    If strUsrExistF = "N" Then
            '        lblErrorMessage.Text = "Owner not found. Please select an existing Owner."
            '        lblErrorMessage.Visible = True
            '        Exit Sub
            '    End If
            'End If

            If Trim(cbOwner.Text) <> "" Then
                'strOwner = cbOwner.SelectedRowValues(0)
                Dim strUsrExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbOwner.Text), strOwner, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Owner not found. Please select an existing Owner."
                    lblErrorMessage.Visible = True
                    cbOwner.Focus()
                    Exit Sub
                End If
            End If

            'If (rdolstStatus.SelectedIndex = 1) Then

            'End If

            Dim intDeviceTypeID As Integer = 0

            If (ddlDeviceType.SelectedIndex > 0) Then
                intDeviceTypeID = IIf(ddlDeviceType.SelectedValue = "-1", "0", ddlDeviceType.SelectedValue)
            End If

            ''*** Insert Record for Single Addition
            If hdnAddType.Value = "S" Then
                ''** Insert Record
                intRetVal = clsAsset.fnAssetInsertRecSingleAddition( _
                                rdAssetType.SelectedValue, ddlAssetCat.SelectedValue, ddlAssetSubCat.SelectedValue, _
                                IIf(hdnSCatAddInfo1.Value = "", "", txtAddInfo1.Text), _
                                IIf(hdnSCatAddInfo2.Value = "", "", txtAddInfo2.Text), _
                                IIf(hdnSCatAddInfo3.Value = "", "", txtAddInfo3.Text), _
                                IIf(hdnSCatAddInfo4.Value = "", "", txtAddInfo4.Text), _
                                IIf(hdnSCatAddInfo5.Value = "", "", txtAddInfo5.Text), _
                                txtBrand.Text, txtDesc.Text, _
                                strCost, _
                                "", _
                                txtPurchaseDt.Text, txtWarExpDt.Text, _
                                txtRemarks.Text, _
                                IIf(rdAssetType.SelectedValue = "A", "Y", "N"), _
                                IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue), _
                                IIf(ddlLocation.SelectedValue = "-1", "0", ddlLocation.SelectedValue), _
                                strOwner.Trim(), _
                                Session("UsrID"), strAssetIDs, _
                                IIf(ddlLocSub.SelectedValue = "-1", "0", ddlLocSub.SelectedValue), _
                                rdCtrlItemF.SelectedValue, txtShortDescription.Text.Trim(), txtOtherInformation.Text.Trim(), lblAssetIdNFS.Text.Trim(),
                                intDeviceTypeID, txtSerialNo.Text, txtMacAddress.Text,
                                txtHostName.Text, rdolstStatus.SelectedValue, txtMachineModel.Text)

            Else
                ''*** Insert Record for Bulk Addition
                ''Get Addtional information
                If hdnSCatAddInfo1.Value = "" Then
                    strAddInfoXMLF = "N"
                Else
                    strAddInfoXML = fnGetAddionalInformationXML()
                    strAddInfoXMLF = IIf(strAddInfoXML = "", "N", "Y")
                End If


                ''Insert Recod
                intRetVal = clsAsset.fnAssetInsertRecBulkAddition( _
                                rdAssetType.SelectedValue, ddlAssetCat.SelectedValue, ddlAssetSubCat.SelectedValue, _
                                strAddInfoXMLF, strAddInfoXML, _
                                txtBrand.Text, txtDesc.Text, _
                                strCost, _
                                "", _
                                txtPurchaseDt.Text, txtWarExpDt.Text, _
                                txtRemarks.Text, _
                                IIf(rdAssetType.SelectedValue = "A", "Y", "N"), _
                                IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue), _
                                IIf(ddlLocation.SelectedValue = "-1", "0", ddlLocation.SelectedValue), _
                                strOwner, _
                                hdnQuantity.Value, Session("UsrID"), strAssetIDs, strAssetFiles, _
                                IIf(ddlLocSub.SelectedValue = "-1", "0", ddlLocSub.SelectedValue), _
                                rdCtrlItemF.SelectedValue, txtShortDescription.Text.Trim(), txtOtherInformation.Text.Trim(), lblAssetIdNFS.Text.Trim(), _
                                intDeviceTypeID, txtSerialNo.Text, txtMacAddress.Text, _
                                txtHostName.Text, rdolstStatus.SelectedValue, txtMachineModel.Text)

            End If

            If intRetVal > 0 Then
                ''Sending Email

                fnEmail4AssetCreationAMS(strAssetIDs)

                hdnAssetIds.Value = strAssetIDs
                hdnAddSuccessF.Value = "Y"

                ''Assign Asset ID for Session for Print Label
                Session("LabelPrintAssetID") = ""
                Session("LabelPrintAssetID") = strAssetIDs
            Else
                strMsg = "New Asset Added Failed."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnGetAddionalInformationXML() As String
        Try
            Dim TblAddInfo As DataTable = CType(Session("dtAddInfo"), DataTable)
            Dim dsAddInfo As DataSet
            Dim myKey(1) As DataColumn
            Dim dr As DataRow
            Dim textboxAddInfo As TextBox
            Dim intGridCnt As Integer = 0
            Dim intCurrentAddInfo As Integer = 0
            Dim strCurrentAddInfoName As String = ""
            Dim strAddInfoVal As String = ""
            Dim sXMLString As String = ""

            dsAddInfo = New DataSet             'creating a dataset
            dsAddInfo.Tables.Add(TblAddInfo)    'assign datatable to dataset
            If Not dsAddInfo Is Nothing Then

                myKey(0) = dsAddInfo.Tables(0).Columns("SNum")
                For Each dr In dsAddInfo.Tables(0).Rows
                    For intCurrentAddInfo = 1 To 5
                        strCurrentAddInfoName = ""
                        strAddInfoVal = ""

                        Select Case intCurrentAddInfo
                            Case "1"
                                strCurrentAddInfoName = Trim(hdnSCatAddInfo1.Value)
                                If strCurrentAddInfoName <> "" Then
                                    textboxAddInfo = CType(dgSubCatAddInfo.Items(intGridCnt).Cells(1).FindControl("txtAddInfo1"), TextBox)
                                    strAddInfoVal = textboxAddInfo.Text
                                    dr("AddInfo1") = strAddInfoVal
                                End If

                            Case "2"
                                strCurrentAddInfoName = Trim(hdnSCatAddInfo2.Value)
                                If strCurrentAddInfoName <> "" Then
                                    textboxAddInfo = CType(dgSubCatAddInfo.Items(intGridCnt).Cells(2).FindControl("txtAddInfo2"), TextBox)
                                    strAddInfoVal = textboxAddInfo.Text
                                    dr("AddInfo2") = strAddInfoVal
                                End If

                            Case "3"
                                strCurrentAddInfoName = Trim(hdnSCatAddInfo3.Value)
                                If strCurrentAddInfoName <> "" Then
                                    textboxAddInfo = CType(dgSubCatAddInfo.Items(intGridCnt).Cells(3).FindControl("txtAddInfo3"), TextBox)
                                    strAddInfoVal = textboxAddInfo.Text
                                    dr("AddInfo3") = strAddInfoVal
                                End If

                            Case "4"
                                strCurrentAddInfoName = Trim(hdnSCatAddInfo4.Value)
                                If strCurrentAddInfoName <> "" Then
                                    textboxAddInfo = CType(dgSubCatAddInfo.Items(intGridCnt).Cells(4).FindControl("txtAddInfo4"), TextBox)
                                    strAddInfoVal = textboxAddInfo.Text
                                    dr("AddInfo4") = strAddInfoVal
                                End If

                            Case "5"
                                strCurrentAddInfoName = Trim(hdnSCatAddInfo5.Value)
                                If strCurrentAddInfoName <> "" Then
                                    textboxAddInfo = CType(dgSubCatAddInfo.Items(intGridCnt).Cells(5).FindControl("txtAddInfo5"), TextBox)
                                    strAddInfoVal = textboxAddInfo.Text
                                    dr("AddInfo5") = strAddInfoVal
                                End If

                        End Select

                        If strCurrentAddInfoName = "" Then
                            Exit For
                        End If
                    Next

                    intGridCnt = intGridCnt + 1
                Next

                Dim loCol As DataColumn
                'Prepare XML output from the Cancel Bookings DataSet as a string
                For Each loCol In dsAddInfo.Tables(0).Columns
                    loCol.ColumnMapping = System.Data.MappingType.Attribute
                Next
                sXMLString = dsAddInfo.GetXml

            End If

            fnGetAddionalInformationXML = sXMLString

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub fnCreateNewFileAsset(ByVal strFiles As String)
        Try
            Dim arrEachFilesFull() As String
            Dim arrEachFileParts() As String
            Dim arrFileIDs() As String
            Dim strLocation As String = Server.MapPath("..\tempFile\AssetFile")
            Dim strFileNameOriginal As String = ""
            Dim strFileNameNew As String = ""
            Dim strFileName As String = ""
            Dim intCurFilesFull As Integer = "0"
            Dim intCurFileIDs As Integer = "0"

            ''1. split each File records that need to add
            arrEachFilesFull = Split(strFiles, "|")
            For intCurFilesFull = 0 To UBound(arrEachFilesFull) - 1

                If arrEachFilesFull(intCurFilesFull) <> "" Then
                    ''2. Split each file information
                    arrEachFileParts = Split(arrEachFilesFull(intCurFilesFull), "*")
                    If arrEachFileParts(0) <> "" Then
                        strFileNameOriginal = strLocation + "\" + arrEachFileParts(0) + "_" + arrEachFileParts(1)
                        strFileName = arrEachFileParts(1)

                        ''Check whether original file exist
                        If File.Exists(strFileNameOriginal) Then
                            ''3. split File ID to create
                            arrFileIDs = Split(arrEachFileParts(2), "^")
                            For intCurFileIDs = 0 To UBound(arrFileIDs) - 1
                                If arrFileIDs(intCurFileIDs) <> "" Then
                                    strFileNameNew = strLocation + "\" + arrFileIDs(intCurFileIDs) + "_" + strFileName
                                    File.Copy(strFileNameOriginal, strFileNameNew)
                                End If
                            Next
                        End If

                    End If
                End If

            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub rdAssetType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdAssetType.SelectedIndexChanged
        Try
            If rdAssetType.SelectedValue = "A" Then
                lblMandatory1.Visible = True
                lblTempAsset.Text = "Yes"
                trAssetIdNFS.Visible = False

                trSerialNo.Visible = False
                trMacAddress.Visible = False
                trHostName.Visible = False
                trStatus.Visible = False
                trDeviceType.Visible = False
                trMachineModel.Visible = False
                txtSerialNo.Text = ""
                txtMacAddress.Text = ""
                txtHostName.Text = ""
                rdolstStatus.SelectedIndex = -1
                If (ddlDeviceType.Items.Count > 0) Then
                    ddlDeviceType.SelectedIndex = 0
                End If

                txtMachineModel.Text = ""
            ElseIf rdAssetType.SelectedValue = "I" Then
                lblMandatory1.Visible = False
                lblTempAsset.Text = "No"
                trAssetIdNFS.Visible = False

                trSerialNo.Visible = False
                trMacAddress.Visible = False
                trHostName.Visible = False
                trStatus.Visible = False
                trDeviceType.Visible = False
                trMachineModel.Visible = False
                txtSerialNo.Text = ""
                txtMacAddress.Text = ""
                txtHostName.Text = ""
                rdolstStatus.SelectedIndex = -1
                If (ddlDeviceType.Items.Count > 0) Then
                    ddlDeviceType.SelectedIndex = 0
                End If
                txtMachineModel.Text = ""
            ElseIf rdAssetType.SelectedValue = "L" Then
                lblMandatory1.Visible = False
                lblTempAsset.Text = "No"

                trAssetIdNFS.Visible = False
                trSerialNo.Visible = True
                trMacAddress.Visible = True
                trHostName.Visible = True
                trDeviceType.Visible = True
                trStatus.Visible = True
                trMachineModel.Visible = True
                txtSerialNo.Text = ""
                txtMacAddress.Text = ""
                txtHostName.Text = ""
                rdolstStatus.SelectedIndex = -1
                If (ddlDeviceType.Items.Count > 0) Then
                    ddlDeviceType.SelectedIndex = 0
                End If
                txtMachineModel.Text = ""
            End If

            If rdAssetType.SelectedValue = "K" Then
                lblMandatory1.Visible = True
                lblTempAsset.Text = "Yes"
                'trAssetIdNFS.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLocation.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCodeName", False)
            Else
                ddlLocSub.Items.Clear()
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetNFSID(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = clsAsset.fnGetNFSID(prefix).Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}", row("fld_AssetNFSID")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

    Private Sub hdnclickprint_Click(sender As Object, e As System.EventArgs) Handles hdnclickprint.Click
        Try
            Dim strAssetIds As String = hdnAssetIds.Value
            'Dim strFileNameLabel As String = ""

            If Trim(strAssetIds) <> "" Then

                ClientScript.RegisterStartupScript(Me.[GetType](), "myalert", "window.open('../common/PrintPopUp.aspx','_blank','left=400, top=200, width=400, height=200, menubar=no, resizable=no, status=no, titlebar=no, toolbar=no');", True)

                Session("AssetIds" + Session("UsrID").ToString()) = strAssetIds
                Session("ModuleName" + Session("UsrID").ToString()) = "AMS"
                Session("Asset" + Session("UsrID").ToString()) = "1"
                Session("AddAsset" + Session("UsrID").ToString()) = "1"
                Session("Location" + Session("UsrID").ToString()) = ""

                'clsRFIDLabel.fnCreateRFIDLabelAsset(strAssetIds, Session("UsrID"), _
                '                                   Server.MapPath(gAMSRFIDLabelPath), strFileNameLabel)

                '    ''Insert Audit Trail
                '    clsCommon.fnAuditInsertRec(Session("UsrID"), strAssetIds, "RFID passive label printed.")

                '    'Dim strJavaScript As String
                '    'strJavaScript = "<script language = 'Javascript'>" & _
                '    '                "alert('" + "RFID Tag print succesfully." + "');" & _
                '    '                "</script>"
                '    'Response.Write(strJavaScript)

            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

End Class