#Region "Information Section"
' ****************************************************************************************************
' Description       : Search Asset 
' Purpose           : Search Asset Information
' Date              : 26/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports GControllerStrobeLight
Imports System.Threading
Imports System.Web.Services

#End Region

Partial Public Class asset_search
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********
            Me.Header.DataBind()
            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            ''Get Owner (in-case page load again)
            'cbOwner.DataSource = fnGetUserList(cbOwner.List.PageSize, "", "")

            If Not Page.IsPostBack Then
                hdnCallFrm.Value = Request("CallFrm")
                Me.butSearch.Attributes.Add("OnClick", "return chkFrm()")

                ''*** Populate Control
                fnPopulateCtrl()

                fnDDLDeviceType()

                ''*** Checking Call From which function
                ''    --> 1. Maintain Asset (MA), 2. Redundancy Control (RC), 
                ''        3. Pending to Disposed(PD), 4. Delete Asset (DA)
                ''        5. Disposed Asset (DiA)
                If hdnCallFrm.Value = "MA" Then
                    lblHeader.Text = "Maintain Asset"
                    divNFSAssetID.Visible = True
                    divLocation.Visible = True
                    divLabelPrint.Visible = True
                    'butReadActTag.Visible = True
                    butReadActTag.Visible = False

                ElseIf hdnCallFrm.Value = "RC" Then
                    lblHeader.Text = "Redundancy Control"
                    divNFSAssetID.Visible = True
                    ddlAssetType.Items.Remove(ddlAssetType.Items.FindByValue("L"))
                    ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("R"))
                    ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("C"))
                    ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("D"))
                    ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("T"))
                    divLocation.Visible = True
                    divLabelPrint.Visible = False
                    butReadActTag.Visible = False

                ElseIf hdnCallFrm.Value = "PD" Then
                    lblHeader.Text = "Pending Disposed"
                    divNFSAssetID.Visible = True
                    ddlAssetType.Items.Remove(ddlAssetType.Items.FindByValue("L"))
                    ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("A"))
                    ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("I"))
                    ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("D"))
                    ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("T"))
                    divLocation.Visible = True
                    divLabelPrint.Visible = False
                    butReadActTag.Visible = False

                ElseIf hdnCallFrm.Value = "DA" Then
                    lblHeader.Text = "Delete Asset"
                    divNFSAssetID.Visible = False
                    'ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("0"))
                    'ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("I"))
                    'ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("R"))
                    'ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("C"))
                    'ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("D"))
                    'divLocation.Visible = False
                    divLabelPrint.Visible = False
                    butReadActTag.Visible = False

                ElseIf hdnCallFrm.Value = "DiA" Then
                    lblHeader.Text = "Disposed Asset"
                    divNFSAssetID.Visible = True
                    ddlAssetType.Items.Remove(ddlAssetType.Items.FindByValue("L"))
                    ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("0"))
                    ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("A"))
                    ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("I"))
                    ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("R"))
                    ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("C"))
                    ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("T"))
                    divLocation.Visible = True
                    divLabelPrint.Visible = False
                    butReadActTag.Visible = False

                End If
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnDDLDeviceType()
        Try

            Dim dt As DataTable = clsDeviceType.fnDeviceTypeGetAllRec("fld_DeviceType", "ASC").Tables(0)
            If (dt.Rows.Count > 0) Then
                ddlDeviceType.DataSource = dt
                ddlDeviceType.DataTextField = "fld_DeviceType"
                ddlDeviceType.DataValueField = "fld_DeviceTypeID"
                ddlDeviceType.DataBind()
                ddlDeviceType.Items.Insert(0, New ListItem("", "-1"))
            End If

        Catch ex As Exception

        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Asset Type --> Fixed Asset, Inventory, Leased Inventory
            fnPopulateAssetTypeInDDL(ddlAssetType)

            ''Get Category
            fnPopulateDropDownList(clsCategory.fnCategoryGetCategory(), ddlAssetCat, "fld_CategoryID", "fld_CatCodeName", False)

            ''Get SubCategory (set default --> -1)
            ddlAssetSubCat.Items.Insert(0, New ListItem("", "-1"))

            ''Get Controlled Item --> Yes, No
            fnPopulateYesNoInDDL(ddlCtrlItem)

            ''Get Asset Status --> Active, InActive, Condemation, Dispose, Returned
            fnPopulateAssetStatusInDDL(ddlStatus)

            ''Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False)
            If Session("AdminF") = "N" Then ddlDepartment.Items.Remove(ddlDepartment.Items.FindByValue("-1"))

            ''Get Location
            fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLocation, "fld_LocationID", "fld_LoctCodeName", False)

            ''Get Sub Location 
            'ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            ddlLocation_SelectedIndexChanged(Nothing, Nothing)

            ' ''Get Owner
            'cbOwner.DataSource = fnGetUserList(cbOwner.List.PageSize, "", "")
            '

            ''Get Temporary Asset Type --> Yes, No
            fnPopulateYesNoInDDL(ddlTempAsset)

            ''Get Label Printed status
            fnPopulateYesNoInDDL(ddlLabelPrintStatus)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Protected Sub ddlAssetCat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAssetCat.SelectedIndexChanged
        Try
            If ddlAssetCat.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsCategory.fnCategoryGetAllRecDR(ddlAssetCat.SelectedValue, 0, "fld_CatSubName", "ASC"), ddlAssetSubCat, "fld_CatSubID", "fld_CatSubName", False)
            Else
                ddlAssetSubCat.Items.Clear()
                ddlAssetSubCat.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            'txtAssetIDAMS.Text = ""
            'txtAssetIDNFS.Text = ""
            'ddlAssetType.SelectedValue = ""
            'ddlAssetCat.SelectedValue = "-1"
            'ddlAssetCat_SelectedIndexChanged(Nothing, Nothing)
            'If Not (hdnCallFrm.Value = "DA" Or hdnCallFrm.Value = "DiA") Then ddlStatus.SelectedValue = "0"
            'txtPurchaseFrmDt.Text = ""
            'txtPurchaseToDt.Text = ""
            'txtWarExpFrmDt.Text = ""
            'txtWarExpToDt.Text = ""
            'If Session("AdminF") = "Y" Then ddlDepartment.SelectedValue = "-1"
            'ddlLocation.SelectedValue = "-1"
            ''ddlOwner.SelectedValue = "-1"
            'ddlTempAsset.SelectedValue = ""
            'ddlLabelPrintStatus.SelectedValue = ""

            Server.Transfer("asset_search.aspx?CallFrm=" & hdnCallFrm.Value)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
        Try
            Dim dsAsset As New DataSet
            Dim strURL As String = ""

            Dim strAssetIDAMS As String = Trim(txtAssetIDAMS.Text)
            Dim strAssetIDNFS As String = Trim(txtAssetIDNFS.Text)
            Dim strAssetType As String = ddlAssetType.SelectedValue
            Dim intAssetCatID As Integer = IIf(ddlAssetCat.SelectedValue = "-1", "0", ddlAssetCat.SelectedValue)
            Dim intAssetCatSubID As Integer = IIf(ddlAssetSubCat.SelectedValue = "-1", "0", ddlAssetSubCat.SelectedValue)
            Dim strCtrlItemF As String = ddlCtrlItem.SelectedValue
            Dim strAssetStatus As String = ddlStatus.SelectedValue
            Dim strPurchaseFDt As String = Trim(txtPurchaseFrmDt.Text)
            Dim strPurchaseTDt As String = Trim(txtPurchaseToDt.Text)
            Dim strWarExpFDt As String = Trim(txtWarExpFrmDt.Text)
            Dim strWarExpTDt As String = Trim(txtWarExpToDt.Text)
            Dim intDeptID As Integer = IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue)
            Dim intLocID As Integer = IIf(ddlLocation.SelectedValue = "-1", "0", ddlLocation.SelectedValue)
            Dim intLocSubID As Integer = IIf(ddlLocSub.SelectedValue = "-1", "0", ddlLocSub.SelectedValue)
            Dim strOwnerID As String = "" '= IIf(ddlOwner.SelectedValue = "-1", "", ddlOwner.SelectedValue)
            Dim strTempAsset As String = ddlTempAsset.SelectedValue
            Dim strLabelPrintF As String = ddlLabelPrintStatus.SelectedValue
            Dim strCallFrm As String = hdnCallFrm.Value
            Dim StrSortName As String = "fldAssetBarcode"
            Dim strSortOrder As String = "ASC"
            Dim strShortDescription As String = Trim(txtShortDescription.Text)
            Dim strOtherInformation As String = Trim(txtOtherInformation.Text)
            Dim strSerialNo As String = Trim(txtSerialNo.Text)
            Dim strMacAddress As String = Trim(txtMacAddress.Text)
            Dim strNewHostName As String = Trim(txtHostName.Text)
            Dim strMachineModel As String = Trim(txtMachineModel.Text)

            Dim intDeviceTypeID As Integer = 0

            If (ddlDeviceType.SelectedIndex > 0) Then
                intDeviceTypeID = IIf(ddlDeviceType.SelectedValue = "-1", "0", ddlDeviceType.SelectedValue)
            End If

            strAssetIDAMS = strAssetIDAMS.Replace("'", "''")
            'strAssetIDAMS = strAssetIDAMS.Replace("[", "[[]")
            'strAssetIDAMS = strAssetIDAMS.Replace("%", "[%]")
            'strAssetIDAMS = strAssetIDAMS.Replace("_", "[_]")

            strAssetIDNFS = strAssetIDNFS.Replace("'", "''")
            'strAssetIDNFS = strAssetIDNFS.Replace("[", "[[]")
            'strAssetIDNFS = strAssetIDNFS.Replace("%", "[%]")
            'strAssetIDNFS = strAssetIDNFS.Replace("_", "[_]")

            strShortDescription = strShortDescription.Replace("'", "''")
            'strShortDescription = strShortDescription.Replace("[", "[[]")
            'strShortDescription = strShortDescription.Replace("%", "[%]")
            'strShortDescription = strShortDescription.Replace("_", "[_]")

            strOtherInformation = strOtherInformation.Replace("'", "''")
            'strOtherInformation = strOtherInformation.Replace("[", "[[]")
            'strOtherInformation = strOtherInformation.Replace("%", "[%]")
            'strOtherInformation = strOtherInformation.Replace("_", "[_]")

            strSerialNo = strSerialNo.Replace("'", "''")
            'strSerialNo = strSerialNo.Replace("[", "[[]")
            'strSerialNo = strSerialNo.Replace("%", "[%]")
            'strSerialNo = strSerialNo.Replace("_", "[_]")

            strMacAddress = strMacAddress.Replace("'", "''")
            'strMacAddress = strMacAddress.Replace("[", "[[]")
            'strMacAddress = strMacAddress.Replace("%", "[%]")
            'strMacAddress = strMacAddress.Replace("_", "[_]")

            strNewHostName = strNewHostName.Replace("'", "''")
            'strNewHostName = strNewHostName.Replace("[", "[[]")
            'strNewHostName = strNewHostName.Replace("%", "[%]")
            'strNewHostName = strNewHostName.Replace("_", "[_]")

            strMachineModel = strMachineModel.Replace("'", "''")
            'strMachineModel = strMachineModel.Replace("[", "[[]")
            'strMachineModel = strMachineModel.Replace("%", "[%]")
            'strMachineModel = strMachineModel.Replace("_", "[_]")

            If Trim(cbOwner.Text) <> "" Then
                'strOwner = cbOwner.SelectedRowValues(0)
                Dim strUsrExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbOwner.Text), strOwnerID, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Owner not found. Please select an existing Owner."
                    lblErrorMessage.Visible = True
                    cbOwner.Focus()
                    Exit Sub
                End If
            End If

            dsAsset = clsAsset.fnAssetSearchRecord( _
                         strAssetIDAMS, strAssetIDNFS, _
                         strAssetType, intAssetCatID, intAssetCatSubID, _
                         strAssetStatus, strPurchaseFDt, strPurchaseTDt, _
                         strWarExpFDt, strWarExpTDt, _
                         intDeptID, intLocID, strOwnerID, _
                         strTempAsset, strLabelPrintF, _
                         strCallFrm, StrSortName, strSortOrder, intLocSubID, strCtrlItemF,
                         strShortDescription, strOtherInformation,
                         intDeviceTypeID, strSerialNo, strMacAddress,
                         strNewHostName, rdolstStatus.SelectedValue, strMachineModel
                    )


            If dsAsset.Tables(0).Rows.Count > 0 Then
                ''Just for Maintain Asset, Check for Smart Shelf for on the light
                'Dim strSmartShelfErrorF As String = "N"
                'If hdnCallFrm.Value = "MA" Then
                '    If strAssetIDAMS <> "" Or intLocSubID <> "0" Then
                '        fnCheckforSmartShelf(strAssetIDAMS, intLocSubID, strSmartShelfErrorF)
                '    End If
                'End If

                'If strSmartShelfErrorF = "N" Then
                ''Get URL for Asset listing
                strURL = "asset_view.aspx?CallFrm=" + clsEncryptDecrypt.EncryptText(strCallFrm) + "&AIdAMS=" + clsEncryptDecrypt.EncryptText(strAssetIDAMS) + "&AIdNFS=" + _
                    clsEncryptDecrypt.EncryptText(strAssetIDNFS) + _
                         "&AType=" + clsEncryptDecrypt.EncryptText(strAssetType) + "&CatID=" + clsEncryptDecrypt.EncryptText(CStr(intAssetCatID)) + "&CatSID=" + _
                         clsEncryptDecrypt.EncryptText(CStr(intAssetCatSubID)) + _
                         "&Astatus=" + clsEncryptDecrypt.EncryptText(strAssetStatus) + "&PFDt=" + clsEncryptDecrypt.EncryptText(strPurchaseFDt) + "&PTDt=" + clsEncryptDecrypt.EncryptText(strPurchaseTDt) + _
                         "&WFDt=" + clsEncryptDecrypt.EncryptText(strWarExpFDt) + "&WTDt=" + clsEncryptDecrypt.EncryptText(strWarExpTDt) + _
                         "&DeptID=" + clsEncryptDecrypt.EncryptText(CStr(intDeptID)) + "&LocID=" + clsEncryptDecrypt.EncryptText(CStr(intLocID)) + _
                         "&TempAsset=" + clsEncryptDecrypt.EncryptText(strTempAsset) + "&LabelPrint=" + clsEncryptDecrypt.EncryptText(strLabelPrintF) + "&SLocID=" + _
                         clsEncryptDecrypt.EncryptText(CStr(intLocSubID)) + "&OwnerID=" + clsEncryptDecrypt.EncryptText(CStr(strOwnerID)) + _
                         "&CtrlItemF=" + clsEncryptDecrypt.EncryptText(strCtrlItemF) + _
                         "&ShortDescription=" + clsEncryptDecrypt.EncryptText(strShortDescription) + "&OtherInformation=" + clsEncryptDecrypt.EncryptText(strOtherInformation) + _
                         "&DeviceTypeID=" + clsEncryptDecrypt.EncryptText(intDeviceTypeID.ToString()) + "&SerialNo=" + clsEncryptDecrypt.EncryptText(strSerialNo) + _
                         "&MacAddress=" + clsEncryptDecrypt.EncryptText(strMacAddress) + _
                         "&NewHostname=" + clsEncryptDecrypt.EncryptText(strNewHostName) + "&Status=" + clsEncryptDecrypt.EncryptText(rdolstStatus.SelectedValue) + _
                         "&MachineModel=" + clsEncryptDecrypt.EncryptText(strMachineModel)
                Response.Redirect(strURL)
                'End If
            Else
                lblErrorMessage.Visible = True
                lblErrorMessage.Text = "Record(s) not found."
            End If

            'Server.Transfer("asset_view.aspx?CallFrm=" & hdnCallFrm.Value)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLocation.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCodeName", False)
            Else
                ddlLocSub.Items.Clear()
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butReadActTag_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReadActTag.Click
        Dim objRdr As SqlDataReader
        Try
            Dim strErrorMsg As String = ""
            Dim strRFIDTag As String = clsReadWriteActTag.fnReadActTag(strErrorMsg)
            Dim strAMSID As Integer = 0

            If strErrorMsg <> "" Then
                lblErrorMessage.Text = strErrorMsg
                lblErrorMessage.Visible = True
            ElseIf strRFIDTag = "" Then
                lblErrorMessage.Text = "Please try again to read Active Tag."
                lblErrorMessage.Visible = True
            Else
                strAMSID = strRFIDTag.Substring(0, Len(strRFIDTag) - 1)

                objRdr = clsAsset.fnAsset_GetAssetBarcode(strAMSID)
                If Not objRdr Is Nothing Then
                    If objRdr.HasRows Then
                        objRdr.Read()
                        txtAssetIDAMS.Text = CStr(objRdr("fldAssetBarcode"))
                    End If
                End If
                objRdr.Close()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
            clsCommon.fnDataReader_Close(objRdr)
        End Try
    End Sub

    Private Sub fnCheckforSmartShelf(ByVal strAssetIDAMS As String, ByVal intLocSubID As String, ByRef strSmartShelfErrorF As String)
        Dim strLightIP As String = ""
        Dim objRdr As SqlDataReader
        Try
            Dim strActivate As String = "N"
            Dim strLightType As String = "0"


            objRdr = clsCommon.fnRFID_SmartShelfLight(strAssetIDAMS, intLocSubID)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()

                    strActivate = CStr(objRdr("fld_Activate"))
                    strLightIP = CStr(objRdr("fld_LightIPAdd"))
                    strLightType = CStr(objRdr("fld_LightType"))
                End If
            End If
            objRdr.Close()

            ''If Activate Light is YES, then on the light
            If strActivate = "Y" Then
                Dim i As Integer = 1
                Dim strLightReturn As Boolean
                Dim StrobeLight As New GControllerStrobeLight.StrobeLightDevice()
                StrobeLight.openConnection(strLightIP, 10000)

                If strLightType = "1" Then
                    ''Sets the Strobe Light Device to Visual Alert Mode. The Red LED will be activated in this mode. 
                    strLightReturn = StrobeLight.sendVisualAlert()  ''red light

                ElseIf strLightType = "2" Then
                    ''Sets the Strobe Light Device to ON mode. The Green LED will be activated in this mode. 
                    strLightReturn = StrobeLight.sendOn ''green light

                ElseIf strLightType = "3" Then
                    ''Sets the Strobe Light Device to Alert Mode. The Red LED and the Buzzer will be activated in this mode. 
                    strLightReturn = StrobeLight.sendAlert()  ''sound and red light

                End If

                'For i = 0 To 20000
                '    strLightReturn = strLightReturn
                'Next
                Thread.Sleep(gSmartShelfLight)

                ''Sets the Strobe Light Device to OFF mode. All LEDs and Buzzer will be turned off. 
                strLightReturn = StrobeLight.sendOff

                StrobeLight.closeConnection()
            End If

        Catch ex As Exception
            'Throw ex
            strSmartShelfErrorF = "Y"
            lblErrorMessage.Text = "Smart Shelf Light is not working for this IP Address : " & strLightIP 'ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
            clsCommon.fnDataReader_Close(objRdr)
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class