Public Partial Class FloatCtrl_printExpExcel
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If

            ''Get Today Date
            lblPrintDate.Text = mfnGetCurrentDateForTopPage()

            ''Bind record in datagrid
            dgFloatCtrl.DataSource = clsAsset.fnAssetFloatCtrl_GetRecord("fld_CategoryCode", "ASC")
            dgFloatCtrl.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class