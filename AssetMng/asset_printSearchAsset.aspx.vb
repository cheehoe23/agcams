Partial Public Class asset_printSearchAsset
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If

            ''Get Today Date
            lblPrintDate.Text = mfnGetCurrentDateForTopPage()

            If Not Request("CallFrm") = "MA" Then
                dgAsset.Columns(10).Visible = False
            End If

            ''Bind record in datagrid
            dgAsset.DataSource = CType(Session("AssetRecord"), DataSet)
            dgAsset.DataBind()

            ''Set Session to blank
            Session("AssetRecord") = ""
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class