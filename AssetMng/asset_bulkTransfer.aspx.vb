#Region "Information Section"
' ****************************************************************************************************
' Description       : Bulk Transer screen
' Purpose           : For Bulk Transfer Assets between Department/Location/Owner
' Date              : 30/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Web.Services

#End Region

Partial Public Class asset_bulkTransfer
    Inherits System.Web.UI.Page

    Dim culture As IFormatProvider = New System.Globalization.CultureInfo("fr-FR", True)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            Me.Header.DataBind()

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butTransferAsset.Attributes.Add("OnClick", "return chkFrm()")

                ''Get Default Value
                hdnCallFrm.Value = Request("CallFrm")
                hdnAssetIDs.Value = Request("AIDs")    '' format --> 12^13^16^

                ''Populate Control
                fnPopulateCtrl()

                ''Populate Records
                fnPopulateRecords()

                ''Set Default 
                ddlLocation.SelectedValue = "1"
                ddlLocation_SelectedIndexChanged(Nothing, Nothing)
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False)
            If Session("AdminF") = "N" Then ddlDepartment.Items.Remove(ddlDepartment.Items.FindByValue("-1"))

            ''Get Location
            fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLocation, "fld_LocationID", "fld_LoctCodeName", False)

            ''Get Sub Location 
            'ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            'ddlLocation_SelectedIndexChanged(Nothing, Nothing)

            ' ''Get Owner
            'cbOwner.DataSource = fnGetUserList(cbOwner.List.PageSize, "", "")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            ''Get Records
            dgAsset.DataSource = clsAsset.fnAssetGetAssetRecords(hdnAssetIDs.Value, "fldAssetBarcode", "ASC")
            dgAsset.DataBind()
            If Not dgAsset.Items.Count > 0 Then ''Not Records found
                dgAsset.Visible = False
                butTransferAsset.Enabled = False
                butReset.Enabled = False
                ddlDepartment.Enabled = False
                ddlLocation.Enabled = False
                cbOwner.Enabled = False
                lblErrorMessage.Text = "There is no asset record."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            'If Session("AdminF") = "Y" Then ddlDepartment.SelectedValue = "-1"
            'ddlLocation.SelectedValue = "1"
            'ddlOwner.SelectedValue = "-1"

            Server.Transfer("asset_bulkTransfer.aspx?CallFrm=" + hdnCallFrm.Value + "&AIDs=" + hdnAssetIDs.Value)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butTransferAsset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butTransferAsset.Click
        Try
            Dim intDeptID As Integer = IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue)
            Dim intLocID As Integer = IIf(ddlLocation.SelectedValue = "-1", "0", ddlLocation.SelectedValue)
            Dim intLocSubID As Integer = IIf(ddlLocSub.SelectedValue = "-1", "0", ddlLocSub.SelectedValue)
            Dim strOwner As String = "" 'IIf(ddlOwner.SelectedValue = "-1", "", ddlOwner.SelectedValue)
            Dim strOwnerEmail As String = " - "
            Dim intRetVal As Integer = "0"
            Dim strMsg As String = ""

            If Trim(cbOwner.Text) <> "" Then
                strOwnerEmail = Trim(hfOwner.Value)

                Dim strUsrExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbOwner.Text), strOwner, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Owner not found. Please select an existing Owner."
                    lblErrorMessage.Visible = True
                    cbOwner.Focus()
                    Exit Sub
                End If
            End If

            Dim TransDate As Date = Date.Parse(txtTransDt.Text, culture, System.Globalization.DateTimeStyles.AssumeLocal)

            ''insert record
            intRetVal = clsAsset.fnAssetTransferUpdateMovement( _
                                hdnAssetIDs.Value, intDeptID, intLocID, _
                                strOwner, Session("UsrID"), intLocSubID, TransDate.ToString("yyyy/MM/dd"))

            If intRetVal > 0 Then
                strMsg = "Asset Transfer Successfully."

                ''Email Notification
                fnEmail4AssetTranser(hdnAssetIDs.Value, _
                        IIf(ddlDepartment.SelectedValue = "-1", " - ", ddlDepartment.SelectedItem.ToString), _
                        IIf(ddlLocation.SelectedValue = "-1", " - ", ddlLocation.SelectedItem.ToString), _
                        IIf(ddlLocSub.SelectedValue = "-1", " - ", ddlLocSub.SelectedItem.ToString), _
                        strOwnerEmail)

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "document.location.href='asset_search.aspx?CallFrm=" + hdnCallFrm.Value + "';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "Asset Transfer Failed."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLocation.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCodeName", False)
            Else
                ddlLocSub.Items.Clear()
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class