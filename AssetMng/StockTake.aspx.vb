#Region "Information Section"
' ****************************************************************************************************
' Description       : Transaction Action
' Purpose           : Transaction Action
' Date              : 26/12/20087
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class StockTake
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butSearch.Attributes.Add("OnClick", "return chkSrhFrm()")
                Me.butUpdateLocation.Attributes.Add("OnClick", "return fnCheckSelect('ADD','cdCheckAddS')")


                ''default
                hdnImpFrmDt.Value = ""
                hdnImpToDt.Value = ""

                fnPopulateRecords()

                ''Display Record
                'fnPopulateRecords()

                ' ''Get Cost Center
                'subDisplayCostCenter()

                ' ''Check access right
                'If (InStr(Session("AR"), "NFSTrf|TrfA|upd") = 0) Then
                '    butUpdateLocation.Enabled = False
                '    dgTrfAdd.Enabled = False

                '    butTrfInAdd.Enabled = False
                '    dgTrfInAdd.Enabled = False

                '    butTrfInUpd.Enabled = False
                '    dgTrfInUpd.Enabled = False

                '    butTrfRCT.Enabled = False
                '    dgRCTUpd.Enabled = False

                '    butTrfADJ.Enabled = False
                '    dgADJUpd.Enabled = False

                '    butTrfAddExp.Enabled = False
                '    dgTrfAddExp.Enabled = False

                '    butTrfInAddExp.Enabled = False
                '    dgTrfInAddExp.Enabled = False

                '    butTrfInUpdExp.Enabled = False
                '    dgTrfInUpdExp.Enabled = False
                'End If
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        
    End Function

    Protected Sub chkAll_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim chk As CheckBox
        For Each rowItem As GridViewRow In gvStockTakeList.Rows
            chk = CType(rowItem.Cells(0).FindControl("chkCheck"), CheckBox)
            chk.Checked = DirectCast(sender, CheckBox).Checked
        Next rowItem
    End Sub

    Protected Sub gvStockTakeList_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            'Dim fld_RFID_Stock_Take_ID As Label = CType(e.Row.FindControl("fld_RFID_Stock_Take_ID"), Label)
            'fld_RFID_Stock_Take_ID.Text = DataBinder.Eval(e.Row.DataItem, "fld_RFID_Stock_Take_ID").ToString()
            Dim id As String = DataBinder.Eval(e.Row.DataItem, "fld_RFID_Stock_Take_ID").ToString()

            Dim ddlSubLocation As DropDownList = CType(e.Row.FindControl("ddlSubLocation"), DropDownList)
            Dim chkCheck As CheckBox = CType(e.Row.FindControl("chkCheck"), CheckBox)
            Dim dt As DataTable = New DataTable()
            dt = clsAsset.fnSelectByfld_RFID_Stock_Take_ID(Convert.ToInt32(id)).Tables(0)

            ddlSubLocation.DataSource = dt
            ddlSubLocation.DataValueField = "fld_SubLocationID"
            ddlSubLocation.DataTextField = "fld_LocSubCode"
            ddlSubLocation.DataBind()

            'Dim fld_MulipleLocStatus As Boolean = DataBinder.Eval(e.Row.DataItem, "fld_MulipleLocStatus").ToString()
            Dim fld_AssetFound As String = DataBinder.Eval(e.Row.DataItem, "fld_AssetFound").ToString()
            Dim fld_LocationFound As String = DataBinder.Eval(e.Row.DataItem, "fld_LocationFound").ToString()

            If (fld_AssetFound = "Yes" And fld_LocationFound = "Yes") Or (fld_AssetFound = "No") Then
                If (ddlSubLocation.Items.Count > 0) Then
                    ddlSubLocation.SelectedIndex = 1
                End If
                chkCheck.Enabled = False
                chkCheck.Visible = False
                ddlSubLocation.Enabled = False
            ElseIf (fld_AssetFound = "Yes" And fld_LocationFound = "No") Then
                If (ddlSubLocation.Items.Count > 0) Then
                    ddlSubLocation.SelectedIndex = 1
                End If
            Else
                If (ddlSubLocation.Items.Count > 0) Then
                    ddlSubLocation.SelectedIndex = 0
                End If
            End If
        End If
    End Sub

    Protected Sub gvStockTakeList_Sorting(ByVal sender As Object, ByVal e As GridViewSortEventArgs)
        Dim dataTable As DataTable = CType(Session("StockTake"), DataTable)
        Dim strSort As String = ""
        Dim strdirection As String = ""
        If dataTable IsNot Nothing Then
            Dim dataView As New DataView(dataTable)

            strSort = e.SortExpression

            strdirection = GetSortDirection(e.SortExpression)

            dataView.Sort = strSort & " " & strdirection

            gvStockTakeList.DataSource = dataView
            gvStockTakeList.DataBind()
            'gvIndexList()

        End If
    End Sub

    Private Function GetSortDirection(ByVal column As String) As String

        ' By default, set the sort direction to ascending.
        Dim sortDirection As String = "ASC"

        ' Retrieve the last column that was sorted.
        Dim sortExpression As String = TryCast(ViewState("SortExpression"), String)

        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection As String = TryCast(ViewState("SortDirection"), String)
                If (lastDirection IsNot Nothing) AndAlso (lastDirection = "ASC") Then
                    sortDirection = "DESC"
                End If
            End If
        End If

        ' Save new values in ViewState.
        ViewState("SortDirection") = sortDirection
        ViewState("SortExpression") = column

        Return sortDirection
    End Function


    Protected Sub butUpdateLocation_Click(sender As Object, e As EventArgs) Handles butUpdateLocation.Click
        For Each dvr As GridViewRow In gvStockTakeList.Rows
            Dim chkCheck As CheckBox = CType(dvr.FindControl("chkCheck"), CheckBox)
            If (chkCheck.Checked And (chkCheck.Enabled = True And chkCheck.Visible = True)) Then
                Dim ddlSubLocation As DropDownList = CType(dvr.FindControl("ddlSubLocation"), DropDownList)
                Dim lblAssetID As LinkButton = CType(dvr.FindControl("lblAssetID"), LinkButton)


                If (ddlSubLocation.SelectedIndex = 0) Then
                    lblErrorMessage.Text = "Please choose one loction"
                    Return
                End If

                clsAsset.fnRFIDInsertLocation(Convert.ToInt32(lblAssetID.Text), Convert.ToInt32(ddlSubLocation.SelectedValue))
            End If
        Next dvr
    End Sub

    Protected Sub butSearch_Click(sender As Object, e As EventArgs) Handles butSearch.Click
        Dim dt As DataTable = New DataTable()
        Dim culture As IFormatProvider = New System.Globalization.CultureInfo("fr-FR", True)
        Dim FromDate As Date = Date.Parse(txtFrmDt.Text, culture, System.Globalization.DateTimeStyles.AssumeLocal)
        Dim ToDate As Date = Date.Parse(txtToDt.Text, culture, System.Globalization.DateTimeStyles.AssumeLocal)

        dt = clsAsset.fnSelectRFIDRecord(FromDate, ToDate).Tables(0)
        Session("StockTake") = dt
        gvStockTakeList.DataSource = dt
        gvStockTakeList.DataBind()
    End Sub
End Class