#Region "Information Section"
' ****************************************************************************************************
' Description       : Add Asset Chosen
' Purpose           : Add Asset Chosen Information
' Date              : 26/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class asset_addChoosen
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butCreate.Attributes.Add("OnClick", "return chkFrm()")
                Me.rdCreateChoosen.Attributes.Add("OnClick", "return fnAdditionType()")

                ''Populate Control
                fnPopulateCtrl()

                ''Set Default 
                rdCreateChoosen.SelectedValue = "S"

                ''Check Access Right ''AssetMng|AddAsset|AddAsset,AssetMng|AddAsset|AddInvety,AssetMng|AddAsset|AddLInvtry
                If (InStr(Session("AR"), "AssetMng|AddAsset|AddAsset") = 0 And InStr(Session("AR"), "AssetMng|AddAsset|AddInvety") = 0 And InStr(Session("AR"), "AssetMng|AddAsset|AddLInvtry") = 0) Then
                    rdCreateChoosen.Enabled = False
                    txtQuantity.Enabled = False
                    butCreate.Enabled = False
                    butReset.Enabled = False
                End If
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Addtion Type --> Individual, Bulk Addition
            fnPopulateAssetChoosenInRD(rdCreateChoosen)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub butCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCreate.Click
        Try
            '' Clear Temp Asset File before clear new Asset
            fnClearTempAssetFile(Server.MapPath("..\tempFile\AssetFile"), Session("UsrID"))

            '' Access Create New Asset page
            Response.Redirect("asset_add.aspx?AddType=" & rdCreateChoosen.SelectedValue & "&Quantity=" & txtQuantity.Text)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        ''Set Default 
        rdCreateChoosen.SelectedValue = "S"
        txtQuantity.Text = ""
    End Sub
End Class