#Region "Information Section"
' ****************************************************************************************************
' Description       : Redundancy/Condemnation/Disposed Asset 
' Purpose           : View Asset Information
' Date              : 26/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
#End Region

Partial Public Class asset_DisposeCondemmRedun
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butRedundancy.Attributes.Add("OnClick", "return fnConfirmAction()")
                Me.butCondemn.Attributes.Add("OnClick", "return fnConfirmAction()")
                Me.butDisposed.Attributes.Add("OnClick", "return fnConfirmAction()")
                butAddFile.Attributes.Add("OnClick", "return mfnOpenAddImagesWindow()")

                ''Get Default Value
                hdnCallFrm.Value = Request("CallFrm")
                hdnAssetIDs.Value = Request("AIDs")    '' format --> 12^13^16^
                hdnAddFileF.Value = "N"

                '' Clear Temp File 
                fnClearTempStatusFile(Server.MapPath("..\tempFile\AssetStatusFile"), Session("UsrID"))

                ''Display Record
                fnPopulateRecords()

                ''*** Checking Call From which function
                ''    --> 1. Condemnation Control (CC), 2. Redundancy Control (RC), 3. Pending to Disposed(PD)
                If hdnCallFrm.Value = "CC" Then
                    lblHeader.Text = "Condemnation Control"
                    lblReason.Text = "Condemnation"
                    butRedundancy.Visible = False
                    butCondemn.Visible = True
                    butDisposed.Visible = False

                ElseIf hdnCallFrm.Value = "RC" Then
                    lblHeader.Text = "Redundancy Control"
                    lblReason.Text = "Redundancy"
                    butRedundancy.Visible = True
                    butCondemn.Visible = False
                    butDisposed.Visible = False

                ElseIf hdnCallFrm.Value = "PD" Then
                    lblHeader.Text = "Pending Disposed"
                    lblReason.Text = "Disposed"
                    butRedundancy.Visible = False
                    butCondemn.Visible = False
                    butDisposed.Visible = True
                ElseIf hdnCallFrm.Value = "MA" Then
                    butRedundancy.Visible = False
                    butCondemn.Visible = False
                    butDisposed.Visible = True
                End If
            End If


            '' Check "Add New File"
            If hdnAddFileF.Value = "Y" Then
                fnPopulateFile()

                ''set back to default
                hdnAddFileF.Value = "N"
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            ''Get Records
            dgAsset.DataSource = clsAsset.fnAssetGetAssetRecords(hdnAssetIDs.Value, "fldAssetBarcode", "ASC")
            dgAsset.DataBind()
            If Not dgAsset.Items.Count > 0 Then ''Not Records found
                dgAsset.Visible = False
                txtReason.Enabled = False
                butAddFile.Enabled = False
                butRedundancy.Enabled = False
                butCondemn.Enabled = False
                butDisposed.Enabled = False
                butReset.Enabled = False
                lblErrorMessage.Text = "There is no asset record."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub fnPopulateFile()
        Try
            ''Get Records
            dgFile.DataSource = clsAsset.fnStatusFileGetRec(Session("UsrID"), "0", "Y")
            dgFile.DataBind()
            If Not dgFile.Items.Count > 0 Then ''Not Records found
                dgFile.Visible = False
            Else
                dgFile.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnGetFileName(ByVal strFileNameSave As String) As String
        Try
            Dim strRetVal As String = ""
            strRetVal += "<a id=hlFile href='../tempFile/AssetStatusFile/" & strFileNameSave & "' target=_blank >" & _
                         "<img id=imgView src=../images/audit.gif alt='Click for view file.' border=0 >" & _
                         "</a>"

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub dgFile_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgFile.DeleteCommand
        Try
            Dim retval As Integer = "0"
            Dim strFileIDDel As String = e.Item.Cells(0).Text
            Dim strFileNameDel As String = e.Item.Cells(1).Text
            Dim strFileLocation As String = Server.MapPath("..\tempFile\AssetStatusFile") & "\" & strFileNameDel

            ''Start: Delete selected records from database
            retval = clsAsset.fnStatusFileDeleteRec(Session("UsrID"), strFileIDDel, "DP", "")
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 Then
                ''Delete File in AssetFile folder
                If File.Exists(strFileLocation) Then
                    File.Delete(strFileLocation)
                End If

                ''populate File Images
                fnPopulateFile()
            Else
                lblErrorMessage.Text = "File delete failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    'Public Sub fnClearTempStatusFile( _
    '                    ByVal strFileLoc As String, _
    '                    ByVal intUsrID As Integer)
    '    ' ****************************************************************************************************
    '    ' Purpose  		    : To clear temp file for Status File.
    '    ' Returns		    : Boolean
    '    ' Date			    : 29.10.2007
    '    ' ****************************************************************************************************
    '    Try
    '        Dim retval As Integer = "0"
    '        Dim strFileNameDels As String = ""
    '        Dim strFileLocation As String = strFileLoc
    '        Dim strFileLocationDel As String = ""
    '        Dim arrFileName() As String
    '        Dim intCurrentFileDel As Integer = 0

    '        ''Start: Delete selected records from database
    '        retval = clsAsset.fnStatusFileDeleteRec(intUsrID, 0, "CA", strFileNameDels)
    '        ''End  : Delete selected records from database

    '        ''Start: Message Notification
    '        If retval > 0 And Trim(strFileNameDels) <> "" Then
    '            arrFileName = Split(strFileNameDels, "*")
    '            For intCurrentFileDel = 0 To UBound(arrFileName) - 1
    '                strFileLocationDel = strFileLocation + "\" + arrFileName(intCurrentFileDel)

    '                ''Delete File in AssetFile folder
    '                If File.Exists(strFileLocationDel) Then
    '                    File.Delete(strFileLocationDel)
    '                End If
    '            Next
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            txtReason.Text = ""

            '' Clear Temp File 
            fnClearTempStatusFile(Server.MapPath("..\tempFile\AssetStatusFile"), Session("UsrID"))

            ''populate File Images
            fnPopulateFile()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butDisposed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDisposed.Click
        Try
            Dim intStatusInfoID As Integer = "0"

            fnUpdateAssetStatus("D", "asset_search.aspx?CallFrm=" + hdnCallFrm.Value, intStatusInfoID)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butCondemn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCondemn.Click
        Try
            Dim intStatusInfoID As Integer = "0"

            fnUpdateAssetStatus("C", "CondemnationCtrl_view.aspx", intStatusInfoID)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butRedundancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butRedundancy.Click
        Try
            Dim intStatusInfoID As Integer = "0"
            'Dim strParameterP As String = ""
            'Dim strJavascript As String = ""

            ''Update Status
            fnUpdateAssetStatus("R", "asset_search.aspx?CallFrm=" + hdnCallFrm.Value, intStatusInfoID)

            ' ''Get URL for pop-up window --> Redundancy Certificate
            'strParameterP = "asset_RedundancyCert.aspx?StatusInfoID=" & CStr(intStatusInfoID)

            ' ''Call Pop-up window to generate report
            'strJavascript = "<script language=javascript>" & _
            '                "window.open('" & strParameterP & "', 'Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1');" & _
            '                "</script>"
            'Response.Write(strJavascript)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub fnUpdateAssetStatus( _
                            ByVal strNewStatus As String, _
                            ByVal strURL As String, _
                            ByRef intStatusInfoID As Integer)
        Try
            Dim intRetVal As Integer = "0"
            'Dim intStatusInfoID As Integer = "0"
            Dim strMsg As String = ""
            Dim strParameterP As String = ""

            ''insert record
            intRetVal = clsAsset.fnAssetUpdateAssetStatus( _
                                hdnAssetIDs.Value, strNewStatus, Trim(txtReason.Text), _
                                Session("UsrID"), intStatusInfoID)

            If intRetVal > 0 Then
                strMsg = "Status Update Successfully."

                If strNewStatus = "R" Then
                    ''Get URL for pop-up window --> Redundancy Certificate
                    strParameterP = "window.open('asset_RedundancyCert.aspx?StatusInfoID=" & CStr(intStatusInfoID) & "', 'Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1'); "
                ElseIf strNewStatus = "C" Then
                    ''Get URL for pop-up window --> Condemnation Certificate
                    strParameterP = "window.open('asset_CondemnCert.aspx?StatusInfoID=" & CStr(intStatusInfoID) & "', 'Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1'); "
                End If

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "document.location.href='" + strURL + "';" & _
                                strParameterP & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "Status Update Failed."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class