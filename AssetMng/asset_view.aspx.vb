#Region "Information Section"
' ****************************************************************************************************
' Description       : View Asset 
' Purpose           : View Asset Information
' Date              : 26/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
Imports System.Threading
#End Region

Partial Public Class asset_view
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            ClientScript.GetPostBackEventReference(Me, String.Empty)

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                '//--> (PL)Print AMS Label, (PRPL)Print RFID Passive Label, (PRAT)Print RFID Active Tag, (AT)Asset Transfer, (DA)Delete Asset, (DiA)Disposed Asset, (RA)Redundancy Asset
                Me.butPrintLabel.Attributes.Add("OnClick", "return fnConfirmSelectedRec('PL')")
                Me.butPrintRFIDLabel.Attributes.Add("OnClick", "return fnConfirmSelectedRec('PRPL')")
                Me.butPrintRFIDActTag.Attributes.Add("OnClick", "return fnConfirmSelectedRec('PRAT')")
                Me.butBulkTransfer.Attributes.Add("OnClick", "return fnConfirmSelectedRec('AT')")
                Me.butDelAsset.Attributes.Add("OnClick", "return fnConfirmSelectedRec('DA')")
                Me.butDisposed.Attributes.Add("OnClick", "return fnConfirmSelectedRec('DiA')")
                Me.butRedundancy.Attributes.Add("OnClick", "return fnConfirmSelectedRec('RA')")

                ''Get Value pass from Search Screen
                hdnCallFrm.Value = clsEncryptDecrypt.DecryptText(Request("CallFrm"))
                hdnAssetIdAMS.Value = clsEncryptDecrypt.DecryptText(Request("AIdAMS"))
                hdnAssetIdNFS.Value = clsEncryptDecrypt.DecryptText(Request("AIdNFS"))
                hdnAssetType.Value = clsEncryptDecrypt.DecryptText(Request("AType"))
                hdnAssetCatID.Value = clsEncryptDecrypt.DecryptText(Request("CatID"))
                hdnAssetCatSubID.Value = clsEncryptDecrypt.DecryptText(Request("CatSID"))
                hdnAssetStatus.Value = clsEncryptDecrypt.DecryptText(Request("Astatus"))
                hdnPurchaseFDt.Value = clsEncryptDecrypt.DecryptText(Request("PFDt"))
                hdnPurchaseTDt.Value = clsEncryptDecrypt.DecryptText(Request("PTDt"))
                hdnWarExpFDt.Value = clsEncryptDecrypt.DecryptText(Request("WFDt"))
                hdnWarExpTDt.Value = clsEncryptDecrypt.DecryptText(Request("WTDt"))
                hdnDeptID.Value = clsEncryptDecrypt.DecryptText(Request("DeptID"))
                hdnLocID.Value = clsEncryptDecrypt.DecryptText(Request("LocID"))
                hdnLocSubID.Value = clsEncryptDecrypt.DecryptText(Request("SLocID"))
                hdnOwnerID.Value = clsEncryptDecrypt.DecryptText(Request("OwnerID"))
                hdnTempAsset.Value = clsEncryptDecrypt.DecryptText(Request("TempAsset"))
                hdnLabelPrintF.Value = clsEncryptDecrypt.DecryptText(Request("LabelPrint"))
                hdnCtrlItemF.Value = clsEncryptDecrypt.DecryptText(Request("CtrlItemF"))

                hdnShortDescription.Value = clsEncryptDecrypt.DecryptText(Request("ShortDescription"))
                hdnOtherInformation.Value = clsEncryptDecrypt.DecryptText(Request("OtherInformation"))
                hdnDeviceTypeID.Value = clsEncryptDecrypt.DecryptText(Request("DeviceTypeID"))
                hdnSerialNo.Value = clsEncryptDecrypt.DecryptText(Request("SerialNo"))
                hdnMacAddress.Value = clsEncryptDecrypt.DecryptText(Request("MacAddress"))
                hdnNewHostname.Value = clsEncryptDecrypt.DecryptText(Request("NewHostname"))
                hdnStatus.Value = clsEncryptDecrypt.DecryptText(Request("Status"))
                hdnMachineModel.Value = clsEncryptDecrypt.DecryptText(Request("MachineModel"))

                ''default Sorting/setting
                hdnSortName.Value = "fldAssetBarcode"
                hdnSortAD.Value = "ASC"
                hdnPrintLabelF.Value = "N"
                hdnPrintRFIDLabelF.Value = "N"
                hdnAssetIDsSelected.Value = ""

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()

                ''Checking Access Right and Function available for each module
                fnCheckAccessRightAvailable()

            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnCheckAccessRightAvailable()
        Try
            ''*** Checking Call From which function
            ''    --> 1. Maintain Asset (MA), 2. Redundancy Control (RC), 
            ''        3. Pending to Disposed(PD), 4. Delete Asset (DA)
            ''        5. Disposed Asset (DiA)
            If hdnCallFrm.Value = "MA" Then
                lblHeader.Text = "Maintain Asset"
                butPrintLabel.Visible = True
                butPrintRFIDLabel.Visible = True
                'butPrintRFIDActTag.Visible = True
                butBulkTransfer.Visible = True
                butDelAsset.Visible = False
                'butDisposed.Visible = False
                butRedundancy.Visible = False

                ''Checking For Assign Right
                If (InStr(Session("AR"), "AssetMng|Asset|printResult") = 0) Then butPrintResult.Visible = False
                If (InStr(Session("AR"), "AssetMng|Asset|printLabel") = 0) Then
                    butPrintLabel.Visible = False
                End If
                If (InStr(Session("AR"), "AssetMng|Asset|printRFIDP") = 0) Then
                    butPrintRFIDLabel.Visible = False
                End If
                If (InStr(Session("AR"), "AssetMng|Asset|printRFIDA") = 0) Then
                    butPrintRFIDActTag.Visible = False
                End If
                If (InStr(Session("AR"), "AssetMng|Asset|AssetTrans") = 0) Then butBulkTransfer.Visible = False
                If ((InStr(Session("AR"), "AssetMng|Asset|AssetTrans") = 0) And (InStr(Session("AR"), "AssetMng|Asset|printLabel") = 0)) And (InStr(Session("AR"), "AssetMng|Asset|printRFIDP") = 0) And (InStr(Session("AR"), "AssetMng|Asset|printRFIDA") = 0) Then
                    'dgAsset.Columns(13).Visible = False
                    dgAsset.Columns(19).Visible = False
                End If
                If Not (InStr(Session("AR"), "AssetMng|Asset|IssAsset") > 0 Or InStr(Session("AR"), "AssetMng|Asset|editAsset") > 0 Or InStr(Session("AR"), "AssetMng|Asset|editinvety") > 0 Or InStr(Session("AR"), "AssetMng|Asset|EditLInvtry") > 0 Or (InStr(Session("AR"), "AssetMng|Asset|printLabel") > 0) Or (InStr(Session("AR"), "AssetMng|Asset|viewAudit") > 0)) Then
                    'dgAsset.Columns(12).Visible = False
                    dgAsset.Columns(18).Visible = False
                End If

            ElseIf hdnCallFrm.Value = "RC" Then
                lblHeader.Text = "Redundancy Control"
                butPrintLabel.Visible = False
                butPrintRFIDLabel.Visible = False
                butPrintRFIDActTag.Visible = False
                butBulkTransfer.Visible = False
                butDelAsset.Visible = False
                butDisposed.Visible = False
                butRedundancy.Visible = True
                dgAsset.Columns(11).Visible = False
                dgAsset.Columns(12).Visible = False

                ''Checking For Assign Right
                If (InStr(Session("AR"), "AssetMng|RedunCtrl|print") = 0) Then butPrintResult.Visible = False
                If (InStr(Session("AR"), "AssetMng|RedunCtrl|Redun") = 0) Then
                    dgAsset.Columns(13).Visible = False
                    butRedundancy.Visible = False
                End If

            ElseIf hdnCallFrm.Value = "PD" Then
                lblHeader.Text = "Pending Disposed"
                butPrintLabel.Visible = False
                butPrintRFIDLabel.Visible = False
                butPrintRFIDActTag.Visible = False
                butBulkTransfer.Visible = False
                butDelAsset.Visible = False
                'butDisposed.Visible = True   --Changes by CSS, 06 Nov 2008, don't allow to disposed here
                butRedundancy.Visible = False
                dgAsset.Columns(11).Visible = False
                dgAsset.Columns(12).Visible = False

                ''Checking For Assign Right  
                If (InStr(Session("AR"), "AssetMng|PendDisposed|print") = 0) Then butPrintResult.Visible = False

                '--Changes by CSS, 06 Nov 2008, don't have disposed access right anymore
                'If (InStr(Session("AR"), "AssetMng|PendDisposed|dispose") = 0) Then 
                dgAsset.Columns(13).Visible = False
                butDisposed.Visible = False
                'End If

            ElseIf hdnCallFrm.Value = "DA" Then
                lblHeader.Text = "Delete Asset"
                butPrintLabel.Visible = False
                butPrintRFIDLabel.Visible = False
                butPrintRFIDActTag.Visible = False
                butBulkTransfer.Visible = False
                butDelAsset.Visible = True
                butDisposed.Visible = False
                butRedundancy.Visible = False
                dgAsset.Columns(11).Visible = False
                dgAsset.Columns(12).Visible = False

                ''Checking For Assign Right    
                If (InStr(Session("AR"), "AssetMng|delAsset|print") = 0) Then butPrintResult.Visible = False
                If (InStr(Session("AR"), "AssetMng|delAsset|del") = 0) Then
                    dgAsset.Columns(13).Visible = False
                    butDelAsset.Visible = False
                End If

            ElseIf hdnCallFrm.Value = "DiA" Then
                lblHeader.Text = "Disposed Asset"
                butPrintLabel.Visible = False
                butPrintRFIDLabel.Visible = False
                butPrintRFIDActTag.Visible = False
                butBulkTransfer.Visible = False
                butDelAsset.Visible = False
                butDisposed.Visible = False
                butRedundancy.Visible = False
                dgAsset.Columns(11).Visible = False
                dgAsset.Columns(12).Visible = False
                dgAsset.Columns(13).Visible = False

                ''Checking For Assign Right    
                If (InStr(Session("AR"), "AssetMng|DisAsset|print") = 0) Then butPrintResult.Visible = False

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Sort Header Display
            fnSortHeaderDisplay()

            '' Get Records
            dgAsset.DataSource = clsAsset.fnAssetSearchRecord( _
                                     hdnAssetIdAMS.Value, hdnAssetIdNFS.Value, _
                                     hdnAssetType.Value, hdnAssetCatID.Value, hdnAssetCatSubID.Value, _
                                     hdnAssetStatus.Value, hdnPurchaseFDt.Value, hdnPurchaseTDt.Value, _
                                     hdnWarExpFDt.Value, hdnWarExpTDt.Value, _
                                     hdnDeptID.Value, hdnLocID.Value, hdnOwnerID.Value, _
                                     hdnTempAsset.Value, hdnLabelPrintF.Value, _
                                     hdnCallFrm.Value, hdnSortName.Value, hdnSortAD.Value, hdnLocSubID.Value, hdnCtrlItemF.Value, _
            hdnShortDescription.Value, hdnOtherInformation.Value, _
            hdnDeviceTypeID.Value, hdnSerialNo.Value, hdnMacAddress.Value, _
            hdnNewHostname.Value, hdnStatus.Value, hdnMachineModel.Value _
)
            dgAsset.DataBind()
            If Not dgAsset.Items.Count > 0 Then ''Not Records found
                ddlPageSize.Enabled = False
                butPrintResult.Enabled = False
                butPrintLabel.Enabled = False
                butPrintRFIDLabel.Visible = False
                butPrintRFIDActTag.Visible = False
                butBulkTransfer.Enabled = False
                butDelAsset.Enabled = False
                butDisposed.Enabled = False
                butRedundancy.Enabled = False
                lblErrorMessage.Text = "There is no asset record."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By :-
        ''       fldAssetBarcode (2), fld_AssetTypeStr (3), 
        ''       fld_CategoryName (4), fld_CatSubName (5), 
        ''       fld_DepartmentName (6), fld_LocationName (7), 
        ''       fld_OwnerName (8), fld_AssetStatusStr (9)

        Dim intSortIndex As Integer
        Dim strSortHeader As String = ""

        ''Set Default Header
        dgAsset.Columns(2).HeaderText = "Asset ID"
        dgAsset.Columns(3).HeaderText = "Type"
        dgAsset.Columns(4).HeaderText = "Category"
        dgAsset.Columns(5).HeaderText = "Subcategory"
        dgAsset.Columns(6).HeaderText = "Department"
        dgAsset.Columns(7).HeaderText = "Location"
        dgAsset.Columns(8).HeaderText = "Sub Location"
        dgAsset.Columns(9).HeaderText = "Assigned Owner"
        dgAsset.Columns(10).HeaderText = "Asset Status"
        dgAsset.Columns(11).HeaderText = "Movement Status"

        dgAsset.Columns(12).HeaderText = "Short Description"
        dgAsset.Columns(13).HeaderText = "Other Information"

        dgAsset.Columns(14).HeaderText = "Device Type"
        dgAsset.Columns(15).HeaderText = "Serial No"
        dgAsset.Columns(16).HeaderText = "Mac Address"
        dgAsset.Columns(17).HeaderText = "New Host name"
        dgAsset.Columns(18).HeaderText = "Status"
        dgAsset.Columns(19).HeaderText = "Machine Model"

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fldAssetBarcode"
                intSortIndex = 2
                strSortHeader = "Asset ID"
            Case "fld_AssetTypeStr"
                intSortIndex = 3
                strSortHeader = "Type"
            Case "fld_CategoryName"
                intSortIndex = 4
                strSortHeader = "Category"
            Case "fld_CatSubName"
                intSortIndex = 5
                strSortHeader = "Subcategory"
            Case "fld_DepartmentName"
                intSortIndex = 6
                strSortHeader = "Department"
            Case "fld_LocationName"
                intSortIndex = 7
                strSortHeader = "Location"
            Case "fld_LocSubName"
                intSortIndex = 8
                strSortHeader = "Sub Location"
            Case "fld_OwnerName"
                intSortIndex = 9
                strSortHeader = "Assigned Owner"
            Case "fld_AssetStatusStr"
                intSortIndex = 10
                strSortHeader = "Asset Status"
            Case "DGLastMv"
                intSortIndex = 11
                strSortHeader = "Movement Status"

            Case "fld_ShortDescription"
                intSortIndex = 12
                strSortHeader = "Short Description"
            Case "fld_OtherInformation"
                intSortIndex = 13
                strSortHeader = "Other Information"

            Case "fld_DeviceType"
                intSortIndex = 14
                strSortHeader = "Device Type"
            Case "fld_SerialNo"
                intSortIndex = 15
                strSortHeader = "Serial No"
            Case "fld_MacAddress"
                intSortIndex = 16
                strSortHeader = "Mac Address"
            Case "fld_NewHostname"
                intSortIndex = 17
                strSortHeader = "New Host name"
            Case "fld_Status"
                intSortIndex = 18
                strSortHeader = "Status"
            Case "fld_MachineModel"
                intSortIndex = 19
                strSortHeader = "Machine Model"

        End Select

        If hdnSortAD.Value = "ASC" Then
            dgAsset.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgAsset.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Public Function fnShowEditPrintIcon(ByVal strAssetID As String, ByVal strAssetBarcode As String, ByVal strAssetType As String) As String
        Try
            Dim strRetVal As String = ""

            '' Check For Edit Asset Access Right
            If strAssetType = "Inventory" Then
                If (InStr(Session("AR"), "AssetMng|Asset|editinvety") > 0 Or InStr(Session("AR"), "AssetMng|Asset|IssAsset") > 0) Then
                    strRetVal = "<a id=hlEditAsset href='Asset_edit.aspx?AssetID=" + strAssetID + "&CallFrm=MA' target=_self >" & _
                                 "<img id=imgUpdate src=../images/update.gif alt='Click for update asset.' border=0 >" & _
                                 "</a>"
                End If
            ElseIf strAssetType = "Fixed Asset" Then
                If (InStr(Session("AR"), "AssetMng|Asset|editAsset") > 0 Or InStr(Session("AR"), "AssetMng|Asset|IssAsset") > 0) Then
                    strRetVal = "<a id=hlEditAsset href='Asset_edit.aspx?AssetID=" + strAssetID + "&CallFrm=MA' target=_self >" & _
                                 "<img id=imgUpdate src=../images/update.gif alt='Click for update asset.' border=0 >" & _
                                 "</a>"
                End If
            ElseIf strAssetType = "Linked Asset" Then
                If (InStr(Session("AR"), "AssetMng|Asset|editLinkAsset") > 0 Or InStr(Session("AR"), "AssetMng|Asset|IssAsset") > 0) Then
                    strRetVal = "<a id=hlEditAsset href='Asset_edit.aspx?AssetID=" + strAssetID + "&CallFrm=MA' target=_self >" & _
                                 "<img id=imgUpdate src=../images/update.gif alt='Click for update asset.' border=0 >" & _
                                 "</a>"
                End If
            ElseIf strAssetType = "Leased Inventory" Then
                If (InStr(Session("AR"), "AssetMng|Asset|EditLInvtry") > 0 Or InStr(Session("AR"), "AssetMng|Asset|IssAsset") > 0) Then
                    strRetVal = "<a id=hlEditAsset href='Asset_edit.aspx?AssetID=" + strAssetID + "&CallFrm=MA' target=_self >" & _
                                 "<img id=imgUpdate src=../images/update.gif alt='Click for update asset.' border=0 >" & _
                                 "</a>"
                End If
            End If


            '' Check For Print Asset Access Right
            If (InStr(Session("AR"), "AssetMng|Asset|printRFIDP") > 0) Then
                strRetVal += "<a id=hlEditPrintALabel href=# onclick=javascript:ClickPrint('" + strAssetID + "^')>" & _
                             "<img id=imgUpdate src=../images/print.gif alt='Click for print label.' border=0 >" & _
                             "</a>"
            End If

            '' Check For Audit Trail Asset Access Right
            If (InStr(Session("AR"), "AssetMng|Asset|viewAudit") > 0) Then
                strRetVal += "<a id=hlAAudit href='Asset_AuditTrail.aspx?AssetID=" + strAssetID + "&AssetIDT=" + strAssetBarcode + "' target=_self >" & _
                             "<img id=imgUpdate src=../images/audit.gif alt='View Audit.' border=0 >" & _
                             "</a>"
            End If

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgAsset.CurrentPageIndex = 0
        dgAsset.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Private Sub dgAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAsset.ItemDataBound
        ''For Maintain Asset, Just Asset Status with status Active allow to "Print Label, RFID Label and Asset Transfer"
        If hdnCallFrm.Value = "MA" Then
            Dim cbDelete As CheckBox
            cbDelete = CType(e.Item.FindControl("DeleteThis"), CheckBox)
            If Not cbDelete Is Nothing Then
                If Not e.Item.Cells(10).Text = "Active" Then
                    cbDelete.Enabled = False
                    cbDelete.Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub dgAsset_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgAsset.PageIndexChanged
        dgAsset.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub dgAsset_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgAsset.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

    Protected Sub butPrintResult_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPrintResult.Click
        Try
            Dim dsAsset As New DataSet

            '' Set Session to blank
            Session("AssetRecord") = ""

            '' Get Record
            dsAsset = clsAsset.fnAssetSearchRecord( _
                                     hdnAssetIdAMS.Value, hdnAssetIdNFS.Value, _
                                     hdnAssetType.Value, hdnAssetCatID.Value, hdnAssetCatSubID.Value, _
                                     hdnAssetStatus.Value, hdnPurchaseFDt.Value, hdnPurchaseTDt.Value, _
                                     hdnWarExpFDt.Value, hdnWarExpTDt.Value, _
                                     hdnDeptID.Value, hdnLocID.Value, hdnOwnerID.Value, _
                                     hdnTempAsset.Value, hdnLabelPrintF.Value, _
                                     hdnCallFrm.Value, "fldAssetBarcode", "ASC", hdnLocSubID.Value, hdnCtrlItemF.Value, _
                                     hdnShortDescription.Value, hdnOtherInformation.Value, _
                                     hdnDeviceTypeID.Value, hdnSerialNo.Value, hdnMacAddress.Value, _
                                     hdnNewHostname.Value, hdnStatus.Value, hdnMachineModel.Value _
)

            '' Assign Dataset to session
            Session("AssetRecord") = dsAsset

            ''
            Dim strJavascript As String = ""
            strJavascript = "<script language = 'Javascript'>" & _
                            "window.open('asset_printSearchAsset.aspx?CallFrm=" & hdnCallFrm.Value & "', 'Print_Screen','width=750,height=500,Top=0,left=0,scrollbars=1,menubar=1,resizable=1');" & _
                            "</script>"
            Response.Write(strJavascript)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Function fnGetSelectedFileRec() As String
        Try
            Dim strSelectedRec As String = ""

            ''*** Get record that selected 
            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox
            For Each GridItem In dgAsset.Items
                chkSelectedRec = CType(GridItem.Cells(19).FindControl("DeleteThis"), CheckBox)
                If chkSelectedRec.Checked Then
                    strSelectedRec += GridItem.Cells(0).Text & "^"
                End If
            Next

            Return strSelectedRec
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub butPrintLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPrintLabel.Click
        Try
            Dim strAssetIDs As String = ""

            ''Get Selected Asset IDs
            strAssetIDs = fnGetSelectedFileRec()

            If Trim(strAssetIDs) <> "" Then
                hdnAssetIDsSelected.Value = Trim(strAssetIDs)
                hdnPrintLabelF.Value = "Y"
                'fnPopulateRecords()

                'Dim strJavascript As String = ""
                'strJavascript = "<script language = 'Javascript'>" & _
                '                "gfnPrintAssetLabel('" + strAssetIDs + "');" & _
                '                "</script>"
                'Response.Write(strJavascript)
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butBulkTransfer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBulkTransfer.Click
        Try
            Dim strAssetIDs As String = ""

            ''Get Selected Asset IDs
            strAssetIDs = fnGetSelectedFileRec()

            If Trim(strAssetIDs) <> "" Then
                'Server.Transfer("asset_bulkTransfer.aspx?CallFrm=" & hdnCallFrm.Value & "&AIDs=" & strAssetIDs)
                Response.Redirect("asset_bulkTransfer.aspx?CallFrm=" & hdnCallFrm.Value & "&AIDs=" & strAssetIDs)
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butDisposed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDisposed.Click
        Try
            Dim strAssetIDs As String = ""

            ''Get Selected Asset IDs
            strAssetIDs = fnGetSelectedFileRec()

            If Trim(strAssetIDs) <> "" Then
                Server.Transfer("asset_DisposeCondemmRedun.aspx?CallFrm=" & hdnCallFrm.Value & "&AIDs=" & strAssetIDs)
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butRedundancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butRedundancy.Click
        Try
            Dim strAssetIDs As String = ""

            ''Get Selected Asset IDs
            strAssetIDs = fnGetSelectedFileRec()

            If Trim(strAssetIDs) <> "" Then
                Server.Transfer("asset_DisposeCondemmRedun.aspx?CallFrm=" & hdnCallFrm.Value & "&AIDs=" & strAssetIDs)
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butDelAsset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelAsset.Click
        Try
            Dim strAssetIDs As String = ""
            Dim retval As Integer = "0"
            Dim strMsg As String = ""

            ''Get Selected Asset IDs
            strAssetIDs = fnGetSelectedFileRec()

            If Trim(strAssetIDs) <> "" Then
                ''Start: Delete selected records from database
                retval = clsAsset.fnAssetDeleteAsset(strAssetIDs, Session("UsrID"))
                ''End  : Delete selected records from database

                ''Start: Message Notification
                If retval > 0 Then
                    strMsg = "Selected record(s) deleted successfully."
                    fnPopulateRecords()

                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>alert('" + strMsg + "');</script>"
                    Response.Write(strJavaScript)
                Else
                    strMsg = "Selected record(s) deleted failed."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If
                ''End  : Message Notification
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butPrintRFIDLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPrintRFIDLabel.Click
        Try
            Dim strAssetIDs As String = ""
            Dim strFileNameLabel As String = ""

            ''Get Selected Asset IDs
            strAssetIDs = fnGetSelectedFileRec()

            If Trim(strAssetIDs) <> "" Then
                Session("AssetIds" + Session("UsrID").ToString()) = strAssetIDs
                Session("ModuleName" + Session("UsrID").ToString()) = "AMS"
                Session("Asset" + Session("UsrID").ToString()) = "1"
                Session("Location" + Session("UsrID").ToString()) = ""
                Session("AddAsset" + Session("UsrID").ToString()) = ""
                ClientScript.RegisterStartupScript(Me.[GetType](), "myalert", "window.open('../common/PrintPopUp.aspx','_blank','left=350, top=150, width=400, height=200, menubar=no, resizable=no, status=no, titlebar=no, toolbar=no');", True)

                'clsRFIDLabel.fnCreateRFIDLabelAsset(strAssetIDs, Session("UsrID"), _
                '                                   Server.MapPath(gAMSRFIDLabelPath), strFileNameLabel)

                ' ''Insert Audit Trail
                'clsCommon.fnAuditInsertRec(Session("UsrID"), strAssetIDs, "RFID passive label printed.")

                'Dim strJavaScript As String
                'strJavaScript = "<script language = 'Javascript'>" & _
                '                "alert('" + "RFID Tag print succesfully." + "');" & _
                '                "</script>"
                'Response.Write(strJavaScript)

            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butPrintRFIDActTag_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPrintRFIDActTag.Click
        Try
            Dim strAssetID As String = ""
            Dim strAssetType As String = ""
            Dim strRFIDTag As String = ""
            Dim strErrorMsg As String = ""

            ''*** Get 1st record that selected 
            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox
            For Each GridItem In dgAsset.Items
                chkSelectedRec = CType(GridItem.Cells(13).FindControl("DeleteThis"), CheckBox)
                If chkSelectedRec.Checked Then
                    strAssetID = GridItem.Cells(0).Text
                    strAssetType = GridItem.Cells(3).Text
                    If strAssetType = "Fixed Asset" Then
                        strAssetType = "F"
                    ElseIf strAssetType = "Inventory" Then
                        strAssetType = "I"
                    ElseIf strAssetType = "Linked Asset" Then
                        strAssetType = "K"
                    Else
                        strAssetType = "L"
                    End If

                    Exit For
                End If
            Next

            ''*** Get RFID Tag
            strRFIDTag = strAssetID.PadLeft(11, "0") & strAssetType

            ''*** Write tag in local PC
            'clsReadWriteActTag.fnWriteActTag(strRFIDTag, strErrorMsg)

            ''*** Call client side EXE to write tag
            ''----> Way 1: 
            'Dim startInfo As System.Diagnostics.ProcessStartInfo
            'Dim p As New Process()
            'startInfo = New System.Diagnostics.ProcessStartInfo(gRFIDWriteEXE, strRFIDTag)
            'p.StartInfo = startInfo
            'p.Start()
            'p.WaitForExit()
            'p.Dispose()

            ''----> Way 2: 
            'Dim objScriptShell
            'objScriptShell = CreateObject("Wscript.Shell")
            'objScriptShell.Run(gRFIDWriteEXE & " " & strRFIDTag)
            'objScriptShell = Nothing
            'Thread.Sleep(6000)

            ''----> Way 3: 
            'Dim strScriptWrite As String
            'strScriptWrite = "<script language = 'vbscript'>" & _
            '                "set oWSH = createobject(""WScript.shell"")" & _
            '                "oWSH.run = createobject(""WScript.shell"")" & _
            '                "</script>"

            ''----> Way 4:
            Dim strJavascript As String = ""
            strJavascript = "<script language = 'Javascript'>" & _
                            "window.open('writeActiveTag.asp?tagData=" & strRFIDTag & "', 'WriteActiveTag_Screen','width=330,height=150,Top=0,left=0,scrollbars=1,menubar=0');" & _
                            "</script>"
            Response.Write(strJavascript)

            ''Insert Audit Trail
            clsCommon.fnAuditInsertRec(Session("UsrID"), strAssetID, "RFID active tag printed.")

            ''*** Output msg (just for local)
            'If strErrorMsg <> "" Then
            '    lblErrorMessage.Text = strErrorMsg
            '    lblErrorMessage.Visible = True
            'Else
            '    ''Insert Audit Trail
            '    clsCommon.fnAuditInsertRec(Session("UsrID"), strAssetID, "RFID active tag printed.")

            '    ' ''Message Notification
            '    Dim strJavaScript As String
            '    strJavaScript = "<script language = 'Javascript'>" & _
            '                    "alert('" & "Read Active Tag to confirm Active Tag write successfully." & "');" & _
            '                    "</script>"
            '    'strJavaScript = "<script language = 'Javascript'>" & _
            '    '                "alert('" & "Active Tag write successfully." & "');" & _
            '    '                "</script>"
            '    Response.Write(strJavaScript)
            'End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try

        'Try
        '    Dim strAssetIDs As String = ""
        '    Dim strFileNameLabel As String = ""
        '    Dim intLoopAsset As Integer = "0"

        '    ''Get Selected Asset IDs
        '    strAssetIDs = fnGetSelectedFileRec()

        '    If Trim(strAssetIDs) <> "" Then
        '        clsRFIDLabel.fnCreateRFIDLabelFile(strAssetIDs, "A", Session("UsrID"), _
        '                                           Server.MapPath(gRFIDLabelPath), strFileNameLabel, intLoopAsset)

        '        ''*** send to Printing
        '        fnPrintRFIDLabelFile(strFileNameLabel, intLoopAsset)

        '        ''Insert Audit Trail
        '        clsCommon.fnAuditInsertRec(Session("UsrID"), strAssetIDs, "RFID active tag printed.")
        '    End If
        'Catch ex As Exception
        '    lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
        '    lblErrorMessage.Visible = True
        'End Try
    End Sub

    Public Sub fnPrintRFIDLabelFile(ByVal strFileNameLabel As String)
        Try
            Dim intCount As Integer = "0"
            Dim strFileFullPath As String = ""

            If gRFIDLabelPrintF = "Y" Then
                ''Get File Full path
                strFileFullPath = Server.MapPath(gRFIDLabelPath) & strFileNameLabel

                ''*** Check whether file exist
                If File.Exists(strFileFullPath) Then
                    '' ''*** Printing RFID Label
                    ''Dim objScriptShell
                    ''objScriptShell = Server.CreateObject("Wscript.Shell")
                    ' ''objScriptShell.Run("copy " & strFileFullPath & " com1")
                    ''objScriptShell.Run("copy " & strFileFullPath & " " & gRFIDPrinterIP)

                    ' ''objScriptShell.Run("ftp " & gRFIDPrinterIP)
                    ' ''objScriptShell.Run("send " & strFileFullPath)
                    ''objScriptShell = Nothing

                    '' ''*** Delete File
                    ' ''File.Delete(strFileFullPath)

                    Dim myProcess As New Process
                    myProcess.StartInfo.FileName = gRFIDPrintBatch
                    myProcess.Start()
                    myProcess.WaitForExit() 'wait for the scan to complete
                    myProcess.Dispose()

                    ' ''*** Call client side EXE to write tag
                    'Dim startInfo As System.Diagnostics.ProcessStartInfo
                    'Dim p As New Process()
                    'startInfo = New System.Diagnostics.ProcessStartInfo(gRFIDPrintBatch & "rfid.vbs")
                    'p.StartInfo = startInfo
                    'p.Start()
                    'p.WaitForExit()
                    'p.Dispose()
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub hdnclickprint_Click(sender As Object, e As System.EventArgs) Handles hdnclickprint.Click
        Try
            Dim strAssetIDs As String = hdnAssetIDs.Value
            Dim strFileNameLabel As String = ""

            ''Get Selected Asset IDs
            'strAssetIDs = fnGetSelectedFileRec()

            If Trim(strAssetIDs) <> "" Then

                Session("AssetIds" + Session("UsrID").ToString()) = strAssetIDs.ToString()

                Session("ModuleName" + Session("UsrID").ToString()) = "AMS"
                Session("Asset" + Session("UsrID").ToString()) = "1"
                Session("Location" + Session("UsrID").ToString()) = ""
                Session("AddAsset" + Session("UsrID").ToString()) = ""

                ClientScript.RegisterStartupScript(Me.[GetType](), "myalert", "window.open('../common/PrintPopUp.aspx','_blank','left=350, top=150, width=400, height=200, menubar=no, resizable=no, status=no, titlebar=no, toolbar=no');", True)

                'clsRFIDLabel.fnCreateRFIDLabelAsset(strAssetIDs, Session("UsrID"), _
                '                                   Server.MapPath(gAMSRFIDLabelPath), strFileNameLabel)

                ''Insert Audit Trail
                'clsCommon.fnAuditInsertRec(Session("UsrID"), strAssetIDs, "RFID passive label printed.")

                'Dim strJavaScript As String
                'strJavaScript = "<script language = 'Javascript'>" & _
                '                "alert('" + "RFID Tag print succesfully." + "');" & _
                '                "</script>"
                'Response.Write(strJavaScript)

            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class