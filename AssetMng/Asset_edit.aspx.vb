#Region "Information Section"
' ****************************************************************************************************
' Description       : Modify Asset 
' Purpose           : Modify Asset Information
' Date              : 26/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Services

#End Region

Partial Public Class Asset_edit
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            Me.Header.DataBind()

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            ''Get Owner (in-case page load again)
            'cbOwner.DataSource = fnGetUserList(cbOwner.List.PageSize, "", "")

            If Not Page.IsPostBack Then
                hdnAssetID.Value = Request("AssetID")
                hdnCallFrm.Value = Request("CallFrm")
                hdnCurrentDate.Value = mfnGetCurrentDate()
                butAddImages.Attributes.Add("OnClick", "return mfnOpenAddAssetImagesWindow()")
                butAddCondemnRedunFile.Attributes.Add("OnClick", "return mfnOpenAddStatusFileWindow('CR')")
                butAddDisposedFile.Attributes.Add("OnClick", "return mfnOpenAddStatusFileWindow('D')")
                Me.butSave.Attributes.Add("OnClick", "return chkFrm()")

                ''set Default settings
                fnSetDefaultSettings()
                fnDDLDeviceType()
                ''Populate Control
                fnPopulateCtrl()

                'fncbOwnerLoad()

                ''Populate Records
                fnGetRecordsFromDB()

                ''Checking for status settings
                fnCheckStatusSettings()

                ''Populate Asset File
                fnPopulateAssetFileImages()

                ''Populate Redundancy/condemnation File
                'If hdnRedundancyF.Value = "Y" Then
                '    fnPopulateRedundantCondemnDisposeFile("R", "N")
                'ElseIf hdnCondemnF.Value = "Y" Then
                '    fnPopulateRedundantCondemnDisposeFile("C", "N")
                'End If

                ' ''Populate Dipsosed File
                'If hdnDisposedF.Value = "Y" Then
                '    fnPopulateRedundantCondemnDisposeFile("D", "N")
                'End If

                ''Populate Contract
                fnPopulateContractInfo()


                ''Populate assign Movement
                fnPopulateMovementHistory()

                ''Populate RFID movement
                'fnPopulateRFIDMvHistory()

                ''Check for Access Right
                fnCheckAccessRight()

            End If

            ''** New Asset File adding 
            If hdnAddAssetFileF.Value = "Y" Then
                hdnAddAssetFileF.Value = "N"

                fnPopulateAssetFileImages()
            End If


            ''**** New Condemnation/Redundancy/Disposed File Adding 
            'Dim strStatus As String = ""
            'Dim strTempSaveF As String = ""

            ' ''a. ** New Condemnation/Redundant File adding 
            'If hdnAddStatusFileRCF.Value = "Y" Then
            '    hdnAddStatusFileRCF.Value = "N"

            '    If hdnRedundancyF.Value = "Y" Then
            '        strStatus = "R"
            '        strTempSaveF = "N"
            '    ElseIf hdnCondemnF.Value = "Y" Then
            '        strStatus = "C"
            '        strTempSaveF = "N"
            '    Else
            '        strStatus = ddlAssetStatus.SelectedValue
            '        strTempSaveF = "Y"
            '    End If

            '    fnPopulateRedundantCondemnDisposeFile(strStatus, strTempSaveF)
            'End If

            ''b. ** New Disposed File adding 
            'If hdnAddStatusFileDF.Value = "Y" Then
            '    hdnAddStatusFileDF.Value = "N"

            '    strStatus = "D"
            '    If hdnDisposedF.Value = "Y" Then
            '        strTempSaveF = "N"
            '    Else
            '        strTempSaveF = "Y"
            '    End If

            '    fnPopulateRedundantCondemnDisposeFile(strStatus, strTempSaveF)
            'End If


        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnDDLDeviceType()
        Try

            Dim dt As DataTable = clsDeviceType.fnDeviceTypeGetAllRec("fld_DeviceType", "ASC").Tables(0)
            If (dt.Rows.Count > 0) Then
                ddlDeviceType.DataSource = dt
                ddlDeviceType.DataTextField = "fld_DeviceType"
                ddlDeviceType.DataValueField = "fld_DeviceTypeID"
                ddlDeviceType.DataBind()
                ddlDeviceType.Items.Insert(0, New ListItem("", "-1"))
            End If

        Catch ex As Exception

        End Try
    End Sub

    Public Sub fnCheckAccessRight()
        Try
            ''Check whether allow to edit asset, except Location Information
            If (hdnAssetType.Value = "A" And InStr(Session("AR"), "AssetMng|Asset|editAsset") = 0) Or _
               (hdnAssetType.Value = "I" And InStr(Session("AR"), "AssetMng|Asset|editinvety") = 0) Or _
               (hdnAssetType.Value = "L" And InStr(Session("AR"), "AssetMng|Asset|EditLInvtry") = 0) Or _
               (hdnAssetType.Value = "K" And InStr(Session("AR"), "AssetMng|Asset|editLinkAsset") = 0) Then
                ddlAssetSubCat.Enabled = False
                txtAddInfo1.Enabled = False
                txtAddInfo2.Enabled = False
                txtAddInfo3.Enabled = False
                txtAddInfo4.Enabled = False
                txtAddInfo5.Enabled = False
                rdCtrlItemF.Enabled = False
                txtBrand.Enabled = False
                ddlAssetStatus.Enabled = False
                txtDesc.Enabled = False
                txtCost.Enabled = False
                txtPurchaseDt.Enabled = False
                linkPurchaseDt.Visible = False
                txtWarExpDt.Enabled = False
                linkWarExpDt.Visible = False
                'butAddImages.Enabled = False
                butAddImages.Visible = False
                dgAssetFileImages.Columns(5).Visible = False
                txtRemarks.Enabled = False
                butCondemnRedunReprintCert.Enabled = False
                txtCondemnRedunReason.Enabled = False
                butAddCondemnRedunFile.Enabled = False
                dgCondemnRedunFile.Columns(5).Visible = False
                txtDisposedReason.Enabled = False
                butAddDisposedFile.Enabled = False
                dgDisposedFile.Columns(5).Visible = False
            End If

            ''Check whether allow to edit "Location Information"
            If (InStr(Session("AR"), "AssetMng|Asset|IssAsset") = 0) Then
                ddlDepartment.Enabled = False
                ddlLocation.Enabled = False
                ddlLocSub.Enabled = False
                cbOwner.Enabled = False
            End If

            If ((hdnAssetType.Value = "A" And InStr(Session("AR"), "AssetMng|Asset|editAsset") = 0) Or _
                (hdnAssetType.Value = "I" And InStr(Session("AR"), "AssetMng|Asset|editinvety") = 0) Or _
                 (hdnAssetType.Value = "K" And InStr(Session("AR"), "AssetMng|Asset|editLinkAsset") = 0) Or _
                (hdnAssetType.Value = "L" And InStr(Session("AR"), "AssetMng|Asset|EditLInvtry") = 0)) And _
               InStr(Session("AR"), "AssetMng|Asset|IssAsset") = 0 Then
                butSave.Enabled = False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnSetDefaultSettings()
        Try
            '' Clear Temp Asset File 
            fnClearTempAssetFile(Server.MapPath("..\tempFile\AssetFile"), Session("UsrID"))

            '' Clear Temp Status File 
            fnClearTempStatusFile(Server.MapPath("..\tempFile\AssetStatusFile"), Session("UsrID"))

            ''Hidden Fields
            hdnSCatAddInfo1.Value = ""
            hdnSCatAddInfo2.Value = ""
            hdnSCatAddInfo3.Value = ""
            hdnSCatAddInfo4.Value = ""
            hdnSCatAddInfo5.Value = ""
            hdnSCatAddInfoMO1.Value = "N"
            hdnSCatAddInfoMO2.Value = "N"
            hdnSCatAddInfoMO3.Value = "N"
            hdnSCatAddInfoMO4.Value = "N"
            hdnSCatAddInfoMO5.Value = "N"
            hdnAssetStatus.Value = ""
            hdnRedundancyF.Value = "N"
            hdnRedundancyID.Value = ""
            hdnCondemnF.Value = "N"
            hdnCondemID.Value = ""
            hdnDisposedF.Value = "N"
            hdnDisposedID.Value = "N"
            hdnAddAssetFileF.Value = "N"
            hdnAddStatusFileRCF.Value = "N"
            hdnAddStatusFileDF.Value = "N"


            ''Display Field in screen
            divAddInfo1.Visible = False
            divAddInfo2.Visible = False
            divAddInfo3.Visible = False
            divAddInfo4.Visible = False
            divAddInfo5.Visible = False
            divTempAsset.Visible = False
            divCondemnRedundant.Visible = False
            divDisposed.Visible = False
            divContract.Visible = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Status
            fnPopulateAssetStatusInDDL(ddlAssetStatus)
            ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("0"))

            ''Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False)
            If Session("AdminF") = "N" Then ddlDepartment.Items.Remove(ddlDepartment.Items.FindByValue("-1"))

            ''Get Location
            fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLocation, "fld_LocationID", "fld_LoctCodeName", False)

            ' ''Get Owner
            'cbOwner.DataSource = fnGetUserList(cbOwner.List.PageSize, "", "")
            '

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnGetRecordsFromDB()
        Dim objRdr As SqlDataReader
        Try
            objRdr = clsAsset.fnAssetModify_GetAssetRecords(hdnAssetID.Value)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()

                    lblAssetIDAMS.Text = CStr(objRdr("fldAssetBarcode"))
                    If CStr(objRdr("fld_AssetFrom")) = "N" Then
                        lblAssetIdNFS.Text = CStr(objRdr("fld_AssetNFSID"))
                    Else
                        lblAssetIdNFS.Text = " - "
                    End If
                    lblAssetCreateDt.Text = CStr(objRdr("AssetCreationDt"))
                    hdnAssetType.Value = CStr(objRdr("fld_AssetType"))
                    'lblAssetType.Text = IIf(CStr(objRdr("fld_AssetType")) = "A", "Fixed Asset", "Inventory")
                    If CStr(objRdr("fld_AssetType")) = "A" Then
                        lblAssetType.Text = "Fixed Asset"
                        lblMandatory1.Visible = True

                        trSerialNo.Visible = False
                        trMacAddress.Visible = False
                        trHostName.Visible = False
                        trStatus.Visible = False
                        trDeviceType.Visible = False
                        trMachineModel.Visible = False
                    ElseIf CStr(objRdr("fld_AssetType")) = "I" Then
                        lblAssetType.Text = "Inventory"
                        lblMandatory1.Visible = False

                        trSerialNo.Visible = False
                        trMacAddress.Visible = False
                        trHostName.Visible = False
                        trStatus.Visible = False
                        trDeviceType.Visible = False
                        trMachineModel.Visible = False
                    ElseIf CStr(objRdr("fld_AssetType")) = "L" Then
                        lblAssetType.Text = "Leased Inventory"
                        lblMandatory1.Visible = False
                        trSerialNo.Visible = True
                        trMacAddress.Visible = True
                        trHostName.Visible = True
                        trStatus.Visible = True
                        trDeviceType.Visible = True
                        trMachineModel.Visible = True
                    End If

                    ''Get Category
                    lblAssetCat.Text = CStr(objRdr("fld_CategoryName"))

                    'Populate Sub Category
                    ddlAssetSubCat.Items.Clear()
                    fnPopulateDropDownList(clsCategory.fnCategoryGetAllRecDR(CStr(objRdr("fld_AssetCategory")), 0, "fld_CatSubName", "ASC"), ddlAssetSubCat, "fld_CatSubID", "fld_CatSubName", True)
                    ddlAssetSubCat.SelectedValue = CStr(objRdr("fld_AssetCategorySub"))

                    ''Check additional infomration for sucategory
                    If Trim(CStr(objRdr("fld_CatSubAddInfo1"))) <> "" Then
                        divAddInfo1.Visible = True
                        hdnSCatAddInfo1.Value = Trim(CStr(objRdr("fld_CatSubAddInfo1")))
                        hdnSCatAddInfoMO1.Value = CStr(objRdr("fld_CatSubAddInfoM1"))
                        lblAddInfo1.Text = IIf(hdnSCatAddInfoMO1.Value = "Y", "<font color='red'>*</font>", "") & Trim(CStr(objRdr("fld_CatSubAddInfo1")))
                        txtAddInfo1.Text = Trim(CStr(objRdr("fld_CatAddInfo1")))
                    End If
                    If Trim(CStr(objRdr("fld_CatSubAddInfo2"))) <> "" Then
                        divAddInfo2.Visible = True
                        hdnSCatAddInfo2.Value = Trim(CStr(objRdr("fld_CatSubAddInfo2")))
                        hdnSCatAddInfoMO2.Value = CStr(objRdr("fld_CatSubAddInfoM2"))
                        lblAddInfo2.Text = IIf(hdnSCatAddInfoMO2.Value = "Y", "<font color='red'>*</font>", "") & Trim(CStr(objRdr("fld_CatSubAddInfo2")))
                        txtAddInfo2.Text = Trim(CStr(objRdr("fld_CatAddInfo2")))
                    End If
                    If Trim(CStr(objRdr("fld_CatSubAddInfo3"))) <> "" Then
                        divAddInfo3.Visible = True
                        hdnSCatAddInfo3.Value = Trim(CStr(objRdr("fld_CatSubAddInfo3")))
                        hdnSCatAddInfoMO3.Value = CStr(objRdr("fld_CatSubAddInfoM3"))
                        lblAddInfo3.Text = IIf(hdnSCatAddInfoMO3.Value = "Y", "<font color='red'>*</font>", "") & Trim(CStr(objRdr("fld_CatSubAddInfo3")))
                        txtAddInfo3.Text = Trim(CStr(objRdr("fld_CatAddInfo3")))
                    End If
                    If Trim(CStr(objRdr("fld_CatSubAddInfo4"))) <> "" Then
                        divAddInfo4.Visible = True
                        hdnSCatAddInfo4.Value = Trim(CStr(objRdr("fld_CatSubAddInfo4")))
                        hdnSCatAddInfoMO4.Value = CStr(objRdr("fld_CatSubAddInfoM4"))
                        lblAddInfo4.Text = IIf(hdnSCatAddInfoMO4.Value = "Y", "<font color='red'>*</font>", "") & Trim(CStr(objRdr("fld_CatSubAddInfo4")))
                        txtAddInfo4.Text = Trim(CStr(objRdr("fld_CatAddInfo4")))
                    End If
                    If Trim(CStr(objRdr("fld_CatSubAddInfo5"))) <> "" Then
                        divAddInfo5.Visible = True
                        hdnSCatAddInfo5.Value = Trim(CStr(objRdr("fld_CatSubAddInfo5")))
                        hdnSCatAddInfoMO5.Value = CStr(objRdr("fld_CatSubAddInfoM5"))
                        lblAddInfo5.Text = IIf(hdnSCatAddInfoMO5.Value = "Y", "<font color='red'>*</font>", "") & Trim(CStr(objRdr("fld_CatSubAddInfo5")))
                        txtAddInfo5.Text = Trim(CStr(objRdr("fld_CatAddInfo5")))
                    End If

                    rdCtrlItemF.SelectedValue = CStr(objRdr("fld_CtrlItemF"))
                    txtBrand.Text = CStr(objRdr("fld_AssetBrand"))

                    ddlAssetStatus.SelectedValue = CStr(objRdr("fld_AssetStatus"))
                    hdnAssetStatus.Value = CStr(objRdr("fld_AssetStatus"))

                    txtDesc.Text = CStr(objRdr("fld_AssetDesc"))
                    txtCost.Text = CStr(objRdr("fld_AssetCost"))
                    'lblCostCenter.Text = IIf(CStr(objRdr("fld_AssetType")) = "L", " - ", CStr(objRdr("fld_AssetCostCenter")))
                    lblCostCenter.Text = CStr(objRdr("fld_AssetCostCenter"))
                    lblNetBookValue.Text = IIf(CStr(objRdr("fld_AssetType")) = "L", "0.00", CStr(objRdr("NetBookValue")))
                    lblAccDepreciate.Text = IIf(CStr(objRdr("fld_AssetType")) = "L", "0.00", CStr(objRdr("AccDepreciation")))
                    txtPurchaseDt.Text = CStr(objRdr("fld_PurchaseDt"))

                    If (Not objRdr("fld_WarrantyExpDt") Is Nothing And objRdr("fld_WarrantyExpDt").ToString() <> "") Then
                        'If Not IsNothing objRdr("fld_WarrantyExpDt") Then
                        txtWarExpDt.Text = CStr(objRdr("fld_WarrantyExpDt"))
                    End If

                    ddlDepartment.SelectedValue = IIf(CStr(objRdr("fld_DeptID")) = "0", "-1", CStr(objRdr("fld_DeptID")))
                    ddlLocation.SelectedValue = IIf(CStr(objRdr("fld_LocationID")) = "0", "-1", CStr(objRdr("fld_LocationID")))
                    ddlLocation_SelectedIndexChanged(Nothing, Nothing)
                    ddlLocSub.SelectedValue = IIf(CStr(objRdr("fld_LocSubID")) = "0", "-1", CStr(objRdr("fld_LocSubID")))

                    If CStr(objRdr("MissOwnerF")) = "N" And CStr(objRdr("fld_OwnerLoginID")) <> "" Then
                        'ddlOwner.SelectedValue = IIf(CStr(objRdr("fld_OwnerLoginID")) = "", "-1", CStr(objRdr("fld_OwnerLoginID")))
                        'cbOwner.SelectedRowValues(0) = CStr(objRdr("fld_OwnerLoginID"))
                        'cbOwner.SelectedRowValues(1) = CStr(objRdr("fld_OwnerName"))
                        'cbOwner.SelectedItem.Text = CStr(objRdr("fld_OwnerName"))
                        'cbOwner.SelectedItem.Value = CStr(objRdr("fld_OwnerLoginID"))
                        'cbOwner.TextBox.Value = CStr(objRdr("fld_OwnerName"))
                        cbOwner.Text = CStr(objRdr("fld_OwnerName")) + " (" + CStr(objRdr("fld_OwnerDepartment")) + ")"
                        hfOwner.Value = CStr(objRdr("fld_OwnerName"))
                    End If

                    txtShortDescription.Text = (objRdr("fld_ShortDescription").ToString())
                    txtOtherInformation.Text = (objRdr("fld_OtherInformation").ToString())

                    txtRemarks.Text = CStr(objRdr("fld_AssetRemarks"))
                    'rdTempAsset.SelectedValue = CStr(objRdr("fld_TempAsset"))
                    lblTempAsset.Text = IIf(CStr(objRdr("fld_TempAsset")) = "Y", "Yes", "No")
                    If CStr(objRdr("fld_AssetFrom")) = "A" Then
                        divTempAsset.Visible = True
                    End If

                    'Dim intDeviceTypeID As Integer = 0

                    'If (ddlDeviceType.SelectedIndex > 0) Then
                    '    intDeviceTypeID = IIf(ddlDeviceType.SelectedValue = "-1", "0", ddlDeviceType.SelectedValue)
                    'End If

                    ddlDeviceType.SelectedValue = IIf(CStr(objRdr("fld_DeviceTypeID").ToString()) = "0", "-1", CStr(objRdr("fld_DeviceTypeID").ToString()))
                    txtSerialNo.Text = CStr(objRdr("fld_SerialNo").ToString())
                    txtMacAddress.Text = CStr(objRdr("fld_MacAddress").ToString())
                    txtHostName.Text = CStr(objRdr("fld_NewHostname").ToString())
                    rdolstStatus.SelectedValue = CStr(objRdr("fld_Status").ToString())
                    txtMachineModel.Text = CStr(objRdr("fld_MachineModel").ToString())

                    ''Check whether have Redundancy Information
                    'If CStr(objRdr("fld_RedundancyF")) = "Y" Then
                    '    hdnRedundancyF.Value = CStr(objRdr("fld_RedundancyF"))
                    '    hdnRedundancyID.Value = CStr(objRdr("fld_RedundancyID"))
                    '    divCondemnRedundant.Visible = True
                    '    lblCondemnRedunT1.Text = "Redundancy"
                    '    butCondemnRedunReprintCert.Text = "[Re-print Redundancy Certificate]"
                    '    lblCondemnRedunT2.Text = "Redundancy"
                    '    txtCondemnRedunReason.Text = CStr(objRdr("redundancyReason"))
                    '    txtCondemnRedunReason.Enabled = False
                    '    lblCondemnRedunAssetIncluded.Text = CStr(objRdr("redundancyAssetinclude"))
                    'End If

                    ''Check whether have Condemnation Information
                    'If CStr(objRdr("fld_CondemnF")) = "Y" Then
                    '    hdnCondemnF.Value = CStr(objRdr("fld_CondemnF"))
                    '    hdnCondemID.Value = CStr(objRdr("fld_CondemnID"))
                    '    divCondemnRedundant.Visible = True
                    '    lblCondemnRedunT1.Text = "Condemnation"
                    '    butCondemnRedunReprintCert.Text = "[Re-print Condemnation Certificate]"
                    '    lblCondemnRedunT2.Text = "Condemnation"
                    '    txtCondemnRedunReason.Text = CStr(objRdr("CondemnReason"))
                    '    txtCondemnRedunReason.Enabled = False
                    '    lblCondemnRedunAssetIncluded.Text = CStr(objRdr("CondemnAssetinclude"))
                    'End If

                    ''Check whether have Disposed Information
                    'If CStr(objRdr("fld_DisposedF")) = "Y" Then
                    '    hdnDisposedF.Value = CStr(objRdr("fld_DisposedF"))
                    '    hdnDisposedID.Value = CStr(objRdr("fld_DisposedID"))
                    '    divDisposed.Visible = True
                    '    txtDisposedReason.Text = CStr(objRdr("DisposedReason"))
                    '    txtDisposedReason.Enabled = False
                    '    lblDisposedAssetIncluded.Text = CStr(objRdr("DisposedAssetinclude"))
                    'End If
                End If
            End If
            objRdr.Close()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Public Sub fnCheckStatusSettings()
        Try
            If hdnAssetType.Value = "L" Then
                ''check Status --> for Leased Inventory
                ''just have status 'Active' , 'Returned'
                If hdnAssetStatus.Value = "T" Then
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("A"))
                End If

                ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("I"))
                ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("R"))
                ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("C"))
                ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("D"))

            Else
                ''check Status --> for Fixed Asset and Inventory
                If hdnAssetStatus.Value = "A" Then
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("D"))
                    'If Session("AdminF") = "N" Then
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("R"))
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("C"))
                    'End If
                ElseIf hdnAssetStatus.Value = "I" Then
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("D"))
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("R"))
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("C"))
                ElseIf hdnAssetStatus.Value = "C" Then
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("A"))
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("I"))
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("R"))
                    'If Session("AdminF") = "N" Then
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("D"))
                    'End If
                ElseIf hdnAssetStatus.Value = "R" Then
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("A"))
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("I"))
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("C"))
                    'If Session("AdminF") = "N" Then
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("D"))
                    'End If
                ElseIf hdnAssetStatus.Value = "D" Then
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("A"))
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("I"))
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("R"))
                    ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("C"))
                End If

                ddlAssetStatus.Items.Remove(ddlAssetStatus.Items.FindByValue("T"))
            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub fnPopulateAssetFileImages()
        Try
            ''Get Records
            dgAssetFileImages.DataSource = clsAsset.fnAssetFileGetRec("0", hdnAssetID.Value, "N")
            dgAssetFileImages.DataBind()
            If Not dgAssetFileImages.Items.Count > 0 Then ''Not Records found
                dgAssetFileImages.Visible = False
            Else
                dgAssetFileImages.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnGetFileName(ByVal strFileNameSave As String) As String
        Try
            Dim strRetVal As String = ""
            strRetVal += "<a id=hlFile href='../tempFile/AssetFile/" & strFileNameSave & "' target=_blank >" & _
                         "<img id=imgView src=../images/audit.gif alt='Click for view file.' border=0 >" & _
                         "</a>"

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub dgAssetFileImages_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAssetFileImages.DeleteCommand
        Try
            Dim retval As Integer = "0"
            Dim strFileIDDel As String = e.Item.Cells(0).Text
            Dim strFileNameDel As String = e.Item.Cells(1).Text
            Dim strFileNameReal As String = e.Item.Cells(3).Text
            Dim strFileLocation As String = Server.MapPath("..\tempFile\AssetFile") & "\" & strFileNameDel

            ''Start: Delete selected records from database
            retval = clsAsset.fnAssetFileDeleteRec(Session("UsrID"), strFileIDDel, "DP", "")
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 Then
                ''Delete File in AssetFile folder
                'If File.Exists(strFileLocation) Then
                '    File.Delete(strFileLocation)
                'End If

                ''populate File Images
                fnPopulateAssetFileImages()

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), hdnAssetID.Value + "^", "<font class=DisplayAudit>[Asset Images Deleted]</font> <br>Asset ID : " + lblAssetIDAMS.Text + "<br>File Name : " + strFileNameReal)

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('Asset Images delete successfully.');" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                lblErrorMessage.Text = "Asset Images delete failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub fnPopulateRedundantCondemnDisposeFile( _
                                ByVal strStatus As String, _
                                ByVal strTempSaveF As String)
        Try
            Dim strUserID As String = "0"
            Dim strStatusFileID As String = "0"
            Dim dsStatusFile As New DataSet

            ''Get Value for retrieve record
            If strTempSaveF = "Y" Then   ''For Temporary File, not have StatusInfoID, store File under userID
                strUserID = Session("UsrID")
                strStatusFileID = "0"

            Else                         ''For Existing file, get StatusInfoID to retrieve record
                strUserID = "0"
                If strStatus = "R" Then       ''Get Redundancy StatusInfoID
                    strStatusFileID = hdnRedundancyID.Value
                ElseIf strStatus = "C" Then   ''Get Condmenation StatusInfoID
                    strStatusFileID = hdnCondemID.Value
                ElseIf strStatus = "D" Then   ''Get Disposed StatusInfoID
                    strStatusFileID = hdnDisposedID.Value
                End If
            End If

            ''Get Record from database
            dsStatusFile = clsAsset.fnStatusFileGetRec(strUserID, strStatusFileID, strTempSaveF)

            ''Bind Record in datagrid, If Status is Redundancy or Condemnation
            If strStatus = "R" Or strStatus = "C" Then
                dgCondemnRedunFile.DataSource = dsStatusFile
                dgCondemnRedunFile.DataBind()
                If Not dgCondemnRedunFile.Items.Count > 0 Then ''Not Records found
                    dgCondemnRedunFile.Visible = False
                Else
                    dgCondemnRedunFile.Visible = True
                End If

            ElseIf strStatus = "D" Then
                dgDisposedFile.DataSource = dsStatusFile
                dgDisposedFile.DataBind()
                If Not dgDisposedFile.Items.Count > 0 Then ''Not Records found
                    dgDisposedFile.Visible = False
                Else
                    dgDisposedFile.Visible = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnGetCondemnRedunDisposedFileName(ByVal strFileNameSave As String) As String
        Try
            Dim strRetVal As String = ""
            strRetVal += "<a id=hlFile href='../tempFile/AssetStatusFile/" & strFileNameSave & "' target=_blank >" & _
                         "<img id=imgView src=../images/audit.gif alt='Click for view file.' border=0 >" & _
                         "</a>"

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub dgCondemnRedunFile_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCondemnRedunFile.DeleteCommand
        Try
            Dim strFileIDDel As String = e.Item.Cells(0).Text
            Dim strFileNameDel As String = e.Item.Cells(1).Text
            Dim strFileNameReal As String = e.Item.Cells(3).Text
            Dim strStatus As String = ddlAssetStatus.SelectedValue
            Dim strTempSaveF As String = "Y"

            ''Delete File 
            If fnDeleteRedunCondemnDisposeFile(strFileIDDel, strFileNameDel) Then
                If hdnRedundancyF.Value = "Y" Then
                    strStatus = "R"
                    strTempSaveF = "N"

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), hdnAssetID.Value + "^", "<font class=DisplayAudit>[Redundancy File Deleted]</font> <br>Asset ID : " + lblAssetIDAMS.Text + "<br>File Name : " + strFileNameReal)

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('Redundancy File delete successfully.');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                ElseIf hdnCondemnF.Value = "Y" Then
                    strStatus = "C"
                    strTempSaveF = "N"

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), hdnAssetID.Value + "^", "<font class=DisplayAudit>[Condemnation File Deleted]</font> <br>Asset ID : " + lblAssetIDAMS.Text + "<br>File Name : " + strFileNameReal)

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('Condemnation File delete successfully.');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                End If

                ''Populate File 
                fnPopulateRedundantCondemnDisposeFile(strStatus, strTempSaveF)
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub dgDisposedFile_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDisposedFile.DeleteCommand
        Try
            Dim strFileIDDel As String = e.Item.Cells(0).Text
            Dim strFileNameDel As String = e.Item.Cells(1).Text
            Dim strFileNameReal As String = e.Item.Cells(3).Text
            Dim strStatus As String = "D"
            Dim strTempSaveF As String = "Y"

            ''Delete File 
            If fnDeleteRedunCondemnDisposeFile(strFileIDDel, strFileNameDel) Then
                If hdnDisposedF.Value = "Y" Then
                    strTempSaveF = "N"

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), hdnAssetID.Value + "^", "<font class=DisplayAudit>[Disposed File Deleted]</font> <br>Asset ID : " + lblAssetIDAMS.Text + "<br>File Name : " + strFileNameReal)

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('Disposed File delete successfully.');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                End If

                ''Populate File 
                fnPopulateRedundantCondemnDisposeFile(strStatus, strTempSaveF)
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Function fnDeleteRedunCondemnDisposeFile( _
                        ByVal strFileIDDel As String, _
                        ByVal strFileNameDel As String) As Boolean
        Try
            Dim retval As Integer = "0"
            Dim strFileLocation As String = Server.MapPath("..\tempFile\AssetStatusFile") & "\" & strFileNameDel

            ''Start: Delete selected records from database
            retval = clsAsset.fnStatusFileDeleteRec(Session("UsrID"), strFileIDDel, "DP", "")
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 Then
                ''Delete File in AssetFile folder
                If File.Exists(strFileLocation) Then
                    File.Delete(strFileLocation)
                End If

                fnDeleteRedunCondemnDisposeFile = True
            Else
                lblErrorMessage.Text = "File delete failed."
                lblErrorMessage.Visible = True

                fnDeleteRedunCondemnDisposeFile = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Protected Sub butCondemnRedunReprintCert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCondemnRedunReprintCert.Click
        Try
            Dim strParameterP As String = ""

            If hdnRedundancyF.Value = "Y" Then
                ''Get URL for pop-up window --> Redundancy Certificate
                strParameterP = "window.open('asset_RedundancyCert.aspx?StatusInfoID=" & CStr(hdnRedundancyID.Value) & "', 'Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1'); "
            ElseIf hdnCondemnF.Value = "Y" Then
                ''Get URL for pop-up window --> Condemnation Certificate
                strParameterP = "window.open('asset_CondemnCert.aspx?StatusInfoID=" & CStr(hdnCondemID.Value) & "', 'Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1'); "
            End If

            ''Message Notification
            Dim strJavaScript As String
            strJavaScript = "<script language = 'Javascript'>" & _
                            strParameterP & _
                            "</script>"
            Response.Write(strJavaScript)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub fnPopulateContractInfo()
        Try
            ''Get Contract Info
            dgContract.DataSource = clsAsset.fnAsset_GetContractInfo(hdnAssetID.Value)
            dgContract.DataBind()
            If Not dgContract.Items.Count > 0 Then ''Not Records found
                divContract.Visible = False
                dgContract.Visible = False
            Else
                divContract.Visible = True
                dgContract.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub fnPopulateMovementHistory()
        Try
            ''Get Movement Info
            dgAssetMv.DataSource = clsAsset.fnAssetModify_GetMovementHistory(hdnAssetID.Value)
            dgAssetMv.DataBind()
            If Not dgAssetMv.Items.Count > 0 Then ''Not Records found
                dgAssetMv.Visible = False
            Else
                dgAssetMv.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgAssetMv_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgAssetMv.PageIndexChanged
        Try
            dgAssetMv.CurrentPageIndex = e.NewPageIndex
            fnPopulateMovementHistory()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    'Protected Sub ddlAssetStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAssetStatus.SelectedIndexChanged
    '    Try
    '        ''*** Clear Data --> for Fixed Asset, Inventory
    '        If hdnAssetType.Value = "A" Or hdnAssetType.Value = "I" Then
    '            If hdnAssetStatus.Value = "A" Or hdnAssetStatus.Value = "I" Then
    '                ''If Original Status = "A"/"I" --> Clear Redundant/Condemnation Information

    '                '' Clear Redundancy/Condemantion Details
    '                divCondemnRedundant.Visible = False
    '                lblCondemnRedunT1.Text = ""
    '                lblCondemnRedunT2.Text = ""
    '                txtCondemnRedunReason.Text = ""
    '                lblCondemnRedunAssetIncluded.Text = ""
    '                dgCondemnRedunFile.Visible = False

    '                '' Clear Temp Status File 
    '                fnClearTempStatusFile(Server.MapPath("..\tempFile\AssetStatusFile"), Session("UsrID"))

    '            ElseIf hdnAssetStatus.Value = "R" Or hdnAssetStatus.Value = "C" Then
    '                ''If Original Status = "R"/"C" --> Clear Disposed Information

    '                '' Clear Disposed Details
    '                divDisposed.Visible = False
    '                txtDisposedReason.Text = ""
    '                lblDisposedAssetIncluded.Text = ""
    '                dgDisposedFile.Visible = False

    '                '' Clear Temp Status File 
    '                fnClearTempStatusFile(Server.MapPath("..\tempFile\AssetStatusFile"), Session("UsrID"))
    '            End If

    '            ''** Display Related Information
    '            If ddlAssetStatus.SelectedValue = "R" And hdnRedundancyF.Value = "N" Then
    '                ''Display Redundancy Information
    '                divCondemnRedundant.Visible = True
    '                lblCondemnRedunT1.Text = "Redundancy"
    '                butCondemnRedunReprintCert.Visible = False
    '                lblCondemnRedunT2.Text = "Redundancy"
    '                txtCondemnRedunReason.Text = ""
    '                lblCondemnRedunAssetIncluded.Text = lblAssetIDAMS.Text

    '                ''Populate Redundant File
    '                fnPopulateRedundantCondemnDisposeFile("R", "Y")

    '            ElseIf ddlAssetStatus.SelectedValue = "C" And hdnCondemnF.Value = "N" Then
    '                ''Display Condemnation Information
    '                divCondemnRedundant.Visible = True
    '                lblCondemnRedunT1.Text = "Condemnation"
    '                butCondemnRedunReprintCert.Visible = False
    '                lblCondemnRedunT2.Text = "Condemnation"
    '                txtCondemnRedunReason.Text = ""
    '                lblCondemnRedunAssetIncluded.Text = lblAssetIDAMS.Text

    '                ''Populate Condemnation File
    '                fnPopulateRedundantCondemnDisposeFile("C", "Y")

    '            ElseIf ddlAssetStatus.SelectedValue = "D" And hdnDisposedF.Value = "N" Then
    '                ''Display Disposed Information
    '                divDisposed.Visible = True
    '                txtDisposedReason.Text = ""
    '                lblDisposedAssetIncluded.Text = lblAssetIDAMS.Text

    '                ''Populate Condemnation File
    '                fnPopulateRedundantCondemnDisposeFile("D", "Y")
    '            End If
    '        End If

    '    Catch ex As Exception
    '        lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
    '        lblErrorMessage.Visible = True
    '    End Try
    'End Sub

    Protected Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        If hdnCallFrm.Value = "MA" Then
            Server.Transfer("asset_search.aspx?CallFrm=MA")
        Else
            Server.Transfer("UsrResign_view.aspx")
        End If

    End Sub

    Protected Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        Try
            Dim intRetVal As Integer = 0
            Dim strMsg As String = ""
            Dim strCost As String = ""
            Dim strOwner As String = ""
            Dim intStatusInfoID As Integer = "0"
            Dim strParameterP As String = ""

            ''*** Get Cost
            If Trim(txtCost.Text) <> "" Then
                strCost = Math.Round(CDec(txtCost.Text), 2, MidpointRounding.AwayFromZero)
            End If

            ''*** Get Owner
            'If Trim(hfOwner.Value) <> "" Then
            '    'strOwner = cbOwner.SelectedRowValues(0)
            '    Dim strUsrExistF As String = "N"
            '    ClsUser.fnUsr_CheckUserExistAD(Trim(hfOwner.Value), strOwner, strUsrExistF)
            '    If strUsrExistF = "N" Then
            '        lblErrorMessage.Text = "Owner not found. Please select an existing Owner."
            '        lblErrorMessage.Visible = True
            '        Exit Sub
            '    End If
            'End If

            If (txtPurchaseDt.Text.ToString() <> "") Then

                Try
                    Dim culture As IFormatProvider = New System.Globalization.CultureInfo("fr-FR", True)
                    Dim dt As DateTime = DateTime.Parse(txtPurchaseDt.Text, culture, System.Globalization.DateTimeStyles.AssumeLocal)

                    If (dt > System.DateTime.Now) Then
                        lblErrorMessage.Text = "Purchase date Cannot be greater than today date!"
                        lblErrorMessage.Visible = True
                        txtPurchaseDt.Focus()
                        Return
                    Else
                        lblErrorMessage.Text = ""
                        lblErrorMessage.Visible = False
                    End If
                Catch ex As Exception

                End Try

            End If

            If Trim(cbOwner.Text) <> "" Then
                'strOwner = cbOwner.SelectedRowValues(0)
                Dim strUsrExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbOwner.Text), strOwner, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Owner not found. Please select an existing Owner."
                    lblErrorMessage.Visible = True
                    cbOwner.Focus()
                    Exit Sub
                End If
            End If

            Dim intDeviceTypeID As Integer = 0

            If (ddlDeviceType.SelectedIndex > 0) Then
                intDeviceTypeID = IIf(ddlDeviceType.SelectedValue = "-1", "0", ddlDeviceType.SelectedValue)
            End If

            ''*** update Record 
            intRetVal = clsAsset.fnAsset_updateAsset( _
                              hdnAssetID.Value, _
                              IIf(hdnSCatAddInfo1.Value = "", "", txtAddInfo1.Text), _
                              IIf(hdnSCatAddInfo2.Value = "", "", txtAddInfo2.Text), _
                              IIf(hdnSCatAddInfo3.Value = "", "", txtAddInfo3.Text), _
                              IIf(hdnSCatAddInfo4.Value = "", "", txtAddInfo4.Text), _
                              IIf(hdnSCatAddInfo5.Value = "", "", txtAddInfo5.Text), _
                              ddlAssetStatus.SelectedValue, txtBrand.Text, txtDesc.Text, _
                              strCost, _
                              lblCostCenter.Text, _
                              txtPurchaseDt.Text, txtWarExpDt.Text, _
                              txtRemarks.Text, _
                              IIf(lblTempAsset.Text = "Yes", "Y", "N"), _
                              IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue), _
                              IIf(ddlLocation.SelectedValue = "-1", "0", ddlLocation.SelectedValue), _
                              strOwner.Trim(), _
                              txtCondemnRedunReason.Text, txtDisposedReason.Text, _
                              Session("UsrID"), ddlAssetSubCat.SelectedValue, intStatusInfoID, _
                              IIf(ddlLocSub.SelectedValue = "-1", "0", ddlLocSub.SelectedValue), _
                              rdCtrlItemF.SelectedValue, txtShortDescription.Text.Trim(), txtOtherInformation.Text.Trim(), lblAssetIdNFS.Text.Trim(),
                              intDeviceTypeID, txtSerialNo.Text, txtMacAddress.Text, _
                              txtHostName.Text, rdolstStatus.SelectedValue, txtMachineModel.Text)

            If intRetVal > 0 Then
                ''Check whether change from "A"/"I" to "R"/"C", then generate certificate
                'If (hdnAssetStatus.Value = "A" Or hdnAssetStatus.Value = "I") And _
                '    (ddlAssetStatus.SelectedValue = "R" Or ddlAssetStatus.SelectedValue = "C") Then
                '    If ddlAssetStatus.SelectedValue = "R" Then
                '        ''Get URL for pop-up window --> Redundancy Certificate
                '        strParameterP = "window.open('asset_RedundancyCert.aspx?StatusInfoID=" & CStr(intStatusInfoID) & "', 'Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1'); "
                '    ElseIf ddlAssetStatus.SelectedValue = "C" Then
                '        ''Get URL for pop-up window --> Condemnation Certificate
                '        strParameterP = "window.open('asset_CondemnCert.aspx?StatusInfoID=" & CStr(intStatusInfoID) & "', 'Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1'); "
                '    End If
                'End If

                ''Message Notification
                Dim strJavaScript As String = ""
                Dim strURL As String = IIf(hdnCallFrm.Value = "MA", "asset_search.aspx?CallFrm=MA", "UsrResign_view.aspx")
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('Asset update successfully.');" & _
                                "document.location.href='" + strURL + "';" & _
                                strParameterP & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "Asset update failed."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Protected Sub ddlAssetSubCat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAssetSubCat.SelectedIndexChanged
        Dim objRdr As SqlDataReader
        Try
            hdnSCatAddInfo1.Value = ""
            hdnSCatAddInfo2.Value = ""
            hdnSCatAddInfo3.Value = ""
            hdnSCatAddInfo4.Value = ""
            hdnSCatAddInfo5.Value = ""
            hdnSCatAddInfoMO1.Value = ""
            hdnSCatAddInfoMO2.Value = ""
            hdnSCatAddInfoMO3.Value = ""
            hdnSCatAddInfoMO4.Value = ""
            hdnSCatAddInfoMO5.Value = ""
            divAddInfo1.Visible = False
            lblAddInfo1.Text = ""
            txtAddInfo1.Text = ""
            divAddInfo2.Visible = False
            lblAddInfo2.Text = ""
            txtAddInfo2.Text = ""
            divAddInfo3.Visible = False
            lblAddInfo3.Text = ""
            txtAddInfo3.Text = ""
            divAddInfo4.Visible = False
            lblAddInfo4.Text = ""
            txtAddInfo4.Text = ""
            divAddInfo5.Visible = False
            lblAddInfo5.Text = ""
            txtAddInfo5.Text = ""

            ''  If subcategory seleceted... get additional information
            If ddlAssetSubCat.SelectedValue <> "-1" Then

                objRdr = clsCategory.fnCategoryGetAllRecDR(0, ddlAssetSubCat.SelectedValue, "fld_CatSubID", "ASC")
                If Not objRdr Is Nothing Then
                    If objRdr.HasRows Then
                        objRdr.Read()

                        ''Check whether have subcategory
                        If CStr(objRdr("fld_CatSubAddInfo1")) <> "" Then '' Got subcategory
                            hdnSCatAddInfo1.Value = Trim(CStr(objRdr("fld_CatSubAddInfo1")))
                            hdnSCatAddInfo2.Value = Trim(CStr(objRdr("fld_CatSubAddInfo2")))
                            hdnSCatAddInfo3.Value = Trim(CStr(objRdr("fld_CatSubAddInfo3")))
                            hdnSCatAddInfo4.Value = Trim(CStr(objRdr("fld_CatSubAddInfo4")))
                            hdnSCatAddInfo5.Value = Trim(CStr(objRdr("fld_CatSubAddInfo5")))
                            hdnSCatAddInfoMO1.Value = CStr(objRdr("fld_CatSubAddInfoM1"))
                            hdnSCatAddInfoMO2.Value = CStr(objRdr("fld_CatSubAddInfoM2"))
                            hdnSCatAddInfoMO3.Value = CStr(objRdr("fld_CatSubAddInfoM3"))
                            hdnSCatAddInfoMO4.Value = CStr(objRdr("fld_CatSubAddInfoM4"))
                            hdnSCatAddInfoMO5.Value = CStr(objRdr("fld_CatSubAddInfoM5"))


                            DisplayAddInfoTB4Subcategory()
                        End If
                    End If
                End If
                objRdr.Close()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
            clsCommon.fnDataReader_Close(objRdr)
        End Try
    End Sub

    Public Sub DisplayAddInfoTB4Subcategory()
        Try
            Dim strOutput As String = ""
            Dim intCurrentAddInfo As Integer = 0
            Dim strCurrentAddInfoName As String = ""
            Dim strCurentAddInfoMO As String = ""

            For intCurrentAddInfo = 1 To 5
                strCurrentAddInfoName = ""
                strCurentAddInfoMO = ""

                Select Case intCurrentAddInfo
                    Case "1"
                        strCurrentAddInfoName = Trim(hdnSCatAddInfo1.Value)
                        strCurentAddInfoMO = IIf(hdnSCatAddInfoMO1.Value = "Y", "<font color='red'>*</font>", "")
                        If strCurrentAddInfoName <> "" Then
                            divAddInfo1.Visible = True
                            lblAddInfo1.Text = strCurentAddInfoMO + strCurrentAddInfoName
                        End If
                    Case "2"
                        strCurrentAddInfoName = Trim(hdnSCatAddInfo2.Value)
                        strCurentAddInfoMO = IIf(hdnSCatAddInfoMO2.Value = "Y", "<font color='red'>*</font>", "")
                        If strCurrentAddInfoName <> "" Then
                            divAddInfo2.Visible = True
                            lblAddInfo2.Text = strCurentAddInfoMO + strCurrentAddInfoName
                        End If
                    Case "3"
                        strCurrentAddInfoName = Trim(hdnSCatAddInfo3.Value)
                        strCurentAddInfoMO = IIf(hdnSCatAddInfoMO3.Value = "Y", "<font color='red'>*</font>", "")
                        If strCurrentAddInfoName <> "" Then
                            divAddInfo3.Visible = True
                            lblAddInfo3.Text = strCurentAddInfoMO + strCurrentAddInfoName
                        End If
                    Case "4"
                        strCurrentAddInfoName = Trim(hdnSCatAddInfo4.Value)
                        strCurentAddInfoMO = IIf(hdnSCatAddInfoMO4.Value = "Y", "<font color='red'>*</font>", "")
                        If strCurrentAddInfoName <> "" Then
                            divAddInfo4.Visible = True
                            lblAddInfo4.Text = strCurentAddInfoMO + strCurrentAddInfoName
                        End If
                    Case "5"
                        strCurrentAddInfoName = Trim(hdnSCatAddInfo5.Value)
                        strCurentAddInfoMO = IIf(hdnSCatAddInfoMO5.Value = "Y", "<font color='red'>*</font>", "")
                        If strCurrentAddInfoName <> "" Then
                            divAddInfo5.Visible = True
                            lblAddInfo5.Text = strCurentAddInfoMO + strCurrentAddInfoName
                        End If
                End Select

                If strCurrentAddInfoName = "" Then
                    Exit For
                End If
            Next
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLocation.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCodeName", False)
            Else
                ddlLocSub.Items.Clear()
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub fnPopulateRFIDMvHistory()
        Try
            ''Get Movement Info
            dgRFIDMv.DataSource = clsAsset.fnAssetModify_GetRFIGMvHistory(hdnAssetID.Value)
            dgRFIDMv.DataBind()
            'If Not dgRFIDMv.Items.Count > 0 Then ''Not Records found
            '    dgRFIDMv.Visible = False
            'Else
            '    dgRFIDMv.Visible = True
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgRFIDMv_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgRFIDMv.PageIndexChanged
        Try
            dgRFIDMv.CurrentPageIndex = e.NewPageIndex
            fnPopulateRFIDMvHistory()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class