#Region "Information Section"
' ****************************************************************************************************
' Description       : Returned (Leased Inventory)
' Purpose           : View Returned (Leased Inventory) Information
' Date              : 29/07/2008
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class ReturnLI_searchResult
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butReturn.Attributes.Add("OnClick", "return fnConfirmSelectedRec()")
                Me.butPendingNotice.Attributes.Add("OnClick", "return fnConfirmPendNotice()")

                ''Checking For Assign Right   AssetMng|ReturnLI|Return,AssetMng|ReturnLI|PendNotice
                If (InStr(Session("AR"), "AssetMng|ReturnLI|Return") = 0) Then
                    butReturn.Visible = False
                    dgAsset.Columns(14).Visible = False
                End If
                If (InStr(Session("AR"), "AssetMng|ReturnLI|PendNotice") = 0) Then
                    butPendingNotice.Visible = False
                    dgAsset.Columns(13).Visible = False
                End If

                ''Get Value pass from Search Screen
                hdnAssetIdAMS.Value = Request("AIdAMS")
                hdnAssetType.Value = Request("AType")
                hdnAssetCatID.Value = Request("CatID")
                hdnAssetCatSubID.Value = Request("CatSID")
                hdnAssetStatus.Value = Request("Astatus")
                hdnPurchaseFDt.Value = Request("PFDt")
                hdnPurchaseTDt.Value = Request("PTDt")
                hdnWarExpFDt.Value = Request("WFDt")
                hdnWarExpTDt.Value = Request("WTDt")
                hdnDeptID.Value = Request("DeptID")
                hdnLocID.Value = Request("LocID")
                hdnLocSubID.Value = Request("SLocID")
                hdnOwnerID.Value = Request("OwnerID")
                hdnCtrlItemF.Value = Request("CtrlItemF")

                ''default Sorting
                hdnSortName.Value = "fldAssetBarcode"
                hdnSortAD.Value = "ASC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Sort Header Display
            fnSortHeaderDisplay()

            ''set session to blank
            Session("ReturnAsset") = ""

            ''Get Records
            Dim ds As New DataSet
            ds = clsAsset.fnReturnedAsset_SearchRecord( _
                             hdnAssetIdAMS.Value, _
                             hdnAssetType.Value, hdnAssetCatID.Value, hdnAssetCatSubID.Value, _
                             hdnAssetStatus.Value, hdnPurchaseFDt.Value, hdnPurchaseTDt.Value, _
                             hdnWarExpFDt.Value, hdnWarExpTDt.Value, _
                             hdnDeptID.Value, hdnLocID.Value, hdnOwnerID.Value, _
                             hdnSortName.Value, hdnSortAD.Value, "0", "Y", Session("UsrID"), hdnLocSubID.Value, hdnCtrlItemF.Value)
            Session("ReturnAsset") = ds
            dgAsset.DataSource = ds
            dgAsset.DataBind()
            If Not dgAsset.Items.Count > 0 Then ''Not Records found
                ddlPageSize.Enabled = False
                lblErrorMessage.Text = "There is no record."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By :-
        ''       fldAssetBarcode (2), fld_AssetTypeStr (3), 
        ''       fld_CategoryName (4), fld_CatSubName (5), 
        ''       fld_DepartmentName (6), fld_LocationName (7), 
        ''       fld_OwnerName (8), fld_AssetStatusStr (9), fld_purchaseDt (10), fld_condemnExp(11)

        Dim intSortIndex As Integer
        Dim strSortHeader As String = ""

        ''Set Default Header
        dgAsset.Columns(2).HeaderText = "Asset ID"
        dgAsset.Columns(3).HeaderText = "Type"
        dgAsset.Columns(4).HeaderText = "Category"
        dgAsset.Columns(5).HeaderText = "Subcategory"
        dgAsset.Columns(6).HeaderText = "Department"
        dgAsset.Columns(7).HeaderText = "Location"
        dgAsset.Columns(8).HeaderText = "Sub Location"
        dgAsset.Columns(9).HeaderText = "Assigned Owner"
        dgAsset.Columns(10).HeaderText = "Status"
        dgAsset.Columns(11).HeaderText = "Purchase Date"
        dgAsset.Columns(12).HeaderText = "Expiry Date"

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fldAssetBarcode"
                intSortIndex = 2
                strSortHeader = "Asset ID"
            Case "fld_AssetTypeStr"
                intSortIndex = 3
                strSortHeader = "Type"
            Case "fld_CategoryName"
                intSortIndex = 4
                strSortHeader = "Category"
            Case "fld_CatSubName"
                intSortIndex = 5
                strSortHeader = "Subcategory"
            Case "fld_DepartmentName"
                intSortIndex = 6
                strSortHeader = "Department"
            Case "fld_LocationName"
                intSortIndex = 7
                strSortHeader = "Location"
            Case "fld_LocSubName"
                intSortIndex = 8
                strSortHeader = "Sub Location"
            Case "fld_OwnerName"
                intSortIndex = 9
                strSortHeader = "Assigned Owner"
            Case "fld_AssetStatusStr"
                intSortIndex = 10
                strSortHeader = "Status"
            Case "fld_purchaseDt"
                intSortIndex = 11
                strSortHeader = "Purchase Date"
            Case "fld_ActualExpiryDt"
                intSortIndex = 12
                strSortHeader = "Expiry Date"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgAsset.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgAsset.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Private Sub dgAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAsset.ItemDataBound
        Dim ddl As DropDownList

        If Not (InStr(Session("AR"), "AssetMng|ReturnLI|PendNotice") = 0) Then
            ddl = CType(e.Item.FindControl("ddlNoticeDays"), DropDownList)
            If Not ddl Is Nothing Then
                fnPopulateNumberInDDL(ddl, "20")
            End If
        End If
    End Sub


    Private Sub dgAsset_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgAsset.PageIndexChanged
        dgAsset.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub


    Private Sub dgAsset_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgAsset.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub


    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgAsset.CurrentPageIndex = 0
        dgAsset.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub


    Protected Sub butReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReturn.Click
        Try
            Dim strSelectedRec As String = ""
            Dim strMsg As String = ""
            Dim retval As Integer = "0"

            ''*** Get record selected 
            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox
            For Each GridItem In dgAsset.Items
                chkSelectedRec = CType(GridItem.Cells(14).FindControl("DeleteThis"), CheckBox)
                If chkSelectedRec.Checked Then
                    strSelectedRec += GridItem.Cells(0).Text & "^"
                End If
            Next

            If strSelectedRec <> "" Then
                ''Update Record 
                retval = clsAsset.fnReturnedAsset_ReturnAsset(strSelectedRec, Session("UsrID"))
                If retval > 0 Then
                    strMsg = "Record(s) update successfully."
                    fnPopulateRecords()

                    Dim strJavaScript As String = ""
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" & strMsg & "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    strMsg = "Record(s) update failed."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If
            Else
                lblErrorMessage.Text = "Please select at least 1 record for return."
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butPendingNotice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPendingNotice.Click
        Try
            Dim strErrorF As String = "N"
            Dim strMsg As String = ""
            Dim retval As Integer = "0"
            Dim strXMLrec As String = ""

            ''Validate user selected at least 1 record for update
            fnValidateSelectRecord(strErrorF, strMsg)

            If strErrorF = "N" Then
                strXMLrec = fnGetXMLRec()
                If strXMLrec <> "" Then
                    ''Update Record 
                    retval = clsAsset.fnReturnedAsset_UpdatePendingNotice(strXMLrec, Session("UsrID"))
                    If retval > 0 Then
                        strMsg = "Record(s) update successfully."
                        fnPopulateRecords()

                        Dim strJavaScript As String = ""
                        strJavaScript = "<script language = 'Javascript'>" & _
                                        "alert('" & strMsg & "');" & _
                                        "</script>"
                        Response.Write(strJavaScript)
                    Else
                        strMsg = "Record(s) update failed."
                        lblErrorMessage.Text = strMsg
                        lblErrorMessage.Visible = True
                    End If
                End If
            Else
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnGetXMLRec() As String
        Try
            Dim dsAsset As DataSet = CType(Session("ReturnAsset"), DataSet)
            Dim myKey(1) As DataColumn
            Dim dr As DataRow
            Dim intGridCnt As Integer = 0
            Dim sXMLString As String = ""

            If Not dsAsset Is Nothing Then
                myKey(0) = dsAsset.Tables(0).Columns("fld_AssetID")
                dsAsset.Tables(0).PrimaryKey = myKey
                For Each dr In dsAsset.Tables(0).Rows
                    ''check whether record in page is reach
                    If intGridCnt = ddlPageSize.SelectedValue Then
                        Exit For
                    End If

                    Dim ddlPenddays As DropDownList
                    ddlPenddays = CType(dgAsset.Items(intGridCnt).Cells(13).FindControl("ddlNoticeDays"), DropDownList)
                    If Not ddlPenddays Is Nothing Then
                        If ddlPenddays.SelectedValue <> "0" Then
                            dr("fld_PendNDays") = CStr((CInt(ddlPenddays.SelectedValue) * 7))
                        End If
                    End If

                    intGridCnt = intGridCnt + 1
                Next

                ' this will be the datatable with the changed rows
                Dim dsChanged As DataSet
                dsChanged = dsAsset.GetChanges(DataRowState.Modified)

                Dim loCol As DataColumn
                'Prepare XML output from the DataSet as a string
                For Each loCol In dsChanged.Tables(0).Columns
                    loCol.ColumnMapping = System.Data.MappingType.Attribute
                Next
                sXMLString = dsChanged.GetXml
            End If

            fnGetXMLRec = Trim(sXMLString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub fnValidateSelectRecord(ByRef strErrorF As String, ByRef strErrorMsg As String)
        Try
            Dim strSelectedRecF As String = "N"

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As DropDownList
            For Each GridItem In dgAsset.Items
                chkSelectedRec = CType(GridItem.Cells(13).FindControl("ddlNoticeDays"), DropDownList)
                If Not chkSelectedRec.SelectedValue = "0" Then
                    strSelectedRecF = "Y"
                End If
            Next

            ''*** Check for whether record selected 
            If strSelectedRecF = "N" Then
                strErrorF = "Y"
                strErrorMsg = "At least one record should be selected for pending notice."
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class