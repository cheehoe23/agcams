#Region "Information Section"
' ****************************************************************************************************
' Description       : Temporary Asset 
' Purpose           : View Temporary Asset Information
' Date              : 26/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class assetTemp_view
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butAssetMerge.Attributes.Add("OnClick", "return ConfirmMerge()")

                ''default Sorting
                hdnSortName.Value = "fldAssetBarcode"
                hdnSortAD.Value = "ASC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Sort Header Display
            fnSortHeaderDisplay()

            ''set session to nothing
            Session("TempAsset") = ""
            
            ''Get User Group Records
            Dim ds As New DataSet
            ds = clsAsset.fnTempAsset_GetAssetRecords(hdnSortName.Value, hdnSortAD.Value)
            Session("TempAsset") = ds
            dgAssetTemp.DataSource = ds
            dgAssetTemp.DataBind()
            If Not dgAssetTemp.Items.Count > 0 Then ''Not Records found
                dgAssetTemp.Visible = False
                ddlPageSize.Enabled = False
                butAssetMerge.Enabled = False
                lblErrorMessage.Text = "There is no record."
                lblErrorMessage.Visible = True
            Else
                dgAssetTemp.Visible = True
                ddlPageSize.Enabled = True
                butAssetMerge.Enabled = True
                lblErrorMessage.Visible = False
            End If

            ''Checking For Assign Right
            If (InStr(Session("AR"), "AssetMng|TempAsset|update") = 0) Then butAssetMerge.Visible = False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By :-
        ''       fldAssetBarcode (2), fld_AssetTypeStr (3), 
        ''       fld_CategoryName (4), fld_CatSubName (5), 
        ''       fld_AssetBrand (6), fld_DepartmentNamestr (7)

        Dim intSortIndex As Integer
        Dim strSortHeader As String = ""

        ''Set Default Header
        dgAssetTemp.Columns(2).HeaderText = "Asset ID"
        dgAssetTemp.Columns(3).HeaderText = "Type"
        dgAssetTemp.Columns(4).HeaderText = "Category"
        dgAssetTemp.Columns(5).HeaderText = "Subcategory"
        dgAssetTemp.Columns(6).HeaderText = "Brand"
        dgAssetTemp.Columns(7).HeaderText = "Department"

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fldAssetBarcode"
                intSortIndex = 2
                strSortHeader = "Asset ID"
            Case "fld_AssetTypeStr"
                intSortIndex = 3
                strSortHeader = "Type"
            Case "fld_CategoryName"
                intSortIndex = 4
                strSortHeader = "Category"
            Case "fld_CatSubName"
                intSortIndex = 5
                strSortHeader = "Subcategory"
            Case "fld_AssetBrand"
                intSortIndex = 6
                strSortHeader = "Brand"
            Case "fld_DepartmentNamestr"
                intSortIndex = 7
                strSortHeader = "Department"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgAssetTemp.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgAssetTemp.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Private Sub dgAssetTemp_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAssetTemp.ItemDataBound
        Dim drNSFRec As SqlDataReader
        Try
            ''*** Get Asset Id ***
            Dim intAssetID As String = e.Item.Cells(0).Text

            ''*** Start : Populate NFS record 
            Dim ddl As DropDownList
            ddl = CType(e.Item.FindControl("ddlAssetIdNFS"), DropDownList)
            If Not ddl Is Nothing Then

                ''*** Start: Get NFS record

                drNSFRec = clsAsset.fnTempAsset_GetNFSAssetRecords(intAssetID)
                If Not drNSFRec Is Nothing Then
                    If drNSFRec.HasRows Then
                        ''assign Populate NFS record in Dropdownlist
                        fnPopulateDropDownList(drNSFRec, ddl, "fld_AssetID", "NFSAssetIDDept", False)

                    Else
                        ddl.Items.Add(New ListItem("", "-1"))
                    End If
                Else
                    ddl.Items.Add(New ListItem("", "-1"))
                End If
                drNSFRec.Close()
                ''*** End  : Get NFS record

                ''Checking For Assign Right
                If (InStr(Session("AR"), "AssetMng|TempAsset|update") = 0) Then ddl.Enabled = False
            End If
            ''*** End  : Populate NFS record 
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
            clsCommon.fnDataReader_Close(drNSFRec)
        End Try
    End Sub

    Private Sub dgAssetTemp_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgAssetTemp.PageIndexChanged
        dgAssetTemp.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub dgAssetTemp_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgAssetTemp.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgAssetTemp.CurrentPageIndex = 0
        dgAssetTemp.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Protected Sub butAssetMerge_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAssetMerge.Click
        Try
            Dim strErrorF As String = "N"
            Dim strMsg As String = ""
            Dim retval As Integer = "0"
            Dim strXMLrec As String = ""

            ''Validate user selected at list 1 NFS for update and not duplicate NFS ID selected
            fnValidateSelectRecord(strErrorF, strMsg)

            If strErrorF = "N" Then
                strXMLrec = fnGetTempXMLRec()
                If strXMLrec <> "" Then
                    ''Update Record 
                    retval = clsAsset.fnTempAsset_UpdateRecord(strXMLrec, Session("UsrID"))
                    If retval > 0 Then
                        strMsg = "Record(s) merge successfully."
                        fnPopulateRecords()

                        Dim strJavaScript As String = ""
                        strJavaScript = "<script language = 'Javascript'>" & _
                                        "alert('" & strMsg & "');" & _
                                        "</script>"
                        Response.Write(strJavaScript)
                    Else
                        strMsg = "Record(s) merge failed."
                        lblErrorMessage.Text = strMsg
                        lblErrorMessage.Visible = True
                    End If
                End If
            Else
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnGetTempXMLRec() As String
        Try
            Dim dsTempAsset As DataSet = CType(Session("TempAsset"), DataSet)
            Dim myKey(1) As DataColumn
            Dim dr As DataRow
            Dim intGridCnt As Integer = 0
            Dim sXMLString As String = ""
            Dim rowindex As Integer = 0

            If Not dsTempAsset Is Nothing Then
                myKey(0) = dsTempAsset.Tables(0).Columns("fld_AssetID")
                dsTempAsset.Tables(0).PrimaryKey = myKey

                For Each dgItem As DataGridItem In dgAssetTemp.Items
                    'For Each dr In dsTempAsset.Tables(0).Rows
                    Dim ddlNFSAssetID As DropDownList
                    Try
                        'ddlNFSAssetID = CType(dgAssetTemp.Items(intGridCnt).Cells(8).FindControl("ddlAssetIdNFS"), DropDownList)
                        'If ddlNFSAssetID.SelectedValue <> "-1" And ddlNFSAssetID.SelectedIndex > 0 Then
                        '    dr("fld_NSFAssetID") = ddlNFSAssetID.SelectedValue
                        'End If 

                        rowindex = dgAssetTemp.CurrentPageIndex
                        If (rowindex > 0) Then
                            rowindex = (rowindex * Convert.ToInt32(ddlPageSize.SelectedValue)) + intGridCnt
                        Else
                            rowindex = intGridCnt
                        End If

                        ddlNFSAssetID = CType(dgItem.Cells(8).FindControl("ddlAssetIdNFS"), DropDownList)
                        If ddlNFSAssetID.SelectedValue <> "-1" And ddlNFSAssetID.SelectedIndex > 0 Then
                            dsTempAsset.Tables(0).Rows(rowindex)("fld_NSFAssetID") = ddlNFSAssetID.SelectedValue
                        End If
                    Catch ex As Exception

                    End Try
                    intGridCnt = intGridCnt + 1
                Next

                ' this will be the datatable with the changed rows
                Dim dsTempAssetChanged As DataSet
                dsTempAssetChanged = dsTempAsset.GetChanges(DataRowState.Modified)

                Dim loCol As DataColumn
                'Prepare XML output from the DataSet as a string
                For Each loCol In dsTempAssetChanged.Tables(0).Columns
                    loCol.ColumnMapping = System.Data.MappingType.Attribute
                Next
                sXMLString = dsTempAssetChanged.GetXml
            End If

            fnGetTempXMLRec = Trim(sXMLString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub fnValidateSelectRecord(ByRef strErrorF As String, ByRef strErrorMsg As String)
        Try
            Dim strSelectedRecF As String = "N"
            Dim arrLoop1() As String
            Dim intLoop1 As Integer = "0"
            Dim intLoop2 As Integer = "0"

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As DropDownList
            Dim strNFSIds As String = ""
            For Each GridItem In dgAssetTemp.Items
                chkSelectedRec = CType(GridItem.Cells(8).FindControl("ddlAssetIdNFS"), DropDownList)
                If Not chkSelectedRec.SelectedValue = "-1" Then
                    strSelectedRecF = "Y"
                    strNFSIds += chkSelectedRec.SelectedValue & "^"
                End If
            Next

            ''*** Check for whether record selected 
            If strSelectedRecF = "N" Then
                strErrorF = "Y"
                strErrorMsg = "At least one record should be selected for merge temporary asset with NFS asset."
            End If

            ''*** Check for duplicate selection
            If strErrorF = "N" Then
                arrLoop1 = Split(strNFSIds, "^")
                For intLoop1 = "0" To UBound(arrLoop1) - 1
                    For intLoop2 = intLoop1 + 1 To UBound(arrLoop1) - 1
                        If arrLoop1(intLoop1) = arrLoop1(intLoop2) Then
                            strErrorF = "Y"
                            strErrorMsg = "Duplicate NFS Asset ID was selected."
                            Exit For
                        End If
                    Next
                Next
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class