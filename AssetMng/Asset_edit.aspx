<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Asset_edit.aspx.vb" Inherits="AMS.Asset_edit" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<%@ Register TagPrefix="fluent" Namespace="Fluent.MultiLineTextBoxValidator" Assembly="Fluent.MultiLineTextBoxValidator" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>	
	<LINK href="../common/ComboBox/styles/contactsearch/combo.css" type="text/css" rel="Stylesheet">
    <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
    <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />
	<script  type="text/javascript">
	    $(document).ready(function () {
	        SearchText("cbOwner", "hfOwner");
	    });
	    function SearchText(obj1, obj2) {
	        $("#" + obj1).autocomplete({
	            source: function (request, response) {
	                $.ajax({
	                    url: '<%#ResolveUrl("~/AssetMng/asset_edit.aspx/GetUsersList") %>',
	                    data: "{ 'prefix': '" + request.term + "'}",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    success: function (data) {
	                        response($.map(data.d, function (item) {
	                            return {
	                                label: item.split('~')[0],
	                                val: item.split('~')[1]
	                            }
	                        }))
	                    },
	                    error: function (response) {
	                        alert(response.responseText);
	                    },
	                    failure: function (response) {
	                        alert(response.responseText);
	                    }
	                });
	            },
	            select: function (e, i) {
	                $("#" + obj2).val(i.item.val);
	            },
	            minLength: 0
	        });

	    }
	    function mfnOpenAddAssetImagesWindow() 
			{
			    var AssetID = document.Frm_AssetEdit.hdnAssetID.value;
			    var ABarcode = document.getElementById('lblAssetIDAMS').innerHTML;
			    			
			    //open pop-up window
		        window.open('../common/AddNewFile.aspx?CallFrm=EditAsset&ASIID=' + AssetID + '&ABarcode=' + ABarcode, 'AddFile','width=450,height=250,Top=0,left=0,scrollbars=1');
				return false;
			}
		function mfnOpenAddStatusFileWindow(cType) 
			{
			    var CallFrm, StatusInfoID;
			    var ABarcode = document.getElementById('lblAssetIDAMS').innerHTML;
			    var AssetID = document.Frm_AssetEdit.hdnAssetID.value;
			    var AssetStatus = '';
			    
			    if(cType == 'CR'){
			        if (document.Frm_AssetEdit.hdnRedundancyF.value == 'Y'){   //Already have redundant record, add file permanently
			            CallFrm = 'AddStatusRCFileP';
			            StatusInfoID = document.Frm_AssetEdit.hdnRedundancyID.value;
			            AssetStatus = 'R'
			        }
			        else if (document.Frm_AssetEdit.hdnCondemnF.value == 'Y'){ //Already have condemnation record, add file permanently
			            CallFrm = 'AddStatusRCFileP';
			            StatusInfoID = document.Frm_AssetEdit.hdnCondemID.value;
			            AssetStatus = 'C'
			        }
			        else {    //Already not have redundant/condemnation record, add file temporary
			            CallFrm = 'AddStatusRCFileT';
			            StatusInfoID = '0';
			        }
			    }
			    else if (cType == 'D'){
			        if (document.Frm_AssetEdit.hdnDisposedF.value == 'Y'){
			            CallFrm = 'AddStatusDFileP';
			            StatusInfoID = document.Frm_AssetEdit.hdnDisposedID.value;
			            AssetStatus = 'D'
			        }
			        else {
			            CallFrm = 'AddStatusDFileT';
			            StatusInfoID = '0';
			        }
			    }
			
			    //open pop-up window
		        window.open('../common/AddNewFile.aspx?CallFrm=' + CallFrm + '&ASIID=' + StatusInfoID + '&ABarcode=' + ABarcode + '&AssetID=' + AssetID + '&AssetStatus=' + AssetStatus, 'AddFile','width=450,height=250,Top=0,left=0,scrollbars=1');
				return false;
			}
		function chkFrm() {
			var foundError = false;
			var i = 0;
			var j = 0;
			var curRow = 0;
			
			//Validate Additional Information 
			if (!foundError){
			    for (i=1;i<=5;i++){
			       if (eval('document.Frm_AssetEdit.hdnSCatAddInfo'+i+'.value') == ''){
			            break;
			       }
			       else{
			            if (eval('document.Frm_AssetEdit.hdnSCatAddInfoMO'+i+'.value') == 'Y' && eval('document.Frm_AssetEdit.txtAddInfo'+i+'.value') == ''){
			                foundError=true;
			                eval('document.Frm_AssetEdit.txtAddInfo'+i+'.focus()');
			                alert("Please enter " + eval('document.Frm_AssetEdit.hdnSCatAddInfo'+i+'.value') + ".");
			                break;
			            }
			       } 
			    }
			}
			
			//Validate Brand
//			if (!foundError && gfnIsFieldBlank(document.Frm_AssetEdit.txtBrand)) {
//				foundError=true;
//				document.Frm_AssetEdit.txtBrand.focus();
//				alert("Please enter Brand.");
//			}
			
			//Validate Cost (just for Fixed Asset)
			if (!foundError && document.Frm_AssetEdit.hdnAssetType.value == 'A'){
                if (!foundError && gfnIsFieldBlank(document.Frm_AssetEdit.txtCost)) {
				    foundError=true;
				    document.Frm_AssetEdit.txtCost.focus();
				    alert("Please enter Cost.");
			    }   
   
            }
            
            //Validate Cost (if Cost not blank)
            if (!foundError && gfnIsFieldBlank(document.Frm_AssetEdit.txtCost)== false) {
			    if (!foundError && gfnCheckNumeric(document.Frm_AssetEdit.txtCost,'.')) {
				    foundError=true;
				    document.Frm_AssetEdit.txtCost.focus();
				    alert("Cost just allow number only.");
			    } 
		    }
		    
		    //validate purchase date
			if (!foundError && gfnCheckDate(document.Frm_AssetEdit.txtPurchaseDt, "Purchase Date", "M") == false) {
				foundError=true
			}
            
			//validate Warranty Expiry Date
		    if (!foundError && gfnCheckDate(document.Frm_AssetEdit.txtWarExpDt, "Warranty Expiry Date", "O") == false) {
				foundError=true
			}
			if (!foundError && gfnIsFieldBlank(document.Frm_AssetEdit.txtWarExpDt) == false) {
			    if (!foundError && gfnCheckFrmToDt(document.Frm_AssetEdit.txtPurchaseDt,document.Frm_AssetEdit.txtWarExpDt,'<=') == false) {
				    foundError=true
				    document.Frm_AssetEdit.txtWarExpDt.focus()
				    alert("Warranty Expiry Date should be greater than Purchase Date.");
			    }
			}
			
			//validate for department
			if (!foundError && document.Frm_AssetEdit.ddlDepartment.value == "-1") {
				foundError=true
				document.Frm_AssetEdit.ddlDepartment.focus()
				alert("Please select the Department.")
			}
			
			//validate for Location
			//--> if Location is seleted, Sub Location must selected.
			if (!foundError && document.Frm_AssetEdit.ddlLocation.value != "-1") {
				if (!foundError && document.Frm_AssetEdit.ddlLocSub.value == "-1") {
				    foundError=true
				    document.Frm_AssetEdit.ddlLocSub.focus()
				    alert("Please select the Sub Location.")
			    }
			}
			
			//Validate Owner
//			if (!foundError && !gfnIsFieldBlank(document.Frm_AssetEdit.cbOwner)) {
//			    if (!foundError && gfnIsFieldBlank(document.Frm_AssetEdit.cbOwnerSelectedValue0)) {
//		                foundError=true
//		                alert("Owner not found. Please select an existing Owner.")
//                }
//            }
			
									 
 			if (!foundError){
 			    var flag = false;
 				flag = window.confirm("Are you sure you want to update this Asset?");
 				return flag;
 			}
			else
				return false;
		}
	</script>
</head>
<body>
    <form id="Frm_AssetEdit" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Asset Management : Modify Asset</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" height="500" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																				    <TR>
																						<TD valign="middle" width="35%">
																						    <FONT class="DisplayTitle">Asset ID (AMS) : </FONT>
																						</TD>
																						<TD width="65%">
																							<asp:Label ID="lblAssetIDAMS" runat=server></asp:Label>
																						</TD>
																					</TR>
																					<TR>
																						<TD>
																						    <FONT class="DisplayTitle">Asset ID (NFS) : </FONT>
																						</TD>
																						<TD width="65%">
																							<asp:Label ID="lblAssetIdNFS" runat=server></asp:Label>
																						</TD>
																					</TR>
																					<TR>
																						<TD>
																						    <FONT class="DisplayTitle">Asset Creation Date : </FONT>
																						</TD>
																						<TD width="65%">
																							<asp:Label ID="lblAssetCreateDt" runat=server></asp:Label>
																						</TD>
																					</TR>
																					<TR>
																						<TD>
																						    <FONT class="DisplayTitle">Asset Type : </FONT>
																						</TD>
																						<TD width="65%">
																							<asp:Label ID="lblAssetType" runat=server></asp:Label>
																						</TD>
																					</TR>
																			        <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Asset Category : </FONT>
																					    </TD>
																					    <TD>
																					       <asp:Label ID="lblAssetCat" runat=server></asp:Label>
																						</TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Asset Subcategory : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:DropDownList ID="ddlAssetSubCat" Runat="server" AutoPostBack=true></asp:DropDownList>
																						</TD>
																				    </TR>
																				    <tr>
																				        <td colspan=2>
																				            <!-- Start: Display Additional Informations for Subcategory -->
																							<div id=divAddInfo1 runat=server>
																							    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table9">
																							        <TR>
																					                    <TD width="35%">
																					                        <FONT class="DisplayTitle"><asp:Label ID=lblAddInfo1 runat=server></asp:Label> </FONT>
																					                    </TD>
																					                    <TD width="65%">
																					                        <asp:TextBox id="txtAddInfo1" maxlength="250" Runat="server" Width="300px"></asp:TextBox>
																						                </TD>
																				                    </TR>
																							    </TABLE>
																							</div>
																							<div id=divAddInfo2 runat=server>
																							    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table10">
																							        <TR>
																					                    <TD width="35%">
																					                        <FONT class="DisplayTitle"><asp:Label ID=lblAddInfo2 runat=server></asp:Label> </FONT>
																					                    </TD>
																					                    <TD width="65%">
																					                        <asp:TextBox id="txtAddInfo2" maxlength="250" Runat="server" Width="300px"></asp:TextBox>
																						                </TD>
																				                    </TR>
																							    </TABLE>
																							</div>
																							<div id=divAddInfo3 runat=server>
																							    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table11">
																							        <TR>
																					                    <TD width="35%">
																					                        <FONT class="DisplayTitle"><asp:Label ID=lblAddInfo3 runat=server></asp:Label> </FONT>
																					                    </TD>
																					                    <TD width="65%">
																					                        <asp:TextBox id="txtAddInfo3" maxlength="250" Runat="server" Width="300px"></asp:TextBox>
																						                </TD>
																				                    </TR>
																							    </TABLE>
																							</div>
																							<div id=divAddInfo4 runat=server>
																							    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table12">
																							        <TR>
																					                    <TD width="35%">
																					                        <FONT class="DisplayTitle"><asp:Label ID=lblAddInfo4 runat=server></asp:Label> </FONT>
																					                    </TD>
																					                    <TD width="65%">
																					                        <asp:TextBox id="txtAddInfo4" maxlength="250" Runat="server" Width="300px"></asp:TextBox>
																						                </TD>
																				                    </TR>
																							    </TABLE>
																							</div>
																							<div id=divAddInfo5 runat=server>
																							    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table13">
																							        <TR>
																					                    <TD width="35%">
																					                        <FONT class="DisplayTitle"><asp:Label ID=lblAddInfo5 runat=server></asp:Label> </FONT>
																					                    </TD>
																					                    <TD width="65%">
																					                        <asp:TextBox id="txtAddInfo5" maxlength="250" Runat="server" Width="300px"></asp:TextBox>
																						                </TD>
																				                    </TR>
																							    </TABLE>
																							</div>
																							<!-- End  : Display Additional Informations for Subcategory -->
																				        </td>
																				    </tr>
																				    <TR>
															                            <TD><FONT class="DisplayTitle">Controlled Item : </FONT></TD>
															                            <TD>
															                                <asp:radiobuttonlist id="rdCtrlItemF" Runat="server" RepeatDirection="Horizontal">
																	                            <asp:ListItem Value="Y">Yes</asp:ListItem>
																	                            <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
																                            </asp:radiobuttonlist>
															                            </TD>
														                            </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Brand : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtBrand" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Asset Status : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlAssetStatus" Runat="server" AutoPostBack=true></asp:DropDownList>
																						</TD>
																				    </TR>
																				    <TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle">Description: </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtDesc" TextMode=MultiLine maxlength="1000" Columns="40" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtDescWord" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="MMLValDesc" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Description to 1000 Characters." OutputControl="txtDescWord" ControlToValidate="txtDesc"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																				    <TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Cost Information</font></TD>
																					</TR>
																					<TR>
																					    <TD>
																					        <asp:Label ID=lblMandatory1 runat=server><font color="red">*</font></asp:Label><FONT class="DisplayTitle">Cost (S$) : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtCost" maxlength="250" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Cost Center : </FONT>
																					    </TD>
																					    <TD><asp:Label id="lblCostCenter" Runat="server"></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Net Book Value [NBV] (S$) : </FONT>
																					    </TD>
																					    <TD><asp:Label ID="lblNetBookValue" runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Accumulated Depreciation (S$) : </FONT>
																					    </TD>
																					    <TD><asp:Label ID="lblAccDepreciate" runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <font color="red">*</font><FONT class="DisplayTitle">Purchase Date : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:TextBox ID="txtPurchaseDt" Width="120" Runat="server"></asp:TextBox>
																						    <a id=linkPurchaseDt runat=server href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fSingleDtPop(document.Frm_AssetEdit.txtPurchaseDt);return false;"
																							    HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select Date">
																						    </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Warranty Expiry Date : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:TextBox ID="txtWarExpDt" Width="120" Runat="server"></asp:TextBox>
																						    <a id=linkWarExpDt runat=server href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fSingleDtPop(document.Frm_AssetEdit.txtWarExpDt);return false;"
																							    HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select Date">
																						    </a>
																					    </TD>
																				    </TR>
																				    <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																				    <TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Location Information</font></TD>
																					</TR>
																				    <TR>
																					    <TD>
																					        <font color="red">*</font><FONT class="DisplayTitle">Department : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:DropDownList ID="ddlDepartment" Runat="server"></asp:DropDownList>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Location : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:DropDownList ID="ddlLocation" Runat="server"  AutoPostBack=true></asp:DropDownList>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Sub Location : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:DropDownList ID="ddlLocSub" Runat="server"></asp:DropDownList>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Assigned Owner : </FONT>
																					    </TD>
																					    <TD>																						    
																					        <asp:TextBox ID="cbOwner" runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																				    <TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Others Information</font></TD>
																					</TR>
																					<TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle">Images/File(s) : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:LinkButton ID="butAddImages" runat="server">[Add New File]</asp:LinkButton>
																						    <!--Datagrid for display record.-->
									                                                        &nbsp;<span style="font-size:12.0pt;mso-bidi-font-size:10.0pt;
font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA"><v:shapetype
 id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
 path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f"><v:stroke joinstyle="miter" xmlns:v="urn:schemas-microsoft-com:vml"/><v:formulas><v:f 
                                                                                                eqn="if lineDrawn pixelLineWidth 0" xmlns:v="urn:schemas-microsoft-com:vml"/><v:f 
                                                                                                eqn="sum @0 1 0" xmlns:v="urn:schemas-microsoft-com:vml"/><v:f 
                                                                                                eqn="sum 0 0 @1" xmlns:v="urn:schemas-microsoft-com:vml"/><v:f 
                                                                                                eqn="prod @2 1 2" xmlns:v="urn:schemas-microsoft-com:vml"/><v:f 
                                                                                                eqn="prod @3 21600 pixelWidth" xmlns:v="urn:schemas-microsoft-com:vml"/><v:f 
                                                                                                eqn="prod @3 21600 pixelHeight" xmlns:v="urn:schemas-microsoft-com:vml"/><v:f 
                                                                                                eqn="sum @0 0 1" xmlns:v="urn:schemas-microsoft-com:vml"/><v:f 
                                                                                                eqn="prod @6 1 2" xmlns:v="urn:schemas-microsoft-com:vml"/><v:f 
                                                                                                eqn="prod @7 21600 pixelWidth" xmlns:v="urn:schemas-microsoft-com:vml"/><v:f 
                                                                                                eqn="sum @8 21600 0" xmlns:v="urn:schemas-microsoft-com:vml"/><v:f 
                                                                                                eqn="prod @7 21600 pixelHeight" xmlns:v="urn:schemas-microsoft-com:vml"/><v:f 
                                                                                                eqn="sum @10 21600 0" xmlns:v="urn:schemas-microsoft-com:vml"/></v:formulas><v:path 
                                                                                                o:extrusionok="f" gradientshapeok="t" o:connecttype="rect" 
                                                                                                xmlns:v="urn:schemas-microsoft-com:vml"/><o:lock v:ext="edit" 
                                                                                                aspectratio="t" xmlns:o="urn:schemas-microsoft-com:office:office"/></v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:249.75pt;
 height:11.25pt' o:ole=""><v:imagedata src="file:///C:\Users\T440\AppData\Local\Temp\msohtmlclip1\01\clip_image001.png"
  o:title="" xmlns:v="urn:schemas-microsoft-com:vml"/></v:shape><!--[if gte mso 9]><xml>
 <o:oleobject Type="Embed" ProgID="Paint.Picture" ShapeID="_x0000_i1025"
  DrawAspect="Content" ObjectID="_1533810824">
 </o:OLEObject>
                                                                                            </xml><![endif]--><FONT class="DisplayTitle"><span 
                                                                                                style="font-size: 10.0pt; line-height: 107%; font-family: &quot;Calibri&quot;,sans-serif; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA; color: #FF0000;"><strong>File 
                                                                                            size limit is 2 MB</strong></span></FONT></span>
                                                                                            <asp:datagrid id="dgAssetFileImages" Runat="server" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#CC9966" datakeyfield="fld_AssetFileID" BackColor="White" BorderWidth="1px" CellPadding="4">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="#FFFFCC" Font-Bold="True" HorizontalAlign="Center"
											                                                        ></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn headertext="fld_AssetFileID" datafield="fld_AssetFileID"  Visible=False>
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="fld_FileSaveName" datafield="fld_FileSaveName"  Visible=False>
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                        </asp:boundcolumn>
										                                                            <asp:boundcolumn headertext="S/No" datafield="SNum" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="File Name" datafield="fld_FileName">
												                                                        <itemstyle width="70%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn ItemStyle-Width="10%" HeaderText="<IMG SRC=../images/audit.gif Border=0>" 
							                                                                            ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Center>
								                                                                        <itemtemplate>
									                                                                        <%#fnGetFileName(DataBinder.Eval(Container.DataItem, "fld_FileSaveName"))%>
								                                                                        </itemtemplate>
							                                                                        </asp:templatecolumn>
																									<asp:ButtonColumn Text="&lt;IMG SRC=../images/delete.gif Border=0&gt;"
																										HeaderText="&lt;IMG SRC=../images/delete.gif Border=0&gt;" CommandName="Delete">
                                                                                                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                                                    </asp:ButtonColumn>									
										                                                        </Columns>
									                                                        </asp:datagrid>
                                                                                        </TD>
																				    </TR>										    
																				    <tr>
																					    <TD>
																					        <FONT class="DisplayTitle">Short Description : </FONT>
																					    </TD>
																					    <TD>
																					                        <asp:TextBox id="txtShortDescription" maxlength="250" 
                                                                                                Runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </tr>
                                                                                    <tr>
																					    <TD>
																					        <FONT class="DisplayTitle">Other Information : </FONT>
																					    </TD>
																					    <TD>
																					                        <asp:TextBox id="txtOtherInformation" maxlength="250" 
                                                                                                Runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </tr>
																				    <TR>
																					    <TD valign="top"><FONT class="DisplayTitle">Remarks : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:TextBox id="txtRemarks" maxlength="1000" TextMode="MultiLine" Columns="40" Rows="5" Runat="server"></asp:TextBox><br />
																					        <asp:textbox id="txtRemarksWord" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					        <fluent:multilinetextboxvalidator id="MLLValRemarks" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Remarks to 1000 Characters." OutputControl="txtRemarksWord" ControlToValidate="txtRemarks"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
                                                                                    <tr id="trSerialNo" runat="server">
																						                <TD valign="middle" width="35%">
																						                    <FONT class="DisplayTitle">Serial No : </FONT>
																						                </TD>
																						                <TD width="65%" >
																					                        <asp:TextBox id="txtSerialNo" maxlength="250" 
                                                                                                Runat="server" Width="300px"></asp:TextBox>
																						                </TD>
																					                </TR>
																							        <TR id="trMacAddress" runat="server">
																						                <TD valign="middle" width="35%">
																						                    <FONT class="DisplayTitle">Mac Address : </FONT>
																						                </TD>
																						                <TD width="65%">
																					                        <asp:TextBox id="txtMacAddress" maxlength="250" 
                                                                                                Runat="server" Width="300px"></asp:TextBox>
																						                </TD>
																					                </TR>
																							        <TR id="trHostName" runat="server">
																						                <TD valign="middle" width="35%">
																						                    <FONT class="DisplayTitle">New Hostname : </FONT>
																						                </TD>
																						                <TD width="65%">
																					                        <asp:TextBox id="txtHostName" maxlength="250" 
                                                                                                Runat="server" Width="300px"></asp:TextBox>
																						                </TD>
																					                </TR>
																							        <tr id="trStatus" runat="server">
																						                <TD valign="middle" width="35%">
																						                    <FONT class="DisplayTitle">Status :</FONT></TD>
																						                <TD width="65%">
																					                        <asp:RadioButtonList ID="rdolstStatus" runat="server" 
                                                                                                                RepeatDirection="Horizontal">
                                                                                                                <asp:ListItem Value="S">Spare</asp:ListItem>
                                                                                                                <asp:ListItem Value="L">Loan</asp:ListItem>
                                                                                                                <asp:ListItem Value="D">Deployed</asp:ListItem>
                                                                                                            </asp:RadioButtonList>
																						                </TD>
																					                </tr>
																							        <tr id="trDeviceType" runat="server">
																						                <TD valign="middle" width="35%">
																						                    <FONT class="DisplayTitle">Device Type : </FONT>
																						                </TD>
																						                <TD width="65%">
																					                        <asp:DropDownList ID="ddlDeviceType" runat="server">
                                                                                                            </asp:DropDownList>
																						                </TD>
																					                </tr>
																							        <tr id="trMachineModel" runat="server">
																						                <TD valign="middle" width="35%">
																						                    <FONT class="DisplayTitle">Machine Model : </FONT>
																						                </TD>
																						                <TD width="65%" >
																					                        <asp:TextBox id="txtMachineModel" maxlength="250" 
                                                                                                Runat="server" Width="300px"></asp:TextBox>
																						                </TD>
																					                </TR>
																				    <tr>
																				        <td colspan=2>
																				            <!-- Start: Temporary Asset just for AMS record -->
																							<div id=divTempAsset runat=server>
																							    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table14">
																							        
                                                                                                    <TR>
																						                <TD valign="middle" width="35%">
																						                    <FONT class="DisplayTitle">Temporary Asset : </FONT><img src="../images/question.jpg" alt="Temporary Assets are asset that are pending creation from NFS" />
																						                </TD>
																						                <TD width="65%">
																						                    <asp:Label ID=lblTempAsset runat=server></asp:Label>
																						                </TD>
																					                </TR>
																							    </TABLE>
																							</div>
																							<!-- End  : Temporary Asset just for AMS record -->
																						</td>
																					</tr>
																					<tr>
																				        <td colspan=2>
																				            <!-- Start: Condemnation/Redundancy Status Information -->
																							<div id=divCondemnRedundant runat=server>
																							    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table15">
																							        <TR>
																					                    <TD colSpan="2">&nbsp;</TD>
																				                    </TR>
																				                    <TR class="TRTitleBG">
																						                <TD>&nbsp;<b><asp:Label ID=lblCondemnRedunT1 runat= server></asp:Label> Information</b></TD>
																						                <TD align=right>&nbsp;<asp:LinkButton ID=butCondemnRedunReprintCert Text="[Reprint Certificate]" runat=server></asp:LinkButton>&nbsp;</TD>
																							        </TR>
																				                    <TR>
																					                    <TD valign="top" width="35%"><FONT class="DisplayTitle">Reason of <asp:Label ID=lblCondemnRedunT2 runat= server></asp:Label> : </FONT></TD>
																					                    <TD width="65%">
																					                        <asp:TextBox id="txtCondemnRedunReason" maxlength="1000" TextMode="MultiLine" Columns="40" Rows="5" Runat="server"></asp:TextBox><br />
																					                        <asp:textbox id="txtCondemnRedunReasonWord" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					                        <fluent:multilinetextboxvalidator id="MLLReasonCondemnRedun" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Reason to 1000 Characters." OutputControl="txtCondemnRedunReasonWord" ControlToValidate="txtCondemnRedunReason"></fluent:multilinetextboxvalidator>
																					                    </TD>
																				                    </TR>
																				                    <TR>
																					                    <TD valign="top" width="35%"><FONT class="DisplayTitle">Asset Included : </FONT></TD>
																					                    <TD width="65%">
																					                        <asp:Label ID="lblCondemnRedunAssetIncluded" runat=server></asp:Label>
																					                    </TD>
																				                    </TR>
																				                    <TR>
																					                    <TD valign=top>
																					                        <FONT class="DisplayTitle">File Uploaded : </FONT>
																					                    </TD>
																					                    <TD>
																						                    [<asp:LinkButton ID=butAddCondemnRedunFile Text="Add New File" runat=server></asp:LinkButton>]<br />
																						                    <!--Datagrid for display record.-->
									                                                                        <asp:datagrid id="dgCondemnRedunFile" Runat="server" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                                        BorderColor="#CC9966" datakeyfield="fld_FileID" BackColor="White" BorderWidth="1px" CellPadding="4">
										                                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="#FFFFCC" Font-Bold="True" HorizontalAlign="Center"
											                                                                        ></headerstyle>
										                                                                        <Columns>
										                                                                            <asp:boundcolumn headertext="fld_FileID" datafield="fld_FileID"  Visible=False>
												                                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                                        </asp:boundcolumn>
											                                                                        <asp:boundcolumn headertext="fld_FileSaveName" datafield="fld_FileSaveName"  Visible=False>
												                                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                                        </asp:boundcolumn>
										                                                                            <asp:boundcolumn headertext="S/No" datafield="SNum" >
												                                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                                        </asp:boundcolumn>
											                                                                        <asp:boundcolumn headertext="File Name" datafield="fld_FileName">
												                                                                        <itemstyle width="70%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                                        </asp:boundcolumn>
											                                                                        <asp:templatecolumn ItemStyle-Width="10%" HeaderText="<IMG SRC=../images/audit.gif Border=0>" 
							                                                                                            ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Center>
								                                                                                        <itemtemplate>
									                                                                                        <%#fnGetCondemnRedunDisposedFileName(DataBinder.Eval(Container.DataItem, "fld_FileSaveName"))%>
								                                                                                        </itemtemplate>
							                                                                                        </asp:templatecolumn>
																									                <asp:ButtonColumn Text="&lt;IMG SRC=../images/delete.gif Border=0&gt;"
																										                HeaderText="&lt;IMG SRC=../images/delete.gif Border=0&gt;" CommandName="Delete">
                                                                                                                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                                                                    </asp:ButtonColumn>									
										                                                                        </Columns>
									                                                                        </asp:datagrid>
																					                    </TD>
																				                    </TR>
																							    </TABLE>
																							</div>
																							<!-- End  : Condemnation/Redundancy Status Information -->
																							
																						    <!-- Start: Disposed Status Information -->
																							<div id=divDisposed runat=server>
																							    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table16">
																							        <TR>
																					                    <TD colSpan="2">&nbsp;</TD>
																				                    </TR>
																				                    <TR class="TRTitleBG">
																						                <TD colspan="2">&nbsp;<b>Disposed Information</b></TD>
																					                </TR>
																							        <TR>
																					                    <TD valign="top" width="35%"><FONT class="DisplayTitle">Reason of Disposed : </FONT></TD>
																					                    <TD width="65%">
																					                        <asp:TextBox id="txtDisposedReason" maxlength="1000" TextMode="MultiLine" Columns="40" Rows="5" Runat="server"></asp:TextBox><br />
																					                        <asp:textbox id="txtDisposedReasonWord" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					                        <fluent:multilinetextboxvalidator id="MMLDisposedReasonWord" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Disposed Reason to 1000 Characters." OutputControl="txtDisposedReasonWord" ControlToValidate="txtDisposedReason"></fluent:multilinetextboxvalidator>
																					                    </TD>
																				                    </TR>
																				                    <TR>
																					                    <TD valign="top" width="35%"><FONT class="DisplayTitle">Asset Included : </FONT></TD>
																					                    <TD width="65%">
																					                        <asp:Label ID="lblDisposedAssetIncluded" runat=server></asp:Label>
																					                    </TD>
																				                    </TR>
																				                    <TR>
																					                    <TD valign=top>
																					                        <FONT class="DisplayTitle">File Uploaded : </FONT>
																					                    </TD>
																					                    <TD>
																						                    [<asp:LinkButton ID=butAddDisposedFile Text="Add New File" runat=server></asp:LinkButton>]<br />
																						                    <!--Datagrid for display record.-->
									                                                                        <asp:datagrid id="dgDisposedFile" Runat="server" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                                        BorderColor="#CC9966" datakeyfield="fld_FileID" BackColor="White" BorderWidth="1px" CellPadding="4">
										                                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="#FFFFCC" Font-Bold="True" HorizontalAlign="Center"
											                                                                        ></headerstyle>
										                                                                        <Columns>
										                                                                            <asp:boundcolumn headertext="fld_FileID" datafield="fld_FileID"  Visible=False>
												                                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                                        </asp:boundcolumn>
											                                                                        <asp:boundcolumn headertext="fld_FileSaveName" datafield="fld_FileSaveName"  Visible=False>
												                                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                                        </asp:boundcolumn>
										                                                                            <asp:boundcolumn headertext="S/No" datafield="SNum" >
												                                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                                        </asp:boundcolumn>
											                                                                        <asp:boundcolumn headertext="File Name" datafield="fld_FileName">
												                                                                        <itemstyle width="70%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                                        </asp:boundcolumn>
											                                                                        <asp:templatecolumn ItemStyle-Width="10%" HeaderText="<IMG SRC=../images/audit.gif Border=0>" 
							                                                                                            ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Center>
								                                                                                        <itemtemplate>
									                                                                                        <%#fnGetCondemnRedunDisposedFileName(DataBinder.Eval(Container.DataItem, "fld_FileSaveName"))%>
								                                                                                        </itemtemplate>
							                                                                                        </asp:templatecolumn>
																									                <asp:ButtonColumn Text="&lt;IMG SRC=../images/delete.gif Border=0&gt;"
																										                HeaderText="&lt;IMG SRC=../images/delete.gif Border=0&gt;" CommandName="Delete">
                                                                                                                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                                                                    </asp:ButtonColumn>									
										                                                                        </Columns>
									                                                                        </asp:datagrid>
																					                    </TD>
																				                    </TR>
																							    </TABLE>
																							</div>
																							<!-- End  : Disposed Status Information -->
																							
																							<!-- Start: Contract Information -->
																							<div id=divContract runat=server>
																							    <TR>
																						            <TD colSpan="2">&nbsp;</TD>
																					            </TR>
																				                <TR class="TRTitleBG">
																						            <TD colSpan="2">&nbsp;<b>Contract Information</b></TD>
																					            </TR>
																					            <TR>
																						            <TD colSpan="2">
																						                <asp:datagrid id="dgContract" Runat="server" AlternatingItemStyle-Height="25"
										                                                                    ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                                    BorderColor="#ffffff"  datakeyfield="fld_ContractID" AlternatingItemStyle-BackColor="#e3d9ee" >
										                                                                    <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                                    Height="25"></headerstyle>
										                                                                    <Columns>
										                                                                        <asp:boundcolumn HeaderText="Contract ID" datafield="fld_ContractID" Visible=false>
												                                                                    <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                                    </asp:boundcolumn>
										                                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                                    <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                                    </asp:boundcolumn>
											                                                                    <asp:boundcolumn HeaderText="Contract ID" datafield="fld_ContractID">
												                                                                    <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                                    </asp:boundcolumn>
											                                                                    <asp:boundcolumn HeaderText="Contract Title" datafield="fld_ContractTitle">
												                                                                    <itemstyle width="30%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                                    </asp:boundcolumn>
											                                                                    <asp:boundcolumn HeaderText="Contract Cost" datafield="fld_ContractCost">
												                                                                    <itemstyle width="15%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                                    </asp:boundcolumn>
											                                                                    <asp:boundcolumn HeaderText="Contract From Date" datafield="fld_ContractFDt">
												                                                                    <itemstyle width="20%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                                    </asp:boundcolumn>
											                                                                    <asp:boundcolumn HeaderText="Contract To Date" datafield="fld_ContractTDt">
												                                                                    <itemstyle width="20%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                                    </asp:boundcolumn>
										                                                                    </Columns>
									                                                                    </asp:datagrid>
																						            </TD>
																					            </TR>
																							</div>
																							<!-- End  : Contract Information -->
																						</td>
																					</tr>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																				    <TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Assigned Location History</b></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																						    <asp:datagrid id="dgAssetMv" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Right"
										                                                        PagerStyle-VerticalAlign="Middle" PagerStyle-Mode="NumericPages" PagerStyle-Position="Top"
										                                                        PagerStyle-BackColor="#a3a9cc" datakeyfield="fld_AssetMvID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Movement ID" datafield="fld_AssetMvID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
										                                                            <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn HeaderText="Department" datafield="fld_DepartmentName">
												                                                        <itemstyle width="20%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn HeaderText="Location" datafield="fld_LocationName">
												                                                        <itemstyle width="20%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn HeaderText="Sub Location" datafield="fld_LocSubName">
												                                                        <itemstyle width="20%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn HeaderText="Owner" datafield="fld_OwnerName">
												                                                        <itemstyle width="20%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn HeaderText="Date Movement" datafield="fld_CreatedDt" dataformatstring="{0:dd/MM/yyyy HH:mm:ss}" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
                                                                                                    <asp:boundcolumn HeaderText="Trans Date" datafield="fld_TransDate" dataformatstring="{0:dd/MM/yyyy HH:mm:ss}" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign="Center"></itemstyle>
											                                                        </asp:boundcolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																				    <TR style="display:none" class="TRTitleBG"> <!--Removed by YK 20160810-->
																						<TD colSpan="2">&nbsp;<b>RFID Movement History</b></TD>
																					</TR>
																					<TR style="display:none"> <!--Removed by YK 20160810-->
																						<TD colSpan="2">
																						    <asp:datagrid id="dgRFIDMv" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" AllowPaging="True" PageSize="5" PagerStyle-HorizontalAlign="Right"
										                                                        PagerStyle-VerticalAlign="Middle" PagerStyle-Mode="NumericPages" PagerStyle-Position="Top"
										                                                        PagerStyle-BackColor="#a3a9cc" datakeyfield="fld_RFIDMvID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Movement ID" datafield="fld_RFIDMvID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
										                                                            <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn HeaderText="Location" datafield="fld_LocationName">
												                                                        <itemstyle width="40%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn HeaderText="Sub Location" datafield="fld_LocSubName">
												                                                        <itemstyle width="40%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn HeaderText="Date Movement" datafield="fld_CreatedDt" dataformatstring="{0:dd/MM/yyyy HH:mm:ss}" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
																						</TD>
																					</TR>
																					<TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:Button id="butSave" Text="Save" Runat="Server" />&nbsp;
																							<asp:Button id="butCancel" Text="Cancel" Runat="Server" />
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnCurrentDate" runat="server">   
			<input type="hidden" id="hdnAssetID" runat="server">
			<input type="hidden" id="hdnAssetType" runat="server">
			<input type="hidden" id="hdnCallFrm" runat="server">
			<input type="hidden" id="hdnSCatAddInfo1" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfo2" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfo3" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfo4" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfo5" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfoMO1" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfoMO2" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfoMO3" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfoMO4" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfoMO5" runat="server">
			<input type="hidden" id="hdnAssetStatus" runat="server">
			<input type="hidden" id="hdnRedundancyF" runat="server">
			<input type="hidden" id="hdnRedundancyID" runat="server">
			<input type="hidden" id="hdnCondemnF" runat="server">
			<input type="hidden" id="hdnCondemID" runat="server">
			<input type="hidden" id="hdnDisposedF" runat="server">
			<input type="hidden" id="hdnDisposedID" runat="server">
			<input type="hidden" id="hdnAddAssetFileF" runat="server">
			<input type="hidden" id="hdnAddStatusFileRCF" runat="server">
			<input type="hidden" id="hdnAddStatusFileDF" runat="server">
            <input type="hidden" id="hfOwner" runat="server">
			<!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
				src="../common/DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="Z-INDEX:101; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px">
			</iframe>
			<!-- End  : Hidden Fields -->
		</form>
</body>
</html>
