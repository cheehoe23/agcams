#Region "Information Section"
' ****************************************************************************************************
' Description       : View Audit Trail
' Purpose           : To view Audit Trail for particular asset
' Date              : 29/10/2007
' **************************************************************************************************** 
#End Region

Partial Public Class Asset_AuditTrail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                hdnAssetID.Value = Request("AssetID") + "^"
                lblAssetID.Text = Request("AssetIDT")

                ''default Sorting
                hdnSortName.Value = "fld_AuditDate"
                hdnSortAD.Value = "DESC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Sort Header Display
            fnSortHeaderDisplay()
            
            ''Get User Group Records
            dgViewAudit.DataSource = clsAsset.fnAssetViewAuditTrail(hdnAssetID.Value, hdnSortName.Value, hdnSortAD.Value)
            dgViewAudit.DataBind()
            If Not dgViewAudit.Items.Count > 0 Then ''Not Records found
                dgViewAudit.Visible = False
                ddlPageSize.Enabled = False
                lblErrorMessage.Text = "Not Records Found."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By fld_AuditDate (1), fld_AuditAction (2), fld_usrName(3)
        Dim intSortIndex As Integer
        Dim strSortHeader As String = ""

        ''Set Default Header
        dgViewAudit.Columns(1).HeaderText = "Date"
        dgViewAudit.Columns(2).HeaderText = "Action"
        dgViewAudit.Columns(3).HeaderText = "Action By"

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fld_AuditDate"
                intSortIndex = 1
                strSortHeader = "Date"
            Case "fld_AuditAction"
                intSortIndex = 2
                strSortHeader = "Action"
            Case "fld_usrName"
                intSortIndex = 3
                strSortHeader = "Action By"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgViewAudit.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgViewAudit.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Private Sub dgViewAudit_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgViewAudit.PageIndexChanged
        dgViewAudit.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub dgViewAudit_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgViewAudit.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgViewAudit.CurrentPageIndex = 0
        dgViewAudit.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub
End Class