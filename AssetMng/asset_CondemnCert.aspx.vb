Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Partial Public Class asset_CondemnCert
    Inherits System.Web.UI.Page
    Private report As New ReportDocument()
    Private report2 As New ReportDocument()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                hdnStatusInfoID.Value = Request("StatusInfoID")
            End If

            Dim myConnection As New SqlClient.SqlConnection()
            myConnection.ConnectionString = cnFTSDBString
            Dim MyCommand As New SqlClient.SqlCommand()
            MyCommand.Connection = myConnection
            MyCommand.CommandType = CommandType.StoredProcedure
            MyCommand.CommandText = "P_StatusInfo_GetRecForCertificate"
            MyCommand.Parameters.Add("@intStatusInfoID", SqlDbType.Int, 10)
            MyCommand.Parameters(0).Value = hdnStatusInfoID.Value
            Dim MyDA As New SqlClient.SqlDataAdapter()
            MyDA.SelectCommand = MyCommand
            Dim ds As New DataSet
            MyDA.Fill(ds, "tbl_StatusCert")

            ''*** 1. Create a report object
            'Dim report As New ReportDocument()
            Dim rptFile As String = Server.MapPath("ReportFile\CondemnCert.rpt")
            report.Load(rptFile)
            report.SetDataSource(ds)
            Me.crvCondemnCert1.ReportSource = report

            ''*** 2. Create a report object
            'Dim report2 As New ReportDocument()
            Dim rptFile2 As String = Server.MapPath("ReportFile\CondemnCert2.rpt")
            report2.Load(rptFile2)
            report2.SetDataSource(ds)
            Me.crvCondemnCert2.ReportSource = report2

            myConnection.Close()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            report.Close()
            report.Dispose()

            report2.Close()
            report2.Dispose()

            GC.Collect()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

End Class