#Region "Information Section"
' ****************************************************************************************************
' Description       : Float Control 
' Purpose           : View Float Control  Information
' Date              : 27/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class FloatCtrl_view
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                
                ''Checking For Assign Right  
                If (InStr(Session("AR"), "AssetMng|FloatCtrl|export") = 0) Then butExpExcel.Visible = False
                If (InStr(Session("AR"), "AssetMng|FloatCtrl|print") = 0) Then butPrint.Visible = False

                ''default Sorting
                hdnSortName.Value = "fld_CategoryCode"
                hdnSortAD.Value = "ASC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Sort Header Display
            fnSortHeaderDisplay()
           
            ''Get Records
            dgFloatCtrl.DataSource = clsAsset.fnAssetFloatCtrl_GetRecord(hdnSortName.Value, hdnSortAD.Value)
            dgFloatCtrl.DataBind()
            If Not dgFloatCtrl.Items.Count > 0 Then ''Not Records found
                ddlPageSize.Enabled = False
                butExpExcel.Enabled = False
                butPrint.Enabled = False
                lblErrorMessage.Text = "There is no record."
                lblErrorMessage.Visible = True
            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By fld_CategoryCode (2), fld_CategoryName (3), fld_CatSubCode (4), fld_CatSubName (5)
        Dim intSortIndex As Integer
        Dim strSortHeader As String = ""

        ''Set Default Header
        dgFloatCtrl.Columns(2).HeaderText = "Category Code"
        dgFloatCtrl.Columns(3).HeaderText = "Category Name"
        dgFloatCtrl.Columns(4).HeaderText = "Subcategory Code"
        dgFloatCtrl.Columns(5).HeaderText = "Subcategory Name"

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fld_CategoryCode"
                intSortIndex = 2
                strSortHeader = "Category Code"
            Case "fld_CategoryName"
                intSortIndex = 3
                strSortHeader = "Category Name"
            Case "fld_CatSubCode"
                intSortIndex = 4
                strSortHeader = "Subcategory Code"
            Case "fld_CatSubName"
                intSortIndex = 5
                strSortHeader = "Subcategory Name"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgFloatCtrl.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgFloatCtrl.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Private Sub dgFloatCtrl_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgFloatCtrl.PageIndexChanged
        dgFloatCtrl.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub dgFloatCtrl_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgFloatCtrl.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgFloatCtrl.CurrentPageIndex = 0
        dgFloatCtrl.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Protected Sub butExpExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExpExcel.Click
        Try
            Dim strJavascript As String = ""
            strJavascript = "<script language = 'Javascript'>" & _
                            "window.open('FloatCtrl_ExpExcel.aspx', 'Print_Screen','width=700,height=500,Top=0,left=0,scrollbars=1, resizable=1,menubar=1');" & _
                            "</script>"
            Response.Write(strJavascript)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPrint.Click
        Try
            Dim strJavascript As String = ""
            strJavascript = "<script language = 'Javascript'>" & _
                            "window.open('FloatCtrl_print.aspx', 'Print_Screen','width=700,height=500,Top=0,left=0,scrollbars=1, resizable=1,menubar=1');" & _
                            "</script>"
            Response.Write(strJavascript)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class