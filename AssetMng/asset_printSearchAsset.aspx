<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="asset_printSearchAsset.aspx.vb" Inherits="AMS.asset_printSearchAsset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
</head>
<body>
    <form id="frn_KIVprint" method="post" runat="server">
        <span style="font-size: 10pt">
        <b>Asset Record</b><br />
        </span>
        Printed Date : <asp:Label ID="lblPrintDate" runat="server"></asp:Label> <br /><br />
		<!--Datagrid for display record.-->
        <asp:datagrid id="dgAsset" Runat="server" AlternatingItemStyle-Height="25"
			ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
			BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" 
			datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
			<headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
				Height="25"></headerstyle>
			<Columns>
				<asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
					<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
				</asp:boundcolumn>
				<asp:boundcolumn HeaderText="Asset ID" datafield="fldAssetBarcode" SortExpression="fldAssetBarcode">
					<itemstyle width="15%" cssclass="GridText" verticalalign="Middle"></itemstyle>
				</asp:boundcolumn>
				<asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr" SortExpression="fld_AssetTypeStr" >
					<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
				</asp:boundcolumn>
				<asp:boundcolumn headertext="Category" datafield="fld_CategoryName" SortExpression="fld_CategoryName">
					<itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
				</asp:boundcolumn>
				<asp:boundcolumn headertext="Subcategory" datafield="fld_CatSubName" SortExpression="fld_CatSubName">
					<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
				</asp:boundcolumn>
				<asp:boundcolumn headertext="Department" datafield="fld_DepartmentName" SortExpression="fld_DepartmentName">
					<itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
				</asp:boundcolumn>
				<asp:boundcolumn headertext="Location" datafield="fld_LocationName" SortExpression="fld_LocationName">
					<itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
				</asp:boundcolumn>
				<asp:boundcolumn headertext="Sub Location" datafield="fld_LocSubName" SortExpression="fld_LocSubName">
					<itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
				</asp:boundcolumn>
				<asp:boundcolumn headertext="Assigned Owner" datafield="fld_OwnerName" SortExpression="fld_OwnerName">
					<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
				</asp:boundcolumn>
				<asp:boundcolumn headertext="Status" datafield="fld_AssetStatusStr" SortExpression="fld_AssetStatusStr">
					<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
				</asp:boundcolumn>
				<asp:boundcolumn headertext="Movement Status" datafield="DGLastMv" SortExpression="DGLastMv">
					<itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
				</asp:boundcolumn>
			</Columns>
		</asp:datagrid>
		</form>
		<script type="text/javascript">
		//call to print this page
		window.print()
		window.onfocus = function () { window.close(); }
		//Close Window
		//window.close()
		</script>
</body>
</html>
