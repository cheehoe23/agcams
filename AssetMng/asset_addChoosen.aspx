<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="asset_addChoosen.aspx.vb" Inherits="AMS.asset_addChoosen" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script type="text/javascript">
	    function fnAdditionType(){
	        //document.Frm_AssetAddChoosen.rdCreateChoosen_0.checked
	        if (document.Frm_AssetAddChoosen.rdCreateChoosen[0].checked==true){
	            document.getElementById("divBulkAddition").style.display = 'none';
	        }
	        else{
	            document.getElementById("divBulkAddition").style.display = '';
	        }
	    }
	    function chkFrm(){
	        var foundError = false;
								
				
	        if (document.Frm_AssetAddChoosen.rdCreateChoosen[1].checked==true){
	            //validate Quantity
				//--> Checking Quantity cannot be blank
				if (!foundError && gfnIsFieldBlank(document.Frm_AssetAddChoosen.txtQuantity)) {
					foundError=true;
					document.Frm_AssetAddChoosen.txtQuantity.focus();
					alert("Please enter Quantity.");
				}
				//--> Checking Quantity much numeric
				if (!foundError && gfnCheckNumeric(document.Frm_AssetAddChoosen.txtQuantity,'')) {
					foundError=true;
					document.Frm_AssetAddChoosen.txtQuantity.focus();
					alert("Quantity just allow number only.");
				}
				//--> Checking Quantity much more than 1
				if (!foundError && parseInt(document.Frm_AssetAddChoosen.txtQuantity.value) < 2) {
					foundError=true;
					document.Frm_AssetAddChoosen.txtQuantity.focus();
					alert("Please make sure Quantity must more than 2 or equal to 2.");
				}
	        }
	        
	        if (!foundError)
 				return true;
			else
				return false;
	    }
	</script>
</head>
<body>
    <form id="Frm_AssetAddChoosen" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Asset Management : Create New Asset</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" height="400" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																					<TR>
																						<TD valign="middle" width="35%">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Type of Addition Asset : </FONT>
																						</TD>
																						<TD width="65%">
																							<asp:radiobuttonlist id="rdCreateChoosen" Runat="server" RepeatDirection="Horizontal"></asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD colspan=2>
																						    <div id=divBulkAddition style="display:none;">
																							    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table9">
																							        <TR>
																					                    <TD width="35%">
																					                        <FONT class="DisplayTitle"><font color="red">*</font>Quantity : </FONT>
																					                    </TD>
																					                    <TD width="65%"><asp:TextBox id="txtQuantity" maxlength="250" Runat="server"></asp:TextBox></TD>
																				                    </TR>
																							    </TABLE>
																							</div>
																						</TD>
																					</TR>
																			        
																					<TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:Button id="butCreate" Text="Create New Asset" Runat="Server" />&nbsp;
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
	</form>
</body>
</html>
