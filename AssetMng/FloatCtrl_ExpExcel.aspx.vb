Public Partial Class FloatCtrl_ExpExcel
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If

            ''Get Today Date
            lblPrintDate.Text = mfnGetCurrentDateForTopPage()

            ''Bind record in datagrid
            dgFloatCtrl.DataSource = clsAsset.fnAssetFloatCtrl_GetRecord("fld_CategoryCode", "ASC")
            dgFloatCtrl.DataBind()

            ''Export to Excel
            'Response.ContentType = "application/ms-excel"
            'Response.AddHeader("Content-Disposition", "inline;filename=FloatControl.xls")

            Response.ContentType = "application/ms-excel"
            Response.AddHeader("Content-Disposition", "inline;filename=FloatControl.xls")
            HttpContext.Current.ApplicationInstance.CompleteRequest()
            

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class