<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FloatCtrl_ExpExcel.aspx.vb" Inherits="AMS.FloatCtrl_ExpExcel" ValidateRequest="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
</head>
<body>
    <form id="frmFloatCtrl" runat="server">
    <div>
        <span style="font-size: 10pt">
        <b>Float Control Record</b><br />
        </span>
        Printed Date : <asp:Label ID=lblPrintDate runat=server></asp:Label> <br /><br />
        <!--Datagrid for display record.-->
		<asp:datagrid id="dgFloatCtrl" Runat="server" AlternatingItemStyle-Height="25"
			ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
			BorderColor="#ffffff" datakeyfield="fld_CatSubID" AlternatingItemStyle-BackColor="#e3d9ee">
			<headerstyle verticalalign="Middle" BackColor="#a3a9cc" Font-Bold="True" HorizontalAlign="Center"
				Height="25"></headerstyle>
			<Columns>
			    <asp:boundcolumn HeaderText="fld_CatSubID" datafield="fld_CatSubID" Visible=false>
					<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
				</asp:boundcolumn>
				<asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
					<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
				</asp:boundcolumn>
				<asp:boundcolumn HeaderText="Category Code" datafield="fld_CategoryCode">
					<itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
				</asp:boundcolumn>
				<asp:boundcolumn HeaderText="Category Name" datafield="fld_CategoryName" >
					<itemstyle width="20%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
				</asp:boundcolumn>
				<asp:boundcolumn headertext="Subcategory Code" datafield="fld_CatSubCode" >
					<itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
				</asp:boundcolumn>
				<asp:boundcolumn headertext="Subcategory Name" datafield="fld_CatSubName">
					<itemstyle width="20%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
				</asp:boundcolumn>
				<asp:boundcolumn headertext="Number of Assets" datafield="TotAsset">
					<itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
				</asp:boundcolumn>	
				<asp:boundcolumn headertext="Float Level" datafield="fld_FloatLevel">
					<itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
				</asp:boundcolumn>									
			</Columns>
		</asp:datagrid>
    </div>
    </form>
</body>
</html>
