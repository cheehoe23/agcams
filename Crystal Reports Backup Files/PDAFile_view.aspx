<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDAFile_view.aspx.vb" Inherits="AMS.PDAFile_view" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
</head>
<body>
     <form id="Frm_UploadExcel" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Issuance Management : Generate PDA File</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="352" valign="top">
																		    <div id=divGenFile runat=server>
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=center>
																				    <TR>
																						<TD><b>Please click on button below to generate text file for Issuance.</b></TD>
																					</TR>
																					<TR>
																						<TD>&nbsp;</TD>
																					</TR>
																				    <TR>
																						<TD valign="middle">
																							<asp:Button ID=butGenFile runat=server Text="Generate PDA Text File" Width="199px" />
																						</TD>
																					</TR>
																					<TR>
																						<TD>&nbsp;</TD>
																					</TR>																																						
																				</TBODY>
																			</TABLE>
																			</div>
																			
																			<div id=divSaveFile runat=server>
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table9">
																				<TBODY align=center>
																				    <TR>
																						<TD>
																						<b>Text File generated successfully.</b><br />
																						Please remember to save the text file to PDA for issuance.<br />
																						Please right click on the link below and select "Save target as..." to save the file.
																						</TD>
																					</TR>
																					<TR>
																						<TD>&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD><b><asp:HyperLink ID=hylFileName runat=server Target="Issuance" Font-Size="11pt"></asp:HyperLink></b></TD>
																					</TR>
																					<TR>
																						<TD>&nbsp;</TD>
																					</TR>
																				    <TR>
																						<TD>
																						    <asp:Button ID="butBack" Text="Back to Generate Text File" runat=server Width="233px" />
																						</TD>
																					</TR>			
																				</TBODY>
																			</TABLE>
																			</div>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>																														
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>

		</form>

</body>
</html>
