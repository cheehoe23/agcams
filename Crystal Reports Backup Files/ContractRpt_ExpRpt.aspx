<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ContractRpt_ExpRpt.aspx.vb" Inherits="AMS.ContractRpt_ExpRpt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
</head>
<body>
        <b><asp:Label ID=lblRptTitle runat=server></asp:Label></b><br />
        Printed Date : <asp:Label ID=lblPrintDate runat=server></asp:Label> <br /><br />
		<!--Datagrid for display record.-->
        <asp:datagrid id="dgContract" Runat="server" AlternatingItemStyle-Height="25"
			ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
			BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" 
			datakeyfield="SNum" AlternatingItemStyle-BackColor="#e3d9ee"  >
			<headerstyle verticalalign="Middle" BackColor="#a3a9cc" Font-Bold="True" HorizontalAlign="Center"
				Height="25"></headerstyle>
				<Columns>
				    <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
					    <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
				    </asp:boundcolumn>
				</Columns>
		</asp:datagrid>
</body>
</html>
