<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="location_AddSub.aspx.vb" Inherits="AMS.location_AddSub" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS -- Add Sub Location</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet" />
	<script language="JavaScript" type="text/javascript" src="../Common/CommonJS.js"></script>
	<script language="javascript">
		function chkFrm() {
			var foundError = false;
			var hdnDuplicateStatus = document.frm_locationAdd.hdnDuplicateStatus.value;

			if (hdnDuplicateStatus == "1") {
			    foundError = true;
			    document.frm_locationAdd.txtSubLocCode.focus();
			    alert("Duplicate Sub Location Code in the Asset Management System! Please Try Again!");
			}

			//Validate Sub Location Code
			if (!foundError && gfnIsFieldBlank(document.frm_locationAdd.txtSubLocCode)) {
				foundError=true;
				document.frm_locationAdd.txtSubLocCode.focus();
				alert("Please enter Sub Location Code.");
			}
			if (!foundError && document.frm_locationAdd.txtSubLocCode.value.length > 10) {
				foundError=true
				document.frm_locationAdd.txtSubLocCode.focus()
				alert("Please make sure the Sub Location Code is less than 10 characters long.")
			}
			
			//Validate Sub Location Name
			if (!foundError && gfnIsFieldBlank(document.frm_locationAdd.txtSubLocName)) {
				foundError=true;
				document.frm_locationAdd.txtSubLocName.focus();
				alert("Please enter Sub Location Name.");
			}
			if (!foundError && document.frm_locationAdd.txtSubLocName.value.length > 250) {
				foundError=true
				document.frm_locationAdd.txtSubLocName.focus()
				alert("Please make sure the Sub Location Name is less than 250 characters long.")
			}
			
			//if Activate is "Yes", Light IP Add is mandatory field
			if (!foundError && document.frm_locationAdd.rdActivate_0.checked) {
					//Validate Light IP Add
			        if (!foundError && gfnIsFieldBlank(document.frm_locationAdd.txtLightIP)) {
				        foundError=true
				        document.frm_locationAdd.txtLightIP.focus()
				        alert("Please enter Light IP Address.")
			        }
			        if (!foundError && document.frm_locationAdd.txtLightIP.value.length > 20) {
				        foundError=true
				        document.frm_locationAdd.txtLightIP.focus()
				        alert("Please make sure the Light IP Address is less than 20 characters long.")
			        }
			        
			        //validate light type
			        if (!foundError && document.frm_locationAdd.ddlLightType.value == "0") {
				        foundError=true
				        document.frm_locationAdd.ddlLightType.focus()
				        alert("Please select the Light Type.")
			        }
			}
			
			//if Door Gantry is "Yes", Movement Status is mandatory field
			if (!foundError && document.frm_locationAdd.rdDoorGantryF_0.checked) {
					//Validate Movement Status
			        if (!foundError && gfnIsFieldBlank(document.frm_locationAdd.txtDGMvStatus)) {
				        foundError=true
				        document.frm_locationAdd.txtDGMvStatus.focus()
				        alert("Please enter Movement Status.")
			        }
			        if (!foundError && document.frm_locationAdd.txtDGMvStatus.value.length > 15) {
				        foundError=true
				        document.frm_locationAdd.txtDGMvStatus.focus()
				        alert("Please make sure the Movement Status is less than 15 characters long.")
			        }        
			}
					  
 			if (!foundError){
 			    var flag = false;
 				flag = window.confirm("Are you sure you want to add this Sub Location?");
 				return flag;
 			}
			else
				return false;
		}
	</script>
</head>
<body>
    <form id="frm_locationAdd" method="post" runat="server">
			<TABLE id="tableSubAdd" cellSpacing="1" cellPadding="1" width="90%" align="center"
				bgColor="white" border="0">
				<TBODY>
					<tr>
						<td colspan="2"><b>Add Sub Location</b></td>
					</tr>
					<TR>
						<TD align="center" colSpan="2"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></TD>
					</TR>
					<tr>
						<td colSpan="2">
						    <TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
									width="100%" ID="Table6">
									<TBODY>
										<TR>
											<TD height="4">
												<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
											</TD>
										</TR>
										<TR>
											<TD height="150" valign="top">
												<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
													<TBODY align=left>
													    <TR>
														    <TD valign="middle" width="35%">
														        <FONT class="DisplayTitle">Location Code : </FONT>
														    </TD>
														    <TD width="65%"><asp:Label id="lblLocCode" Runat="server"></asp:Label></TD>
													    </TR>
													    <TR>
														    <TD>
														        <FONT class="DisplayTitle">Location Name : </FONT>
														    </TD>
														    <TD><asp:Label id="lblLocName" Runat="server"></asp:Label></TD>
													    </TR>
													    <TR>
														    <TD>
														        <FONT class="DisplayTitle"><font color="red">*</font>Sub Location Code : </FONT>
														    </TD>
														    <TD width="65%">
															    <asp:textbox id="txtSubLocCode" Runat="server" MaxLength=8 AutoPostBack="True"></asp:textbox>&nbsp;
																<FONT class="DisplayFieldSize">(Maximum 8 characters)</FONT>
															</TD>
													    </TR>
													    <TR>
														    <TD>
														        <FONT class="DisplayTitle"><font color="red">*</font>Sub Location Name : </FONT>
														    </TD>
														    <TD><asp:textbox id="txtSubLocName" Runat="server" maxlength="250" Width="300px"></asp:textbox></TD>
													    </TR>
													    <TR style="visibility:hidden">
															<TD><FONT class="DisplayTitle">Smart Shelf Rack : </FONT></TD>
															<TD>
															    <asp:radiobuttonlist id="rdActivate" Runat="server" RepeatDirection="Horizontal" AutoPostBack=true>
																	<asp:ListItem Value="Y">Yes</asp:ListItem>
																	<asp:ListItem Value="N" Selected="True">No</asp:ListItem>
																</asp:radiobuttonlist>
															</TD>
														</TR>
														<TR>
															<TD colSpan="2">
															    <div id=divLightIP runat=server>
															        <TABLE id="Table9" cellSpacing="1" cellPadding="1" width="100%" border="0">
															        <TR>
															            <TD width="35%" align=left>
																            <FONT class="DisplayTitle"><font color="red">*</font>Light IP Address : </FONT>
															            </TD>
															            <TD width="65%" align=left><asp:textbox id="txtLightIP" Runat="server" maxlength="20" Width="300px"></asp:textbox></TD>
														            </TR>
														            <TR>
															            <TD width="35%" align=left>
																            <FONT class="DisplayTitle"><font color="red">*</font>Light Type : </FONT>
															            </TD>
															            <TD width="65%" align=left><asp:DropDownList ID="ddlLightType" Runat="server"></asp:DropDownList></TD>
														            </TR>
														            </TABLE>
														        </div>
															</TD>
														</TR>
														<TR style="visibility:hidden">
								                            <TD><FONT class="DisplayTitle">Door Gantry : </FONT></TD>
								                            <TD>
								                                <asp:radiobuttonlist id="rdDoorGantryF" Runat="server" RepeatDirection="Horizontal" AutoPostBack=true>
										                            <asp:ListItem Value="Y">Yes</asp:ListItem>
										                            <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
									                            </asp:radiobuttonlist>
								                            </TD>
							                            </TR>
							                            <TR>
															<TD colSpan="2">
															    <div id=divGoorGantry runat=server>
															        <TABLE id="Table10" cellSpacing="1" cellPadding="1" width="100%" border="0">
															        <TR>
															            <TD width="35%" align=left>
																            <FONT class="DisplayTitle"><font color="red">*</font>Movement Status : </FONT>
															            </TD>
															            <TD width="65%" align=left><asp:textbox id="txtDGMvStatus" Runat="server" maxlength="15" Width="300px"></asp:textbox></TD>
														            </TR>
														            </TABLE>
														        </div>
															</TD>
														</TR>
														<TR>
															<TD align="center" colspan="2"><BR>
																<asp:Button id="butSubmit" Text="Add" Runat="Server" />&nbsp;
																<asp:Button id="butReset" Text="Reset" Runat="Server" />&nbsp;
																<asp:Button id="butCancel" Text="Cancel" Runat="Server" />
															</TD>
														</TR>
													</TBODY>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD height="2"></TD>
										</TR>
									</TBODY>
								</TABLE>
					    </td>
					</tr>
				</TBODY>
			</TABLE>
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnAddSubLocF" runat="server">
			<input type="hidden" id="hdnCallFrm" runat="server"> 
			<input type="hidden" id="hdnLocID" runat="server"> 
			<input type="hidden" id="hdnSLocID" runat="server">
			<input type="hidden" id="hdnDeleteF" runat="server">  
            <input type="hidden" id="hdnDuplicateStatus" runat="server">             
			<!-- End  : Hidden Fields -->
		</form>
		<script language="javascript">
			if (document.frm_locationAdd.hdnAddSubLocF.value == 'Y')
			{	
				//Call From Add Location Screen
				if (document.frm_locationAdd.hdnCallFrm.value=='Add')
				{	
					var openerForm = opener.document.forms.frm_locationAdd;
					openerForm.action = "location_Add.aspx";
					window.opener.document.forms(0).hdnAddSubLocF.value = document.frm_locationAdd.hdnAddSubLocF.value;
					window.opener.document.forms(0).hdnAddSubLocCode.value = document.frm_locationAdd.txtSubLocCode.value;
					window.opener.document.forms(0).hdnAddSubLocName.value = document.frm_locationAdd.txtSubLocName.value;
					if (document.frm_locationAdd.rdActivate_0.checked) {
					    window.opener.document.forms(0).hdnAddSubLocActv.value = 'Y'; 
					    window.opener.document.forms(0).hdnAddSubLocLightIP.value = document.frm_locationAdd.txtLightIP.value;
					    window.opener.document.forms(0).hdnAddSubLocLightType.value = document.frm_locationAdd.ddlLightType.value;
					}
					else {
					    window.opener.document.forms(0).hdnAddSubLocActv.value = 'N'; 
					    window.opener.document.forms(0).hdnAddSubLocLightIP.value = '';
					    window.opener.document.forms(0).hdnAddSubLocLightType.value = '0';
					}
					if (document.frm_locationAdd.rdDoorGantryF_0.checked) {
					    window.opener.document.forms(0).hdnAddSubLocDGF.value = 'Y'; 
					    window.opener.document.forms(0).hdnAddSubLocDGMvS.value = document.frm_locationAdd.txtDGMvStatus.value;
					}
					else {
					    window.opener.document.forms(0).hdnAddSubLocDGF.value = 'N'; 
					    window.opener.document.forms(0).hdnAddSubLocDGMvS.value = '';
					}
				}	
				
				//Call From Edit Location Screen
				if (document.frm_locationAdd.hdnCallFrm.value=='Edit')
				{
					var openerForm = opener.document.forms.frm_locationEdit;
					openerForm.action = "location_Edit.aspx?loctID=" + document.frm_locationAdd.hdnLocID.value;
					window.opener.document.forms(0).hdnAddSubLocF.value = document.frm_locationAdd.hdnAddSubLocF.value;
				}
				
					openerForm.method = "post";
					openerForm.submit();
					window.close();	
			}
		</script>
</body>
</html>
