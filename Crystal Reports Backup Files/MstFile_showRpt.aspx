<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MstFile_showRpt.aspx.vb" Inherits="FTS.MstFile_showRpt"%>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>MstFile_showRpt</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<CR:CrystalReportViewer id="CRVMstListRpt" Width="350px" Height="50px"
				runat="server" 
            AutoDataBind="true" HasCrystalLogo="False" HasSearchButton="False" 
            EnableDatabaseLogonPrompt="False" BorderStyle="Solid"
            EnableParameterPrompt="False" />
			<!-- Start : Hidden Fields -->
			<input type="hidden" id="hdnSortName" runat="server" NAME="hdnSortName"> <input type="hidden" id="hdnSortAD" runat="server" NAME="hdnSortAD">
			<input type="hidden" id="hdnDeptNameCode" runat="server" NAME="hdnDeptNameCode">
			<!-- End   : Hidden Fields -->
		</form>
	</body>
</HTML>
