﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrintPopUp.aspx.vb" Inherits="AMS.PrintPopUp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1 {
            width: 332px;
        }
        .style2
        {
            width: 119px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
			<TABLE id="tableSubStatusAdd" cellSpacing="1" cellPadding="1" width="90%" align="center"
				bgColor="white" border="0">
				<TBODY>
					<tr>
						<td>
							<!-- Start: Search Part -->
							<table width="100%" border="0">
								<TR>
									<TD align="left" bgColor="#ffff99" colSpan="2"><b>Print RFID Label</b></TD>
								</TR>
								<TR>
                                    <%--<TD vAlign="middle" width="20%"><FONT color="#666666">Type</FONT></TD>
									<TD><asp:radiobuttonlist id="rdOLType" RepeatDirection="Horizontal" AutoPostBack="True" Runat="server">
											<asp:ListItem Value="U">Officer</asp:ListItem>
											<asp:ListItem Value="L" Selected>Location</asp:ListItem>
										</asp:radiobuttonlist></TD> --%>
								<TR>
									<TD vAlign="middle" class="style1" colspan="2">&nbsp;</TD>
								</TR>
								<TR>
									<TD vAlign="middle" colspan="2"><FONT color="#666666">Please choose one 
                                        your printer Location and click print button.</FONT></TD>
								</TR>
								<TR>
									<TD vAlign="middle" class="style2"><FONT color="#666666">Print Location :</FONT></TD>
									<TD>
                                        <asp:DropDownList ID="ddlPrintLocation" runat="server">
                                        </asp:DropDownList>
                                    </TD>
								</TR>
								<TR>
									<TD align="center" colSpan="2">
                                        <asp:button id="btnprint" Runat="Server" 
                                           Text="Print"></asp:button>
										<asp:button id="butSearchCancel" Runat="Server" Width="60px" Text="Cancel"></asp:button></TD>
								</TR>
							</table>
							<!-- End  : Search Part -->
							<!-- Start: Search Result Part -->
							<div id="divSearchResult" runat="server">
							</div>
							<!-- End  : Search Result Part --></td>
					</tr>
				</TBODY>
			</TABLE>
    
    </div>
    </form>
</body>
</html>
