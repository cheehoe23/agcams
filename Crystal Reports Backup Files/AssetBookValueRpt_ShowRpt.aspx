<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssetBookValueRpt_ShowRpt.aspx.vb" Inherits="AMS.AssetBookValueRpt_ShowRpt" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frm_Rpt" runat="server">
    <div>
        <asp:Label ID="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:Label>
        <br />
        <CR:CrystalReportViewer ID="crvABV" runat="server" 
            AutoDataBind="true" HasCrystalLogo="False" HasSearchButton="False" 
            EnableDatabaseLogonPrompt="False" BorderStyle="Solid"
            EnableParameterPrompt="False" />
        <br />
    </div>
    <input type="hidden" id="hdnDFDt" runat="server">
    <input type="hidden" id="hdnDTDt" runat="server">
    </form>
</body>
</html>
