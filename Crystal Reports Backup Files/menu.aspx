<%@ Page Language="vb" AutoEventWireup="false" Codebehind="menu.aspx.vb" Inherits="AMS.menu" %>
<%@ Register TagPrefix="ie" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>menu</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="CommonStyle.css" type="text/css" rel="Stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout" bgcolor="#e8e8f4">
		<form id="frm_Menu" method="post" runat="server">
			<!-- Start: Menu -->
			<!--
			<table border="1" cellSpacing="0" cellPadding="0" width="100%" height="400" align="center">
				<tr valign=top>
					<td class="sidebar" valign="top">-->
						<table border="0" cellSpacing="0" cellPadding="0" width="100%" height="370" align="center">
							<tr height="12">
								<td vAlign="top"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></td>
							</tr>
							<tr height="12">
								<td vAlign="top">
								    <table border="1" cellSpacing="1" cellPadding="1" width="180" align="center">
								        <tr>
								            <td align=center>
                                                <strong>Go To</strong></td>
                                            <td colspan="2">
                                                <asp:LinkButton ID="lbtnFTS" runat="server">File Management Module</asp:LinkButton>
                                            </td>
								        </tr>
								        <tr>
								            <td align=center colspan="2"><font class="DisplayTitleHeader">Information</font></td>
								        </tr>
								        <tr>
								            <td colspan="2">
								                <table border="0" cellSpacing="0" cellPadding="0" width="95%"  align="center">
								                    <tr>
								                        <td>Pending Condemn : </td>
								                        <td><b><asp:Label ID=lblMenuCondemn runat=server></asp:Label></b></td>
								                    </tr>
								                    <tr>
								                        <td>Pending Returned : </td>
								                        <td><b><asp:Label ID=lblMenuReturn runat=server></asp:Label></b></td>
								                    </tr>
								                    <tr>
								                        <td>Unidentified Owner : </td>
								                        <td><b><asp:Label ID=lblMenuMissOwner runat=server></asp:Label></b></td>
								                    </tr>
								                    <tr>
								                        <td>New NFS (Today) : </td>
								                        <td><b><asp:Label ID=lblMenuNFSToday runat=server></asp:Label></b></td>
								                    </tr>
								                </table>
								            </td>
								            
								        </tr>
								    </table>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td vAlign="top"><ie:treeview id="TvMenu" runat="server" SelectExpands="True" SystemImagesPath="/webctrl_client/1_0/treeimages/"
										SelectedStyle="font-family:Verdana, Helvetica, Sans-serif;" DefaultStyle="font-size:9pt;font-weight:bold;color:#615AB7;font-family:Verdana, Helvetica, Sans-serif;"
										HoverStyle="font-family:Verdana, Helvetica, Sans-serif;"></ie:treeview></td>
							</tr>
						</table>
					<!--</td>
				</tr>
			</table>-->
			<!-- End  : Menu -->
		</form>
	</body>
</HTML>
