<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="asset_CondemnCert.aspx.vb" Inherits="AMS.asset_CondemnCert" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS --> Certificate of Condemnation</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
</head>
<body>
    <form id="frm_CondemnCert" runat="server">
    <div>
        <asp:Label ID="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:Label>
        <br />
        <CR:CrystalReportViewer ID="crvCondemnCert1" runat="server" 
            AutoDataBind="true" HasCrystalLogo="False" HasGotoPageButton="False" HasSearchButton="False" 
            HasToggleGroupTreeButton="False" EnableDatabaseLogonPrompt="False" BorderStyle="Solid"
            DisplayGroupTree="False"/>
        <br />
        <CR:CrystalReportViewer ID="crvCondemnCert2" runat="server" 
            AutoDataBind="true" HasCrystalLogo="False" HasGotoPageButton="False" HasSearchButton="False" 
            HasToggleGroupTreeButton="False" EnableDatabaseLogonPrompt="False" BorderStyle="Solid"
            DisplayGroupTree="False"/>
        <br />
        <input type=hidden id=hdnStatusInfoID value="" runat=server />
    </div>
    </form>
</body>
</html>
