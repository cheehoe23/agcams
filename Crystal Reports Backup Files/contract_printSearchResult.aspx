<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="contract_printSearchResult.aspx.vb" Inherits="AMS.contract_printSearchResult" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
</head>
<body>
    <form id="frn_KIVprint" method="post" runat="server">
        <span style="font-size: 10pt">
        <b>Contract Record</b><br />
        </span>
        Printed Date : <asp:Label ID=lblPrintDate runat=server></asp:Label> <br /><br />
		<!--Datagrid for display record.-->
        <asp:datagrid id="dgContract" Runat="server" AlternatingItemStyle-Height="25"
		    ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
		    BorderColor="#ffffff"  datakeyfield="fld_ContractID" AlternatingItemStyle-BackColor="#e3d9ee">
		    <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
			    Height="25"></headerstyle>
		    <Columns>
		        <asp:boundcolumn HeaderText="fld_ContractID" datafield="fld_ContractID" Visible=false>
                    <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                </asp:boundcolumn>
                <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
                    <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                </asp:boundcolumn>
                <asp:boundcolumn HeaderText="Contract ID" datafield="contractIDStr" >
                    <itemstyle width="7%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
                </asp:boundcolumn>
                <asp:boundcolumn HeaderText="Contract Title" datafield="fld_ContractTitle" >
                    <itemstyle width="20%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                </asp:boundcolumn>
                <asp:boundcolumn HeaderText="Cost (S$)" datafield="fld_ContractCost" >
                    <itemstyle width="7%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                </asp:boundcolumn>
                <asp:boundcolumn HeaderText="From Date" datafield="fld_ContractFDt" >
                    <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
                </asp:boundcolumn>
                <asp:boundcolumn HeaderText="To Date" datafield="fld_ContractTDt" >
                    <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
                </asp:boundcolumn>
                <asp:boundcolumn HeaderText="Contract Owner" datafield="fld_OwnerFullname" >
                    <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                </asp:boundcolumn>
                <asp:boundcolumn HeaderText="Department" datafield="fld_DepartmentCode" >
                    <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                </asp:boundcolumn>
                <asp:boundcolumn HeaderText="Vendor Name" datafield="fld_ContractVendor" >
                    <itemstyle width="16%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                </asp:boundcolumn>
		    </Columns>
	    </asp:datagrid>
		</form>
		<script language="javascript">
		//call to print this page
		window.print()
		window.onfocus = function () { window.close(); }
		//Close Window
		//window.close()
		</script>
</body>
</html>
