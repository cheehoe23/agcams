<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="StockTake.aspx.vb" Inherits="AMS.StockTake" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet" />
	<script language="JavaScript" type="text/javascript" src="../Common/CommonJS.js"></script>
	<script language="javascript" type="text/javascript">
	    function chkSrhFrm() {
			var foundError = false;
		    
		    //validate Import from date
			if (!foundError && gfnCheckDate(document.frmStockTake.txtFrmDt, "Import From Date", "O") == false) {
				foundError=true
			}
			
			//validate Import to date
			if (!foundError && gfnCheckDate(document.frmStockTake.txtToDt, "Import To Date", "O") == false) {
				foundError=true
			}
						 
 			if (!foundError){
 			   return true;
 			}
			else
				return false;
		}
//	    function fnCheckSelect(TrfType, ChkBoxID) 
//			{
//				var flag, confirmAction;
//				flag = false;
//				
//				//Get Message for related action
//				switch(TrfType)    
//				{
//					case 'ADD':
//						confirmAction = "You have chosen to add one or more record(s).\nYou cannot undo this action. Continue?"; 
//						break    
//				}				
//				
//				if (gfnCheckSelectByValue(ChkBoxID)){
//					flag = window.confirm(confirmAction);
//				}
//				else  {
//				    alert("At least one record should be selected.");
//					flag = false;
//				}
//				return flag;
//			}		

//			alert("You have changed the Quantity.");
//			return false;
		
	</script>
</head>
<body>
    <form id="frmStockTake" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>NFS Transaction : Transaction Action</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																				    <TR>
																					    <TD width="35%">
																						    <FONT class="DisplayTitle">Date : </FONT>
																					    </TD>
																					    <TD width="65%">
																						    From <asp:TextBox ID="txtFrmDt" Width="110" 
                                                                                                Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.frmStockTake.txtFrmDt, document.frmStockTake.txtToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtToDt" Width="110" 
                                                                                                Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.frmStockTake.txtFrmDt, document.frmStockTake.txtToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>    
																				        <TD>&nbsp;</TD>
																						<TD align="left" style="height: 36px"><BR>
																							<asp:Button id="butSearch" Text="Search" Runat="Server" />
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />
																						</TD>
																					</TR>
																				    <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Search Result</b></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
                                                                                            <asp:button id="butUpdateLocation" Runat="server" 
                                                                                                Text="Update Location"></asp:button> </TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
							    <asp:GridView ID="gvStockTakeList" runat="server" AutoGenerateColumns="false" 
                                HeaderStyle-CssClass="gridViewHeader" OnRowDataBound="gvStockTakeList_RowDataBound"
                                 AllowPaging="true" PagerSettings-Visible="false" OnSorting="gvStockTakeList_Sorting"
                                AllowSorting="true" PageSize="50" Width="100%">
                                <Columns>    
                                     <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridViewRows" HeaderStyle-CssClass="gridViewHeader"
                                        HeaderStyle-Width="5px" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate><asp:CheckBox ID="chkAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkAll_CheckedChanged" /></HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkCheck" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="SrNo" SortExpression="SrNo" HeaderText="S/No"
                                        ItemStyle-CssClass="gridViewRows" HeaderStyle-CssClass="gridViewHeader" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-Width="10px" />                                                                     
                                    <%--<asp:BoundField DataField="fld_AssetID" SortExpression="fld_AssetID" HeaderText="fld_AssetID"
                                        ItemStyle-CssClass="gridViewRows" HeaderStyle-CssClass="gridViewHeader" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-Width="30px" Visible="false"/>--%>
                                    <asp:TemplateField HeaderText="fld_AssetID" SortExpression="fld_AssetID" ItemStyle-CssClass="gridViewRows"
                                        HeaderStyle-CssClass="gridViewHeader" ItemStyle-HorizontalAlign="Left" Visible="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblAssetID" runat="server" Text='<%# Eval("fld_AssetID") %>'/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="fld_RFID_Stock_Take_ID" SortExpression="fld_RFID_Stock_Take_ID" HeaderText="fld_RFID_Stock_Take_ID"
                                        ItemStyle-CssClass="gridViewRows" HeaderStyle-CssClass="gridViewHeader" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-Width="60px" Visible="false"/>
                                    <asp:BoundField DataField="fld_RFID" SortExpression="fld_RFID" HeaderText="RFID"
                                        ItemStyle-CssClass="gridViewRows" HeaderStyle-CssClass="gridViewHeader" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-Width="30px" />
                                    <asp:BoundField DataField="fld_AssetBarCode" SortExpression="fld_AssetBarCode" HeaderText="Asset ID"
                                        ItemStyle-CssClass="gridViewRows" HeaderStyle-CssClass="gridViewHeader" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-Width="30px" />                 
                                    <asp:TemplateField HeaderText="Sub Location ID" ItemStyle-CssClass="gridViewRows" HeaderStyle-CssClass="gridViewHeader"
                                        HeaderStyle-Width="30px" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlSubLocation" runat="server" AppendDataBoundItems="true" Width="200px">
                                            <asp:ListItem Selected="True" Text="" Value="0" />
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="fld_AssetFound" SortExpression="fld_AssetFound" HeaderText="Asset Found(Y/N)"
                                        ItemStyle-CssClass="gridViewRows" HeaderStyle-CssClass="gridViewHeader" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-Width="60px" />  
                                    <asp:BoundField DataField="fld_LocationFound" SortExpression="fld_LocationFound" HeaderText="Location Found(Y/N)"
                                        ItemStyle-CssClass="gridViewRows" HeaderStyle-CssClass="gridViewHeader" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-Width="60px" />  
                                    <asp:BoundField DataField="fld_Status" SortExpression="fld_Status" HeaderText="Status"
                                        ItemStyle-CssClass="gridViewRows" HeaderStyle-CssClass="gridViewHeader" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-Width="60px" />
                                </Columns>
                                <HeaderStyle CssClass="gridViewHeader" />
                                <AlternatingRowStyle CssClass="gridViewRowsAlt" />
                                <RowStyle CssClass="gridViewRows" />
                            </asp:GridView>
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
			<!-- Start: Hidden Fields -->
			<!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
				src="../common/DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="Z-INDEX:101; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px">
			</iframe>
			<input id="hdnImpFrmDt" type="hidden" name="hdnImpFrmDt" runat="server"> 
			<input id="hdnImpToDt" type="hidden" name="hdnImpToDt" runat="server"> 
			<!-- End  : Hidden Fields -->
	</form>
</body>
</html>
