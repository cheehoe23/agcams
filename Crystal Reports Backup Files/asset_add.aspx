<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="asset_add.aspx.vb" Inherits="AMS.asset_add" ValidateRequest="false" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<%@ Register TagPrefix="fluent" Namespace="Fluent.MultiLineTextBoxValidator" Assembly="Fluent.MultiLineTextBoxValidator" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>	
	<LINK href="../common/ComboBox/styles/contactsearch/combo.css" type="text/css" rel="Stylesheet">
    <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
    <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />
    
	<script type="text/javascript">
	    	    $(document).ready(function () {
	    	        SearchText("cbOwner", "hfUser");
	    	        SearchTextForNFSID("lblAssetIdNFS");
	    	    });
	    	    function SearchTextForNFSID(obj1) {
	    	        $("#" + obj1).autocomplete({
	    	            source: function (request, response) {
	    	                $.ajax({
	    	                    url: '<%#ResolveUrl("~/AssetMng/asset_add.aspx/GetNFSID") %>',
	    	                    data: "{ 'prefix': '" + request.term + "'}",
	    	                    dataType: "json",
	    	                    type: "POST",
	    	                    contentType: "application/json; charset=utf-8",
	    	                    success: function (data) {
	    	                        response($.map(data.d, function (item) {
	    	                            return {
	    	                                label: item.split('~')[0],
	    	                                val: item.split('~')[1]

	    	                            }


	    	                        }))
	    	                    },
	    	                    error: function (response) {
	    	                        alert(response.responseText);
	    	                    },
	    	                    failure: function (response) {
	    	                        alert(response.responseText);
	    	                    }
	    	                });
	    	            },	    	           
	    	            minLength: 0
	    	        });

	    	    }
	    	    function SearchText(obj1, obj2) {
	    	        $("#" + obj1).autocomplete({
	    	            source: function (request, response) {
	    	                $.ajax({
	    	                    url: '<%#ResolveUrl("~/AssetMng/asset_add.aspx/GetUsersList") %>',
	    	                    data: "{ 'prefix': '" + request.term + "'}",
	    	                    dataType: "json",
	    	                    type: "POST",
	    	                    contentType: "application/json; charset=utf-8",
	    	                    success: function (data) {
	    	                        response($.map(data.d, function (item) {
	    	                            return {
	    	                                label: item.split('~')[0],
	    	                                val: item.split('~')[1]	    	                                
	    	                            }  
	    	                        }))
	    	                    },
	    	                    error: function (response) {
	    	                        alert(response.responseText);
	    	                    },
	    	                    failure: function (response) {
	    	                        alert(response.responseText);
	    	                    }
	    	                });
	    	            },
	    	            select: function (e, i) {
	    	                $("#" + obj2).val(i.item.val);
	    	            },
	    	            minLength: 0
	    	        });

	    	    }
        
		function mfnOpenAddImagesWindow() 
			{
			    //open pop-up window
		        window.open('../common/AddNewFile.aspx?CallFrm=AddAsset', 'AddFile','width=450,height=250,Top=0,left=0,scrollbars=1');
				return false;
			}
		function chkFrm() {
			var foundError = false;
			var i = 0;
			var j = 0;
			var curRow = 0;
			
			//Validate Category
			if (!foundError && document.Frm_AssetAdd.ddlAssetCat.value == '-1') {
				foundError=true;
				document.Frm_AssetAdd.ddlAssetCat.focus();
				alert("Please select Category.");
			}
			
			//Validate SubCategory
			if (!foundError && document.Frm_AssetAdd.ddlAssetSubCat.value == '-1') {
				foundError=true;
				document.Frm_AssetAdd.ddlAssetSubCat.focus();
				alert("Please select Subcategory.");
			}
			
			//Validate Additional Information (Single Addition)
			if (!foundError && document.Frm_AssetAdd.hdnAddType.value == 'S'){
			    for (i=1;i<=5;i++){
			       if (eval('document.Frm_AssetAdd.hdnSCatAddInfo'+i+'.value') == ''){
			            break;
			       }
			       else{
			            if (eval('document.Frm_AssetAdd.hdnSCatAddInfoMO'+i+'.value') == 'Y' && eval('document.Frm_AssetAdd.txtAddInfo'+i+'.value') == ''){
			                foundError=true;
			                eval('document.Frm_AssetAdd.txtAddInfo'+i+'.focus()');
			                alert("Please enter " + eval('document.Frm_AssetAdd.hdnSCatAddInfo'+i+'.value') + ".");
			                break;
			            }
			       } 
			    }
			}
			
			//Validate Brand
//			if (!foundError && gfnIsFieldBlank(document.Frm_AssetAdd.txtBrand)) {
//				foundError=true;
//				document.Frm_AssetAdd.txtBrand.focus();
//				alert("Please enter Brand.");
//			}
			
			//Validate Cost (just for Fixed Asset)
			//if (!foundError && document.Frm_AssetAdd.rdAssetType[0].checked==true){
			//alert ("rdAssetType = " + mfnGetSelectedRadioButVal(document.Frm_AssetAdd.rdAssetType))
			if (!foundError && mfnGetSelectedRadioButVal(document.Frm_AssetAdd.rdAssetType) == 'A'){
                if (!foundError && gfnIsFieldBlank(document.Frm_AssetAdd.txtCost)) {
				    foundError=true;
				    document.Frm_AssetAdd.txtCost.focus();
				    alert("Please enter Cost.");
			    }       
            }
            
            //Validate Cost (if Cost not blank)
            if (!foundError && gfnIsFieldBlank(document.Frm_AssetAdd.txtCost)== false) {
			    if (!foundError && gfnCheckNumeric(document.Frm_AssetAdd.txtCost,'.')) {
				    foundError=true;
				    document.Frm_AssetAdd.txtCost.focus();
				    alert("Cost just allow number only.");
			    } 
		    }
		    
		    //validate purchase date
			if (!foundError && gfnCheckDate(document.Frm_AssetAdd.txtPurchaseDt, "Purchase Date", "M") == false) {
				foundError=true
			}

            var today = new Date();
            var todaydate = today.getDate();

            if (!foundError && (todaydate<document.Frm_AssetAdd.txtPurchaseDt.value)) {
                foundError = true;
            }

			//validate Warranty Expiry Date
		    if (!foundError && gfnCheckDate(document.Frm_AssetAdd.txtWarExpDt, "Warranty Expiry Date", "O") == false) {
				foundError=true
			}
			if (!foundError && gfnIsFieldBlank(document.Frm_AssetAdd.txtWarExpDt) == false) {
			    if (!foundError && gfnCheckFrmToDt(document.Frm_AssetAdd.txtPurchaseDt,document.Frm_AssetAdd.txtWarExpDt,'<=') == false) {
				    foundError=true
				    document.Frm_AssetAdd.txtWarExpDt.focus()
				    alert("Warranty Expiry Date should be greater than Purchase Date.");
			    }
			}
			
			//validate for department
			if (!foundError && document.Frm_AssetAdd.ddlDepartment.value == "-1") {
				foundError=true
				document.Frm_AssetAdd.ddlDepartment.focus()
				alert("Please select the Department.")
			}
			
			//validate for Location
			//--> if Location is seleted, Sub Location must selected.
			if (!foundError && document.Frm_AssetAdd.ddlLocation.value != "-1") {
				if (!foundError && document.Frm_AssetAdd.ddlLocSub.value == "-1") {
				    foundError=true
				    document.Frm_AssetAdd.ddlLocSub.focus()
				    alert("Please select the Sub Location.")
			    }
			}
			
			//Validate Owner
			//alert("owner1 = " + document.Frm_AssetAdd.cbOwnerSelectedValue0.value);
			//alert("owner2 = " + document.Frm_AssetAdd.cbOwner.value);
//			if (!foundError && gfnIsFieldBlank(document.Frm_AssetAdd.cbOwner)) {
//			    if (!foundError && gfnIsFieldBlank(document.Frm_AssetAdd.cbOwnerSelectedValue0)) {
//		                foundError=true
//		                alert("Owner not found. Please select an existing Owner.")
//                }
//            }
                
		
			//Validate Additional Information (Bulk Addition)
			if (!foundError && document.Frm_AssetAdd.hdnAddType.value == 'B'){
			    for (i=1;i<=5;i++){
			       if (eval('document.Frm_AssetAdd.hdnSCatAddInfo'+i+'.value') == ''){
			            break;
			       }
			       else{
			            for (j=1;j<=parseInt(document.Frm_AssetAdd.hdnQuantity.value);j++){
			                curRow = j + 1;
			                if (eval('document.Frm_AssetAdd.hdnSCatAddInfoMO'+i+'.value') == 'Y' && eval('document.Frm_AssetAdd.dgSubCatAddInfo$ctl0'+curRow+'$txtAddInfo'+i+'.value') == ''){
			                    foundError=true;
			                    eval('document.Frm_AssetAdd.dgSubCatAddInfo$ctl0'+curRow+'$txtAddInfo'+i+'.focus()');
			                    alert("Please enter " + eval('document.Frm_AssetAdd.hdnSCatAddInfo'+i+'.value') + ".");
			                    break;
			                }
			            }
			       } 
			       if (foundError){break;}
			    }
			}
			
						 
 			if (!foundError){
 			    var flag = false;
 				flag = window.confirm("Are you sure you want to add this Asset?");
 				return flag;
 			}
			else
				return false;
		}
	function mfnGetSelectedRadioButVal(RadioBut) 
	    {   //*** Return selected value of radio button
	        //*** Parameter : document.[Form Name].[Radio Button ID]
	        var rad_val = '';
	    
	        // if the button group is an array (one button is not an array)
            if (RadioBut[0]) { 
	            for (var i=0; i < RadioBut.length; i++)
                {
                      if (RadioBut[i].checked)
                      {
                        rad_val = RadioBut[i].value;
                      }
                }
            }
            
            // The button group is just the one button (not an array) 
            else {
                rad_val = RadioBut.value;
            }
          
            return rad_val;
	    }
	</script>
</head>
<body>
    <form id="Frm_AssetAdd" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Asset Management : Create New Asset</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" height="500" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
                                                                                    <TR>
																						<TD valign="middle" width="35%">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Asset Type : </FONT>
																						</TD>
																						<TD width="65%">
																							<asp:radiobuttonlist id="rdAssetType" Runat="server" RepeatDirection="Horizontal" AutoPostBack="true"></asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<tr id = "trAssetIdNFS" runat="server">
																						<TD>
																						    <FONT class="DisplayTitle">Asset ID (NFS) : </FONT>
																						</TD>
																						<TD width="65%">                                                                                            
																						    <asp:TextBox ID="lblAssetIdNFS" runat="server" Width="300px"></asp:TextBox>
																						</TD>
																					</tr>
																					
																			        <TR>
																					    <TD>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Asset Category : </FONT>
																					    </TD>
																					    <TD>
																					       <asp:DropDownList ID="ddlAssetCat" AutoPostBack=true Runat="server"></asp:DropDownList>
																						</TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Asset Subcategory : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlAssetSubCat" Runat="server" AutoPostBack=true></asp:DropDownList>
																						</TD>
																				    </TR>
																				    <tr>
																				        <td colspan=2>
																				            <!-- Start: Display Additional Informations for Subcategory (Single Addition) -->
																							<div id=divAddInfo1 runat=server>
																							    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table9">
																							        <TR>
																					                    <TD width="35%">
																					                        <FONT class="DisplayTitle"><asp:Label ID=lblAddInfo1 runat=server></asp:Label> </FONT>
																					                    </TD>
																					                    <TD width="65%">
																					                        <asp:TextBox id="txtAddInfo1" maxlength="250" Runat="server" Width="300px"></asp:TextBox>
																						                </TD>
																				                    </TR>
																							    </TABLE>
																							</div>
																							<div id=divAddInfo2 runat=server>
																							    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table10">
																							        <TR>
																					                    <TD width="35%">
																					                        <FONT class="DisplayTitle"><asp:Label ID=lblAddInfo2 runat=server></asp:Label> </FONT>
																					                    </TD>
																					                    <TD width="65%">
																					                        <asp:TextBox id="txtAddInfo2" maxlength="250" Runat="server" Width="300px"></asp:TextBox>
																						                </TD>
																				                    </TR>
																							    </TABLE>
																							</div>
																							<div id=divAddInfo3 runat=server>
																							    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table11">
																							        <TR>
																					                    <TD width="35%">
																					                        <FONT class="DisplayTitle"><asp:Label ID=lblAddInfo3 runat=server></asp:Label> </FONT>
																					                    </TD>
																					                    <TD width="65%">
																					                        <asp:TextBox id="txtAddInfo3" maxlength="250" Runat="server" Width="300px"></asp:TextBox>
																						                </TD>
																				                    </TR>
																							    </TABLE>
																							</div>
																							<div id=divAddInfo4 runat=server>
																							    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table12">
																							        <TR>
																					                    <TD width="35%">
																					                        <FONT class="DisplayTitle"><asp:Label ID=lblAddInfo4 runat=server></asp:Label> </FONT>
																					                    </TD>
																					                    <TD width="65%">
																					                        <asp:TextBox id="txtAddInfo4" maxlength="250" Runat="server" Width="300px"></asp:TextBox>
																						                </TD>
																				                    </TR>
																							    </TABLE>
																							</div>
																							<div id=divAddInfo5 runat=server>
																							    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table13">
																							        <TR>
																					                    <TD width="35%">
																					                        <FONT class="DisplayTitle"><asp:Label ID=lblAddInfo5 runat=server></asp:Label> </FONT>
																					                    </TD>
																					                    <TD width="65%">
																					                        <asp:TextBox id="txtAddInfo5" maxlength="250" Runat="server" Width="300px"></asp:TextBox>
																						                </TD>
																				                    </TR>
																							    </TABLE>
																							</div>
																							<!-- End  : Display Additional Informations for Subcategory (Single Addition) -->
																				        </td>
																				    </tr>
																				    <TR>
															                            <TD><FONT class="DisplayTitle">Controlled Item : </FONT></TD>
															                            <TD>
															                                <asp:radiobuttonlist id="rdCtrlItemF" Runat="server" RepeatDirection="Horizontal">
																	                            <asp:ListItem Value="Y">Yes</asp:ListItem>
																	                            <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
																                            </asp:radiobuttonlist>
															                            </TD>
														                            </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Brand : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtBrand" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle">Description: </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtDesc" TextMode=MultiLine maxlength="1000" Columns="40" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtDescWord" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="MMLValDesc" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Description to 1000 Characters." OutputControl="txtDescWord" ControlToValidate="txtDesc"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Cost Information</font></TD>
																					</TR>
																					<TR>
																					    <TD>
																					        <asp:Label ID=lblMandatory1 runat=server><font color="red">*</font></asp:Label><FONT class="DisplayTitle">Cost (S$) : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtCost" maxlength="250" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <font color="red">*</font><FONT class="DisplayTitle">Purchase Date : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:TextBox ID="txtPurchaseDt" Width="120" Runat="server" ></asp:TextBox>
																						    <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fSingleDtPop(document.Frm_AssetAdd.txtPurchaseDt);return false;"
																							    HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select Date">
                                                                                            </a>
                                                                                        </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Warranty Expiry Date : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:TextBox ID="txtWarExpDt" Width="120" Runat="server"></asp:TextBox>
																						    <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fSingleDtPop(document.Frm_AssetAdd.txtWarExpDt);return false;"
																							    HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select Date">
																						    </a>
																					    </TD>
																				    </TR>
																				    <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																				    <TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Location Information</font></TD>
																					</TR>
																				    <TR>
																					    <TD>
																					        <font color="red">*</font><FONT class="DisplayTitle">Department : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:DropDownList ID="ddlDepartment" Runat="server"></asp:DropDownList>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Location : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:DropDownList ID="ddlLocation" Runat="server" AutoPostBack=true></asp:DropDownList>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Sub Location : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:DropDownList ID="ddlLocSub" Runat="server"></asp:DropDownList>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Assigned Owner : </FONT>
																					    </TD>
																					    <TD>                                                                                            
                                                                                            <asp:TextBox ID="cbOwner" runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        &nbsp;</TD>
																					    <TD>                                                                                            
                                                                                            &nbsp;</TD>
																				    </TR>
																				    <TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Other Information</font></TD>
																					</TR>
																					<TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle">Image(s)/File(s) : </FONT>
																					    </TD>
																					    <TD>
																					        [<asp:LinkButton ID="butAddImages" runat="server">Add New File</asp:LinkButton>]
																						    <!--Datagrid for display record.-->
									                                                        <FONT class="DisplayTitle">
                                                                                            <span style="font-size: 10.0pt; line-height: 107%; font-family: &quot;Calibri&quot;,sans-serif; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA; color: #FF0000;">
                                                                                            <strong>File size limit is 2 MB</strong></span></FONT><asp:datagrid id="dgAssetFileImages" Runat="server" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#CC9966" datakeyfield="fld_AssetFileID" BackColor="White" BorderWidth="1px" CellPadding="4">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="#FFFFCC" Font-Bold="True" HorizontalAlign="Center"
											                                                        ></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn headertext="fld_AssetFileID" datafield="fld_AssetFileID"  Visible=False>
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="fld_FileSaveName" datafield="fld_FileSaveName"  Visible=False>
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                        </asp:boundcolumn>
										                                                            <asp:boundcolumn headertext="S/No" datafield="SNum" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="File Name" datafield="fld_FileName">
												                                                        <itemstyle width="70%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn ItemStyle-Width="10%" HeaderText="<IMG SRC=../images/audit.gif Border=0>" 
							                                                                            ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Center>
								                                                                        <itemtemplate>
									                                                                        <%#fnGetFileName(DataBinder.Eval(Container.DataItem, "fld_FileSaveName"))%>
								                                                                        </itemtemplate>
							                                                                        </asp:templatecolumn>
																									<asp:ButtonColumn Text="&lt;IMG SRC=../images/delete.gif Border=0&gt;"
																										HeaderText="&lt;IMG SRC=../images/delete.gif Border=0&gt;" CommandName="Delete">
                                                                                                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                                                    </asp:ButtonColumn>									
										                                                        </Columns>
									                                                        </asp:datagrid>
                                                                                        </TD>
																				    </TR>
																				    													    
																				    <tr>
																					    <TD>
																					        <FONT class="DisplayTitle">Short Description : </FONT>
																					    </TD>
																					    <TD>
																					                        <asp:TextBox id="txtShortDescription" maxlength="100" 
                                                                                                Runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </tr>
                                                                                    <tr>
																					    <TD>
																					        <FONT class="DisplayTitle">Other Information : </FONT>
																					    </TD>
																					    <TD>
																					                        <asp:TextBox id="txtOtherInformation" maxlength="250" 
                                                                                                Runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </tr>
                                                                                    <tr>
																						<TD colSpan="2">&nbsp;</TD>
																					</tr>
																				    													    
																				    <TR>
																					    <TD valign="top"><FONT class="DisplayTitle">Remarks : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:TextBox id="txtRemarks" maxlength="1000" TextMode="MultiLine" Columns="40" Rows="5" Runat="server"></asp:TextBox><br />
																					        <asp:textbox id="txtRemarksWord" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					        <fluent:multilinetextboxvalidator id="MLLValRemarks" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Remarks to 1000 Characters." OutputControl="txtRemarksWord" ControlToValidate="txtRemarks"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    													    
																				    <tr id="trSerialNo" runat="server">
																					    <TD valign="top"><FONT class="DisplayTitle">Serial No : </FONT>
																					    </TD>
																					    <TD>
																					                        <asp:TextBox id="txtSerialNo" maxlength="250" 
                                                                                                Runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </tr>
                                                                                    <tr id="trMacAddress" runat="server">
																					    <TD valign="top"><FONT class="DisplayTitle">Mac Address : </FONT>
																					    </TD>
																					    <TD>
																					                        <asp:TextBox id="txtMacAddress" maxlength="250" 
                                                                                                Runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </tr>
                                                                                    <tr id="trHostName" runat="server">
																					    <TD valign="top"><FONT class="DisplayTitle">New Hostname : </FONT>
																					    </TD>
																					    <TD>
																					                        <asp:TextBox id="txtHostName" maxlength="250" 
                                                                                                Runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </tr>
                                                                                    <tr id="trStatus" runat="server">
																					    <TD valign="top"><FONT class="DisplayTitle">Status : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:RadioButtonList ID="rdolstStatus" runat="server" 
                                                                                                RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="S">Spare</asp:ListItem>
                                                                                                <asp:ListItem Value="L">Loan</asp:ListItem>
                                                                                                <asp:ListItem Value="D">Deployed</asp:ListItem>
                                                                                            </asp:RadioButtonList>
																					    </TD>
																				    </tr>
                                                                                    <tr id="trDeviceType" runat="server">
																					    <TD valign="top"><FONT class="DisplayTitle">Device Type : </FONT>
																					    </TD>
																					    <TD>
																					                        <asp:DropDownList ID="ddlDeviceType" runat="server">
                                                                                                            </asp:DropDownList>
																					    </TD>
																				    </tr>
																				    													    
																				    <TR id="trMachineModel" runat="server">
																					    <TD valign="top"><FONT class="DisplayTitle">Machine Model : </FONT>
																					    </TD>
																					    <TD>
																					                        <asp:TextBox id="txtMachineModel" maxlength="250" 
                                                                                                Runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </TR>
																					<TR>
																						<TD valign="middle" width="35%">
																						    <FONT class="DisplayTitle">Temporary Asset : </FONT><img src="../images/question.jpg" alt="Temporary Assets are asset that are pending creation from NFS" />
																						</TD>
																						<TD width="65%">
																							<asp:Label ID=lblTempAsset runat=server></asp:Label>
																						</TD>
																					</TR>
																					<tr>
																				        <td colspan=2>
																				            <div id=divAddInfoB runat=server>
																				            <!-- Start: Display Additional Informations for Subcategory (Bulk Addition) -->
																							<asp:datagrid id="dgSubCatAddInfo" Runat="server" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#CC9966" datakeyfield="SNum" BackColor="White" BorderWidth="1px" CellPadding="4">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="#FFFFCC" Font-Bold="True" HorizontalAlign="Center"
											                                                        ></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn headertext="S/No" datafield="SNum" >
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn ItemStyle-Width="20%" HeaderText="Additional Information 1" 
							                                                                            ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Center>
								                                                                        <itemtemplate>
									                                                                        <asp:TextBox ID=txtAddInfo1 runat=server MaxLength=250></asp:TextBox>
								                                                                        </itemtemplate>
							                                                                        </asp:templatecolumn>	
							                                                                        <asp:templatecolumn ItemStyle-Width="20%" HeaderText="Additional Information 2" 
							                                                                            ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Center>
								                                                                        <itemtemplate>
									                                                                        <asp:TextBox ID=txtAddInfo2 runat=server MaxLength=250></asp:TextBox>
								                                                                        </itemtemplate>
							                                                                        </asp:templatecolumn>	
							                                                                        <asp:templatecolumn ItemStyle-Width="20%" HeaderText="Additional Information 3" 
							                                                                            ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Center>
								                                                                        <itemtemplate>
									                                                                        <asp:TextBox ID=txtAddInfo3 runat=server MaxLength=250></asp:TextBox>
								                                                                        </itemtemplate>
							                                                                        </asp:templatecolumn>	
							                                                                        <asp:templatecolumn ItemStyle-Width="20%" HeaderText="Additional Information 4" 
							                                                                            ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Center>
								                                                                        <itemtemplate>
									                                                                        <asp:TextBox ID=txtAddInfo4 runat=server MaxLength=250></asp:TextBox>
								                                                                        </itemtemplate>
							                                                                        </asp:templatecolumn>	
							                                                                        <asp:templatecolumn ItemStyle-Width="20%" HeaderText="Additional Information 5" 
							                                                                            ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Center>
								                                                                        <itemtemplate>
									                                                                        <asp:TextBox ID=txtAddInfo5 runat=server MaxLength=250></asp:TextBox>
								                                                                        </itemtemplate>
							                                                                        </asp:templatecolumn>								
										                                                        </Columns>
									                                                        </asp:datagrid>
																							</div>
																							<!-- End  : Display Additional Informations for Subcategory (Bulk Addition) -->
																				        </td>
																				    </tr>
																					<TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:Button id="butSubmit" Text="Submit" Runat="Server" />&nbsp;
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />&nbsp;
																							<asp:Button id="butCancel" Text="Cancel" Runat="Server" />
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnCurrentDate" runat="server">
			<input type="hidden" id="hdnAddSuccessF" runat="server"> 
			<input type="hidden" id="hdnAddType" runat="server"> 
			<input type="hidden" id="hdnQuantity" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfo1" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfo2" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfo3" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfo4" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfo5" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfoMO1" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfoMO2" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfoMO3" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfoMO4" runat="server"> 
			<input type="hidden" id="hdnSCatAddInfoMO5" runat="server">
			<input type="hidden" id="hdnAddFileF" runat="server"> 
			<input type="hidden" id="hdnAssetIds" runat="server"> 
            <input type="hidden" id="hfOwner" runat="server">             
            <asp:Button ID="hdnclickprint" runat="server" Visible ="false" ></asp:button>
			<!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
				src="../common/DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="Z-INDEX:101; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px">
			</iframe>
			<!-- End  : Hidden Fields -->
		</form>
		<script language="javascript">
		    function ClickPrint() {
		        __doPostBack('hdnclickprint', 'OnClick');		        
		    }

			if (document.Frm_AssetAdd.hdnAddSuccessF.value == 'Y')
			{	
				document.Frm_AssetAdd.hdnAddSuccessF.value = 'N';
				
				var flag = false;
				var AssetIDs = document.Frm_AssetAdd.hdnAssetIds.value;
				
				flag = window.confirm("New record added successfully.\nDo you want to print the label now?");
				if (flag){
				    ClickPrint()
				}
                else
                    document.location.href = 'asset_addChoosen.aspx';

				//document.location.href = 'asset_addChoosen.aspx';
			}            
		</script>
</body>
</html>
