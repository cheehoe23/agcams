<%@ Page Language="vb" AutoEventWireup="false" Codebehind="location_View.aspx.vb" Inherits="AMS.location_View"%>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>location_View</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
		<script language="JavaScript" src="../Common/CommonJS.js"></script>
		<script language="javascript">
		function ConfirmDelete() 
			{
				var flag;
				flag = false;
				if (gfnCheckDelSelect()){
					flag = window.confirm("You have chosen to delete one or more Location.\nYou cannot undo this action. Continue?");
				}
				else  {
					flag = false;
				}
				return flag;
			}
		function fnCheckSelect(ChkBoxID) 
			{
				var flag;
				flag = false;
									
				if (gfnCheckSelectByValue(ChkBoxID)){
					flag = window.confirm("You have chosen to print one or more record(s). Continue?");
				}
				else  {
				    alert("At least one record should be selected.");
					flag = false;
				}
				return flag;
			}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Frm_LocationView" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Administration : Location</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<table id="Table2" height="400" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr>
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td align="center"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td vAlign="top" width="100%">
						<!--Start Main Content-->
						<asp:button id="butAddLoct" Runat="server" Text="Add New Location"></asp:button>
						<asp:button id="butDelLoct" Runat="server" Text="Delete Location"></asp:button>
						<asp:button id="butPrintRFIDLabel" Runat="server" Text="Print Location Marker"></asp:button>
						<table height="400" cellSpacing="1" cellPadding="0" width="100%" align="left" border="0">
							<tr>
								<td vAlign="top" align="right" bgColor="#a3a9cc">
									<table cellSpacing="1" cellPadding="1" width="100%" border="0">
										<tr>
											<td width="20%"><asp:dropdownlist id="ddlPageSize" Runat="server" AutoPostBack="True"></asp:dropdownlist>
												&nbsp;&nbsp;<font color="white">per page</font>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td vAlign="top" align="center" style="height: 401px">
									<!--Datagrid for display record.-->
									<asp:datagrid id="dgLocation" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										ItemStyle-Height="25" AllowSorting="True" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" AllowPaging="True" PageSize="10" PagerStyle-HorizontalAlign="Right"
										PagerStyle-VerticalAlign="Middle" PagerStyle-Mode="NumericPages" PagerStyle-Position="Top"
										PagerStyle-BackColor="#a3a9cc" datakeyfield="fld_LocationID" AlternatingItemStyle-BackColor="#e3d9ee">
										<headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											Height="25"></headerstyle>
										<Columns>
											<asp:boundcolumn visible="false" datafield="fld_LocationID" headertext="fld_LocationID" ItemStyle-Height="10">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn visible="false" datafield="fld_LocSubID" headertext="fld_LocSubID" ItemStyle-Height="10">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn visible="false" datafield="LoctDeleteF" headertext="LoctDeleteF" ItemStyle-Height="10">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											
											<asp:boundcolumn datafield="fld_LocationCode" headertext="Location Code" ItemStyle-Height="10" SortExpression="fld_LocationCode">
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:hyperlinkcolumn headertext="Location Name" text="fld_LocationName" SortExpression="fld_LocationName" datatextfield="fld_LocationName"
												navigateurl="location_Edit.aspx" datanavigateurlfield="fld_LocationID" DataNavigateUrlFormatString="location_Edit.aspx?loctID={0}">
												<itemstyle cssclass="GridText" horizontalalign="Left" width="15%" verticalalign="Middle"></itemstyle>
											</asp:hyperlinkcolumn>
											<asp:boundcolumn datafield="fld_LocationName" SortExpression="fld_LocationName" headertext="Location Name" ItemStyle-Height="10" Visible="false">
												<itemstyle width="15%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											
											<asp:boundcolumn datafield="fld_LocSubCode" headertext="Sub Location Code" ItemStyle-Height="10" SortExpression="fld_LocSubCode">
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:hyperlinkcolumn headertext="Sub Location Name" text="fld_LocSubName" SortExpression="fld_LocSubName" datatextfield="fld_LocSubName"
												navigateurl="location_EditSub.aspx" datanavigateurlfield="fld_LocSubID" DataNavigateUrlFormatString="location_EditSub.aspx?CallFrm=View&SLocID={0}">
												<itemstyle cssclass="GridText" horizontalalign="Left" width="15%" verticalalign="Middle"></itemstyle>
											</asp:hyperlinkcolumn>
											<asp:boundcolumn datafield="fld_LocSubName" SortExpression="fld_LocSubName" headertext="Sub Location Name" ItemStyle-Height="10" Visible="false">
												<itemstyle width="15%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											
											<asp:boundcolumn datafield="LocSubActvName" SortExpression="fld_Activate" headertext="Smart Shelf Rack" ItemStyle-Height="10" Visible=false>
												<itemstyle width="6%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn datafield="fld_LightIPAdd" SortExpression="fld_LightIPAdd" headertext="Light IP Address" ItemStyle-Height="10" Visible=false>
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn datafield="fld_LightTypeStr" SortExpression="fld_LightTypeStr" headertext="Light Type" ItemStyle-Height="10" Visible=false>
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											</asp:boundcolumn>
											
											<asp:boundcolumn datafield="fld_DGFName" SortExpression="fld_DGF" headertext="Door Gantry" ItemStyle-Height="10" Visible=false>
												<itemstyle width="6%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn datafield="fld_DGMvStatus" SortExpression="fld_DGMvStatus" headertext="Movement Status" ItemStyle-Height="10" Visible=false>
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>
											
											<asp:templatecolumn HeaderText="Select" ItemStyle-Width="3%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" Visible=false>
												<itemtemplate>
													<%#fnShowEditHyperlink(DataBinder.Eval(Container.DataItem, "fld_LocationID"), DataBinder.Eval(Container.DataItem, "LoctEditable"))%>
												</itemtemplate>
											</asp:templatecolumn>
											<asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="5%">
												<headertemplate>
													<img src="../images/delete.gif" alt="Delete" runat="server" id="imgDelete"><br />
                                                    <asp:checkbox id="CheckAll" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'DeleteThis', 'CheckAll');" runat="server" />

													<%--<asp:checkbox id="CheckAll" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
														runat="server" />--%>
												</headertemplate>
												<itemtemplate>
													<center>
                                                        <asp:checkbox id="DeleteThis" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'DeleteThis', 'CheckAll');" runat="server" />

														<%--<asp:checkbox id="DeleteThis" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
															runat="server" />--%>
													</center>
												</itemtemplate>
											</asp:templatecolumn>
											<asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="5%">
                                                <headertemplate>
                                                    Print Marker
                                                    <asp:checkbox id="cdCheckAll" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckS', 'cdCheckAll');" runat="server" />
                                                </headertemplate>
                                                <itemtemplate>
                                                    <center>
                                                        <asp:checkbox id="cdCheckS" onclick="javascript: return gfnSelect_deselectAllByValue (this.checked, this.id, 'cdCheckS', 'cdCheckAll');" runat="server" />
                                                    </center>
                                                </itemtemplate>
                                            </asp:templatecolumn>
										</Columns>
									</asp:datagrid></td>
							</tr>
						</table>
						<!--End Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- Start: Hidden Fields -->
			<input id="hdnSortName" type="hidden" name="hdnSortName" runat="server"> 
			<input id="hdnSortAD" type="hidden" name="hdnSortAD" runat="server">
			<!-- End  : Hidden Fields -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
		</form>
	</body>
</HTML>
