<%@ Page Language="vb" AutoEventWireup="false" Codebehind="locationOrOfficer_Edit.aspx.vb" Inherits="AMS.locationOrOfficer_Edit"%>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../commonFTS/FTSfooter_cr.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD id="Head1" runat="server">
		<title>File_EditVolume</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../commonFTS/FTSstyle.css" type="text/css" rel="Stylesheet">
		<script language="JavaScript" src="../Common/CommonJS.js"></script>       
        <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
        <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
        <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />

		<script language="javascript">

		    $(document).ready(function () {
		        SearchText("txtLocOfficerName", "hdnLocOfficerName");
		    });

		    function SearchText(obj1, obj2) {
		        $("#" + obj1).autocomplete({
		            source: function (request, response) {
		                $.ajax({
		                    url: '<%#ResolveUrl("~/sysSetting/locationOrOfficer_Edit.aspx/GetUsersList") %>',
		                    data: "{ 'prefix': '" + request.term + "'}",
		                    dataType: "json",
		                    type: "POST",
		                    contentType: "application/json; charset=utf-8",
		                    success: function (data) {
		                        response($.map(data.d, function (item) {
		                            return {
		                                label: item.split('~')[0],
		                                val: item.split('~')[1]
		                            }
		                        }))
		                    },
		                    error: function (response) {
		                        alert(response.responseText);
		                    },
		                    failure: function (response) {
		                        alert(response.responseText);
		                    }
		                });
		            },
		            select: function (e, i) {
		                $("#" + obj2).val(i.item.val);
		            },
		            minLength: 0
		        });

		    }
		
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="frm_LocationOfficerEdit" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>File Management : Edit File</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table id="Table2" height="500" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<!--Start: Main Content-->
						<table id="Table3" height="400" cellSpacing="1" cellPadding="0" width="100%" align="left"
							bgColor="silver" border="0">
							<tr>
								<td>
									<table id="Table4" height="400" cellSpacing="0" cellPadding="0" width="100%" align="left"
										bgColor="white" border="0">
										<tr>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
													<TR>
														<TD vAlign="top" align="center" width="100%"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE id="Table6" height="126" cellSpacing="0" cellPadding="3" width="100%" align="left"
																bgColor="white" border="0">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</FONT></B></DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" height="352">
																			<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
																				<TBODY>
																					<TR>
																						<TD vAlign="middle" align="right" width="30%"><FONT color="#666666">
                                                                                            *Location 
                                                                                            / Officer Name:&nbsp; </FONT>
																						</TD>
																						<TD width="70%">
																							<asp:TextBox ID="txtLocOfficerName" Width="331px" MaxLength="255" 
                                                                                                Runat="server"></asp:TextBox>
																						</TD>
																					</TR>
																					<tr style="visibility:hidden">
																						<TD vAlign="middle" align="right"><FONT color="#666666">File Barcode : </FONT>
																						</TD>
																						<TD><asp:label id="lblBarcode" Runat="server"></asp:label></TD>
																					</tr>
																					<TR>
																						<TD align="center" colSpan="2"><BR>
                                                                                            
                                                                                            <asp:Button ID="butSubmit" runat="server" Text="Save" />
																							<asp:button id="butReset" Runat="Server" Text="Reset"></asp:button><asp:button id="butCancel" Runat="Server" Text="Cancel"></asp:button></TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start : Hidden Part -->
			<input id="hdnCallFrm" type="hidden" name="hdnCallFrm" runat="server"> 
			<input id="hdnLocationOfficerID" type="hidden" name="hdnLocationOfficerID" runat="server"> 
            <%--<input id="hdnLocationOfficerName" type="hidden" name="hdnLocationOfficerName" runat="server">--%>
			<input id="hdnLocOfficerName" type="hidden" name="hdnLocOfficerName" runat="server"> 

            <!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe id="gToday:contrast:agenda.js" style="Z-INDEX: 101; LEFT: -500px; VISIBILITY: visible; POSITION: absolute; TOP: -500px"
				name="gToday:contrast:agenda.js" src="../common/DateRange/ipopeng.htm" frameBorder="0"
				width="132" scrolling="no" height="142"></iframe>
			<!-- End   : Hidden Part -->
			<tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer></form>
	</body>
</HTML>
