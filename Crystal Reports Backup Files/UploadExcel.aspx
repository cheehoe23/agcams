<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UploadExcel.aspx.vb" Inherits="AMS.UploadExcel" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script type="text/javascript">
	function chkFrm() {
		var flag;
		flag = false;
		
		flag = window.confirm("Are you sure you want to upload file?");
		
		return flag;						
	}
	function fnViewExcelFileResult() {
		    window.open('UploadExcel_result.aspx', 'IssUploadExcelResult_Screen','width=700,height=500,Top=0,left=0,scrollbars=1, resizable=1,menubar=1');
		    return false;
	} 
                           
	</script>
</head>
<body>
    <form id="Frm_UploadExcel" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Issuance Management : Upload Excel</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="352" valign="top">
																		    <div id=divUpload runat=server>
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																				    <TR>
																		                <TD height="4" colspan=2>
																			                <DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		                </TD>
																	                </TR>
																				    <TR>
																						<TD valign="middle" width="35%">
																						    <font color="red">*</font><FONT class="DisplayTitle">Upload Excel File : </FONT>
																						</TD>
																						<TD width="65%">
																							<input id="FUploadFile" runat="server" type="file" />
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2" align=center><asp:Button id="butUpdateAsset" Text="Update Asset" Runat="Server" /></TD>
																					</TR>																																							
																				</TBODY>
																			</TABLE>
																			</div>
																			
																			<div id=divUpdateResult runat=server>
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table9">
																				<TBODY align=left>
																				    <TR>
																						<TD colSpan="2"><b>Please refer below for asset updated result:-</b></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																						    <font color=red>Note:</font> Please click 
																						    <asp:button id="butUploadResult" Runat="server" Text="Generate Excel for Asset Updated Result" Width="291px"></asp:button>
																						    to save results in PC.
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																				    <TR>
																						<TD colSpan="2" height=350 valign=top>
																						    <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgAsset" Runat="server"  AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" datakeyfield="fld_AssetID" 
										                                                        AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_AssetID" headertext="Asset ID" ItemStyle-Height="10">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_Desc" headertext="Description" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_Dept" headertext="Department" ItemStyle-Height="10">
												                                                        <itemstyle width="8%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_Loc" headertext="Location" ItemStyle-Height="10">
												                                                        <itemstyle width="8%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_LocSub" headertext="Sub Location" ItemStyle-Height="10">
												                                                        <itemstyle width="8%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_Owner" headertext="Assigned Owner" ItemStyle-Height="10">
												                                                        <itemstyle width="12%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_SubCat" headertext="Sub Category Name" ItemStyle-Height="10">
												                                                        <itemstyle width="12%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_Remarks" headertext="Remarks" ItemStyle-Height="10">
												                                                        <itemstyle width="12%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_UpdateF" headertext="Updated Status" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fld_ReasonFail" headertext="Reason for Updated Failed" ItemStyle-Height="10">
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
										                                                        </Columns>
									                                                        </asp:datagrid>
																						</TD>
																					</TR>			
																				</TBODY>
																			</TABLE>
																			</div>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>																														
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>

		</form>
</body>
</html>
