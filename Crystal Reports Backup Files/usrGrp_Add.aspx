<%@ Page Language="vb" AutoEventWireup="false" Codebehind="usrGrp_Add.aspx.vb" Inherits="AMS.usrGrp_Add" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>usrGrp_Add</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
		<script language="javascript">
		function checkAll() {
			for (var i=0;i<document.Frm_usrGrpAdd.elements.length;i++) {
				var e = document.Frm_usrGrpAdd.elements[i];
				if (e.name == 'cb_moduleName')
					e.checked = true;
				}
				
			return false;
		}

		function uncheckAll() {
			for (var i=0;i<document.Frm_usrGrpAdd.elements.length;i++) {
				var e = document.Frm_usrGrpAdd.elements[i];
				if (e.name == 'cb_moduleName')
					e.checked = false;
				}
				
			return false;
		}
		
		function isFieldBlank(theField) {
			if (theField.value.length == 0)
				return true;
			else
				return false;
		}

		function chkFrm() {
			var foundError = false;
			var chk = false;
			var i;
			
			if (!foundError && isFieldBlank(document.Frm_usrGrpAdd.txtGroupName)) {
				foundError=true
				document.Frm_usrGrpAdd.txtGroupName.focus()
				alert("Please enter User Group Name.")
			}
			
			
			for (i=0; i < document.Frm_usrGrpAdd.elements.length; i++) {
				if (!foundError && document.Frm_usrGrpAdd.elements[i].checked && document.Frm_usrGrpAdd.elements[i].name=='cb_moduleName') {
					chk = true;
				}
			}

			if (!foundError && chk==false) {
				foundError=true
				alert("Please select at least one Access Rights.")
			}

			if (!foundError){
				var flag = false;
 				flag = window.confirm("Are you sure you want to add this user group?");
 				return flag;}
			else{
				//alert("found error")
				return false;}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Frm_usrGrpAdd" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="TblHeader">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="TblHeader2">
							<tr>
								<td><B>User Management : Add New User Group</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" height="400" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="TblContent">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left">
																				<B><FONT color="red">*</FONT> denotes mandatory field</B></DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="0" cellSpacing="0" width="100%">
																				<TBODY>
																					<TR>
																						<TD valign=top width="35%" align=left>
																							<FONT color="#666666"><font color="red">*</font>User Group Name : </FONT>
																						</TD>
																						<TD width="65%">
																							<asp:TextBox id="txtGroupName" maxlength="100" Runat="server" Width="300px" /> 
																							<br /><FONT size="1" color="#666666">(Maximum 100 characters)</FONT>
																						</TD>
																					</TR>
																					<TR>
																						<TD valign="middle" width="35%">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Admin : </FONT>
																						</TD>
																						<TD width="65%">
																							<asp:radiobuttonlist id="rdAdmin" Runat="server" RepeatDirection="Horizontal">
																							    <asp:ListItem Value="Y">Yes</asp:ListItem>
																							    <asp:ListItem Value="N" Selected=True>No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD colspan="2">
																							<TABLE align="left" border="0" bgcolor="silver" cellPadding="0" cellSpacing="1" width="100%">
																								<tr>
																									<td bgcolor="white"><FONT color="#0000CC"><strong>Module Access Rights For Asset 
                                                                                                        Management Module</strong></FONT></td>
																								</tr>																								
																								<tr>
																									<td>
																										<TABLE border="0" bgcolor="white" cellPadding="0" cellSpacing="1" width="100%">
																											<!-- Start: Display Modules-->
																											<%DisplayModule%>
																											<!-- End  : Display Modules-->
																										</TABLE>
																									</td>
																								</tr>
                                                                                                <tr>
																									<td bgColor="white"><FONT color="#0000CC"><strong>Module Access Rights For File Tracking Module</strong></FONT></td>
																								</tr>
                                                                                                <tr>
																									<td>
																										<TABLE cellSpacing="1" cellPadding="0" width="100%" bgColor="white" border="0">
																											<!-- Start: Display Modules-->
																											<%DisplayModuleForFTS%>
																											<!-- End  : Display Modules-->
                                                                                                        </TABLE>
																									</td>
																								</tr>
																							</TABLE>
																						</TD>
																					</TR>
																					<TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:Button id="butSubmit" Text="Submit" Runat="server"></asp:Button>
																							<asp:Button ID="butReset" Text="Reset" Runat="server"></asp:Button>
																							<asp:Button id="butCheckAll" Text="Check All" Runat="server"></asp:Button>
																							<asp:Button id="butUnCheckAll" Text="UnCheck All" Runat="server"></asp:Button>
																							<asp:Button id="butCancel" Text="Cancel" Runat="server"></asp:Button>
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																			<BR>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
		</form>
	</body>
</HTML>
