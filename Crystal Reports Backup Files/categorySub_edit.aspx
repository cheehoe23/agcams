<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="categorySub_edit.aspx.vb" Inherits="AMS.categorySub_edit" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script type="text/javascript">
	    function chkFrm() {
			var foundError = false;
			
			//Validate Sub category Name
			if (!foundError && gfnIsFieldBlank(document.Frm_SubCategoryEdit.txtSubCatName)) {
				foundError=true;
				document.Frm_SubCategoryEdit.txtSubCatName.focus();
				alert("Please enter Sub Category Name.");
			}
			if (!foundError && document.Frm_SubCategoryEdit.txtSubCatName.value.length > 250) {
				foundError=true
				document.Frm_SubCategoryEdit.txtSubCatName.focus()
				alert("Please make sure the Sub Category Name is less than 250 characters long.")
			}
			
			//Validate Sub Category Additional Information 1
			if (!foundError && document.Frm_SubCategoryEdit.cbAddInfo1.checked) {
			    if (!foundError && gfnIsFieldBlank(document.Frm_SubCategoryEdit.txtAddInfo1)) {
				    foundError=true;
				    document.Frm_SubCategoryEdit.txtAddInfo1.focus();
				    alert("Please enter Additional Information 1.");
			    }
			    if (!foundError && document.Frm_SubCategoryEdit.txtAddInfo1.value.length > 250) {
				    foundError=true
				    document.Frm_SubCategoryEdit.txtAddInfo1.focus()
				    alert("Please make sure the Additional Information 1 is less than 250 characters long.")
			    }
			 }
			 
			 //Validate Sub Category Additional Information 2
			if (!foundError && document.Frm_SubCategoryEdit.cbAddInfo2.checked) {
			    if (!foundError && gfnIsFieldBlank(document.Frm_SubCategoryEdit.txtAddInfo2)) {
				    foundError=true;
				    document.Frm_SubCategoryEdit.txtAddInfo2.focus();
				    alert("Please enter Additional Information 2.");
			    }
			    if (!foundError && document.Frm_SubCategoryEdit.txtAddInfo2.value.length > 250) {
				    foundError=true
				    document.Frm_SubCategoryEdit.txtAddInfo2.focus()
				    alert("Please make sure the Additional Information 2 is less than 250 characters long.")
			    }
			 }
			
			//Validate Sub Category Additional Information 3
			if (!foundError && document.Frm_SubCategoryEdit.cbAddInfo3.checked) {
			    if (!foundError && gfnIsFieldBlank(document.Frm_SubCategoryEdit.txtAddInfo3)) {
				    foundError=true;
				    document.Frm_SubCategoryEdit.txtAddInfo3.focus();
				    alert("Please enter Additional Information 3.");
			    }
			    if (!foundError && document.Frm_SubCategoryEdit.txtAddInfo3.value.length > 250) {
				    foundError=true
				    document.Frm_SubCategoryEdit.txtAddInfo3.focus()
				    alert("Please make sure the Additional Information 3 is less than 250 characters long.")
			    }
			 }
		
			//Validate Sub Category Additional Information 4
			if (!foundError && document.Frm_SubCategoryEdit.cbAddInfo4.checked) {
			    if (!foundError && gfnIsFieldBlank(document.Frm_SubCategoryEdit.txtAddInfo4)) {
				    foundError=true;
				    document.Frm_SubCategoryEdit.txtAddInfo4.focus();
				    alert("Please enter Additional Information 4.");
			    }
			    if (!foundError && document.Frm_SubCategoryEdit.txtAddInfo4.value.length > 250) {
				    foundError=true
				    document.Frm_SubCategoryEdit.txtAddInfo4.focus()
				    alert("Please make sure the Additional Information 4 is less than 250 characters long.")
			    }
			 }
			 
			//Validate Sub Category Additional Information 5
			if (!foundError && document.Frm_SubCategoryEdit.cbAddInfo5.checked) {
			    if (!foundError && gfnIsFieldBlank(document.Frm_SubCategoryEdit.txtAddInfo5)) {
				    foundError=true;
				    document.Frm_SubCategoryEdit.txtAddInfo5.focus();
				    alert("Please enter Additional Information 5.");
			    }
			    if (!foundError && document.Frm_SubCategoryEdit.txtAddInfo5.value.length > 250) {
				    foundError=true
				    document.Frm_SubCategoryEdit.txtAddInfo5.focus()
				    alert("Please make sure the Additional Information 5 is less than 250 characters long.")
			    }
			}
			 
			//validate Useful Life
//			if (!foundError && document.Frm_SubCategoryEdit.ddlValidYear.value == 0) {
//			    foundError=true
//			    document.Frm_SubCategoryEdit.ddlValidYear.focus()
//			    alert("Please select Useful Life.")
//			}
			
			//validate Float Level
			if (!foundError && !gfnIsFieldBlank(document.Frm_SubCategoryEdit.txtFloatLevel)) {
			    //--> Checking Float Level much numeric
				if (!foundError && gfnCheckNumeric(document.Frm_SubCategoryEdit.txtFloatLevel,'')) {
					foundError=true;
					document.Frm_SubCategoryEdit.txtFloatLevel.focus();
					alert("Float Level just allow number only.");
				}
			}
			 
 			if (!foundError){
 			    var flag = false;
 				flag = window.confirm("Are you sure you want to update this Sub Category?");
 				return flag;
 			}
			else
				return false;
		}
		function fnDeleteImages(){
		        var flag = false;
 				flag = window.confirm("You have chosen to delete image.\nYou cannot undo this action. Continue?");
 				return flag;
		}
	</script>
</head>
<body>
    <form id="Frm_SubCategoryEdit" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Administration : Edit Sub Category</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" height="400" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																				    <TR>
																					    <TD valign="middle" width="35%">
																					        <FONT class="DisplayTitle">Category Code : </FONT>
																					    </TD>
																					    <TD width="65%"><asp:Label id="lblCatCode" Runat="server"></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Category Name : </FONT>
																					    </TD>
																					    <TD><asp:Label id="lblCatName" Runat="server"></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Sub Category Code : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtSubCatCode" MaxLength="4" Width="40px" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Sub Category Name : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtSubCatName" MaxLength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Additional Information 1 : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:CheckBox ID="cbAddInfo1" runat=server Text="" />&nbsp;
																					        <asp:TextBox id="txtAddInfo1" MaxLength="250" Runat="server" Width="270px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Additional Information 2 : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:CheckBox ID="cbAddInfo2" runat=server Text="" />&nbsp;
																					        <asp:TextBox id="txtAddInfo2" MaxLength="250" Runat="server" Width="270px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Additional Information 3 : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:CheckBox ID="cbAddInfo3" runat=server Text="" />&nbsp;
																					        <asp:TextBox id="txtAddInfo3" MaxLength="250" Runat="server" Width="270px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Additional Information 4 : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:CheckBox ID="cbAddInfo4" runat=server Text="" />&nbsp;
																					        <asp:TextBox id="txtAddInfo4" MaxLength="250" Runat="server" Width="270px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Additional Information 5 : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:CheckBox ID="cbAddInfo5" runat=server Text="" />&nbsp;
																					        <asp:TextBox id="txtAddInfo5" MaxLength="250" Runat="server" Width="270px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Useful Life : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlValidYear" Runat="server"></asp:DropDownList> Year
																					    </TD>
																				    </TR>
																			        <TR>
																					    <TD valign="middle" width="35%">
																					        <FONT class="DisplayTitle">Float Level : </FONT>
																					    </TD>
																					    <TD width="65%"><asp:TextBox id="txtFloatLevel" maxlength="5" Width="40px" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Expiring Notification before : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlExpNotiB4" Runat="server"></asp:DropDownList> Month
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">AC Tag : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:CheckBox ID="cbACTag" runat="server" Text="" />
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Upload Image : </FONT>
																					    </TD>
																					    <TD>
																					        <input id="FUploadImage" runat="server" type="file" />
																					        <asp:LinkButton ID=butDelImage runat=server>[Delete Image]</asp:LinkButton>
																					        <asp:HyperLink ID=hylFileName runat=server Target="Image4SubCat"></asp:HyperLink>
																					    </TD>
																				    </TR>
																					<TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:Button id="butSubmit" Text="Submit" Runat="Server" />&nbsp;
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />&nbsp;
																							<asp:Button id="butCancel" Text="Cancel" Runat="Server" />
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnCategoryID" runat="server"> 
			<input type="hidden" id="hdnSCategoryID" runat="server"> 
			<input type="hidden" id="hdnCallFrm" runat="server"> 
			<input type="hidden" id="hdnCatAssign2Asset" runat="server">
			<input type="hidden" id="hdnImageSaveF" runat="server">
			<!-- End  : Hidden Fields -->
	</form>
</body>
</html>
