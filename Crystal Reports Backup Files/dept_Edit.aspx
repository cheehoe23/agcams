<%@ Page Language="vb" AutoEventWireup="false" Codebehind="dept_Edit.aspx.vb" Inherits="AMS.dept_Edit" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
		<title>dept_Edit</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
		<script language="JavaScript" src="../Common/CommonJS.js"></script>
	    <LINK href="../common/ComboBox/styles/contactsearch/combo.css" type="text/css" rel="Stylesheet">
        <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
        <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
        <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />

		<script language="javascript">

		    $(document).ready(function () {
		        SearchText("cbHOD", "hfHOD");
		    });

		    function SearchText(obj1, obj2) {
		        $("#" + obj1).autocomplete({
		            source: function (request, response) {
		                $.ajax({
		                    url: '<%#ResolveUrl("~/sysSetting/dept_add.aspx/GetUsersList") %>',
		                    data: "{ 'prefix': '" + request.term + "'}",
		                    dataType: "json",
		                    type: "POST",
		                    contentType: "application/json; charset=utf-8",
		                    success: function (data) {
		                        response($.map(data.d, function (item) {
		                            return {
		                                label: item.split('~')[0],
		                                val: item.split('~')[1]
		                            }
		                        }))
		                    },
		                    error: function (response) {
		                        alert(response.responseText);
		                    },
		                    failure: function (response) {
		                        alert(response.responseText);
		                    }
		                });
		            },
		            select: function (e, i) {
		                $("#" + obj2).val(i.item.val);
		            },
		            minLength: 0
		        });
		    }

		function chkFrm() {
			var foundError = false;

			//validate Department Code 
			if (!foundError && gfnIsFieldBlank(document.frm_deptEdit.txtDeptCode)) {
				foundError=true;
				document.frm_deptEdit.txtDeptCode.focus();
				alert("Please enter Department Code.");
			}
			if (!foundError && document.frm_deptEdit.txtDeptCode.value.length > 10){
					foundError=true
					document.frm_deptEdit.txtDeptCode.focus()
					alert("Please make sure the Department Code is less than 10 character long.");
			}
			
			//Validate Department Name
			if (!foundError && gfnIsFieldBlank(document.frm_deptEdit.txtDeptName)) {
				foundError=true
				document.frm_deptEdit.txtDeptName.focus()
				alert("Please enter Department Name.")
			}
			if (!foundError && document.frm_deptEdit.txtDeptName.value.length > 250) {
				foundError=true
				document.frm_deptEdit.txtDeptName.focus()
				alert("Please make sure the Department Name is less than 250 characters long.")
			}
			
			//Validate Head of Department
			if (!foundError && gfnIsFieldBlank(document.frm_deptEdit.cbHOD)) {
                foundError=true
                alert("Please enter Head of Department.")
            }
			
 			if (!foundError){
 				var flag = false;
 				flag = window.confirm("Are you sure want to update this record?");
 				return flag;
 			}
			else
				return false;
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="frm_deptEdit" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Administration : Edit Department</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table id="Table2" height="400" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<!--Start: Main Content-->
						<table id="Table3" height="400" cellSpacing="1" cellPadding="0" width="100%" align="left"
							bgColor="silver" border="0">
							<tr>
								<td>
									<table id="Table4" height="400" cellSpacing="0" cellPadding="0" width="100%" align="left"
										bgColor="white" border="0">
										<tr>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
													<TR>
														<TD vAlign="top" align="center" width="100%">
														    <asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE id="Table6" height="126" cellSpacing="0" cellPadding="3" width="100%" align="left"
																bgColor="white" border="0">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" height="352">
																			<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
																				<TBODY>
																					<TR>
																						<TD vAlign="middle" width="35%" align="left">
																						    <FONT color="#666666"><font color="red">*</font>Department Code : </FONT>
																						</TD>
																						<TD width="65%">
																						    <asp:textbox id="txtDeptCode" Runat="server" MaxLength=10></asp:textbox>
																							<FONT color="#666666" size="1">(Maximum 10 characters)</FONT></TD>
																					</TR>
																					<TR>
																						<TD vAlign=top width="35%" align="left">
																						    <FONT color="#666666"><font color="red">*</font>Department Name : </FONT>
																						</TD>
																						<TD width="65%">
																						    <asp:textbox id="txtDeptName" Runat="server" maxlength="250" Width="300px"></asp:textbox>
																							<br /><FONT color="#666666" size="1">(Maximum 250 characters)</FONT></TD>
																					</TR>
																					<TR>
																					    <TD>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Head of Department : </FONT>
																					    </TD>
																					    <TD>
                                                                                            <asp:TextBox ID="cbHOD" runat="server" Width="300px"></asp:TextBox>                                                                                            
																					    </TD>
																				    </TR>
																					<TR>
																						<TD align="center" colSpan="2"><BR>
																							<asp:button id="butSubmit" Runat="Server" Text="Submit"></asp:button>
																							<asp:button id="butReset" Runat="Server" Text="Reset"></asp:button>
																							<asp:button id="ButCancel" Runat="Server" Text="Cancel"></asp:button>
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start : Hidden Fields -->
			<asp:TextBox Visible="False" ID="hdnDeptID" Runat="server"></asp:TextBox>
			<!-- End   : Hidden Fields -->
			<tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer>
            <asp:HiddenField ID="hfHOD" runat="server" />
		</form>
	</body>
</HTML>
