<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="asset_view.aspx.vb" Inherits="AMS.asset_view" AspCompat="true" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script type="text/javascript">

	    function ClickPrint(assetid) {
	        if (confirm('Confirm to print RFID Tag?')) {
	            document.getElementById("hdnAssetIDs").value = assetid;
	            __doPostBack('hdnclickprint', 'OnClick');
	        }
	    }

	function fnConfirmSelectedRec(CallType) {
				var flag, CallFrm, ErrorSelect, confirmAction;
				flag = false;
				
				//Get Error Message for related action
				//--> (PL)Print Label, (PRPL)Print RFID Passive Label, (AT)Asset Transfer, (DA)Delete Asset, (DiA)Disposed Asset, (RA)Redundancy Asset
				switch(CallType)    
				{
					case 'PL':
						ErrorSelect = "At least one record should be selected for barcode label printing.";
						confirmAction = "Are you sure you want to print barcode label for this record(s)?"; 
						break    
						
					case 'PRPL':
						ErrorSelect = "At least one record should be selected for RFID passive label printing.";
						confirmAction = "Are you sure you want to print RFID passive label for this record(s)?"; 
						break  
						
					case 'PRAT':
						ErrorSelect = "At least one record should be selected for RFID active tag printing.";
						confirmAction = "Are you sure you want to print RFID active tag for this record(s)?"; 
						break    
						
					case 'AT':
						ErrorSelect = "At least one record should be selected for transfer location.";
						confirmAction = "Are you sure you want to transfer location for this record(s)?"; 
						break
						
					case 'DA':
						ErrorSelect = "At least one record should be selected for deletion.";
						confirmAction = "You have chosen to delete one or more asset(s).\nYou cannot undo this action. Continue?"; 
						break
						
					case 'DiA':
						ErrorSelect = "At least one record should be selected for disposed.";
						confirmAction = "Are you sure you want to dispose for this record(s)?"; 
						break
						
					case 'RA':
						ErrorSelect = "At least one record should be selected for redundancy.";
						confirmAction = "Are you sure you want to redundancy for this record(s)?"; 
						break
				}
				
				
				if (gfnCheckSelect(ErrorSelect)){
				    flag = window.confirm(confirmAction);                    
				}
				else  {
					flag = false;
				}
				return flag;
		}
	</script>
</head>
<body>
    <form id="Frm_AssetView" method="post" runat="server">
    <asp:Button ID="hdnclickprint" runat="server" Visible =false ></asp:button>
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Asset Management : <asp:Label ID=lblHeader runat=server></asp:Label></B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<table id="Table2" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tbody>
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr>
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td align="center"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td vAlign="top" width="100%">
						<!--Start Main Content-->
						<table height="400" cellSpacing="1" cellPadding="0" width="100%" align="left" border="0">
							<tr>
								<td vAlign="top" align="left">
								    <asp:button id="butPrintResult" Runat="server" Text="Print Search Result" Width="162px"></asp:button>
								    <asp:button id="butPrintLabel" Runat="server" Text="Print Barcode Label" Width="160px"></asp:button>
								    <asp:button id="butPrintRFIDLabel" Runat="server" 
                                        Text="Print RFID and Barcode " Width="195px"></asp:button>
								    <asp:button id="butPrintRFIDActTag" Runat="server" Text="Print RFID Active Tag" 
                                        Width="180px" Visible="False"></asp:button>
								    <asp:button id="butBulkTransfer" Runat="server" Text="Asset Transfer" Width="124px"></asp:button>
									<asp:button id="butDelAsset" Runat="server" Text="Delete Asset" Width="115px"></asp:button>
									<asp:button id="butDisposed" Runat="server" Text="Dispose Asset" Width="130px"></asp:button>
									<asp:button id="butRedundancy" Runat="server" Text="Redundant Asset" Width="140px"></asp:button>
								</td>
							</tr>
							<tr>
								<td vAlign="top" align="right" bgColor="#a3a9cc">
									<table cellSpacing="1" cellPadding="1" width="100%" border="0">
										<tr align=left>
											<td width="20%">
											    <asp:dropdownlist id="ddlPageSize" Runat="server" AutoPostBack="True"></asp:dropdownlist>&nbsp;&nbsp;<font color="white">per page</font>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td vAlign="top" align="center" height="400">
									<!--Datagrid for display record.-->
									<asp:datagrid id="dgAsset" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										ItemStyle-Height="25" AllowSorting="True" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" AllowPaging="True" PageSize="10" PagerStyle-HorizontalAlign="Right"
										PagerStyle-VerticalAlign="Middle" PagerStyle-Mode="NumericPages" PagerStyle-Position="Top"
										PagerStyle-BackColor="#a3a9cc" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										<headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											Height="25"></headerstyle>
										<Columns>
										    <asp:boundcolumn HeaderText="fld_AssetID" datafield="fld_AssetID" Visible=false>
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn HeaderText="Asset ID" datafield="fldAssetBarcode" SortExpression="fldAssetBarcode">
												<itemstyle width="15%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr" SortExpression="fld_AssetTypeStr" >
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Category" datafield="fld_CategoryName" SortExpression="fld_CategoryName">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Subcategory" datafield="fld_CatSubName" SortExpression="fld_CatSubName">
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Department" datafield="fld_DepartmentName" SortExpression="fld_DepartmentName">
												<itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Location" datafield="fld_LocationName" SortExpression="fld_LocationName">
												<itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Sub Location" datafield="fld_LocSubName" SortExpression="fld_LocSubName">
												<itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Assigned Owner" datafield="fld_OwnerName" SortExpression="fld_OwnerName">
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Asset Status" datafield="fld_AssetStatusStr" SortExpression="fld_AssetStatusStr">
												<itemstyle width="8%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											
											<asp:boundcolumn headertext="Movement Status" datafield="DGLastMv" SortExpression="DGLastMv" Visible=false>
												<itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>
											
                                            <asp:boundcolumn headertext="Short Description" datafield="fld_ShortDescription" SortExpression="fld_ShortDescription" >
												<itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>

                                            <asp:boundcolumn headertext="Other Information" datafield="fld_OtherInformation" SortExpression="fld_OtherInformation" >
												<itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>

                                            <asp:boundcolumn headertext="Device Type" datafield="fld_DeviceType" SortExpression="fld_DeviceType" >
												<itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>

                                            <asp:boundcolumn headertext="Serial No" datafield="fld_SerialNo" SortExpression="fld_SerialNo" >
												<itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>

                                            <asp:boundcolumn headertext="Mac Address" datafield="fld_MacAddress" SortExpression="fld_MacAddress" >
												<itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>

                                            <asp:boundcolumn headertext="New Host name" datafield="fld_NewHostname" SortExpression="fld_NewHostname" >
												<itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>

                                            <asp:boundcolumn headertext="Status" datafield="fld_Status" SortExpression="fld_Status" >
												<itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>

                                            <asp:boundcolumn headertext="Machine Model" datafield="fld_MachineModel" SortExpression="fld_MachineModel" >
												<itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>

											<asp:templatecolumn HeaderText="Select" ItemStyle-Width="17%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
												<itemtemplate>
													<%#fnShowEditPrintIcon(DataBinder.Eval(Container.DataItem, "fld_AssetID"), DataBinder.Eval(Container.DataItem, "fldAssetBarcode"), DataBinder.Eval(Container.DataItem, "fld_AssetTypeStr"))%>
												</itemtemplate>
											</asp:templatecolumn>
											<asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="5%">
												<headertemplate>
													<asp:checkbox id="CheckAll" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
														runat="server" />
												</headertemplate>
												<itemtemplate>
													<center>
														<asp:checkbox id="DeleteThis" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
															runat="server" />
													</center>
												</itemtemplate>
											</asp:templatecolumn>
										</Columns>
									</asp:datagrid>
								</td>
							</tr>
						</table>
						<!--End Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
				</tbody>
			</table>
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnAssetIDs" runat="server"> 
			<input type="hidden" id="hdnSortName" runat="server"> 
			<input type="hidden" id="hdnSortAD" runat="server">
			<input type="hidden" id="hdnCallFrm" runat="server">
			<input type="hidden" id="hdnAssetIdAMS" runat="server">
			<input type="hidden" id="hdnAssetIdNFS" runat="server">
			<input type="hidden" id="hdnAssetType" runat="server">
			<input type="hidden" id="hdnAssetCatID" runat="server">
			<input type="hidden" id="hdnAssetCatSubID" runat="server">
			<input type="hidden" id="hdnCtrlItemF" runat="server">
			<input type="hidden" id="hdnAssetStatus" runat="server">
			<input type="hidden" id="hdnPurchaseFDt" runat="server">
			<input type="hidden" id="hdnPurchaseTDt" runat="server">
			<input type="hidden" id="hdnWarExpFDt" runat="server">
			<input type="hidden" id="hdnWarExpTDt" runat="server">
			<input type="hidden" id="hdnDeptID" runat="server">
			<input type="hidden" id="hdnLocID" runat="server">
			<input type="hidden" id="hdnLocSubID" runat="server">
			<input type="hidden" id="hdnOwnerID" runat="server">
			<input type="hidden" id="hdnTempAsset" runat="server">
			<input type="hidden" id="hdnLabelPrintF" runat="server">
			<input type="hidden" id="hdnPrintLabelF" runat=server />
			<input type="hidden" id="hdnPrintRFIDLabelF" runat="server">
			<input type="hidden" id="hdnAssetIDsSelected" runat=server />

            <input type="hidden" id="hdnShortDescription" runat="server" />
            <input type="hidden" id="hdnOtherInformation" runat="server" />
            <input type="hidden" id="hdnDeviceTypeID" runat="server" />
            <input type="hidden" id="hdnSerialNo" runat="server" />
            <input type="hidden" id="hdnMacAddress" runat="server" />
            <input type="hidden" id="hdnNewHostname" runat="server" />
            <input type="hidden" id="hdnStatus" runat="server" />
            <input type="hidden" id="hdnMachineModel" runat="server" />
			<!-- End  : Hidden Fields -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
		</form>
		<script language="javascript">
			if (document.Frm_AssetView.hdnPrintLabelF.value == 'Y')
			{	
				var AssetIDs = document.Frm_AssetView.hdnAssetIDsSelected.value;
				//gfnPrintAssetLabel(AssetIDs);
				gfnPrintAssetLabelMultiple(AssetIDs);
				
				document.Frm_AssetView.hdnPrintLabelF.value = 'N';
				document.Frm_AssetView.hdnAssetIDsSelected.value = '';
			}
//			if (document.Frm_AssetView.hdnPrintRFIDLabelF.value == 'Y')
//			{	
//				var AssetIDs = document.Frm_AssetView.hdnAssetIDsSelected.value;
//				gfnPrintRFIDLabel(AssetIDs);
//				
//				document.Frm_AssetView.hdnPrintRFIDLabelF.value = 'N';
//				document.Frm_AssetView.hdnAssetIDsSelected.value = '';
//			}
		</script>
		<%--<script language="vbscript" type="text/vbscript">
		    'if request("hdnWriteActiveTagF")= "Y" then
		        
		        set oWSH = createobject("WScript.shell")
		        oWSH.Run gRFIDWriteEXE, 1, true
		    'end if
		</script>--%>
</body>
</html>
