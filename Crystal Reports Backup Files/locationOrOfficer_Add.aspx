<%@ Page Language="vb" AutoEventWireup="false" Codebehind="locationOrOfficer_Add.aspx.vb" Inherits="AMS.locationOrOfficer_Add"%>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="~/commonFTS/FTSfooter_cr.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD id="Head1" runat="server">
		<title>Create Location</title>
			<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE11" />
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../commonFTS/FTSstyle.css" type="text/css" rel="Stylesheet">
		<script language="JavaScript" src="../Common/CommonJS.js"></script>	
	    <LINK href="../common/ComboBox/styles/contactsearch/combo.css" type="text/css" rel="Stylesheet">
        <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
    <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />
		<script type="text/javascript" language="javascript">

		    $(document).ready(function () {
		        SearchText("txtLocOfficerName", "hdnLocOfficerName");
		    });

		    function SearchText(obj1, obj2) {
		        $("#" + obj1).autocomplete({
		            source: function (request, response) {
		                $.ajax({
		                    url: '<%#ResolveUrl("~/sysSetting/locationOrOfficer_Add.aspx/GetUsersList") %>',
		                    data: "{ 'prefix': '" + request.term + "'}",
		                    dataType: "json",
		                    type: "POST",
		                    contentType: "application/json; charset=utf-8",
		                    success: function (data) {
		                        response($.map(data.d, function (item) {
		                            return {
		                                label: item.split('~')[0],
		                                val: item.split('~')[1]
		                            }
		                        }))
		                    },
		                    error: function (response) {
		                        alert(response.responseText);
		                    },
		                    failure: function (response) {
		                        alert(response.responseText);
		                    }
		                });
		            },
		            select: function (e, i) {
		                $("#" + obj2).val(i.item.val);
		            },
		            minLength: 0
		        });

		    }

		function chkFrm() {
			var foundError = false;
			var flag = false;
			
			//Validate LocOfficerName
			if (!foundError && gfnIsFieldBlank(document.frm_locationOrOfficer_Add.txtLocOfficerName)) {
				foundError=true
				document.frm_locationOrOfficer_Add.txtLocOfficerName.focus()
				alert("Please enter Location/Officer Name!");
				return false;
            }                         
		}
		</script>
	</HEAD>
	<body>
		<form id="frm_locationOrOfficer_Add" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>File Management : Create New File</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table id="Table2" height="500" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<!--Start: Main Content-->
						<table id="Table3" height="400" cellSpacing="1" cellPadding="0" width="100%" align="left"
							bgColor="silver" border="0">
							<tr>
								<td>
									<table id="Table4" height="400" cellSpacing="0" cellPadding="0" width="100%" align="left"
										bgColor="white" border="0">
										<tr>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
													<TR>
														<TD vAlign="top" align="center" width="100%"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE id="Table6" height="126" cellSpacing="0" cellPadding="3" width="100%" align="left"
																bgColor="white" border="0">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</FONT></B></DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" height="352">
																			<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
																				<TBODY>
																					<TR>
																						<TD vAlign="middle" align="right" width="30%">
                                                                                            <font color="red">*</font>Location 
                                                                                            / Officer Name:</FONT>

																						</TD>
																						<TD width="70%">
																							<asp:TextBox ID="txtLocOfficerName" Width="331px" MaxLength="255" 
                                                                                                Runat="server"></asp:TextBox>
																						</TD>
																					</TR>
																					<TR>
																						<TD align="center" colSpan="2">
                                                                                            
                                                                                            &nbsp;</TD>
																					</TR>
																					<TR>
																						<TD align="center" colSpan="2">
                                                                                            
                                                                                            &nbsp;</TD>
																					</TR>
																					<TR>
																						<TD align="center" colSpan="2">
                                                                                            
                                                                                            <asp:Button ID="butSubmit" runat="server" Text="Save" />
																							<asp:button id="butReset" Runat="Server" Text="Reset"></asp:button>
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>


																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer>
			<!-- Start: Hidden Fields -->
			<input id="hdnLocOfficerName" type="hidden" name="hdnLocOfficerName" runat="server"> 			
			<!-- End  : Hidden Fields -->
		</form>		
	</body>
</HTML>
