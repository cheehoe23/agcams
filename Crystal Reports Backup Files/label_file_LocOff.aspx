<%@ Page Language="vb" AutoEventWireup="false" Codebehind="label_file_LocOff.aspx.vb" Inherits="AMS.label_file_LocOff"%>
<%@ Register TagPrefix="neobarcode" Namespace="Neodynamic.WebControls.BarcodeProfessional" Assembly="Neodynamic.WebControls.BarcodeProfessional" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>label_file</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="FTSstyle.css" type="text/css" rel="Stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="frm_LabelFile" method="post" runat="server">
			<table width="100%" border="0">
				<tr>
					<td>
						<div id="divBarcode">
							<div style="PADDING-RIGHT: 5px; PADDING-LEFT: 10px; PADDING-BOTTOM: 5px; WIDTH: 380px; PADDING-TOP: 5px; TEXT-ALIGN: center">
                                <%--<table width="100%" cellpadding="3" cellspacing="0">
									<tr>
										<td colSpan="2"><span style="FONT-SIZE: 13pt; FONT-FAMILY: Arial"><b>
                                            <asp:label id="lblFileRef" Runat="server"></asp:label></b></span></td>
									</tr>
									<tr>
										<td colSpan="2"><span style="FONT-SIZE: 12pt; FONT-FAMILY: Arial"><span style="FONT-SIZE: 10pt; FONT-FAMILY: Arial"><asp:label 
                                                id="lblFileDetail" Runat="server"></asp:label>
											</span>
										    </span></td>
									</tr>
									<tr>
										<td width="40%"><neobarcode:barcodeprofessional id="barcodeCode" runat="server" Symbology="Code128" Font-Underline="False" Font-Strikeout="False"
												Font-Size="10pt" Font-Names="Arial" Font-Italic="False" Font-Bold="False" BarHeight="0.4"></neobarcode:barcodeprofessional></td>
										<td align="left"><span style="FONT-SIZE: 10pt; FONT-FAMILY: Arial">
                                            <span style="FONT-SIZE: 12pt; FONT-FAMILY: Arial"><asp:label id="lblFileTitle" Runat="server"></asp:label></span>&nbsp;</span></td>
									</tr>
								</table>--%>
                                <table cellpadding="0" cellspacing="0" style="width: 48%" align=left border="0">
						            <tr>
                                        <td align=left style="height: 6px"><span style="FONT-SIZE: 7pt; FONT-FAMILY: Arial">
                                                    <asp:label id="lblLocOfficerID" Runat="server"></asp:label></span></td>
							            <td align=right style="height: 6px">
                                            <span style="FONT-SIZE: 7pt; FONT-FAMILY: Arial">
                                                    &nbsp;</span>
                                        </td>
						            </tr>
						            <tr>
							            <td colspan=2 align=left>

                                         <neobarcode:barcodeprofessional id="barcodeCode" runat="server" Symbology="Code128" Font-Underline="False" Font-Strikeout="False"
									            Font-Size="7pt" Font-Names="Arial" Font-Italic="False" Font-Bold="False" BackColor="White" CacheExpiresAtDateTime="" 
									            ForeColor="Black" Height="35px" IsbnSupplementCode="" Width="170px" QuietZoneWidth="0" DisplayCode=false Dpi="80"
									             ></neobarcode:barcodeprofessional>
                                        </td>                                       
                                        
						            </tr>
                                    <tr>
                                        <td align=left style="height: 6px"><span style="FONT-SIZE: 7pt; FONT-FAMILY: Arial">
                                                            <span style="FONT-SIZE: 7pt; FONT-FAMILY: Arial">
                                            <asp:label id="lblLocOffName" Runat="server"></asp:label></span></span>
                                        </td>
							            <td align=right style="height: 6px">&nbsp;</td>
						            </tr>
						    
					            </table>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</form>
		<script type="text/javascript">       
         // Print the window            
         
         window.print();
         
         // Close the window
         //window.close();    
		</script>
	</body>
</HTML>
