<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CRCert_SearchResult.aspx.vb" Inherits="AMS.CRCert_SearchResult" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" type="text/javascript" src="../Common/CommonJS.js"></script>
</head>
<body>
    <form id="Frm_SearchCert" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Dispose Management : Condemnation/Redundancy Search Result Certificate</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<table id="Table2" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tbody>
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr>
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td align="center"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td vAlign="top" width="100%">
						<!--Start Main Content-->
						<table height="400" cellSpacing="1" cellPadding="0" width="100%" align="left" border="0">
							<tr>
								<td vAlign="top" align="right" bgColor="#a3a9cc">
									<table cellSpacing="1" cellPadding="1" width="100%" border="0">
										<tr align=left>
											<td width="20%">
											    <asp:dropdownlist id="ddlPageSize" Runat="server" AutoPostBack="True"></asp:dropdownlist>&nbsp;&nbsp;<font color="white">per page</font>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td vAlign="top" align="center" height="400">
									<!--Datagrid for display record.-->
									<asp:datagrid id="dgCert" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										ItemStyle-Height="25" AllowSorting="True" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" AllowPaging="True" PageSize="10" PagerStyle-HorizontalAlign="Right"
										PagerStyle-VerticalAlign="Middle" PagerStyle-Mode="NumericPages" PagerStyle-Position="Top"
										PagerStyle-BackColor="#a3a9cc" datakeyfield="fld_CertMstID" AlternatingItemStyle-BackColor="#e3d9ee">
										<headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											Height="25"></headerstyle>
										<Columns>
										    <asp:boundcolumn HeaderText="fld_CertMstID" datafield="fld_CertMstID" Visible=false>
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn HeaderText="Cert S/No" datafield="fld_CertSNo" SortExpression="fld_CertSNo">
												<itemstyle width="20%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											</asp:boundcolumn>
											<asp:templatecolumn HeaderText="Cert Status" SortExpression="fld_CertStatus" ItemStyle-Width="20%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
												<itemtemplate>
													<%#fnShowStatus(DataBinder.Eval(Container.DataItem, "fld_CertStatus"))%>
												</itemtemplate>
											</asp:templatecolumn>
											<asp:templatecolumn HeaderText="Cert Type" SortExpression="fld_CertType" ItemStyle-Width="20%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
												<itemtemplate>
													<%#fnShowType(DataBinder.Eval(Container.DataItem, "fld_CertType"))%>
												</itemtemplate>
											</asp:templatecolumn>
											<asp:boundcolumn datafield="fld_CreatedDt" SortExpression="fld_CreatedDt" headertext="Creation Date" dataformatstring="{0:dd/MM/yyyy}"
												ItemStyle-Height="10">
												<itemstyle width="20%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:templatecolumn HeaderText="Select" ItemStyle-Width="10%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
												<itemtemplate>
													<%#fnShowEditIcon(DataBinder.Eval(Container.DataItem, "fld_CertType"), DataBinder.Eval(Container.DataItem, "fld_CertMstID"))%>
												</itemtemplate>
											</asp:templatecolumn>
										</Columns>
									</asp:datagrid>
								</td>
							</tr>
						</table>
						<!--End Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
				</tbody>
			</table>
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnSortName" runat="server"> 
			<input type="hidden" id="hdnSortAD" runat="server">
			<input type="hidden" id="hdnCertSNo" runat="server">
			<input type="hidden" id="hdnCertCFDt" runat="server">
			<input type="hidden" id="hdnCertCTDt" runat="server">
			<input type="hidden" id="hdnCertStatus" runat="server">
			<input type="hidden" id="hdnCertType" runat="server">
			<input type="hidden" id="hdnAssetIDAMS" runat="server">
			<input type="hidden" id="hdnAssetIDNFS" runat="server">
			<!-- End  : Hidden Fields -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
		</form>
</body>
</html>
