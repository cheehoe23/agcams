<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="category_add.aspx.vb" Inherits="AMS.category_add" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../common/CommonJS.js"></script>
    <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
        <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
        <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />
	<script language="javascript">	    

			function mfnOpenAddSubStatusWindow() 
			{
				var foundError = false;
				
				//validate Category Code is not blank 
				if (!foundError && gfnIsFieldBlank(document.Frm_CategoryAdd.txtCatCode)) {
					foundError=true;
					document.Frm_CategoryAdd.txtCatCode.focus();
					alert("Please enter Category Code.");
				}
				
				//validate Category Name is not blank 
				if (!foundError && gfnIsFieldBlank(document.Frm_CategoryAdd.txtCatName)) {
					foundError=true;
					document.Frm_CategoryAdd.txtCatName.focus();
					alert("Please enter Category Name.");
				}
				
				if (!foundError){
				    var CC = document.Frm_CategoryAdd.txtCatCode.value;
					var CN = document.Frm_CategoryAdd.txtCatName.value;
					window.open('category_AddSubAsset.aspx?CallFrm=Add&CN='+CN+'&CC='+CC, 'Add_Asset','width=550,height=480,Top=0,left=0,scrollbars=1');
				}
				
				return false;
			
			}
			
			function chkFrm() {
				var foundError = false;
				
				//validate Category Code 
				if (!foundError && gfnIsFieldBlank(document.Frm_CategoryAdd.txtCatCode)) {
					foundError=true;
					document.Frm_CategoryAdd.txtCatCode.focus();
					alert("Please enter Category Code.");
				}
				if (!foundError && document.Frm_CategoryAdd.txtCatCode.value.length != 2) {
					foundError=true
					document.Frm_CategoryAdd.txtCatCode.focus();
					alert("Please make sure the Category Code is 2 characters long.");
				}
				
				//validate Category Name 
				if (!foundError && gfnIsFieldBlank(document.Frm_CategoryAdd.txtCatName)) {
					foundError=true;
					document.Frm_CategoryAdd.txtCatName.focus();
					alert("Please enter Category Name.");
				}
				if (!foundError && document.Frm_CategoryAdd.txtCatName.value.length > 250) {
					foundError=true
					document.Frm_CategoryAdd.txtCatName.focus();
					alert("Please make sure the Category Name is less than 250 characters long.");
				}
				
				
				//Validate at least 1 Sub Status is created (if Status have Sub Status)
				if (!foundError && document.Frm_CategoryAdd.hdnSubCatID.value == '0') {
					foundError=true;
					alert("Please create at least 1 Subcategory.")
				}
				
								
 				if (!foundError){
 				    var flag = false;
 					flag = window.confirm("Are you sure you want to add this category?");
 					return flag;
 				}
				else
					return false;
			}
    </script>
</head>
<body>
    <form id="Frm_CategoryAdd" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Administration : Add New Category</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" height="400" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																				    <TR>
																					    <TD valign="middle" width="35%">
																					        <FONT class="DisplayTitle"><font color="red">*</font>Category Code : </FONT>
																					    </TD>
																					    <TD width="65%"><asp:TextBox id="txtCatCode" maxlength="2" Width="40px" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD valign="middle" width="35%">
																					        <FONT class="DisplayTitle"><font color="red">*</font>Category Name : </FONT>
																					    </TD>
																					    <TD width="65%"><asp:TextBox id="txtCatName" maxlength="250" Runat="server" Width="300px"></asp:TextBox>
                                                                                        </TD>
																				        
                                                                                    </TR>
																			        <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Subcategory</b></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2" align=right>[<asp:LinkButton ID=butAddSubCat runat=server>Add Subcategory</asp:LinkButton>]</TD>
																					</TR>
																				    <TR>
																					    <TD colspan=2>
																					        <!--Datagrid for display record.-->
																					        <asp:datagrid id="dgSubCategory" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_CatSubID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn datafield="fld_CatSubID" headertext="fld_CatSubID" ItemStyle-Height="10" Visible=false>
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
										                                                            <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Sub Category Code" datafield="fld_CatSubCode">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Sub Category Name" datafield="fld_CatSubName">
												                                                        <itemstyle width="20%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn ItemStyle-Width="25%" HeaderText="Additional Information (Mandatory/Optional)" 
											                                                            ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												                                                        <itemtemplate>
													                                                        <%#fnGetAddInfo(DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfo1"), DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfoM1"), _
													                                                                        DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfo2"), DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfoM2"), _
													                                                                        DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfo3"), DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfoM3"), _
													                                                                        DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfo4"), DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfoM4"), _
													                                                                        DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfo5"), DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfoM5"))%>
												                                                        </itemtemplate>
											                                                        </asp:templatecolumn>
											                                                        <asp:boundcolumn headertext="Useful Life (Year)" datafield="fld_UsefulLifeYear">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>	
											                                                        <asp:boundcolumn headertext="Float Level" datafield="fld_FloatLevel">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>	
											                                                        <asp:boundcolumn headertext="Expiring Notification (Month)" datafield="fld_ExpNoticeB4">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="AC Tag" datafield="fld_ACTagName">
												                                                        <itemstyle width="7%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn ItemStyle-Width="5%" HeaderText="<IMG SRC=../images/audit.gif Border=0 alt='View Images.'>" 
							                                                                            ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Center>
								                                                                        <itemtemplate>
									                                                                        <%#fnGetFileName(DataBinder.Eval(Container.DataItem, "fld_CatSubID"), DataBinder.Eval(Container.DataItem, "fld_FileName"))%>
								                                                                        </itemtemplate>
							                                                                        </asp:templatecolumn>
											                                                        <asp:ButtonColumn ItemStyle-Width="5%" Text="<IMG SRC=../images/delete.gif Border=0>" ItemStyle-HorizontalAlign="Center"
																										HeaderText="<IMG SRC=../images/delete.gif Border=0>" CommandName="Delete"></asp:ButtonColumn>		
										                                                        </Columns>
									                                                        </asp:datagrid>
																					    </TD>
																					</TR>
																					<TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:Button id="butSubmit" Text="Submit" Runat="Server" />&nbsp;
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />&nbsp;
																							<asp:Button id="butCancel" Text="Cancel" Runat="Server" />
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnSubCatID" runat="server"> 
			<input type="hidden" id="hdnAddSubCatF" runat="server">
			<!-- End  : Hidden Fields -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
	</form>
</body>
</html>
