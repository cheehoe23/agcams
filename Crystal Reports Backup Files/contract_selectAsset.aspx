<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="contract_selectAsset.aspx.vb" Inherits="AMS.contract_addAsset" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>AMS</title>
    <LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>	
	<LINK href="../common/ComboBox/styles/contactsearch/combo.css" type="text/css" rel="Stylesheet">
    <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
    <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />

	<script language="javascript">
	    $(document).ready(function () {
	        SearchText("cbOwner", "hfUser");
	    });

	    function SearchText(obj1, obj2) {
	        $("#" + obj1).autocomplete({
	            source: function (request, response) {
	                $.ajax({
	                    url: '<%#ResolveUrl("~/ContractMng/contract_selectAsset.aspx/GetUsersList") %>',
	                    data: "{ 'prefix': '" + request.term + "'}",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    success: function (data) {
	                        response($.map(data.d, function (item) {
	                            return {
	                                label: item.split('~')[0],
	                                val: item.split('~')[1]

	                            }


	                        }))
	                    },
	                    error: function (response) {
	                        alert(response.responseText);
	                    },
	                    failure: function (response) {
	                        alert(response.responseText);
	                    }
	                });
	            },
	            select: function (e, i) {
	                $("#" + obj2).val(i.item.val);
	            },
	            minLength: 0
	        });

	    }

		function ConfirmAddAsset() 
			{
				var flag;
				flag = false;
				
				 //Call From Edit Contract Screen
				if (document.frm_AssetIncluded.hdnCallFrm.value=='EditContract')
				{
				    if (gfnCheckSelect("At least one record should be selected.")){
					    flag = window.confirm("Are you sure want to add one or more asset(s) for contract?");
				    }
				    else  {
					    flag = false;
				    }
				    return flag;
				}
				else{
				    if (gfnCheckSelect("At least one record should be selected.")){
				        flag = true;
				    }
				    else  {
					    flag = false;
				    }
				    return flag;
				}
								
			}
	</script>
</head>
<body>
    <form id="frm_AssetIncluded" method="post" runat="server">
			<TABLE id="tableSubStatusAdd" cellSpacing="1" cellPadding="1" width="90%" align="center"
				bgColor="white" border="0">
				<TBODY>
					<TR>
						<TD colSpan="2"><B>Select Asset Included</B></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></TD>
					</TR>
					<tr>
						<td colSpan="2">
							<!-- Start: Search Part -->
							<table width="100%" border="0">
								<TR>
									<TD align="left" colSpan="2">&nbsp;</TD>
								</TR>
								<TR class="TRTitleBG">
									<TD align="left" colSpan="2"><b>Search</b></TD>
								</TR>
								<TR>
									<TD valign="middle" width="35%">
									    <FONT class="DisplayTitle">Asset Type : </FONT>
									</TD>
									<TD width="65%">
									    <asp:DropDownList ID="ddlAssetType" Runat="server"></asp:DropDownList>
									</TD>
								</TR>
						        <TR>
								    <TD>
								        <FONT class="DisplayTitle">Asset Category : </FONT>
								    </TD>
								    <TD>
								       <asp:DropDownList ID="ddlAssetCat" AutoPostBack=true Runat="server"></asp:DropDownList>
									</TD>
							    </TR>
							    <TR>
								    <TD>
								        <FONT class="DisplayTitle">Asset Subcategory : </FONT>
								    </TD>
								    <TD>
								        <asp:DropDownList ID="ddlAssetSubCat" Runat="server"></asp:DropDownList>
									</TD>
							    </TR>
						        <TR>
								    <TD>
								        <FONT class="DisplayTitle">Department : </FONT>
								    </TD>
								    <TD>
									    <asp:DropDownList ID="ddlDepartment" Runat="server"></asp:DropDownList>
								    </TD>
							    </TR>
							    <TR>
								    <TD>
								        <FONT class="DisplayTitle">Location : </FONT>
								    </TD>
								    <TD>
									    <asp:DropDownList ID="ddlLocation" Runat="server" AutoPostBack=true></asp:DropDownList>
								    </TD>
							    </TR>
							    <TR>
				                    <TD>
				                        <FONT class="DisplayTitle">Sub Location : </FONT>
				                    </TD>
				                    <TD>
					                    <asp:DropDownList ID="ddlLocSub" Runat="server"></asp:DropDownList>
				                    </TD>
			                    </TR>
							    <TR>
								    <TD>
								        <FONT class="DisplayTitle">Assigned Owner : </FONT>
								    </TD>
								    <TD>
									    
                                                                                            <asp:TextBox ID="cbOwner" runat="server" Width="300px"></asp:TextBox>
									    
								    </TD>
							    </TR>
								<TR>
									<TD align="center" colSpan="2">
									    <asp:button id="butSearch" Runat="Server" Width="50" Text="Search"></asp:button>
										<asp:button id="butSearchReset" Runat="Server" Width="50" Text="Reset"></asp:button>
										<asp:button id="butSearchCancel" Runat="Server" Width="50" Text="Cancel"></asp:button></TD>
								</TR>
							</table>
							<!-- End  : Search Part -->
							<!-- Start: Search Result Part -->
							<div id="divSearchResult" runat="server">
								<table width="100%" border="0">
									<TR>
										<TD align="left" colSpan="2">&nbsp;</TD>
									</TR>
									<TR>
										<TD align="left" colSpan="2">&nbsp;</TD>
									</TR>
									<TR class="TRTitleBG">
										<TD align="left" colSpan="2"><b>Search Result</b></TD>
									</TR>
									<TR>
										<TD vAlign="top" colSpan="2">
											<!--Datagrid for display record.-->
                                            <asp:datagrid id="dgAsset" Runat="server" AlternatingItemStyle-Height="25"
                                                ItemStyle-Height="25" AllowSorting="True" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
                                                BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
                                                <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
                                                    Height="25"></headerstyle>
                                                <Columns>
                                                    <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                                    </asp:boundcolumn>
                                                    <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                                    </asp:boundcolumn>
                                                    <asp:boundcolumn HeaderText="Asset ID" datafield="fldAssetBarcode" >
												        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											        </asp:boundcolumn>
											        <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr"  >
												        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											        </asp:boundcolumn>
											        <asp:boundcolumn headertext="Category" datafield="fld_CategoryName" >
												        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											        </asp:boundcolumn>
											        <asp:boundcolumn headertext="Subcategory" datafield="fld_CatSubName" >
												        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											        </asp:boundcolumn>
                                                    <asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="5%">
												        <headertemplate>
													        <asp:checkbox id="CheckAll" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
														        runat="server" />
												        </headertemplate>
												        <itemtemplate>
													        <center>
														        <asp:checkbox id="DeleteThis" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
															        runat="server" />
													        </center>
												        </itemtemplate>
											        </asp:templatecolumn>
                                                </Columns>
                                            </asp:datagrid>
                                         </TD>
									</TR>
									<TR>
										<TD vAlign="top" align=center colSpan="2">
										<asp:button id="butAdd" Runat="Server" Text="Add Asset"></asp:button>
									</TD>
									</TR>
								</table>
							</div>
							<!-- End  : Search Result Part --></td>
					</tr>
				</TBODY>
			</TABLE>
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnCallFrm" runat="server"> 
			<input type="hidden" id="hdnAssetAddF" runat="server">
			<input type="hidden" id="hdnNewAssetIDs" runat="server">
			<input type="hidden" id="hdnContractID" runat="server">
			<!-- End  : Hidden Fields --></form>
		<script language="javascript">
			if (document.frm_AssetIncluded.hdnAssetAddF.value == 'Y')
			{	
			     //Call From Add Contract Screen
				if (document.frm_AssetIncluded.hdnCallFrm.value=='AddContact')
				{	
					var openerForm = opener.document.forms.Frm_ContractAdd;
					openerForm.action = "contract_add.aspx";
					window.opener.document.forms(0).hdnAddAssetF.value = 'Y';
					window.opener.document.forms(0).hdnNewAssetIDs.value = document.frm_AssetIncluded.hdnNewAssetIDs.value;
				}	
				
				 //Call From Edit Contract Screen
				if (document.frm_AssetIncluded.hdnCallFrm.value=='EditContract')
				{	
					var openerForm = opener.document.forms.Frm_ContractEdit;
					openerForm.action = "Contract_edit.aspx";
					window.opener.document.forms(0).hdnAddAssetF.value = 'Y';
				}
				
				 //Call From Add Renewal Contract Screen
				if (document.frm_AssetIncluded.hdnCallFrm.value=='RenewalContract')
				{	
					var openerForm = opener.document.forms.Frm_ContractAdd;
					openerForm.action = "contract_addRenewal.aspx";
					window.opener.document.forms(0).hdnAddAssetF.value = 'Y';
					window.opener.document.forms(0).hdnNewAssetIDs.value = document.frm_AssetIncluded.hdnNewAssetIDs.value;
				}		
			
				openerForm.method = "post";
				openerForm.submit();
				window.close();	
			}
		</script>
</body>
</html>
