<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="label_Asset.aspx.vb" Inherits="AMS.label_Asset" %>
<%@ Register TagPrefix="neobarcode" Namespace="Neodynamic.WebControls.BarcodeProfessional" Assembly="Neodynamic.WebControls.BarcodeProfessional" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Asset Label</title>
    <LINK href="CommonStyle.css" type="text/css" rel="Stylesheet">
</head>
<body>
    <form id="frm_LabelAsset" method="post" runat="server">
			<table width="100%" border="0">
				<tr>
					<td>
						<div id="divBarcode">
							<div style="PADDING-RIGHT: 5px; PADDING-LEFT: 10px; PADDING-BOTTOM: 5px; WIDTH: 225px; PADDING-TOP: 5px; TEXT-ALIGN: center">
								<table cellpadding="0" cellspacing="0" style="width: 80%" align=left border="0">
									<tr>
										<td align=left style="height: 13px">&nbsp;&nbsp;&nbsp;<asp:label id="lblNFSAssetID" Runat="server"></asp:label></td>
										<td align=right style="height: 13px"><asp:label id="lblLocCode" Runat="server"></asp:label></td>
									</tr>
									<tr>
										<td colspan=2 align=left>
										    <neobarcode:barcodeprofessional id="barcodeCode" runat="server" Symbology="Code128" Font-Underline="False" Font-Strikeout="False"
												Font-Size="10pt" Font-Names="Arial" Font-Italic="False" Font-Bold="False" BarHeight="0.4" DisplayCode=false></neobarcode:barcodeprofessional>
										</td>
									</tr>
									<tr>
										<td align=left>&nbsp;&nbsp;&nbsp;<asp:label id="lblAMSAssetID" Runat="server"></asp:label></td>
										<td align=right><asp:label id="lblDeptCode" Runat="server"></asp:label></td>
									</tr>
									<tr>
										<td colspan=2 align=left>&nbsp;&nbsp;&nbsp;<asp:label id="lblDesc" Runat="server"></asp:label></td>
									</tr>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</table>
			<input type=hidden id=hdnAssetIDs value="" runat=server />
		</form>
		<script type="text/javascript">       
         // Print the window            
         window.print();
         
         // Close the window
         //window.close();    
		</script>
</body>
</html>
