<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Bypasslogin.aspx.vb" Inherits="AMS.ByPassLogin" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <link href="login.css" type="text/css" rel="stylesheet" />
    <title>AMS Backend Login Page</title>
    <script language="JavaScript" src="Common/CommonJS.js"></script>
		 <style type="text/css">
	    /* mac hide \*/
	    html,body{height:100%;width:100%;}
	    /* end hide */	    
    </style>
    	<script language="javascript">
		function chkFrm() {
			var foundError = false;
			
			//Validate Login ID
			if (!foundError && gfnIsFieldBlank(document.frmLogin.txt_userID)) {
				foundError=true
				document.frmLogin.txt_userID.focus()
				alert("Please enter your User ID.")
			}
			//Validate Login ID
			if (!foundError && gfnIsFieldBlank(document.frmLogin.txt_password)) {
				foundError=true
				document.frmLogin.txt_password.focus()
				alert("Please enter your Password.")
			}

 			if (!foundError)
 				return true
			else
				return false
		}
		</script>

</head>
<body>
    <form id="frmLogin" runat="server">
            <div id="outer">
	        <div id="container">
		        <div id="inner">
			        <div class="logobox">
				        <img src="images/rp_logo.gif" alt="" />
			        </div>
				    <div class="userbox">
					    <h2>Asset Management System</h2>
					    <div id="divLoginInfo">
				            
					        <p>If you are not an authorized user, please quit now.</p> 
					        <p>Please diable all pop-up blockers, and ensure that your browser is updated with the latest version.</p> 
					        <p>Minimum resolution size is 1024 x 768.</p>
					    </div>
					    <br />
					   
<div id="divMessage" runat="server" class="custom_message" enableviewstate="false" visible="false">
   <strong><asp:Literal ID="ltMessage" runat="server"></asp:Literal></strong>
</div>

					    <div class="loginform">
					        <p><label for='tbUserId'>Username:</label><asp:textbox runat="server"  name="txt_userID" type="text" maxlength="30" id="txt_userID" cssclass="login" style="width:150px;" ></asp:textbox> </p>
					        <p><label for='tbPassword'>Password:</label><asp:textbox runat="server" TextMode="password"  name="txt_password" type="password" maxlength="30" id="txt_password" cssclass="login" style="width:150px;" ></asp:textbox> </p>
					        <p style="line-height:25px"><asp:button runat="server"  text="Login" id="btnLogin" cssclass="genButton" style="width:80px;" ></asp:button><br />
                        </div>
                    </div>
                    <div class="clr"></div>
                </div>						
		    </div>
	    </div>
        
    
</form>
</body>
</html>
