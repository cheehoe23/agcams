<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="category_view.aspx.vb" Inherits="AMS.category_view" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script language="javascript">
		function ConfirmDelete() 
			{
				var flag;
				flag = false;
				if (gfnCheckDelSelect()){
					flag = window.confirm("You have chosen to delete one or more Category/Subcategory.\nYou cannot undo this action. Continue?");
				}
				else  {
					flag = false;
				}
				return flag;
			}
		</script>
</head>
<body>
     <form id="Frm_CategoryView" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Administration : Category</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<table id="Table2" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tbody>
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr>
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td align="center"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td vAlign="top" width="100%">
						<!--Start Main Content-->
						<table height="400" cellSpacing="1" cellPadding="0" width="100%" align="left" border="0">
							<tr>
								<td vAlign="top" align="left">
									<asp:button id="butAddCategory" Runat="server" Text="Add New Category"></asp:button>&nbsp;
									<asp:button id="butDelCategory" Runat="server" Text="Delete Category"></asp:button>
								</td>
							</tr>
							<tr>
								<td vAlign="top" align="right" bgColor="#a3a9cc">
									<table cellSpacing="1" cellPadding="1" width="100%" border="0">
										<tr align=left>
											<td width="20%">
											    <asp:dropdownlist id="ddlPageSize" Runat="server" AutoPostBack="True"></asp:dropdownlist>&nbsp;&nbsp;<font color="white">per page</font>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td vAlign="top" align="center" height="400">
									<!--Datagrid for display record.-->
									<asp:datagrid id="dgCategory" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										ItemStyle-Height="25" AllowSorting="True" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" AllowPaging="True" PageSize="10" PagerStyle-HorizontalAlign="Right"
										PagerStyle-VerticalAlign="Middle" PagerStyle-Mode="NumericPages" PagerStyle-Position="Top"
										PagerStyle-BackColor="#a3a9cc" datakeyfield="fld_CatSubID" AlternatingItemStyle-BackColor="#e3d9ee">
										<headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											Height="25"></headerstyle>
										<Columns>
										    <asp:boundcolumn HeaderText="Category ID" datafield="fld_CategoryID" Visible=false>
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
										    <asp:boundcolumn HeaderText="Sub Category ID" datafield="fld_CatSubID" Visible=false>
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn HeaderText="Category Code" datafield="fld_CategoryCode" SortExpression="fld_CategoryCode">
												<itemstyle width="8%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:hyperlinkcolumn headertext="Category Name" text="fld_CategoryName" SortExpression="fld_CategoryName" datatextfield="fld_CategoryName"
												navigateurl="category_edit.aspx" datanavigateurlfield="fld_CategoryID" DataNavigateUrlFormatString="category_edit.aspx?CategoryID={0}">
												<itemstyle cssclass="GridText" horizontalalign="Left" width="15%" verticalalign="Middle"></itemstyle>
											</asp:hyperlinkcolumn>
											<asp:boundcolumn headertext="Sub Category Code" datafield="fld_CatSubCode" SortExpression="fld_CatSubCode" >
												<itemstyle width="8%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:hyperlinkcolumn headertext="Sub Category Name" text="fld_CatSubName" SortExpression="fld_CatSubName" datatextfield="fld_CatSubName"
												navigateurl="categorySub_edit.aspx" datanavigateurlfield="fld_CatSubID" DataNavigateUrlFormatString="categorySub_edit.aspx?CallFrm=View&SCategoryID={0}">
												<itemstyle cssclass="GridText" horizontalalign="Left" width="19%" verticalalign="Middle"></itemstyle>
											</asp:hyperlinkcolumn>
											<asp:templatecolumn ItemStyle-Width="20%" HeaderText="Additional Information (Mandatory/Optional)" 
											    ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												<itemtemplate>
													<%#fnGetAddInfo(DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfo1"), DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfoM1"), _
													                         DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfo2"), DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfoM2"), _
													                         DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfo3"), DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfoM3"), _
													                         DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfo4"), DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfoM4"), _
													                         DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfo5"), DataBinder.Eval(Container.DataItem, "fld_CatSubAddInfoM5"))%>
												</itemtemplate>
											</asp:templatecolumn>
											<asp:boundcolumn headertext="Useful Life (Year)" datafield="fld_UsefulLifeYear">
												<itemstyle width="7%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											</asp:boundcolumn>	
											<asp:boundcolumn headertext="Float Level" datafield="fld_FloatLevel">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>	
											<asp:boundcolumn headertext="Expiring Notification (Month)" datafield="fld_ExpNoticeB4">
												<itemstyle width="7%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											</asp:boundcolumn>	
											<asp:boundcolumn headertext="AC Tag" datafield="fld_ACTagName">
												<itemstyle width="7%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											</asp:boundcolumn>	
											<asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="5%">
												<headertemplate>
												    <img src="../images/delete.gif" alt="Delete" runat="server" id="imgDelete">
													<asp:checkbox id="CheckAll" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
														runat="server" />
												</headertemplate>
												<itemtemplate>
													<center>
														<asp:checkbox id="DeleteThis" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
															runat="server" />
													</center>
												</itemtemplate>
											</asp:templatecolumn>
											<asp:boundcolumn HeaderText="CatAssign2Asset" datafield="CatAssign2Asset" Visible=false>
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>			
											<asp:boundcolumn HeaderText="fld_CategoryName" datafield="fld_CategoryName" Visible=false>
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn HeaderText="fld_CatSubName" datafield="fld_CatSubName" Visible=false>
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>		
										</Columns>
									</asp:datagrid>
								</td>
							</tr>
						</table>
						<!--End Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
				</tbody>
			</table>
			<!-- Start: Hidden Fields -->
			<input id="hdnSortName" type="hidden" name="hdnSortName" runat="server"> 
			<input id="hdnSortAD" type="hidden" name="hdnSortAD" runat="server">
			<!-- End  : Hidden Fields -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
	</form>
</body>
</html>
