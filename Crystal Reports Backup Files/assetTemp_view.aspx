<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="assetTemp_view.aspx.vb" Inherits="AMS.assetTemp_view" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script language="javascript">
		function ConfirmMerge() 
			{
				var flag;
				flag = false;
				flag = window.confirm("You have chosen to merge Temporary Asset with NFS Asset.\nYou cannot undo this action. Continue?");
				return flag;
			}
		</script>
</head>
<body>
     <form id="Frm_AssetTemp" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Asset Management : Link AMS to NFS</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<table id="Table2" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tbody>
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr>
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td align="center"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td vAlign="top" width="100%">
						<!--Start Main Content-->
						<table height="400" cellSpacing="1" cellPadding="0" width="100%" align="left" border="0">
							<tr>
								<td vAlign="top" align="left">
									<asp:button id="butAssetMerge" Runat="server" Text="Merge AMS asset with NFS asset" Width="247px"></asp:button>
								</td>
							</tr>
							<tr>
								<td vAlign="top" align="right" bgColor="#a3a9cc">
									<table cellSpacing="1" cellPadding="1" width="100%" border="0">
										<tr align=left>
											<td width="20%">
											    <asp:dropdownlist id="ddlPageSize" Runat="server" AutoPostBack="True"></asp:dropdownlist>&nbsp;&nbsp;<font color="white">per page</font>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td vAlign="top" align="center" height="400">
									<!--Datagrid for display record.-->
									<asp:datagrid id="dgAssetTemp" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										ItemStyle-Height="25" AllowSorting="True" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" AllowPaging="True" PageSize="10" PagerStyle-HorizontalAlign="Right"
										PagerStyle-VerticalAlign="Middle" PagerStyle-Mode="NumericPages" PagerStyle-Position="Top"
										PagerStyle-BackColor="#a3a9cc" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										<headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											Height="25"></headerstyle>
										<Columns>
										    <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn HeaderText="Asset ID" datafield="fldAssetBarcode" SortExpression="fldAssetBarcode">
												<itemstyle width="17%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr" SortExpression="fld_AssetTypeStr" >
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Category" datafield="fld_CategoryName" SortExpression="fld_CategoryName">
												<itemstyle width="13%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Subcategory" datafield="fld_CatSubName" SortExpression="fld_CatSubName">
												<itemstyle width="13%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>  
											<asp:boundcolumn headertext="Brand" datafield="fld_AssetBrand" SortExpression="fld_AssetBrand">
												<itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn headertext="Department" datafield="fld_DepartmentNamestr" SortExpression="fld_DepartmentNamestr">
												<itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											</asp:boundcolumn>
											<asp:TemplateColumn HeaderText="Asset ID (NFS)" ItemStyle-HorizontalAlign=Center>
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlAssetIdNFS" runat="server" ></asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>																						
										</Columns>
									</asp:datagrid>
								</td>
							</tr>
						</table>
						<!--End Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
				</tbody>
			</table>
			<!-- Start: Hidden Fields -->
			<input id="hdnSortName" type="hidden" name="hdnSortName" runat="server"> 
			<input id="hdnSortAD" type="hidden" name="hdnSortAD" runat="server">
			<!-- End  : Hidden Fields -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
		</form>
</body>
</html>
