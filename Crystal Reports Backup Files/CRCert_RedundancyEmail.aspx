<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CRCert_RedundancyEmail.aspx.vb" Inherits="AMS.CRCert_RedundancyEmail" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<%@ Register TagPrefix="fluent" Namespace="Fluent.MultiLineTextBoxValidator" Assembly="Fluent.MultiLineTextBoxValidator" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet" />
	<script language="JavaScript" type="text/javascript" src="../Common/CommonJS.js"></script>
	<script type="text/javascript">
	    function fnConfirmRec(CallType) {
				var flag = false;
				var foundError = false;
				var confirmAction;
								
				//Get Error Message for related action
				//--> (A)pprove, (R)eject
				switch(CallType)    
				{
					case 'A':
						confirmAction = "Are you sure you want to approve this certificate?"; 
						break
						
					case 'R':
						confirmAction = "Are you sure you want to reject this certificate?"; 
						break
					
				}
								
				
				if (CallType == 'R'){
				    //validate Reasons for Reject
				    if (!foundError && gfnIsFieldBlank(document.Frm_RedCodemn.txtRejReason)) {
				        foundError=true;
				        document.Frm_RedCodemn.txtRejReason.focus();
				        alert("Please enter Reasons for Reject.");
			        }
				}
				
								
				if (!foundError){
 					flag = window.confirm(confirmAction);
				    return flag;
 				}
				else
					return false;
		}
	</script>
</head>
<body>
    <form id="Frm_RedCodemn" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Approving Certificate of Redundancy</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" height="400" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table3">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table4">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table5">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table6">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table7">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table9">
																				<TBODY align=left>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Certificate S/No : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:Label ID=lblCertSNo runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Certificate Status : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:Label ID=lblCertStatus runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Certificate File : </FONT>
																					    </TD>
																					    <TD>
																						    <span id=spanCertLink runat=server></span>
																					    </TD>
																				    </TR>
																				    <tr>
																						<TD ><FONT class="DisplayTitle">Other Supporting Document File :</FONT> </TD>
                                                                                        <TD ><span id=spanOtherSupportingDoc runat=server></span></TD>
																					</tr>
																				    <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Department : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:Label ID=lblDept runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Receiving Party if donated : </FONT>
																					    </TD>
																					    <TD><asp:Label id="lblRevParty" Runat="server"></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD  width="35%">
																					        <FONT class="DisplayTitle">Total Net Book Value [NBV] (S$) : </FONT>
																					    </TD>
																					    <TD width="65%">
																					        <asp:Label ID=lblTotNBV runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Disposal Method : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:Label ID=lblDispMethod runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD valign="top" width="35%"><FONT class="DisplayTitle">Reasons for redundant : </FONT>
																					    </TD>
																					    <TD width="65%">
																					        <asp:Label ID=lblReason runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR><TD colSpan="2">&nbsp;</TD></TR>
																				    <TR>            
																						<TD colSpan="2"><font class="DisplayTitleHeader">Approving Officer Information</font>
																						                <br />(for Total NBV less than or equal to S$500,000)</TD>
																					</TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Approving Officer : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:Label ID=lblAppOff runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Designation : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblAODesg runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Date : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblAODt runat=server></asp:Label></TD>
																				    </TR>
																				    <TR><TD colSpan="2">&nbsp;</TD></TR>
																				    <TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Requestor Information</font></TD>
																					</TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Requestor : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblRequestor runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Designation : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblReqDesg runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Date : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblReqDt runat=server></asp:Label></TD>
																				    </TR>
																				    <TR><TD colSpan="2">&nbsp;</TD></TR>
																				    <TR>
																						<TD colSpan="2">
																						    <font class="DisplayTitleHeader">Ministry of Finance (MOF) Information</font> 
																						    <br />(for Total NBV more than S$500,000)
																						    <asp:Label ID=lblMOFSysApp runat=server><br /><font color="red">Note: </font>System approval by AGC</asp:Label></TD>
																					</TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">MOF : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblMOFName runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Designation : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblMOFDesg runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Email : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblMOFEmail runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Date : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblMOFDt runat=server></asp:Label></TD>
																				    </TR>
																				    
																				    <TR>
																						<TD colSpan="2">
																						    <div id=divCompDisposalView runat=server>
																						    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table2">
																				                <TBODY align=left>
																				                    <TR><TD colSpan="2">&nbsp;</TD></TR>
																				                    <TR>
																						                <TD colSpan="2"><font class="DisplayTitleHeader">Disposal Information</font></TD>
																					                </TR>
																				                    <TR>
																					                    <TD width="35%">
																					                        <FONT class="DisplayTitle">Disposal : </FONT>
																					                    </TD>
																					                    <TD width="65%">
																					                        <asp:Label ID=lblCDispName runat=server></asp:Label>
																					                    </TD>
																				                    </TR>
																				                    <TR>
																					                    <TD>
																					                        <FONT class="DisplayTitle">Designation : </FONT>
																					                    </TD>
																					                    <TD>
																					                         <asp:Label ID=lblCDispDesg runat=server></asp:Label>
																					                    </TD>
																				                    </TR>
																				                    <TR>
																					                    <TD>
																						                    <FONT class="DisplayTitle">Date : </FONT>
																					                    </TD>
																					                    <TD>
																					                        <asp:Label ID=lblCDispDt runat=server></asp:Label>
																					                    </TD>
																				                    </TR>
																				                </TBODY>
																				            </TABLE>
																				            </div>
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																						    <div id=divReceiverView runat=server>
																						    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table11">
																				                <TBODY align=left>
																				                    <TR><TD colSpan="2">&nbsp;</TD></TR>
																				                    <TR>
																						                <TD colSpan="2">
																						                    <font class="DisplayTitleHeader">Receiver Information</font>
																						                    <br /><font color="red">Note: </font>System approval by AGC</TD>
																					                </TR>
																				                    <TR>
																					                    <TD width="35%">
																					                        <FONT class="DisplayTitle">Receiver : </FONT>
																					                    </TD>
																					                    <TD width="65%"><asp:Label ID=lblRevName runat=server></asp:Label></TD>
																				                    </TR>
																				                    <TR>
																					                    <TD>
																					                        <FONT class="DisplayTitle">Designation : </FONT>
																					                    </TD>
																					                    <TD><asp:Label ID=lblRevDesg runat=server></asp:Label></TD>
																				                    </TR>
																				                    <TR>
																					                    <TD>
																						                    <FONT class="DisplayTitle">Date : </FONT>
																					                    </TD>
																					                    <TD><asp:Label ID=lblRevDt runat=server></asp:Label></TD>
																				                    </TR>
																				                    <TR>
																					                    <TD>
																					                        <FONT class="DisplayTitle">Email : </FONT>
																					                    </TD>
																					                    <TD><asp:Label ID=lblRevEmail runat=server></asp:Label></TD>
																				                    </TR>
																				                </TBODY>
																				            </TABLE>
																				            </div>
																					    </TD>
																					</TR>
																				    <TR>
																						<TD colSpan="2">
																						    <div id=divReasonRejectField runat=server>
																						    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table13">
																				                <TBODY align=left>
																				                    <TR><TD colSpan="2">&nbsp;</TD></TR>
																				                    <TR>
																					                    <TD valign="top" width="35%"><FONT class="DisplayTitle">Reasons for Reject : </FONT>
																					                    </TD>
																					                    <TD width="65%">
																					                        <asp:TextBox id="txtRejReason" maxlength="1000" TextMode="MultiLine" Columns="40" Rows="5" Runat="server"></asp:TextBox><br />
																					                        <asp:textbox id="txtRejReasonWord" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					                        <fluent:multilinetextboxvalidator id="MLLReason" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Reason to 1000 Characters." OutputControl="txtRejReasonWord" ControlToValidate="txtRejReason"></fluent:multilinetextboxvalidator>
																					                    </TD>
																				                    </TR>
																				                </TBODY>
																				            </TABLE>
																				            </div>
																				            
																				            <div id=divReasonRejectView runat=server>
																						    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table14">
																				                <TBODY align=left>
																				                    <TR><TD colSpan="2">&nbsp;</TD></TR>
																				                    <TR>
																					                    <TD valign="top" width="35%"><FONT class="DisplayTitle">Reasons for Reject : </FONT></TD>
																					                    <TD width="65%">
																					                        <asp:Label id=lblRejReason runat=server></asp:Label>
																					                    </TD>
																				                    </TR>
																				                </TBODY>
																				            </TABLE>
																				            </div>
																				         </TD>
																				    </TR>
																				    <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:Button id="butApprove" Text="Approve" Runat="Server" />&nbsp;
																							<asp:Button id="butReject" Text="Reject" Runat="Server" />
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnCertID" runat="server">
			<input type="hidden" id="hdnDipMethod" runat="server">
			<input type="hidden" id="hdnCertStatus" runat="server">
			<input type="hidden" id="hdnMore5hkF" runat="server">
			<!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
				src="../common/DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="Z-INDEX:101; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px">
			</iframe>
			<!-- End  : Hidden Fields -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
    </form>
</body>
</html>
