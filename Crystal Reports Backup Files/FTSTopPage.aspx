<%@ Page Language="vb" AutoEventWireup="false" Codebehind="FTSTopPage.aspx.vb" Inherits="AMS.FTSTopPage"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>TopPage</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="FTSstyle.css" type="text/css" rel="Stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="frm_TopPage" method="post" runat="server">
			<table width="100%" height="110" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td valign="top" align="center">
						<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td height="100%" width="100%" valign="top">
									<table width="100%" height="75%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td valign="top" height="1" width="10%" align="left"><div align="left"><img src="../images/AGCLogosmall.JPG"></div>
											</td>
											<td valign="middle" align=left><h1>File and Asset Management System</h1>
												Version 1.0</td>
											<td align="right" valign="bottom">
												Welcome <strong>
													<asp:Label ID="UsrName" Runat="server"></asp:Label></strong>&nbsp;&nbsp;<br>
												<asp:Label ID="lblTodayDt" Runat="server"></asp:Label>&nbsp;&nbsp;</td>
										</tr>
										<tr>
											<td colspan="3" height="20" class="kiv">
												<table width="100%" height="20" border=0 cellpadding=0 cellspacing=0>
													<tr>
														<td align="center">
															<div id="ShowTicker" runat="server">
																<DIV ID="TICKER" class="kiv" STYLE="DISPLAY:none; OVERFLOW:hidden; WIDTH:800px" onmouseover="TICKER_PAUSED=true"
																	onmouseout="TICKER_PAUSED=false">
																	<%DisplayTicker%>
																</DIV>
																<script type="text/javascript" src="webticker_lib.js" language="javascript"></script>
															</div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td colspan="3" valign="top" height="1"><img src="../images/spacer.gif" height="1"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
