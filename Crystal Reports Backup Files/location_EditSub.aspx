<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="location_EditSub.aspx.vb" Inherits="AMS.location_EditSub" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>location_Edit</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script language="javascript" type="text/javascript">
		function chkFrm() {
			var foundError = false;
			var hdnDuplicateStatus = document.frm_locationEdit.hdnDuplicateStatus.value;

			if (hdnDuplicateStatus == "1") {
			    foundError = true;
			    document.frm_locationEdit.txtSLocCode.focus();
			    alert("Duplicate Sub Location Code in the Asset Management System! Please Try Again!");
			}

			//Validate Location Code
			if (!foundError && gfnIsFieldBlank(document.frm_locationEdit.txtSLocCode)) {
				foundError=true;
				document.frm_locationEdit.txtSLocCode.focus();
				alert("Please enter Sub Location Code.");
			}
			if (!foundError && document.frm_locationEdit.txtSLocCode.value.length > 10) {
				foundError=true
				document.frm_locationEdit.txtSLocCode.focus()
				alert("Please make sure the Sub Location Code is less than 10 characters long.")
			}
			
			//Validate Location Name
			if (!foundError && gfnIsFieldBlank(document.frm_locationEdit.txtSLocName)) {
				foundError=true
				document.frm_locationEdit.txtSLocName.focus()
				alert("Please enter Sub Location Name.")
			}
			if (!foundError && document.frm_locationEdit.txtSLocName.value.length > 250) {
				foundError=true
				document.frm_locationEdit.txtSLocName.focus()
				alert("Please make sure the Sub Location Name is less than 250 characters long.")
			}
			
			//if Activate is "Yes", Light IP Add is mandatory field
			if (!foundError && document.frm_locationEdit.rdActivate_0.checked) {
					//Validate Light IP Add
			        if (!foundError && gfnIsFieldBlank(document.frm_locationEdit.txtLightIP)) {
				        foundError=true
				        document.frm_locationEdit.txtLightIP.focus()
				        alert("Please enter Light IP Address.")
			        }
			        if (!foundError && document.frm_locationEdit.txtLightIP.value.length > 20) {
				        foundError=true
				        document.frm_locationEdit.txtLightIP.focus()
				        alert("Please make sure the Light IP Address is less than 20 characters long.")
			        }
			        
			        //validate light type
			        if (!foundError && document.frm_locationEdit.ddlLightType.value == "0") {
				        foundError=true
				        document.frm_locationEdit.ddlLightType.focus()
				        alert("Please select the Light Type.")
			        }		        
			}
			
			//if Door Gantry is "Yes", Movement Status is mandatory field
			if (!foundError && document.frm_locationEdit.rdDoorGantryF_0.checked) {
					//Validate Movement Status
			        if (!foundError && gfnIsFieldBlank(document.frm_locationEdit.txtDGMvStatus)) {
				        foundError=true
				        document.frm_locationEdit.txtDGMvStatus.focus()
				        alert("Please enter Movement Status.")
			        }
			        if (!foundError && document.frm_locationEdit.txtDGMvStatus.value.length > 15) {
				        foundError=true
				        document.frm_locationEdit.txtDGMvStatus.focus()
				        alert("Please make sure the Movement Status is less than 15 characters long.")
			        }        
			}
			
 			if (!foundError){
 				var flag = false;
 				flag = window.confirm("Are you sure want to update this record?");
 				return flag;
 			}
			else
				return false
		}
	</script>
</head>
<body>
    <form id="frm_locationEdit" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Administration : Edit Sub Location</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table id="Table2" height="400" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<!--Start: Main Content-->
						<table id="Table3" height="400" cellSpacing="1" cellPadding="0" width="100%" align="left"
							bgColor="silver" border="0">
							<tr>
								<td>
									<table id="Table4" height="400" cellSpacing="0" cellPadding="0" width="100%" align="left"
										bgColor="white" border="0">
										<tr>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
													<TR>
														<TD vAlign="top" align="center" width="100%">
														    <asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE id="Table6" height="126" cellSpacing="0" cellPadding="3" width="100%" align="left"
																bgColor="white" border="0">
																<TBODY align=left>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" height="352">
																			<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
																				<TBODY>
																				    <TR>
																						<TD vAlign="middle" width="35%" align=left>
																						    <FONT color="#666666">Location Code : </FONT>
																						</TD>
																						<TD width="65%">
																						    <asp:label ID=lblLocCode runat=server></asp:label></TD>
																					</TR>
																					<TR>
																						<TD vAlign=top width="35%" align="left">
																							<FONT class="DisplayTitle">Location Name : </FONT>
																						</TD>
																						<TD width="65%"><asp:label ID=lblLocName runat=server></asp:label></TD>
																					</TR>
																				    <TR>
																						<TD vAlign="middle" width="35%" align=left>
																						    <FONT color="#666666"><font color="red">*</font>Sub Location Code : </FONT>
																						</TD>
																						<TD width="65%">
																						    <asp:textbox id="txtSLocCode" Runat="server" MaxLength=8 AutoPostBack="True"></asp:textbox>&nbsp;
																							<FONT class="DisplayFieldSize">(Maximum 8 characters)</FONT></TD>
																					</TR>
																					<TR>
																						<TD vAlign=top width="35%" align="left">
																							<FONT class="DisplayTitle"><font color="red">*</font>Sub Location Name : </FONT>
																						</TD>
																						<TD width="65%"><asp:textbox id="txtSLocName" Runat="server" maxlength="250" Width="300px"></asp:textbox></TD>
																					</TR>
																					<TR style="visibility:hidden">
															                            <TD><FONT class="DisplayTitle">Smart Shelf Rack : </FONT></TD>
															                            <TD>
															                                <asp:radiobuttonlist id="rdActivate" Runat="server" RepeatDirection="Horizontal" AutoPostBack=true>
																	                            <asp:ListItem Value="Y">Yes</asp:ListItem>
																	                            <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
																                            </asp:radiobuttonlist>
															                            </TD>
														                            </TR>
														                            <TR style="visibility:hidden">
																						<TD colSpan="2">
																						    <div id=divLightIP runat=server>
																						        <TABLE id="Table9" cellSpacing="1" cellPadding="1" width="100%" border="0">
																						        <TR>
																						            <TD width="35%" align=left>
																							            <FONT class="DisplayTitle"><font color="red">*</font>Light IP Address : </FONT>
																						            </TD>
																						            <TD width="65%" align=left><asp:textbox id="txtLightIP" Runat="server" maxlength="20" Width="300px"></asp:textbox></TD>
																					            </TR>
																					            <TR>
																						            <TD width="35%" align=left>
																							            <FONT class="DisplayTitle"><font color="red">*</font>Light Type : </FONT>
																						            </TD>
																						            <TD width="65%" align=left><asp:DropDownList ID="ddlLightType" Runat="server"></asp:DropDownList></TD>
																					            </TR>
																					            </TABLE>
																					        </div>
																						</TD>
																					</TR>
																					<TR style="visibility:hidden">
															                            <TD><FONT class="DisplayTitle">Door Gantry : </FONT></TD>
															                            <TD>
															                                <asp:radiobuttonlist id="rdDoorGantryF" Runat="server" RepeatDirection="Horizontal" AutoPostBack=true>
																	                            <asp:ListItem Value="Y">Yes</asp:ListItem>
																	                            <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
																                            </asp:radiobuttonlist>
															                            </TD>
														                            </TR>
														                            <TR>
																						<TD colSpan="2">
																						    <div id=divGoorGantry runat=server>
																						        <TABLE id="Table10" cellSpacing="1" cellPadding="1" width="100%" border="0">
																						        <TR>
																						            <TD width="35%" align=left>
																							            <FONT class="DisplayTitle"><font color="red">*</font>Movement Status : </FONT>
																						            </TD>
																						            <TD width="65%" align=left><asp:textbox id="txtDGMvStatus" Runat="server" maxlength="15" Width="300px"></asp:textbox></TD>
																					            </TR>
																					            </TABLE>
																					        </div>
																						</TD>
																					</TR>
																					<TR>
																						<TD align="center" colSpan="2" style="height: 36px"><BR>
																							<asp:button id="butSubmit" Runat="Server" Text="Submit"></asp:button>
																							<asp:button id="butReset" Runat="Server" Text="Reset"></asp:button>
																							<asp:button id="ButCancel" Runat="Server" Text="Cancel"></asp:button>
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start : Hidden Fields -->
			<input type="hidden" id="hdnLocID" runat="server" />
			<input type="hidden" id="hdnSLocID" runat="server" />
			<input type="hidden" id="hdnCallFrm"  runat="server" />
            <input id="hdnDuplicateStatus" type="hidden" name="hdnDuplicateStatus" runat="server">
			<!-- End   : Hidden Fields -->
			<tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer>
		</form>
</body>
</html>
