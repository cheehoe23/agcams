<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UploadExcel_result.aspx.vb" Inherits="AMS.UploadExcel_result" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
</head>
<body>
    <b> Issuance Management - Asset Updated Results </b><br />
        Printed Date : <asp:Label ID=lblPrintDate runat=server></asp:Label> <br /><br />
		<!--Datagrid for display record.-->
        <asp:datagrid id="dgAsset" Runat="server" AlternatingItemStyle-Height="25"
			ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
			BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" 
			datakeyfield="SNum" AlternatingItemStyle-BackColor="#e3d9ee"  >
			<headerstyle verticalalign="Middle" BackColor="#a3a9cc" Font-Bold="True" HorizontalAlign="Center"
				Height="25"></headerstyle>
				<Columns>
                    <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                    </asp:boundcolumn>
                    <asp:boundcolumn datafield="fld_AssetID" headertext="Asset ID" ItemStyle-Height="10">
                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                    </asp:boundcolumn>
                    <asp:boundcolumn datafield="fld_Desc" headertext="Description" ItemStyle-Height="10">
                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                    </asp:boundcolumn>
                    <asp:boundcolumn datafield="fld_Dept" headertext="Department" ItemStyle-Height="10">
                        <itemstyle width="8%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                    </asp:boundcolumn>
                    <asp:boundcolumn datafield="fld_Loc" headertext="Location" ItemStyle-Height="10">
                        <itemstyle width="8%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                    </asp:boundcolumn>
                    <asp:boundcolumn datafield="fld_LocSub" headertext="Sub Location" ItemStyle-Height="10">
                        <itemstyle width="8%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                    </asp:boundcolumn>
                    <asp:boundcolumn datafield="fld_Owner" headertext="Assigned Owner" ItemStyle-Height="10">
                        <itemstyle width="12%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                    </asp:boundcolumn>
                    <asp:boundcolumn datafield="fld_SubCat" headertext="Sub Category Name" ItemStyle-Height="10">
                        <itemstyle width="12%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                    </asp:boundcolumn>
                    <asp:boundcolumn datafield="fld_Remarks" headertext="Remarks" ItemStyle-Height="10">
                        <itemstyle width="12%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                    </asp:boundcolumn>
                    <asp:boundcolumn datafield="fld_UpdateF" headertext="Updated Status" ItemStyle-Height="10">
                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                    </asp:boundcolumn>
                    <asp:boundcolumn datafield="fld_ReasonFail" headertext="Reason for Updated Failed" ItemStyle-Height="10">
                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                    </asp:boundcolumn>
                </Columns>
		</asp:datagrid>
</body>
</html>
