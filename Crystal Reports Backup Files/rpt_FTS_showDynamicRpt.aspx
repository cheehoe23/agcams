﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="rpt_FTS_showDynamicRpt.aspx.vb" Inherits="AMS.rpt_FTS_showDynamicRpt" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>rpt_showDynamicRpt</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<CR:CRYSTALREPORTVIEWER id="CRVdynamicRpt" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 16px"
				runat="server" Height="50px" Width="350px" AutoDataBind="true" HasSearchButton="False"
				></CR:CRYSTALREPORTVIEWER>
			<!-- Start : Hidden Fields -->
			<input type="hidden" id="hdnRunNo" runat="server"> <input type="hidden" id="hdnRptT" runat="server">
			<input type="hidden" id="hdnDspyField" runat="server"> 
			<!-- End   : Hidden Fields --></form>
	</body>
</HTML>
