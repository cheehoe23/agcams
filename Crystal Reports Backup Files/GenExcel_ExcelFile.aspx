<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GenExcel_ExcelFile.aspx.vb" Inherits="AMS.GenExcel_ExcelFile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
</head>
<body>
    	<!--Datagrid for display record.-->
        <asp:datagrid id="dgAsset" Runat="server" AlternatingItemStyle-Height="25"
			ItemStyle-Height="25" AutoGenerateColumns="False" BorderStyle="None"
			BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" Width="100%"
			datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor=white  >
			<headerstyle verticalalign="Middle" BackColor=white Font-Bold="True" HorizontalAlign="Center"
				Height="25"></headerstyle>
				<Columns>
				    <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
					    <itemstyle Width="10%"  cssclass="GridText" verticalalign="Middle"></itemstyle>
				    </asp:boundcolumn>
				    <asp:boundcolumn datafield="fld_DescBlank" headertext="Description" ItemStyle-Height="10">
					    <itemstyle Width="25%" cssclass="GridText" verticalalign="Middle"></itemstyle>
				    </asp:boundcolumn>
				    <asp:boundcolumn datafield="fld_DeptBlank" headertext="Department" ItemStyle-Height="10">
					    <itemstyle Width="20%" cssclass="GridText" verticalalign="Middle"></itemstyle>
				    </asp:boundcolumn>
				    <asp:boundcolumn datafield="fld_LocBlank" headertext="Location" ItemStyle-Height="10">
					    <itemstyle Width="20%" cssclass="GridText" verticalalign="Middle"></itemstyle>
				    </asp:boundcolumn>
				    <asp:boundcolumn datafield="fld_LocBlank" headertext="Sub Location" ItemStyle-Height="10">
					    <itemstyle Width="20%" cssclass="GridText" verticalalign="Middle"></itemstyle>
				    </asp:boundcolumn>
				    <asp:boundcolumn datafield="fld_OwnerBlank" headertext="Assigned Owner" ItemStyle-Height="10">
					    <itemstyle Width="25%" cssclass="GridText" verticalalign="Middle"></itemstyle>
				    </asp:boundcolumn>
				    <asp:boundcolumn datafield="fld_DescBlank" headertext="Sub Category Name" ItemStyle-Height="10">
					    <itemstyle Width="25%" cssclass="GridText" verticalalign="Middle"></itemstyle>
				    </asp:boundcolumn>
				    <asp:boundcolumn datafield="fld_DescBlank" headertext="Remarks" ItemStyle-Height="10">
					    <itemstyle Width="25%" cssclass="GridText" verticalalign="Middle"></itemstyle>
				    </asp:boundcolumn>
				</Columns>
		</asp:datagrid>
</body>
</html>
