<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MovementUpdate_Detail.aspx.vb" Inherits="AMS.MovementUpdate_Detail" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script type="text/javascript">
	function chkFrm() {
		var flag;
		flag = false;
		
		flag = window.confirm("Are you sure you want to upload file?");
		
		return flag;						
	}
	</script>
</head>
<body>
     <form id="Frm_StockTake" method="post" runat="server">
			&nbsp;<br />
            <br />
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Offline Processing : Stocktake History <asp:Label ID=lblHeader runat=server></asp:Label></B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																				    <TR>
																					    <TD valign=top width="35%">
																					        <FONT class="DisplayTitle">Stocktake For : </FONT>
																					    </TD>
																					    <TD width="65%">
																						    <asp:Label ID=lblSTFor runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD valign=top width="35%">
																					        <FONT class="DisplayTitle">Stocktake ID : </FONT>
																					    </TD>
																					    <TD width="65%">
																						    <asp:Label ID=lblSTID runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Stocktake Date : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:Label ID=lblSTDate runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																	                    <TD>
																	                        <FONT class="DisplayTitle">Stocktake By : </FONT>
																	                    </TD>
																	                    <TD>
																		                    <asp:Label ID=lblSTBy runat=server></asp:Label>
																	                    </TD>
																                    </TR>
																                    <TR>
																	                    <TD>
																	                        <FONT class="DisplayTitle">Total Asset(s) : </FONT>
																	                    </TD>
																	                    <TD>
																		                    <asp:Label ID=lblTotRec runat=server></asp:Label>
																	                    </TD>
																                    </TR>
																                    <TR>
																	                    <TD>
																	                        <FONT class="DisplayTitle">Total Scan : </FONT>
																	                    </TD>
																	                    <TD>
																		                    <asp:Label ID=lblTotScan runat=server></asp:Label>
																	                    </TD>
																                    </TR>
																                    <TR>
																	                    <TD>
																	                        <FONT class="DisplayTitle">Total Unscanned : </FONT>
																	                    </TD>
																	                    <TD>
																		                    <asp:Label ID=lblTotMiss runat=server></asp:Label>
																	                    </TD>
																                    </TR>
																                    <tr id="trTotalFound" runat="server" visible="false">
																	                    <TD>
																	                        <FONT class="DisplayTitle">Total Found : </FONT>
																	                    </TD>
																	                    <TD>
																		                    <asp:Label ID=lblTotFound runat=server></asp:Label>
																	                    </TD>
																                    </tr>
                                                                                    <b><asp:HyperLink ID=hylFileName runat=server Target="StockTake" Visible = "false"></asp:HyperLink></b>
                                                                                    <input id="FUploadFile" runat="server" type="file" visible = false />&nbsp;<asp:Button id="butMvUpdate" Text="Update Movement" Runat="Server" Visible = "false"/>
																                    <%--<TR style="visibility:hidden">
																	                    <TD valign=top>
																	                        <FONT class="DisplayTitle"><b>File Name</b> : </FONT>
																	                    </TD>
																	                    <TD>
																		                    <b><asp:HyperLink ID=hylFileName runat=server Target="StockTake"></asp:HyperLink></b><br />
																		                    <FONT color="#666666" size="1">(Right click on the link and select "Save target as..." to save the file)</FONT>
																	                    </TD>
																                    </TR>
																					<TR style="visibility:hidden">
																						<TD align="center" colspan="2" style=""><BR>
																							<input id="FUploadFile" runat="server" type="file" />&nbsp;<asp:Button id="butMvUpdate" Text="Update Movement" Runat="Server" />
																						</TD>
																					</TR>--%>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Stocktake Result</b></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Asset Scan</font></TD>
																					</TR>
																					<tr>
								                                                        <td vAlign="top" align="center" colspan=2>
									                                                        <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgAssetScan" Runat="server" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
                                                                                                    <asp:boundcolumn datafield="fld_AssetDesc" headertext="Description" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr" >
												                                                        <itemstyle width="7%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Department" datafield="fld_DepartmentName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Location" datafield="fld_LocationName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Sub Location" datafield="fld_LocSubName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Assigned Owner" datafield="fld_OwnerName" Visible = "false">
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Remarks" datafield="fld_AssetRemark" Visible=false >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Location" datafield="fld_NewLoc" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Sub Location" datafield="fld_newLocSub" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Assigned Owner" datafield="fld_NewOwner" Visible = "false" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Remarks" datafield="fld_NewRemark" Visible="false">
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>					
										                                                        </Columns>
									                                                        </asp:datagrid>
								                                                        </td>
							                                                        </tr>
							                                                        <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Asset Unscanned</font></TD>
																					</TR>
																					<tr>
								                                                        <td vAlign="top" align="center" colspan=2>
									                                                        <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgAssetMiss" Runat="server" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
                                                                                                    <asp:boundcolumn datafield="fld_AssetDesc" headertext="Description" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                         <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Department" datafield="fld_DepartmentName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Location" datafield="fld_LocationName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Sub Location" datafield="fld_LocSubName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Assigned Owner" datafield="fld_OwnerName" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Original) Remarks" datafield="fld_AssetRemark" Visible="false" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>					
										                                                        </Columns>
									                                                        </asp:datagrid>
								                                                        </td>
							                                                        </tr>
							                                                        <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR runat="server" visible="false">
																						<TD colSpan="2"><font class="DisplayTitleHeader">Asset Found</font></TD>
																					</TR>
																					<tr runat="server" visible="false">
								                                                        <td vAlign="top" align="center" colspan=2>
									                                                        <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgAssetFound" Runat="server" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
                                                                                                    <asp:boundcolumn datafield="fld_AssetDesc" headertext="Description" ItemStyle-Height="10">
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                         <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Department" datafield="fld_NewDept" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Location" datafield="fld_NewLoc" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="(Stocktake Result) Sub Location" datafield="fld_newLocSub" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Assigned Owner" datafield="fld_NewOwner" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Remarks" datafield="fld_NewRemark" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>					
										                                                        </Columns>
									                                                        </asp:datagrid>
								                                                        </td>
							                                                        </tr>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start : Hidden Fields -->
			<asp:TextBox Visible="False" ID="hdnSTID" Runat="server"></asp:TextBox>
			<asp:TextBox Visible="False" ID="hdnSTPID" Runat="server"></asp:TextBox>
			<asp:TextBox Visible="False" ID="hdnOverDueF" Runat="server"></asp:TextBox>
			<!-- End   : Hidden Fields -->
			<tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer>
	</form>
</body>
</html>
