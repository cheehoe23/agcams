<%@ Page Language="vb" AutoEventWireup="false" Codebehind="usrGrp_View.aspx.vb" Inherits="AMS.usrGrp_View" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>usrGrp_View</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
		<script language="javascript">
		/*Using modified select_deselectAll script function of my original one, from Developerfusion.com forum members - ketcapli & thombo Forum Post - [http://www.developerfusion.co.uk/forums/topic-22773]*/
		function select_deselectAll (chkVal, idVal) 
		{
			var frm = document.forms[0];
			if (idVal.indexOf('DeleteThis') != -1 && chkVal == true)
			{
					var AllAreSelected = true;
					for (i=0; i<frm.length; i++) 
					{
						if (frm.elements[i].id.indexOf('DeleteThis') != -1 && frm.elements[i].checked == false)
						{ 
							AllAreSelected = false;
							break;
						} 
					} 
					if(AllAreSelected == true)
					{
						for (j=0; j<frm.length; j++) 
						{
							if (frm.elements[j].id.indexOf ('CheckAll') != -1) 
							{	
								frm.elements[j].checked = true;
								break;
							}
						}
					}
			} 
			else 
			{
				for (i=0; i<frm.length; i++) 
					{
						if (idVal.indexOf ('CheckAll') != -1) 
						{
							if(chkVal == true) 
							{
								frm.elements[i].checked = true; 
							} 
							else 
							{
								frm.elements[i].checked = false; 
							}
						} 
						else if (idVal.indexOf('DeleteThis') != -1 && frm.elements[i].checked == false) 
						{
							for (j=0; j<frm.length; j++) 
							{
								if (frm.elements[j].id.indexOf ('CheckAll') != -1) 
								{ 
									frm.elements[j].checked = false;
									break; 
								} 
							} 
						} 
					} 
			} 
		} 
		
		function fnCheckDelSelect()
		{
			var lstrStatus;
			var frm = document.forms[0];			
			lstrStatus='N';
			for (i=0; i<frm.length; i++) 
			{			
				if (frm.elements[i].id.indexOf('DeleteThis') != -1 && frm.elements[i].checked == true) 
				{					
						lstrStatus='Y';
				}						
			}	
			if (lstrStatus=='N')
				{
					alert("At least one record should be selected for deletion");
					return false;
				}			
			else if(ConfirmDelete() == true)
				{
					return true;
				}
			else
				{
					return false;
				}
		}
		function ConfirmDelete() 
		{
			var flag;
			flag = false;
			flag = window.confirm("You have chosen to delete one or more User Group.\nYou cannot undo this action. Continue?");
			//alert ('flag = ' + flag);
			return flag;
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Frm_usrGrpView" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>User Management : User Group</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<table id="Table2" height="400" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr>
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td align="center"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td vAlign="top" width="100%">
						<!--Start Main Content-->
						<asp:button id="butAddUsrGrp" Runat="server" Text="Add New User Group"></asp:button>
						<asp:button id="butDelUsrGrp" Runat="server" Text="Delete User Group"></asp:button>
						<table height="400" cellSpacing="1" cellPadding="0" width="100%" align="left" border="0">
							<tr>
								<td vAlign="top" align="right" bgColor="#a3a9cc">
									<table cellSpacing="1" cellPadding="1" width="100%" border="0">
										<tr>
											<td width="20%"><asp:dropdownlist id="ddlPageSize" Runat="server" AutoPostBack="True"></asp:dropdownlist>&nbsp;&nbsp;<font color="white">per 
													page</font>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td vAlign="top" align="center" height="400">
									<!--Datagrid for display record.-->
									<asp:datagrid id="dgUsrGrp" Runat="server" AlternatingItemStyle-BackColor="#e3d9ee" datakeyfield="fld_GroupID"
										PagerStyle-BackColor="#a3a9cc" PagerStyle-Position="Top" PagerStyle-Mode="NumericPages" PagerStyle-VerticalAlign="Middle"
										PagerStyle-HorizontalAlign="Right" PageSize="10" AllowPaging="True" PagerStyle-CssClass="DGpageStyle"
										BorderColor="#ffffff" BorderStyle="None" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
										ItemStyle-Height="25" AlternatingItemStyle-Height="25" PagerStyle-Height="25">
										<headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											Height="25"></headerstyle>
										<Columns>
											<asp:boundcolumn visible="false" datafield="fld_GroupID" headertext="Group ID" ItemStyle-Height="10">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:boundcolumn visible="false" datafield="UsrGrpExist" headertext="UsrGrpExist" ItemStyle-Height="10">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:templatecolumn HeaderText="Group Name" ItemStyle-Width="85%" SortExpression="fld_groupName">
												<itemtemplate>
													<%# Container.DataItem("fld_groupName") %>
												</itemtemplate>
											</asp:templatecolumn>
											<asp:boundcolumn visible="false" datafield="fld_groupName" headertext="Group Name" ItemStyle-Height="10">
												<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											</asp:boundcolumn>
											<asp:templatecolumn HeaderText="Select" ItemStyle-Width="5%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
												<itemtemplate>
													<%# fnShowEditHyperlink(DataBinder.Eval(Container.DataItem, "fld_GroupID")) %>
												</itemtemplate>
											</asp:templatecolumn>
											<asp:templatecolumn ItemStyle-Height="10">
												<headertemplate>
													<img src="../images/delete.gif" alt="Delete" runat="server" id="imgDelete">
													<asp:checkbox id="CheckAll" onclick="javascript: return select_deselectAll (this.checked, this.id);"
														runat="server" />
												</headertemplate>
												<itemtemplate>
													<center>
														<asp:checkbox id="DeleteThis" onclick="javascript: return select_deselectAll (this.checked, this.id);"
															runat="server" />
													</center>
												</itemtemplate>
											</asp:templatecolumn>
										</Columns>
									</asp:datagrid></td>
							</tr>
						</table>
						<!--End Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- Start: Hidden Fields --><input id="hdnSortName" type="hidden" runat="server">
			<input id="hdnSortAD" type="hidden" runat="server"> 
			<!-- End  : Hidden Fields -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
		</form>
	</body>
</HTML>
