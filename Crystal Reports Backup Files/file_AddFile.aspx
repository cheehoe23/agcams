<%@ Page Language="vb" AutoEventWireup="false" Codebehind="file_AddFile.aspx.vb" Inherits="AMS.file_AddFile"%>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="~/commonFTS/FTSfooter_cr.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>file_AddMaster</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../commonFTS/FTSstyle.css" type="text/css" rel="Stylesheet">
		<script language="JavaScript" src="../Common/CommonJS.js"></script>
		<script language="javascript">
		function chkFrm() {
			var foundError = false;
			var flag = false;
			var hdnDuplicateStatus = document.frm_file_AddFile.hdnDuplicateStatus.value;
			
			//Validate ELMS File Ref No
			if (!foundError && gfnIsFieldBlank(document.frm_file_AddFile.txtFileRef)) {
				foundError=true
				document.frm_file_AddFile.txtFileRef.focus()
				alert("Please enter ELMS File Ref No.");
			}

            if (!foundError && gfnIsFieldBlank(document.frm_file_AddFile.txtfileDetailRef)) {
                foundError = true
                document.frm_file_AddFile.txtfileDetailRef.focus()
                alert("Please enter External File Reference No.");
            }

            if (hdnDuplicateStatus == "1") {          
                foundError = true;
                document.frm_file_AddFile.txtfileDetailRef.focus();
                alert("You have enter duplicate ELMS File Reference No and External File Reference No in the File Management System. Please check it!");
            }

            if (!foundError) {
                var FTitle = document.frm_file_AddFile.txtFileTitle.value;
                var FDetailRef = document.frm_file_AddFile.txtfileDetailRef.value;
                var FRef = document.frm_file_AddFile.txtFileRef.value;
                var FRemarks = document.frm_file_AddFile.txtRemarks.value;

                flag = window.confirm("You are creating a new File with the following details:-\n\nELMS File Ref No: " + FRef + "\n\nExternal File Ref No: " + FDetailRef + "\n\nFile Title: " + FTitle + "\n\nRemarks: " + FRemarks + "\n\nAre you sure you want to add this record(s)?");
                return flag;
            }

			else
				return false
		}
		</script>
	    <style type="text/css">

TEXTAREA
	{BACKGROUND: #ffffff; BORDER-BOTTOM: #eeeeee 0.75pt ridge; BORDER-LEFT: #eeeeee 0.75pt ridge; BORDER-RIGHT: #eeeeee 0.75pt ridge; BORDER-TOP: #eeeeee 0.75pt ridge; COLOR: #000000; FONT-FAMILY: Courier New, Courier, mono; FONT-SIZE: 9pt;}
        </style>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="frm_file_AddFile" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>File Management : Create New File</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table id="Table2" height="500" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<!--Start: Main Content-->
						<table id="Table3" height="400" cellSpacing="1" cellPadding="0" width="100%" align="left"
							bgColor="silver" border="0">
							<tr>
								<td>
									<table id="Table4" height="400" cellSpacing="0" cellPadding="0" width="100%" align="left"
										bgColor="white" border="0">
										<tr>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
													<TR>
														<TD vAlign="top" align="center" width="100%"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE id="Table6" height="126" cellSpacing="0" cellPadding="3" width="100%" align="left"
																bgColor="white" border="0">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</FONT></B></DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" height="352">
																			<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
																				<TBODY>
																					<TR>
																						<TD vAlign="middle" align="right" width="30%"><font color="red">*</font><FONT color="#666666"><FONT class="DisplayTitle">ELMS File Reference No :</FONT></FONT> 
																						</TD>
																						<TD width="70%">
																							<asp:TextBox ID="txtFileRef" Width="600px" MaxLength="50" Runat="server" 
                                                                                                AutoPostBack="True"></asp:TextBox>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" align="right"><FONT color="#666666"><font color="red">*</font>External File 
																								Reference No : </FONT>
																						</TD>
																						<TD><asp:TextBox ID="txtfileDetailRef" Width="600px" MaxLength="50" Runat="server" 
                                                                                                AutoPostBack="True"></asp:TextBox></TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" align="right"><FONT color="#666666">Exteranl File 
																								Title : </FONT>
																						</TD>
																						<TD><asp:TextBox ID="txtFileTitle" Width="600px" MaxLength="250" Runat="server"></asp:TextBox></TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" align="right"><FONT color="#666666">Remarks :</FONT></TD>
																						<TD><asp:TextBox ID="txtRemarks" Width="600px" MaxLength="2000" Runat="server" 
                                                                                                Height="150px" TextMode="MultiLine"></asp:TextBox></TD>
																					</TR>
																					<TR>
																						<TD align="center" colSpan="2">
                                                                                            
                                                                                            <asp:Button ID="butSubmit" runat="server" Text="Save" />
																							<asp:button id="butReset" Runat="Server" Text="Reset"></asp:button>
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer>
			<!-- Start: Hidden Fields -->
			<input id="hdnFNo" type="hidden" name="hdnFNo" runat="server"> <input id="hdnFTitle" type="hidden" name="hdnFTitle" runat="server">
            <input id="hdnRemarks" type="hidden" name="hdnRemarks" runat="server">
			<input id="hdnDuplicateStatus" type="hidden" name="hdnDuplicateStatus" runat="server">
            <input id="hdnFRef" type="hidden" name="hdnFRef" runat="server"> <input id="hdnFRefMsg" type="hidden" name="hdnFRefMsg" runat="server">
            <input id="hdnFDetailRef" type="hidden" name="hdnFDetailRef" runat="server">
			<input id="hdnFBarcode" type="hidden" name="hdnFBarcode" runat="server"> <input id="hdnAddSuccessF" type="hidden" name="hdnAddSuccessF" runat="server">
            <input type="hidden" id="hdnFileDetailRefs" runat="server"> 
            <asp:Button ID="hdnclickprint" runat="server" Visible ="false" ></asp:button>
			<!-- End  : Hidden Fields -->
		</form>
		<script language="javascript">
		    function ClickPrint() {		        
		        __doPostBack('hdnclickprint', 'OnClick');		        
		    }

			if (document.frm_file_AddFile.hdnAddSuccessF.value == 'Y')
			{	
				document.frm_file_AddFile.hdnAddSuccessF.value = 'N';
				
				var flag = false;
				var FTitle = document.frm_file_AddFile.hdnFTitle.value;
				var FNo = document.frm_file_AddFile.hdnFNo.value;
				var FBarcode = document.frm_file_AddFile.hdnFBarcode.value;				
				var FRef= document.frm_file_AddFile.hdnFRef.value;
				var FRefMsg= document.frm_file_AddFile.hdnFRefMsg.value;
				var FDetailRef = document.frm_file_AddFile.hdnFDetailRef.value;
				var FRemarks = document.frm_file_AddFile.hdnRemarks.value;
				var status = 'File';

				flag = window.confirm("The new File with the following details has been added successfully:-\n\nELMS File Ref No: " + FRefMsg + "\n\nExternal File Ref No:" + FDetailRef + "\n\nFile Title: " + FTitle + "\n\nRemarks: " + FRemarks + "\n\nDo you want to print the File label now?");
				if (flag) {
				    ClickPrint()
				    //gfnprint(FBarcode, status);
				}	
			}
		</script>
	</body>
</HTML>
