<%@ Page Language="vb" AutoEventWireup="false" Codebehind="generalSetting.aspx.vb" Inherits="AMS.generalSetting" ValidateRequest="false" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<%@ Register TagPrefix="fluent" Namespace="Fluent.MultiLineTextBoxValidator" Assembly="Fluent.MultiLineTextBoxValidator" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>generalSetting</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
		<script language="JavaScript" src="../Common/CommonJS.js"></script>		
        <%--<LINK href="../common/ComboBox/styles/outlook/combo.css" type="text/css" rel="Stylesheet">--%><%--<link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />  --%>  
        <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
        <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>
        <%--<link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />--%>
        <link href="../jquery-ui-1.11.4.custom/external/jquery-ui.css" rel="stylesheet" type="text/css" />

		<script language="javascript">
		    $(document).ready(function () {
		        SearchText("ComboInventoryCtrEmail");
		        SearchText("ComboAssetNFSSendTo3");
		        SearchText("ComboAssetAMSSend3");
		        SearchText("ComboWarrantyExpSend3");
		        SearchText("ComboCondemnExpSend3");
		        SearchText("ComboLeaseInvExpSend3");
		        SearchText("ComboContractExpSend3");
		        SearchText("ComboContractExpSend5");
		        SearchText("ComboContractExp2Send3");
		        SearchText("ComboContractExp2Send5");
		        SearchText("ComboAssetTransferSend3");
		        SearchText("ComboFloatSend2");
		    });
		    function SearchText(obj) {
		        $("#" + obj).autocomplete({
		            source: function (request, response) {		                
		                $.ajax({
		                    url: '<%=ResolveUrl("~/sysSetting/generalSetting.aspx/GetUsers") %>',
		                    data: "{ 'prefix': '" + request.term + "'}",
		                    dataType: "json",
		                    type: "POST",
		                    contentType: "application/json; charset=utf-8",
		                    success: function (data) {
		                        response($.map(data.d, function (item) {
		                            return {
		                                label: item.split('~')[0]

		                            }
		                        }))
		                    },
		                    error: function (response) {
		                        alert(response.responseText);
		                    },
		                    failure: function (response) {
		                        alert(response.responseText);
		                    }
		                });
		            },
		            focus: function () {
		                // prevent value inserted on focus
		                return false;
		            },
		            select: function (event, ui) {
		                var terms = split(this.value);
		                // remove the current input
		                terms.pop();
		                // add the selected item
		                terms.push(ui.item.value);
		                // add placeholder to get the comma-and-space at the end
		                terms.push("");
		                this.value = terms.join("; ");
		                return false;
		            }
		        });
		        $("#" + obj).bind("keydown", function (event) {
		            if (event.keyCode === $.ui.keyCode.TAB &&
$(this).data("autocomplete").menu.active) {
		                event.preventDefault();
		            }
		        })
		        function split(val) {
		            return val.split(/;\s*/);
		        }
		        function extractLast(term) {
		            return split(term).pop();
		        }
		    }
		    

		    function fntest() {
		        //the purpose of this function is to allow the enter key to 
		        //point to the correct button to click.

		        var key;

		        if (window.event) {
		            key = window.event.keyCode;     //IE		        
		        }
		        else {
		            key = e.which;      //firefox		          
		        }
		        if (key == 13) {

		            document.frm_genSet.txttest.value = document.frm_genSet.txttest.value + ",";		           
		            document.frm_genSet.txttest.focus();
		            event.keyCode = 0;		            
		        }
		    }

		    function chkFrm() {
				var foundError = false;
				
				//validate Inventory Controller Email
				//--> Checking Inventory Controller Email cannot be blank
				if (!foundError && gfnIsFieldBlankValue(document.getElementById("ComboInventoryCtrEmail").object.GetTextValue())) {
					foundError=true;
					//document.frm_genSet.ComboInventoryCtrEmail.focus();
					alert("Please enter Inventory Controller Email.");
				}
					
				//validate New Addition of Asset from NFS
				//--> Checking New Addition for NFS subject cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtAssetNFSSubject)) {
					foundError=true;
					document.frm_genSet.txtAssetNFSSubject.focus();
					alert("Please enter Email Subject (New Addition of Asset from NFS).");
				}
				if (!foundError && document.frm_genSet.txtAssetNFSSubject.value.length > 250) {
				    foundError=true
				    document.frm_genSet.txtAssetNFSSubject.focus()
				    alert("Please make sure the Email Subject (New Addition of Asset from NFS) is less than 250 characters long.")
			    }
				
				//--> Checking at least 1 "New Addition for NFS Send to" selected 
				if (!foundError && !(document.frm_genSet.cbAssetNFSSendTo1.checked || document.frm_genSet.cbAssetNFSSendTo2.checked || document.frm_genSet.cbAssetNFSSendTo3.checked)){
	                foundError=true;
					document.frm_genSet.cbAssetNFSSendTo1.focus();
					alert("Please select Send To (New Addition of Asset from NFS).");
	            }
	            if (!foundError && document.frm_genSet.cbAssetNFSSendTo3.checked) {
	                if (!foundError && gfnIsFieldBlankValue(document.getElementById("ComboAssetNFSSendTo3").object.GetTextValue())) {
					    foundError=true;
					    //document.frm_genSet.ComboAssetNFSSendTo3.focus();
					    alert("Please enter Email for Send To 'Others' (New Addition of Asset from NFS).");
				    }
			    }
	            
	            //--> Checking New Addition for NFS Content cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtAssetNFSContent)) {
					foundError=true;
					document.frm_genSet.txtAssetNFSContent.focus();
					alert("Please enter Email Content (New Addition of Asset from NFS).");
				}
				if (!foundError && document.frm_genSet.txtAssetNFSContent.value.length > 1000) {
				    foundError=true
				    document.frm_genSet.txtAssetNFSContent.focus()
				    alert("Please make sure the Email Content (New addition of Asset from NFS) is less than 1000 characters long.")
			    }
				
				//validate New Addition of Asset in AMS
				//--> Checking New Addition of Asset in AMS subject cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtAssetAMSSubject)) {
					foundError=true;
					document.frm_genSet.txtAssetAMSSubject.focus();
					alert("Please enter Email Subject (New Addition of Asset in AMS).");
				}
				if (!foundError && document.frm_genSet.txtAssetAMSSubject.value.length > 250) {
				    foundError=true
				    document.frm_genSet.txtAssetAMSSubject.focus()
				    alert("Please make sure the Email Subject (New Addition of Asset in AMS) is less than 250 characters long.")
			    }
				
				//--> Checking at least 1 "New Addition of Asset in AMS Send to" selected 
				if (!foundError && !(document.frm_genSet.cbAssetAMSSend1.checked || document.frm_genSet.cbAssetAMSSend2.checked || document.frm_genSet.cbAssetAMSSend3.checked)){
	                foundError=true;
					document.frm_genSet.cbAssetAMSSend1.focus();
					alert("Please select Send To (New Addition of Asset in AMS).");
	            }
	            if (!foundError && document.frm_genSet.cbAssetAMSSend3.checked) {
	                if (!foundError && gfnIsFieldBlankValue(document.getElementById("ComboAssetAMSSend3").object.GetTextValue())) {
					    foundError=true;
					    //document.frm_genSet.ComboAssetNFSSendTo3.focus();
					    alert("Please enter Email for Send To 'Others' (New Addition of Asset in AMS).");
				    }
			    }
	            
	            //--> Checking New Addition of Asset in MAS Content cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtAssetAMSContent)) {
					foundError=true;
					document.frm_genSet.txtAssetAMSContent.focus();
					alert("Please enter Email Content (New Addition of Asset in AMS).");
				}
				if (!foundError && document.frm_genSet.txtAssetAMSContent.value.length > 1000) {
				    foundError=true
				    document.frm_genSet.txtAssetAMSContent.focus()
				    alert("Please make sure the Email Content (New addition of Asset in AMS) is less than 1000 characters long.")
			    }
			    
			    //validate Warranty Expiry
				//--> Checking Warranty Expiry subject cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtWarrantyExpSubj)) {
					foundError=true;
					document.frm_genSet.txtWarrantyExpSubj.focus();
					alert("Please enter Email Subject (Warranty Expiry).");
				}
				if (!foundError && document.frm_genSet.txtWarrantyExpSubj.value.length > 250) {
				    foundError=true
				    document.frm_genSet.txtWarrantyExpSubj.focus()
				    alert("Please make sure the Email Subject (Warranty Expiry) is less than 250 characters long.")
			    }
				
				//--> Checking at least 1 "Warranty Expiry Send to" selected 
				if (!foundError && !(document.frm_genSet.cbWarrantyExpSend1.checked || document.frm_genSet.cbWarrantyExpSend2.checked || document.frm_genSet.cbWarrantyExpSend3.checked)){
	                foundError=true;
					document.frm_genSet.cbWarrantyExpSend1.focus();
					alert("Please select Send To (Warranty Expiry).");
	            }
	            if (!foundError && document.frm_genSet.cbWarrantyExpSend3.checked) {
	                if (!foundError && gfnIsFieldBlankValue(document.getElementById("ComboWarrantyExpSend3").object.GetTextValue())) {
					    foundError=true;
					    //document.frm_genSet.ComboAssetNFSSendTo3.focus();
					    alert("Please enter Email for Send To 'Others' (Warranty Expiry).");
				    }
			    }

	//--> Checking Warranty Expiry Content cannot be blank
	if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtWarrantyExpContent)) {
	    foundError = true;
	    document.frm_genSet.txtWarrantyExpContent.focus();
	    alert("Please enter Email Content (Warranty Expiry).");
	}
	if (!foundError && document.frm_genSet.txtWarrantyExpContent.value.length > 1000) {
	    foundError = true
	    document.frm_genSet.txtWarrantyExpContent.focus()
	    alert("Please make sure the Email Content (Warranty Expiry) is less than 1000 characters long.")
	}
	
	//validate Condemn Expiry
	//--> Checking Warranty Expiry subject cannot be blank
	if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtCondemnExpSubj)) {
	    foundError = true;
	    document.frm_genSet.txtCondemnExpSubj.focus();
	    alert("Please enter Email Subject (Condemn Expiry).");
	}
	if (!foundError && document.frm_genSet.txtCondemnExpSubj.value.length > 250) {
	    foundError = true
	    document.frm_genSet.txtCondemnExpSubj.focus()
	    alert("Please make sure the Email Subject (Condemn Expiry) is less than 250 characters long.")
	}

	//--> Checking at least 1 "Warranty Expiry Send to" selected 
	if (!foundError && !(document.frm_genSet.cbCondemnExpSend1.checked || document.frm_genSet.cbCondemnExpSend2.checked || document.frm_genSet.cbCondemnExpSend3.checked)) {
	    foundError = true;
	    document.frm_genSet.cbCondemnExpSend1.focus();
	    alert("Please select Send To (Condemn Expiry).");
	}
	if (!foundError && document.frm_genSet.cbCondemnExpSend3.checked) {
	    if (!foundError && gfnIsFieldBlankValue(document.getElementById("ComboCondemnExpSend3").object.GetTextValue())) {
	        foundError = true;
	        alert("Please enter Email for Send To 'Others' (Condemn Expiry).");
	    }
	}

	//--> Checking Condemn Expiry Content cannot be blank
	if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtCondemnExpContent)) {
	    foundError = true;
	    document.frm_genSet.txtCondemnExpContent.focus();
	    alert("Please enter Email Content (Condemn Expiry).");
	}
	if (!foundError && document.frm_genSet.txtCondemnExpContent.value.length > 1000) {
	    foundError = true
	    document.frm_genSet.txtCondemnExpContent.focus()
	    alert("Please make sure the Email Content (Condemn Expiry) is less than 1000 characters long.")
	}

	//validate Leased Inventory Expiry
	//--> Checking Warranty Expiry subject cannot be blank
	if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtLeaseInvExpSubj)) {
	    foundError = true;
	    document.frm_genSet.txtLeaseInvExpSubj.focus();
	    alert("Please enter Email Subject (Leased Inventory Expiry).");
	}
	if (!foundError && document.frm_genSet.txtLeaseInvExpSubj.value.length > 250) {
	    foundError = true
	    document.frm_genSet.txtLeaseInvExpSubj.focus()
	    alert("Please make sure the Email Subject (Leased Inventory Expiry) is less than 250 characters long.")
	}

	//--> Checking at least 1 "Lease Inventory Expiry Send to" selected 
	if (!foundError && !(document.frm_genSet.cbLeaseInvExpSend1.checked || document.frm_genSet.cbLeaseInvExpSend2.checked || document.frm_genSet.cbLeaseInvExpSend3.checked)) {
	    foundError = true;
	    document.frm_genSet.cbLeaseInvExpSend1.focus();
	    alert("Please select Send To (Leased Inventory Expiry).");
	}
	if (!foundError && document.frm_genSet.cbLeaseInvExpSend3.checked) {
	    if (!foundError && gfnIsFieldBlankValue(document.getElementById("ComboLeaseInvExpSend3").object.GetTextValue())) {
	        foundError = true;
	        alert("Please enter Email for Send To 'Others' (Leased Inventory Expiry).");
	    }
	}

	//--> Checking Leased Inventory Expiry Content cannot be blank
	if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtLeaseInvExpContent)) {
	    foundError = true;
	    document.frm_genSet.txtLeaseInvExpContent.focus();
	    alert("Please enter Email Content (Leased Inventory Expiry).");
	}
	if (!foundError && document.frm_genSet.txtLeaseInvExpContent.value.length > 1000) {
	    foundError = true
	    document.frm_genSet.txtLeaseInvExpContent.focus()
	    alert("Please make sure the Email Content (Leased Inventory Expiry) is less than 1000 characters long.")
	}    
		
		        
		        //validate New Contract Expiry - 1st Reminder
				//--> Checking Contract Expiry cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtContractExpSubj)) {
					foundError=true;
					document.frm_genSet.txtContractExpSubj.focus();
					alert("Please enter Email Subject (Contract Expiry - 1st Reminder).");
				}
				if (!foundError && document.frm_genSet.txtContractExpSubj.value.length > 250) {
				    foundError=true
				    document.frm_genSet.txtContractExpSubj.focus()
				    alert("Please make sure the Email Subject (Contract Expiry - 1st Reminder) is less than 250 characters long.")
			    }
				
				//--> Checking at least 1 "Contract Expiry Send to" selected 
				if (!foundError && !(document.frm_genSet.cbContractExpSend2.checked || document.frm_genSet.cbContractExpSend3.checked || document.frm_genSet.cbContractExpSend4.checked || document.frm_genSet.cbContractExpSend5.checked)){
	                foundError=true;
					document.frm_genSet.cbContractExpSend2.focus();
					alert("Please select Send To/CC (Contract Expiry - 1st Reminder).");
	            }
	            if (!foundError && document.frm_genSet.cbContractExpSend3.checked) {
	                if (!foundError && gfnIsFieldBlankValue(document.getElementById("ComboContractExpSend3").object.GetTextValue())) {
					    foundError=true;
					    //document.frm_genSet.ComboAssetNFSSendTo3.focus();
					    alert("Please enter Email for Send To 'Others' (Contract Expiry - 1st Reminder).");
				    }
			    }
			    if (!foundError && document.frm_genSet.cbContractExpSend5.checked) {
	                if (!foundError && gfnIsFieldBlankValue(document.getElementById("ComboContractExpSend5").object.GetTextValue())) {
					    foundError=true;
					    //document.frm_genSet.ComboAssetNFSSendTo3.focus();
					    alert("Please enter Email for Send CC 'Others' (Contract Expiry - 1st Reminder).");
				    }
			    }
	            
	            //--> Checking Contract Expiry Content cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtContractExpContent)) {
					foundError=true;
					document.frm_genSet.txtContractExpContent.focus();
					alert("Please enter Email Content (Contract Expiry - 1st Reminder).");
				}
				if (!foundError && document.frm_genSet.txtContractExpContent.value.length > 1000) {
				    foundError=true
				    document.frm_genSet.txtContractExpContent.focus()
				    alert("Please make sure the Email Content (Contract Expiry - 1st Reminder) is less than 1000 characters long.")
			    }
			    
			    //validate New Contract Expiry - 2nd Reminder
				//--> Checking Contract Expiry cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtContractExpSubj2)) {
					foundError=true;
					document.frm_genSet.txtContractExpSubj2.focus();
					alert("Please enter Email Subject (Contract Expiry - 2nd Reminder).");
				}
				if (!foundError && document.frm_genSet.txtContractExpSubj2.value.length > 250) {
				    foundError=true
				    document.frm_genSet.txtContractExpSubj2.focus()
				    alert("Please make sure the Email Subject (Contract Expiry - 2nd Reminder) is less than 250 characters long.")
			    }
				
				//--> Checking at least 1 "Contract Expiry Send to" selected 
				if (!foundError && !(document.frm_genSet.cbContractExp2Send2.checked || document.frm_genSet.cbContractExp2Send3.checked || document.frm_genSet.cbContractExp2Send4.checked || document.frm_genSet.cbContractExp2Send5.checked)){
	                foundError=true;
					document.frm_genSet.cbContractExp2Send2.focus();
					alert("Please select Send To/CC (Contract Expiry - 2nd Reminder).");
	            }
	            if (!foundError && document.frm_genSet.cbContractExp2Send3.checked) {
	                if (!foundError && gfnIsFieldBlankValue(document.getElementById("ComboContractExp2Send3").object.GetTextValue())) {
					    foundError=true;
					    //document.frm_genSet.ComboAssetNFSSendTo3.focus();
					    alert("Please enter Email for Send To 'Others' (Contract Expiry - 2nd Reminder).");
				    }
			    }
			    if (!foundError && document.frm_genSet.cbContractExp2Send5.checked) {
	                if (!foundError && gfnIsFieldBlankValue(document.getElementById("ComboContractExp2Send5").object.GetTextValue())) {
					    foundError=true;
					    //document.frm_genSet.ComboAssetNFSSendTo3.focus();
					    alert("Please enter Email for Send CC 'Others' (Contract Expiry - 2nd Reminder).");
				    }
			    }
	            
	            //--> Checking Contract Expiry Content cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtContractExpContent2)) {
					foundError=true;
					document.frm_genSet.txtContractExpContent2.focus();
					alert("Please enter Email Content (Contract Expiry - 2nd Reminder).");
				}
				if (!foundError && document.frm_genSet.txtContractExpContent2.value.length > 1000) {
				    foundError=true
				    document.frm_genSet.txtContractExpContent2.focus()
				    alert("Please make sure the Email Content (Contract Expiry - 2nd Reminder) is less than 1000 characters long.")
			    }
		 
                //validate Asset Transfer
				//--> Checking Asset Transfer subject cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtAssetTransferSubj)) {
					foundError=true;
					document.frm_genSet.txtAssetTransferSubj.focus();
					alert("Please enter Email Subject (Asset Transfer).");
				}
				if (!foundError && document.frm_genSet.txtAssetTransferSubj.value.length > 250) {
				    foundError=true
				    document.frm_genSet.txtAssetTransferSubj.focus()
				    alert("Please make sure the Email Subject (Asset Transfer) is less than 250 characters long.")
			    }
				
				//--> Checking at least 1 "Asset Transfer Send to" selected 
				if (!foundError && !(document.frm_genSet.cbAssetTransferSend1.checked || document.frm_genSet.cbAssetTransferSend2.checked || document.frm_genSet.cbAssetTransferSend3.checked)){
	                foundError=true;
					document.frm_genSet.cbAssetTransferSend1.focus();
					alert("Please select Send To (Asset Transfer).");
	            }
	            if (!foundError && document.frm_genSet.cbAssetTransferSend3.checked) {
	                if (!foundError && gfnIsFieldBlankValue(document.getElementById("ComboAssetTransferSend3").object.GetTextValue())) {
					    foundError=true;
					    //document.frm_genSet.ComboAssetNFSSendTo3.focus();
					    alert("Please enter Email for Send To 'Others' (Asset Transfer).");
				    }
			    }
	            
	            //--> Checking Asset Transfer Content cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtAssetTransferContent)) {
					foundError=true;
					document.frm_genSet.txtAssetTransferContent.focus();
					alert("Please enter Email Content (Asset Transfer).");
				}
				if (!foundError && document.frm_genSet.txtAssetTransferContent.value.length > 1000) {
				    foundError=true
				    document.frm_genSet.txtAssetTransferContent.focus()
				    alert("Please make sure the Email Content (Asset Transfer) is less than 1000 characters long.")
			    }
			    
			    //validate Float Replacement Level is reached
				//--> Checking Float Replacement Level is reached subject cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtFloatSubj)) {
					foundError=true;
					document.frm_genSet.txtFloatSubj.focus();
					alert("Please enter Email Subject (Float Replacement Level is reached).");
				}
				if (!foundError && document.frm_genSet.txtFloatSubj.value.length > 250) {
				    foundError=true
				    document.frm_genSet.txtFloatSubj.focus()
				    alert("Please make sure the Email Subject (Float Replacement Level is reached) is less than 250 characters long.")
			    }
				
				//--> Checking at least 1 "Float Replacement Level is reached Send to" selected 
				if (!foundError && !(document.frm_genSet.cbFloatSend1.checked || document.frm_genSet.cbFloatSend2.checked)){
	                foundError=true;
					document.frm_genSet.cbFloatSend1.focus();
					alert("Please select Send To (Float Replacement Level is reached).");
	            }
	            if (!foundError && document.frm_genSet.cbFloatSend2.checked) {
	                if (!foundError && gfnIsFieldBlankValue(document.getElementById("ComboFloatSend2").object.GetTextValue())) {
					    foundError=true;
					    //document.frm_genSet.ComboAssetNFSSendTo3.focus();
					    alert("Please enter Email for Send To 'Others' (Float Replacement Level is reached).");
				    }
			    }
	            
	            //--> Checking Float Replacement Level is reached Content cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtFloatContent)) {
					foundError=true;
					document.frm_genSet.txtFloatContent.focus();
					alert("Please enter Email Content (Float Replacement Level is reached).");
				}
				if (!foundError && document.frm_genSet.txtFloatContent.value.length > 1000) {
				    foundError=true
				    document.frm_genSet.txtFloatContent.focus()
				    alert("Please make sure the Email Content (Float Replacement Level is reached) is less than 1000 characters long.")
			    }
			    
			    //--> validate Renewal Contract in X Days txtRenewalCtrlDays
			    if (!foundError && gfnIsFieldBlank(document.frm_genSet.txtRenewalCtrlDays)) {
					foundError=true;
					document.frm_genSet.txtRenewalCtrlDays.focus();
					alert("Please enter 'Days for renewal after a ontract has expired'.");
				}
			    if (!foundError && gfnCheckNumeric(document.frm_genSet.txtRenewalCtrlDays,'')) {
				    foundError=true;
				    document.frm_genSet.txtRenewalCtrlDays.focus();
				    alert("'Days for renewal after a ontract has expired' just allow number only.");
			    } 
//			    if (!foundError && document.frm_genSet.txtRenewalCtrlDays.value >= 0) {
//				    foundError=true
//				    document.frm_genSet.txtRenewalCtrlDays.focus()
//				    alert("Please make sure 'Days for renewal after a ontract has expired' is more than or equal to '0'.")
//			    }
			    
			    /*
			    //validate stocktake from date
			    if (!foundError && gfnCheckDate(document.frm_genSet.txtSTFrmDt, "Stocktake From Date", "M") == false) {
				    foundError=true
			    }
    			
			    //validate stocktake to date
			    if (!foundError && gfnCheckDate(document.frm_genSet.txtSTToDt, "Stocktake To Date", "M") == false) {
				    foundError=true
			    }
		        */
		        
 				if (!foundError){
 					var flag = false;
 				    flag = window.confirm("Are you sure want to update this record?");
 				    return flag;
 				}
				else
					return false;
			}
		function fnShowHideOtherEmail(CallType){
		    //alert ('in')
	        
	        if (CallType == '1') { //New Addition for NFS
	            if (document.frm_genSet.cbAssetNFSSendTo3.checked==true){
	                //document.getElementById("ComboAssetNFSSendTo3").style.display = '';
	                document.getElementById("divAssetNFSSendTo3").style.display = '';
	            }
	            else{
	                //document.getElementById("ComboAssetNFSSendTo3").style.display = 'none';
	                document.getElementById('divAssetNFSSendTo3').style.display = 'none';
	            }
	        }
	        
	        else if (CallType == '2') { //New Addition for AMS
	            if (document.frm_genSet.cbAssetAMSSend3.checked==true){
	                //document.getElementById("ComboAssetAMSSend3").style.display = '';
	                document.getElementById("divAssetAMSSend3").style.display = '';
	            }
	            else{
	                //document.getElementById("ComboAssetAMSSend3").style.display = 'none';
	                document.getElementById("divAssetAMSSend3").style.display = 'none';
	            }
	        }
	        
	        else if (CallType == '3') { //Warranty Expiry
	            if (document.frm_genSet.cbWarrantyExpSend3.checked==true){
	                //document.getElementById("ComboWarrantyExpSend3").style.display = '';
	                document.getElementById("divWarrantyExpSend3").style.display = '';
	            }
	            else{
	                //document.getElementById("ComboWarrantyExpSend3").style.display = 'none';
	                document.getElementById("divWarrantyExpSend3").style.display = 'none';
	            }
	        }
	        
	        else if (CallType == '4') { //Contract Expiry
	            if (document.frm_genSet.cbContractExpSend3.checked==true){
	                //document.getElementById("ComboContractExpSend3").style.display = '';
	                document.getElementById("divContractExpSend3").style.display = '';
	            }
	            else{
	                //document.getElementById("ComboContractExpSend3").style.display = 'none';
	                document.getElementById("divContractExpSend3").style.display = 'none';
	            }
	        }
	        
	        else if (CallType == '5') { //Asset Transfer 
	            if (document.frm_genSet.cbAssetTransferSend3.checked==true){
	                //document.getElementById("ComboAssetTransferSend3").style.display = '';
	                document.getElementById("divAssetTransferSend3").style.display = '';
	            }
	            else{
	                //document.getElementById("ComboAssetTransferSend3").style.display = 'none';
	                document.getElementById("divAssetTransferSend3").style.display = 'none';
	            }
	        }
	        
	        else if (CallType == '6') { //Float Replacement 
	            if (document.frm_genSet.cbFloatSend2.checked==true){
	                //document.getElementById("ComboFloatSend2").style.display = '';
	                document.getElementById("divFloatSend2").style.display = '';
	            }
	            else{
	                //document.getElementById("ComboFloatSend2").style.display = 'none';
	                document.getElementById("divFloatSend2").style.display = 'none';
	            }
	        }
	        
	        else if (CallType == '7') { //Contract Expiry - 2nd Reminder
	            if (document.frm_genSet.cbContractExp2Send3.checked==true){
	                //document.getElementById("ComboContractExpSend3").style.display = '';
	                document.getElementById("divContractExp2Send3").style.display = '';
	            }
	            else{
	                //document.getElementById("ComboContractExpSend3").style.display = 'none';
	                document.getElementById("divContractExp2Send3").style.display = 'none';
	            }
	        }
	        
	        else if (CallType == '8') { //Contract Expiry -> CC
	            if (document.frm_genSet.cbContractExpSend5.checked==true){
	                //document.getElementById("ComboContractExpSend3").style.display = '';
	                document.getElementById("divContractExpSend5").style.display = '';
	            }
	            else{
	                //document.getElementById("ComboContractExpSend3").style.display = 'none';
	                document.getElementById("divContractExpSend5").style.display = 'none';
	            }
	        }
	        
	        else if (CallType == '9') { //Contract Expiry - 2nd Reminder -> CC
	            if (document.frm_genSet.cbContractExp2Send5.checked==true){
	                //document.getElementById("ComboContractExpSend3").style.display = '';
	                document.getElementById("divContractExp2Send5").style.display = '';
	            }
	            else{
	                //document.getElementById("ComboContractExpSend3").style.display = 'none';
	                document.getElementById("divContractExp2Send5").style.display = 'none';
	            }
	        }

	        else if (CallType == '10') { //Condemn Expiry
	            if (document.frm_genSet.cbCondemnExpSend3.checked == true) {
	                document.getElementById("divCondemnExpSend3").style.display = '';
	            }
	            else {
	                document.getElementById("divCondemnExpSend3").style.display = 'none';
	            }
	        }

	        else if (CallType == '11') { //Leased Inventory Expiry
	            if (document.frm_genSet.cbLeaseInvExpSend3.checked == true) {
	                document.getElementById("divLeaseInvSend3").style.display = '';
	            }
	            else {
	                document.getElementById("divLeaseInvSend3").style.display = 'none';
	            }
	        }
	    }
		</script>
	</HEAD>
	<body>
		<form id="frm_genSet" method="post" runat="server">
        <ajaxToolkit:ToolkitScriptManager runat="Server" ID="ScriptManager1" ScriptMode="Release" />
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Administration : General Setting</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table id="Table2" height="500" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<!--Start: Main Content-->
						<table id="Table3" height="400" cellSpacing="1" cellPadding="0" width="100%" align="left"
							bgColor="silver" border="0">
							<tr>
								<td>
									<table id="Table4" height="400" cellSpacing="0" cellPadding="0" width="100%" align="left"
										bgColor="white" border="0">
										<tr>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
													<TR>
														<TD vAlign="top" align="center" width="100%">
														    <asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE id="Table6" height="126" cellSpacing="0" cellPadding="3" width="100%" align="left"
																bgColor="white" border="0">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" height="352">
																			<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
																				<TBODY>
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Inventory Controller</b></TD>
																					</TR>
																					<TR>
																						<TD vAlign=top width="30%">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Inventory Controller Email : </FONT></TD>
																						<TD width="70%">
                                                                                            <%--<ntb:Combo ID="ComboInventoryCtrEmail" runat="server" Mode=SmartList 
                                                                                                       Width="335px" Height="70px" PostBackOnSelectEvent="False" 
                                                                                                       PreconfiguredStylesheet="Outlook" SmartListSeparator=";" >
                                                                                                  <List EnableDatabaseSearch="True" Width="335px" ></List>
                                                                                                  <ListColumnDefinitionItems>
                                                                                                    <ntb:ListColumnDefinition DataFieldIndex="1" Width="335px"></ntb:ListColumnDefinition>
                                                                                                  </ListColumnDefinitionItems> 
                                                                                                  <TextBox DataFieldIndex="1"></TextBox>
                                                                                            </ntb:Combo--%>
                                                                                            
                                                                                            <asp:TextBox runat="server" ID="ComboInventoryCtrEmail" autocomplete="off" 
                                                                                                      Width="335px" Height="70px"   TextMode="MultiLine" />
            
                                                                              
                                                                                            
																						</TD>
																					</TR>
																					<TR>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Email for New Addition of Asset from NFS</b></TD>
																					</TR>
																					<TR>
																						<TD><FONT class="DisplayTitle"><font color="red">*</font>Active? </FONT></TD>
																						<TD>
																						    <asp:radiobuttonlist id="rdAssetNFSActive" Runat="server" RepeatDirection="Horizontal">
																								<asp:ListItem Value="Y" Selected>Yes</asp:ListItem>
																								<asp:ListItem Value="N">No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Email Subject : </FONT></TD>
																						<TD>
																						    <asp:textbox id="txtAssetNFSSubject" Runat="server" maxlength="250" Width="335px"></asp:textbox></TD>
																					</TR>
																					<TR>
																						<TD valign=top><FONT class="DisplayTitle"><font color="red">*</font>Send to </FONT></TD>
																						<TD>
																						    <asp:CheckBox ID=cbAssetNFSSendTo1 Text="Inventory Controller" runat=server></asp:CheckBox><br />
																						    <asp:CheckBox ID=cbAssetNFSSendTo2 Text="Assigned Owner" runat=server></asp:CheckBox><br />
																						    <asp:CheckBox ID=cbAssetNFSSendTo3 Text="Others" runat=server></asp:CheckBox><br />
																						    <div id=divAssetNFSSendTo3>
																						                                                                                                   
                                                                                            <asp:TextBox runat="server" ID="ComboAssetNFSSendTo3" autocomplete="off" 
                                                                                                      Width="335px" Height="70px"   TextMode="MultiLine" />
                                                                                           </div>
																						</TD>
																					</TR>
																					<TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Email Content : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtAssetNFSContent" TextMode=MultiLine maxlength="1000" Columns="45" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtAssetNFSContentCount" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="MMLAssetNFSContent" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Email Content to 1000 Characters." OutputControl="txtAssetNFSContentCount" ControlToValidate="txtAssetNFSContent"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																				    <TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Email for New Addition of Asset in AMS</b></TD>
																					</TR>
																					<TR>
																						<TD><FONT class="DisplayTitle"><font color="red">*</font>Active? </FONT></TD>
																						<TD>
																						    <asp:radiobuttonlist id="rbAssetAMSActive" Runat="server" RepeatDirection="Horizontal">
																								<asp:ListItem Value="Y" Selected>Yes</asp:ListItem>
																								<asp:ListItem Value="N">No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Email Subject : </FONT></TD>
																						<TD>
																						    <asp:textbox id="txtAssetAMSSubject" Runat="server" maxlength="250" Width="335px"></asp:textbox></TD>
																					</TR>
																					<TR>
																						<TD valign=top><FONT class="DisplayTitle"><font color="red">*</font>Send to </FONT></TD>
																						<TD>
																						    <asp:CheckBox ID=cbAssetAMSSend1 Text="Inventory Controller" runat=server></asp:CheckBox><br />
																						    <asp:CheckBox ID=cbAssetAMSSend2 Text="Assigned Owner" runat=server></asp:CheckBox><br />
																						    <asp:CheckBox ID=cbAssetAMSSend3 Text="Others" runat=server></asp:CheckBox><br />
																						    <div id=divAssetAMSSend3>
																						        
                                                                                                <asp:TextBox runat="server" ID="ComboAssetAMSSend3" autocomplete="off" 
                                                                                                      Width="335px" Height="70px"   TextMode="MultiLine" />
                                                                                           </div>
																						</TD>
																					</TR>
																					<TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Email Content : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtAssetAMSContent" TextMode=MultiLine maxlength="1000" Columns="45" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtAssetAMSContentCount" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="MMLAssetAMS" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Email Content to 1000 Characters." OutputControl="txtAssetAMSContentCount" ControlToValidate="txtAssetAMSContent"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																					<TR>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																				    <TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Email for Warranty Expiry</b></TD>
																					</TR>
																					<TR>
																						<TD><FONT class="DisplayTitle"><font color="red">*</font>Active? </FONT></TD>
																						<TD>
																						    <asp:radiobuttonlist id="rbWarrantyExp" Runat="server" RepeatDirection="Horizontal">
																								<asp:ListItem Value="Y" Selected>Yes</asp:ListItem>
																								<asp:ListItem Value="N">No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Email Subject : </FONT></TD>
																						<TD>
																						    <asp:textbox id="txtWarrantyExpSubj" Runat="server" maxlength="250" Width="335px"></asp:textbox></TD>
																					</TR>
																					<TR>
																						<TD valign=top><FONT class="DisplayTitle"><font color="red">*</font>Send to </FONT></TD>
																						<TD>
																						    <asp:CheckBox ID=cbWarrantyExpSend1 Text="Inventory Controller" runat=server></asp:CheckBox><br />
																						    <asp:CheckBox ID=cbWarrantyExpSend2 Text="Assigned Owner" runat=server></asp:CheckBox><br />
																						    <asp:CheckBox ID=cbWarrantyExpSend3 Text="Others" runat=server></asp:CheckBox><br />
																						    <div id=divWarrantyExpSend3>
																						        
                                                                                                <asp:TextBox runat="server" ID="ComboWarrantyExpSend3" autocomplete="off" 
                                                                                                      Width="335px" Height="70px"   TextMode="MultiLine" />
                                                                                            </div>
																						</TD>
																					</TR>
																					<TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Email Content : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtWarrantyExpContent" TextMode=MultiLine maxlength="1000" Columns="45" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtWarrantyExpContentCount" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="MMLWarrantyExp" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Email Content to 1000 Characters." OutputControl="txtWarrantyExpContentCount" ControlToValidate="txtWarrantyExpContent"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																					<TR>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																				    <TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Email for Condemn Expiry</b></TD>
																					</tr>
                                                                                    <tr>
																						<TD><FONT class="DisplayTitle"><font color="red">*</font>Active? </FONT></TD>
																						<TD>
																						    <asp:radiobuttonlist id="rbCondemnExp" Runat="server" 
                                                                                                RepeatDirection="Horizontal">
																								<asp:ListItem Value="Y" Selected>Yes</asp:ListItem>
																								<asp:ListItem Value="N">No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</tr>
                                                                                    <tr>
																						<TD vAlign="middle">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Email Subject : </FONT></TD>
																						<TD>
																						    <asp:textbox id="txtCondemnExpSubj" Runat="server" maxlength="250" 
                                                                                                Width="335px"></asp:textbox></TD>
																					</tr>
                                                                                    <tr>
																						<TD valign=top><FONT class="DisplayTitle"><font color="red">*</font>Send to </FONT></TD>
																						<TD>
																						    <asp:CheckBox ID=cbCondemnExpSend1 Text="Inventory Controller" runat=server></asp:CheckBox><br />
																						    <asp:CheckBox ID=cbCondemnExpSend2 Text="Assigned Owner" runat=server></asp:CheckBox><br />
																						    <asp:CheckBox ID=cbCondemnExpSend3 Text="Others" runat=server></asp:CheckBox><br />
																						    <div id=divCondemnExpSend3>
																						        
                                                                                                <asp:TextBox runat="server" ID="ComboCondemnExpSend3" autocomplete="off" 
                                                                                                      Width="335px" Height="70px"   TextMode="MultiLine" />
                                                                                            </div>
																						</TD>
																					</tr>
                                                                                    <tr>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Email Content : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtCondemnExpContent" TextMode=MultiLine maxlength="1000" 
                                                                                                 Columns="45" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtCondemnExpContentCount" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="MMLCondemnExp" MaxLength="1000" 
                                                                                                 Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" 
                                                                                                 ShowJavascriptAlert="True" 
                                                                                                 ErrorMessage="Please Keep Your Email Content to 1000 Characters." 
                                                                                                 OutputControl="txtCondemnExpContentCount" 
                                                                                                 ControlToValidate="txtCondemnExpContent"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </tr>
                                                                                    <tr>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </tr>
                                                                                    <TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Email for Leased Inventory Expiry</b></TD>
																					</tr>
                                                                                    <tr>
																						<TD><FONT class="DisplayTitle"><font color="red">*</font>Active? </FONT></TD>
																						<TD>
																						    <asp:radiobuttonlist id="rbLeaseInvExp" Runat="server" 
                                                                                                RepeatDirection="Horizontal">
																								<asp:ListItem Value="Y" Selected>Yes</asp:ListItem>
																								<asp:ListItem Value="N">No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</tr>
                                                                                    <tr>
																						<TD vAlign="middle">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Email Subject : </FONT></TD>
																						<TD>
																						    <asp:textbox id="txtLeaseInvExpSubj" Runat="server" maxlength="250" 
                                                                                                Width="335px"></asp:textbox></TD>
																					</tr>
                                                                                    <tr>
																						<TD valign=top><FONT class="DisplayTitle"><font color="red">*</font>Send to </FONT></TD>
																						<TD>
																						    <asp:CheckBox ID=cbLeaseInvExpSend1 Text="Inventory Controller" runat=server></asp:CheckBox><br />
																						    <asp:CheckBox ID=cbLeaseInvExpSend2 Text="Assigned Owner" runat=server></asp:CheckBox><br />
																						    <asp:CheckBox ID=cbLeaseInvExpSend3 Text="Others" runat=server></asp:CheckBox><br />
																						    <div id=divLeaseInvSend3>
																						        
                                                                                                <asp:TextBox runat="server" ID="ComboLeaseInvExpSend3" autocomplete="off" 
                                                                                                      Width="335px" Height="70px"   TextMode="MultiLine" />
                                                                                            </div>
																						</TD>
																					</tr>
                                                                                    <tr>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Email Content : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtLeaseInvExpContent" TextMode=MultiLine maxlength="1000" 
                                                                                                 Columns="45" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtLeaseInvExpContentCount" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="MMLLeaseInvExp" MaxLength="1000" 
                                                                                                 Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" 
                                                                                                 ShowJavascriptAlert="True" 
                                                                                                 ErrorMessage="Please Keep Your Email Content to 1000 Characters." 
                                                                                                 OutputControl="txtLeaseInvExpContentCount" 
                                                                                                 ControlToValidate="txtLeaseInvExpContent"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </tr>
                                                                                    <tr>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </tr>
																				    <TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Email for Contract Expiry (1st Reminder)</b></TD>
																					</TR>
																					<TR>
																						<TD><FONT class="DisplayTitle"><font color="red">*</font>Active? </FONT></TD>
																						<TD>
																						    <asp:radiobuttonlist id="rbContractExp" Runat="server" RepeatDirection="Horizontal">
																								<asp:ListItem Value="Y" Selected>Yes</asp:ListItem>
																								<asp:ListItem Value="N">No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Email Subject : </FONT></TD>
																						<TD>
																						    <asp:textbox id="txtContractExpSubj" Runat="server" maxlength="250" Width="335px"></asp:textbox></TD>
																					</TR>
																					<TR>
																						<TD valign=top><FONT class="DisplayTitle"><font color="red">*</font>Send to </FONT></TD>
																						<TD>
																						    <asp:CheckBox ID=cbContractExpSend2 Text="Contract Owner" runat=server></asp:CheckBox><br />
																						    <asp:CheckBox ID=cbContractExpSend3 Text="Others" runat=server></asp:CheckBox><br />
																						    <div id=divContractExpSend3>
																						        
                                                                                                <asp:TextBox runat="server" ID="ComboContractExpSend3" autocomplete="off" 
                                                                                                      Width="335px" Height="70px"   TextMode="MultiLine" />
                                                                                            </div>
																						</TD>
																					</TR>
																					<TR>
																						<TD valign=top><FONT class="DisplayTitle"><font color="red">*</font>Send CC </FONT></TD>
																						<TD>
																						    <asp:CheckBox ID=cbContractExpSend4 Text="Inventory Controller" runat=server></asp:CheckBox><br />
																						    <asp:CheckBox ID=cbContractExpSend5 Text="Others" runat=server></asp:CheckBox><br />
																						    <div id=divContractExpSend5>																						        
                                                                                                <asp:TextBox runat="server" ID="ComboContractExpSend5" autocomplete="off" 
                                                                                                      Width="335px" Height="70px"   TextMode="MultiLine" />
                                                                                            </div>
																						</TD>
																					</TR>
																					<TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Email Content : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtContractExpContent" TextMode=MultiLine maxlength="1000" Columns="45" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtContractExpContentCount" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="MMLContractExp" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Email Content to 1000 Characters." OutputControl="txtContractExpContentCount" ControlToValidate="txtContractExpContent"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    
																				    <TR>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																				    <TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Email for Contract Expiry (2nd Reminder)</b></TD>
																					</TR>
																					<TR>
																					    <TD colSpan="2">&nbsp;<font color="red">Note: </font><FONT class="DisplayTitle">Head of Department (HOD) will be automatically notified.</FONT></TD>
																				    </TR>
																					<TR>
																						<TD><FONT class="DisplayTitle"><font color="red">*</font>Active? </FONT></TD>
																						<TD>
																						    <asp:radiobuttonlist id="rbContractExp2" Runat="server" RepeatDirection="Horizontal">
																								<asp:ListItem Value="Y" Selected>Yes</asp:ListItem>
																								<asp:ListItem Value="N">No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Email Subject : </FONT></TD>
																						<TD>
																						    <asp:textbox id="txtContractExpSubj2" Runat="server" maxlength="250" Width="335px"></asp:textbox></TD>
																					</TR>
																					<TR>
																						<TD valign=top><FONT class="DisplayTitle"><font color="red">*</font>Send to </FONT></TD>
																						<TD>
																						    <asp:CheckBox ID=cbContractExp2Send2 Text="Contract Owner" runat=server></asp:CheckBox><br />
																						    <asp:CheckBox ID=cbContractExp2Send3 Text="Others" runat=server></asp:CheckBox><br />
																						    <div id=divContractExp2Send3>
																						        
                                                                                                <asp:TextBox runat="server" ID="ComboContractExp2Send3" autocomplete="off" 
                                                                                                      Width="335px" Height="70px"   TextMode="MultiLine" />
                                                                                            </div>
																						</TD>
																					</TR>
																					<TR>
																						<TD valign=top><FONT class="DisplayTitle"><font color="red">*</font>Send CC </FONT></TD>
																						<TD>
																						    <asp:CheckBox ID=cbContractExp2Send4 Text="Inventory Controller" runat=server></asp:CheckBox><br />
																						    <asp:CheckBox ID=cbContractExp2Send5 Text="Others" runat=server></asp:CheckBox><br />
																						    <div id=divContractExp2Send5>
																						        
                                                                                                <asp:TextBox runat="server" ID="ComboContractExp2Send5" autocomplete="off" 
                                                                                                      Width="335px" Height="70px"   TextMode="MultiLine" />
                                                                                            </div>
																						</TD>
																					</TR>
																					<TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Email Content : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtContractExpContent2" TextMode=MultiLine maxlength="1000" Columns="45" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtContractExpContentCount2" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="Multilinetextboxvalidator2" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Email Content to 1000 Characters." OutputControl="txtContractExpContentCount2" ControlToValidate="txtContractExpContent2"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    
																					<TR>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																				    <TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Email for Asset Transfer</b></TD>
																					</TR>
																					<TR>
																						<TD><FONT class="DisplayTitle"><font color="red">*</font>Active? </FONT></TD>
																						<TD>
																						    <asp:radiobuttonlist id="rbAssetTransfer" Runat="server" RepeatDirection="Horizontal">
																								<asp:ListItem Value="Y" Selected>Yes</asp:ListItem>
																								<asp:ListItem Value="N">No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Email Subject : </FONT></TD>
																						<TD>
																						    <asp:textbox id="txtAssetTransferSubj" Runat="server" maxlength="250" Width="335px"></asp:textbox></TD>
																					</TR>
																					<TR>
																						<TD valign=top><FONT class="DisplayTitle"><font color="red">*</font>Send to </FONT></TD>
																						<TD>
																						    <asp:CheckBox ID=cbAssetTransferSend1 Text="Inventory Controller" runat=server></asp:CheckBox><br />
																						    <asp:CheckBox ID=cbAssetTransferSend2 Text="Assigned Owner" runat=server></asp:CheckBox><br />
																						    <asp:CheckBox ID=cbAssetTransferSend3 Text="Others" runat=server></asp:CheckBox><br />
																						    <div id=divAssetTransferSend3>
																						        
                                                                                                <asp:TextBox runat="server" ID="ComboAssetTransferSend3" autocomplete="off" 
                                                                                                      Width="335px" Height="70px"   TextMode="MultiLine" />
                                                                                            </div>
																						</TD>
																					</TR>
																					<TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Email Content : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtAssetTransferContent" TextMode=MultiLine maxlength="1000" Columns="45" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtAssetTransferContentCount" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="MMLAssetTransfer" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Email Content to 1000 Characters." OutputControl="txtAssetTransferContentCount" ControlToValidate="txtAssetTransferContent"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																				    <TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Email for Float Replacement Level is reached</b></TD>
																					</TR>
																					<TR>
																						<TD><FONT class="DisplayTitle"><font color="red">*</font>Active? </FONT></TD>
																						<TD>
																						    <asp:radiobuttonlist id="rbFloatActive" Runat="server" RepeatDirection="Horizontal">
																								<asp:ListItem Value="Y" Selected>Yes</asp:ListItem>
																								<asp:ListItem Value="N">No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Email Subject : </FONT></TD>
																						<TD>
																						    <asp:textbox id="txtFloatSubj" Runat="server" maxlength="250" Width="335px"></asp:textbox></TD>
																					</TR>
																					<TR>
																						<TD valign=top><FONT class="DisplayTitle"><font color="red">*</font>Send to </FONT></TD>
																						<TD>
																						    <asp:CheckBox ID=cbFloatSend1 Text="Inventory Controller" runat=server></asp:CheckBox><br />
																						    <asp:CheckBox ID=cbFloatSend2 Text="Others" runat=server></asp:CheckBox><br />
																						    <div id=divFloatSend2>
																						        
                                                                                                <asp:TextBox runat="server" ID="ComboFloatSend2" autocomplete="off" 
                                                                                                      Width="335px" Height="70px"   TextMode="MultiLine" />
                                                                                            </div>
																						</TD>
																					</TR>
																					<TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Email Content : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtFloatContent" TextMode=MultiLine maxlength="1000" Columns="45" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtFloatContentCount" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="Multilinetextboxvalidator1" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Email Content to 1000 Characters." OutputControl="txtFloatContentCount" ControlToValidate="txtFloatContent"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																				    <TR class="TRTitleBG">
																			            <TD colSpan="2">&nbsp;<b>Days for Renewal after Contract has expired</b></TD>
																		            </TR>
																		            <TR>
																						<TD vAlign="middle">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Days for renewal after a contract has expired : </FONT></TD>
																						<TD>
																						    <asp:textbox id="txtRenewalCtrlDays" Runat="server" maxlength="5" Width="45px"></asp:textbox> days</TD>
																					</TR>
																				    <TR style="visibility:hidden">
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																				    <TR class="TRTitleBG" style="visibility:hidden">
																			            <TD colSpan="2">&nbsp;<b>Others Information</b></TD>
																		            </TR>
																		            <TR style="visibility:hidden">
																					    <TD valign=top colspan=2>
																					         <a href="../cron/asp/view_log.asp" target="ViewLog">View Batch Log</a> 
																					    </TD>
																				    </TR>
																				    <TR style="visibility:hidden">
																					    <TD valign=top colspan=2>
																					         <a href="../cron/asp/upload_asset.asp" target="UploadAsset">Upload Asset</a> 
																					    </TD>
																				    </TR>
																				    <TR style="visibility:hidden">
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																				    <tr>
																				        <TD colSpan="2">
																				            <div id=divSTAction runat=server visible=false>
																				            <TABLE id="Table9" height="126" cellSpacing="0" cellPadding="3" width="100%" align="left" bgColor="white" border="0">
																				                <TR class="TRTitleBG">
																						            <TD colSpan="2">&nbsp;<b>Stocktake Action</b></TD>
																					            </TR>
																					            <TR>
																						            <TD><FONT class="DisplayTitle"><font color="red">*</font>Active? </FONT></TD>
																						            <TD>
																						                <asp:radiobuttonlist id="rdStocktakeActive" Runat="server" RepeatDirection="Horizontal">
																								            <asp:ListItem Value="Y" Selected>Yes</asp:ListItem>
																								            <asp:ListItem Value="N">No</asp:ListItem>
																							            </asp:radiobuttonlist>
																						            </TD>
																					            </TR>
																					            <TR>
																					                <TD>
																						                <FONT class="DisplayTitle"><font color="red">*</font>Stocktake Date : </FONT>
																					                </TD>
																					                <TD>
																						                From <asp:TextBox ID="txtSTFrmDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.frm_genSet.txtSTFrmDt, document.frm_genSet.txtSTToDt);return false;"
										                                                                    HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                                    </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtSTToDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.frm_genSet.txtSTFrmDt, document.frm_genSet.txtSTToDt);return false;"
										                                                                    HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                                    </a>
																					                </TD>
																				                </TR>
																				            </TABLE>
																				            </div>
																				        </TD>
																				    </tr>
																				    
																					<TR>
																						<TD align="center" colSpan="2"><BR>
																							<asp:button id="butSubmit" Runat="Server" Text="Submit"></asp:button>
																							<asp:button id="butReset" Runat="Server" Text="Reset"></asp:button></TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer>
			<!-- Start: Hidden Fields -->
			<!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
				src="../common/DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="Z-INDEX:101; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px">
			</iframe>
            <asp:HiddenField ID="hftest" runat="server" />
			<!-- End  : Hidden Fields -->
			<script type="text/javascript">
			    fnShowHideOtherEmail('1')
			    fnShowHideOtherEmail('2')
			    fnShowHideOtherEmail('3')
			    fnShowHideOtherEmail('4')
			    fnShowHideOtherEmail('5')
			    fnShowHideOtherEmail('6')
			    fnShowHideOtherEmail('7')
			    fnShowHideOtherEmail('8')
			    fnShowHideOtherEmail('9')
			</script>
		</form>
	</body>
</HTML>
