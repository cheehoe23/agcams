<%@ Page Language="vb" AutoEventWireup="false" Codebehind="usr_Edit.aspx.vb" Inherits="AMS.usr_Edit" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<%@ Register TagPrefix="fluent" Namespace="Fluent.MultiLineTextBoxValidator" Assembly="Fluent.MultiLineTextBoxValidator" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>usr_Edit</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
		<script language="JavaScript" src="../Common/CommonJS.js"></script>
		<script language="javascript">
		function chkFrm() {
			var foundError = false;
												
				//validate for department
				if (!foundError && document.Frm_usrEdit.ddlDepartment.value == "-1") {
					foundError=true
					document.Frm_usrEdit.ddlDepartment.focus()
					alert("Please select the Department.")
				}
						
				//validate for User Group
				if (!foundError && document.Frm_usrEdit.ddlUserGroup.value == "-1") {
					foundError=true
					document.Frm_usrEdit.ddlUserGroup.focus()
					alert("Please select the User Group.")
				}
				
 			if (!foundError){
 				var flag = false;
 				flag = window.confirm("Are you sure want to update this record?");
 				return flag;
 			}
			else
				return false;
		}
		</script>
	</HEAD>
	<body>
		<form id="Frm_usrEdit" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>User Management : Edit User</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table id="Table2" height="400" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<!--Start: Main Content-->
						<table id="Table3" height="400" cellSpacing="1" cellPadding="0" width="100%" align="left"
							bgColor="silver" border="0">
							<tr>
								<td>
									<table id="Table4" height="400" cellSpacing="0" cellPadding="0" width="100%" align="left"
										bgColor="white" border="0">
										<tr>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
													<TR>
														<TD vAlign="top" align="center" width="100%">
														    <asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE id="Table6" height="126" cellSpacing="0" cellPadding="3" width="100%" align="left"
																bgColor="white" border="0">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" height="352">
																			<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
																				<TBODY align=left>
																					<TR>
																						<td colspan="2">
																							<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7a">
																									<TR>
																										<TD width="35%"><FONT class="DisplayTitle">Name : </FONT></TD>
																										<TD width="65%"><asp:label id="lblUserName" Runat="server"></asp:label></TD>
																									</TR>
																									<TR>
																										<TD><FONT class="DisplayTitle">Email : </FONT></TD>
																										<TD><asp:label id="txtEmail" Runat="server"></asp:label></TD>
																									</TR>
																									<TR>
																										<TD><FONT class="DisplayTitle"><font color="red">*</font>Department : </FONT></TD>
																										<TD><asp:dropdownlist id="ddlDepartment" Runat="server"></asp:dropdownlist></TD>
																									</TR>
																									<TR>
																										<TD><FONT class="DisplayTitle"><font color="red">*</font>User Group : </FONT></TD>
																										<TD><asp:dropdownlist id="ddlUserGroup" Runat="server"></asp:dropdownlist></TD>
																									</TR>
																									<TR>
																										<TD valign=top><FONT class="DisplayTitle">Remarks : </FONT></TD>
																										<TD><asp:TextBox id="txtRemarks" maxlength="1000" TextMode="MultiLine" Columns="40" Rows="5" Runat="server"></asp:TextBox><br />
																					                        <asp:textbox id="txtRemarksWord" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					                        <fluent:multilinetextboxvalidator id="MMLValRemarks" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Remarks to 1000 Characters." OutputControl="txtRemarksWord" ControlToValidate="txtRemarks"></fluent:multilinetextboxvalidator>
																					                    </TD>
																									</TR>
																								</TABLE>
																						</td>
																					</TR>
																					<TR>
																						<TD align="center" colSpan="2"><BR>
																							<asp:button id="butSubmit" Runat="Server" Text="Submit"></asp:button>&nbsp;
																							<asp:button id="butReset" Runat="Server" Text="Reset"></asp:button>&nbsp;
																							<asp:button id="ButCancel" Runat="Server" Text="Cancel"></asp:button>
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start : Hidden Fields -->
			<asp:TextBox id="hdnUsrID" Runat="server" Visible="False"></asp:TextBox>
			<!-- End   : Hidden Fields -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
		</form>
	</body>
</HTML>
