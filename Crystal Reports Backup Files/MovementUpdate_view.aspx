<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MovementUpdate_view.aspx.vb" Inherits="AMS.MovementUpdate_view" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
</head>
<body>
    <form id="Frm_Stocktake" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Offline Processing : Stocktake History <asp:Label ID=lblHeader runat=server></asp:Label></B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" height="400" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white"  width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" height="400" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0"
																width="100%" ID="Table6">
																<TBODY valign=top>
																	<TR>
																		<TD height="352" valign="top">
																			<table height="400" cellSpacing="1" cellPadding="0" width="100%" align="left" border="0">
						                                                        <tr>
							                                                        <td vAlign="top" align=left bgColor="#a3a9cc">
								                                                        <table cellSpacing="1" cellPadding="1" width="100%" border="0">
									                                                        <tr>
										                                                        <td width="20%"><asp:dropdownlist id="ddlPageSize" Runat="server" AutoPostBack="True"></asp:dropdownlist>
											                                                        &nbsp;&nbsp;<font color="white">per page</font>
										                                                        </td>
									                                                        </tr>
								                                                        </table>
							                                                        </td>
						                                                        </tr>
						                                                        <tr>
							                                                        <td vAlign="top" align="center" height="400">
								                                                        <!--Datagrid for display record.-->
								                                                        <asp:datagrid id="dgStocktake" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
									                                                        ItemStyle-Height="25" AllowSorting="True" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
									                                                        BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" AllowPaging="True" PageSize="10" PagerStyle-HorizontalAlign="Right"
									                                                        PagerStyle-VerticalAlign="Middle" PagerStyle-Mode="NumericPages" PagerStyle-Position="Top"
									                                                        PagerStyle-BackColor="#a3a9cc" datakeyfield="fld_StocktakeID" AlternatingItemStyle-BackColor="#e3d9ee">
									                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
										                                                        Height="25"></headerstyle>
									                                                        <Columns>										                                                        
										                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
											                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
										                                                        </asp:boundcolumn>
                                                                                                <asp:boundcolumn datafield="fld_StocktakeID" headertext="Stocktake ID" ItemStyle-Height="10">
											                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
										                                                        </asp:boundcolumn>
										                                                        <asp:boundcolumn datafield="StocktakeFor" headertext="Stocktake For" ItemStyle-Height="10">
											                                                        <itemstyle width="30%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
										                                                        </asp:boundcolumn>
										                                                        <asp:boundcolumn datafield="fld_CreatedDt" headertext="Stocktake Date" dataformatstring="{0:dd/MM/yyyy}"
											                                                        ItemStyle-Height="10">
											                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
										                                                        </asp:boundcolumn>
										                                                        <asp:boundcolumn datafield="fld_usrName" headertext="Stocktake By" ItemStyle-Height="10">
											                                                        <itemstyle width="20%" cssclass="GridText" verticalalign="Middle"></itemstyle>
										                                                        </asp:boundcolumn>
										                                                         <asp:boundcolumn datafield="TotRec" headertext="Total Asset" ItemStyle-Height="10">
											                                                        <itemstyle width="7%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
										                                                        </asp:boundcolumn>
										                                                        <asp:boundcolumn datafield="TotScan" headertext="Total Scan" ItemStyle-Height="10">
											                                                        <itemstyle width="7%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
										                                                        </asp:boundcolumn>
										                                                        <asp:boundcolumn datafield="TotMiss" headertext="Total Unscan" ItemStyle-Height="10">
											                                                        <itemstyle width="7%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
										                                                        </asp:boundcolumn>
										                                                        <asp:boundcolumn datafield="TotFound" headertext="Total Found" Visible="false"  ItemStyle-Height="10">
											                                                        <itemstyle width="7%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
										                                                        </asp:boundcolumn>
										                                                        <asp:templatecolumn HeaderText="Select" ItemStyle-Width="10%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
											                                                        <itemtemplate>
												                                                        <%#fnShowEditHyperlink(DataBinder.Eval(Container.DataItem, "fld_StocktakeID"))%>
											                                                        </itemtemplate>
										                                                        </asp:templatecolumn>
									                                                        </Columns>
								                                                        </asp:datagrid>
								                                                     </td>
						                                                        </tr>
					                                                        </table>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
			<!-- Start : Hidden Fields -->
			<input type="hidden" id="hdnSTPID" runat="server"> 
			<!-- End   : Hidden Fields -->
	</form>
</body>
</html>
