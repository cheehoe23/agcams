<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="asset_bulkTransfer.aspx.vb" Inherits="AMS.asset_bulkTransfer" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>	
	<LINK href="../common/ComboBox/styles/contactsearch/combo.css" type="text/css" rel="Stylesheet">

    <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
    <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />
                
	<script type="text/javascript">

	    $(document).ready(function () {
	        SearchText("cbOwner", "hfOwner");
	    });
	    function SearchText(obj1, obj2) {
	        $("#" + obj1).autocomplete({
	            source: function (request, response) {
	                $.ajax({
	                    url: '<%#ResolveUrl("~/AssetMng/asset_bulkTransfer.aspx/GetUsersList") %>',
	                    data: "{ 'prefix': '" + request.term + "'}",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    success: function (data) {
	                        response($.map(data.d, function (item) {
	                            return {
	                                label: item.split('~')[0],
	                                val: item.split('~')[1]
	                            }
	                        }))
	                    },
	                    error: function (response) {
	                        alert(response.responseText);
	                    },
	                    failure: function (response) {
	                        alert(response.responseText);
	                    }
	                });
	            },
	            select: function (e, i) {
	                $("#" + obj2).val(i.item.val);
	            },
	            minLength: 0
	        });

	    }
        
	    function chkFrm() {
			var foundError = false;
			            
//			//at least 1 location selected
//			if (!foundError && (document.Frm_BulkTransfer.ddlDepartment.value == '-1' && document.Frm_BulkTransfer.ddlLocation.value == '-1' && document.Frm_BulkTransfer.ddlOwner.value == '-1')) {
//			    foundError=true
//			    document.Frm_BulkTransfer.ddlDepartment.focus()
//			    alert("Please select Asset Location.");
//			}

            //validate for department
			if (!foundError && document.Frm_BulkTransfer.ddlDepartment.value == "-1") {
				foundError=true
				document.Frm_BulkTransfer.ddlDepartment.focus()
				alert("Please select the Department.")
			}
			
			//validate for Location
			//--> if Location is seleted, Sub Location must selected.
			if (!foundError && document.Frm_BulkTransfer.ddlLocation.value != "-1") {
				if (!foundError && document.Frm_BulkTransfer.ddlLocSub.value == "-1") {
				    foundError=true
				    document.Frm_BulkTransfer.ddlLocSub.focus()
				    alert("Please select the Sub Location.")
			    }
			}
			
//			//Validate Owner
//			if (!foundError && !gfnIsFieldBlank(document.Frm_BulkTransfer.cbOwner)) {
//			    if (!foundError && gfnIsFieldBlank(document.Frm_BulkTransfer.cbOwnerSelectedValue0)) {
//		                foundError=true
//		                alert("Owner not found. Please select an existing Owner.")
//                }
//            }

            //validate purchase date
if (!foundError && gfnCheckDate(document.Frm_BulkTransfer.txtTransDt, "Trans Date", "M") == false) {
                foundError = true
            }
						 
 			if (!foundError){
 			    var flag = false;
 				flag = window.confirm("Are you sure you want to transfer Asset(s)?");
 				return flag;
 			}
			else
				return false;
		}
	</script>
</head>
<body>
     <form id="Frm_BulkTransfer" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Asset Management : Asset Transfer</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" height="400" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table3">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table4">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table5">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table6">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table7">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table9">
																				<TBODY align=left>
																				    <TR>
																					    <TD valign="middle" width="35%">
																					        <font color="red">*</font><FONT class="DisplayTitle">Department : </FONT>
																					    </TD>
																					    <TD width="65%">
																						    <asp:DropDownList ID="ddlDepartment" Runat="server"></asp:DropDownList>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Location : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:DropDownList ID="ddlLocation" Runat="server" AutoPostBack=true></asp:DropDownList>
																					    </TD>
																				    </TR>
																				    <TR>
																	                    <TD>
																	                        <FONT class="DisplayTitle">Sub Location : </FONT>
																	                    </TD>
																	                    <TD>
																		                    <asp:DropDownList ID="ddlLocSub" Runat="server"></asp:DropDownList>
																	                    </TD>
																                    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Assigned Owner : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:TextBox ID="cbOwner" runat="server" Width="250px"></asp:TextBox>

																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">
																					        <font color="red">*</font>Trans Date : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:TextBox ID="txtTransDt" Width="120" Runat="server"></asp:TextBox>
																						    <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fSingleDtPop(document.Frm_BulkTransfer.txtTransDt);return false;"
																							    HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select Date">
																						    </a>
                                                                                        </TD>
																				    </TR>
																				    <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Asset Included</b></TD>
																					</TR>
																					<tr>
								                                                        <td vAlign="top" align="center" colspan=2>
									                                                        <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgAsset" Runat="server" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
												                                                        <itemstyle width="12%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr">
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Category" datafield="fld_CategoryName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Subcategory" datafield="fld_CatSubName" >
												                                                        <itemstyle width="13%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>							
											                                                        <asp:boundcolumn headertext="Department" datafield="fld_DepartmentName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Location" datafield="fld_LocationName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Sub Location" datafield="fld_LocSubName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Assigned Owner" datafield="fld_OwnerName" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Status" datafield="fld_AssetStatusStr" >
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>												
										                                                        </Columns>
									                                                        </asp:datagrid>
								                                                        </td>
							                                                        </tr>
																					<TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:Button id="butTransferAsset" Text="Transfer Asset" Runat="Server" />&nbsp;
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start: Hidden Fields --> 
			<input id="hdnCallFrm" type="hidden" runat="server">
			<input id="hdnAssetIDs" type="hidden" runat="server">
            <input type="hidden" id="hfOwner" runat="server"> 
			<!-- End  : Hidden Fields -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
            <!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
				src="../common/DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="Z-INDEX:101; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px">
			</iframe>
    </form>
</body>
</html>
