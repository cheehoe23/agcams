#Region "Information Section"
' ****************************************************************************************************
' Description       : Search Asset 
' Purpose           : Search Asset Information
' Date              : 10/11/2008
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Web.Services

#End Region

Partial Public Class CRAsset_Search
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            Me.Header.DataBind()

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butSearch.Attributes.Add("OnClick", "return chkFrm()")

                hdnCallFrm.Value = Request("CallType")
                If hdnCallFrm.Value = "C" Then
                    lblHeader.Text = "Condemnation Search Asset"
                Else
                    lblHeader.Text = "Redundancy Search Asset"
                End If

                ''*** Populate Control
                fnPopulateCtrl()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Asset Type --> Fixed Asset, Inventory, Leased Inventory
            fnPopulateAssetTypeInDDL(ddlAssetType)
            ddlAssetType.Items.Remove(ddlAssetType.Items.FindByValue(""))
            ddlAssetType.Items.Remove(ddlAssetType.Items.FindByValue("L"))

            ''Get Category
            fnPopulateDropDownList(clsCategory.fnCategoryGetCategory(), ddlAssetCat, "fld_CategoryID", "fld_CatCodeName", False)

            ''Get SubCategory (set default --> -1)
            ddlAssetSubCat.Items.Insert(0, New ListItem("", "-1"))

            ''Get Controlled Item --> Yes, No
            fnPopulateYesNoInDDL(ddlCtrlItem)

            ''Get Asset Status --> Active, InActive, Condemation, Dispose, Returned
            fnPopulateAssetStatusInDDL(ddlStatus)
            ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("R"))
            ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("C"))
            ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("D"))
            ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("T"))

            ''Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False)
            If Session("AdminF") = "N" Then ddlDepartment.Items.Remove(ddlDepartment.Items.FindByValue("-1"))

            ''Get Location
            fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLocation, "fld_LocationID", "fld_LoctCodeName", False)

            ''Get Sub Location
            ddlLocation_SelectedIndexChanged(Nothing, Nothing)

            ''Get Temporary Asset Type --> Yes, No
            fnPopulateYesNoInDDL(ddlTempAsset)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub ddlAssetCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAssetCat.SelectedIndexChanged
        Try
            If ddlAssetCat.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsCategory.fnCategoryGetAllRecDR(ddlAssetCat.SelectedValue, 0, "fld_CatSubName", "ASC"), ddlAssetSubCat, "fld_CatSubID", "fld_CatSubName", False)
            Else
                ddlAssetSubCat.Items.Clear()
                ddlAssetSubCat.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            Server.Transfer("CRAsset_Search.aspx")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSearch.Click
        Try
            Dim dsAsset As New DataSet
            Dim strURL As String = ""

            Dim strAssetIDAMS As String = Trim(txtAssetIDAMS.Text)
            Dim strAssetIDNFS As String = Trim(txtAssetIDNFS.Text)
            Dim strAssetType As String = ddlAssetType.SelectedValue
            Dim intAssetCatID As Integer = IIf(ddlAssetCat.SelectedValue = "-1", "0", ddlAssetCat.SelectedValue)
            Dim intAssetCatSubID As Integer = IIf(ddlAssetSubCat.SelectedValue = "-1", "0", ddlAssetSubCat.SelectedValue)
            Dim strCtrlItemF As String = ddlCtrlItem.SelectedValue
            Dim strAssetStatus As String = ddlStatus.SelectedValue
            Dim strPurchaseFDt As String = txtPurchaseFrmDt.Text
            Dim strPurchaseTDt As String = txtPurchaseToDt.Text
            Dim strWarExpFDt As String = txtWarExpFrmDt.Text
            Dim strWarExpTDt As String = txtWarExpToDt.Text
            Dim intDeptID As Integer = IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue)
            Dim intLocID As Integer = IIf(ddlLocation.SelectedValue = "-1", "0", ddlLocation.SelectedValue)
            Dim intLocSubID As Integer = IIf(ddlLocSub.SelectedValue = "-1", "0", ddlLocSub.SelectedValue)
            Dim strOwnerID As String = "" '= IIf(ddlOwner.SelectedValue = "-1", "", ddlOwner.SelectedValue)
            Dim strTempAsset As String = ddlTempAsset.SelectedValue
            Dim strCallFrm As String = hdnCallFrm.Value
            Dim StrSortName As String = "fldAssetBarcode"
            Dim strSortOrder As String = "ASC"

            ''*** Get Owner
            If Trim(cbOwner.Text) <> "" Then
                Dim strUsrExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbOwner.Text), strOwnerID, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Owner not found. Please select an existing Owner."
                    lblErrorMessage.Visible = True
                    cbOwner.Focus()
                    Exit Sub
                End If
            End If

            dsAsset = clsDisMng.fnDisMngSrhAsset_SearchRecord( _
                         strAssetIDAMS, strAssetIDNFS, _
                         strAssetType, intAssetCatID, intAssetCatSubID, _
                         strAssetStatus, strPurchaseFDt, strPurchaseTDt, _
                         strWarExpFDt, strWarExpTDt, _
                         intDeptID, intLocID, strOwnerID, _
                         strTempAsset, _
                         strCallFrm, StrSortName, strSortOrder, intLocSubID, strCtrlItemF)


            If dsAsset.Tables(0).Rows.Count > 0 Then
                strURL = "CRAsset_SearchResult.aspx?CallFrm=" + strCallFrm + "&AIdAMS=" + strAssetIDAMS + "&AIdNFS=" + strAssetIDNFS + _
                         "&AType=" + strAssetType + "&CatID=" + CStr(intAssetCatID) + "&CatSID=" + CStr(intAssetCatSubID) + _
                         "&Astatus=" + strAssetStatus + "&PFDt=" + strPurchaseFDt + "&PTDt=" + strPurchaseTDt + _
                         "&WFDt=" + strWarExpFDt + "&WTDt=" + strWarExpTDt + _
                         "&DeptID=" + CStr(intDeptID) + "&LocID=" + CStr(intLocID) + "&OwnerID=" + strOwnerID + _
                         "&TempAsset=" + strTempAsset + "&SLocID=" + CStr(intLocSubID) + "&CtrlItemF=" + strCtrlItemF

                Response.Redirect(strURL)
            Else
                lblErrorMessage.Visible = True
                lblErrorMessage.Text = "Record(s) not found."
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLocation.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCodeName", False)
            Else
                ddlLocSub.Items.Clear()
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class