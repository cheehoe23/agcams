#Region "Information Section"
' ****************************************************************************************************
' Description       : View Search Result Certificate 
' Purpose           : View Search Result Certificate  Information
' Date              : 13/11/2008
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class CRCert_SearchResult
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                ''Get Value pass from Search Screen
                hdnCertSNo.Value = Request("CertSNo")
                hdnCertCFDt.Value = Request("CertCFDt")
                hdnCertCTDt.Value = Request("CertCTDt")
                hdnCertStatus.Value = Request("CertStatus")
                hdnCertType.Value = Request("CertType")
                hdnAssetIDAMS.Value = Request("AssetIDAMS")
                hdnAssetIDNFS.Value = Request("AssetIDNFS")


                ''default Sorting/setting
                hdnSortName.Value = "fld_CertSNo"
                hdnSortAD.Value = "DESC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()

            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Sort Header Display
            fnSortHeaderDisplay()

            '' Get Records
            dgCert.DataSource = clsDisMng.fnDisMng_SearchCertRecord( _
                                 hdnCertSNo.Value, hdnCertCFDt.Value, hdnCertCTDt.Value, _
                                 hdnCertStatus.Value, hdnCertType.Value, _
                                 hdnAssetIDAMS.Value, hdnAssetIDNFS.Value, _
                                 hdnSortName.Value, hdnSortAD.Value)
            dgCert.DataBind()
            If Not dgCert.Items.Count > 0 Then ''Not Records found
                ddlPageSize.Enabled = False
                lblErrorMessage.Text = "There is no cert record."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        Dim intSortIndex As Integer
        Dim strSortHeader As String = ""

        ''Set Default Header
        dgCert.Columns(2).HeaderText = "Cert S/No"
        dgCert.Columns(3).HeaderText = "Cert Status"
        dgCert.Columns(4).HeaderText = "Cert Type"
        dgCert.Columns(5).HeaderText = "Creation Date"

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fld_CertSNo"
                intSortIndex = 2
                strSortHeader = "Cert S/No"
            Case "fld_CertStatus"
                intSortIndex = 3
                strSortHeader = "Cert Status"
            Case "fld_CertType"
                intSortIndex = 4
                strSortHeader = "Cert Type"
            Case "fld_CreatedDt"
                intSortIndex = 5
                strSortHeader = "Creation Date"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgCert.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgCert.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgCert.CurrentPageIndex = 0
        dgCert.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Public Function fnShowStatus(ByVal strStatus As String) As String
        Try
            Dim strRetVal As String = ""

            If strStatus = "P" Then
                strRetVal = "Pending"
            ElseIf strStatus = "A" Then
                strRetVal = "Approved"
            ElseIf strStatus = "R" Then
                strRetVal = "Reject"
            ElseIf strStatus = "D" Then
                strRetVal = "Disposed"
            End If

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Public Function fnShowType(ByVal strType As String) As String
        Try
            Dim strRetVal As String = ""

            If strType = "C" Then
                strRetVal = "Condemnation"
            ElseIf strType = "R" Then
                strRetVal = "Redundancy"
            End If

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Public Function fnShowEditIcon(ByVal strType As String, ByVal strCertID As String) As String
        Try
            Dim strRetVal As String = ""

            If strType = "C" Then
                strRetVal = "<a id=hlCDCert href='CRCert_Condemn.aspx?CertID=" + strCertID + "' target=_self >" & _
                             "<img id=imgUpdate src=../images/update.gif border=0 >" & _
                             "</a>"

            ElseIf strType = "R" Then
                strRetVal = "<a id=hlCDCert href='CRCert_Redundancy.aspx?CertID=" + strCertID + "' target=_self >" & _
                             "<img id=imgUpdate src=../images/update.gif border=0 >" & _
                             "</a>"

            End If

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub dgCert_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgCert.PageIndexChanged
        dgCert.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub dgCert_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgCert.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub
End Class