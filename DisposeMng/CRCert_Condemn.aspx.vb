#Region "Information Section"
' ****************************************************************************************************
' Description       : Condemnation Cert 
' Purpose           : Condemnation Cert Information
' Date              : 13/11/2008
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Web.Services

#End Region

Partial Public Class CRCert_Condemn
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********
            Me.Header.DataBind()

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butApprove.Attributes.Add("OnClick", "return fnConfirmRec('A')")
                Me.butReject.Attributes.Add("OnClick", "return fnConfirmRec('R')")
                Me.butDispose.Attributes.Add("OnClick", "return fnConfirmRec('D')")

                ''Get Default Value
                hdnCertID.Value = Request("CertID")

                ''Populate Control
                fnPopulateCtrl()

                ''Display Record
                fnPopulateRecords()

                ''Check Access Right
                If hdnCertStatus.Value = "P" Then
                    If (InStr(Session("AR"), "DisMng|CRC|AR") = 0) Then
                        ''checking access right
                        butApprove.Visible = False
                        butReject.Visible = False
                        divReasonRejectField.Visible = False
                    Else
                        ''checking for "just approving officer allow to approve/reject"
                        If Not clsDisMng.fnDisMng_CondemnCheckAppOff(Session("UsrID"), "", hdnCertID.Value, "M") Then
                            butApprove.Visible = False
                            butReject.Visible = False
                            divReasonRejectField.Visible = False

                            lblErrorMessage.Text = "You are not authorized to approve/reject certificate."
                            lblErrorMessage.Visible = True
                        End If
                    End If
                ElseIf hdnCertStatus.Value = "A" Then
                    If (InStr(Session("AR"), "DisMng|CRC|Dis") = 0) Then
                        ''checking access right
                        butDispose.Visible = False
                        divCompDisposalField.Visible = False
                    End If
                End If


            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Completion Disposal
            cbCompDisp.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Dim objRdr As SqlDataReader
        Try
            Dim strStatus As String = ""
            Dim strPath1a As String = gCondemnPath & "C" & hdnCertID.Value & "_1a.pdf"
            Dim strPath1b As String = gCondemnPath & "C" & hdnCertID.Value & "_1b.pdf"
            Dim strPath1c As String = gCondemnPath & "C" & hdnCertID.Value & "_1c.pdf"
            Dim strPath2a As String = gCondemnPath & "C" & hdnCertID.Value & "_2a.pdf"
            Dim strPath2b As String = gCondemnPath & "C" & hdnCertID.Value & "_2b.pdf"
            Dim strPath2c As String = gCondemnPath & "C" & hdnCertID.Value & "_2c.pdf"
            Dim dtFile As DataTable

            ''Get Certificate Records
            objRdr = clsDisMng.fnDisMng_GetCondemnRecordsDR(hdnCertID.Value, "M")
            If Not objRdr Is Nothing Then

                dtFile = New DataTable()
                dtFile = clsDisMng.fnCondemnFileGetRec(0, hdnCertID.Value, "N").Tables(0)

                For i As Integer = 0 To dtFile.Rows.Count - 1
                    spanOtherSupportingDoc.InnerHtml += "<a id=hlFile href='" & gCondemnPath & dtFile.Rows(i)("fld_FileSaveName").ToString() & "' target=CondemnFile3 >" & _
                                                 "View " & dtFile.Rows(i)("fld_FileName").ToString() & _
                                                 "</a>, "
                Next

                If objRdr.HasRows Then
                    objRdr.Read()
                    lblCertSNo.Text = CStr(objRdr("fld_CDSNo"))
                    strStatus = CStr(objRdr("fld_CDCertStatus"))
                    hdnCertStatus.Value = CStr(objRdr("fld_CDCertStatus"))

                    If strStatus = "P" Then
                        lblCertStatus.Text = "Pending"

                        spanCertLink.InnerHtml = "<a id=hlFile href='" & strPath1a & "' target=CondemnFile1 >" & _
                                                 "View certificate Part 1" & _
                                                 "</a>, " & _
                                                 "<a id=hlFile href='" & strPath2a & "' target=CondemnFile2 >" & _
                                                 "View certificate Part 2" & _
                                                 "</a>"

                        divCompDisposalView.Visible = False
                        divCompDisposalField.Visible = False
                        divReasonRejectField.Visible = True
                        divReasonRejectView.Visible = False
                        butApprove.Visible = True
                        butReject.Visible = True
                        butDispose.Visible = False

                    ElseIf strStatus = "A" Then
                        lblCertStatus.Text = "Approved"

                        spanCertLink.InnerHtml = "<a id=hlFile href='" & strPath1b & "' target=CondemnFile1 >" & _
                                                 "View certificate Part 1" & _
                                                 "</a>, " & _
                                                 "<a id=hlFile href='" & strPath2b & "' target=CondemnFile2 >" & _
                                                 "View certificate Part 2" & _
                                                 "</a>"

                        divCompDisposalView.Visible = False
                        divCompDisposalField.Visible = True
                        divReasonRejectField.Visible = False
                        divReasonRejectView.Visible = False
                        butApprove.Visible = False
                        butReject.Visible = False
                        butDispose.Visible = True

                    ElseIf strStatus = "R" Then
                        lblCertStatus.Text = "Reject"

                        spanCertLink.InnerHtml = "<a id=hlFile href='" & strPath1b & "' target=CondemnFile1 >" & _
                                                 "View certificate Part 1" & _
                                                 "</a>, " & _
                                                 "<a id=hlFile href='" & strPath2b & "' target=CondemnFile2 >" & _
                                                 "View certificate Part 2" & _
                                                 "</a>"

                        divCompDisposalView.Visible = False
                        divCompDisposalField.Visible = False
                        divReasonRejectField.Visible = False
                        divReasonRejectView.Visible = True
                        butApprove.Visible = False
                        butReject.Visible = False
                        butDispose.Visible = False

                    ElseIf strStatus = "D" Then
                        lblCertStatus.Text = "Disposed"

                        spanCertLink.InnerHtml = "<a id=hlFile href='" & strPath1c & "' target=CondemnFile1 >" & _
                                                 "View certificate Part 1" & _
                                                 "</a>, " & _
                                                 "<a id=hlFile href='" & strPath2c & "' target=CondemnFile2 >" & _
                                                 "View certificate Part 2" & _
                                                 "</a>"

                        divCompDisposalView.Visible = True
                        divCompDisposalField.Visible = False
                        divReasonRejectField.Visible = False
                        divReasonRejectView.Visible = False
                        butApprove.Visible = False
                        butReject.Visible = False
                        butDispose.Visible = False
                    End If

                    lblDept.Text = CStr(objRdr("fld_DeptName")) & " (" & CStr(objRdr("fld_DeptCode")) & ")"
                    lblTotNBV.Text = CStr(objRdr("fld_TotNBV"))
                    lblCondemnReason.Text = CStr(objRdr("fld_CondemnReason"))
                    lblDisposalMethod.Text = CStr(objRdr("fld_DisposalMethod"))
                    lblTotProceed.Text = CStr(objRdr("fld_TotProceed"))
                    lblTotRemovalC.Text = CStr(objRdr("fld_TotRemovalCost"))

                    lblAppOff.Text = CStr(objRdr("fld_AppOffName"))
                    lblAODesg.Text = CStr(objRdr("fld_AppOffDesg"))
                    lblAODt.Text = fnCheckNull(objRdr("fld_AppOffDt"))

                    lblRequestor.Text = CStr(objRdr("fld_RequestorName"))
                    lblReqDesg.Text = CStr(objRdr("fld_RequestorDesg"))
                    lblReqDt.Text = CStr(objRdr("fld_RequestorDt"))

                    lblCompDisp.Text = fnCheckNull(objRdr("fld_CDisposeName"))
                    lblCDispDesg.Text = fnCheckNull(objRdr("fld_CDisposeDesg"))
                    lblCDispDt.Text = fnCheckNull(objRdr("fld_CDisposeDt"))

                    lblRejReason.Text = Replace(fnCheckNull(objRdr("fld_RejectReason")), vbCrLf, "<br>") 'fnCheckNull(objRdr("fld_RejectReason"))

                End If
            End If
            objRdr.Close()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Function

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Protected Sub butApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butApprove.Click
        Try
            Dim intRetVal As Integer
            Dim strCertID As String = hdnCertID.Value

            ''*** Approve Certificate 
            intRetVal = clsDisMng.fnDisMng_CondemnCertApprove( _
                            strCertID, Session("UsrID"))

            If intRetVal > 0 Then
                ''Generate Condemn PDF
                Dim strPath1 As String = gCondemnPath & "C" & strCertID & "_1b.pdf"
                Dim strPath2 As String = gCondemnPath & "C" & strCertID & "_2b.pdf"
                fnGenPDF1(strCertID, Server.MapPath(strPath1))
                fnGenPDF2(strCertID, Server.MapPath(strPath2))

                ''store Condemn Cert to eRegistry
                Dim strCertID1 As String = "C" & strCertID & "_1b.pdf" '""
                Dim strCertID2 As String = "C" & strCertID & "_2b.pdf" '""
                ''Check whether PDF File generated
                If File.Exists(Server.MapPath(strPath1)) And File.Exists(Server.MapPath(strPath2)) Then
                    ''Store Condemn Cert to eRegistry is going here...

                Else
                    lblErrorMessage.Text = "Certificate of Condemnation generate failded. Please contact your administrator."
                    lblErrorMessage.Visible = True
                    Exit Sub
                End If

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Certificate of Condemnation with S/No: " & lblCertSNo.Text & " have been approved.")

                Me.fnEmail("Approved")
                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + "Certificate of Condemnation approved successfully." + "');" & _
                                "document.location.href='CRCert_Condemn.aspx?CertID=" & strCertID & "';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                lblErrorMessage.Text = "Certificate of Condemnation approve failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnEmail4CPFFinance(ByVal strCondemnID As String, ByVal strCertID1 As String, ByVal strCertID2 As String)
        Dim objRdr As SqlDataReader
        Try
            Dim strEmailActiveF As String = "N"
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""

            Dim strEmailTo As String = ""
            Dim strEmailCC As String = ""
            Dim strMailBody As String = ""

            ''*** Get Email Details from database
            objRdr = clsDisMng.fnDisMng_GetEmailInfo4Condemn2(strCondemnID)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()

                    strEmailActiveF = CStr(objRdr("EmailActiveF"))
                    strMailSubject = CStr(objRdr("EmailSubject")) & " S/No:" & Replace(lblCertSNo.Text, "A", "D")
                    strEmailContent = objRdr("EmailContent")
                    strEmailTo = CStr(objRdr("EmailTo"))

                    ''*** If Email Active, checking for send email
                    If strEmailActiveF = "Y" Then
                        ''--> remove duplicate email ids in the To List and CC List.
                        strEmailTo = fnRemoveNameFromEmailAdd(strEmailTo)
                        strEmailTo = fnRemoveDuplicates(strEmailTo, ";")
                        'strEmailCC = fnRemoveNameFromEmailAdd(strEmailCC)
                        'strEmailCC = fnRemoveDuplicates2(strEmailTo, strEmailCC, ";")

                        ''--> Get Email Body
                        strMailBody = "Dear Sir/Madam,<br><br><br>"
                        strMailBody += Replace(strEmailContent, vbCrLf, "<br>")
                        strMailBody += "<BR><br>Please refer below for Condemnation Certificate:-<br>"
                        strMailBody += "Condemnation Certificate Part 1 : "
                        strMailBody += "<a id=hlFile href='" & gCondemnEReg() & strCertID1 & "' target=CondemnFile1 >"
                        strMailBody += "Click for view certificate"
                        strMailBody += "</a>"
                        strMailBody += "<br>"
                        strMailBody += "Condemnation Certificate Part 2 : "
                        strMailBody += "<a id=hlFile href='" & gCondemnEReg() & strCertID2 & "' target=CondemnFile2 >"
                        strMailBody += "Click for view certificate"
                        strMailBody += "</a>"
                        strMailBody += "<BR><br><br>Thanks and Regards,"
                        strMailBody += "<BR>File and Asset Management System"
                        strMailBody += "<BR><br><br>* This is a computer generated e-mail, please do not reply to it."

                        ''--> Send Email
                        If Trim(strEmailTo) <> "" Or Trim(strEmailCC) <> "" Then
                            fnSendEmail(strEmailTo, strEmailCC, "", strMailSubject, strMailBody, "", "H")
                        End If
                    Else
                        Exit Sub
                    End If
                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Public Sub fnGenPDF1(ByVal strCondemnID As String, ByVal strFullPath As String)
        Try
            Dim ds As New DataSet("tbl_CondemnMst")
            ds = clsDisMng.fnDisMng_GetCondemnRecords(strCondemnID, "M")

            If ds.Tables(0).Rows.Count > 0 Then
                Dim myReportDocument As New ReportDocument()
                myReportDocument.Load(Server.MapPath("ReportFile\CondemnCert.rpt"))
                myReportDocument.SetDataSource(ds.Tables(0))
                myReportDocument.SetParameterValue("CondemnID", strCondemnID)
                Dim DiskOpts2 As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions()
                myReportDocument.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile
                DiskOpts2.DiskFileName = strFullPath
                myReportDocument.ExportOptions.DestinationOptions = DiskOpts2
                myReportDocument.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                myReportDocument.Export()

                myReportDocument.Close()
                myReportDocument.Dispose()
                GC.Collect()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnGenPDF2(ByVal strCondemnID As String, ByVal strFullPath As String)
        Try
            Dim ds As New DataSet()
            ds = clsDisMng.fnDisMng_GetCondemnRecords4Rpt(strCondemnID)

            If ds.Tables(0).Rows.Count > 0 Then
                Dim myReportDocument As New ReportDocument()
                myReportDocument.Load(Server.MapPath("ReportFile\CondemnCert2.rpt"))
                myReportDocument.SetDataSource(ds.Tables(0))
                Dim DiskOpts2 As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions()
                myReportDocument.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile
                DiskOpts2.DiskFileName = strFullPath
                myReportDocument.ExportOptions.DestinationOptions = DiskOpts2
                myReportDocument.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                myReportDocument.Export()

                myReportDocument.Close()
                myReportDocument.Dispose()
                GC.Collect()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub butReject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReject.Click
        Try
            Dim intRetVal As Integer
            Dim strCertID As String = hdnCertID.Value

            ''*** Reject Certificate 
            intRetVal = clsDisMng.fnDisMng_CondemnCertReject( _
                            strCertID, Session("UsrID"), Trim(txtRejReason.Text))

            If intRetVal > 0 Then
                ''Generate Condemn PDF
                Dim strPath1 As String = gCondemnPath & "C" & strCertID & "_1b.pdf"
                Dim strPath2 As String = gCondemnPath & "C" & strCertID & "_2b.pdf"
                fnGenPDF1(strCertID, Server.MapPath(strPath1))
                fnGenPDF2(strCertID, Server.MapPath(strPath2))

                ''store Condemn Cert to eRegistry
                Dim strCertID1 As String = "C" & strCertID & "_1b.pdf" '""
                Dim strCertID2 As String = "C" & strCertID & "_2b.pdf" '""
                ''Check whether PDF File generated
                If File.Exists(Server.MapPath(strPath1)) And File.Exists(Server.MapPath(strPath2)) Then
                    ''Store Condemn Cert to eRegistry is going here...

                Else
                    lblErrorMessage.Text = "Certificate of Condemnation generate failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                    Exit Sub
                End If

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Certificate of Condemnation with S/No: " & lblCertSNo.Text & " have been reject.")

                Me.fnEmail("Rejected")
                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + "Certificate of Condemnation reject successfully." + "');" & _
                                "document.location.href='CRCert_Condemn.aspx?CertID=" & strCertID & "';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                lblErrorMessage.Text = "Certificate of Condemnation reject failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butDispose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDispose.Click
        Try
            Dim intRetVal As Integer
            Dim strCertID As String = hdnCertID.Value
            Dim strCDispName As String = Trim(cbCompDisp.Text)
            Dim strCDispLID As String = ""
            Dim strCDispDesg As String = Trim(txtCDispDesg.Text)

            ''*** Checking For Completion Disposal
            If strCDispName <> "" Then
                Dim strUsrExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(strCDispName, strCDispLID, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Completion Disposal not found. Please select an existing Completion Disposal."
                    lblErrorMessage.Visible = True
                    cbCompDisp.Focus()
                    Exit Sub
                End If
            End If

            ''*** Dispose Certificate 
            intRetVal = clsDisMng.fnDisMng_CondemnCertDispose( _
                            strCertID, strCDispLID, strCDispDesg, Session("UsrID"))

            If intRetVal > 0 Then
                ''Generate Condemn PDF
                Dim strPath1 As String = gCondemnPath & "C" & strCertID & "_1c.pdf"
                Dim strPath2 As String = gCondemnPath & "C" & strCertID & "_2c.pdf"
                fnGenPDF1(strCertID, Server.MapPath(strPath1))
                fnGenPDF2(strCertID, Server.MapPath(strPath2))

                ''store Condemn Cert to eRegistry
                Dim strCertID1 As String = "C" & strCertID & "_1c.pdf" '""
                Dim strCertID2 As String = "C" & strCertID & "_2c.pdf" '""
                ''Check whether PDF File generated
                If File.Exists(Server.MapPath(strPath1)) And File.Exists(Server.MapPath(strPath2)) Then
                    ''Store Condemn Cert to eRegistry is going here...

                Else
                    lblErrorMessage.Text = "Certificate of Condemnation generate failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                    Exit Sub
                End If

                ''Send Email to CPD Finance 
                fnEmail4CPFFinance(strCertID, strCertID1, strCertID2)

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Certificate of Condemnation with S/No: " & lblCertSNo.Text & " have been dispose.")

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + "Certificate of Condemnation dispose successfully." + "');" & _
                                "document.location.href='CRCert_Condemn.aspx?CertID=" & strCertID & "';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                lblErrorMessage.Text = "Certificate of Condemnation dispose failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

    Public Sub fnEmail(ByVal Type As String)
        Dim objRdr As SqlDataReader
        Try
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""

            Dim strEmailTo As String = ""
            Dim strEmailCC As String = ""
            Dim strMailBody As String = ""
            Dim dtFile As DataTable

            ''*** Get Email Details from database
            objRdr = clsDisMng.fnDisMng_GetCondemnRequesterEmail(hdnCertID.Value)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()

                    strMailSubject = "[" & Type & "] Condemnation S/No:" & lblCertSNo.Text
                    strEmailTo = CStr(objRdr("fld_email"))

                    ''*** If Email Active, checking for send email
                    ''--> remove duplicate email ids in the To List and CC List.
                    'strEmailTo = fnRemoveNameFromEmailAdd(strEmailTo)
                    'strEmailTo = fnRemoveDuplicates(strEmailTo, ";")
                    'strEmailCC = fnRemoveNameFromEmailAdd(strEmailCC)
                    'strEmailCC = fnRemoveDuplicates2(strEmailTo, strEmailCC, ";")

                    ''--> Get Email Body
                    strMailBody = "Dear Sir/Madam,<br><br><br>"
                    If Type = "Approved" Then
                        strMailBody += "The above condemnation request has been approved.<br><br>"
                    Else
                        strMailBody += "The above condemnation request has been rejected.<br><br>"
                        strMailBody += "Reason for Reject:-<br> "
                        strMailBody += Me.txtRejReason.Text.Replace(vbCrLf, "<br>")
                    End If


                    strMailBody += "<BR><br><br>Thanks and Regards,"
                    strMailBody += "<BR>File and Asset Management System"
                    strMailBody += "<BR><br><br>* This is a computer generated e-mail, please do not reply to it."

                    ''--> Send Email
                    If Trim(strEmailTo) <> "" Or Trim(strEmailCC) <> "" Then
                        fnSendEmail(strEmailTo, strEmailCC, "", strMailSubject, strMailBody, "", "H")
                    End If

                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub
End Class