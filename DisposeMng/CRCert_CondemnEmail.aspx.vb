#Region "Information Section"
' ****************************************************************************************************
' Description       : Condemnation Cert 
' Purpose           : Condemnation Cert Information
' Date              : 13/11/2008
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
#End Region

Partial Public Class CRCert_CondemnEmail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            ''********** End  : Check Session Time out ***********

            If Not Page.IsPostBack Then
                Me.butApprove.Attributes.Add("OnClick", "return fnConfirmRec('A')")
                Me.butReject.Attributes.Add("OnClick", "return fnConfirmRec('R')")

                ''Get Default Value
                hdnCertID.Value = Request("CertID")

                ''get Window Login ID
                Dim arrWin As Array
                If Not Request.ServerVariables("logon_user") Is Nothing Then
                    arrWin = Request.ServerVariables("logon_user").Split("\")

                    '****** For Showing Login Details *******

                    'Dim UsrID As String
                    'UsrID = Request("loginid")
                    'If UsrID = "" And arrWin(1).ToString() = "" Then
                    If (arrWin(1).ToString() <> "") Then
                        'Response.Redirect("login.asp")
                        If fnCheckLogin(arrWin(1), "") = False Then
                            Dim strJavaScript As String = ""
                            strJavaScript = "<script language = 'Javascript'>" & _
                                            "parent.location.href='../common/logout.aspx';" & _
                                            "</script>"
                            Response.Write(strJavaScript)
                            Exit Sub
                        End If
                    End If

                Else
                    Dim strJavaScript As String = ""
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "parent.location.href='../common/logout.aspx';" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                    Exit Sub
                End If
                Dim strLoginId As String = arrWin(1)
                'Dim strLoginId As String = Session("LoginID")
                ''checking for "just approving officer allow to approve/reject"
                If Not clsDisMng.fnDisMng_CondemnCheckAppOff("0", strLoginId, hdnCertID.Value, "E") Then
                    Response.Redirect("CRCert_NotAuthorize.aspx?CallFrm=C")
                    Exit Sub
                End If

                ''Display Record
                fnPopulateRecords()
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            Dim strStatus As String = ""
            Dim strPath1a As String = gCondemnPath & "C" & hdnCertID.Value & "_1a.pdf"
            Dim strPath1b As String = gCondemnPath & "C" & hdnCertID.Value & "_1b.pdf"
            Dim strPath1c As String = gCondemnPath & "C" & hdnCertID.Value & "_1c.pdf"
            Dim strPath2a As String = gCondemnPath & "C" & hdnCertID.Value & "_2a.pdf"
            Dim strPath2b As String = gCondemnPath & "C" & hdnCertID.Value & "_2b.pdf"
            Dim strPath2c As String = gCondemnPath & "C" & hdnCertID.Value & "_2c.pdf"
            Dim dtFile As DataTable


            ''Get Certificate Records
            Dim objRdr As SqlDataReader
            objRdr = clsDisMng.fnDisMng_GetCondemnRecordsDR(hdnCertID.Value, "M")
            If Not objRdr Is Nothing Then

                dtFile = New DataTable()
                dtFile = clsDisMng.fnCondemnFileGetRec(0, hdnCertID.Value, "N").Tables(0)

                For i As Integer = 0 To dtFile.Rows.Count - 1
                    spanOtherSupportingDoc.InnerHtml += "<a id=hlFile href='" & gCondemnPath & dtFile.Rows(i)("fld_FileSaveName").ToString() & "' target=CondemnFile3 >" & _
                                                 "View " & dtFile.Rows(i)("fld_FileName").ToString() & _
                                                 "</a>, "
                Next

                If objRdr.HasRows Then
                    objRdr.Read()
                    lblCertSNo.Text = CStr(objRdr("fld_CDSNo"))
                    strStatus = CStr(objRdr("fld_CDCertStatus"))

                    If strStatus = "P" Then
                        lblCertStatus.Text = "Pending"

                        spanCertLink.InnerHtml = "<a id=hlFile href='" & strPath1a & "' target=CondemnFile1 >" & _
                                                 "View certificate Part 1" & _
                                                 "</a>, " & _
                                                 "<a id=hlFile href='" & strPath2a & "' target=CondemnFile2 >" & _
                                                 "View certificate Part 2" & _
                                                 "</a>"

                        divCompDisposalView.Visible = False
                        divReasonRejectField.Visible = True
                        divReasonRejectView.Visible = False
                        butApprove.Visible = True
                        butReject.Visible = True

                    ElseIf strStatus = "A" Then
                        lblCertStatus.Text = "Approved"

                        spanCertLink.InnerHtml = "<a id=hlFile href='" & strPath1b & "' target=CondemnFile1 >" & _
                                                 "View certificate Part 1" & _
                                                 "</a>, " & _
                                                 "<a id=hlFile href='" & strPath2b & "' target=CondemnFile2 >" & _
                                                 "View certificate Part 2" & _
                                                 "</a>"

                        divCompDisposalView.Visible = False
                        divReasonRejectField.Visible = False
                        divReasonRejectView.Visible = False
                        butApprove.Visible = False
                        butReject.Visible = False

                    ElseIf strStatus = "R" Then
                        lblCertStatus.Text = "Reject"

                        spanCertLink.InnerHtml = "<a id=hlFile href='" & strPath1b & "' target=CondemnFile1 >" & _
                                                 "View certificate Part 1" & _
                                                 "</a>, " & _
                                                 "<a id=hlFile href='" & strPath2b & "' target=CondemnFile2 >" & _
                                                 "View certificate Part 2" & _
                                                 "</a>"

                        divCompDisposalView.Visible = False
                        divReasonRejectField.Visible = False
                        divReasonRejectView.Visible = True
                        butApprove.Visible = False
                        butReject.Visible = False

                    ElseIf strStatus = "D" Then
                        lblCertStatus.Text = "Disposed"

                        spanCertLink.InnerHtml = "<a id=hlFile href='" & strPath1c & "' target=CondemnFile1 >" & _
                                                 "View certificate Part 1" & _
                                                 "</a>, " & _
                                                 "<a id=hlFile href='" & strPath2c & "' target=CondemnFile2 >" & _
                                                 "View certificate Part 2" & _
                                                 "</a>"

                        divCompDisposalView.Visible = True
                        divReasonRejectField.Visible = False
                        divReasonRejectView.Visible = False
                        butApprove.Visible = False
                        butReject.Visible = False
                    End If

                    lblDept.Text = CStr(objRdr("fld_DeptName")) & " (" & CStr(objRdr("fld_DeptCode")) & ")"
                    lblTotNBV.Text = CStr(objRdr("fld_TotNBV"))
                    lblCondemnReason.Text = CStr(objRdr("fld_CondemnReason"))
                    lblDisposalMethod.Text = CStr(objRdr("fld_DisposalMethod"))
                    lblTotProceed.Text = CStr(objRdr("fld_TotProceed"))
                    lblTotRemovalC.Text = CStr(objRdr("fld_TotRemovalCost"))

                    lblAppOff.Text = CStr(objRdr("fld_AppOffName"))
                    lblAODesg.Text = CStr(objRdr("fld_AppOffDesg"))
                    lblAODt.Text = fnCheckNull(objRdr("fld_AppOffDt"))

                    lblRequestor.Text = CStr(objRdr("fld_RequestorName"))
                    lblReqDesg.Text = CStr(objRdr("fld_RequestorDesg"))
                    lblReqDt.Text = CStr(objRdr("fld_RequestorDt"))

                    lblCompDisp.Text = fnCheckNull(objRdr("fld_CDisposeName"))
                    lblCDispDesg.Text = fnCheckNull(objRdr("fld_CDisposeDesg"))
                    lblCDispDt.Text = fnCheckNull(objRdr("fld_CDisposeDt"))

                    lblRejReason.Text = Replace(fnCheckNull(objRdr("fld_RejectReason")), vbCrLf, "<br>") 'fnCheckNull(objRdr("fld_RejectReason"))

                End If
            End If
            objRdr.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub butApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butApprove.Click
        Try
            Dim intRetVal As Integer
            Dim strCertID As String = hdnCertID.Value

            ''*** Approve Certificate 
            intRetVal = clsDisMng.fnDisMng_CondemnCertApproveEmail( _
                            strCertID, "0")

            If intRetVal > 0 Then
                ''Generate Condemn PDF
                Dim strPath1 As String = gCondemnPath & "C" & strCertID & "_1b.pdf"
                Dim strPath2 As String = gCondemnPath & "C" & strCertID & "_2b.pdf"
                fnGenPDF1(strCertID, Server.MapPath(strPath1))
                fnGenPDF2(strCertID, Server.MapPath(strPath2))

                ''store Condemn Cert to eRegistry
                Dim strCertID1 As String = "C" & strCertID & "_1b.pdf" '""
                Dim strCertID2 As String = "C" & strCertID & "_2b.pdf" '""
                ''Check whether PDF File generated
                If File.Exists(Server.MapPath(strPath1)) And File.Exists(Server.MapPath(strPath2)) Then
                    ''Store Condemn Cert to eRegistry is going here...

                Else
                    lblErrorMessage.Text = "Certificate of Condemnation generate failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                    Exit Sub
                End If

                Me.fnEmail("Approved")
                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Certificate of Condemnation with S/No: " & lblCertSNo.Text & " has been approved.")

                 ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + "Certificate of Condemnation approved successfully." + "');" & _
                                "document.location.href='" & gCondemnAppPage & strCertID & "';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                lblErrorMessage.Text = "Certificate of Condemnation approve failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butReject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReject.Click
        Try
            Dim intRetVal As Integer
            Dim strCertID As String = hdnCertID.Value

            ''*** Reject Certificate 
            intRetVal = clsDisMng.fnDisMng_CondemnCertRejectEmail( _
                            strCertID, "0", Trim(txtRejReason.Text))

            If intRetVal > 0 Then
                ''Generate Condemn PDF
                Dim strPath1 As String = gCondemnPath & "C" & strCertID & "_1b.pdf"
                Dim strPath2 As String = gCondemnPath & "C" & strCertID & "_2b.pdf"
                fnGenPDF1(strCertID, Server.MapPath(strPath1))
                fnGenPDF2(strCertID, Server.MapPath(strPath2))

                ''store Condemn Cert to eRegistry
                Dim strCertID1 As String = "C" & strCertID & "_1b.pdf" '""
                Dim strCertID2 As String = "C" & strCertID & "_2b.pdf" '""
                ''Check whether PDF File generated
                If File.Exists(Server.MapPath(strPath1)) And File.Exists(Server.MapPath(strPath2)) Then
                    ''Store Condemn Cert to eRegistry is going here...

                Else
                    lblErrorMessage.Text = "Certificate of Condemnation generate failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                    Exit Sub
                End If

                Me.fnEmail("Rejected")
                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Certificate of Condemnation with S/No: " & lblCertSNo.Text & " has been rejected.")
                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + "Certificate of Condemnation reject successfully." + "');" & _
                                "document.location.href='" & gCondemnAppPage & strCertID & "';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                lblErrorMessage.Text = "Certificate of Condemnation reject failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnGenPDF1(ByVal strCondemnID As String, ByVal strFullPath As String)
        Try
            Dim ds As New DataSet("tbl_CondemnMst")
            ds = clsDisMng.fnDisMng_GetCondemnRecords(strCondemnID, "M")

            If ds.Tables(0).Rows.Count > 0 Then
                Dim myReportDocument As New ReportDocument()
                myReportDocument.Load(Server.MapPath("ReportFile\CondemnCert.rpt"))
                myReportDocument.SetDataSource(ds.Tables(0))
                myReportDocument.SetParameterValue("CondemnID", strCondemnID)
                Dim DiskOpts2 As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions()
                myReportDocument.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile
                DiskOpts2.DiskFileName = strFullPath
                myReportDocument.ExportOptions.DestinationOptions = DiskOpts2
                myReportDocument.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                myReportDocument.Export()

                myReportDocument.Close()
                myReportDocument.Dispose()
                GC.Collect()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnGenPDF2(ByVal strCondemnID As String, ByVal strFullPath As String)
        Try
            Dim ds As New DataSet()
            ds = clsDisMng.fnDisMng_GetCondemnRecords4Rpt(strCondemnID)

            If ds.Tables(0).Rows.Count > 0 Then
                Dim myReportDocument As New ReportDocument()
                myReportDocument.Load(Server.MapPath("ReportFile\CondemnCert2.rpt"))
                myReportDocument.SetDataSource(ds.Tables(0))
                Dim DiskOpts2 As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions()
                myReportDocument.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile
                DiskOpts2.DiskFileName = strFullPath
                myReportDocument.ExportOptions.DestinationOptions = DiskOpts2
                myReportDocument.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                myReportDocument.Export()

                myReportDocument.Close()
                myReportDocument.Dispose()
                GC.Collect()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function fnCheckLogin(ByVal strLoginID As String, _
                             ByVal strPwd As String) As Boolean
        Try
            Dim intUsrId As Integer = "0"
            Dim strUsrName As String = ""
            Dim strAccess As String = ""
            Dim strLSuccessF As String = ""
            Dim strAdminF As String = ""
            Dim intLoginUsrID As Integer = "0"

            clsCommon.fnLoginValidation(strLoginID, strPwd, intUsrId, strUsrName, _
                                        strAccess, strLSuccessF, strAdminF, intLoginUsrID)
            If strLSuccessF = "Y" Then ''Login Successfully
                Session("Name") = strUsrName            '' Full Name of the User
                Session("UsrID") = intUsrId             '' User Id in Database
                Session("LoginID") = strLoginID         '' Login User Id   'UsrMng|grp|delete,&nbsp;
                Session("AR") = strAccess
                Session("AdminF") = strAdminF
                Session("LoginUsersID") = intLoginUsrID

                ''**** Insert Audit Trail
                '   clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "User Login")

                Return True
                'Server.Transfer("index.aspx")
            Else
                Return False
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function


    Public Sub fnEmail(ByVal Type As String)
        Try
            Dim objRdr As SqlDataReader
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""

            Dim strEmailTo As String = ""
            Dim strEmailCC As String = ""
            Dim strMailBody As String = ""
            Dim dtFile As DataTable

            ''*** Get Email Details from database
            objRdr = clsDisMng.fnDisMng_GetCondemnRequesterEmail(hdnCertID.Value)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()

                    strMailSubject = "[" & Type & "] Certificate of Condemnation S/No:" & lblCertSNo.Text
                    strEmailTo = CStr(objRdr("fld_email"))

                    ''*** If Email Active, checking for send email
                    ''--> remove duplicate email ids in the To List and CC List.
                    'strEmailTo = fnRemoveNameFromEmailAdd(strEmailTo)
                    'strEmailTo = fnRemoveDuplicates(strEmailTo, ";")
                    'strEmailCC = fnRemoveNameFromEmailAdd(strEmailCC)
                    'strEmailCC = fnRemoveDuplicates2(strEmailTo, strEmailCC, ";")

                    ''--> Get Email Body
                    strMailBody = "Dear Sir/Madam,<br><br><br>"
                    If Type = "Approved" Then
                        strMailBody += "The above Certificate of Condemnation has been approved.<br><br>"
                    Else
                        strMailBody += "The above Certificate of Condemnation has been rejected.<br><br>"
                        strMailBody += "Reason for Reject:-<br> "
                        strMailBody += Me.txtRejReason.Text.Replace(vbCrLf, "<br>")
                    End If
                

                    strMailBody += "<BR><br><br>Thanks and Regards,"
                    strMailBody += "<BR>File and Asset Management System"
                    strMailBody += "<BR><br><br>* This is a computer generated e-mail, please do not reply to it."

                    ''--> Send Email
                    If Trim(strEmailTo) <> "" Or Trim(strEmailCC) <> "" Then
                        fnSendEmail(strEmailTo, strEmailCC, "", strMailSubject, strMailBody, "", "H")
                    End If

                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class