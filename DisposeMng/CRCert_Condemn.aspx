<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CRCert_Condemn.aspx.vb" Inherits="AMS.CRCert_Condemn" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<%@ Register TagPrefix="fluent" Namespace="Fluent.MultiLineTextBoxValidator" Assembly="Fluent.MultiLineTextBoxValidator" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet" />
	<script language="JavaScript" type="text/javascript" src="../Common/CommonJS.js"></script>	
	<link href="../common/ComboBox/styles/contactsearch/combo.css" type="text/css" rel="Stylesheet" />
    <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
    <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />

	<script type="text/javascript">

	    	    $(document).ready(function () {
	    	        SearchText("cbCompDisp", "hfCompDisp");
	    	    });
	    	   
	    	    function SearchText(obj1, obj2) {
	    	        $("#" + obj1).autocomplete({
	    	            source: function (request, response) {
	    	                $.ajax({
	    	                    url: '<%#ResolveUrl("~/DisposeMng/CRCert_Condemn.aspx/GetUsersList") %>',
	    	                    data: "{ 'prefix': '" + request.term + "'}",
	    	                    dataType: "json",
	    	                    type: "POST",
	    	                    contentType: "application/json; charset=utf-8",
	    	                    success: function (data) {
	    	                        response($.map(data.d, function (item) {
	    	                            return {
	    	                                label: item.split('~')[0],
	    	                                val: item.split('~')[1]

	    	                            }  


	    	                        }))
	    	                    },
	    	                    error: function (response) {
	    	                        alert(response.responseText);
	    	                    },
	    	                    failure: function (response) {
	    	                        alert(response.responseText);
	    	                    }
	    	                });
	    	            },
	    	            select: function (e, i) {
	    	                $("#" + obj2).val(i.item.val);
	    	            },
	    	            minLength: 0
	    	        });

	    	    }

	function fnConfirmRec(CallType) {
	            var foundError = false;
				var flag, confirmAction;
				flag = false;
				
				//Get Error Message for related action
				//--> (A)pprove, (R)eject, (D)ispose
				switch(CallType)    
				{
					case 'A':
						confirmAction = "Are you sure you want to approve this certificate?"; 
						break
						
					case 'R':
						confirmAction = "Are you sure you want to reject this certificate?"; 
						break
						
				    case 'D':
						confirmAction = "Are you sure you want to dispose this certificate?"; 
						break
				}
								
				if (CallType == 'R'){
				    //validate Reasons for Reject
				    if (!foundError && gfnIsFieldBlank(document.Frm_RedCodemn.txtRejReason)) {
				        foundError=true;
				        document.Frm_RedCodemn.txtRejReason.focus();
				        alert("Please enter Reasons for Reject.");
			        }
				}
				
				if (CallType == 'D'){
    			    //validate Completion Disposal
    				if (!foundError && gfnIsFieldBlankValue(document.getElementById("cbCompDisp").object.GetTextValue())) {
    					foundError=true;
    					alert("Please select Completion Disposal.");
    				}
    				
    				//validate Completion Disposal - Designation
    				if (!foundError && gfnIsFieldBlank(document.Frm_RedCodemn.txtCDispDesg)) {
    				    foundError=true;
    				    document.Frm_RedCodemn.txtCDispDesg.focus();
  	    			    alert("Please enter Completion Disposal Designation.");
    			    }  	
                    		        
                    //		        //validate Completion Disposal - date
                    //			    if (!foundError && gfnCheckDate(document.Frm_RedCodemn.txtCDispDt, "Completion Disposal Date", "M") == false) {
                    //				    foundError=true
                    //			    }
				}
				
				if (!foundError){
 					flag = window.confirm(confirmAction);
				    return flag;
 				}
				else
					return false;
				
		}
	</script>
</head>
<body>
    <form id="Frm_RedCodemn" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Dispose Management : Condemnation Certificate</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" height="400" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table3">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table4">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table5">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table6">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table7">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table9">
																				<TBODY align=left>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Certificate S/No : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:Label ID=lblCertSNo runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Certificate Status : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:Label ID=lblCertStatus runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Certificate File : </FONT>
																					    </TD>
																					    <TD>
																						    <span id=spanCertLink runat=server></span>
																					    </TD>
																				    </TR>

                                                                                    <TR>
																						<TD ><FONT class="DisplayTitle">Other Supporting Document File :</FONT> </TD>
                                                                                        <TD ><span id=spanOtherSupportingDoc runat=server></span></TD>
																					</TR>

																				    <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>


																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Department : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:Label ID=lblDept runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD  width="35%">
																					        <FONT class="DisplayTitle">Total Net Book Value [NBV] (S$) : </FONT>
																					    </TD>
																					    <TD width="65%">
																					        <asp:Label ID=lblTotNBV runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">If item is condemned for reasons other than fair wear and tear state reason : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblCondemnReason runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Recommended method of disposal : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblDisposalMethod runat=server></asp:Label></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Total Proceeds (S$) : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:Label ID=lblTotProceed runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Total Removal Cost (S$) : </FONT>
																					    </TD>
																					    <TD><asp:Label ID=lblTotRemovalC runat=server></asp:Label></TD>
																				    </TR>
																				    <TR><TD colSpan="2">&nbsp;</TD></TR>
																				    <TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Approving Officer Information</font></TD>
																					</TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Approving Officer : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:Label ID=lblAppOff runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Designation : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:Label ID=lblAODesg runat=server></asp:Label>
																					     </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Date : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:Label ID=lblAODt runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR><TD colSpan="2">&nbsp;</TD></TR>
																				    <TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Requestor Information</font></TD>
																					</TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Requestor : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:Label ID=lblRequestor runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Designation : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:Label ID=lblReqDesg runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Date : </FONT>
																					    </TD>
																					    <TD>    
																					        <asp:Label ID=lblReqDt runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    
																				    <TR>
																						<TD colSpan="2">
																						    <div id=divCompDisposalView runat=server>
																						    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table2">
																				                <TBODY align=left>
																				                    <TR><TD colSpan="2">&nbsp;</TD></TR>
																				                    <TR>
																						                <TD colSpan="2"><font class="DisplayTitleHeader">Completion Disposal Information</font></TD>
																					                </TR>
																				                    <TR>
																					                    <TD width="35%">
																					                        <FONT class="DisplayTitle">Completion Disposal : </FONT>
																					                    </TD>
																					                    <TD width="65%">
																					                        <asp:Label ID=lblCompDisp runat=server></asp:Label>
																					                    </TD>
																				                    </TR>
																				                    <TR>
																					                    <TD>
																					                        <FONT class="DisplayTitle">Designation : </FONT>
																					                    </TD>
																					                    <TD>
																					                         <asp:Label ID=lblCDispDesg runat=server></asp:Label>
																					                    </TD>
																				                    </TR>
																				                    <TR>
																					                    <TD>
																						                    <FONT class="DisplayTitle">Date : </FONT>
																					                    </TD>
																					                    <TD>
																					                        <asp:Label ID=lblCDispDt runat=server></asp:Label>
																					                    </TD>
																				                    </TR>
																				                </TBODY>
																				            </TABLE>
																				            </div>
																				            
																				            <div id=divCompDisposalField runat=server>
																				            <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table10">
																				                <TBODY align=left>
																				                    <TR><TD colSpan="2">&nbsp;</TD></TR>
																				                    <TR>
																						                <TD colSpan="2"><font class="DisplayTitleHeader">Completion Disposal Information</font></TD>
																					                </TR>
																				                    <TR>
																					                    <TD width="35%">
																					                        <font color="red">*</font><FONT class="DisplayTitle">Completion Disposal : </FONT>
																					                    </TD>
																					                    <TD width="65%">
																						                    
                                                                                            <asp:TextBox ID="cbCompDisp" runat="server" Width="300px"></asp:TextBox>
																					                    </TD>
																				                    </TR>
																				                    <TR>
																					                    <TD>
																					                        <FONT class="DisplayTitle"><font color="red">*</font>Designation : </FONT>
																					                    </TD>
																					                    <TD><asp:TextBox id="txtCDispDesg" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				                    </TR>
																				                </TBODY>
																				            </TABLE>
																				            </div>
																				            
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																						    <div id=divReasonRejectField runat=server>
																						    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table11">
																				                <TBODY align=left>
																				                    <TR><TD colSpan="2">&nbsp;</TD></TR>
																				                    <TR>
																					                    <TD valign="top" width="35%"><FONT class="DisplayTitle">Reasons for Reject : </FONT>
																					                    </TD>
																					                    <TD width="65%">
																					                        <asp:TextBox id="txtRejReason" maxlength="1000" TextMode="MultiLine" Columns="40" Rows="5" Runat="server"></asp:TextBox><br />
																					                        <asp:textbox id="txtRejReasonWord" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					                        <fluent:multilinetextboxvalidator id="MLLReason" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Reason to 1000 Characters." OutputControl="txtRejReasonWord" ControlToValidate="txtRejReason"></fluent:multilinetextboxvalidator>
																					                    </TD>
																				                    </TR>
																				                </TBODY>
																				            </TABLE>
																				            </div>
																				            
																				            <div id=divReasonRejectView runat=server>
																						    <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table12">
																				                <TBODY align=left>
																				                    <TR><TD colSpan="2">&nbsp;</TD></TR>
																				                    <TR>
																					                    <TD valign="top" width="35%"><FONT class="DisplayTitle">Reasons for Reject : </FONT>
																					                    </TD>
																					                    <TD width="65%">
																					                        <asp:Label id=lblRejReason runat=server></asp:Label>
																					                    </TD>
																				                    </TR>
																				                </TBODY>
																				            </TABLE>
																				            </div>
																				         </TD>
																				    </TR>
																				    <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:Button id="butApprove" Text="Approve" Runat="Server" />&nbsp;
																							<asp:Button id="butReject" Text="Reject" Runat="Server" />&nbsp;
																							<asp:Button id="butDispose" Text="Dispose" Runat="Server" />
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnCertID" runat="server">
			<input type="hidden" id="hdnCertStatus" runat="server">
            <asp:HiddenField ID="hfCompDisp" runat="server" />

			<!-- End  : Hidden Fields -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
    </form>
</body>
</html>
