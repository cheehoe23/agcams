'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class CRCert_Redundancy

    '''<summary>
    '''Frm_RedCodemn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Frm_RedCodemn As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''lblErrorMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblErrorMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCertSNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCertSNo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCertStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCertStatus As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''spanCertLink control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents spanCertLink As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''spanOtherSupportingDoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents spanOtherSupportingDoc As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lblDept control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDept As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRevParty control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRevParty As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTotNBV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotNBV As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDispMethod control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDispMethod As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblReason control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblReason As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAppOff control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAppOff As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAODesg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAODesg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAODt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAODt As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRequestor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRequestor As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblReqDesg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblReqDesg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblReqDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblReqDt As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblMOFSysApp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMOFSysApp As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblMOFName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMOFName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblMOFDesg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMOFDesg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblMOFEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMOFEmail As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblMOFDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMOFDt As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''divCompDisposalView control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divCompDisposalView As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lblCDispName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCDispName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCDispDesg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCDispDesg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCDispDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCDispDt As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''divCompDisposalField control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divCompDisposalField As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''cbCompDisp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbCompDisp As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCDispDesg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCDispDesg As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''divReceiverView control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divReceiverView As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lblRevName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRevName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRevDesg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRevDesg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRevDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRevDt As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRevEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRevEmail As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''divReceiverField control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divReceiverField As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''txtRevName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRevName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtRevDesg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRevDesg As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtRevDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRevDt As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtRevEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRevEmail As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''divReasonRejectField control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divReasonRejectField As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''txtRejReason control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRejReason As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtRejReasonWord control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRejReasonWord As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''MLLReason control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents MLLReason As Global.Fluent.MultiLineTextBoxValidator.MultiLineTextBoxValidator

    '''<summary>
    '''divReasonRejectView control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divReasonRejectView As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lblRejReason control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRejReason As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''butApprove control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents butApprove As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''butReject control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents butReject As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''butDispose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents butDispose As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''hdnCertID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnCertID As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnDipMethod control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnDipMethod As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnCertStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnCertStatus As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnMore5hkF control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnMore5hkF As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hfCompDisp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfCompDisp As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Footer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Footer As Global.AMS.footer_cr
End Class
