<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DisSettings.aspx.vb" Inherits="AMS.DisSettings" ValidateRequest="false" %>

<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<%@ Register TagPrefix="fluent" Namespace="Fluent.MultiLineTextBoxValidator" Assembly="Fluent.MultiLineTextBoxValidator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet" />
	<script language="JavaScript" type="text/javascript" src="../Common/CommonJS.js"></script>
    <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>
    <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />    
	<link href="../common/ComboBox/styles/contactsearch/combo.css" type="text/css" rel="Stylesheet" />
	<script language="javascript">
	    

		    function chkFrm() {
				var foundError = false;
				

				//validate Condemn - Approving Officer
				//--> Checking Condemn - Approving Officer cannot be blank
				if (!foundError && gfnIsFieldBlankValue(document.getElementById("cbCDAppOff").object.GetTextValue())) {
					foundError=true;
					alert("Please select Approving Officer (Condemnation Settings).");
				}
				
				//validate Condemn - CPD-Finance
				//--> Checking Condemn - CPD-Finance Email cannot be blank
				if (!foundError && gfnIsFieldBlankValue(document.getElementById("cbCDCPDFinance").object.GetTextValue())) {
					foundError=true;
					alert("Please enter CPD-Finance Email (Condemnation Settings).");
				}
			
			    //validate for Condemn - department
			    if (!foundError && document.frm_DisSet.ddlCDDept.value == "-1") {
				    foundError=true
				    document.frm_DisSet.ddlCDDept.focus()
				    alert("Please select the Department (Condemnation Settings).")
			    }
					
				//validate Email for Approving Officer
				//--> Checking subject cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_DisSet.txtCDAOSubj)) {
					foundError=true;
					document.frm_DisSet.txtCDAOSubj.focus();
					alert("Please enter Email Subject for Approving Officer (Condemnation Settings).");
				}
				if (!foundError && document.frm_DisSet.txtCDAOSubj.value.length > 250) {
				    foundError=true
				    document.frm_DisSet.txtCDAOSubj.focus()
				    alert("Please make sure the Email Subject for Approving Officer (Condemnation Settings) is less than 250 characters long.")
			    }
				         
	            //--> Checking Content cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_DisSet.txtCDAOContent)) {
					foundError=true;
					document.frm_DisSet.txtCDAOContent.focus();
					alert("Please enter Email Content for Approving Officer (Condemnation Settings).");
				}
				if (!foundError && document.frm_DisSet.txtCDAOContent.value.length > 1000) {
				    foundError=true
				    document.frm_DisSet.txtCDAOContent.focus()
				    alert("Please make sure the Email Content for Approving Officer (Condemnation Settings) is less than 1000 characters long.")
			    }
			    
			    //validate Email for CPF-Finance
				//--> Checking subject cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_DisSet.txtCDCFSubj)) {
					foundError=true;
					document.frm_DisSet.txtCDCFSubj.focus();
					alert("Please enter Email Subject for CPD-Finance (Condemnation Settings).");
				}
				if (!foundError && document.frm_DisSet.txtCDCFSubj.value.length > 250) {
				    foundError=true
				    document.frm_DisSet.txtCDCFSubj.focus()
				    alert("Please make sure the Email Subject for CPD-Finance (Condemnation Settings) is less than 250 characters long.")
			    }
				         
	            //--> Checking Content cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_DisSet.txtCDCFContent)) {
					foundError=true;
					document.frm_DisSet.txtCDCFContent.focus();
					alert("Please enter Email Content for CPD-Finance (Condemnation Settings).");
				}
				if (!foundError && document.frm_DisSet.txtCDCFContent.value.length > 1000) {
				    foundError=true
				    document.frm_DisSet.txtCDCFContent.focus()
				    alert("Please make sure the Email Content for CPD-Finance (Condemnation Settings) is less than 1000 characters long.")
			    }
			    
			    //validate Redundancy - Approving Officer
				//--> Checking Redundancy - Approving Officer cannot be blank
				if (!foundError && gfnIsFieldBlankValue(document.getElementById("cbRDAppOff").object.GetTextValue())) {
					foundError=true;
					alert("Please select Approving Officer (Redundancy Settings).");
				}
				
				//validate Redundancy - CPD-Finance
				//--> Checking Redundancy - CPD-Finance Email cannot be blank
				if (!foundError && gfnIsFieldBlankValue(document.getElementById("cbRDCPDFinance").object.GetTextValue())) {
					foundError=true;
					alert("Please enter CPD-Finance Email (Redundancy Settings).");
				}
			
			    //validate for Redundancy - department
			    if (!foundError && document.frm_DisSet.ddlRDDept.value == "-1") {
				    foundError=true
				    document.frm_DisSet.ddlRDDept.focus()
				    alert("Please select the Department (Redundancy Settings).")
			    }
					
				//validate Email for Approving Officer
				//--> Checking subject cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_DisSet.txtRDAOSubj)) {
					foundError=true;
					document.frm_DisSet.txtRDAOSubj.focus();
					alert("Please enter Email Subject for Approving Officer (Redundancy Settings).");
				}
				if (!foundError && document.frm_DisSet.txtRDAOSubj.value.length > 250) {
				    foundError=true
				    document.frm_DisSet.txtRDAOSubj.focus()
				    alert("Please make sure the Email Subject for Approving Officer (Redundancy Settings) is less than 250 characters long.")
			    }
				         
	            //--> Checking Content cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_DisSet.txtRDAOContent)) {
					foundError=true;
					document.frm_DisSet.txtRDAOContent.focus();
					alert("Please enter Email Content for Approving Officer (Redundancy Settings).");
				}
				if (!foundError && document.frm_DisSet.txtRDAOContent.value.length > 1000) {
				    foundError=true
				    document.frm_DisSet.txtRDAOContent.focus()
				    alert("Please make sure the Email Content for Approving Officer (Redundancy Settings) is less than 1000 characters long.")
			    }
			    
			    //validate Email for CPF-Finance
				//--> Checking subject cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_DisSet.txtRDCFSubj)) {
					foundError=true;
					document.frm_DisSet.txtRDCFSubj.focus();
					alert("Please enter Email Subject for CPD-Finance (Redundancy Settings).");
				}
				if (!foundError && document.frm_DisSet.txtRDCFSubj.value.length > 250) {
				    foundError=true
				    document.frm_DisSet.txtRDCFSubj.focus()
				    alert("Please make sure the Email Subject for CPD-Finance (Redundancy Settings) is less than 250 characters long.")
			    }
				         
	            //--> Checking Content cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_DisSet.txtRDCFContent)) {
					foundError=true;
					document.frm_DisSet.txtRDCFContent.focus();
					alert("Please enter Email Content for CPD-Finance (Redundancy Settings).");
				}
				if (!foundError && document.frm_DisSet.txtRDCFContent.value.length > 1000) {
				    foundError=true
				    document.frm_DisSet.txtRDCFContent.focus()
				    alert("Please make sure the Email Content for CPD-Finance (Redundancy Settings) is less than 1000 characters long.")
			    }		
			    
			    //validate Email for MOF
				//--> Checking subject cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_DisSet.txtRDMOFSubj)) {
					foundError=true;
					document.frm_DisSet.txtRDMOFSubj.focus();
					alert("Please enter Email Subject for MOF (Redundancy Settings).");
				}
				if (!foundError && document.frm_DisSet.txtRDMOFSubj.value.length > 250) {
				    foundError=true
				    document.frm_DisSet.txtRDMOFSubj.focus()
				    alert("Please make sure the Email Subject for MOF (Redundancy Settings) is less than 250 characters long.")
			    }
			    
			    //--> Checking Content cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_DisSet.txtRDMOFContent)) {
					foundError=true;
					document.frm_DisSet.txtRDMOFContent.focus();
					alert("Please enter Email Content for MOF (Redundancy Settings).");
				}
				if (!foundError && document.frm_DisSet.txtRDMOFContent.value.length > 1000) {
				    foundError=true
				    document.frm_DisSet.txtRDMOFContent.focus()
				    alert("Please make sure the Email Content for MOF (Redundancy Settings) is less than 1000 characters long.")
			    }	
			    
			    //validate Email for Disposal
				//--> Checking subject cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_DisSet.txtRDDispSubj)) {
					foundError=true;
					document.frm_DisSet.txtRDDispSubj.focus();
					alert("Please enter Email Subject for Disposal (Redundancy Settings).");
				}
				if (!foundError && document.frm_DisSet.txtRDDispSubj.value.length > 250) {
				    foundError=true
				    document.frm_DisSet.txtRDDispSubj.focus()
				    alert("Please make sure the Email Subject for Disposal (Redundancy Settings) is less than 250 characters long.")
			    }
				         
	            //--> Checking Content cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_DisSet.txtRDDispContent)) {
					foundError=true;
					document.frm_DisSet.txtRDDispContent.focus();
					alert("Please enter Email Content for Disposal (Redundancy Settings).");
				}
				if (!foundError && document.frm_DisSet.txtRDDispContent.value.length > 1000) {
				    foundError=true
				    document.frm_DisSet.txtRDDispContent.focus()
				    alert("Please make sure the Email Content for Disposal (Redundancy Settings) is less than 1000 characters long.")
			    }	
			    
			    //validate Email for Receiver
				//--> Checking subject cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_DisSet.txtRDRevSubj)) {
					foundError=true;
					document.frm_DisSet.txtRDRevSubj.focus();
					alert("Please enter Email Subject for Receiver (Redundancy Settings).");
				}
				if (!foundError && document.frm_DisSet.txtRDRevSubj.value.length > 250) {
				    foundError=true
				    document.frm_DisSet.txtRDRevSubj.focus()
				    alert("Please make sure the Email Subject for Receiver (Redundancy Settings) is less than 250 characters long.")
			    }
				         
	            //--> Checking Content cannot be blank
				if (!foundError && gfnIsFieldBlank(document.frm_DisSet.txtRDRevContent)) {
					foundError=true;
					document.frm_DisSet.txtRDRevContent.focus();
					alert("Please enter Email Content for Receiver (Redundancy Settings).");
				}
				if (!foundError && document.frm_DisSet.txtRDRevContent.value.length > 1000) {
				    foundError=true
				    document.frm_DisSet.txtRDRevContent.focus()
				    alert("Please make sure the Email Content for Receiver (Redundancy Settings) is less than 1000 characters long.")
			    }						    		  	
		        
 				if (!foundError){
 					var flag = false;
 				    flag = window.confirm("Are you sure want to update this record?");
 				    return flag;
 				}
				else
					return false;
			}
	</script>
</head>
<body>
    <form id="frm_DisSet" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Administration : Disposal Setting</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table id="Table2" height="500" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<!--Start: Main Content-->
						<table id="Table3" height="400" cellSpacing="1" cellPadding="0" width="100%" align="left"
							bgColor="silver" border="0">
							<tr>
								<td>
									<table id="Table4" height="400" cellSpacing="0" cellPadding="0" width="100%" align="left"
										bgColor="white" border="0">
										<tr>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
													<TR>
														<TD vAlign="top" align="center" width="100%">
														    <asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE id="Table6" height="126" cellSpacing="0" cellPadding="3" width="100%" align="left"
																bgColor="white" border="0">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" height="352">
																			<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
																				<TBODY align=left>
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Settings for Condemnation</b></TD>
																					</TR>
																					<TR>
																						<TD vAlign=top width="30%">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Approving Officer : </FONT></TD>
																						<TD width="70%">                                                                                            
                                                                                            <asp:TextBox ID="cbCDAppOff" runat="server" Width="300px"></asp:TextBox>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign=top>
																						    <FONT class="DisplayTitle"><font color="red">*</font>CPD-Finance Email : </FONT></TD>
																						<TD>
																						    <asp:TextBox ID="cbCDCPDFinance" runat="server" autocomplete="off" 
                                                                                                Height="70px" TextMode="MultiLine" Width="335px" />
																						</TD>
																					</TR>
																					<TR>
																						<TD>
																						    <FONT class="DisplayTitle"><font color="red">*</font>Department : </FONT></TD>
																						<TD>
                                                                                            <asp:DropDownList ID="ddlCDDept" Runat="server"></asp:DropDownList>
																						</TD>
																					</TR>
																					<TR>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Email for Approving Officer</font></TD>
																					</TR>
																					<TR>
																						<TD><FONT class="DisplayTitle"><font color="red">*</font>Active? </FONT></TD>
																						<TD>
																						    <asp:radiobuttonlist id="rdCDAOActive" Runat="server" RepeatDirection="Horizontal">
																								<asp:ListItem Value="Y" Selected>Yes</asp:ListItem>
																								<asp:ListItem Value="N">No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Email Subject : </FONT></TD>
																						<TD>
																						    <asp:textbox id="txtCDAOSubj" Runat="server" maxlength="250" Width="335px"></asp:textbox></TD>
																					</TR>
																					<TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Email Content : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtCDAOContent" TextMode=MultiLine maxlength="1000" Columns="45" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtCDAOContentCount" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="MMLCDAOContent" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Email Content to 1000 Characters." OutputControl="txtCDAOContentCount" ControlToValidate="txtCDAOContent"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Email for CPD-Finance</font></TD>
																					</TR>
																					<TR>
																						<TD><FONT class="DisplayTitle"><font color="red">*</font>Active? </FONT></TD>
																						<TD>
																						    <asp:radiobuttonlist id="rdCDCFActive" Runat="server" RepeatDirection="Horizontal">
																								<asp:ListItem Value="Y" Selected>Yes</asp:ListItem>
																								<asp:ListItem Value="N">No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Email Subject : </FONT></TD>
																						<TD>
																						    <asp:textbox id="txtCDCFSubj" Runat="server" maxlength="250" Width="335px"></asp:textbox></TD>
																					</TR>
																					<TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Email Content : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtCDCFContent" TextMode=MultiLine maxlength="1000" Columns="45" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtCDCFContentCount" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="MMLCDCFContentCount" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Email Content to 1000 Characters." OutputControl="txtCDCFContentCount" ControlToValidate="txtCDCFContent"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																				    <TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Settings for Redundancy</b></TD>
																					</TR>
																					<TR>
																						<TD vAlign=top width="30%">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Approving Officer : </FONT></TD>
																						<TD width="70%">                                                                                            
                                                                                            <asp:TextBox ID="cbRDAppOff" runat="server" Width="300px"></asp:TextBox>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign=top>
																						    <FONT class="DisplayTitle"><font color="red">*</font>CPD-Finance Email : </FONT></TD>
																						<TD>
																						    <asp:TextBox ID="cbRDCPDFinance" runat="server" autocomplete="off" 
                                                                                                Height="70px" TextMode="MultiLine" Width="335px" />
																						</TD>
																					</TR>
																					<TR>
																						<TD>
																						    <FONT class="DisplayTitle"><font color="red">*</font>Department : </FONT></TD>
																						<TD>
                                                                                            <asp:DropDownList ID="ddlRDDept" Runat="server"></asp:DropDownList>
																						</TD>
																					</TR>
																					<TR>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Email for Approving Officer</font></TD>
																					</TR>
																					<TR>
																						<TD><FONT class="DisplayTitle"><font color="red">*</font>Active? </FONT></TD>
																						<TD>
																						    <asp:radiobuttonlist id="rdRDAOActive" Runat="server" RepeatDirection="Horizontal">
																								<asp:ListItem Value="Y" Selected>Yes</asp:ListItem>
																								<asp:ListItem Value="N">No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Email Subject : </FONT></TD>
																						<TD>
																						    <asp:textbox id="txtRDAOSubj" Runat="server" maxlength="250" Width="335px"></asp:textbox></TD>
																					</TR>
																					<TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Email Content : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtRDAOContent" TextMode=MultiLine maxlength="1000" Columns="45" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtRDAOContentCount" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="MMLRDAOContent" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Email Content to 1000 Characters." OutputControl="txtRDAOContentCount" ControlToValidate="txtRDAOContent"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Email for CPD-Finance</font></TD>
																					</TR>
																					<TR>
																						<TD><FONT class="DisplayTitle"><font color="red">*</font>Active? </FONT></TD>
																						<TD>
																						    <asp:radiobuttonlist id="rdRDCFActive" Runat="server" RepeatDirection="Horizontal">
																								<asp:ListItem Value="Y" Selected>Yes</asp:ListItem>
																								<asp:ListItem Value="N">No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Email Subject : </FONT></TD>
																						<TD>
																						    <asp:textbox id="txtRDCFSubj" Runat="server" maxlength="250" Width="335px"></asp:textbox></TD>
																					</TR>
																					<TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Email Content : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtRDCFContent" TextMode=MultiLine maxlength="1000" Columns="45" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtRDCFContentCount" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="MMLRDCFContent" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Email Content to 1000 Characters." OutputControl="txtRDCFContentCount" ControlToValidate="txtRDCFContent"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																				    <TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Email for MOF</font></TD>
																					</TR>
																					<TR>
																						<TD><FONT class="DisplayTitle"><font color="red">*</font>Active? </FONT></TD>
																						<TD>
																						    <asp:radiobuttonlist id="rdRDMOFActive" Runat="server" RepeatDirection="Horizontal">
																								<asp:ListItem Value="Y" Selected>Yes</asp:ListItem>
																								<asp:ListItem Value="N">No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Email Subject : </FONT></TD>
																						<TD>
																						    <asp:textbox id="txtRDMOFSubj" Runat="server" maxlength="250" Width="335px"></asp:textbox></TD>
																					</TR>
																					<TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Email Content : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtRDMOFContent" TextMode=MultiLine maxlength="1000" Columns="45" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtRDMOFContentCount" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="Multilinetextboxvalidator1" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Email Content to 1000 Characters." OutputControl="txtRDMOFContentCount" ControlToValidate="txtRDMOFContent"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Email for Disposal</font></TD>
																					</TR>
																					<TR>
																						<TD><FONT class="DisplayTitle"><font color="red">*</font>Active? </FONT></TD>
																						<TD>
																						    <asp:radiobuttonlist id="rdRDDispActive" Runat="server" RepeatDirection="Horizontal">
																								<asp:ListItem Value="Y" Selected>Yes</asp:ListItem>
																								<asp:ListItem Value="N">No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Email Subject : </FONT></TD>
																						<TD>
																						    <asp:textbox id="txtRDDispSubj" Runat="server" maxlength="250" Width="335px"></asp:textbox></TD>
																					</TR>
																					<TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Email Content : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtRDDispContent" TextMode=MultiLine maxlength="1000" Columns="45" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtRDDispContentCount" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="Multilinetextboxvalidator2" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Email Content to 1000 Characters." OutputControl="txtRDDispContentCount" ControlToValidate="txtRDDispContent"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																					<TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Email for Receiver</font></TD>
																					</TR>
																					<TR>
																						<TD><FONT class="DisplayTitle"><font color="red">*</font>Active? </FONT></TD>
																						<TD>
																						    <asp:radiobuttonlist id="rdRDRevActive" Runat="server" RepeatDirection="Horizontal">
																								<asp:ListItem Value="Y" Selected>Yes</asp:ListItem>
																								<asp:ListItem Value="N">No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Email Subject : </FONT></TD>
																						<TD>
																						    <asp:textbox id="txtRDRevSubj" Runat="server" maxlength="250" Width="335px"></asp:textbox></TD>
																					</TR>
																					<TR>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Email Content : </FONT>
																					    </TD>
																					    <TD>
																					         <asp:TextBox id="txtRDRevContent" TextMode=MultiLine maxlength="1000" Columns="45" Rows="5" Runat="server"></asp:TextBox><br />
																					         <asp:textbox id="txtRDRevContentCount" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					         <fluent:multilinetextboxvalidator id="MMLRDRevContent" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Email Content to 1000 Characters." OutputControl="txtRDRevContentCount" ControlToValidate="txtRDRevContent"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD colSpan="2">&nbsp;</TD>
																				    </TR>
																					<TR>
																						<TD align="center" colSpan="2"><BR>
																							<asp:button id="butSubmit" Runat="Server" Text="Submit"></asp:button>
																							<asp:button id="butReset" Runat="Server" Text="Reset"></asp:button></TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer>
			<input type="hidden" id="hdnCDAppOff" runat="server"> 
            <input type="hidden" id="hfRDAppOff" runat="server"> 
		</form>
        <script type="text/javascript">
            	    $(document).ready(function () {
            	        SearchText2("cbCDCPDFinance");
            	        SearchText2("cbRDCPDFinance");
            	        SearchText1("cbCDAppOff", "hdnCDAppOff");
            	        SearchText1("cbRDAppOff", "hfRDAppOff");	        
            	    });

            	    function SearchText2(obj) {
            	        $("#" + obj).autocomplete({
            	            source: function (request, response) {
            	                $.ajax({
            	                    url: '<%=ResolveUrl("~/DisposeMng/DisSettings.aspx/GetUsers") %>',
            	                    data: "{ 'prefix': '" + request.term + "'}",
            	                    dataType: "json",
            	                    type: "POST",
            	                    contentType: "application/json; charset=utf-8",
            	                    success: function (data) {
            	                        response($.map(data.d, function (item) {
            	                            return {
            	                                label: item.split('~')[0]

            	                            }
            	                        }))
            	                    },
            	                    error: function (response) {
            	                        alert(response.responseText);
            	                    },
            	                    failure: function (response) {
            	                        alert(response.responseText);
            	                    }
            	                });
            	            },
            	            focus: function () {
            	                // prevent value inserted on focus
            	                return false;
            	            },
            	            select: function (event, ui) {
            	                var terms = split(this.value);
            	                // remove the current input
            	                terms.pop();
            	                // add the selected item
            	                terms.push(ui.item.value);
            	                // add placeholder to get the comma-and-space at the end
            	                terms.push("");
            	                this.value = terms.join("; ");
            	                return false;
            	            }
            	        });
            	        $("#" + obj).bind("keydown", function (event) {
            	            if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("autocomplete").menu.active) {
            	                event.preventDefault();
            	            }
            	        })
            	        function split(val) {
            	            return val.split(/;\s*/);
            	        }
            	        function extractLast(term) {
            	            return split(term).pop();
            	        }
            	    }
                                	   
            	    function SearchText1(obj1, obj2) {
            	        $("#" + obj1).autocomplete({
            	            source: function (request, response) {
            	                $.ajax({
            	                    url: '<%=ResolveUrl("~/DisposeMng/DisSettings.aspx/GetUsersList") %>',
            	                    data: "{ 'prefix': '" + request.term + "'}",
            	                    dataType: "json",
            	                    type: "POST",
            	                    contentType: "application/json; charset=utf-8",
            	                    success: function (data) {
            	                        response($.map(data.d, function (item) {
            	                            return {
            	                                label: item.split('~')[0],
            	                                val: item.split('~')[1]

            	                            }


            	                        }))
            	                    },
            	                    error: function (response) {
            	                        alert(response.responseText);
            	                    },
            	                    failure: function (response) {
            	                        alert(response.responseText);
            	                    }
            	                });
            	            },
            	            select: function (e, i) {
            	                $("#" + obj2).val(i.item.val);
            	            },
            	            minLength: 0
            	        });

            	    }
        </script>
</body>
</html>
