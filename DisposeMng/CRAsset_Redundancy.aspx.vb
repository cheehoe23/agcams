#Region "Information Section"
' ****************************************************************************************************
' Description       : Redundancy Asset 
' Purpose           : Redundancy Asset Information
' Date              : 12/11/2008
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Web.Services

#End Region

Partial Public Class CRAsset_Redundancy
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            Me.Header.DataBind()

            If Not Page.IsPostBack Then
                Me.butRedundant.Attributes.Add("OnClick", "return chkFrm()")
                Me.butAddImages.Attributes.Add("OnClick", "return mfnOpenAddImagesWindow()")

                ''Get Default Value
                'hdnAssetIDs.Value = Session("DisAssets") 'Request("AIDs")
                hdnAddFileF.Value = "N"

                ''Populate Control
                fnPopulateCtrl()

                ''Display Record
                fnPopulateRecords()
            End If

            '' Check "Add New File Images"
            If hdnAddFileF.Value = "Y" Then
                fnPopulateFileImages()

                ''set back to default
                hdnAddFileF.Value = "N"
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False)
            If Session("AdminF") = "N" Then ddlDepartment.Items.Remove(ddlDepartment.Items.FindByValue("-1"))

            ''Get Disposal Method
            fnPopulateDisposalMethodInDDL(ddlDispMethod)

            ''Get Approving Officer
            cbAppOff.DataBind()

            ''Get Requestor
            cbRequestor.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Dim objRdr As SqlDataReader
        Try
            Dim strTotNBV As String = ""

            ''Get Asset Records
            'dgAsset.DataSource = clsAsset.fnAssetGetAssetRecords(hdnAssetIDs.Value, "fldAssetBarcode", "ASC")
            dgAsset.DataSource = clsDisMng.fnDisMng_GetAssetRecords(Session("DisAssets"), "fldAssetBarcode", "ASC", strTotNBV)
            dgAsset.DataBind()
            If Not dgAsset.Items.Count > 0 Then ''Not Records found
                butRedundant.Enabled = False
                butReset.Enabled = False
                lblErrorMessage.Text = "There is no asset record."
                lblErrorMessage.Visible = True
            End If

            ''Get Tot NBV
            If strTotNBV <> "" Then
                lblTotNBV.Text = strTotNBV
            Else
                lblTotNBV.Text = "0.00"
            End If

            ''Check whether Tot NBV > 500,000 (for MOF) or Tot NBV <= 500,000 (for AO)
            If CDec(strTotNBV) > "500000" Then
                lblAOMdty1.Visible = False
                lblAOMdty2.Visible = False
                lblMandty1.Visible = True
                lblMandty2.Visible = True
                lblMandty3.Visible = True
                hdnMandatoryF.Value = "MOF"
            Else
                lblAOMdty1.Visible = True
                lblAOMdty2.Visible = True
                lblMandty1.Visible = False
                lblMandty2.Visible = False
                lblMandty3.Visible = False
                hdnMandatoryF.Value = "AO"
            End If

            ''Get Default Approving Officer

            objRdr = clsGeneralSetting.fnGrlSetGetGeneralSettingInfo("P", "Redundancy - Approving Officer", "2")
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()
                    cbAppOff.Text = CStr(objRdr("fld_systFieldsStr2")) & " (" + CStr(objRdr("fld_department")) & ")"
                End If
            End If
            objRdr.Close()

            ''Get Default Department
            If Session("AdminF") = "Y" Then
                objRdr = clsGeneralSetting.fnGrlSetGetGeneralSettingInfo("P", "Redundancy - Department", "2")
                If Not objRdr Is Nothing Then
                    If objRdr.HasRows Then
                        objRdr.Read()
                        ddlDepartment.SelectedValue = CStr(objRdr("fld_systFieldsStr"))
                    End If
                End If
                objRdr.Close()
            End If
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Function

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        'Server.Transfer("CRAsset_Redundancy.aspx?AIDs=" & hdnAssetIDs.Value)
        Server.Transfer("CRAsset_Redundancy.aspx")
    End Sub

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Protected Sub butRedundant_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butRedundant.Click
        Try
            Dim intRetVal As Integer
            Dim strUsrExistF As String = "N"
            Dim strRedunID As String = ""
            Dim strSNum As String = ""
            Dim strABarcode As String = ""

            ''*** Get Value
            Dim strAssetIDs As String = Session("DisAssets") 'hdnAssetIDs.Value
            Dim strDept As String = IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue)
            Dim strRevParty As String = Trim(txtRevParty.Text)
            Dim strDisposalMethod As String = ddlDispMethod.SelectedValue
            Dim strReasonRedun As String = Trim(txtReason.Text)
            Dim strAppOffName As String = Trim(cbAppOff.Text)
            Dim strAppOffLID As String = ""
            Dim strAppOffDesg As String = Trim(txtAODesg.Text)
            Dim strReqName As String = Trim(cbRequestor.Text)
            Dim strReqLID As String = ""
            Dim strReqDesg As String = Trim(txtReqDesg.Text)
            Dim strReqDt As String = Trim(txtReqDt.Text)
            Dim strMOFName As String = Trim(txtMOFName.Text)
            Dim strMOFDesg As String = Trim(txtMOFDesg.Text)
            Dim strMOFEmail As String = Trim(txtMOFEmail.Text)
            Dim strMore5hkF As String = IIf(hdnMandatoryF.Value = "MOF", "Y", "N")

            ''*** Checking For :-
            ''1. Approving Officer
            If strAppOffName <> "" Then
                strUsrExistF = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(strAppOffName, strAppOffLID, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Approving Officer not found. Please select an existing Approving Officer."
                    lblErrorMessage.Visible = True
                    cbAppOff.Focus()
                    Exit Sub
                End If
            End If

            ''2. Requestor
            If strReqName <> "" Then
                strUsrExistF = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(strReqName, strReqLID, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Requestor not found. Please select an existing Requestor."
                    lblErrorMessage.Visible = True
                    cbRequestor.Focus()
                    Exit Sub
                End If
            End If

            ''*** Redundancy Asset 
            intRetVal = clsDisMng.fnDisMng_RedundAsset( _
                            strAssetIDs, strDept, strRevParty, strDisposalMethod, strReasonRedun, _
                            strAppOffLID, strAppOffDesg, _
                            strReqLID, strReqDesg, strReqDt, _
                            strMOFName, strMOFDesg, strMOFEmail, strMore5hkF, _
                            Session("UsrID"), strRedunID, strSNum, strABarcode)

            If intRetVal > 0 Then
                ''Generate Redundancy PDF
                Dim strPath As String = gRedunPath & "R" & strRedunID & "_1.pdf"
                fnGenPDF(strRedunID, Server.MapPath(strPath))

                ''store Redundancy Cert to eRegistry
                Dim strCertID As String = "R" & strRedunID & "_1.pdf"
                ''Check whether PDF File generated
                If File.Exists(Server.MapPath(strPath)) Then
                    ''Store Redundancy Cert to eRegistry is going here...

                Else
                    lblErrorMessage.Text = "Certificate of Redundancy generate failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                    Exit Sub
                End If

                ''Send Email  
                If strMore5hkF = "Y" Then
                    ''to MOF
                    If strMOFEmail <> "" Then
                        fnEmail4MOF(strRedunID, strSNum)
                    End If
                Else
                    ''to Approving Officer 
                    fnEmail4AppOff(strRedunID, strCertID, strSNum)
                End If

                ''Insert Audit Trail
                If Len(strABarcode) > 7904 Then
                    strABarcode = strABarcode.Substring(0, 7904) & ", ..."
                End If
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "<font class=DisplayAudit>[Certificate of Redundancy Generated]</font><br><b>S/No</b> : " & strSNum & "<br><b>Asset IDs Included</b> :-<br>" & strABarcode)

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + "Certificate of Redundancy generate successfully with S/No:" & strSNum & "." + "');" & _
                                "document.location.href='CRAsset_Search.aspx?CallType=R';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                lblErrorMessage.Text = "Certificate of Redundancy generate failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnEmail4MOF(ByVal strRedunID As String, ByVal strSNum As String)
        Try
            Dim objRdr As SqlDataReader
            Dim strEmailActiveF As String = "N"
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""

            Dim strEmailTo As String = ""
            Dim strEmailCC As String = ""
            Dim strMailBody As String = ""

            ''*** Get Email Details from database
            objRdr = clsDisMng.fnDisMng_GetEmailInfo4Redund4(strRedunID)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()

                    strEmailActiveF = CStr(objRdr("EmailActiveF"))
                    strMailSubject = CStr(objRdr("EmailSubject")) '& " S/No:" & strSNum
                    strEmailContent = objRdr("EmailContent")
                    strEmailTo = CStr(objRdr("EmailTo"))

                    ''*** If Email Active, checking for send email
                    If strEmailActiveF = "Y" Then
                        ''--> Get Email Body
                        strMailBody = "Dear Sir/Madam,<br><br><br>"
                        strMailBody += Replace(strEmailContent, vbCrLf, "<br>")
                        strMailBody += "<BR><br><br>Thanks and Regards,"
                        strMailBody += "<BR>File and Asset Management System"
                        strMailBody += "<BR><br><br>* This is a computer generated e-mail, please do not reply to it."

                        ''--> Send Email
                        If Trim(strEmailTo) <> "" Or Trim(strEmailCC) <> "" Then
                            fnSendEmail(strEmailTo, strEmailCC, "", strMailSubject, strMailBody, "", "H")
                        End If
                    Else
                        Exit Sub
                    End If
                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnEmail4AppOff(ByVal strRedunID As String, ByVal strCertID As String, ByVal strSNum As String)
        Try
            Dim objRdr As SqlDataReader
            Dim strEmailActiveF As String = "N"
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""

            Dim strEmailTo As String = ""
            Dim strEmailCC As String = ""
            Dim strMailBody As String = ""
            Dim dtFile As DataTable

            ''*** Get Email Details from database
            objRdr = clsDisMng.fnDisMng_GetEmailInfo4Redund1(strRedunID)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()

                    strEmailActiveF = CStr(objRdr("EmailActiveF"))
                    strMailSubject = CStr(objRdr("EmailSubject")) & " S/No:" & strSNum
                    strEmailContent = objRdr("EmailContent")
                    strEmailTo = CStr(objRdr("EmailTo"))

                    ''*** If Email Active, checking for send email
                    If strEmailActiveF = "Y" Then
                        ''--> remove duplicate email ids in the To List and CC List.
                        'strEmailTo = fnRemoveNameFromEmailAdd(strEmailTo)
                        'strEmailTo = fnRemoveDuplicates(strEmailTo, ";")
                        'strEmailCC = fnRemoveNameFromEmailAdd(strEmailCC)
                        'strEmailCC = fnRemoveDuplicates2(strEmailTo, strEmailCC, ";")

                        ''--> Get Email Body
                        strMailBody = "Dear Sir/Madam,<br><br><br>"
                        strMailBody += Replace(strEmailContent, vbCrLf, "<br>")
                        strMailBody += "<BR><br>Please refer to Redundancy Certificate for followup action:-<br>"
                        strMailBody += "Redundancy Certificate : "
                        strMailBody += "<a id=hlFile href='" & gRedunEReg() & strCertID & "' target=RedundFile >"
                        strMailBody += "Click for view certificate"
                        strMailBody += "</a>"
                        strMailBody += "<br>"

                        dtFile = New DataTable()
                        dtFile = clsDisMng.fnRedundancyFileGetRec(Session("UsrID"), strRedunID, "N").Tables(0)

                        For i As Integer = 0 To dtFile.Rows.Count - 1
                            strMailBody += "Redundancy other supporting document Part " & i + 2 & ": "
                            strMailBody += "<a id=hlFile3 href='" & gRedunEReg() & dtFile.Rows(i)("fld_FileSaveName").ToString() & "' target=RedundFile2 >"
                            strMailBody += "Click for view other supporting document"
                            strMailBody += "</a>"
                            strMailBody += "<br>"
                        Next
                        strMailBody += "<br>"

                        strMailBody += "For approval of certificate, click "
                        strMailBody += "<a id=hlRDApp href='" & gRedunAppPage() & strRedunID & "' target=RedundApp >"
                        strMailBody += "here"
                        strMailBody += "</a>."
                        strMailBody += "<BR><br><br>Thanks and Regards,"
                        strMailBody += "<BR>File and Asset Management System"
                        strMailBody += "<BR><br><br>* This is a computer generated e-mail, please do not reply to it."

                        ''--> Send Email
                        If Trim(strEmailTo) <> "" Or Trim(strEmailCC) <> "" Then
                            fnSendEmail(strEmailTo, strEmailCC, "", strMailSubject, strMailBody, "", "H")
                        End If
                    Else
                        Exit Sub
                    End If
                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnGenPDF(ByVal strRedunID As String, ByVal strFullPath As String)
        Try
            Dim ds As New DataSet()
            ds = clsDisMng.fnDisMng_GetRedundRecords4Rpt(strRedunID)

            If ds.Tables(0).Rows.Count > 0 Then
                Dim myReportDocument As New ReportDocument()
                myReportDocument.Load(Server.MapPath("ReportFile\RedundancyCert.rpt"))
                myReportDocument.SetDataSource(ds.Tables(0))
                Dim DiskOpts2 As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions()
                myReportDocument.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile
                DiskOpts2.DiskFileName = strFullPath
                myReportDocument.ExportOptions.DestinationOptions = DiskOpts2
                myReportDocument.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                myReportDocument.Export()

                myReportDocument.Close()
                myReportDocument.Dispose()
                GC.Collect()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

    Public Function fnGetFileName(ByVal strFileNameSave As String) As String
        Try
            Dim strRetVal As String = ""
            strRetVal += "<a id=hlFile href='../tempFile/RendundancyFile/" & strFileNameSave & "' target=RendundancyFile >" & _
                         "<img id=imgView src=../images/audit.gif alt='Click for view file.' border=0 >" & _
                         "</a>"

            Return (strRetVal)

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub fnPopulateFileImages()
        Try
            ''Get Records
            dgRedundancyFileImages.DataSource = clsDisMng.fnRedundancyFileGetRec(Session("UsrID"), "0", "Y")
            dgRedundancyFileImages.DataBind()
            If Not dgRedundancyFileImages.Items.Count > 0 Then ''Not Records found
                dgRedundancyFileImages.Visible = False
            Else
                dgRedundancyFileImages.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgRedundancyFileImages_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgRedundancyFileImages.DeleteCommand
        Try
            Dim retval As Integer = "0"
            Dim strFileIDDel As String = e.Item.Cells(0).Text
            Dim strFileNameDel As String = e.Item.Cells(1).Text
            Dim strFileLocation As String = Server.MapPath("..\tempFile\RendundancyFile") & "\" & strFileNameDel

            ''Start: Delete selected records from database
            retval = clsDisMng.fnRedundancyFileDeleteRec(Session("UsrID"), strFileIDDel, "DP", "")
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 Then
                ''Delete File in AssetFile folder
                If File.Exists(strFileLocation) Then
                    File.Delete(strFileLocation)
                End If

                ''populate File Images
                fnPopulateFileImages()
            Else
                lblErrorMessage.Text = "File delete failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

End Class