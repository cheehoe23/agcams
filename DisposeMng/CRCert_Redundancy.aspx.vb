#Region "Information Section"
' ****************************************************************************************************
' Description       : Redundancy Cert 
' Purpose           : Redundancy Cert Information
' Date              : 13/11/2008
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
#End Region

Partial Public Class CRCert_Redundancy
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            Me.Header.DataBind()

            If Not Page.IsPostBack Then
                Me.butApprove.Attributes.Add("OnClick", "return fnConfirmRec('A')")
                Me.butReject.Attributes.Add("OnClick", "return fnConfirmRec('R')")
                Me.butDispose.Attributes.Add("OnClick", "return fnConfirmRec('D')")

                ''Get Default Value
                hdnCertID.Value = Request("CertID")

                ''Populate Control
                fnPopulateCtrl()

                ''Display Record
                fnPopulateRecords()

                ''Check Access Right
                If hdnCertStatus.Value = "P" Then
                    If (InStr(Session("AR"), "DisMng|CRC|AR") = 0) Then
                        ''checking access right
                        butApprove.Visible = False
                        butReject.Visible = False
                        divReasonRejectField.Visible = False
                    Else
                        If hdnMore5hkF.Value = "N" Then
                            ''For Tot NBV <= 500,000, checking for "just approving officer allow to approve/reject"
                            If Not clsDisMng.fnDisMng_RedundCheckAppOff(Session("UsrID"), "", hdnCertID.Value, "M") Then
                                butApprove.Visible = False
                                butReject.Visible = False
                                divReasonRejectField.Visible = False

                                lblErrorMessage.Text = "You are not authorized to approve/reject certificate."
                                lblErrorMessage.Visible = True
                            End If
                        End If
                    End If
                ElseIf hdnCertStatus.Value = "A" Then
                    If (InStr(Session("AR"), "DisMng|CRC|Dis") = 0) Then
                        ''checking access right
                        butDispose.Visible = False
                        divCompDisposalField.Visible = False
                        divReceiverField.Visible = False
                    End If
                End If

            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Completion Disposal
            cbCompDisp.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Dim objRdr As SqlDataReader
        Try
            Dim strStatus As String = ""
            Dim strPath1 As String = gRedunPath & "R" & hdnCertID.Value & "_1.pdf"
            Dim strPath2 As String = gRedunPath & "R" & hdnCertID.Value & "_2.pdf"
            Dim strPath3 As String = gRedunPath & "R" & hdnCertID.Value & "_3.pdf"
            Dim dtFile As DataTable

            ''Get Certificate Records

            objRdr = clsDisMng.fnDisMng_GetRedundRecords(hdnCertID.Value)
            If Not objRdr Is Nothing Then

                dtFile = New DataTable()
                dtFile = clsDisMng.fnRedundancyFileGetRec(0, hdnCertID.Value, "N").Tables(0)

                For i As Integer = 0 To dtFile.Rows.Count - 1
                    spanOtherSupportingDoc.InnerHtml += "<a id=hlFile href='" & gRedunPath & dtFile.Rows(i)("fld_FileSaveName").ToString() & "' target=CondemnFile3 >" & _
                                                 "View " & dtFile.Rows(i)("fld_FileName").ToString() & _
                                                 "</a>, "
                Next

                If objRdr.HasRows Then
                    objRdr.Read()

                    hdnCertStatus.Value = CStr(objRdr("fld_RDCertStatus"))
                    hdnDipMethod.Value = CStr(objRdr("fld_DisposalMethod"))
                    hdnMore5hkF.Value = CStr(objRdr("fld_RDMore5hkF"))

                    lblCertSNo.Text = CStr(objRdr("fld_RDSNo"))
                    strStatus = CStr(objRdr("fld_RDCertStatus"))

                    If strStatus = "P" Then
                        lblCertStatus.Text = "Pending"

                        spanCertLink.InnerHtml = "<a id=hlFile href='" & strPath1 & "' target=RedundFile1 >" & _
                                                 "View certificate" & _
                                                 "</a>"

                        divCompDisposalView.Visible = False
                        divCompDisposalField.Visible = False
                        divReceiverView.Visible = False
                        divReceiverField.Visible = False
                        divReasonRejectField.Visible = True
                        divReasonRejectView.Visible = False
                        butApprove.Visible = True
                        butReject.Visible = True
                        butDispose.Visible = False
                        lblMOFSysApp.Visible = False

                    ElseIf strStatus = "A" Then
                        lblCertStatus.Text = "Approved"

                        spanCertLink.InnerHtml = "<a id=hlFile href='" & strPath2 & "' target=RedundFile1 >" & _
                                                 "View certificate" & _
                                                 "</a>"

                        divCompDisposalView.Visible = False
                        divCompDisposalField.Visible = True
                        divReceiverView.Visible = False
                        If hdnDipMethod.Value = "5" Then
                            divReceiverField.Visible = False
                        Else
                            divReceiverField.Visible = True
                        End If
                        divReasonRejectField.Visible = False
                        divReasonRejectView.Visible = False
                        butApprove.Visible = False
                        butReject.Visible = False
                        butDispose.Visible = True
                        If hdnMore5hkF.Value = "N" Then
                            lblMOFSysApp.Visible = False
                        Else
                            lblMOFSysApp.Visible = True
                        End If


                    ElseIf strStatus = "R" Then
                        lblCertStatus.Text = "Reject"

                        spanCertLink.InnerHtml = "<a id=hlFile href='" & strPath2 & "' target=RedundFile1 >" & _
                                                 "View certificate" & _
                                                 "</a>"

                        divCompDisposalView.Visible = False
                        divCompDisposalField.Visible = False
                        divReceiverView.Visible = False
                        divReceiverField.Visible = False
                        divReasonRejectField.Visible = False
                        divReasonRejectView.Visible = True
                        butApprove.Visible = False
                        butReject.Visible = False
                        butDispose.Visible = False
                        If hdnMore5hkF.Value = "N" Then
                            lblMOFSysApp.Visible = False
                        Else
                            lblMOFSysApp.Visible = True
                        End If

                    ElseIf strStatus = "D" Then
                        lblCertStatus.Text = "Disposed"

                        spanCertLink.InnerHtml = "<a id=hlFile href='" & strPath3 & "' target=RedundFile1 >" & _
                                                                         "View certificate" & _
                                                                         "</a>"

                        divCompDisposalView.Visible = True
                        divCompDisposalField.Visible = False
                        If hdnDipMethod.Value = "5" Then
                            divReceiverView.Visible = False
                        Else
                            divReceiverView.Visible = True
                        End If
                        divReceiverField.Visible = False
                        divReasonRejectField.Visible = False
                        divReasonRejectView.Visible = False
                        butApprove.Visible = False
                        butReject.Visible = False
                        butDispose.Visible = False
                        If hdnMore5hkF.Value = "N" Then
                            lblMOFSysApp.Visible = False
                        Else
                            lblMOFSysApp.Visible = True
                        End If

                    End If

                    lblDept.Text = CStr(objRdr("fld_DeptName")) & " (" & CStr(objRdr("fld_DeptCode")) & ")"
                    lblRevParty.Text = CStr(objRdr("fld_ReceiverParty"))
                    lblTotNBV.Text = CStr(objRdr("fld_TotNBV"))

                    If CStr(objRdr("fld_DisposalMethod")) = "1" Then
                        lblDispMethod.Text = "Donation to another Department"
                    ElseIf CStr(objRdr("fld_DisposalMethod")) = "2" Then
                        lblDispMethod.Text = "Donation to a non-Government body or Statutory Board"
                    ElseIf CStr(objRdr("fld_DisposalMethod")) = "3" Then
                        lblDispMethod.Text = "Normal Sale"
                    ElseIf CStr(objRdr("fld_DisposalMethod")) = "4" Then
                        lblDispMethod.Text = "Trade-in"
                    ElseIf CStr(objRdr("fld_DisposalMethod")) = "5" Then
                        lblDispMethod.Text = "Write-off"
                    End If

                    lblReason.Text = Replace(CStr(objRdr("fld_RDReason")), vbCrLf, "<br>")

                    lblAppOff.Text = CStr(objRdr("fld_AppOffName"))
                    lblAODesg.Text = CStr(objRdr("fld_AppOffDesg"))
                    lblAODt.Text = fnCheckNull(objRdr("fld_AppOffDt"))

                    lblRequestor.Text = CStr(objRdr("fld_RequestorName"))
                    lblReqDesg.Text = CStr(objRdr("fld_RequestorDesg"))
                    lblReqDt.Text = CStr(objRdr("fld_RequestorDt"))

                    lblMOFName.Text = CStr(objRdr("fld_MOFName"))
                    lblMOFDesg.Text = CStr(objRdr("fld_MOFDesg"))
                    lblMOFEmail.Text = CStr(objRdr("fld_MOFEmail"))
                    lblMOFDt.Text = fnCheckNull(objRdr("fld_MOFDt"))

                    lblCDispName.Text = fnCheckNull(objRdr("fld_DisposalName"))
                    lblCDispDesg.Text = fnCheckNull(objRdr("fld_DisposalDesg"))
                    lblCDispDt.Text = fnCheckNull(objRdr("fld_DisposalDt"))

                    lblRevName.Text = fnCheckNull(objRdr("fld_ReceiverName"))
                    lblRevDesg.Text = fnCheckNull(objRdr("fld_ReceiverDesg"))
                    lblRevDt.Text = fnCheckNull(objRdr("fld_ReceiverDt"))
                    lblRevEmail.Text = fnCheckNull(objRdr("fld_ReceiverEmail"))

                    lblRejReason.Text = Replace(fnCheckNull(objRdr("fld_RejectReason")), vbCrLf, "<br>") 'fnCheckNull(objRdr("fld_RejectReason"))

                End If
            End If
            objRdr.Close()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Function

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Protected Sub butReject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReject.Click
        Try
            Dim intRetVal As Integer
            Dim strCertID As String = hdnCertID.Value

            ''*** Reject Certificate 
            intRetVal = clsDisMng.fnDisMng_RedundCertReject( _
                            strCertID, Trim(txtRejReason.Text), Session("UsrID"), "M")

            If intRetVal > 0 Then
                ''Generate  PDF
                Dim strPath As String = gRedunPath & "R" & strCertID & "_2.pdf"
                fnGenPDF(strCertID, Server.MapPath(strPath))

                ''store Redundancy Cert to eRegistry
                Dim strCertIDeReg As String = "R" & strCertID & "_2.pdf"
                ''Check whether PDF File generated
                If File.Exists(Server.MapPath(strPath)) Then
                    ''Store Redundancy Cert to eRegistry is going here...

                Else
                    lblErrorMessage.Text = "Certificate of Redundancy generate failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                    Exit Sub
                End If

                Me.fnEmail("Rejected")
                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Certificate of Redundancy with S/No: " & lblCertSNo.Text & " has been rejected.")

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + "Certificate of Redundancy reject successfully." + "');" & _
                                "document.location.href='CRCert_Redundancy.aspx?CertID=" & strCertID & "';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                lblErrorMessage.Text = "Certificate of Redundancy reject failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butApprove.Click
        Try
            Dim intRetVal As Integer
            Dim strCertID As String = hdnCertID.Value

            ''*** Approve Certificate 
            intRetVal = clsDisMng.fnDisMng_RedundCertApprove( _
                            strCertID, Session("UsrID"), "M")

            If intRetVal > 0 Then
                ''Generate  PDF
                Dim strPath As String = gRedunPath & "R" & strCertID & "_2.pdf"
                fnGenPDF(strCertID, Server.MapPath(strPath))

                ''store Redundancy Cert to eRegistry
                Dim strCertIDeReg As String = "R" & strCertID & "_2.pdf"
                ''Check whether PDF File generated
                If File.Exists(Server.MapPath(strPath)) Then
                    ''Store Redundancy Cert to eRegistry is going here...

                Else
                    lblErrorMessage.Text = "Certificate of Redundancy generate failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                    Exit Sub
                End If

                Me.fnEmail("Approved")

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Certificate of Redundancy with S/No: " & lblCertSNo.Text & " have been approved.")

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + "Certificate of Redundancy approved successfully." + "');" & _
                                "document.location.href='CRCert_Redundancy.aspx?CertID=" & strCertID & "';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                lblErrorMessage.Text = "Certificate of Redundancy approve failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnEmail4CPDFin(ByVal strRedunID As String, ByVal strCertID As String)
        Dim objRdr As SqlDataReader
        Try
            Dim strEmailActiveF As String = "N"
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""

            Dim strEmailTo As String = ""
            Dim strEmailCC As String = ""
            Dim strMailBody As String = ""

            ''*** Get Email Details from database
            objRdr = clsDisMng.fnDisMng_GetEmailInfo4Redund2(strRedunID)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()

                    strEmailActiveF = CStr(objRdr("EmailActiveF"))
                    strMailSubject = CStr(objRdr("EmailSubject")) & " S/No:" & Replace(lblCertSNo.Text, "A", "D")
                    strEmailContent = objRdr("EmailContent")
                    strEmailTo = CStr(objRdr("EmailTo"))

                    ''*** If Email Active, checking for send email
                    If strEmailActiveF = "Y" Then
                        ''--> remove duplicate email ids in the To List and CC List.
                        strEmailTo = fnRemoveNameFromEmailAdd(strEmailTo)
                        strEmailTo = fnRemoveDuplicates(strEmailTo, ";")
                        'strEmailCC = fnRemoveNameFromEmailAdd(strEmailCC)
                        'strEmailCC = fnRemoveDuplicates2(strEmailTo, strEmailCC, ";")

                        ''--> Get Email Body
                        strMailBody = "Dear Sir/Madam,<br><br><br>"
                        strMailBody += Replace(strEmailContent, vbCrLf, "<br>")
                        strMailBody += "<BR><br>Please refer below for Redundancy Certificate:-<br>"
                        strMailBody += "Redundancy Certificate : "
                        strMailBody += "<a id=hlFile href='" & gRedunEReg() & strCertID & "' target=RedundFile >"
                        strMailBody += "Click for view certificate"
                        strMailBody += "</a>"
                        strMailBody += "<BR><br><br>Thanks and Regards,"
                        strMailBody += "<BR>File and Asset Management System"
                        strMailBody += "<BR><br><br>* This is a computer generated e-mail, please do not reply to it."

                        ''--> Send Email
                        If Trim(strEmailTo) <> "" Or Trim(strEmailCC) <> "" Then
                            fnSendEmail(strEmailTo, strEmailCC, "", strMailSubject, strMailBody, "", "H")
                        End If
                    Else
                        Exit Sub
                    End If
                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Public Sub fnEmail4Rev(ByVal strRedunID As String, ByVal strCertID As String)
        Dim objRdr As SqlDataReader
        Try

            Dim strEmailActiveF As String = "N"
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""

            Dim strEmailTo As String = ""
            Dim strEmailCC As String = ""
            Dim strMailBody As String = ""

            ''*** Get Email Details from database
            objRdr = clsDisMng.fnDisMng_GetEmailInfo4Redund3(strRedunID)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()

                    strEmailActiveF = CStr(objRdr("EmailActiveF"))
                    strMailSubject = CStr(objRdr("EmailSubject"))
                    strEmailContent = objRdr("EmailContent")
                    strEmailTo = CStr(objRdr("EmailTo"))

                    ''*** If Email Active, checking for send email
                    If strEmailActiveF = "Y" Then
                        ''--> remove duplicate email ids in the To List and CC List.
                        'strEmailTo = fnRemoveNameFromEmailAdd(strEmailTo)
                        'strEmailTo = fnRemoveDuplicates(strEmailTo, ";")
                        'strEmailCC = fnRemoveNameFromEmailAdd(strEmailCC)
                        'strEmailCC = fnRemoveDuplicates2(strEmailTo, strEmailCC, ";")

                        ''--> Get Email Body
                        strMailBody = "Dear Sir/Madam,<br><br><br>"
                        strMailBody += Replace(strEmailContent, vbCrLf, "<br>")
                        strMailBody += "<BR><br><br>Thanks and Regards,"
                        strMailBody += "<BR>File and Asset Management System"
                        strMailBody += "<BR><br><br>* This is a computer generated e-mail, please do not reply to it."

                        ''--> Send Email
                        If Trim(strEmailTo) <> "" Or Trim(strEmailCC) <> "" Then
                            fnSendEmail(strEmailTo, strEmailCC, "", strMailSubject, strMailBody, "", "H")
                        End If
                    Else
                        Exit Sub
                    End If
                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Public Sub fnEmail4Disp(ByVal strRedunID As String, ByVal strCertID As String)
        Dim objRdr As SqlDataReader
        Try
            Dim strEmailActiveF As String = "N"
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""

            Dim strEmailTo As String = ""
            Dim strEmailCC As String = ""
            Dim strMailBody As String = ""

            ''*** Get Email Details from database
            objRdr = clsDisMng.fnDisMng_GetEmailInfo4Redund5(strRedunID)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()

                    strEmailActiveF = CStr(objRdr("EmailActiveF"))
                    strMailSubject = CStr(objRdr("EmailSubject")) & " S/No:" & Replace(lblCertSNo.Text, "A", "D")
                    strEmailContent = objRdr("EmailContent")
                    strEmailTo = CStr(objRdr("EmailTo"))

                    ''*** If Email Active, checking for send email
                    If strEmailActiveF = "Y" Then
                        ''--> Get Email Body
                        strMailBody = "Dear Sir/Madam,<br><br><br>"
                        strMailBody += Replace(strEmailContent, vbCrLf, "<br>")
                        strMailBody += "<BR><br>Please refer below for Redundancy Certificate:-<br>"
                        strMailBody += "Redundancy Certificate : "
                        strMailBody += "<a id=hlFile href='" & gRedunEReg() & strCertID & "' target=RedundFile >"
                        strMailBody += "Click for view certificate"
                        strMailBody += "</a>"
                        strMailBody += "<BR><br><br>Thanks and Regards,"
                        strMailBody += "<BR>File and Asset Management System"
                        strMailBody += "<BR><br><br>* This is a computer generated e-mail, please do not reply to it."

                        ''--> Send Email
                        If Trim(strEmailTo) <> "" Or Trim(strEmailCC) <> "" Then
                            fnSendEmail(strEmailTo, strEmailCC, "", strMailSubject, strMailBody, "", "H")
                        End If
                    Else
                        Exit Sub
                    End If
                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Public Sub fnGenPDF(ByVal strRedunID As String, ByVal strFullPath As String)
        Try
            Dim ds As New DataSet()
            ds = clsDisMng.fnDisMng_GetRedundRecords4Rpt(strRedunID)

            If ds.Tables(0).Rows.Count > 0 Then
                Dim myReportDocument As New ReportDocument()
                myReportDocument.Load(Server.MapPath("ReportFile\RedundancyCert.rpt"))
                myReportDocument.SetDataSource(ds.Tables(0))
                Dim DiskOpts2 As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions()
                myReportDocument.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile
                DiskOpts2.DiskFileName = strFullPath
                myReportDocument.ExportOptions.DestinationOptions = DiskOpts2
                myReportDocument.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                myReportDocument.Export()

                myReportDocument.Close()
                myReportDocument.Dispose()
                GC.Collect()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub butDispose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDispose.Click
        Try
            Dim intRetVal As Integer
            Dim strCertID As String = hdnCertID.Value
            Dim strCompDispName As String = Trim(cbCompDisp.Text)
            Dim strCompDispLID As String = ""
            Dim strCompDispDesg As String = Trim(txtCDispDesg.Text)
            Dim strRevName As String = Trim(txtRevName.Text)
            Dim strRevDesg As String = Trim(txtRevDesg.Text)
            Dim strRevDt As String = Trim(txtRevDt.Text)
            Dim strRevEmail As String = Trim(txtRevEmail.Text)

            ''*** Checking For Disposal
            If strCompDispName <> "" Then
                Dim strUsrExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(strCompDispName, strCompDispLID, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Disposal not found. Please select an existing Disposal."
                    lblErrorMessage.Visible = True
                    cbCompDisp.Focus()
                    Exit Sub
                End If
            End If

            ''*** Dispose Certificate 
            intRetVal = clsDisMng.fnDisMng_RedundCertDispose( _
                            strCertID, strCompDispLID, strCompDispDesg, _
                            strRevName, strRevDesg, strRevDt, strRevEmail, Session("UsrID"))

            If intRetVal > 0 Then
                ''Generate  PDF
                Dim strPath As String = gRedunPath & "R" & strCertID & "_3.pdf"
                fnGenPDF(strCertID, Server.MapPath(strPath))

                ''store Redundancy Cert to eRegistry
                Dim strCertIDeReg As String = "R" & strCertID & "_3.pdf"
                ''Check whether PDF File generated
                If File.Exists(Server.MapPath(strPath)) Then
                    ''Store Redundancy Cert to eRegistry is going here...

                Else
                    lblErrorMessage.Text = "Certificate of Redundancy generate failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                    Exit Sub
                End If

                ''Send Email to CPD Finance
                fnEmail4CPDFin(strCertID, strCertIDeReg)

                ''Send Email to Receiver
                If hdnDipMethod.Value <> "5" And strRevEmail <> "" Then
                    fnEmail4Rev(strCertID, strCertIDeReg)
                End If

                ''Send Email to Disposal
                fnEmail4Disp(strCertID, strCertIDeReg)

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Certificate of Redundancy with S/No: " & lblCertSNo.Text & " have been disposed.")

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + "Certificate of Redundancy disposed successfully." + "');" & _
                                "document.location.href='CRCert_Redundancy.aspx?CertID=" & strCertID & "';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                lblErrorMessage.Text = "Certificate of Redundancy dispose failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnEmail(ByVal Type As String)
        Dim objRdr As SqlDataReader
        Try
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""

            Dim strEmailTo As String = ""
            Dim strEmailCC As String = ""
            Dim strMailBody As String = ""

            ''*** Get Email Details from database
            objRdr = clsDisMng.fnDisMng_GetRedundRequesterEmail(hdnCertID.Value)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()

                    strMailSubject = "[" & Type & "] Certificate of Redundancy S/No:" & lblCertSNo.Text
                    strEmailTo = CStr(objRdr("fld_email"))

                    ''*** If Email Active, checking for send email
                    ''--> remove duplicate email ids in the To List and CC List.
                    'strEmailTo = fnRemoveNameFromEmailAdd(strEmailTo)
                    'strEmailTo = fnRemoveDuplicates(strEmailTo, ";")
                    'strEmailCC = fnRemoveNameFromEmailAdd(strEmailCC)
                    'strEmailCC = fnRemoveDuplicates2(strEmailTo, strEmailCC, ";")

                    ''--> Get Email Body
                    strMailBody = "Dear Sir/Madam,<br><br><br>"
                    If Type = "Approved" Then
                        strMailBody += "The above Certificate of Redundancy has been approved.<br><br>"
                    Else
                        strMailBody += "The above Certificate of Redundancy has been rejected.<br><br>"
                        strMailBody += "Reason for Reject:-<br> "
                        strMailBody += Me.txtRejReason.Text.Replace(vbCrLf, "<br>")
                    End If


                    strMailBody += "<BR><br><br>Thanks and Regards,"
                    strMailBody += "<BR>File and Asset Management System"
                    strMailBody += "<BR><br><br>* This is a computer generated e-mail, please do not reply to it."

                    ''--> Send Email
                    If Trim(strEmailTo) <> "" Or Trim(strEmailCC) <> "" Then
                        fnSendEmail(strEmailTo, strEmailCC, "", strMailSubject, strMailBody, "", "H")
                    End If

                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub
End Class