#Region "Information Section"
' ****************************************************************************************************
' Description       : View Asset 
' Purpose           : View Asset Information
' Date              : 10/11/2008
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class CRAsset_SearchResult
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                '//--> (CA)Condemn Asset, (RA)Redundancy Asset
                Me.butCondemnation.Attributes.Add("OnClick", "return fnConfirmSelectedRec('CA')")
                Me.butRedundancy.Attributes.Add("OnClick", "return fnConfirmSelectedRec('RA')")

                ''Get Value pass from Search Screen
                hdnCallFrm.Value = Request("CallFrm")
                hdnAssetIdAMS.Value = Request("AIdAMS")
                hdnAssetIdNFS.Value = Request("AIdNFS")
                hdnAssetType.Value = Request("AType")
                hdnAssetCatID.Value = Request("CatID")
                hdnAssetCatSubID.Value = Request("CatSID")
                hdnAssetStatus.Value = Request("Astatus")
                hdnPurchaseFDt.Value = Request("PFDt")
                hdnPurchaseTDt.Value = Request("PTDt")
                hdnWarExpFDt.Value = Request("WFDt")
                hdnWarExpTDt.Value = Request("WTDt")
                hdnDeptID.Value = Request("DeptID")
                hdnLocID.Value = Request("LocID")
                hdnLocSubID.Value = Request("SLocID")
                hdnOwnerID.Value = Request("OwnerID")
                hdnTempAsset.Value = Request("TempAsset")
                hdnCtrlItemF.Value = Request("CtrlItemF")

                ''default Sorting/setting
                hdnSortName.Value = "fldAssetBarcode"
                hdnSortAD.Value = "ASC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()

                If hdnCallFrm.Value = "C" Then
                    lblHeader.Text = "Condemnation Search Result Asset"

                    ''Checking Access Right 
                    butRedundancy.Visible = False
                    'win 20160530
                    'If InStr(Session("AR"), "DisMng|CA|CD") = 0 Then
                    '    butCondemnation.Visible = False
                    '    dgAsset.Columns(11).Visible = False
                    'End If
                    'win 20160530
                Else
                    lblHeader.Text = "Redundancy Search Result Asset"

                    ''Checking Access Right 
                    butCondemnation.Visible = False

                    'win 20160530
                    'If InStr(Session("AR"), "DisMng|RA|RD") = 0 Then
                    '    dgAsset.Columns(11).Visible = False
                    '    butRedundancy.Visible = False
                    'End If
                    'win 20160530
                End If


            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Sort Header Display
            fnSortHeaderDisplay()

            '' Get Records
            dgAsset.DataSource = clsDisMng.fnDisMngSrhAsset_SearchRecord( _
                                     hdnAssetIdAMS.Value, hdnAssetIdNFS.Value, _
                                     hdnAssetType.Value, hdnAssetCatID.Value, hdnAssetCatSubID.Value, _
                                     hdnAssetStatus.Value, hdnPurchaseFDt.Value, hdnPurchaseTDt.Value, _
                                     hdnWarExpFDt.Value, hdnWarExpTDt.Value, _
                                     hdnDeptID.Value, hdnLocID.Value, hdnOwnerID.Value, _
                                     hdnTempAsset.Value, _
                                     hdnCallFrm.Value, hdnSortName.Value, hdnSortAD.Value, hdnLocSubID.Value, hdnCtrlItemF.Value)
            dgAsset.DataBind()
            If Not dgAsset.Items.Count > 0 Then ''Not Records found
                ddlPageSize.Enabled = False
                butCondemnation.Enabled = False
                butRedundancy.Enabled = False
                lblErrorMessage.Text = "There is no asset record."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By :-
        ''       fldAssetBarcode (2), fld_AssetTypeStr (3), 
        ''       fld_CategoryName (4), fld_CatSubName (5), 
        ''       fld_DepartmentName (6), fld_LocationName (7), 
        ''       fld_OwnerName (8), fld_AssetStatusStr (9)

        Dim intSortIndex As Integer
        Dim strSortHeader As String = ""

        ''Set Default Header
        dgAsset.Columns(2).HeaderText = "Asset ID"
        dgAsset.Columns(3).HeaderText = "Type"
        dgAsset.Columns(4).HeaderText = "Category"
        dgAsset.Columns(5).HeaderText = "Subcategory"
        dgAsset.Columns(6).HeaderText = "Department"
        dgAsset.Columns(7).HeaderText = "Location"
        dgAsset.Columns(8).HeaderText = "Sub Location"
        dgAsset.Columns(9).HeaderText = "Assigned Owner"
        dgAsset.Columns(10).HeaderText = "Status"

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fldAssetBarcode"
                intSortIndex = 2
                strSortHeader = "Asset ID"
            Case "fld_AssetTypeStr"
                intSortIndex = 3
                strSortHeader = "Type"
            Case "fld_CategoryName"
                intSortIndex = 4
                strSortHeader = "Category"
            Case "fld_CatSubName"
                intSortIndex = 5
                strSortHeader = "Subcategory"
            Case "fld_DepartmentName"
                intSortIndex = 6
                strSortHeader = "Department"
            Case "fld_LocationName"
                intSortIndex = 7
                strSortHeader = "Location"
            Case "fld_LocSubName"
                intSortIndex = 8
                strSortHeader = "Sub Location"
            Case "fld_OwnerName"
                intSortIndex = 9
                strSortHeader = "Assigned Owner"
            Case "fld_AssetStatusStr"
                intSortIndex = 10
                strSortHeader = "Status"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgAsset.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgAsset.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgAsset.CurrentPageIndex = 0
        dgAsset.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Private Sub dgAsset_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgAsset.PageIndexChanged
        dgAsset.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub dgAsset_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgAsset.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

    Private Function fnGetSelectedRec() As String
        Try
            'Dim strSelectedRec As String = ""
            ' ''*** Get File Volume that selected 
            'Dim GridItem As DataGridItem
            'Dim chkSelectedRec As CheckBox
            'For Each GridItem In dgAsset.Items
            '    chkSelectedRec = CType(GridItem.Cells(10).FindControl("DeleteThis"), CheckBox)
            '    If chkSelectedRec.Checked Then
            '        strSelectedRec += GridItem.Cells(0).Text & "^"
            '    End If
            'Next
            'Return strSelectedRec

            Dim sXMLString As String = ""
            Dim TblAsset As DataTable = fnCreateDataTableAsset()
            Dim RowAsset As DataRow

            ''Get Selected Asset IDs
            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox
            For Each GridItem In dgAsset.Items
                chkSelectedRec = CType(GridItem.Cells(11).FindControl("DeleteThis"), CheckBox)
                If chkSelectedRec.Checked Then
                    ''Add New Row to Datatable
                    RowAsset = TblAsset.NewRow()        'declaring a new row
                    RowAsset.Item("dt_AssetID") = GridItem.Cells(0).Text
                    TblAsset.Rows.Add(RowAsset)
                End If
            Next

            If TblAsset.Rows.Count > 0 Then
                ''Get XML
                Dim ds As New DataSet
                ds = New DataSet            'creating a dataset
                ds.Tables.Add(TblAsset)    'assign datatable to dataset
                If Not ds Is Nothing Then
                    Dim loCol As DataColumn
                    'Prepare XML output from the DataSet as a string
                    For Each loCol In ds.Tables(0).Columns
                        loCol.ColumnMapping = System.Data.MappingType.Attribute
                    Next
                    sXMLString = ds.GetXml
                End If
            End If

            fnGetSelectedRec = Trim(sXMLString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnCreateDataTableAsset() As DataTable
        Try
            ''****1. creating a table named TblAsset
            Dim TblAsset As DataTable
            TblAsset = New DataTable("TblAsset")

            ''Column 1: Asset id
            Dim dt_AssetID As DataColumn = New DataColumn("dt_AssetID")    'declaring a column named Name
            dt_AssetID.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            TblAsset.Columns.Add(dt_AssetID)                               'adding the column to table

            ''return dataTable 
            fnCreateDataTableAsset = TblAsset
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub butRedundancy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butRedundancy.Click
        Try
            Dim strAssetIDs As String = ""

            ''Get Selected Asset IDs
            strAssetIDs = fnGetSelectedRec()

            If Trim(strAssetIDs) <> "" Then
                'Server.Transfer("../AssetMng/asset_DisposeCondemmRedun.aspx?CallFrm=RC&AIDs=" & strAssetIDs)
                'Server.Transfer("CRAsset_Redundancy.aspx?AIDs=" & strAssetIDs)
                'Response.Redirect("CRAsset_Redundancy.aspx?AIDs=" & strAssetIDs)

                Session("DisAssets") = strAssetIDs
                Response.Redirect("CRAsset_Redundancy.aspx")
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butCondemnation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCondemnation.Click
        Try
            Dim strAssetIDs As String = ""

            ''Get Selected Asset IDs
            strAssetIDs = fnGetSelectedRec()

            If Trim(strAssetIDs) <> "" Then
                'Server.Transfer("../AssetMng/asset_DisposeCondemmRedun.aspx?CallFrm=CC&AIDs=" & strAssetIDs) 
                'Server.Transfer("CRAsset_Condemn.aspx?AIDs=" & strAssetIDs)
                'Response.Redirect("CRAsset_Condemn.aspx?AIDs=" & strAssetIDs)

                Session("DisAssets") = strAssetIDs
                Response.Redirect("CRAsset_Condemn.aspx")
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class