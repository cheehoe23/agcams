#Region "Information Section"
' ****************************************************************************************************
' Description       : Search Certificate 
' Purpose           : Search Certificate Information
' Date              : 12/11/2008
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class CRCert_Search
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butSearch.Attributes.Add("OnClick", "return chkFrm()")

                ''*** Populate Control
                fnPopulateCtrl()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Certificate Status --> Pending, Approved, Reject, Dispose
            ddlCertStatus.Items.Add(New ListItem("", "0"))
            ddlCertStatus.Items.Add(New ListItem("Pending", "P"))
            ddlCertStatus.Items.Add(New ListItem("Approved", "A"))
            ddlCertStatus.Items.Add(New ListItem("Reject", "R"))
            ddlCertStatus.Items.Add(New ListItem("Disposed", "D"))

            ''Get Certificate Type --> Redundancy, Condemation
            fnPopulateAssetStatusInDDL(ddlCertType)
            ddlCertType.Items.Remove(ddlCertType.Items.FindByValue("A"))
            ddlCertType.Items.Remove(ddlCertType.Items.FindByValue("I"))
            ddlCertType.Items.Remove(ddlCertType.Items.FindByValue("D"))
            ddlCertType.Items.Remove(ddlCertType.Items.FindByValue("T"))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            Server.Transfer("CRCert_Search.aspx")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
        Try
            Dim ds As New DataSet
            Dim strURL As String = ""

            Dim strCertSNo As String = Trim(txtCertSNo.Text)
            Dim strCertCFDt As String = Trim(txtCertFrmDt.Text)
            Dim strCertCTDt As String = Trim(txtCertToDt.Text)
            Dim strCertStatus As String = ddlCertStatus.SelectedValue
            Dim strCertType As String = ddlCertType.SelectedValue
            Dim strAssetIDAMS As String = "" 'Trim(txtAssetIDAMS.Text)
            Dim strAssetIDNFS As String = "" 'Trim(txtAssetIDNFS.Text)
            Dim StrSortName As String = "fld_CertSNo"
            Dim strSortOrder As String = "ASC"

            ds = clsDisMng.fnDisMng_SearchCertRecord( _
                         strCertSNo, strCertCFDt, strCertCTDt, _
                         strCertStatus, strCertType, _
                         strAssetIDAMS, strAssetIDNFS, _
                         StrSortName, strSortOrder)


            If ds.Tables(0).Rows.Count > 0 Then
                strURL = "CRCert_SearchResult.aspx?CertSNo=" + strCertSNo + "&CertCFDt=" + strCertCFDt + "&CertCTDt=" + strCertCTDt + _
                         "&CertStatus=" + strCertStatus + "&CertType=" + strCertType + _
                         "&AssetIDAMS=" + strAssetIDAMS + "&AssetIDNFS=" + strAssetIDNFS

                Response.Redirect(strURL)
            Else
                lblErrorMessage.Visible = True
                lblErrorMessage.Text = "Record(s) not found."
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class