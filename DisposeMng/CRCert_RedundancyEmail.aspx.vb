#Region "Information Section"
' ****************************************************************************************************
' Description       : Redundancy Cert 
' Purpose           : Redundancy Cert Information
' Date              : 13/11/2008
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
#End Region

Partial Public Class CRCert_RedundancyEmail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            'If Len(Session("UsrID")) = 0 Then
            '    Dim strJavaScript As String = ""
            '    strJavaScript = "<script language = 'Javascript'>" & _
            '                    "parent.location.href='../common/logout.aspx';" & _
            '                    "</script>"
            '    Response.Write(strJavaScript)
            '    Exit Sub
            'End If

            If Not Page.IsPostBack Then
                Me.butApprove.Attributes.Add("OnClick", "return fnConfirmRec('A')")
                Me.butReject.Attributes.Add("OnClick", "return fnConfirmRec('R')")

                ''Get Default Value
                hdnCertID.Value = Request("CertID")

                ''get Window Login ID
                Dim arrWin As Array
                If Not Request.ServerVariables("logon_user") Is Nothing Then
                    arrWin = Request.ServerVariables("logon_user").Split("\")

                    '****** For Showing Login Details *******

                    'Dim UsrID As String
                    'UsrID = Request("loginid")
                    'If UsrID = "" And arrWin(1).ToString() = "" Then
                    If (arrWin(1).ToString() <> "") Then
                        'Response.Redirect("login.asp")
                        If fnCheckLogin(arrWin(1), "") = False Then
                            Dim strJavaScript As String = ""
                            strJavaScript = "<script language = 'Javascript'>" & _
                                            "parent.location.href='../common/logout.aspx';" & _
                                            "</script>"
                            Response.Write(strJavaScript)
                            Exit Sub
                        End If
                    End If

                Else
                    Dim strJavaScript As String = ""
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "parent.location.href='../common/logout.aspx';" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                    Exit Sub
                End If

                ''Display Record
                fnPopulateRecords()

                ''get Window Login ID
             
                Dim strLoginId As String = arrWin(1)
                'Dim strLoginId As String = Session("LoginID")
                If hdnMore5hkF.Value = "N" Then
                    ''For Tot NBV <= 500,000, checking for "just approving officer allow to approve/reject"
                    If Not clsDisMng.fnDisMng_RedundCheckAppOff("0", strLoginId, hdnCertID.Value, "E") Then
                        Response.Redirect("CRCert_NotAuthorize.aspx?CallFrm=R")
                        Exit Sub
                    End If
                Else
                    Response.Redirect("CRCert_NotAuthorize.aspx?CallFrm=R")
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Dim objRdr As SqlDataReader
        Try
            Dim strStatus As String = ""
            Dim strPath1 As String = gRedunPath & "R" & hdnCertID.Value & "_1.pdf"
            Dim strPath2 As String = gRedunPath & "R" & hdnCertID.Value & "_2.pdf"
            Dim strPath3 As String = gRedunPath & "R" & hdnCertID.Value & "_3.pdf"
            Dim dtFile As DataTable

            ''Get Certificate Records
            objRdr = clsDisMng.fnDisMng_GetRedundRecords(hdnCertID.Value)
            If Not objRdr Is Nothing Then

                dtFile = New DataTable()
                dtFile = clsDisMng.fnRedundancyFileGetRec(0, hdnCertID.Value, "N").Tables(0)

                For i As Integer = 0 To dtFile.Rows.Count - 1
                    spanOtherSupportingDoc.InnerHtml += "<a id=hlFile href='" & gRedunPath & dtFile.Rows(i)("fld_FileSaveName").ToString() & "' target=CondemnFile3 >" & _
                                                 "View " & dtFile.Rows(i)("fld_FileName").ToString() & _
                                                 "</a>, "
                Next

                If objRdr.HasRows Then
                    objRdr.Read()

                    hdnCertStatus.Value = CStr(objRdr("fld_RDCertStatus"))
                    hdnDipMethod.Value = CStr(objRdr("fld_DisposalMethod"))
                    hdnMore5hkF.Value = CStr(objRdr("fld_RDMore5hkF"))

                    lblCertSNo.Text = CStr(objRdr("fld_RDSNo"))
                    strStatus = CStr(objRdr("fld_RDCertStatus"))

                    If strStatus = "P" Then
                        lblCertStatus.Text = "Pending"

                        spanCertLink.InnerHtml = "<a id=hlFile href='" & strPath1 & "' target=RedundFile1 >" & _
                                                 "View certificate" & _
                                                 "</a>"

                        divCompDisposalView.Visible = False
                        divReceiverView.Visible = False
                        divReasonRejectField.Visible = True
                        divReasonRejectView.Visible = False
                        butApprove.Visible = True
                        butReject.Visible = True
                        lblMOFSysApp.Visible = False

                    ElseIf strStatus = "A" Then
                        lblCertStatus.Text = "Approved"

                        spanCertLink.InnerHtml = "<a id=hlFile href='" & strPath2 & "' target=RedundFile1 >" & _
                                                 "View certificate" & _
                                                 "</a>"

                        divCompDisposalView.Visible = False
                        divReceiverView.Visible = False
                        divReasonRejectField.Visible = False
                        divReasonRejectView.Visible = False
                        butApprove.Visible = False
                        butReject.Visible = False
                        If hdnMore5hkF.Value = "N" Then
                            lblMOFSysApp.Visible = False
                        Else
                            lblMOFSysApp.Visible = True
                        End If


                    ElseIf strStatus = "R" Then
                        lblCertStatus.Text = "Reject"

                        spanCertLink.InnerHtml = "<a id=hlFile href='" & strPath2 & "' target=RedundFile1 >" & _
                                                 "View certificate" & _
                                                 "</a>"

                        divCompDisposalView.Visible = False
                        divReceiverView.Visible = False
                        divReasonRejectField.Visible = False
                        divReasonRejectView.Visible = True
                        butApprove.Visible = False
                        butReject.Visible = False
                        If hdnMore5hkF.Value = "N" Then
                            lblMOFSysApp.Visible = False
                        Else
                            lblMOFSysApp.Visible = True
                        End If

                    ElseIf strStatus = "D" Then
                        lblCertStatus.Text = "Disposed"

                        spanCertLink.InnerHtml = "<a id=hlFile href='" & strPath3 & "' target=RedundFile1 >" & _
                                                                         "View certificate" & _
                                                                         "</a>"

                        divCompDisposalView.Visible = True
                        If hdnDipMethod.Value = "5" Then
                            divReceiverView.Visible = False
                        Else
                            divReceiverView.Visible = True
                        End If
                        divReasonRejectField.Visible = False
                        divReasonRejectView.Visible = False
                        butApprove.Visible = False
                        butReject.Visible = False
                        If hdnMore5hkF.Value = "N" Then
                            lblMOFSysApp.Visible = False
                        Else
                            lblMOFSysApp.Visible = True
                        End If

                    End If

                    lblDept.Text = CStr(objRdr("fld_DeptName")) & " (" & CStr(objRdr("fld_DeptCode")) & ")"
                    lblRevParty.Text = CStr(objRdr("fld_ReceiverParty"))
                    lblTotNBV.Text = CStr(objRdr("fld_TotNBV"))

                    If CStr(objRdr("fld_DisposalMethod")) = "1" Then
                        lblDispMethod.Text = "Donation to another Department"
                    ElseIf CStr(objRdr("fld_DisposalMethod")) = "2" Then
                        lblDispMethod.Text = "Donation to a non-Government body or Statutory Board"
                    ElseIf CStr(objRdr("fld_DisposalMethod")) = "3" Then
                        lblDispMethod.Text = "Normal Sale"
                    ElseIf CStr(objRdr("fld_DisposalMethod")) = "4" Then
                        lblDispMethod.Text = "Trade-in"
                    ElseIf CStr(objRdr("fld_DisposalMethod")) = "5" Then
                        lblDispMethod.Text = "Write-off"
                    End If

                    lblReason.Text = Replace(CStr(objRdr("fld_RDReason")), vbCrLf, "<br>")

                    lblAppOff.Text = CStr(objRdr("fld_AppOffName"))
                    lblAODesg.Text = CStr(objRdr("fld_AppOffDesg"))
                    lblAODt.Text = fnCheckNull(objRdr("fld_AppOffDt"))

                    lblRequestor.Text = CStr(objRdr("fld_RequestorName"))
                    lblReqDesg.Text = CStr(objRdr("fld_RequestorDesg"))
                    lblReqDt.Text = CStr(objRdr("fld_RequestorDt"))

                    lblMOFName.Text = CStr(objRdr("fld_MOFName"))
                    lblMOFDesg.Text = CStr(objRdr("fld_MOFDesg"))
                    lblMOFEmail.Text = CStr(objRdr("fld_MOFEmail"))
                    lblMOFDt.Text = fnCheckNull(objRdr("fld_MOFDt"))

                    lblCDispName.Text = fnCheckNull(objRdr("fld_DisposalName"))
                    lblCDispDesg.Text = fnCheckNull(objRdr("fld_DisposalDesg"))
                    lblCDispDt.Text = fnCheckNull(objRdr("fld_DisposalDt"))

                    lblRevName.Text = fnCheckNull(objRdr("fld_ReceiverName"))
                    lblRevDesg.Text = fnCheckNull(objRdr("fld_ReceiverDesg"))
                    lblRevDt.Text = fnCheckNull(objRdr("fld_ReceiverDt"))
                    lblRevEmail.Text = fnCheckNull(objRdr("fld_ReceiverEmail"))

                    lblRejReason.Text = Replace(fnCheckNull(objRdr("fld_RejectReason")), vbCrLf, "<br>") 'fnCheckNull(objRdr("fld_RejectReason"))

                End If
            End If
            objRdr.Close()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Function

    Protected Sub butReject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReject.Click
        Try
            Dim intRetVal As Integer
            Dim strCertID As String = hdnCertID.Value

            ''*** Reject Certificate 
            intRetVal = clsDisMng.fnDisMng_RedundCertReject( _
                            strCertID, Trim(txtRejReason.Text), "0", "E")

            If intRetVal > 0 Then
                ''Generate  PDF
                Dim strPath As String = gRedunPath & "R" & strCertID & "_2.pdf"
                fnGenPDF(strCertID, Server.MapPath(strPath))

                ''store Redundancy Cert to eRegistry
                Dim strCertIDeReg As String = "R" & strCertID & "_2.pdf"
                ''Check whether PDF File generated
                If File.Exists(Server.MapPath(strPath)) Then
                    ''Store Redundancy Cert to eRegistry is going here...

                Else
                    lblErrorMessage.Text = "Certificate of Redundancy generate failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                    Exit Sub
                End If

                Me.fnEmail("Rejected")
                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Certificate of Redundancy with S/No: " & lblCertSNo.Text & " has been rejected.")

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + "Certificate of Redundancy reject successfully." + "');" & _
                                "document.location.href='CRCert_RedundancyEmail.aspx?CertID=" & strCertID & "';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                lblErrorMessage.Text = "Certificate of Redundancy reject failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butApprove.Click
        Try
            Dim intRetVal As Integer
            Dim strCertID As String = hdnCertID.Value

            ''*** Approve Certificate 
            intRetVal = clsDisMng.fnDisMng_RedundCertApprove( _
                            strCertID, "0", "E")

            If intRetVal > 0 Then
                ''Generate  PDF
                Dim strPath As String = gRedunPath & "R" & strCertID & "_2.pdf"
                fnGenPDF(strCertID, Server.MapPath(strPath))

                ''store Redundancy Cert to eRegistry
                Dim strCertIDeReg As String = "R" & strCertID & "_2.pdf"
                ''Check whether PDF File generated
                If File.Exists(Server.MapPath(strPath)) Then
                    ''Store Redundancy Cert to eRegistry is going here...

                Else
                    lblErrorMessage.Text = "Certificate of Redundancy generate failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                    Exit Sub
                End If

                Me.fnEmail("Approved")
                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Certificate of Redundancy with S/No: " & lblCertSNo.Text & " has been approved.")

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + "Certificate of Redundancy approved successfully." + "');" & _
                                "document.location.href='CRCert_RedundancyEmail.aspx?CertID=" & strCertID & "';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                lblErrorMessage.Text = "Certificate of Redundancy approve failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnGenPDF(ByVal strRedunID As String, ByVal strFullPath As String)
        Try
            Dim ds As New DataSet()
            ds = clsDisMng.fnDisMng_GetRedundRecords4Rpt(strRedunID)

            If ds.Tables(0).Rows.Count > 0 Then
                Dim myReportDocument As New ReportDocument()
                myReportDocument.Load(Server.MapPath("ReportFile\RedundancyCert.rpt"))
                myReportDocument.SetDataSource(ds.Tables(0))
                Dim DiskOpts2 As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions()
                myReportDocument.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile
                DiskOpts2.DiskFileName = strFullPath
                myReportDocument.ExportOptions.DestinationOptions = DiskOpts2
                myReportDocument.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                myReportDocument.Export()

                myReportDocument.Close()
                myReportDocument.Dispose()
                GC.Collect()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function fnCheckLogin(ByVal strLoginID As String, _
                            ByVal strPwd As String) As Boolean
        Try
            Dim intUsrId As Integer = "0"
            Dim strUsrName As String = ""
            Dim strAccess As String = ""
            Dim strLSuccessF As String = ""
            Dim strAdminF As String = ""
            Dim intLoginUsrID As Integer = "0"

            clsCommon.fnLoginValidation(strLoginID, strPwd, intUsrId, strUsrName, _
                                        strAccess, strLSuccessF, strAdminF, intLoginUsrID)
            If strLSuccessF = "Y" Then ''Login Successfully
                Session("Name") = strUsrName            '' Full Name of the User
                Session("UsrID") = intUsrId             '' User Id in Database
                Session("LoginID") = strLoginID         '' Login User Id   'UsrMng|grp|delete,&nbsp;
                Session("AR") = strAccess
                Session("AdminF") = strAdminF
                Session("LoginUsersID") = intLoginUsrID

                ''**** Insert Audit Trail
                '   clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "User Login")

                Return True
                'Server.Transfer("index.aspx")
            Else
                Return False
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Sub fnEmail(ByVal Type As String)
        Dim objRdr As SqlDataReader
        Try
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""

            Dim strEmailTo As String = ""
            Dim strEmailCC As String = ""
            Dim strMailBody As String = ""

            ''*** Get Email Details from database
            objRdr = clsDisMng.fnDisMng_GetRedundRequesterEmail(hdnCertID.Value)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()

                    strMailSubject = "[" & Type & "] Certificate of Redundancy S/No:" & lblCertSNo.Text
                    strEmailTo = CStr(objRdr("fld_email"))

                    ''*** If Email Active, checking for send email
                    ''--> remove duplicate email ids in the To List and CC List.
                    'strEmailTo = fnRemoveNameFromEmailAdd(strEmailTo)
                    'strEmailTo = fnRemoveDuplicates(strEmailTo, ";")
                    'strEmailCC = fnRemoveNameFromEmailAdd(strEmailCC)
                    'strEmailCC = fnRemoveDuplicates2(strEmailTo, strEmailCC, ";")

                    ''--> Get Email Body
                    strMailBody = "Dear Sir/Madam,<br><br><br>"
                    If Type = "Approved" Then
                        strMailBody += "The above Certificate of Redundancy has been approved.<br><br>"
                    Else
                        strMailBody += "The above Certificate of Redundancy has been rejected.<br><br>"
                        strMailBody += "Reason for Reject:-<br> "
                        strMailBody += Me.txtRejReason.Text.Replace(vbCrLf, "<br>")
                    End If


                    strMailBody += "<BR><br><br>Thanks and Regards,"
                    strMailBody += "<BR>File and Asset Management System"
                    strMailBody += "<BR><br><br>* This is a computer generated e-mail, please do not reply to it."

                    ''--> Send Email
                    If Trim(strEmailTo) <> "" Or Trim(strEmailCC) <> "" Then
                        fnSendEmail(strEmailTo, strEmailCC, "", strMailSubject, strMailBody, "", "H")
                    End If

                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub
End Class