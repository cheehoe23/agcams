<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CRAsset_Search.aspx.vb" Inherits="AMS.CRAsset_Search" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>	
	<LINK href="../common/ComboBox/styles/contactsearch/combo.css" type="text/css" rel="Stylesheet">
     <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
    <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />

	<script type="text/javascript">

	    $(document).ready(function () {
	        SearchText("cbOwner", "hfOwner");
	    });
	    function SearchText(obj1, obj2) {
	        $("#" + obj1).autocomplete({
	            source: function (request, response) {
	                $.ajax({
	                    url: '<%#ResolveUrl("~/DisposeMng/CRAsset_Search.aspx/GetUsersList") %>',
	                    data: "{ 'prefix': '" + request.term + "'}",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    success: function (data) {
	                        response($.map(data.d, function (item) {
	                            return {
	                                label: item.split('~')[0],
	                                val: item.split('~')[1]
	                            }
	                        }))
	                    },
	                    error: function (response) {
	                        alert(response.responseText);
	                    },
	                    failure: function (response) {
	                        alert(response.responseText);
	                    }
	                });
	            },
	            select: function (e, i) {
	                $("#" + obj2).val(i.item.val);
	            },
	            minLength: 0
	        });
	    }

	    function chkFrm() {
			var foundError = false;
		    
		    //validate purchase from date
			if (!foundError && gfnCheckDate(document.Frm_AssetSearch.txtPurchaseFrmDt, "Purchase From Date", "O") == false) {
				foundError=true
			}
			
			//validate purchase to date
			if (!foundError && gfnCheckDate(document.Frm_AssetSearch.txtPurchaseToDt, "Purchase To Date", "O") == false) {
				foundError=true
			}
			
			//validate Warranty Expiry from Date
		    if (!foundError && gfnCheckDate(document.Frm_AssetSearch.txtWarExpFrmDt, "Warranty Expiry From Date", "O") == false) {
				foundError=true
			}
			
			//validate Warranty Expiry to Date
		    if (!foundError && gfnCheckDate(document.Frm_AssetSearch.txtWarExpToDt, "Warranty Expiry To Date", "O") == false) {
				foundError=true
			}
				 
 			if (!foundError){
 			   return true;
 			}
			else
				return false;
		}
	</script>
</head>
<body>
    <form id="Frm_AssetSearch" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Dispose Management : <asp:Label ID=lblHeader runat=server></asp:Label></B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
					
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
																													
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																				    <TR>
																						<TD valign="middle" width="35%">
																						    <FONT class="DisplayTitle">Asset ID (AMS) : </FONT>
																						</TD>
																						<TD width="65%">
																							<asp:TextBox id="txtAssetIDAMS" maxlength="20" Runat="server"></asp:TextBox>
																						</TD>
																					</TR>
																	                <TR>
																		                <TD valign="middle" width="35%">
																		                    <FONT class="DisplayTitle">Asset ID (NFS) : </FONT>
																		                </TD>
																		                <TD width="65%">
																			                <asp:TextBox id="txtAssetIDNFS" maxlength="20" Runat="server"></asp:TextBox>
																		                </TD>
																	                </TR>
																					<TR>
																						<TD valign="middle" width="35%">
																						    <FONT class="DisplayTitle">Asset Type : </FONT>
																						</TD>
																						<TD width="65%">
																						    <asp:DropDownList ID="ddlAssetType" Runat="server"></asp:DropDownList>
																						</TD>
																					</TR>
																			        <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Asset Category : </FONT>
																					    </TD>
																					    <TD>
																					       <asp:DropDownList ID="ddlAssetCat" AutoPostBack=true Runat="server"></asp:DropDownList>
																						</TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Asset Subcategory : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlAssetSubCat" Runat="server"></asp:DropDownList>
																						</TD>
																				    </TR>
																				    <TR>
																						<TD>
																						    <FONT class="DisplayTitle">Controlled Item : </FONT>
																						</TD>
																						<TD>
																						    <asp:DropDownList ID="ddlCtrlItem" Runat="server"></asp:DropDownList>
																						</TD>
																					</TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Asset Status : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlStatus" Runat="server"></asp:DropDownList>
																						</TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Purchase Date : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtPurchaseFrmDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_AssetSearch.txtPurchaseFrmDt, document.Frm_AssetSearch.txtPurchaseToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtPurchaseToDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_AssetSearch.txtPurchaseFrmDt, document.Frm_AssetSearch.txtPurchaseToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Warranty Expiry Date : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtWarExpFrmDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_AssetSearch.txtWarExpFrmDt, document.Frm_AssetSearch.txtWarExpToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtWarExpToDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_AssetSearch.txtWarExpFrmDt, document.Frm_AssetSearch.txtWarExpToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																	                <TR>
																	                    <TD width="35%">
																	                        <FONT class="DisplayTitle">Department : </FONT>
																	                    </TD>
																	                    <TD width="65%">
																		                    <asp:DropDownList ID="ddlDepartment" Runat="server"></asp:DropDownList>
																	                    </TD>
																                    </TR>
																                    <TR>
																	                    <TD>
																	                        <FONT class="DisplayTitle">Location : </FONT>
																	                    </TD>
																	                    <TD>
																		                    <asp:DropDownList ID="ddlLocation" Runat="server" AutoPostBack=true></asp:DropDownList>
																	                    </TD>
																                    </TR>
																                    <TR>
																	                    <TD>
																	                        <FONT class="DisplayTitle">Sub Location : </FONT>
																	                    </TD>
																	                    <TD>
																		                    <asp:DropDownList ID="ddlLocSub" Runat="server"></asp:DropDownList>
																	                    </TD>
																                    </TR>
																                    <TR>
																	                    <TD>
																	                        <FONT class="DisplayTitle">Assigned Owner : </FONT>
																	                    </TD>
																	                    <TD>

                                                                                                            <asp:TextBox ID="cbOwner" runat="server" Width="250px"></asp:TextBox>

																	                    </TD>
																                    </TR>      
																					<TR>
																						<TD valign="middle" width="35%">
																						    <FONT class="DisplayTitle">Temporary Asset : </FONT><img src="../images/question.jpg" alt="Temporary Assets are asset that are pending creation from NFS" />
																						</TD>
																						<TD width="65%">
																						    <asp:DropDownList ID="ddlTempAsset" Runat="server"></asp:DropDownList>
																						</TD>
																					</TR>
																					<TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:Button id="butSearch" Text="Search" Runat="Server" />&nbsp;
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
															
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
			<!-- Start: Hidden Fields -->
			<!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
				src="../common/DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="Z-INDEX:101; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px">
			</iframe>
			<input type="hidden" id="hdnCallFrm" runat="server">
            <input type="hidden" id="hfOwner" runat="server">
			<!-- End  : Hidden Fields -->
		</form>
</body>
</html>
