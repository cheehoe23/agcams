#Region "Information Section"
' ****************************************************************************************************
' Description       : Condemnation Asset 
' Purpose           : Condemnation Asset Information
' Date              : 11/11/2008
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Web.Services

#End Region

Partial Public Class CRAsset_Condemn
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            Me.Header.DataBind()

            If Not Page.IsPostBack Then
                Me.butCondemn.Attributes.Add("OnClick", "return chkFrm()")
                Me.butAddImages.Attributes.Add("OnClick", "return mfnOpenAddImagesWindow()")

                ''Get Default Value
                'hdnAssetIDs.Value = Session("DisAssets") ' Request("AIDs")
                hdnAddFileF.Value = "N"

                ''Populate Control
                fnPopulateCtrl()

                ''Display Record
                fnPopulateRecords()
            End If

            '' Check "Add New File Images"
            If hdnAddFileF.Value = "Y" Then
                fnPopulateFileImages()

                ''set back to default
                hdnAddFileF.Value = "N"
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False)
            If Session("AdminF") = "N" Then ddlDepartment.Items.Remove(ddlDepartment.Items.FindByValue("-1"))

            ''Get Approving Officer
            cbAppOff.DataBind()

            ''Get Requestor
            cbRequestor.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Dim objRdr As SqlDataReader
        Try
            Dim strTotNBV As String = ""

            ''Get Asset Records
            'dgAsset.DataSource = clsAsset.fnAssetGetAssetRecords(hdnAssetIDs.Value, "fldAssetBarcode", "ASC")
            dgAsset.DataSource = clsDisMng.fnDisMng_GetAssetRecords(Session("DisAssets"), "fldAssetBarcode", "ASC", strTotNBV)
            dgAsset.DataBind()
            If Not dgAsset.Items.Count > 0 Then ''Not Records found
                butCondemn.Enabled = False
                butReset.Enabled = False
                lblErrorMessage.Text = "There is no asset record."
                lblErrorMessage.Visible = True
            End If

            ''Get Tot NBV
            If strTotNBV <> "" Then
                lblTotNBV.Text = strTotNBV
            Else
                lblTotNBV.Text = "0.00"
            End If

            ''Get Default Approving Officer

            objRdr = clsGeneralSetting.fnGrlSetGetGeneralSettingInfo("P", "Condemn - Approving Officer", "2")
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()
                    cbAppOff.Text = CStr(objRdr("fld_systFieldsStr2")) & " (" + CStr(objRdr("fld_department")) & ")"
                End If
            End If
            objRdr.Close()

            ''Get Default Department
            If Session("AdminF") = "Y" Then
                objRdr = clsGeneralSetting.fnGrlSetGetGeneralSettingInfo("P", "Condemn - Department", "2")
                If Not objRdr Is Nothing Then
                    If objRdr.HasRows Then
                        objRdr.Read()
                        ddlDepartment.SelectedValue = CStr(objRdr("fld_systFieldsStr"))
                    End If
                End If
                objRdr.Close()
            End If
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Function

    Private Sub fnPopulateFileImages()
        Try
            ''Get Records
            dgCondemnFileImages.DataSource = clsDisMng.fnCondemnFileGetRec(Session("UsrID"), "0", "Y")
            dgCondemnFileImages.DataBind()
            If Not dgCondemnFileImages.Items.Count > 0 Then ''Not Records found
                dgCondemnFileImages.Visible = False
            Else
                dgCondemnFileImages.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Server.Transfer("CRAsset_Condemn.aspx")
    End Sub

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Protected Sub butCondemn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCondemn.Click
        Try
            Dim intRetVal As Integer
            Dim strMsg As String
            Dim strUsrExistF As String = "N"
            Dim strCondemnID As String = ""
            Dim strSNum As String = ""
            Dim strABarcode As String = ""

            ''*** Get Value
            Dim strAssetIDs As String = Session("DisAssets")
            Dim strDept As String = IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue)
            Dim strCondemnReason As String = Trim(txtCondemnReason.Text)
            Dim strDisposalMethod As String = Trim(txtDisposalMethod.Text)
            Dim strTotProceed As String = Trim(txtTotProceed.Text)
            Dim strTotRemovalC As String = Trim(txtTotRemovalC.Text)
            Dim strAppOffName As String = Trim(cbAppOff.Text)
            Dim strAppOffLID As String = ""
            Dim strAppOffDesg As String = Trim(txtAODesg.Text)
            Dim strReqName As String = Trim(cbRequestor.Text)
            Dim strReqLID As String = ""
            Dim strReqDesg As String = Trim(txtReqDesg.Text)
            Dim strReqDt As String = Trim(txtReqDt.Text)

            ''*** Checking For :-
            ''1. Approving Officer
            If strAppOffName <> "" Then
                strUsrExistF = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(strAppOffName, strAppOffLID, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Approving Officer not found. Please select an existing Approving Officer."
                    lblErrorMessage.Visible = True
                    cbAppOff.Focus()
                    Exit Sub
                End If
            End If

            ''2. Requestor
            If strReqName <> "" Then
                strUsrExistF = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(strReqName, strReqLID, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Requestor not found. Please select an existing Requestor."
                    lblErrorMessage.Visible = True
                    cbRequestor.Focus()
                    Exit Sub
                End If
            End If

            ''*** Condmen Asset 
            intRetVal = clsDisMng.fnDisMng_CondemnAsset( _
                            strAssetIDs, strDept, _
                            strCondemnReason, strDisposalMethod, strTotProceed, strTotRemovalC, _
                            strAppOffLID, strAppOffDesg, _
                            strReqLID, strReqDesg, strReqDt, _
                            Session("UsrID"), strCondemnID, strSNum, strABarcode)

            If intRetVal > 0 Then
                strMsg = "Certificate of Condemnation generate successfully with S/No:" & strSNum & "."

                ''Generate Condemn PDF
                Dim strPath1 As String = gCondemnPath & "C" & strCondemnID & "_1a.pdf"
                Dim strPath2 As String = gCondemnPath & "C" & strCondemnID & "_2a.pdf"
                fnGenPDF1(strCondemnID, Server.MapPath(strPath1))
                fnGenPDF2(strCondemnID, Server.MapPath(strPath2))

                ''store Condemn Cert to eRegistry
                Dim strCertID1 As String = "C" & strCondemnID & "_1a.pdf" '""
                Dim strCertID2 As String = "C" & strCondemnID & "_2a.pdf" '""
                ''Check whether PDF File generated
                If File.Exists(Server.MapPath(strPath1)) And File.Exists(Server.MapPath(strPath2)) Then
                    ''Store Condemn Cert to eRegistry is going here...

                Else
                    lblErrorMessage.Text = "Certificate of Condemnation generate failed. Please contact your administrator."
                    lblErrorMessage.Visible = True
                    Exit Sub
                End If

                ''Send Email to Approving Officer 
                fnEmail4AppOff(strCondemnID, strCertID1, strCertID2, strSNum)

                ''Insert Audit Trail
                If Len(strABarcode) > 7904 Then
                    strABarcode = strABarcode.Substring(0, 7904) & ", ..."
                End If
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "<font class=DisplayAudit>[Certificate of Condemnation Generated]</font><br><b>S/No<b> : " & strSNum & "<br><b>Asset IDs Included</b> :-<br>" & strABarcode)

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "document.location.href='CRAsset_Search.aspx?CallType=C';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "Certificate of Condemnation generate failed."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnEmail4AppOff(ByVal strCondemnID As String, ByVal strCertID1 As String, ByVal strCertID2 As String, ByVal strSNum As String)
        Dim objRdr As SqlDataReader
        Try
            Dim strEmailActiveF As String = "N"
            Dim strMailSubject As String = ""
            Dim strEmailContent As String = ""

            Dim strEmailTo As String = ""
            Dim strEmailCC As String = ""
            Dim strMailBody As String = ""
            Dim dtFile As DataTable

            ''*** Get Email Details from database
            objRdr = clsDisMng.fnDisMng_GetEmailInfo4Condemn1(strCondemnID)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()

                    strEmailActiveF = CStr(objRdr("EmailActiveF"))
                    strMailSubject = CStr(objRdr("EmailSubject")) & " S/No:" & strSNum
                    strEmailContent = objRdr("EmailContent")
                    strEmailTo = CStr(objRdr("EmailTo"))

                    ''*** If Email Active, checking for send email
                    If strEmailActiveF = "Y" Then
                        ''--> remove duplicate email ids in the To List and CC List.
                        'strEmailTo = fnRemoveNameFromEmailAdd(strEmailTo)
                        'strEmailTo = fnRemoveDuplicates(strEmailTo, ";")
                        'strEmailCC = fnRemoveNameFromEmailAdd(strEmailCC)
                        'strEmailCC = fnRemoveDuplicates2(strEmailTo, strEmailCC, ";")

                        ''--> Get Email Body
                        strMailBody = "Dear Sir/Madam,<br><br><br>"
                        strMailBody += Replace(strEmailContent, vbCrLf, "<br>")
                        strMailBody += "<BR><br>Please refer to Condemnation Certificate for followup action:-<br>"
                        strMailBody += "Condemnation Certificate Part 1 : "
                        strMailBody += "<a id=hlFile1 href='" & gCondemnEReg() & strCertID1 & "' target=CondemnFile1 >"
                        strMailBody += "Click for view certificate"
                        strMailBody += "</a>"
                        strMailBody += "<br>"
                        strMailBody += "Condemnation Certificate Part 2 : "
                        strMailBody += "<a id=hlFile2 href='" & gCondemnEReg() & strCertID2 & "' target=CondemnFile2 >"
                        strMailBody += "Click for view certificate"
                        strMailBody += "</a>"
                        strMailBody += "<br>"

                        dtFile = New DataTable()
                        dtFile = clsDisMng.fnCondemnFileGetRec(Session("UsrID"), strCondemnID, "N").Tables(0)

                        For i As Integer = 0 To dtFile.Rows.Count - 1
                            strMailBody += "Condemnation other supporting document Part " & i + 3 & ": "
                            strMailBody += "<a id=hlFile3 href='" & gCondemnEReg() & dtFile.Rows(i)("fld_FileSaveName").ToString() & "' target=CondemnFile3 >"
                            strMailBody += "Click for view other supporting document"
                            strMailBody += "</a>"
                            strMailBody += "<br>"
                        Next

                        strMailBody += "<br>"
                        strMailBody += "For approval of certificate, click "
                        strMailBody += "<a id=hlCDApp href='" & gCondemnAppPage() & strCondemnID & "' target=CondemnApp >"
                        strMailBody += "here"
                        strMailBody += "</a>."
                        strMailBody += "<BR><br><br>Thanks and Regards,"
                        strMailBody += "<BR>File and Asset Management System"
                        strMailBody += "<BR><br><br>* This is a computer generated e-mail, please do not reply to it."

                        ''--> Send Email
                        If Trim(strEmailTo) <> "" Or Trim(strEmailCC) <> "" Then
                            fnSendEmail(strEmailTo, strEmailCC, "", strMailSubject, strMailBody, "", "H")
                        End If
                    Else
                        Exit Sub
                    End If
                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Public Sub fnGenPDF1(ByVal strCondemnID As String, ByVal strFullPath As String)
        Try
            Dim ds As New DataSet("tbl_CondemnMst")
            ds = clsDisMng.fnDisMng_GetCondemnRecords(strCondemnID, "M")

            If ds.Tables(0).Rows.Count > 0 Then
                Dim myReportDocument As New ReportDocument()
                myReportDocument.Load(Server.MapPath("ReportFile\CondemnCert.rpt"))
                myReportDocument.SetDataSource(ds.Tables(0))
                myReportDocument.SetParameterValue("CondemnID", strCondemnID)
                Dim DiskOpts2 As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions()
                myReportDocument.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile
                DiskOpts2.DiskFileName = strFullPath
                myReportDocument.ExportOptions.DestinationOptions = DiskOpts2
                myReportDocument.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                myReportDocument.Export()

                myReportDocument.Close()
                myReportDocument.Dispose()
                GC.Collect()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnGenPDF2(ByVal strCondemnID As String, ByVal strFullPath As String)
        Try
            Dim ds As New DataSet()
            ds = clsDisMng.fnDisMng_GetCondemnRecords4Rpt(strCondemnID)

            If ds.Tables(0).Rows.Count > 0 Then
                Dim myReportDocument As New ReportDocument()
                myReportDocument.Load(Server.MapPath("ReportFile\CondemnCert2.rpt"))
                myReportDocument.SetDataSource(ds.Tables(0))
                Dim DiskOpts2 As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions()
                myReportDocument.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile
                DiskOpts2.DiskFileName = strFullPath
                myReportDocument.ExportOptions.DestinationOptions = DiskOpts2
                myReportDocument.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                myReportDocument.Export()

                myReportDocument.Close()
                myReportDocument.Dispose()
                GC.Collect()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

    Public Function fnGetFileName(ByVal strFileNameSave As String) As String
        Try
            Dim strRetVal As String = ""
            strRetVal += "<a id=hlFile href='../tempFile/CondemnFile/" & strFileNameSave & "' target=AssetFile >" & _
                         "<img id=imgView src=../images/audit.gif alt='Click for view file.' border=0 >" & _
                         "</a>"

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub dgCondemnFileImages_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCondemnFileImages.DeleteCommand
        Try
            Dim retval As Integer = "0"
            Dim strFileIDDel As String = e.Item.Cells(0).Text
            Dim strFileNameDel As String = e.Item.Cells(1).Text
            Dim strFileLocation As String = Server.MapPath("..\tempFile\CondemnFile") & "\" & strFileNameDel

            ''Start: Delete selected records from database
            retval = clsDisMng.fnCondemnFileDeleteRec(Session("UsrID"), strFileIDDel, "DP", "")
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 Then
                ''Delete File in AssetFile folder
                If File.Exists(strFileLocation) Then
                    File.Delete(strFileLocation)
                End If

                ''populate File Images
                fnPopulateFileImages()
            Else
                lblErrorMessage.Text = "File delete failed."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

End Class