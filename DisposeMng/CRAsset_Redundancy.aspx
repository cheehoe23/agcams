<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CRAsset_Redundancy.aspx.vb" Inherits="AMS.CRAsset_Redundancy" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<%@ Register TagPrefix="fluent" Namespace="Fluent.MultiLineTextBoxValidator" Assembly="Fluent.MultiLineTextBoxValidator" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet" />
	<script language="JavaScript" type="text/javascript" src="../Common/CommonJS.js"></script>
	<link href="../common/ComboBox/styles/contactsearch/combo.css" type="text/css" rel="Stylesheet" />

    <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
    <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />

    <style type="text/css">
    .modal
    {
        position: fixed;
        left: 0;        
        z-index: 99;
        opacity: 0.8;
        filter: alpha(opacity=80);
        -moz-opacity: 0.8;
        min-height: 100%;
        width: 100%;
    }
    .loading
    {
        font-family: Arial;
        font-size: 10pt;
        border: 5px solid #67CFF5;
        width: 200px;
        height: 100px;
        display: none;
        position: fixed;
        background-color: White;
        z-index: 999;
    }
</style>

	<script language="javascript">
	    $(document).ready(function () {
	        SearchText("cbAppOff", "hfAppOff");
	        SearchText("cbRequestor", "hfRequestor");
	    });

	    function ShowProgress() {
	        setTimeout(function () {
	            var modal = $('<div />');
	            modal.addClass("modal");
	            $('body').append(modal);
	            var loading = $(".loading");
	            loading.show();
	            var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
	            var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
	            loading.css({ top: top, left: left });
	        }, 200);
	    }

	    function SearchText(obj1, obj2) {
	        $("#" + obj1).autocomplete({
	            source: function (request, response) {
	                $.ajax({
	                    url: '<%#ResolveUrl("~/DisposeMng/CRAsset_Redundancy.aspx/GetUsersList") %>',
	                    data: "{ 'prefix': '" + request.term + "'}",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    success: function (data) {
	                        response($.map(data.d, function (item) {
	                            return {
	                                label: item.split('~')[0],
	                                val: item.split('~')[1]
	                            }
	                        }))
	                    },
	                    error: function (response) {
	                        alert(response.responseText);
	                    },
	                    failure: function (response) {
	                        alert(response.responseText);
	                    }
	                });
	            },
	            select: function (e, i) {
	                $("#" + obj2).val(i.item.val);
	            },
	            minLength: 0
	        });

	    }

	    function mfnOpenAddImagesWindow() {
	        //open pop-up window
	        window.open('../common/AddNewFile.aspx?CallFrm=AddRedundancy', 'AddFile', 'width=450,height=250,Top=0,left=0,scrollbars=1');
	        return false;
	    }

		    function chkFrm() {
				var foundError = false;
				
				//validate department
			    if (!foundError && document.Frm_RedCodemn.ddlDepartment.value == "-1") {
				    foundError=true
				    document.Frm_RedCodemn.ddlDepartment.focus()
				    alert("Please select the Department.")
			    }
			    
			    //validate Receiving Party if donated
				if (!foundError && gfnIsFieldBlank(document.Frm_RedCodemn.txtRevParty)) {
				    foundError=true;
				    document.Frm_RedCodemn.txtRevParty.focus();
				    alert("Please enter Receiving Party if donated.");
			    } 
			    
			    //validate Disposal Method
			    if (!foundError && document.Frm_RedCodemn.ddlDispMethod.value == "0") {
				    foundError=true
				    document.Frm_RedCodemn.ddlDispMethod.focus()
				    alert("Please select the Disposal Method.")
			    }
			    
			    //validate Reasons for redundant
				if (!foundError && gfnIsFieldBlank(document.Frm_RedCodemn.txtReason)) {
				    foundError=true;
				    document.Frm_RedCodemn.txtReason.focus();
				    alert("Please enter Reasons for redundant.");
			    } 
			    			   		    
			    //validate Approving Officer
			    if (!foundError && document.Frm_RedCodemn.hdnMandatoryF.value == 'AO') {
				    if (!foundError && gfnIsFieldBlankValue(document.getElementById("cbAppOff").object.GetTextValue())) {
					    foundError=true;
					    alert("Please select Approving Officer.");
				    }
    				
				    //validate Approving Officer - Designation
				    if (!foundError && gfnIsFieldBlank(document.Frm_RedCodemn.txtAODesg)) {
				        foundError=true;
				        document.Frm_RedCodemn.txtAODesg.focus();
				        alert("Please enter Approving Officer Designation.");
			        }  	
			    }
		        
		        			    
			    //validate Requestor
				if (!foundError && gfnIsFieldBlankValue(document.getElementById("cbRequestor").object.GetTextValue())) {
					foundError=true;
					alert("Please select Requestor.");
				}
				
				//validate Requestor - Designation
				if (!foundError && gfnIsFieldBlank(document.Frm_RedCodemn.txtReqDesg)) {
				    foundError=true;
				    document.Frm_RedCodemn.txtReqDesg.focus();
				    alert("Please enter Requestor Designation.");
			    }  	
		        
		        //validate Requestor - date
			    if (!foundError && gfnCheckDate(document.Frm_RedCodemn.txtReqDt, "Requestor Date", "M") == false) {
				    foundError=true
			    }
			    
			    //validate MOF
				if (!foundError && document.Frm_RedCodemn.hdnMandatoryF.value == 'MOF') {
					//validate MOF Name 
				    if (!foundError && gfnIsFieldBlank(document.Frm_RedCodemn.txtMOFName)) {
				        foundError=true;
				        document.Frm_RedCodemn.txtMOFName.focus();
				        alert("Please enter MOF Name.");
			        } 
					
					//validate MOF - Designation
				    if (!foundError && gfnIsFieldBlank(document.Frm_RedCodemn.txtMOFDesg)) {
				        foundError=true;
				        document.Frm_RedCodemn.txtMOFDesg.focus();
				        alert("Please enter MOF Designation.");
			        } 
			       
			        //validate MOF - Email 
			        if (!foundError && !gfnIsFieldBlank(document.Frm_RedCodemn.txtMOFEmail)) {
			            if (!foundError && !gfnValidateEmail(document.Frm_RedCodemn.txtMOFEmail)) {
				            document.Frm_RedCodemn.txtMOFEmail.focus()
				            alert("Please enter a valid Email Address for MOF.")
				            foundError=true
			            }	    
		            }
		            	
//			        if (!foundError && gfnIsFieldBlank(document.Frm_RedCodemn.txtMOFEmail)) {
//			            foundError=true;
//			            document.Frm_RedCodemn.txtMOFEmail.focus();
//			            alert("Please enter MOF Email.");
//		            }
//		            if (!foundError && !gfnValidateEmail(document.Frm_RedCodemn.txtMOFEmail)) {
//				        document.Frm_RedCodemn.txtMOFEmail.focus()
//				        alert("Please enter a valid Email Address for MOF.")
//				        foundError=true
//			        }			        
				
				}
						        
 				if (!foundError){
 					var flag = false;
 				    flag = window.confirm("Are you sure you want to redundancy this record(s)?");
 				    return flag;
 				}
				else
					return false;
			}
	</script>
</head>
<body>
    <form id="Frm_RedCodemn" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Dispose Management : Redundancy Asset</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" height="400" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table3">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table4">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table5">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table6">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table7">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table9">
																				<TBODY align=left>
																				    <TR>
																					    <TD>
																					        <font color="red">*</font><FONT class="DisplayTitle">Department : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:DropDownList ID="ddlDepartment" Runat="server"></asp:DropDownList>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <font color="red">*</font><FONT class="DisplayTitle">Receiving Party if donated : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtRevParty" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD  width="35%">
																					        <font color="red">*</font><FONT class="DisplayTitle">Total Net Book Value [NBV] (S$) : </FONT>
																					    </TD>
																					    <TD width="65%">
																					        <asp:Label ID=lblTotNBV runat=server></asp:Label>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <font color="red">*</font><FONT class="DisplayTitle">Disposal Method : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:DropDownList ID="ddlDispMethod" Runat="server"></asp:DropDownList>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD valign="top" width="35%"><font color="red">*</font><FONT class="DisplayTitle">Reasons for redundant : </FONT>
																					    </TD>
																					    <TD width="65%">
																					        <asp:TextBox id="txtReason" maxlength="1000" TextMode="MultiLine" Columns="40" Rows="5" Runat="server"></asp:TextBox><br />
																					        <asp:textbox id="txtReasonWord" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					        <fluent:multilinetextboxvalidator id="MLLReason" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Reason to 1000 Characters." OutputControl="txtReasonWord" ControlToValidate="txtReason"></fluent:multilinetextboxvalidator>
																					    </TD>
																				    </TR>
																				    <TR><TD colSpan="2">&nbsp;</TD></TR>
																				    <TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Approving Officer Information</font>
																						                <br />(for Total NBV less than or equal to S$500,000)</TD>
																					</TR>
																				    <TR>
																					    <TD>
																					        <asp:label id="lblAOMdty1" runat=server><font color="red">*</font></asp:label><FONT class="DisplayTitle">Approving Officer : </FONT>
																					    </TD>
																					    <TD>
																					         
																					        <asp:TextBox ID="cbAppOff"  runat="server" Width="300px"></asp:TextBox>
																					         
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <asp:label id="lblAOMdty2" runat=server><font color="red">*</font></asp:label><FONT class="DisplayTitle">Designation : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtAODesg" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR><TD colSpan="2">&nbsp;</TD></TR>
																				    <TR>
																						<TD colSpan="2"><font class="DisplayTitleHeader">Requestor Information</font></TD>
																					</TR>
																				    <TR>
																					    <TD>
																					        <font color="red">*</font><FONT class="DisplayTitle">Requestor : </FONT>
																					    </TD>
																					    <TD>
																					         
																					        <asp:TextBox ID="cbRequestor"  runat="server" Width="300px"></asp:TextBox>
																					         
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle"><font color="red">*</font>Designation : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtReqDesg" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <font color="red">*</font><FONT class="DisplayTitle">Date : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:TextBox ID="txtReqDt" Width="120" Runat="server"></asp:TextBox>
																						    <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fSingleDtPop(document.Frm_RedCodemn.txtReqDt);return false;"
																							    HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select Date">
																						    </a>
																					    </TD>
																				    </TR>
																				    <TR><TD colSpan="2">&nbsp;</TD></TR>
																				    <TR>
																						<TD colSpan="2">
																						    <font class="DisplayTitleHeader">Ministry of Finance (MOF) Information</font> 
																						    <br />(for Total NBV more than S$500,000)</TD>
																					</TR>
																				    <TR>
																					    <TD>
																					        <asp:label id="lblMandty1" runat=server><font color="red">*</font></asp:label><FONT class="DisplayTitle">MOF : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:TextBox id="txtMOFName" maxlength="250" Runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <asp:label id="lblMandty2" runat=server><font color="red">*</font></asp:label><FONT class="DisplayTitle">Designation : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtMOFDesg" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <asp:label id="lblMandty3" runat=server></asp:label><FONT class="DisplayTitle">Email : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:TextBox id="txtMOFEmail" maxlength="250" Runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <tr>
																					    <TD valign=top>
																					        <FONT class="DisplayTitle">Image(s)/File(s) : </FONT>
																					    </TD>
																					    <TD>
																					        [<asp:LinkButton ID="butAddImages" runat="server">Add New File</asp:LinkButton>]
																						    <!--Datagrid for display record.-->
									                                                        <FONT class="DisplayTitle">
                                                                                            <span style="font-size: 10.0pt; line-height: 107%; font-family: &quot;Calibri&quot;,sans-serif; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA; color: #FF0000;">
                                                                                            <strong>File size limit is 2 MB</strong></span></FONT><asp:datagrid id="dgRedundancyFileImages" Runat="server" 
                                                                                                AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#CC9966" datakeyfield="fld_RDFileID" BackColor="White" BorderWidth="1px" CellPadding="4">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="#FFFFCC" Font-Bold="True" HorizontalAlign="Center"
											                                                        ></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn headertext="fld_RDFileID" datafield="fld_RDFileID"  Visible=False>
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="fld_FileSaveName" datafield="fld_FileSaveName"  Visible=False>
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                        </asp:boundcolumn>
										                                                            <asp:boundcolumn headertext="S/No" datafield="SNum" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" Height="10px"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="File Name" datafield="fld_FileName">
												                                                        <itemstyle width="70%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:templatecolumn ItemStyle-Width="10%" HeaderText="<IMG SRC=../images/audit.gif Border=0>" 
							                                                                            ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Center>
								                                                                        <itemtemplate>
									                                                                        <%#fnGetFileName(DataBinder.Eval(Container.DataItem, "fld_FileSaveName"))%>
								                                                                        </itemtemplate>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10%"></ItemStyle>
							                                                                        </asp:templatecolumn>
																									<asp:ButtonColumn Text="&lt;IMG SRC=../images/delete.gif Border=0&gt;"
																										HeaderText="&lt;IMG SRC=../images/delete.gif Border=0&gt;" CommandName="Delete">
                                                                                                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                                                    </asp:ButtonColumn>									
										                                                        </Columns>
									                                                        </asp:datagrid>
                                                                                        </TD>
																				    </tr>
																				    <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Asset Included</b></TD>
																					</TR>
																					<tr>
								                                                        <td vAlign="top" align="center" colspan=2>
									                                                        <!--Datagrid for display record.-->
									                                                        <asp:datagrid id="dgAsset" Runat="server" AlternatingItemStyle-Height="25"
										                                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                        BorderColor="#ffffff" datakeyfield="fld_AssetID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                        Height="25"></headerstyle>
										                                                        <Columns>
										                                                            <asp:boundcolumn HeaderText="Asset ID" datafield="fld_AssetID" Visible=false>
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn datafield="fldAssetBarcode" headertext="Asset ID" ItemStyle-Height="10">
												                                                        <itemstyle width="12%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Type" datafield="fld_AssetTypeStr">
												                                                        <itemstyle width="8%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Category" datafield="fld_CategoryName" >
												                                                        <itemstyle width="8%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Subcategory" datafield="fld_CatSubName" >
												                                                        <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>							
											                                                        <asp:boundcolumn headertext="Department" datafield="fld_DepartmentName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Location" datafield="fld_LocationName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Sub Location" datafield="fld_LocSubName" >
												                                                        <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Assigned Owner" datafield="fld_OwnerName" >
												                                                        <itemstyle width="12%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Status" datafield="fld_AssetStatusStr" >
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>		
											                                                        <asp:boundcolumn headertext="Remaining Useful Lifespan" datafield="fld_RemainUsefulLife">
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                        </asp:boundcolumn>
											                                                        <asp:boundcolumn headertext="Retirement Date" datafield="fld_condemnExpDt" Visible=false>
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>		
											                                                        <asp:boundcolumn headertext="NBV (S$)" datafield="fld_NBV" Visible=false >
												                                                        <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                        </asp:boundcolumn>												
										                                                        </Columns>
									                                                        </asp:datagrid>
								                                                        </td>
							                                                        </tr>
																					<TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:Button id="butRedundant" Text="Redundancy Asset" Runat="Server" OnClientClick = "ShowProgress()"/>&nbsp;
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
                            <div class="loading" align="center">
        Loading. Please wait.<br />
        <br />
        <img src="../images/loader.gif" alt="" />
    </div>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnAssetIDs" runat="server">
			<input type="hidden" id="hdnMandatoryF" runat="server">
			<!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
				src="../common/DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="Z-INDEX:101; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px">
			</iframe>
			<!-- End  : Hidden Fields -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
            <asp:HiddenField ID="hfAppOff" runat="server" />
            <asp:HiddenField ID="hfRequestor" runat="server" />
            <input type="hidden" id="hdnAddFileF" runat="server"> 
    </form>
</body>
</html>
