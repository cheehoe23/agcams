#Region "Information Section"
' ****************************************************************************************************
' Description       : Dispose Management - Settings
' Purpose           : Dispose Management - Settings Information
' Author            : See Siew
' Date              : 06/11/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Web.Services

#End Region

Partial Public Class DisSettings
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            'Me.Header.DataBind()

            If Not Page.IsPostBack Then
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")

                ''Populate Control
                'If (InStr(Session("AR"), "SysSet|DSet|upd") <> 0) Then
                fnPopulateCtrl()
                'End If

                ''Populate Record from backend
                fnGetRecordsFromDB()

                ''Checking for access right whether user not allow to update
                If (InStr(Session("AR"), "SysSet|DSet|upd") = 0) Then
                    'cbCDAppOff.Enabled = False
                    'cbCDCPDFinance.Enabled = False
                    'ddlCDDept.Enabled = False
                    'rdCDAOActive.Enabled = False
                    'txtCDAOSubj.Enabled = False
                    'txtCDAOContent.Enabled = False
                    'rdCDCFActive.Enabled = False
                    'txtCDCFSubj.Enabled = False
                    'txtCDCFContent.Enabled = False

                    'cbRDAppOff.Enabled = False
                    'cbRDCPDFinance.Enabled = False
                    'ddlRDDept.Enabled = False
                    'rdRDAOActive.Enabled = False
                    'txtRDAOSubj.Enabled = False
                    'txtRDAOContent.Enabled = False
                    'rdRDCFActive.Enabled = False
                    'txtRDCFSubj.Enabled = False
                    'txtRDCFContent.Enabled = False

                    'rdRDMOFActive.Enabled = False
                    'txtRDMOFSubj.Enabled = False
                    'txtRDMOFContent.Enabled = False
                    'rdRDDispActive.Enabled = False
                    'txtRDDispSubj.Enabled = False
                    'txtRDDispContent.Enabled = False

                    'rdRDRevActive.Enabled = False
                    'txtRDRevSubj.Enabled = False
                    'txtRDRevContent.Enabled = False

                    'butSubmit.Enabled = False
                    'butReset.Enabled = False
                End If

            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            Dim dsEmail As New DataSet
            dsEmail = fnGetUsrEmailDataSet("")

            ''Condemnation Settings
            ''--> Get Approving Officer 
            cbCDAppOff.DataBind()

            ''--> Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlCDDept, "fld_DepartmentID", "fld_DptCN", False)

            ''Redundancy Settings
            ''--> Get Approving Officer 
            cbRDAppOff.DataBind()

            ''--> Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlRDDept, "fld_DepartmentID", "fld_DptCN", False)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnGetRecordsFromDB()
        Dim objRdr As SqlDataReader
        Try
            objRdr = clsGeneralSetting.fnGrlSetGetGeneralSettingInfo("A", "", "2")
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        ''Check for Selected Header
                        Select Case CStr(objRdr("fld_systFieldsName"))
                            Case "Condemn - Approving Officer"
                                cbCDAppOff.Text = CStr(objRdr("fld_systFieldsStr2")) & " (" + CStr(objRdr("fld_department")) & ")"

                            Case "Condemn - CPD Finance"
                                cbCDCPDFinance.Text = CStr(objRdr("fld_systFieldsStr"))

                            Case "Condemn - Department"
                                ddlCDDept.SelectedValue = IIf(CStr(objRdr("fld_systFieldsStr")) = "" Or CStr(objRdr("fld_systFieldsStr")) = "0", "-1", CStr(objRdr("fld_systFieldsStr")))

                            Case "Condemn - Email for Approving Officer"
                                rdCDAOActive.SelectedValue = CStr(objRdr("fld_systFieldsFlg"))
                                txtCDAOSubj.Text = CStr(objRdr("fld_systFieldsEmailSubj"))
                                txtCDAOContent.Text = CStr(objRdr("fld_systFieldsEmailContent"))

                            Case "Condemn - Email for CPD Finance"
                                rdCDCFActive.SelectedValue = CStr(objRdr("fld_systFieldsFlg"))
                                txtCDCFSubj.Text = CStr(objRdr("fld_systFieldsEmailSubj"))
                                txtCDCFContent.Text = CStr(objRdr("fld_systFieldsEmailContent"))

                            Case "Redundancy - Approving Officer"
                                cbRDAppOff.Text = CStr(objRdr("fld_systFieldsStr2")) & " (" + CStr(objRdr("fld_department")) & ")"

                            Case "Redundancy - CPD Finance"
                                cbRDCPDFinance.Text = CStr(objRdr("fld_systFieldsStr"))

                            Case "Redundancy - Department"
                                ddlRDDept.SelectedValue = IIf(CStr(objRdr("fld_systFieldsStr")) = "" Or CStr(objRdr("fld_systFieldsStr")) = "0", "-1", CStr(objRdr("fld_systFieldsStr")))

                            Case "Redundancy - Email for Approving Officer"
                                rdRDAOActive.SelectedValue = CStr(objRdr("fld_systFieldsFlg"))
                                txtRDAOSubj.Text = CStr(objRdr("fld_systFieldsEmailSubj"))
                                txtRDAOContent.Text = CStr(objRdr("fld_systFieldsEmailContent"))

                            Case "Redundancy - Email for CPD Finance"
                                rdRDCFActive.SelectedValue = CStr(objRdr("fld_systFieldsFlg"))
                                txtRDCFSubj.Text = CStr(objRdr("fld_systFieldsEmailSubj"))
                                txtRDCFContent.Text = CStr(objRdr("fld_systFieldsEmailContent"))

                            Case "Redundancy - Email for MOF"
                                rdRDMOFActive.SelectedValue = CStr(objRdr("fld_systFieldsFlg"))
                                txtRDMOFSubj.Text = CStr(objRdr("fld_systFieldsEmailSubj"))
                                txtRDMOFContent.Text = CStr(objRdr("fld_systFieldsEmailContent"))

                            Case "Redundancy - Email for Disposal"
                                rdRDDispActive.SelectedValue = CStr(objRdr("fld_systFieldsFlg"))
                                txtRDDispSubj.Text = CStr(objRdr("fld_systFieldsEmailSubj"))
                                txtRDDispContent.Text = CStr(objRdr("fld_systFieldsEmailContent"))

                            Case "Redundancy - Email for Receiver"
                                rdRDRevActive.SelectedValue = CStr(objRdr("fld_systFieldsFlg"))
                                txtRDRevSubj.Text = CStr(objRdr("fld_systFieldsEmailSubj"))
                                txtRDRevContent.Text = CStr(objRdr("fld_systFieldsEmailContent"))


                        End Select
                    End While
                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Function fnGetUsrEmailDataSet(ByVal strSearch As String) As DataSet
        Try
            Return ClsUser.fnUsrGetUsrEmailFromAD(strSearch)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            'fnGetRecordsFromDB()
            Server.Transfer("DisSettings.aspx")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim intRetVal As Integer
            Dim strMsg As String
            Dim strUsrExistF As String = "N"

            Dim strCDAppOffName, strCDAppOffLID As String
            Dim strCDCPDFinanceEmail As String
            Dim strCDDept As String
            Dim strCDAOActive, strCDAOSubj, strCDAOContent As String
            Dim strCDCFActive, strCDCFSubj, strCDCFContent As String

            Dim strRDAppOffName, strRDAppOffLID As String
            Dim strRDCPDFinanceEmail As String
            Dim strRDDept As String
            Dim strRDAOActive, strRDAOSubj, strRDAOContent As String
            Dim strRDCFActive, strRDCFSubj, strRDCFContent As String
            Dim strRDRevActive, strRDRevSubj, strRDRevContent As String
            Dim strRDMOFActive, strRDMOFSubj, strRDMOFContent As String
            Dim strRDDispActive, strRDDispSubj, strRDDispContent As String

            ''** Get Value
            ''1. Condemnation Settings --> Approving Officer
            strCDAppOffName = Trim(cbCDAppOff.Text)
            If strCDAppOffName <> "" Then
                strUsrExistF = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(strCDAppOffName, strCDAppOffLID, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Approving Officer (Condemnation Settings) not found. Please select an existing Approving Officer."
                    lblErrorMessage.Visible = True
                    cbCDAppOff.Focus()
                    Exit Sub
                End If
            End If

            ''2. Condemnation Settings --> CPD-Finance
            strCDCPDFinanceEmail = cbCDCPDFinance.Text

            ''3. Condemnation Settings --> Department
            strCDDept = IIf(ddlCDDept.SelectedValue = "-1", "0", ddlCDDept.SelectedValue)

            ''4. Condemnation Settings --> Email for Approving Officer
            strCDAOActive = rdCDAOActive.SelectedValue
            strCDAOSubj = txtCDAOSubj.Text
            strCDAOContent = txtCDAOContent.Text

            ''5. Condemnation Settings --> Email for CPF-Finance
            strCDCFActive = rdCDCFActive.SelectedValue
            strCDCFSubj = txtCDCFSubj.Text
            strCDCFContent = txtCDCFContent.Text

            ''6. Redundancy Settings --> Approving Officer
            strRDAppOffName = Trim(cbRDAppOff.Text)
            If strRDAppOffName <> "" Then
                strUsrExistF = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(strRDAppOffName, strRDAppOffLID, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Approving Officer (Redundancy Settings) not found. Please select an existing Approving Officer."
                    lblErrorMessage.Visible = True
                    cbRDAppOff.Focus()
                    Exit Sub
                End If
            End If

            ''7. Redundancy Settings --> CPD-Finance
            strRDCPDFinanceEmail = cbRDCPDFinance.Text

            ''8. Redundancy Settings --> Department
            strRDDept = IIf(ddlRDDept.SelectedValue = "-1", "0", ddlRDDept.SelectedValue)

            ''9. Redundancy Settings --> Email for Approving Officer
            strRDAOActive = rdRDAOActive.SelectedValue
            strRDAOSubj = txtRDAOSubj.Text
            strRDAOContent = txtRDAOContent.Text

            ''10. Redundancy Settings --> Email for CPF-Finance
            strRDCFActive = rdRDCFActive.SelectedValue
            strRDCFSubj = txtRDCFSubj.Text
            strRDCFContent = txtRDCFContent.Text

            ''11. Redundancy Settings --> Email for Receiver
            strRDRevActive = rdRDRevActive.SelectedValue
            strRDRevSubj = txtRDRevSubj.Text
            strRDRevContent = txtRDRevContent.Text

            ''12. Redundancy - Email for MOF
            strRDMOFActive = rdRDMOFActive.SelectedValue
            strRDMOFSubj = txtRDMOFSubj.Text
            strRDMOFContent = txtRDMOFContent.Text

            ''13. Redundancy - Email for Disposal
            strRDDispActive = rdRDDispActive.SelectedValue
            strRDDispSubj = txtRDDispSubj.Text
            strRDDispContent = txtRDDispContent.Text

            ''update settings
            intRetVal = clsDisMng.fnDisMng_UpdateSettings( _
                            strCDAppOffLID, strCDCPDFinanceEmail, strCDDept, _
                            strCDAOActive, strCDAOSubj, strCDAOContent, _
                            strCDCFActive, strCDCFSubj, strCDCFContent, _
                            strRDAppOffLID, strRDCPDFinanceEmail, strRDDept, _
                            strRDAOActive, strRDAOSubj, strRDAOContent, _
                            strRDCFActive, strRDCFSubj, strRDCFContent, _
                            strRDRevActive, strRDRevSubj, strRDRevContent, _
                            strRDMOFActive, strRDMOFSubj, strRDMOFContent, _
                            strRDDispActive, strRDDispSubj, strRDDispContent, _
                            Session("UsrID"))

            If intRetVal > 0 Then
                strMsg = "Settings Updated Successfully."

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Dispose Management Settings Updated")

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "document.location.href='../common/welcomePg.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "Settings Update Failed."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

    <WebMethod()>
    Public Shared Function GetUsers(prefix As String) As String()

        Dim arrary() As String = prefix.ToString().Split(";")
        Dim dt As DataTable = New DataTable()
        'Dim par As String = ""
        If arrary.Length > 1 Then
            prefix = arrary(arrary.Length - 1).Trim()
        End If

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("EM", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}", row("EmailContent")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class