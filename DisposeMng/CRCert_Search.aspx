<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CRCert_Search.aspx.vb" Inherits="AMS.CRCert_Search" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script type="text/javascript">
	    function chkFrm() {
			var foundError = false;
		    
		    //validate Certificate Creation from date
			if (!foundError && gfnCheckDate(document.Frm_CertSearch.txtCertFrmDt, "Certificate Creation From Date", "O") == false) {
				foundError=true
			}
			
			//validate Certificate Creation to date
			if (!foundError && gfnCheckDate(document.Frm_CertSearch.txtCertToDt, "Certificate Creation To Date", "O") == false) {
				foundError=true
			}
			
							 
 			if (!foundError){
 			   return true;
 			}
			else
				return false;
		}
	</script>
</head>
<body>
    <form id="Frm_CertSearch" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Dispose Management : Condemnation/Redundancy Search Certificate</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
					
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
																													
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																				    <TR>
																						<TD valign="middle" width="35%">
																						    <FONT class="DisplayTitle">Certificate S/No : </FONT>
																						</TD>
																						<TD width="65%">
																							<asp:TextBox id="txtCertSNo" maxlength="10" Runat="server"></asp:TextBox>
																						</TD>
																					</TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Certificate Creation Date : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtCertFrmDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_CertSearch.txtCertFrmDt, document.Frm_CertSearch.txtCertToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtCertToDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_CertSearch.txtCertFrmDt, document.Frm_CertSearch.txtCertToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Certificate Status : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlCertStatus" Runat="server"></asp:DropDownList>
																						</TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Certificate Type : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlCertType" Runat="server"></asp:DropDownList>
																						</TD>
																				    </TR>
																				    <%--<TR>
																						<TD valign="middle" width="35%">
																						    <FONT class="DisplayTitle">Asset ID (AMS) : </FONT>
																						</TD>
																						<TD width="65%">
																							<asp:TextBox id="txtAssetIDAMS" maxlength="20" Runat="server"></asp:TextBox>
																						</TD>
																					</TR>
																	                <TR>
																		                <TD valign="middle" width="35%">
																		                    <FONT class="DisplayTitle">Asset ID (NFS) : </FONT>
																		                </TD>
																		                <TD width="65%">
																			                <asp:TextBox id="txtAssetIDNFS" maxlength="20" Runat="server"></asp:TextBox>
																		                </TD>
																	                </TR>--%>
																	                
																					<TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:Button id="butSearch" Text="Search" Runat="Server" />&nbsp;
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
															
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
			<!-- Start: Hidden Fields -->
			<!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
				src="../common/DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="Z-INDEX:101; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px">
			</iframe>
			<!-- End  : Hidden Fields -->
		</form>
</body>
</html>
