Public Partial Class CRCert_NotAuthorize
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Request("CallFrm") = "C" Then
                lblheader.Text = "Approving Certificate of Condemnation"
            Else
                lblheader.Text = "Approving Certificate of Redundancy"
            End If
        End If
    End Sub

End Class