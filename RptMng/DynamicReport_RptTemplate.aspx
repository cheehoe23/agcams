<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DynamicReport_RptTemplate.aspx.vb" Inherits="AMS.DynamicReport_RptTemplate" EnableEventValidation="false" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script language="JavaScript" src="../Common/SelectFieldOption.js"></script>
	<script language="JavaScript" src="../Common/MoveFieldOption.js"></script>
	<script language="javascript">
		function chkFrm(CallType) {
			var foundError = false;
			
			//validate Report TEmplate 
			if (!foundError && gfnIsFieldBlank(document.Frm_DynamicRpt.txtRptTitle)) {
				foundError=true
				document.Frm_DynamicRpt.txtRptTitle.focus()
				alert("Please enter Report Title.");
			}
			
			//validate Display Fiedls
			if (!foundError &&  document.getElementById("selectedOptions").length == '0'){
			    foundError=true
				alert("Please select Field to Display.");
			}
			
			//Get Selected Field Value
			if (!foundError)  {
			    var i , j; 
			    var DisplayValue = '';
			    j = document.getElementById("selectedOptions").length - 1
			    for(i=0; i<=j ; i++){
			        DisplayValue = DisplayValue + document.getElementById("selectedOptions").options[i].value + '^';
                } 
                document.Frm_DynamicRpt.hdnSelectDisplay.value = DisplayValue;
			}
			
			
			
 			if (!foundError){
 				var flag = false;
 				if (CallType == 'Save') {
 				    flag = window.confirm("Are you sure you want to add this report template?");
 				}
 				else{
 				    flag = window.confirm("Are you sure you want to update this report template?");
 				}
 				
 				return flag;
 			}
			else
				return false;
		}
		</script>
</head>
<body onload=createListObjects()>
    <form id="Frm_DynamicRpt" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Report Management : <asp:Label ID=lblRptTitle runat= server></asp:Label></B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																				    <TR>
																						<TD valign="middle" style="width: 30%">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Report Title : </FONT>
																						</TD>
																						<TD width="70%">
																							<asp:TextBox id="txtRptTitle" maxlength="250" Runat="server" Width="350px"></asp:TextBox>
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b><font color="red">*</font>Field to display</b></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																						    <TABLE border="0" cellPadding="1" cellSpacing="1" width="80%" ID="Table9">
																				            <TBODY align=left>
																				                <tr>
																				                    <td><asp:ListBox ID=availableOptions  Width="280px" SelectionMode=Single Rows=15 runat=server></asp:ListBox></td>
																				                    <td align=center valign=top>
																				                        <asp:Button ID=butSelectAllDisplay Text=">>>" runat=server Width=60px /><br /><br />
																				                        <asp:Button ID=butSelectDisplay Text=">" runat=server Width=60px /><br /><br />
																				                        <asp:Button ID=butSelectNDisplay Text="<" runat=server Width=60px /><br /><br />
																				                        <asp:Button ID=butSelectAllNDisplay Text="<<<" runat=server Width=60px />
																				                    </td>
																				                    <td><asp:ListBox ID=selectedOptions Width="280px" SelectionMode=Single Rows=15 runat=server></asp:ListBox></td>
																				                    <td align=center valign=top>
																				                        <asp:Button ID=butTop Text="Top" runat=server Width=60px /><br /><br />
																				                        <asp:Button ID=butUp Text="Up" runat=server Width=60px /><br /><br />
																				                        <asp:Button ID=butDown Text="Down" runat=server Width=60px /><br /><br />
																				                        <asp:Button ID=butBottom Text="Bottom" runat=server  Width=60px />
																				                    </td>
																				                </tr>
																				            </TBODY>
																				            </TABLE>
																						</TD>
																					</TR>
																					<TR>
																						<TD align="center" colspan="2" style="height: 36px"><BR>
																							<asp:Button id="butSaveTemplate" Text="Save Report Template" Runat="Server" Width="162px" />
																							<asp:Button id="butUpdateTemplate" Text="Update Report Template" Runat="Server" Width="162px" />
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />
																							<asp:Button id="butCancel" Text="Cancel" Runat="Server" />
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnCallType" runat="server">
			<input type="hidden" id="hdnRptTemplateID" runat="server">
			<input type="hidden" id="hdnSelectDisplay" runat="server">
			<!-- End  : Hidden Fields -->
	</form>
</body>
</html>
