#Region "Information Section"
' ****************************************************************************************************
' Description       : Generate Dynamic Report
' Purpose           : Generate Dynamic Report Information
' Date              : 27/09/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Web.Services

#End Region

Partial Public Class DynamicReport_view
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            Me.Header.DataBind()

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butGenerate.Attributes.Add("OnClick", "return chkFrm()")
                Me.butExpExcel.Attributes.Add("OnClick", "return chkFrm()")
                Me.butExpWord.Attributes.Add("OnClick", "return chkFrm()")
                Me.butSelectAllDisplay.Attributes.Add("OnClick", "return addAll()")
                Me.butSelectDisplay.Attributes.Add("OnClick", "return addAttribute()")
                Me.butSelectNDisplay.Attributes.Add("OnClick", "return delAttribute()")
                Me.butSelectAllNDisplay.Attributes.Add("OnClick", "return delAll()")
                Me.butTop.Attributes.Add("OnClick", "return top('selectedOptions')")
                Me.butUp.Attributes.Add("OnClick", "return up('selectedOptions')")
                Me.butDown.Attributes.Add("OnClick", "return down('selectedOptions')")
                Me.butBottom.Attributes.Add("OnClick", "return bottom('selectedOptions')")

                hdnCallType.Value = Request("CallType")    '"N"=New Report, "E"=From Existing Report Template
                hdnRptTemplateID.Value = Request("RID")    'Just for Exsting Report Template
                hdnSelectDisplay.Value = ""

                ''Populate Control
                fnPopulateCtrl()

                ''Populate Record 
                fnPopulateCtrlRecord()

                ''Checking Access Right
                fnCheckAccessRight()

                fnDDLDeviceType()
            End If

            ''To Avoid Field to Diplay Missing... Re-generate Field to display
            If hdnSelectDisplay.Value <> "" Then
                ''Display Fields (Available)
                availableOptions.DataSource = clsReport.fnDynamicRpt_GetField2Display("EN", hdnSelectDisplay.Value)
                availableOptions.DataTextField = "fld_RptFieldName"
                availableOptions.DataValueField = "fld_RptFieldDisplayID"
                availableOptions.DataBind()

                ''Display Fields
                selectedOptions.DataSource = clsReport.fnDynamicRpt_GetField2Display("ED", hdnSelectDisplay.Value)
                selectedOptions.DataTextField = "fld_RptFieldName"
                selectedOptions.DataValueField = "fld_RptFieldDisplayID"
                selectedOptions.DataBind()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnDDLDeviceType()
        Try

            Dim dt As DataTable = clsDeviceType.fnDeviceTypeGetAllRec("fld_DeviceType", "ASC").Tables(0)
            If (dt.Rows.Count > 0) Then
                ddlDeviceType.DataSource = dt
                ddlDeviceType.DataTextField = "fld_DeviceType"
                ddlDeviceType.DataValueField = "fld_DeviceTypeID"
                ddlDeviceType.DataBind()
                ddlDeviceType.Items.Insert(0, New ListItem("", "-1"))
            End If

        Catch ex As Exception

        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Asset Type --> Fixed Asset, Inventory
            fnPopulateAssetTypeInDDL(ddlAssetType)

            ''Get Category
            fnPopulateDropDownList(clsCategory.fnCategoryGetCategory(), ddlAssetCat, "fld_CategoryID", "fld_CatCodeName", False)

            ''Get SubCategory (set default --> -1)
            ddlAssetSubCat.Items.Insert(0, New ListItem("", "-1"))

            ''Get Controlled Item --> Yes, No
            fnPopulateYesNoInDDL(ddlCtrlItem)

            ''Get Asset Status --> Active, InActive, Condemation, Dispose
            fnPopulateAssetStatusInDDL(ddlStatus)

            ''Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False)
            If Session("AdminF") = "N" Then ddlDepartment.Items.Remove(ddlDepartment.Items.FindByValue("-1"))

            ''Get Location
            fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLocation, "fld_LocationID", "fld_LoctCodeName", False)

            ''Get Sub Location 
            ddlLocation_SelectedIndexChanged(Nothing, Nothing)


            ''Add option for text...
            fnPopulateTextOptionInDDL(ddlOptionAssIDAMS)
            fnPopulateTextOptionInDDL(ddlOptionAssIDNFS)
            fnPopulateTextOptionInDDL(ddlOptionBrand)
            fnPopulateTextOptionInDDL(ddlOptionDesc)
            fnPopulateNumericOptionInDDL(ddlOptionCost)
            fnPopulateTextOptionInDDL(ddlOptionCostCenter)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnPopulateCtrlRecord()
        Dim objRdr As SqlDataReader
        Try
            ''Get Field to Display
            If hdnCallType.Value = "N" Then
                ''*** For Create New Report Template 
                ''populate control
                availableOptions.DataSource = clsReport.fnDynamicRpt_GetField2Display("NR", "")
                availableOptions.DataTextField = "fld_RptFieldName"
                availableOptions.DataValueField = "fld_RptFieldDisplayID"
                availableOptions.DataBind()

            Else
                ''*** For Edit Report Template 
                ''Populate Records
                Dim strFieldDisplay As String = ""
                objRdr = clsReport.fnDynamicRpt_GetRptTemplateData(hdnRptTemplateID.Value)
                If Not objRdr Is Nothing Then
                    If objRdr.HasRows Then
                        objRdr.Read()
                        txtRptTitle.Text = CStr(objRdr("fld_RptTitle"))
                        strFieldDisplay = CStr(objRdr("fld_RptFieldDisplay"))
                    End If
                End If
                objRdr.Close()

                ''Display Fields (Available)
                availableOptions.DataSource = clsReport.fnDynamicRpt_GetField2Display("EN", strFieldDisplay)
                availableOptions.DataTextField = "fld_RptFieldName"
                availableOptions.DataValueField = "fld_RptFieldDisplayID"
                availableOptions.DataBind()

                ''Display Fields
                selectedOptions.DataSource = clsReport.fnDynamicRpt_GetField2Display("ED", strFieldDisplay)
                selectedOptions.DataTextField = "fld_RptFieldName"
                selectedOptions.DataValueField = "fld_RptFieldDisplayID"
                selectedOptions.DataBind()
            End If

        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Public Sub fnCheckAccessRight()
        Try
            If InStr(Session("AR"), "Rpt|DymRpt|genRpt") = 0 Then butGenerate.Visible = False

            If InStr(Session("AR"), "Rpt|DymRpt|exportExcel") = 0 Then butExpExcel.Visible = False

            If InStr(Session("AR"), "Rpt|DymRpt|exportWord") = 0 Then butExpWord.Visible = False

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ddlAssetCat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAssetCat.SelectedIndexChanged
        Try
            If ddlAssetCat.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsCategory.fnCategoryGetAllRecDR(ddlAssetCat.SelectedValue, 0, "fld_CatSubName", "ASC"), ddlAssetSubCat, "fld_CatSubID", "fld_CatSubName", False)
            Else
                ddlAssetSubCat.Items.Clear()
                ddlAssetSubCat.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            Server.Transfer("DynamicReport_view.aspx?CallType=" + hdnCallType.Value + "&RID=" + hdnRptTemplateID.Value)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Try
            Server.Transfer("DynamicReport_view1.aspx")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butGenerate.Click
        Try
            fnGenerateRpt("GR")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butExpExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExpExcel.Click
        Try
            fnGenerateRpt("EE")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butExpWord_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExpWord.Click
        Try
            fnGenerateRpt("EW")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub fnGenerateRpt(ByVal strRptType As String)
        Try
            Dim dsAsset As New DataSet

            Dim strAssetIDAMS As String = Trim(txtAssetIDAMS.Text)
            Dim strAssetIDNFS As String = Trim(txtAssetIDNFS.Text)
            Dim strAssetType As String = ddlAssetType.SelectedValue
            Dim intAssetCatID As Integer = IIf(ddlAssetCat.SelectedValue = "-1", "0", ddlAssetCat.SelectedValue)
            Dim intAssetCatSubID As Integer = IIf(ddlAssetSubCat.SelectedValue = "-1", "0", ddlAssetSubCat.SelectedValue)
            Dim strCtrlItemF As String = ddlCtrlItem.SelectedValue
            Dim strAssetStatus As String = ddlStatus.SelectedValue
            Dim strBrand As String = Trim(txtAssetBrand.Text)
            Dim strDesc As String = Trim(txtDesc.Text)
            Dim strCost As String = Trim(txtCost.Text)
            Dim strCostCenter As String = Trim(txtCostCenter.Text)
            Dim strPurchaseFDt As String = txtPurchaseFrmDt.Text
            Dim strPurchaseTDt As String = txtPurchaseToDt.Text
            Dim strWarExpFDt As String = txtWarExpFrmDt.Text
            Dim strWarExpTDt As String = txtWarExpToDt.Text
            Dim intDeptID As Integer = IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue)
            Dim intLocID As Integer = IIf(ddlLocation.SelectedValue = "-1", "0", ddlLocation.SelectedValue)
            Dim intLocSubID As Integer = IIf(ddlLocSub.SelectedValue = "-1", "0", ddlLocSub.SelectedValue)
            Dim strOwnerID As String = "" 'IIf(ddlOwner.SelectedValue = "-1", "", ddlOwner.SelectedValue)

            Dim strOptionAMS As String = ddlOptionAssIDAMS.SelectedValue
            Dim strOptionNFS As String = ddlOptionAssIDNFS.SelectedValue
            Dim strOptionBrand As String = ddlOptionBrand.SelectedValue
            Dim strOptionDesc As String = ddlOptionDesc.SelectedValue
            Dim strOptionCost As String = ddlOptionCost.SelectedValue
            Dim strOptionCostCtr As String = ddlOptionCostCenter.SelectedValue

            Dim strShortDescription As String = Trim(txtShortDescription.Text)
            Dim strOtherInformation As String = Trim(txtOtherInformation.Text)
            Dim strSerialNo As String = Trim(txtSerialNo.Text)
            Dim strMacAddress As String = Trim(txtMacAddress.Text)
            Dim strNewHostName As String = Trim(txtHostName.Text)
            Dim strMachineModel As String = Trim(txtMachineModel.Text)

            Dim intDeviceTypeID As Integer = 0

            Dim arrSelectField As Array
            Dim strOrderBy1 As String = ""
            Dim strOrderBy2 As String = ""
            Dim strOrderBy3 As String = ""
            Dim intCount As Integer = "0"

            ''*** Get Order by 
            arrSelectField = Split(CStr(hdnSelectDisplay.Value), "^")
            For intCount = 0 To UBound(arrSelectField) - 1
                If intCount = "0" Then
                    strOrderBy1 = arrSelectField(intCount)
                ElseIf intCount = "1" Then
                    strOrderBy2 = arrSelectField(intCount)
                ElseIf intCount = "2" Then
                    strOrderBy3 = arrSelectField(intCount)
                End If
            Next

            ''*** Get Owner
            If Trim(cbOwner.Text) <> "" Then
                'strOwner = cbOwner.SelectedRowValues(0)
                Dim strUsrExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbOwner.Text), strOwnerID, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Owner not found. Please select an existing Owner."
                    lblErrorMessage.Visible = True
                    cbOwner.Focus()
                    Exit Sub
                End If
            End If

            strAssetIDAMS = strAssetIDAMS.Replace("'", "''")
            'strAssetIDAMS = strAssetIDAMS.Replace("[", "[[]")
            'strAssetIDAMS = strAssetIDAMS.Replace("%", "[%]")
            'strAssetIDAMS = strAssetIDAMS.Replace("_", "[_]")

            strAssetIDNFS = strAssetIDNFS.Replace("'", "''")
            'strAssetIDNFS = strAssetIDNFS.Replace("[", "[[]")
            'strAssetIDNFS = strAssetIDNFS.Replace("%", "[%]")
            'strAssetIDNFS = strAssetIDNFS.Replace("_", "[_]")

            strShortDescription = strShortDescription.Replace("'", "''")
            'strShortDescription = strShortDescription.Replace("[", "[[]")
            'strShortDescription = strShortDescription.Replace("%", "[%]")
            'strShortDescription = strShortDescription.Replace("_", "[_]")

            strOtherInformation = strOtherInformation.Replace("'", "''")
            'strOtherInformation = strOtherInformation.Replace("[", "[[]")
            'strOtherInformation = strOtherInformation.Replace("%", "[%]")
            'strOtherInformation = strOtherInformation.Replace("_", "[_]")

            strSerialNo = strSerialNo.Replace("'", "''")
            'strSerialNo = strSerialNo.Replace("[", "[[]")
            'strSerialNo = strSerialNo.Replace("%", "[%]")
            'strSerialNo = strSerialNo.Replace("_", "[_]")

            strMacAddress = strMacAddress.Replace("'", "''")
            'strMacAddress = strMacAddress.Replace("[", "[[]")
            'strMacAddress = strMacAddress.Replace("%", "[%]")
            'strMacAddress = strMacAddress.Replace("_", "[_]")

            strNewHostName = strNewHostName.Replace("'", "''")
            'strNewHostName = strNewHostName.Replace("[", "[[]")
            'strNewHostName = strNewHostName.Replace("%", "[%]")
            'strNewHostName = strNewHostName.Replace("_", "[_]")

            strMachineModel = strMachineModel.Replace("'", "''")
            'strMachineModel = strMachineModel.Replace("[", "[[]")
            'strMachineModel = strMachineModel.Replace("%", "[%]")
            'strMachineModel = strMachineModel.Replace("_", "[_]")

            If (ddlDeviceType.SelectedIndex > 0) Then
                intDeviceTypeID = IIf(ddlDeviceType.SelectedValue = "-1", "0", ddlDeviceType.SelectedValue)
            End If

            dsAsset = clsReport.fnDynamicRpt_GenerateRecord( _
                            strAssetIDAMS, strAssetIDNFS, _
                            strAssetType, intAssetCatID, intAssetCatSubID, _
                            strAssetStatus, strBrand, strDesc, strCost, strCostCenter, _
                            strPurchaseFDt, strPurchaseTDt, _
                            strWarExpFDt, strWarExpTDt, _
                            intDeptID, intLocID, strOwnerID, _
                            strOptionAMS, strOptionNFS, _
                            strOptionBrand, strOptionDesc, strOptionCost, strOptionCostCtr, _
                            strOrderBy1, strOrderBy2, strOrderBy3, intLocSubID, strCtrlItemF, _
                            strShortDescription, strOtherInformation,
                         intDeviceTypeID, strSerialNo, strMacAddress,
                         strNewHostName, rdolstStatus.SelectedValue, strMachineModel)

            If dsAsset.Tables(0).Rows.Count > 0 Then
                ''Set session to blank
                Session("DynamicRpt") = ""

                ''assign dataset to Session
                Session("DynamicRpt") = dsAsset

                ''pop-up window for report
                If strRptType = "GR" Then
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "window.open('DynamicRpt_viewRpt.aspx?RptTitle=" & clsEncryptDecrypt.EncryptText(Trim(txtRptTitle.Text)) & "&RptField=" & clsEncryptDecrypt.EncryptText(CStr(hdnSelectDisplay.Value)) & "&CallType=" & clsEncryptDecrypt.EncryptText(CStr(strRptType)) & "', 'Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1,menubar=1'); " & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "window.open('DynamicRpt_ExpRpt.aspx?RptTitle=" & clsEncryptDecrypt.EncryptText(Trim(txtRptTitle.Text)) & "&RptField=" & clsEncryptDecrypt.EncryptText(CStr(hdnSelectDisplay.Value)) & "&CallType=" & clsEncryptDecrypt.EncryptText(CStr(strRptType)) & "', 'Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1,menubar=1'); " & _
                                    "</script>"
                    Response.Write(strJavaScript)
                End If

            Else
                lblErrorMessage.Visible = True
                lblErrorMessage.Text = "Record(s) not found."
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function


    Private Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLocation.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCodeName", False)
            Else
                ddlLocSub.Items.Clear()
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class