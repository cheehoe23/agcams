Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Partial Public Class AssetBookValueRpt_ShowRpt
    Inherits System.Web.UI.Page
    Private report As New ReportDocument()

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********            
            crvABV.HasToggleGroupTreeButton = True
            crvABV.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None

            crvABV.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None

            crvABV.HasToggleParameterPanelButton = False

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            'If Not Page.IsPostBack Then
            hdnDFDt.Value = Request("DFDt")
            hdnDTDt.Value = Request("DTDt")
            'End If

            '' ''Get dataset
            Dim ds As New DataSet()
            ds = CType(Session("ABVRpt"), DataSet)

            ''Create a report object
            Dim rptFile As String = Server.MapPath("RptFile\AssetBookValueRpt.rpt")
            report.Load(rptFile)
            report.SetDataSource(ds.Tables(0))
            report.SetParameterValue("DepreciationFDt", hdnDFDt.Value)
            report.SetParameterValue("DepreciationTDt", hdnDTDt.Value)

            'Show it!
            Me.crvABV.ReportSource = report
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********            
            crvABV.HasToggleGroupTreeButton = True
            crvABV.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None

            'crvABV.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None

            'crvABV.HasToggleParameterPanelButton = False

            'lblErrorMessage.Visible = False
            'lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                hdnDFDt.Value = Request("DFDt")
                hdnDTDt.Value = Request("DTDt")
            End If

            '' ''Get dataset
            'Dim ds As New DataSet()
            'ds = CType(Session("ABVRpt"), DataSet)

            ' ''Create a report object
            'Dim rptFile As String = Server.MapPath("RptFile\AssetBookValueRpt.rpt")
            'report.Load(rptFile)
            'report.SetDataSource(ds.Tables(0))
            'report.SetParameterValue("DepreciationFDt", hdnDFDt.Value)
            'report.SetParameterValue("DepreciationTDt", hdnDTDt.Value)

            ''Show it!
            'Me.crvABV.ReportSource = report
        Catch ex As Exception
            'lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            'lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            report.Close()
            report.Dispose()

            GC.Collect()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

End Class