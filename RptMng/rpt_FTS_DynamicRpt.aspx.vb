﻿#Region "Information Section"
' ****************************************************************************************************
' Description       : Search Report
' Purpose           : Search Report Information
' Author            : Win
' Date              : 05/06/2016
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Public Class rpt_FTS_DynamicRpt
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label
    Protected WithEvents lblDateType As System.Web.UI.WebControls.Label
    Protected WithEvents cbFieldDisplay As System.Web.UI.WebControls.CheckBoxList
    Protected WithEvents cbDelinq As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            '''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("/common/logout.aspx", False)
                Exit Sub
            End If
            '''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butGenRep.Attributes.Add("OnClick", "return chkFrm('rpt')")
                Me.butExp2Excel.Attributes.Add("OnClick", "return chkFrm('excel')")

                ''check access right
                If (InStr(Session("AR"), "Rpt|dyRpt|GenRpt") = 0) Then butGenRep.Visible = False
                If (InStr(Session("AR"), "Rpt|dyRpt|ExpExcel") = 0) Then butExp2Excel.Visible = False

                ''Populate Control
                fnPopulateCtrl()

                ''set default
                butReset_Click(Nothing, Nothing)
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub fnPopulateCtrl()
        Dim sqlRD As SqlDataReader
        Try
            '' Populate Location
            sqlRD = clsLocationOfficer.fnGetLocationOfficer()
            If Not sqlRD Is Nothing Then
                If sqlRD.HasRows Then
                    With lbLocationOfficer
                        .DataSource = sqlRD
                        .DataTextField = "fld_LocationOfficerName"
                        .DataValueField = "fld_LocationOfficerID"
                        .DataBind()
                    End With

                End If
            End If
            sqlRD.Close()

        Catch ex As Exception
            clsCommon.fnDataReader_Close(sqlRD)
            Throw ex
        End Try
    End Sub

    Private Sub ddlStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlStatus.SelectedIndexChanged
        Try

            '    End If
            'End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub cbFileRef_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbFileRef.CheckedChanged
        Try
            If cbFileRef.Checked Then
                txtFileRef.Visible = True
            Else
                txtFileRef.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub cbFileDetailRef_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbFileDetailRef.CheckedChanged
        Try
            If cbFileDetailRef.Checked Then
                txtFileDetailRef.Visible = True
            Else
                txtFileDetailRef.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub cbFileTitle_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbFileTitle.CheckedChanged
        Try
            If cbFileTitle.Checked Then
                txtFileTitle.Visible = True
            Else
                txtFileTitle.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub cbOffLoc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbOffLoc.CheckedChanged
        Try
            If cbOffLoc.Checked Then
                divOffLoc.Visible = True
            Else
                divOffLoc.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub cbCrtDt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCrtDt.CheckedChanged
        Try
            If cbCrtDt.Checked Then
                divCrtDt.Visible = True
            Else
                divCrtDt.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub cbStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbStatus.CheckedChanged
        Try
            If cbStatus.Checked Then
                divStatus.Visible = True
            Else
                divStatus.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub cbCloseDt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCloseDt.CheckedChanged
        Try
            If cbCloseDt.Checked Then
                divCloseDt.Visible = True
            Else
                divCloseDt.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            ''Report Title 
            txtRptTitle.Text = ""

            ''File Reference
            cbFileRef.Checked = False
            cbFileRef_CheckedChanged(Nothing, Nothing)
            txtFileRef.Text = ""

            ''File Detail Reference
            cbFileDetailRef.Checked = False
            cbFileDetailRef_CheckedChanged(Nothing, Nothing)
            txtFileDetailRef.Text = ""

            ''File Title
            cbFileTitle.Checked = False
            cbFileTitle_CheckedChanged(Nothing, Nothing)
            txtFileTitle.Text = ""

            ''Officer/Location
            cbOffLoc.Checked = False
            cbOffLoc_CheckedChanged(Nothing, Nothing)

            ''File Status
            cbStatus.Checked = False
            cbStatus_CheckedChanged(Nothing, Nothing)
            ddlStatus.SelectedValue = "-1"
            ddlStatus_SelectedIndexChanged(Nothing, Nothing)

            ''File Creation Date
            cbCrtDt.Checked = False
            cbCrtDt_CheckedChanged(Nothing, Nothing)
            txtCrtFromDt.Text = ""
            txtCrtToDt.Text = ""

            ''Closed Date
            cbCloseDt.Checked = False
            cbCloseDt_CheckedChanged(Nothing, Nothing)
            txtCloseFD.Text = ""
            txtCloseTD.Text = ""

            ''Clear selected of Field to Display
            Dim FDItem As ListItem
            For Each FDItem In DeleteThis.Items
                If FDItem.Value = "1" Or FDItem.Value = "2" Then
                    FDItem.Selected = True
                Else
                    FDItem.Selected = False
                End If
            Next

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butGenRep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butGenRep.Click
        Try
            subGenerateRec("Report")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butExp2Excel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExp2Excel.Click
        Try
            subGenerateRec("ExpExcel")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub subGenerateRec(ByVal strCallFrm As String)
        Try
            Dim strFRef As String = ""
            Dim strFDetailRef As String = ""
            Dim strFileTitle As String = ""
            Dim strOffLocType As String = ""
            Dim strOffLocID As String = ""
            Dim strFStatus As String = ""
            Dim strCreateFD As String = ""
            Dim strCreateTD As String = ""
            Dim strCloseFD As String = ""
            Dim strCloseTD As String = ""
            Dim strDisplay As String = ""
            Dim strRunNo As String = ""

            ''Get Search Value
            If cbFileRef.Checked Then strFRef = Trim(txtFileRef.Text)

            If cbFileDetailRef.Checked Then strFDetailRef = Trim(txtFileDetailRef.Text)

            If cbFileTitle.Checked Then strFileTitle = Trim(txtFileTitle.Text)

            If cbOffLoc.Checked Then
                Dim OLItem As ListItem
                
                For Each OLItem In lbLocationOfficer.Items
                    If OLItem.Selected Then
                        strOffLocID = strOffLocID & "'" & OLItem.Value & "',"
                    End If
                Next

                If strOffLocID <> "" Then strOffLocID = strOffLocID.Substring(0, strOffLocID.Length - 1)
            End If

            If cbStatus.Checked Then strFStatus = ddlStatus.SelectedValue

            If cbCrtDt.Checked Then
                strCreateFD = Trim(txtCrtFromDt.Text)
                strCreateTD = Trim(txtCrtToDt.Text)
            End If

            If cbCloseDt.Checked Then
                strCloseFD = txtCloseFD.Text
                strCloseTD = txtCloseTD.Text
            End If

            ''Get Display Value
            Dim Item As ListItem
            For Each Item In DeleteThis.Items
                If Item.Selected Then
                    strDisplay = strDisplay & Item.Value & "^"
                End If
            Next

            ''set Session to default
            Session("dsDynamicRptRec") = ""

            ''Generate Report
            Dim ds As New DataSet
            Dim strJavascript As String
            Dim strParameterP As String
            ds = clsReport.fnRep_GetRecForDynamicRepForFTS( _
                                   strFRef, strFDetailRef, strFileTitle, strOffLocType, strOffLocID, _
                                   strFStatus, strCreateFD, strCreateTD, _
                                   strCloseFD, strCloseTD, strDisplay, "", "", Session("LoginID"), strRunNo)

            If ds.Tables(0).Rows.Count > 0 Then
                ''assign Dataset to Session
                Session("dsDynamicRptRec") = ds

                ''Get URL for pop-up window
                'If strCallFrm = "Report" Then
                '    strParameterP = "rpt_FTS_showDynamicRpt.aspx?RunNo=" + strRunNo + "&DF=" + strDisplay + "&RptT=" + txtRptTitle.Text
                'Else
                '    strParameterP = "rpt_ExpExcel.aspx?DF=" + strDisplay + "&RptT=" + txtRptTitle.Text
                'End If

                If strCallFrm = "Report" Then
                    strParameterP = "rpt_FTS_showDynamicRpt.aspx?DF=" + strDisplay + "&RptT=" + txtRptTitle.Text
                Else
                    strParameterP = "rpt_ExpExcel.aspx?DF=" + strDisplay + "&RptT=" + txtRptTitle.Text
                End If

                ''Call Pop-up window to generate report
                strJavascript = "<script language=javascript>" & _
                                "window.open('" & strParameterP & "', 'DynamicRptScreen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1');" & _
                                "</script>"
                Response.Write(strJavascript)
            Else
                lblErrorMessage.Visible = True
                lblErrorMessage.Text = "Record(s) not found."
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    
End Class