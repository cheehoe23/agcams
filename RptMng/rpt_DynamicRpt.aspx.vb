#Region "Information Section"
' ****************************************************************************************************
' Description       : Search Report
' Purpose           : Search Report Information
' Author            : See Siew
' Date              : 19/04/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class rpt_DynamicRpt
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label
    Protected WithEvents lblDateType As System.Web.UI.WebControls.Label
    Protected WithEvents cbFieldDisplay As System.Web.UI.WebControls.CheckBoxList
    Protected WithEvents cbDelinq As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            '''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("/common/logout.aspx", False)
                Exit Sub
            End If
            '''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butGenRep.Attributes.Add("OnClick", "return chkFrm('rpt')")
                Me.butExp2Excel.Attributes.Add("OnClick", "return chkFrm('excel')")

                ''check access right
                If (InStr(Session("AR"), "Rpt|dyRpt|GenRpt") = 0) Then butGenRep.Visible = False
                If (InStr(Session("AR"), "Rpt|dyRpt|ExpExcel") = 0) Then butExp2Excel.Visible = False

                ''Populate Control
                fnPopulateCtrl()

                ''set default
                butReset_Click(Nothing, Nothing)
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub fnPopulateCtrl()
        Dim sqlRD As SqlDataReader
        Dim drStatus As SqlDataReader
        Try
            '' Populate Officer
            sqlRD = ClsUser.fnUsrGetAllUsrDR("fld_usrName", "ASC")
            If Not sqlRD Is Nothing Then
                If sqlRD.HasRows Then
                    With lbOfficer
                        .DataSource = sqlRD
                        .DataTextField = "fld_usrName"
                        .DataValueField = "fld_UsrID"
                        .DataBind()
                    End With

                End If
            End If
            sqlRD.Close()

            '' Populate Location
            sqlRD = clsLocation.fnLocationGetAllRecForDDL("fld_LocationName", "ASC")
            If Not sqlRD Is Nothing Then
                If sqlRD.HasRows Then
                    With lbLocation
                        .DataSource = sqlRD
                        .DataTextField = "fld_LocationName"
                        .DataValueField = "fld_LocationID"
                        .DataBind()
                    End With

                End If
            End If
            sqlRD.Close()

            ''Populate Security Status 
            fnPopulateSecurityStatusInDDL(ddlSecurityStatus)

            ''Get Status 
            drStatus = clsStatus.fnStatusGetRecForDDLOrByPCode("", "0", "0")
            fnPopulateDropDownList(drStatus, ddlStatus, "fld_StatusID", "fld_StatusName", False)
            drStatus.Close()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(sqlRD)
            clsCommon.fnDataReader_Close(drStatus)
            Throw ex
        End Try
    End Sub

    Private Sub ddlStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlStatus.SelectedIndexChanged
        Try
            If ddlStatus.SelectedValue = "-1" Then
                ddlStatusRemark.Visible = False
                hdnStatusID.Value = "-1"
            Else
                ''Check whether Remark exist
                Dim drStatus As SqlDataReader
                Dim drRemark As SqlDataReader
                drStatus = clsStatus.fnStatusGetRecForDDLOrByPCode("", ddlStatus.SelectedValue, "0")
                If Not drStatus Is Nothing Then
                    drStatus.Read()
                    
                    If drStatus("fld_RemarkExistF") = "Y" Then   ''Got Remark
                        ddlStatusRemark.Visible = True
                        hdnStatusID.Value = "-1"
                    Else   ''Not Remark
                        ddlStatusRemark.Visible = False
                        hdnStatusID.Value = drStatus("fld_RemarksID")
                    End If
                    drStatus.Close()

                    ''Populate Remark
                    drRemark = clsStatus.fnStatusGetRecForDDLOrByPCode("", ddlStatus.SelectedValue, "0")
                    fnPopulateDropDownList(drRemark, ddlStatusRemark, "fld_RemarksID", "fld_RemarkName", False)
                    ddlStatusRemark.SelectedValue = hdnStatusID.Value
                End If
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub ddlStatusRemark_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlStatusRemark.SelectedIndexChanged
        Try
            hdnStatusID.Value = ddlStatusRemark.SelectedValue
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub cbFileNo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbFileNo.CheckedChanged
        Try
            If cbFileNo.Checked Then
                txtFileNo.Visible = True
            Else
                txtFileNo.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub cbFileRef_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbFileRef.CheckedChanged
        Try
            If cbFileRef.Checked Then
                txtFileRef.Visible = True
            Else
                txtFileRef.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub cbOffLoc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbOffLoc.CheckedChanged
        Try
            If cbOffLoc.Checked Then
                divOffLoc.Visible = True
            Else
                divOffLoc.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub cbCrtDt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCrtDt.CheckedChanged
        Try
            If cbCrtDt.Checked Then
                divCrtDt.Visible = True
            Else
                divCrtDt.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub cbSecurityStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbSecurityStatus.CheckedChanged
        Try
            If cbSecurityStatus.Checked Then
                ddlSecurityStatus.Visible = True
            Else
                ddlSecurityStatus.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub cbStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbStatus.CheckedChanged
        Try
            If cbStatus.Checked Then
                divStatus.Visible = True
            Else
                divStatus.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub rdOffLoc_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdOffLoc.SelectedIndexChanged
        Try
            If rdOffLoc.SelectedValue = "U" Then
                lbOfficer.Visible = True
                lbLocation.Visible = False
            Else
                lbLocation.Visible = True
                lbOfficer.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    'Private Sub cbDelinq_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbDelinq.CheckedChanged
    '    Try
    '        If cbDelinq.Checked Then
    '            ddlDelinq.Visible = True
    '        Else
    '            ddlDelinq.Visible = False
    '        End If
    '    Catch ex As Exception
    '        lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
    '        lblErrorMessage.Visible = True
    '    End Try
    'End Sub

    Private Sub cbReviewDt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbReviewDt.CheckedChanged
        Try
            If cbReviewDt.Checked Then
                divReviewDt.Visible = True
            Else
                divReviewDt.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub cbCloseDt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbCloseDt.CheckedChanged
        Try
            If cbCloseDt.Checked Then
                divCloseDt.Visible = True
            Else
                divCloseDt.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            ''Report Title 
            txtRptTitle.Text = ""

            ''Delinquency
            'cbDelinq.Checked = False
            'cbDelinq_CheckedChanged(Nothing, Nothing)
            'ddlDelinq.SelectedValue = "A"

            ''File Number 
            cbFileNo.Checked = False
            cbFileNo_CheckedChanged(Nothing, Nothing)
            txtFileNo.Text = ""

            ''File Reference
            cbFileRef.Checked = False
            cbFileRef_CheckedChanged(Nothing, Nothing)
            txtFileRef.Text = ""

            ''Officer/Location
            cbOffLoc.Checked = False
            cbOffLoc_CheckedChanged(Nothing, Nothing)
            rdOffLoc.SelectedValue = "U"
            rdOffLoc_SelectedIndexChanged(Nothing, Nothing)
            Dim Item As ListItem
            For Each Item In lbOfficer.Items
                Item.Selected = False
            Next
            For Each Item In lbLocation.Items
                Item.Selected = False
            Next

            ''File Creation Date
            cbCrtDt.Checked = False
            cbCrtDt_CheckedChanged(Nothing, Nothing)
            txtCrtFromDt.Text = ""
            txtCrtToDt.Text = ""

            ''Security Status
            cbSecurityStatus.Checked = False
            cbSecurityStatus_CheckedChanged(Nothing, Nothing)
            ddlSecurityStatus.SelectedValue = "-1"

            ''File Status
            cbStatus.Checked = False
            cbStatus_CheckedChanged(Nothing, Nothing)
            ddlStatus.SelectedValue = "-1"
            ddlStatus_SelectedIndexChanged(Nothing, Nothing)


            ''Review Date
            cbReviewDt.Checked = False
            cbReviewDt_CheckedChanged(Nothing, Nothing)
            txtReviewFD.Text = ""
            txtReviewTD.Text = ""

            ''Closed Date
            cbCloseDt.Checked = False
            cbCloseDt_CheckedChanged(Nothing, Nothing)
            txtCloseFD.Text = ""
            txtCloseTD.Text = ""

            ''Clear selected of Field to Display
            Dim FDItem As ListItem
            For Each FDItem In DeleteThis.Items
                If FDItem.Value = "1" Or FDItem.Value = "2" Then
                    FDItem.Selected = True
                Else
                    FDItem.Selected = False
                End If
            Next

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butGenRep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butGenRep.Click
        Try
            subGenerateRec("Report")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butExp2Excel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExp2Excel.Click
        Try
            subGenerateRec("ExpExcel")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub subGenerateRec(ByVal strCallFrm As String)
        Try
            Dim strFNum As String = ""
            Dim strFRef As String = ""
            Dim strOffLocType As String = ""
            Dim strOffLocID As String = ""
            Dim strCreateFD As String = ""
            Dim strCreateTD As String = ""
            Dim strSecStatus As String = ""
            Dim strFStatus As String = ""
            Dim strDelin As String = ""
            Dim strReviewFD As String = ""
            Dim strReviewTD As String = ""
            Dim strCloseFD As String = ""
            Dim strCloseTD As String = ""
            Dim strDisplay As String = ""
            Dim strRunNo As String = ""

            ''Get Search Value
            If cbFileNo.Checked Then strFNum = Trim(txtFileNo.Text)
            If cbFileRef.Checked Then strFRef = Trim(txtFileRef.Text)
            If cbOffLoc.Checked Then
                Dim OLItem As ListItem
                strOffLocType = rdOffLoc.SelectedValue

                If strOffLocType = "U" Then '' Officer
                    For Each OLItem In lbOfficer.Items
                        If OLItem.Selected Then
                            strOffLocID = strOffLocID & "'" & OLItem.Value & "',"
                        End If
                    Next
                Else  ''Location
                    For Each OLItem In lbLocation.Items
                        If OLItem.Selected Then
                            strOffLocID = strOffLocID & "'" & OLItem.Value & "',"
                        End If
                    Next
                End If

                If strOffLocID <> "" Then strOffLocID = strOffLocID.Substring(0, strOffLocID.Length - 1)
            End If
            If cbCrtDt.Checked Then
                strCreateFD = Trim(txtCrtFromDt.Text)
                strCreateTD = Trim(txtCrtToDt.Text)
            End If
            If cbSecurityStatus.Checked Then strSecStatus = ddlSecurityStatus.SelectedValue
            If cbStatus.Checked Then strFStatus = hdnStatusID.Value
            'If cbDelinq.Checked Then
            'strDelin = ddlDelinq.SelectedValue
            If cbReviewDt.Checked Then
                strReviewFD = txtReviewFD.Text
                strReviewTD = txtReviewTD.Text
            End If
            If cbCloseDt.Checked Then
                strCloseFD = txtCloseFD.Text
                strCloseTD = txtCloseTD.Text
            End If

            ''Get Display Value
            Dim Item As ListItem
            For Each Item In DeleteThis.Items
                If Item.Selected Then
                    strDisplay = strDisplay & Item.Value & "^"
                End If
            Next

            ''set Session to default
            Session("dsDynamicRptRec") = ""

            ''Generate Report
            Dim ds As New DataSet
            Dim strJavascript As String
            Dim strParameterP As String
            ds = clsReport.fnRep_GetRecForDynamicRep( _
                                   strFNum, strFRef, strOffLocType, strOffLocID, _
                                   strCreateFD, strCreateTD, strSecStatus, strFStatus, _
                                   strReviewFD, strReviewTD, _
                                   strCloseFD, strCloseTD, strDisplay, "", "", Session("LoginID"), strRunNo)
            If ds.Tables(0).Rows.Count > 0 Then
                ''assign Dataset to Session
                Session("dsDynamicRptRec") = ds

                ''Get URL for pop-up window
                If strCallFrm = "Report" Then
                    strParameterP = "rpt_showDynamicRpt.aspx?RunNo=" + strRunNo + "&DF=" + strDisplay + "&RptT=" + txtRptTitle.Text
                Else
                    strParameterP = "rpt_ExpExcel.aspx?DF=" + strDisplay + "&RptT=" + txtRptTitle.Text
                End If

                ''Call Pop-up window to generate report
                strJavascript = "<script language=javascript>" & _
                                "window.open('" & strParameterP & "', 'DynamicRptScreen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1');" & _
                                "</script>"
                Response.Write(strJavascript)
            Else
                lblErrorMessage.Visible = True
                lblErrorMessage.Text = "Record(s) not found."
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
