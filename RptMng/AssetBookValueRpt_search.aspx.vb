#Region "Information Section"
' ****************************************************************************************************
' Description       : Generate Asset Book Value Report 
' Purpose           : Generate Asset Book Value Report  Information
' Date              : 10/11/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Web.Services

#End Region

Partial Public Class AssetBookValueRpt_search
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********
            Me.Header.DataBind()

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butGenerate.Attributes.Add("OnClick", "return chkFrm()")

                ''*** Populate Control
                fnPopulateCtrl()

                ''set Default for Depreciation From/To date
                If Month(Date.Today) < 4 Then
                    txtDepreFDt.Text = "01/04/" + (Year(Date.Today) - 1).ToString
                Else
                    txtDepreFDt.Text = "01/04/" + Year(Date.Today).ToString
                End If
                txtDepreTDt.Text = mfnGetCurrentDate()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Asset Type --> Fixed Asset
            fnPopulateAssetTypeInDDL(ddlAssetType)
            ddlAssetType.Items.Remove(ddlAssetType.Items.FindByValue(""))
            ddlAssetType.Items.Remove(ddlAssetType.Items.FindByValue("I"))
            ddlAssetType.Items.Remove(ddlAssetType.Items.FindByValue("L"))

            ''Get Category
            fnPopulateDropDownList(clsCategory.fnCategoryGetCategory(), ddlAssetCat, "fld_CategoryID", "fld_CatCodeName", False)

            ''Get SubCategory (set default --> -1)
            ddlAssetSubCat.Items.Insert(0, New ListItem("", "-1"))

            ''Get Controlled Item --> Yes, No
            fnPopulateYesNoInDDL(ddlCtrlItem)

            ''Get Asset Status --> Active, InActive, Condemation, Dispose
            fnPopulateAssetStatusInDDL(ddlStatus)
            ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("T"))

            ''Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False)
            If Session("AdminF") = "N" Then ddlDepartment.Items.Remove(ddlDepartment.Items.FindByValue("-1"))

            ''Get Location
            fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLocation, "fld_LocationID", "fld_LoctCodeName", False)

            ''Get Sub Location 
            ddlLocation_SelectedIndexChanged(Nothing, Nothing)

            ''Sorting 
            ddlSort.Items.Insert(0, New ListItem("Asset ID (AMS), Asset ID (NFS), Department", "fldAssetBarcode, fld_AssetNFSID, fld_DepartmentName"))
            ddlSort.Items.Insert(1, New ListItem("Asset ID (AMS), Department, Asset ID (NFS)", "fldAssetBarcode, fld_DepartmentName, fld_AssetNFSID"))
            ddlSort.Items.Insert(2, New ListItem("Asset ID (NFS), Asset ID (AMS), Department", "fld_AssetNFSID, fldAssetBarcode, fld_DepartmentName"))
            ddlSort.Items.Insert(3, New ListItem("Asset ID (NFS), Department, Asset ID (AMS)", "fld_AssetNFSID, fld_DepartmentName, fldAssetBarcode"))
            ddlSort.Items.Insert(4, New ListItem("Department, Asset ID (AMS), Asset ID (NFS)", "fld_DepartmentName, fldAssetBarcode, fld_AssetNFSID"))
            ddlSort.Items.Insert(5, New ListItem("Department, Asset ID (NFS), Asset ID (AMS)", "fld_DepartmentName, fld_AssetNFSID, fldAssetBarcode"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ddlAssetCat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAssetCat.SelectedIndexChanged
        Try
            If ddlAssetCat.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsCategory.fnCategoryGetAllRecDR(ddlAssetCat.SelectedValue, 0, "fld_CatSubName", "ASC"), ddlAssetSubCat, "fld_CatSubID", "fld_CatSubName", False)
            Else
                ddlAssetSubCat.Items.Clear()
                ddlAssetSubCat.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            Server.Transfer("AssetBookValueRpt_search.aspx")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butGenerate.Click
        Try
            Dim dsAsset As New DataSet

            Dim strAssetIDAMS As String = Trim(txtAssetIDAMS.Text)
            Dim strAssetIDNFS As String = Trim(txtAssetIDNFS.Text)
            Dim strAssetType As String = ddlAssetType.SelectedValue
            Dim intAssetCatID As Integer = IIf(ddlAssetCat.SelectedValue = "-1", "0", ddlAssetCat.SelectedValue)
            Dim intAssetCatSubID As Integer = IIf(ddlAssetSubCat.SelectedValue = "-1", "0", ddlAssetSubCat.SelectedValue)
            Dim strCtrlItemF As String = ddlCtrlItem.SelectedValue
            Dim strAssetStatus As String = ddlStatus.SelectedValue
            Dim strPurchaseFDt As String = txtPurchaseFrmDt.Text
            Dim strPurchaseTDt As String = txtPurchaseToDt.Text
            Dim strWarExpFDt As String = txtWarExpFrmDt.Text
            Dim strWarExpTDt As String = txtWarExpToDt.Text
            Dim intDeptID As Integer = IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue)
            Dim intLocID As Integer = IIf(ddlLocation.SelectedValue = "-1", "0", ddlLocation.SelectedValue)
            Dim intLocSubID As Integer = IIf(ddlLocSub.SelectedValue = "-1", "0", ddlLocSub.SelectedValue)
            Dim strOwnerID As String = "" 'IIf(ddlOwner.SelectedValue = "-1", "", ddlOwner.SelectedValue)
            Dim strDepreciationFDt As String = Trim(txtDepreFDt.Text)
            Dim strDepreciationTDt As String = Trim(txtDepreTDt.Text)

            ''*** Get Owner
            If Trim(cbOwner.Text) <> "" Then
                'strOwner = cbOwner.SelectedRowValues(0)
                Dim strUsrExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbOwner.Text), strOwnerID, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Owner not found. Please select an existing Owner."
                    lblErrorMessage.Visible = True
                    cbOwner.Focus()
                    Exit Sub
                End If
            End If

            dsAsset = clsReport.fnABVReport_SearchRecord( _
                         strAssetIDAMS, strAssetIDNFS, _
                         strAssetType, intAssetCatID, intAssetCatSubID, _
                         strAssetStatus, strPurchaseFDt, strPurchaseTDt, _
                         strWarExpFDt, strWarExpTDt, _
                         intDeptID, intLocID, strOwnerID, _
                         strDepreciationFDt, strDepreciationTDt, _
                         ddlSort.SelectedValue, intLocSubID, strCtrlItemF)


            If dsAsset.Tables(0).Rows.Count > 0 Then
                ''Set session to blank
                Session("ABVRpt") = ""

                ''assign dataset to Session
                Session("ABVRpt") = dsAsset


                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "window.open('AssetBookValueRpt_ShowRpt.aspx?DFDt=" & strDepreciationFDt & "&DTDt=" & strDepreciationTDt & "', 'Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1'); " & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                lblErrorMessage.Visible = True
                lblErrorMessage.Text = "Record(s) not found."
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLocation.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCodeName", False)
            Else
                ddlLocSub.Items.Clear()
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class