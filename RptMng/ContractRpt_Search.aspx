<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ContractRpt_Search.aspx.vb" Inherits="AMS.ContractRpt_Search" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script language="JavaScript" src="../Common/SelectFieldOption.js"></script>
	<script language="JavaScript" src="../Common/MoveFieldOption.js"></script>	
	<LINK href="../common/ComboBox/styles/contactsearch/combo.css" type="text/css" rel="Stylesheet">
    <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
    <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
            type="text/css" />
	<script language="javascript">

	    $(document).ready(function () {
	        SearchText("cbOwner", "hfOwner");
	        SearchText("cbOffSignName", "hfOffSignName");
	    });
	    function SearchText(obj1, obj2) {
	        $("#" + obj1).autocomplete({
	            source: function (request, response) {
	                $.ajax({
	                    url: '<%#ResolveUrl("~/RptMng/ContractRpt_Search.aspx/GetUsersList") %>',
	                    data: "{ 'prefix': '" + request.term + "'}",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    success: function (data) {
	                        response($.map(data.d, function (item) {
	                            return {
	                                label: item.split('~')[0],
	                                val: item.split('~')[1]
	                            }
	                        }))
	                    },
	                    error: function (response) {
	                        alert(response.responseText);
	                    },
	                    failure: function (response) {
	                        alert(response.responseText);
	                    }
	                });
	            },
	            select: function (e, i) {
	                $("#" + obj2).val(i.item.val);
	            },
	            minLength: 0
	        });

	    }

		function chkFrm() {
			var foundError = false;
			
			//validate Cost
		    if (!foundError && gfnIsFieldBlank(document.Frm_ContractRpt.tctContCost)== false) {
			    if (!foundError && gfnCheckNumeric(document.Frm_ContractRpt.tctContCost,'.')) {
				    foundError=true;
				    document.Frm_ContractRpt.tctContCost.focus();
				    alert("Contract Cost just allow number only.");
			    } 
		    }
		    
		    //validate Contract start From date
			if (!foundError && gfnCheckDate(document.Frm_ContractRpt.txtContSFrmDt, "Contract Start From Date", "O") == false) {
				foundError=true
			}
			
			//validate Contract start To date
			if (!foundError && gfnCheckDate(document.Frm_ContractRpt.txtContSToDt, "Contract Start To Date", "O") == false) {
				foundError=true
			}
	
			//validate Contract End From date
			if (!foundError && gfnCheckDate(document.Frm_ContractRpt.txtContEFrmDt, "Contract End From Date", "O") == false) {
				foundError=true
			}
			 
			//validate Contract End To date
			if (!foundError && gfnCheckDate(document.Frm_ContractRpt.txtContEToDt, "Contract End To Date", "O") == false) {
				foundError=true
			}
			
			//validate Security Deposit
			if (!foundError && gfnIsFieldBlank(document.Frm_ContractRpt.txtSecurityDeposit)== false) {
			    if (!foundError && gfnCheckNumeric(document.Frm_ContractRpt.txtSecurityDeposit,'.')) {
				        foundError=true;
				        document.Frm_ContractRpt.txtSecurityDeposit.focus();
				        alert("Security Deposit just allow number only.");
			    }   
			 }
			 
			//validate Security Deposit start From Date
			if (!foundError && gfnCheckDate(document.Frm_ContractRpt.txtSecurityDepSFDt, "Security Deposit Start From Date", "O") == false) {
				foundError=true
			}
			
			//validate Security Deposit start to Date
			if (!foundError && gfnCheckDate(document.Frm_ContractRpt.txtSecurityDepSTDt, "Security Deposit Start To Date", "O") == false) {
				foundError=true
			}
			
			//validate Security Deposit End From Date
			if (!foundError && gfnCheckDate(document.Frm_ContractRpt.txtSecurityDepEFDt, "Security Deposit End From Date", "O") == false) {
				foundError=true
			}
			 
			//validate Security Deposit End To Date
			if (!foundError && gfnCheckDate(document.Frm_ContractRpt.txtSecurityDepETDt, "Security Deposit End To Date", "O") == false) {
				foundError=true
			}
			
			//validate Banker's Guarantee Amount
			if (!foundError && gfnIsFieldBlank(document.Frm_ContractRpt.txtBankGuarAmount)== false) {
			    if (!foundError && gfnCheckNumeric(document.Frm_ContractRpt.txtBankGuarAmount,'.')) {
				        foundError=true;
				        document.Frm_ContractRpt.txtBankGuarAmount.focus();
				        alert("Banker's Guarantee Amount just allow number only.");
			    }   
			 }
			    
			//validate Banker's Guarantee Start From Date
			if (!foundError && gfnCheckDate(document.Frm_ContractRpt.txtBankGuarSFDt, "Banker's Guarantee Start From Date", "O") == false) {
				foundError=true
			}
			
			//validate Banker's Guarantee Start To Date
			if (!foundError && gfnCheckDate(document.Frm_ContractRpt.txtBankGuarSTDt, "Banker's Guarantee Start To Date", "O") == false) {
				foundError=true
			}
			
			//validate Banker's Guarantee End From Date
			if (!foundError && gfnCheckDate(document.Frm_ContractRpt.txtBankGuarEFDt, "Banker's Guarantee End From Date", "O") == false) {
				foundError=true
			}
			
			//validate Banker's Guarantee End To Date
			if (!foundError && gfnCheckDate(document.Frm_ContractRpt.txtBankGuarETDt, "Banker's Guarantee End To Date", "O") == false) {
				foundError=true
			}
					
						
			//validate Display Fiedls
			if (!foundError &&  document.getElementById("selectedOptions").length == '0'){
			    foundError=true
				alert("Please select Field to Display.");
			}
			
			//Get Selected Field Value
			if (!foundError)  {
			    var i , j; 
			    var DisplayValue = '';
			    j = document.getElementById("selectedOptions").length - 1
			    for(i=0; i<=j ; i++){
			        DisplayValue = DisplayValue + document.getElementById("selectedOptions").options[i].value + '^';
                } 
                document.Frm_ContractRpt.hdnSelectDisplay.value = DisplayValue;
			}
			
						
 			if (!foundError){
 				var flag = false;
 				flag = window.confirm("Are you sure you want to generate this report?");
 				 				
 				return flag;
 			}
			else
				return false;
		}
		</script>
</head>
<body onload=createListObjects()>
    <form id="Frm_ContractRpt" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Report Management : Contract Dynamic Report</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																				    <TR>
																						<TD valign=top width="35%">
																						    <font color="red">*</font><FONT class="DisplayTitle">Report Type : </FONT>
																						</TD>
																						<TD width="65%">
																							<asp:radiobuttonlist id="rdRptType" Runat="server" RepeatDirection=Vertical AutoPostBack=true>
																							    <asp:ListItem Value="3">List of Contract</asp:ListItem>
																								<asp:ListItem Value="1">Contract Expiry Report</asp:ListItem>
																								<asp:ListItem Value="2">Tracking Renewal Contract (Vendor)</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<b>Search</b></TD>
																					</TR>
																					<TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Contract ID : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtContractID" maxlength="20" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD width="35%">
																					        <FONT class="DisplayTitle">Contract Title : </FONT>
																					    </TD>
																					    <TD width="65%"><asp:TextBox id="txtContTitle" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Contract Cost (S$) : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="tctContCost" maxlength="250" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Contract Start Date : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtContSFrmDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_ContractRpt.txtContSFrmDt, document.Frm_ContractRpt.txtContSToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtContSToDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_ContractRpt.txtContSFrmDt, document.Frm_ContractRpt.txtContSToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Contract End Date : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtContEFrmDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_ContractRpt.txtContEFrmDt, document.Frm_ContractRpt.txtContEToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtContEToDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_ContractRpt.txtContEFrmDt, document.Frm_ContractRpt.txtContEToDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Contract Expiry Period Notification (1st Reminder) : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlContExpNoti" Runat="server"></asp:DropDownList>&nbsp; Months
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD width="35%">
																					        <FONT class="DisplayTitle">Vendor Name : </FONT>
																					    </TD>
																					    <TD width="65%"><asp:TextBox id="txtVendorName" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD width="35%">
																					        <FONT class="DisplayTitle">Description : </FONT>
																					    </TD>
																					    <TD width="65%"><asp:TextBox id="txtDesc" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Name of Contract Owner : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:TextBox ID="cbOwner"  runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Department of Contract Owner : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlDepartment" Runat="server"></asp:DropDownList>
																					    </TD>
																				    </TR>
																				     <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Security Deposit (S$) : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtSecurityDeposit" maxlength="250" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Security Deposit Start Date : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtSecurityDepSFDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_ContractRpt.txtSecurityDepSFDt, document.Frm_ContractRpt.txtSecurityDepSTDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtSecurityDepSTDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_ContractRpt.txtSecurityDepSFDt, document.Frm_ContractRpt.txtSecurityDepSTDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Security Deposit End Date : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtSecurityDepEFDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_ContractRpt.txtSecurityDepEFDt, document.Frm_ContractRpt.txtSecurityDepETDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtSecurityDepETDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_ContractRpt.txtSecurityDepEFDt, document.Frm_ContractRpt.txtSecurityDepETDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Security Deposit Expiry Period Notification : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlSecDpsNoti" Runat="server"></asp:DropDownList>&nbsp; Months
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Security Deposit Reference No. : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtSecDpsRefNo" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Banker's Guarantee Amount (S$) : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtBankGuarAmount" maxlength="250" Runat="server"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Banker's Guarantee Start Date : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtBankGuarSFDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_ContractRpt.txtBankGuarSFDt, document.Frm_ContractRpt.txtBankGuarSTDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtBankGuarSTDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_ContractRpt.txtBankGuarSFDt, document.Frm_ContractRpt.txtBankGuarSTDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Banker's Guarantee End Date : </FONT>
																					    </TD>
																					    <TD>
																						    From <asp:TextBox ID="txtBankGuarEFDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.Frm_ContractRpt.txtBankGuarEFDt, document.Frm_ContractRpt.txtBankGuarETDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
									                                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;To <asp:TextBox ID="txtBankGuarETDt" Width="110" Runat="server"></asp:TextBox><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.Frm_ContractRpt.txtBankGuarEFDt, document.Frm_ContractRpt.txtBankGuarETDt);return false;"
										                                                        HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
									                                                        </a>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Banker's Guarantee Expiry Period Notification : </FONT>
																					    </TD>
																					    <TD>
																					        <asp:DropDownList ID="ddlBankGuaNoti" Runat="server"></asp:DropDownList>&nbsp; Months
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Banker's Guarantee Reference No. : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtBankGuarRefNo" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Name of Officer who signed the contract : </FONT>
																					    </TD>
																					    <TD>
																					        <!--<asp:TextBox id="txtOffSignName" maxlength="250" Runat="server" Width="300px"></asp:TextBox>-->
																					       
																					        <asp:TextBox ID="cbOffSignName" runat="server" Width="300px"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <TR>
																					    <TD>
																					        <FONT class="DisplayTitle">Designation of Officer who signed the contract : </FONT>
																					    </TD>
																					    <TD><asp:TextBox id="txtOffSignDes" maxlength="250" Runat="server" Width="300px"></asp:TextBox></TD>
																				    </TR>
																				    <TR>
																					    <TD>
																						    <FONT class="DisplayTitle">Asset ID Included : </FONT>
																					    </TD>
																					    <TD>
																						    <asp:TextBox id="txtAssetID" maxlength="20" Runat="server"></asp:TextBox>
																					    </TD>
																				    </TR>
																				    <tr>
																				        <td colspan=2>
																				            <div id=divArchive runat=server>
																				                <TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table10">
																				                     <TR>
																					                    <TD width="35%">
																						                    <FONT class="DisplayTitle">Include Archive : </FONT>
																					                    </TD>
																					                    <TD width="65%">
																						                    <asp:radiobuttonlist id="rdArchiveF" Runat="server" RepeatDirection="Horizontal">
																								                <asp:ListItem Value="Y">Yes</asp:ListItem>
																								                <asp:ListItem Value="N" Selected>No</asp:ListItem>
																							                </asp:radiobuttonlist>
																					                    </TD>
																				                    </TR>       
																				                </TABLE>
																				            </div>
																				        </td>
																				    </tr>
																				    <TR>
																						<TD colSpan="2">&nbsp;</TD>
																					</TR>
																					<TR class="TRTitleBG">
																						<TD colSpan="2">&nbsp;<font color="red">*</font><b>Field to display</b></TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																						    <TABLE border="0" cellPadding="1" cellSpacing="1" width="80%" ID="Table9">
																				            <TBODY align=left>
																				                <tr>
																				                    <td colspan=2>Available Fields</td>
																				                    <td colspan=2>Fields to Display on Report</td>
																				                </tr>
																				                <tr>
																				                    <td><asp:ListBox ID=availableOptions  Width="290px" SelectionMode=Single Rows=15 runat=server></asp:ListBox></td>
																				                    <td align=center valign=top>
																				                        <asp:Button ID=butSelectAllDisplay Text=">>>" runat=server Width=60px /><br /><br />
																				                        <asp:Button ID=butSelectDisplay Text=">" runat=server Width=60px /><br /><br />
																				                        <asp:Button ID=butSelectNDisplay Text="<" runat=server Width=60px /><br /><br />
																				                        <asp:Button ID=butSelectAllNDisplay Text="<<<" runat=server Width=60px />
																				                    </td>
																				                    <td><asp:ListBox ID=selectedOptions Width="290px" SelectionMode=Single Rows=15 runat=server></asp:ListBox></td>
																				                    <td align=center valign=top>
																				                        <asp:Button ID=butTop Text="Top" runat=server Width=60px /><br /><br />
																				                        <asp:Button ID=butUp Text="Up" runat=server Width=60px /><br /><br />
																				                        <asp:Button ID=butDown Text="Down" runat=server Width=60px /><br /><br />
																				                        <asp:Button ID=butBottom Text="Bottom" runat=server  Width=60px />
																				                    </td>
																				                </tr>
																				            </TBODY>
																				            </TABLE>
																						</TD>
																					</TR>
																					<TR>
																						<TD align="center" colspan="2" style="height: 36px"><BR>
																							<asp:Button id="butGenerate" Text="Generate Report" Runat="Server" Width="127px" />
																							<asp:Button id="butExpExcel" Text="Export to Excel" Runat="Server" Width="130px" />
																							<asp:Button id="butExpWord" Text="Export to Word" Runat="Server" Width="120px" />
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
                    <input type="hidden" id="hfOwner" runat="server"> 
            <input type="hidden" id="hfOffSignName" runat="server"> 
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
			<!-- Start: Hidden Fields -->
			<input type="hidden" id="hdnRptType" runat="server">
			<input type="hidden" id="hdnSelectDisplay" runat="server">
			<!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
				src="../common/DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="Z-INDEX:101; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px">
			</iframe>
			<!-- End  : Hidden Fields -->
	</form>
</body>
</html>
