<%@ Register TagPrefix="tagFooter" TagName="footer" src="~/commonFTS/FTSfooter_cr.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="rpt_DynamicRpt.aspx.vb" Inherits="AMS.rpt_DynamicRpt"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>rpt_DynamicRpt</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../common/FTSstyle.css" type="text/css" rel="Stylesheet">
		<script language="JavaScript" src="../Common/CommonJS.js"></script>
		<script language="javascript">
		function chkFrm(callFrm){
			var foundError = false;
			var flag = false;
			var msg = "";
			
			if (callFrm == 'rpt'){
				msg = "Are you sure you want to generate the report?"
			}
			else{
				msg = "Are you sure want to export record to excel?"
			}
			
				
			//validate Report Title
			if (!foundError && gfnIsFieldBlank(document.frm_RptSearch.txtRptTitle)) {
				foundError=true
				document.frm_RptSearch.txtRptTitle.focus()
				alert("Please enter Report Title.");
			}
			
			if (!foundError && document.frm_RptSearch.cbCrtDt.checked) {
			    //validate File Creation from date
			    if (!foundError && gfnCheckDate(document.frm_RptSearch.txtCrtFromDt, "File Creation From Date", "O") == false) {
				    foundError=true
			    }
    			
			    //validate File Creation to date
			    if (!foundError && gfnCheckDate(document.frm_RptSearch.txtCrtToDt, "File Creation To Date", "O") == false) {
				    foundError=true
			    }
			}
			
			if (!foundError && document.frm_RptSearch.cbReviewDt.checked) {
			    //validate Review from date
			    if (!foundError && gfnCheckDate(document.frm_RptSearch.txtReviewFD, "Review From Date", "O") == false) {
				    foundError=true
			    }
    			
			    //validate Review to date
			    if (!foundError && gfnCheckDate(document.frm_RptSearch.txtReviewTD, "Review To Date", "O") == false) {
				    foundError=true
			    }
			}
			
			if (!foundError && document.frm_RptSearch.cbCloseDt.checked) {
			    //validate Closed from date
			    if (!foundError && gfnCheckDate(document.frm_RptSearch.txtCloseFD, "Closed From Date", "O") == false) {
				    foundError=true
			    }
    			
			    //validate Closed to date
			    if (!foundError && gfnCheckDate(document.frm_RptSearch.txtCloseTD, "Closed To Date", "O") == false) {
				    foundError=true
			    }
			}
			
			
			//validate Fiels to display
			if (!foundError && !gfnCheckSelect("Please select at least 1 record for 'Field to Display'.")) {
				foundError=true
			}
						
	    	if (!foundError){
					flag = window.confirm(msg);
	    	}    
			else  {
				flag = false;
			}
			return flag;
	    }
	    /*
	    function chkTxt(cbName, sType){	
			if (eval('document.frm_RptSearch.' + cbName + '.checked') == true){
				eval('document.frm_RptSearch.' + sType + '.style.display = ""'); 
			}	
			else {
				eval('document.frm_RptSearch.' + sType + '.style.display = "none"'); 
			}
	    }
	    function chkDiv(cbName, sType){
			alert('in check')
			if (eval('document.frm_RptSearch.' + cbName + '.checked') == true){
				eval(sType + '.style.display = ""'); 
			}	
			else {
				eval(sType + '.style.display = "none"'); 
			}
	    }
	    function chkOffLoc(){
			if (document.frm_RptSearch.rdOffLoc[0].checked == true){
				document.frm_RptSearch.lbOfficer.style.display = ""; 
				document.frm_RptSearch.lbLocation.style.display = "none"; 
			}
			else {
				document.frm_RptSearch.lbOfficer.style.display = "none"; 
				document.frm_RptSearch.lbLocation.style.display = ""; 
			}
				
	    }*/
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="frm_RptSearch" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Report : Search - Dynamic Report</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table id="Table2" height="500" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<!--Start: Main Content-->
						<table id="Table3" height="400" cellSpacing="1" cellPadding="0" width="100%" align="left"
							bgColor="silver" border="0">
							<tr>
								<td>
									<table id="Table4" height="400" cellSpacing="0" cellPadding="0" width="100%" align="left"
										bgColor="white" border="0">
										<tr>
											<td>
												<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
													<TR>
														<TD vAlign="top" align="center" width="100%"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE id="Table6" height="126" cellSpacing="0" cellPadding="3" width="100%" align="left"
																bgColor="white" border="0">
																<TBODY>
																	<TR>
																		<TD vAlign="top" height="352">
																			<TABLE id="Table7" cellSpacing="1" cellPadding="1" width="100%" border="0">
																				<TBODY>
																					<TR>
																						<TD vAlign="middle" align="right" width="27%"><FONT class="DisplayTitle">Report Title : </FONT>
																						</TD>
																						<td colspan="2">
																							<asp:textbox id="txtRptTitle" Runat="server" MaxLength="30" Width="300"></asp:textbox>
																						</td>
																					</TR>
																					<!--
																					<TR>
																						<TD vAlign="middle" align="right"><FONT class="DisplayTitle">Delinquency : </FONT>
																						</TD>
																						<td width="5%" colspan="2">
																							<asp:dropdownlist id="ddlDelinq" Runat="server">
																								<asp:ListItem Value="A" Selected>Active</asp:ListItem>
																								<asp:ListItem Value="D">Deleted</asp:ListItem>
																							</asp:dropdownlist>
																						</td>
																					</TR>
																					-->
																					<tr>
																						<td vAlign="top" colSpan="3">&nbsp;</td>
																					</tr>
																					<tr>
																						<td vAlign="top" colSpan="3">&nbsp;</td>
																					</tr>
																					<tr bgColor="#ffff99">
																						<td vAlign="top" colSpan="3"><b>Search</b></td>
																					</tr>
																					<TR>
																						<TD vAlign="middle" align="right" width="27%"><FONT class="DisplayTitle">File Number : </FONT>
																						</TD>
																						<td width="5%"><asp:CheckBox ID="cbFileNo" Runat="server" AutoPostBack="True"></asp:CheckBox></td>
																						<TD width="70%">
																							<asp:textbox id="txtFileNo" Runat="server" MaxLength="20" Width="300"></asp:textbox>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" align="right"><FONT class="DisplayTitle">File Reference : </FONT>
																						</TD>
																						<td><asp:CheckBox ID="cbFileRef" Runat="server" AutoPostBack="True"></asp:CheckBox></td>
																						<TD>
																							<asp:textbox id="txtFileRef" MaxLength="25" Runat="server" Width="300"></asp:textbox>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="top" align="right"><FONT class="DisplayTitle">Officer/Location : </FONT>
																						</TD>
																						<td valign="top"><asp:CheckBox ID="cbOffLoc" Runat="server" AutoPostBack="True"></asp:CheckBox></td>
																						<TD>
																							<div id="divOffLoc" runat="server">
																								<asp:radiobuttonlist id="rdOffLoc" Runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
																									<asp:ListItem Value="U" Selected>Officer</asp:ListItem>
																									<asp:ListItem Value="L">Location</asp:ListItem>
																								</asp:radiobuttonlist>
																								<asp:ListBox ID="lbOfficer" Rows="5" Runat="server" SelectionMode="Multiple"></asp:ListBox>
																								<asp:ListBox ID="lbLocation" Rows="5" Runat="server" SelectionMode="Multiple"></asp:ListBox>
																							</div>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="top" align="right"><FONT class="DisplayTitle">File Creation Date : </FONT>
																						</TD>
																						<td><asp:CheckBox ID="cbCrtDt" Runat="server" AutoPostBack="True"></asp:CheckBox></td>
																						<TD>
																							<div id="divCrtDt" runat="server">
																								From
																								<asp:TextBox ID="txtCrtFromDt" Width="110" Runat="server"></asp:TextBox>
																								<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.frm_RptSearch.txtCrtFromDt, document.frm_RptSearch.txtCrtToDt);return false;"
																									HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
																								</a>&nbsp;&nbsp;&nbsp;&nbsp;To
																								<asp:TextBox ID="txtCrtToDt" Width="110" Runat="server"></asp:TextBox>
																								<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.frm_RptSearch.txtCrtFromDt, document.frm_RptSearch.txtCrtToDt);return false;"
																									HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
																								</a>
																							</div>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" align="right"><FONT class="DisplayTitle">Security Status : </FONT>
																						</TD>
																						<td><asp:CheckBox ID="cbSecurityStatus" Runat="server" AutoPostBack="True"></asp:CheckBox></td>
																						<TD><asp:dropdownlist id="ddlSecurityStatus" Runat="server"></asp:dropdownlist></TD>
																					</TR>
																					<TR>
																						<TD vAlign="top" align="right"><FONT class="DisplayTitle">File Status : </FONT>
																						</TD>
																						<td width="5%"><asp:CheckBox ID="cbStatus" Runat="server" AutoPostBack="True"></asp:CheckBox></td>
																						<TD><div id="divStatus" runat="server">
																								<asp:dropdownlist id="ddlStatus" Runat="server" AutoPostBack="True"></asp:dropdownlist>&nbsp;
																								<asp:dropdownlist id="ddlStatusRemark" Runat="server" AutoPostBack="True"></asp:dropdownlist>
																							</div>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="top" align="right"><FONT class="DisplayTitle">Review Date : </FONT>
																						</TD>
																						<td><asp:CheckBox ID="cbReviewDt" Runat="server" AutoPostBack="True"></asp:CheckBox></td>
																						<TD>
																							<div id="divReviewDt" runat="server">
																								From
																								<asp:TextBox ID="txtReviewFD" Width="110" Runat="server"></asp:TextBox>
																								<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.frm_RptSearch.txtReviewFD, document.frm_RptSearch.txtReviewTD);return false;"
																									HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
																								</a>&nbsp;&nbsp;&nbsp;&nbsp;To
																								<asp:TextBox ID="txtReviewTD" Width="110" Runat="server"></asp:TextBox>
																								<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.frm_RptSearch.txtReviewFD, document.frm_RptSearch.txtReviewTD);return false;"
																									HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
																								</a>
																							</div>
																						</TD>
																					</TR>
																					<TR>
																						<TD vAlign="top" align="right"><FONT class="DisplayTitle">Closed Date : </FONT>
																						</TD>
																						<td><asp:CheckBox ID="cbCloseDt" Runat="server" AutoPostBack="True"></asp:CheckBox></td>
																						<TD>
																							<div id="divCloseDt" runat="server">
																								From
																								<asp:TextBox ID="txtCloseFD" Width="110" Runat="server"></asp:TextBox>
																								<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.frm_RptSearch.txtCloseFD, document.frm_RptSearch.txtCloseTD);return false;"
																									HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select From Date">
																								</a>&nbsp;&nbsp;&nbsp;&nbsp;To
																								<asp:TextBox ID="txtCloseTD" Width="110" Runat="server"></asp:TextBox>
																								<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.frm_RptSearch.txtCloseFD, document.frm_RptSearch.txtCloseTD);return false;"
																									HIDEFOCUS><img align="absMiddle" src="../images/icon_calendar.gif" border="0" alt="Select End Date">
																								</a>
																							</div>
																						</TD>
																					</TR>
																					<tr>
																						<td vAlign="top" colSpan="3">&nbsp;</td>
																					</tr>
																					<tr>
																						<td vAlign="top" colSpan="3">&nbsp;</td>
																					</tr>
																					<tr bgColor="#ffff99">
																						<td vAlign="top" colSpan="3"><b>Please select field(s) for display in report.</b></td>
																					</tr>
																					<TR>
																						<TD vAlign="top" align="right"><FONT class="DisplayTitle">Field(s) to Display : </FONT>
																						</TD>
																						<TD colspan="2" valign="top">
																							<asp:CheckBoxList ID="DeleteThis" Runat="server" RepeatDirection="Horizontal" RepeatColumns="3">
																								<asp:ListItem Value="1" Selected>File Number</asp:ListItem>
																								<asp:ListItem Value="2" Selected>File Reference</asp:ListItem>
																								<asp:ListItem Value="3">File Title</asp:ListItem>
																								<asp:ListItem Value="4">File Volume</asp:ListItem>
																								<asp:ListItem Value="6">File Remarks</asp:ListItem>
																								<asp:ListItem Value="5">Create Date</asp:ListItem>
																								<asp:ListItem Value="8">Closed Date</asp:ListItem>
																								<asp:ListItem Value="7">Review Date</asp:ListItem>
																								<asp:ListItem Value="9">Officer/Location</asp:ListItem>
																								<asp:ListItem Value="10">Security Status</asp:ListItem>
																								<asp:ListItem Value="11">Last Movement Date</asp:ListItem>
																							</asp:CheckBoxList>
																						</TD>
																					</TR>
																					<TR>
																						<TD align="center" colSpan="3"><BR>
																							<asp:button id="butGenRep" Runat="Server" Text="Generate Report"></asp:button>
																							<asp:button id="butExp2Excel" Runat="Server" Text="Export to Excel"></asp:button>
																							<asp:button id="butReset" Runat="Server" Text="Reset"></asp:button></TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start : Hidden Fields -->
			<input type="hidden" id="hdnStatusID" runat="server"> 
			<!--  PopCalendar(tag name and id must match) Tags should not be enclosed in tags other than the html body tag. -->
			<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
				src="../common/DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="Z-INDEX:101; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px">
			</iframe>
			<!-- End   : Hidden Fields -->
			<tagfooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer>
		</form>
	</body>
</HTML>
