#Region "Information Section"
' ****************************************************************************************************
' Description       : Display Master File List Report
' Purpose           : Display Master File List Information
' Author            : See Siew
' Date              : 21/04/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
#End Region

Public Class MstFile_showRpt
    Inherits System.Web.UI.Page
    Private report As New ReportDocument()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents CRVMstListRpt As CrystalDecisions.Web.CrystalReportViewer
    Protected WithEvents hdnSortName As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents hdnSortAD As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents hdnDeptNameCode As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        'If Not Page.IsPostBack Then
        ''Get Pass Value and Store in Hidden Fields
        hdnSortName.Value = Request("SN")
        hdnSortAD.Value = Request("SO")
        hdnDeptNameCode.Value = Request("DN")
        'End If

        fnCallReport()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '''********** Start: Check Session Time out ***********
        If Len(Session("UsrID")) = 0 Then
            Response.Redirect("/common/logout.aspx", False)
            Exit Sub
        End If
        '''********** End  : Check Session Time out ***********
        CRVMstListRpt.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None

        'If Not Page.IsPostBack Then
        '    ''Get Pass Value and Store in Hidden Fields
        '    hdnSortName.Value = Request("SN")
        '    hdnSortAD.Value = Request("SO")
        '    hdnDeptNameCode.Value = Request("DN")
        'End If

        'fnCallReport()
    End Sub

    Private Sub fnCallReport()
        ''Get Dataset 
        Dim ds As New DataSet
        ds = CType(Session("dsMstFileListRptRec"), DataSet)

        ''Create a report object
        Dim rptFile As String = Server.MapPath("reportFile\rpt_MstFileList.rpt")
        report.Load(rptFile)
        report.SetDataSource(ds.Tables(0))

        ''Assign Parameter to Report
        report.SetParameterValue("strDeptNameCode", hdnDeptNameCode.Value)

        CRVMstListRpt.ReportSource = report
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close()
        report.Dispose()

        GC.Collect()
    End Sub
End Class
