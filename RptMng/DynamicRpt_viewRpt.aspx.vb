Imports System.Data.SqlClient

Partial Public Class DynamicRpt_viewRpt
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If

            Dim strRptField As String = ""
            Dim strRptType As String = ""

            ''Get Today Date
            lblPrintDate.Text = mfnGetCurrentDateForTopPage()

            ''Get REport Title
            lblRptTitle.Text = clsEncryptDecrypt.DecryptText(Request("RptTitle"))

            ''Get Report Fields
            strRptField = clsEncryptDecrypt.DecryptText(Request("RptField"))

            ''Get Report Type
            strRptType = clsEncryptDecrypt.DecryptText(Request("CallType"))

            ''Dynamic Generate Data grid Column
            fnGenerateDGColumn(strRptField)

            ''Bind record in datagrid
            dgAsset.DataSource = CType(Session("DynamicRpt"), DataSet)
            dgAsset.DataBind()

            ''Set Session to blank
            Session("DynamicRpt") = ""

            ''Populate Report into Excel/Word/Print out
            ''If strRptType = "GR" Then
            ''    '*** Do Nothing ***
            ''    'Dim strJavascript = ""
            ''    'strJavascript = "<script type="text/javascript">window.print()</script>"
            ''    'Response.Write(strJavascript)

            ''ElseIf strRptType = "EE" Then
            ''    Response.ContentType = "application/ms-excel"
            ''    Response.AddHeader("Content-Disposition", "inline;filename=DynamicRpt.xls")

            ''ElseIf strRptType = "EW" Then
            ''    Response.ContentType = "application/ms-word"
            ''    Response.AddHeader("Content-Disposition", "inline;filename=DynamicRpt.doc")

            ''End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub fnGenerateDGColumn(ByVal strRptFields As String)
        Dim objRdr As SqlDataReader
        Try
            ' Create Bound Columns 
            Dim nameColumn As BoundColumn = New BoundColumn()

            objRdr = clsReport.fnDynamicRpt_GetField2Display("ED", strRptFields)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        If Trim(CStr(objRdr("fld_RptFieldName"))) <> "" Then
                            nameColumn = New BoundColumn
                            nameColumn.HeaderText = CStr(objRdr("fld_RptFieldName"))
                            nameColumn.DataField = CStr(objRdr("fld_RptFieldDB"))
                            dgAsset.Columns.Add(nameColumn)
                        End If
                    End While
                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub
End Class