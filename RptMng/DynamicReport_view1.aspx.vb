Public Partial Class DynamicReport_view1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butDelRptTemplate.Attributes.Add("OnClick", "return ConfirmDelete()")

                ''default Sorting
                hdnSortName.Value = "fld_RptTitle"
                hdnSortAD.Value = "ASC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Check for Access Right
                fnCheckAccessRight()

                ''Display Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Sub fnCheckAccessRight()
        Try
            ''**** Checking For Assign Right 
            ''Checking for Generate/Export Report Template access right
            If (InStr(Session("AR"), "Rpt|DymRpt|genRpt") = 0 And InStr(Session("AR"), "Rpt|DymRpt|exportExcel") = 0 And InStr(Session("AR"), "Rpt|DymRpt|exportWord") = 0) Then
                butGenDynamicRpt.Visible = False
            End If

            ''Checking for Create New Report Template access right
            If (InStr(Session("AR"), "Rpt|DymRpt|addRptTpl") = 0) Then butAddRptTemplate.Visible = False

            ''Checking for Delete Report Template access right
            If (InStr(Session("AR"), "Rpt|DymRpt|delRptTpl") = 0) Then
                butDelRptTemplate.Visible = False
                dgReport.Columns(6).Visible = False
            End If

            ''Checking for Edit/Generate/Export Report Template access right
            If (InStr(Session("AR"), "Rpt|DymRpt|editRptTpl") = 0 And InStr(Session("AR"), "Rpt|DymRpt|genRpt") = 0 And InStr(Session("AR"), "Rpt|DymRpt|exportExcel") = 0 And InStr(Session("AR"), "Rpt|DymRpt|exportWord") = 0) Then
                dgReport.Columns(5).Visible = False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Sort Header Display
            fnSortHeaderDisplay()

            ''Get Records
            dgReport.DataSource = clsReport.fnDynamicRpt_GetRptTemplate(hdnSortName.Value, hdnSortAD.Value)
            dgReport.DataBind()
            If Not dgReport.Items.Count > 0 Then '' Not Records found
                'dgUsr.Visible = False
                ddlPageSize.Enabled = False
                butDelRptTemplate.Visible = False
                lblErrorMessage.Text = "There is no report template record."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function fnShowEditHyperlink(ByVal strRptID As String) As String
        Dim strRetVal As String = ""

        If Not (InStr(Session("AR"), "Rpt|DymRpt|genRpt") = 0 And InStr(Session("AR"), "Rpt|DymRpt|exportExcel") = 0 And InStr(Session("AR"), "Rpt|DymRpt|exportWord") = 0) Then
            strRetVal = "<a id=hlUsrViewAudit href='DynamicReport_view.aspx?CallType=E&RID=" + strRptID + "' target=_self >" & _
                        "<img id=imgAudit src=../images/audit.gif alt='Click for Generate Report' border=0 >" & _
                        "</a>"
        End If

        If Not (InStr(Session("AR"), "Rpt|DymRpt|editRptTpl") = 0) Then
            strRetVal += "<a id=hlEditUsr href='DynamicReport_RptTemplate.aspx?CallType=E&RID=" + strRptID + "' target=_self >" & _
                         "<img id=imgUpdate src=../images/update.gif alt='Click for Update Report Template.' border=0 >" & _
                         "</a>"
        End If

        Return (strRetVal)
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By fld_RptTitle (2)
        Dim intSortIndex As Integer
        Dim strSortHeader As String

        ''Set Default Header
        dgReport.Columns(2).HeaderText = "Report Title"


        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fld_RptTitle"
                intSortIndex = 2
                strSortHeader = "Report Title"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgReport.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgReport.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Private Sub dgReport_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgReport.PageIndexChanged
        dgReport.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub dgReport_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgReport.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgReport.CurrentPageIndex = 0
        dgReport.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Protected Sub butGenDynamicRpt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butGenDynamicRpt.Click
        Try
            'Server.Transfer("DynamicReport_view.aspx?CallType=N")
            Response.Redirect("DynamicReport_view.aspx?CallType=N")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butAddRptTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAddRptTemplate.Click
        Try
            Server.Transfer("DynamicReport_RptTemplate.aspx?CallType=N")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butDelRptTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelRptTemplate.Click
        Try
            Dim strMsg As String
            Dim retval As Integer


            ''*** Start: Get Report that selected for deleted
            Dim GridItem As DataGridItem
            Dim chkSelectedDel As CheckBox
            Dim strDelRptID As String = ""
            Dim strDelRpt As String = ""
            For Each GridItem In dgReport.Items
                chkSelectedDel = CType(GridItem.Cells(6).FindControl("DeleteThis"), CheckBox)
                If chkSelectedDel.Checked Then
                    strDelRptID += GridItem.Cells(0).Text & "^"
                    strDelRpt += GridItem.Cells(2).Text & ", "
                End If
            Next
            If Trim(strDelRpt) <> "" Then strDelRpt = Trim(strDelRpt).Substring(0, Trim(strDelRpt).Length - 1)
            ''*** End  : Get Report that selected for deleted

            ''Start: Delete selected records from database
            retval = clsReport.fnDynamicRpt_DeleteRec(strDelRptID, Session("UsrID"))
            ''End  : Delete selected records from database

            If retval > 0 Then
                strMsg = "Selected record(s) deleted successfully."
                fnPopulateRecords()

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Delete Report Template(s) : " + strDelRpt)

                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>alert('" + strMsg + "');</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "Selected record(s) deleted failed."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class