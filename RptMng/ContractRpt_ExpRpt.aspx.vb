Imports System.Data.SqlClient

Partial Public Class ContractRpt_ExpRpt
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If

            Dim strRptField As String = ""
            Dim strRptType As String = ""
            Dim strCallType As String = ""

            ''Get Today Date
            lblPrintDate.Text = mfnGetCurrentDateForTopPage()

            ''Get REport Type
            strRptType = clsEncryptDecrypt.DecryptText(Request("RptType"))

            ''Get REport Title
            'If strRptType = "1" Then
            '    lblRptTitle.Text = "Contract Expiry Report"
            'Else
            '    lblRptTitle.Text = "Tracking Renewal Contract (Vendor)"
            'End If
            lblRptTitle.Text = clsEncryptDecrypt.DecryptText(Request("RptTitle"))

            ''Get Report Fields
            strRptField = clsEncryptDecrypt.DecryptText(Request("RptField"))

            ''Get Report Type
            strCallType = clsEncryptDecrypt.DecryptText(Request("CallType"))

            ''Dynamic Generate Data grid Column
            fnGenerateDGColumn(strRptType, strRptField)

            ''Bind record in datagrid
            dgContract.DataSource = CType(Session("ContractDynamicRpt"), DataSet)
            dgContract.DataBind()

            ''Set Session to blank
            Session("ContractDynamicRpt") = ""

            ''Populate Report into Excel/Word/Print out
            If strCallType = "EE" Then
                Response.ContentType = "application/ms-excel"
                Response.AddHeader("Content-Disposition", "inline;filename=ContractDynamicRpt1.xls")

            ElseIf strCallType = "EW" Then
                Response.ContentType = "application/ms-word"
                Response.AddHeader("Content-Disposition", "inline;filename=ContractDynamicRpt2.doc")

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub fnGenerateDGColumn(ByVal RptType As String, ByVal strRptFields As String)
        Try
            ' Create Bound Columns 
            Dim nameColumn As BoundColumn = New BoundColumn()

            Dim objRdr As SqlDataReader
            objRdr = clsReport.fnContractRpt_GetField2Display("GR", RptType, strRptFields)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        If Trim(CStr(objRdr("fld_RptFieldName"))) <> "" Then
                            nameColumn = New BoundColumn
                            nameColumn.HeaderText = CStr(objRdr("fld_RptFieldName"))
                            nameColumn.DataField = CStr(objRdr("fld_RptFieldDB"))
                            dgContract.Columns.Add(nameColumn)
                        End If
                    End While
                End If
            End If
            objRdr.Close()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class