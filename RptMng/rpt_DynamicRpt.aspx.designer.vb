'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class rpt_DynamicRpt

    '''<summary>
    '''frm_RptSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents frm_RptSearch As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''lblErrorMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblErrorMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtRptTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRptTitle As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlDelinq control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlDelinq As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''cbFileNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbFileNo As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''txtFileNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFileNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''cbFileRef control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbFileRef As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''txtFileRef control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFileRef As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''cbOffLoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbOffLoc As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''divOffLoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divOffLoc As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''rdOffLoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdOffLoc As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''lbOfficer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbOfficer As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''lbLocation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbLocation As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''cbCrtDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbCrtDt As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''divCrtDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divCrtDt As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''txtCrtFromDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCrtFromDt As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCrtToDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCrtToDt As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''cbSecurityStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbSecurityStatus As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''ddlSecurityStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlSecurityStatus As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''cbStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbStatus As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''divStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divStatus As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ddlStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlStatus As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlStatusRemark control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlStatusRemark As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''cbReviewDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbReviewDt As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''divReviewDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divReviewDt As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''txtReviewFD control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtReviewFD As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtReviewTD control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtReviewTD As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''cbCloseDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbCloseDt As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''divCloseDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divCloseDt As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''txtCloseFD control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCloseFD As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCloseTD control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCloseTD As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''DeleteThis control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DeleteThis As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''butGenRep control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents butGenRep As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''butExp2Excel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents butExp2Excel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''butReset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents butReset As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''hdnStatusID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnStatusID As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Footer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Footer As Global.AMS.FTSfooter_cr
End Class
