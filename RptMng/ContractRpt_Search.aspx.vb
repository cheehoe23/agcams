#Region "Information Section"
' ****************************************************************************************************
' Description       : ContractDynamic Report
' Purpose           : ContractDynamic Report Information
' Date              : 28/12/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Web.Services

#End Region

Partial Public Class ContractRpt_Search
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            Me.Header.DataBind()

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butGenerate.Attributes.Add("OnClick", "return chkFrm()")
                Me.butExpExcel.Attributes.Add("OnClick", "return chkFrm()")
                Me.butExpWord.Attributes.Add("OnClick", "return chkFrm()")
                Me.butSelectAllDisplay.Attributes.Add("OnClick", "return addAll()")
                Me.butSelectDisplay.Attributes.Add("OnClick", "return addAttribute()")
                Me.butSelectNDisplay.Attributes.Add("OnClick", "return delAttribute()")
                Me.butSelectAllNDisplay.Attributes.Add("OnClick", "return delAll()")
                Me.butTop.Attributes.Add("OnClick", "return top('selectedOptions')")
                Me.butUp.Attributes.Add("OnClick", "return up('selectedOptions')")
                Me.butDown.Attributes.Add("OnClick", "return down('selectedOptions')")
                Me.butBottom.Attributes.Add("OnClick", "return bottom('selectedOptions')")

                ''** Set Default
                hdnRptType.Value = IIf(Trim(Request("RptType")) = "", "3", "1")    '"N"=New Report, "E"=From Existing Report Template
                hdnSelectDisplay.Value = ""

                ''*** Checking Report Type --> 1 = Contract Expiry Report, 2 = Tracking Renewal Contract (Vendor), 3 = List of Contract
                rdRptType.SelectedValue = hdnRptType.Value
                rdRptType_SelectedIndexChanged(Nothing, Nothing)

                ''** Populate Control
                fnPopulateCtrl()

                ''Checking Access Right
                fnCheckAccessRight()
            End If

            ''To Avoid Field to Diplay Missing... Re-generate Field to display
            If hdnSelectDisplay.Value <> "" Then
                ''Display Fields (Available)
                availableOptions.DataSource = clsReport.fnContractRpt_GetField2Display("AF", rdRptType.SelectedValue, hdnSelectDisplay.Value)
                availableOptions.DataTextField = "fld_RptCtrtFieldName"
                availableOptions.DataValueField = "fld_RptCtrtFieldID"
                availableOptions.DataBind()

                ''Display Fields (Field to Display)
                selectedOptions.DataSource = clsReport.fnContractRpt_GetField2Display("DF", rdRptType.SelectedValue, hdnSelectDisplay.Value)
                selectedOptions.DataTextField = "fld_RptFieldName"
                selectedOptions.DataValueField = "fld_RptFieldDisplayID"
                selectedOptions.DataBind()
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False)
            If Session("AdminF") = "N" Then ddlDepartment.Items.Remove(ddlDepartment.Items.FindByValue("-1"))

            ''Contract Expiry Notification
            ddlContExpNoti.Items.Add(New ListItem("", "-1"))
            fnPopulateNumberInDDL(ddlContExpNoti, "12")

            ''Security Deposit Expiry Notification
            ddlSecDpsNoti.Items.Add(New ListItem("", "-1"))
            fnPopulateNumberInDDL(ddlSecDpsNoti, "12")

            ''Banker's Guarantee Expiry Notification
            ddlBankGuaNoti.Items.Add(New ListItem("", "-1"))
            fnPopulateNumberInDDL(ddlBankGuaNoti, "12")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnPopulateCtrlRecord()
        Try
            ''Display Fields (Available)
            availableOptions.DataSource = clsReport.fnContractRpt_GetField2Display("AF", rdRptType.SelectedValue, "")
            availableOptions.DataTextField = "fld_RptCtrtFieldName"
            availableOptions.DataValueField = "fld_RptCtrtFieldID"
            availableOptions.DataBind()

            ''Display Fields (Field to Display)
            selectedOptions.DataSource = clsReport.fnContractRpt_GetField2Display("DF", rdRptType.SelectedValue, "")
            selectedOptions.DataTextField = "fld_RptFieldName"
            selectedOptions.DataValueField = "fld_RptFieldDisplayID"
            selectedOptions.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub rdRptType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdRptType.SelectedIndexChanged
        Try
            ''*** Report Type --> 1 = Contract Expiry Report, 2 = Tracking Renewal Contract (Vendor)
            hdnRptType.Value = rdRptType.SelectedValue
            If rdRptType.SelectedValue = "1" Then
                rdArchiveF.SelectedValue = "N"
                rdArchiveF.Enabled = False
            Else
                rdArchiveF.SelectedValue = "N"
                rdArchiveF.Enabled = True
            End If

            ''reset selected fields
            hdnSelectDisplay.Value = ""

            ''Populate Report Fields 
            fnPopulateCtrlRecord()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnCheckAccessRight()
        Try
            If InStr(Session("AR"), "Rpt|CtrtDymRpt|genRpt") = 0 Then butGenerate.Visible = False

            If InStr(Session("AR"), "Rpt|CtrtDymRpt|exportExcel") = 0 Then butExpExcel.Visible = False

            If InStr(Session("AR"), "Rpt|CtrtDymRpt|exportWord") = 0 Then butExpWord.Visible = False

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            Server.Transfer("ContractRpt_Search.aspx")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butGenerate.Click
        Try
            fnGenerateRpt("GR")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butExpExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExpExcel.Click
        Try
            fnGenerateRpt("EE")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butExpWord_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butExpWord.Click
        Try
            fnGenerateRpt("EW")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub fnGenerateRpt(ByVal strRptType As String)
        Try
            Dim dsContract As New DataSet
            Dim strRptTitle As String = ""

            Dim strContID As String = Trim(txtContractID.Text)
            Dim strContTitle As String = Trim(txtContTitle.Text)
            Dim strContCost As String = Trim(tctContCost.Text)
            Dim strContSFrmDt As String = Trim(txtContSFrmDt.Text)
            Dim strContSToDt As String = Trim(txtContSToDt.Text)
            Dim strContEFrmDt As String = Trim(txtContEFrmDt.Text)
            Dim strContEToDt As String = Trim(txtContEToDt.Text)
            Dim intContNoti As Integer = ddlContExpNoti.SelectedValue
            Dim strVendorName As String = Trim(txtVendorName.Text)
            Dim strDesc As String = Trim(txtDesc.Text)
            Dim strContOwner As String = ""
            Dim strContOwnerDept As Integer = IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue)

            Dim strSecDps As String = Trim(txtSecurityDeposit.Text)
            Dim strSecDpsSFrmDt As String = Trim(txtSecurityDepSFDt.Text)
            Dim strSecDpsSToDt As String = Trim(txtSecurityDepSTDt.Text)
            Dim strSecDpsEFrmDt As String = Trim(txtSecurityDepEFDt.Text)
            Dim strSecDpsEToDt As String = Trim(txtSecurityDepETDt.Text)
            Dim intSecDpsNoti As Integer = ddlSecDpsNoti.SelectedValue
            Dim strSecDpsRefNo As String = Trim(txtSecDpsRefNo.Text)

            Dim strBankGuaAmt As String = Trim(txtBankGuarAmount.Text)
            Dim strBankGuaSFrmDt As String = Trim(txtBankGuarSFDt.Text)
            Dim strBankGuaSToDt As String = Trim(txtBankGuarSTDt.Text)
            Dim strBankGuaEFrmDt As String = Trim(txtBankGuarEFDt.Text)
            Dim strBankGuaEToDt As String = Trim(txtBankGuarETDt.Text)
            Dim intBankGuaNoti As Integer = ddlBankGuaNoti.SelectedValue
            Dim strBankGuaRefNo As String = Trim(txtBankGuarRefNo.Text)

            Dim strOffSignName As String = "" 'Trim(txtOffSignName.Text)
            Dim strOffSignDesg As String = Trim(txtOffSignDes.Text)
            Dim strAssetID As String = Trim(txtAssetID.Text)
            Dim strArchiveF As String = rdArchiveF.SelectedValue

            Dim arrSelectField As Array
            Dim strOrderBy1 As String = ""
            Dim strOrderBy2 As String = ""
            Dim strOrderBy3 As String = ""
            Dim intCount As Integer = "0"

            strContID = strContID.Replace("'", "''")
            strContTitle = strContTitle.Replace("'", "''")
            strContCost = strContCost.Replace("'", "''")
            strContSFrmDt = strContSFrmDt.Replace("'", "''")
            strContSToDt = strContSToDt.Replace("'", "''")
            strContEFrmDt = strContEFrmDt.Replace("'", "''")
            strContEToDt = strContEToDt.Replace("'", "''")

            strVendorName = strVendorName.Replace("'", "''")
            strContOwner = strContOwner.Replace("'", "''")
            strSecDps = strSecDps.Replace("'", "''")

            strSecDpsSFrmDt = strSecDpsSFrmDt.Replace("'", "''")
            strSecDpsSToDt = strSecDpsSToDt.Replace("'", "''")
            strSecDpsEFrmDt = strSecDpsEFrmDt.Replace("'", "''")
            strSecDpsEToDt = strSecDpsEToDt.Replace("'", "''")
            strSecDpsRefNo = strSecDpsRefNo.Replace("'", "''")
            strBankGuaAmt = strBankGuaAmt.Replace("'", "''")

            strBankGuaSFrmDt = strBankGuaSFrmDt.Replace("'", "''")
            strBankGuaSToDt = strBankGuaSToDt.Replace("'", "''")
            strBankGuaEFrmDt = strBankGuaEFrmDt.Replace("'", "''")
            strBankGuaEToDt = strBankGuaEToDt.Replace("'", "''")
            strBankGuaRefNo = strBankGuaRefNo.Replace("'", "''")
            strOffSignName = strOffSignName.Replace("'", "''")

            strOffSignDesg = strOffSignDesg.Replace("'", "''")
            strAssetID = strAssetID.Replace("'", "''")

            ''*** Get Contract Owner
            If Trim(cbOwner.Text) <> "" Then
                Dim strUsrExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbOwner.Text), strContOwner, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "Contract Owner not found. Please select an existing Contract Owner."
                    lblErrorMessage.Visible = True
                    cbOwner.Focus()
                    Exit Sub
                End If
            End If

            ''*** Get Name of Officer who signed the contract
            If Trim(cbOffSignName.Text) <> "" Then
                Dim strOffExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbOffSignName.Text), strOffSignName, strOffExistF)
                If strOffExistF = "N" Then
                    lblErrorMessage.Text = "Name of Officer who signed the contract not found. Please select an existing Name of Officer who signed the contract."
                    lblErrorMessage.Visible = True
                    cbOffSignName.Focus()
                    Exit Sub
                End If
            End If


            ''*** Get Order by 
            arrSelectField = Split(CStr(hdnSelectDisplay.Value), "^")
            For intCount = 0 To UBound(arrSelectField) - 1
                If intCount = "0" Then
                    strOrderBy1 = arrSelectField(intCount)
                ElseIf intCount = "1" Then
                    strOrderBy2 = arrSelectField(intCount)
                ElseIf intCount = "2" Then
                    strOrderBy3 = arrSelectField(intCount)
                End If
            Next


            If rdRptType.SelectedValue = "1" Then   '' Contract Expiry Report
                dsContract = clsReport.fnContractRpt_GenContractExpRpt( _
                                strContID, strContTitle, strContCost, strContSFrmDt, strContSToDt, _
                                strContEFrmDt, strContEToDt, intContNoti, strVendorName, strDesc, _
                                strContOwner, strContOwnerDept, strSecDps, strSecDpsSFrmDt, strSecDpsSToDt, _
                                strSecDpsEFrmDt, strSecDpsEToDt, intSecDpsNoti, strSecDpsRefNo, _
                                strBankGuaAmt, strBankGuaSFrmDt, strBankGuaSToDt, strBankGuaEFrmDt, strBankGuaEToDt, _
                                intBankGuaNoti, strBankGuaRefNo, strOffSignName, strOffSignDesg, _
                                strAssetID, strArchiveF, _
                                strOrderBy1, strOrderBy2, strOrderBy3)

                strRptTitle = "Contract Expiry Report"

            ElseIf rdRptType.SelectedValue = "2" Then  ''Tracking Renewal Contract (Vendor)
                dsContract = clsReport.fnContractRpt_GenTrackRenewalCtrtVendorRpt( _
                                strContID, strContTitle, strContCost, strContSFrmDt, strContSToDt, _
                                strContEFrmDt, strContEToDt, intContNoti, strVendorName, strDesc, _
                                strContOwner, strContOwnerDept, strSecDps, strSecDpsSFrmDt, strSecDpsSToDt, _
                                strSecDpsEFrmDt, strSecDpsEToDt, intSecDpsNoti, strSecDpsRefNo, _
                                strBankGuaAmt, strBankGuaSFrmDt, strBankGuaSToDt, strBankGuaEFrmDt, strBankGuaEToDt, _
                                intBankGuaNoti, strBankGuaRefNo, strOffSignName, strOffSignDesg, _
                                strAssetID, strArchiveF, _
                                strOrderBy1, strOrderBy2, strOrderBy3)

                strRptTitle = "Tracking Renewal Contract (Vendor)"

            Else  ''List of Contract
                dsContract = clsReport.fnContractRpt_GenListOfContract( _
                                                strContID, strContTitle, strContCost, strContSFrmDt, strContSToDt, _
                                                strContEFrmDt, strContEToDt, intContNoti, strVendorName, strDesc, _
                                                strContOwner, strContOwnerDept, strSecDps, strSecDpsSFrmDt, strSecDpsSToDt, _
                                                strSecDpsEFrmDt, strSecDpsEToDt, intSecDpsNoti, strSecDpsRefNo, _
                                                strBankGuaAmt, strBankGuaSFrmDt, strBankGuaSToDt, strBankGuaEFrmDt, strBankGuaEToDt, _
                                                intBankGuaNoti, strBankGuaRefNo, strOffSignName, strOffSignDesg, _
                                                strAssetID, strArchiveF, _
                                                strOrderBy1, strOrderBy2, strOrderBy3)

                strRptTitle = "List of Contract"
            End If

            If dsContract.Tables(0).Rows.Count > 0 Then
                ''Set session to blank
                Session("ContractDynamicRpt") = ""

                ''assign dataset to Session
                Session("ContractDynamicRpt") = dsContract

                ''pop-up window for report
                If strRptType = "GR" Then
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "window.open('ContractRpt_viewRpt.aspx?RptType=" & clsEncryptDecrypt.EncryptText(Trim(rdRptType.SelectedValue)) & "&RptTitle=" & clsEncryptDecrypt.EncryptText(CStr(strRptTitle)) & "&RptField=" & clsEncryptDecrypt.EncryptText(CStr(hdnSelectDisplay.Value)) & "', 'Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1,menubar=1'); " & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "window.open('ContractRpt_ExpRpt.aspx?RptType=" & clsEncryptDecrypt.EncryptText(Trim(rdRptType.SelectedValue)) & "&RptTitle=" & clsEncryptDecrypt.EncryptText(CStr(strRptTitle)) & "&RptField=" & clsEncryptDecrypt.EncryptText(CStr(hdnSelectDisplay.Value)) & "&CallType=" & clsEncryptDecrypt.EncryptText(CStr(strRptType)) & "', 'Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1,menubar=1'); " & _
                                    "</script>"
                    Response.Write(strJavaScript)
                End If

            Else
                lblErrorMessage.Visible = True
                lblErrorMessage.Text = "Record(s) not found."
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("OU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class