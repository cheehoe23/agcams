﻿#Region "Information Section"
' ****************************************************************************************************
' Description       : Display Dynamic Report
' Purpose           : Display Dynamic Information
' Author            : See Siew
' Date              : 19/04/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
#End Region

Public Class rpt_FTS_showDynamicRpt
    Inherits System.Web.UI.Page

    Private report As New ReportDocument()

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents CRVdynamicRpt As CrystalDecisions.Web.CrystalReportViewer
    Protected WithEvents hdnRunNo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents hdnRptT As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents hdnDspyField As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        'If Not Page.IsPostBack Then
        ''Get Pass Value and Store in Hidden Fields
        'hdnRunNo.Value = Request("RunNo")
        hdnRptT.Value = Request("RptT")
        hdnDspyField.Value = Request("DF")
        'End If

        fnCallReport()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '''********** Start: Check Session Time out ***********
        If Len(Session("UsrID")) = 0 Then
            Response.Redirect("/common/logout.aspx", False)
            Exit Sub
        End If
        '''********** End  : Check Session Time out ***********
        CRVdynamicRpt.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
        If Not Page.IsPostBack Then
            ''Get Pass Value and Store in Hidden Fields
            'hdnRunNo.Value = Request("RunNo")
            hdnRptT.Value = Request("RptT")
            hdnDspyField.Value = Request("DF")
        End If

        'fnCallReport()
    End Sub


    Private Sub fnCallReport()
        Dim RunNo As Integer
        Dim strDisplayCol As String
        Dim ReportTitle As String
        Dim arrDisplayCol() As String
        Dim arrParaVal() As String = {"ELMS File Ref No", "External File Ref No", "Exteranl File Title", "Officer/Location", "Status", "Create Date", "Closed Date", "Last Movement Date"}
        Dim i As Integer
        Dim strParaVal As String


        ''Get Passing Parameter
        'RunNo = hdnRunNo.Value ' Request("RunNo")
        ReportTitle = hdnRptT.Value  'Request("RptT")
        strDisplayCol = hdnDspyField.Value  'Request("DF")
        arrDisplayCol = Split(strDisplayCol, "^")
        'RunNo = "9"
        'ReportTitle = "Test for Report Title"
        'strDisplayCol = "1^2^3^4^5^6^"
        'arrDisplayCol = Split(strDisplayCol, "^")

        ''Get Dataset 
        Dim ds As New DataSet
        ds = CType(Session("dsDynamicRptRec"), DataSet)

        ''Create a report object
        Dim rptFile As String = Server.MapPath("RptFile\rpt_FTS_dynamic.rpt")
        report.Load(rptFile)
        report.SetDataSource(ds.Tables(0))

        ''Assign Parameter to Report
        'report.SetParameterValue("RunNo", RunNo)
        report.SetParameterValue("RptTitle", ReportTitle)
        For i = 1 To 8
            ''get Display Column Selected by User
            If UBound(arrDisplayCol) >= i Then
                If arrDisplayCol(i - 1) <> "" Then
                    strParaVal = arrParaVal(arrDisplayCol(i - 1) - 1)
                Else
                    strParaVal = "None"
                End If
            Else
                strParaVal = "None"
            End If

            ''Assign Parameter for Report
            report.SetParameterValue("F" & i, strParaVal)
        Next

        CRVdynamicRpt.ReportSource = report
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        report.Close()
        report.Dispose()

        GC.Collect()
    End Sub
End Class