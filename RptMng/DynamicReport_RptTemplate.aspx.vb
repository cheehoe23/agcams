#Region "Information Section"
' ****************************************************************************************************
' Description       : Create/Edit Report Template
' Purpose           : Create/Edit Report Template Information
' Date              : 08/11/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class DynamicReport_RptTemplate
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butSaveTemplate.Attributes.Add("OnClick", "return chkFrm('Save')")
                Me.butUpdateTemplate.Attributes.Add("OnClick", "return chkFrm('Update')")
                Me.butSelectAllDisplay.Attributes.Add("OnClick", "return addAll()")
                Me.butSelectDisplay.Attributes.Add("OnClick", "return addAttribute()")
                Me.butSelectNDisplay.Attributes.Add("OnClick", "return delAttribute()")
                Me.butSelectAllNDisplay.Attributes.Add("OnClick", "return delAll()")
                Me.butTop.Attributes.Add("OnClick", "return top('selectedOptions')")
                Me.butUp.Attributes.Add("OnClick", "return up('selectedOptions')")
                Me.butDown.Attributes.Add("OnClick", "return down('selectedOptions')")
                Me.butBottom.Attributes.Add("OnClick", "return bottom('selectedOptions')")

                hdnCallType.Value = Request("CallType")
                hdnRptTemplateID.Value = Request("RID")
                hdnSelectDisplay.Value = ""

                ''Populate Control
                fnPopulateCtrlRecord()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrlRecord()
        Dim objRdr As SqlDataReader
        Try
            ''Get Field to Display
            If hdnCallType.Value = "N" Then
                ''*** For Create New Report Template 
                lblRptTitle.Text = "Create New Report Template"
                butUpdateTemplate.Visible = False

                ''populate control
                availableOptions.DataSource = clsReport.fnDynamicRpt_GetField2Display("NR", "")
                availableOptions.DataTextField = "fld_RptFieldName"
                availableOptions.DataValueField = "fld_RptFieldDisplayID"
                availableOptions.DataBind()

            Else
                ''*** For Edit Report Template 
                lblRptTitle.Text = "Edit Report Template"
                butSaveTemplate.Visible = False

                ''Populate Records
                Dim strFieldDisplay As String = ""
                objRdr = clsReport.fnDynamicRpt_GetRptTemplateData(hdnRptTemplateID.Value)
                If Not objRdr Is Nothing Then
                    If objRdr.HasRows Then
                        objRdr.Read()
                        txtRptTitle.Text = CStr(objRdr("fld_RptTitle"))
                        strFieldDisplay = CStr(objRdr("fld_RptFieldDisplay"))
                    End If
                End If
                objRdr.Close()

                ''Display Fields (Available)
                availableOptions.DataSource = clsReport.fnDynamicRpt_GetField2Display("EN", strFieldDisplay)
                availableOptions.DataTextField = "fld_RptFieldName"
                availableOptions.DataValueField = "fld_RptFieldDisplayID"
                availableOptions.DataBind()

                ''Display Fields
                selectedOptions.DataSource = clsReport.fnDynamicRpt_GetField2Display("ED", strFieldDisplay)
                selectedOptions.DataTextField = "fld_RptFieldName"
                selectedOptions.DataValueField = "fld_RptFieldDisplayID"
                selectedOptions.DataBind()
            End If

        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Protected Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            txtRptTitle.Text = ""
            availableOptions.Items.Clear()
            selectedOptions.Items.Clear()
            fnPopulateCtrlRecord()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butUpdateTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdateTemplate.Click
        Try
            fnSaveUpdateReportTemplate()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butSaveTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSaveTemplate.Click
        Try
            fnSaveUpdateReportTemplate()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnSaveUpdateReportTemplate()
        Try
            Dim intRetVal As Integer = "0"
            Dim strMsg As String = ""
            Dim strCallType As String = hdnCallType.Value
            Dim strReportTitle As String = Trim(txtRptTitle.Text)
            Dim strFieldDisplay As String = hdnSelectDisplay.Value

            ''Get Field to display
            'Dim Item As New ListItem
            'For Each Item In selectedOptions.Items
            '    strFieldDisplay = strFieldDisplay & CStr(Item.Value) & "^"
            'Next

            ''Insert/Update Record
            intRetVal = clsReport.fnDynamicRpt_InsertUpdateRptTemplate(IIf(hdnRptTemplateID.Value = "", "0", hdnRptTemplateID.Value), strReportTitle, strFieldDisplay, Session("UsrID"), strCallType)

            If intRetVal > 0 Then
                If strCallType = "N" Then
                    strMsg = "New Report Template Added Successfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Report Template created : " + strReportTitle)
                Else
                    strMsg = "Report Template Update Successfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Report Template updated : " + strReportTitle)
                End If


                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" & strMsg & "');" & _
                                "document.location.href='DynamicReport_view1.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                If strCallType = "N" Then
                    strMsg = "New Report Template Added Failed."
                Else
                    strMsg = "New Report Template Update Failed."
                End If

                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Try
            Server.Transfer("DynamicReport_view1.aspx")
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class