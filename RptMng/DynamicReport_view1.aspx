<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DynamicReport_view1.aspx.vb" Inherits="AMS.DynamicReport_view1" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script language="javascript">
		function ConfirmDelete() 
			{
				var flag;
				flag = false;
				if (gfnCheckDelSelect()){
					flag = window.confirm("You have chosen to delete one or more Report Template.\nYou cannot undo this action. Continue?");
				}
				else  {
					flag = false;
				}
				return flag;
			}
		</script>
</head>
<body>
   <form id="Frm_DynamicRpt" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>Report Management : Dynamic Report</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY align=left>
																	<TR>
																		<TD height="4">
																			<DIV align="left"></DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<!--Start Main Content-->
						                                                    <table height="400" cellSpacing="1" cellPadding="0" width="100%" align="left" border="0">
							                                                    <tr>
								                                                    <td vAlign="top" align="left" style="height: 21px">
								                                                        <asp:button id="butGenDynamicRpt" Runat="server" Text="Generate Dynamic Report" Width="189px"></asp:button>
									                                                    <asp:button id="butAddRptTemplate" Runat="server" Text="Create New Report Template" Width="215px"></asp:button>
									                                                    <asp:button id="butDelRptTemplate" Runat="server" Text="Delete Report Template" Width="191px"></asp:button>
								                                                    </td>
							                                                    </tr>
							                                                    <tr>
								                                                    <td vAlign="top" align="right" bgColor="#a3a9cc">
									                                                    <table cellSpacing="1" cellPadding="1" width="100%" border="0">
										                                                    <tr align=left>
											                                                    <td width="20%"><asp:dropdownlist id="ddlPageSize" Runat="server" AutoPostBack="True"></asp:dropdownlist>&nbsp;&nbsp;<font color="white">per 
													                                                    page</font>
											                                                    </td>
										                                                    </tr>
									                                                    </table>
								                                                    </td>
							                                                    </tr>
							                                                    <tr>
								                                                    <td vAlign="top" align="center" height="400">
									                                                    <!--Datagrid for display record.-->
									                                                    <asp:datagrid id="dgReport" Runat="server" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
										                                                    ItemStyle-Height="25" AllowSorting="True" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
										                                                    BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle" AllowPaging="True" PageSize="10" PagerStyle-HorizontalAlign="Right"
										                                                    PagerStyle-VerticalAlign="Middle" PagerStyle-Mode="NumericPages" PagerStyle-Position="Top"
										                                                    PagerStyle-BackColor="#a3a9cc" datakeyfield="fld_RptTemplateID" AlternatingItemStyle-BackColor="#e3d9ee">
										                                                    <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
											                                                    Height="25"></headerstyle>
										                                                    <Columns>
											                                                    <asp:boundcolumn visible="false" datafield="fld_RptTemplateID" headertext="fld_RptTemplateID" ItemStyle-Height="10">
												                                                    <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                    </asp:boundcolumn>
											                                                    <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
												                                                    <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
											                                                    </asp:boundcolumn>
											                                                    <asp:boundcolumn datafield="fld_RptTitle" SortExpression="fld_RptTitle" headertext="Report Title" ItemStyle-Height="10">
												                                                    <itemstyle width="40%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                    </asp:boundcolumn>
											                                                    <asp:boundcolumn datafield="fld_RptFieldDisplay" headertext="Field(s) To Display" ItemStyle-Height="10">
												                                                    <itemstyle width="30%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
											                                                    </asp:boundcolumn>
											                                                    <asp:boundcolumn datafield="fld_CreatedDt" headertext="Creation Date" ItemStyle-Height="10" dataformatstring="{0:dd/MM/yyyy}" >
												                                                    <itemstyle width="15%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Center></itemstyle>
											                                                    </asp:boundcolumn>
											                                                    <asp:templatecolumn HeaderText="Select" ItemStyle-Width="7%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
												                                                    <itemtemplate>
													                                                    <%#fnShowEditHyperlink(DataBinder.Eval(Container.DataItem, "fld_RptTemplateID"))%>
												                                                    </itemtemplate>
											                                                    </asp:templatecolumn>
											                                                    <asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="3%">
												                                                    <headertemplate>
													                                                    <img src="../images/delete.gif" alt="Delete" runat="server" id="imgDelete">
													                                                    <asp:checkbox id="CheckAll" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
														                                                    runat="server" />
												                                                    </headertemplate>
												                                                    <itemtemplate>
													                                                    <center>
														                                                    <asp:checkbox id="DeleteThis" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
															                                                    runat="server" />
													                                                    </center>
												                                                    </itemtemplate>
											                                                    </asp:templatecolumn>
										                                                    </Columns>
									                                                    </asp:datagrid>
								                                                    </td>
							                                                    </tr>
						                                                    </table>
						                                                    <!--End Main Content-->
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start: Hidden Fields -->
			<input id="hdnSortName" type="hidden" name="hdnSortName" runat="server"> 
			<input id="hdnSortAD" type="hidden" name="hdnSortAD" runat="server">
			<!-- End  : Hidden Fields -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
	</form>
</body>
</html>
