'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.42
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On



'''<summary>
'''ContractRpt_viewRpt class.
'''</summary>
'''<remarks>
'''Auto-generated class.
'''</remarks>
Partial Public Class ContractRpt_viewRpt

    '''<summary>
    '''frn_ContractRpt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents frn_ContractRpt As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''lblRptTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRptTitle As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPrintDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPrintDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''dgContract control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dgContract As Global.System.Web.UI.WebControls.DataGrid
End Class
