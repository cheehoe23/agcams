#Region "Information Section"
' ****************************************************************************************************
' Description       : Generate Master File List
' Purpose           : Generate Master File List
' Author            : See Siew
' Date              : 21/04/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class MstFile_ViewRpt
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            '''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("/common/logout.aspx", False)
                Exit Sub
            End If
            '''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butGenRpt.Attributes.Add("OnClick", "return chkFrm('rpt')")

                ''Checking For Assign Right
                If (InStr(Session("AR"), "Rpt|MstFile|GenRpt") = 0) Then butGenRpt.Visible = False

                ''Populate Department
                fnPopulateDropDownListWithDefaultValue(clsDepartment.fnDeptGetAllRecDDL("F"), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False, "All", "")

                ''default Sorting
                hdnSortName.Value = "FileNum"
                hdnSortAD.Value = "ASC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            ''' Sort Header Display
            fnSortHeaderDisplay()

            ''set session to Empty
            Session("dsMstFileListRptRec") = ""

            ''Get User Group Records
            Dim ds As New DataSet
            ds = clsReport.fnRep_GetRecForMstFileList(ddlDepartment.SelectedValue, hdnSortName.Value, hdnSortAD.Value)
            dgMstList.DataSource = ds
            dgMstList.DataBind()
            If Not dgMstList.Items.Count > 0 Then '''Not  Records found

                dgMstList.Visible = False
                ddlPageSize.Enabled = False
                butGenRpt.Visible = False
                lblErrorMessage.Text = "There is no record(s) found."
                lblErrorMessage.Visible = True
            Else
                ''assign Dataset to Session
                Session("dsMstFileListRptRec") = ds

                dgMstList.Visible = True
                ddlPageSize.Enabled = True
                butGenRpt.Visible = True
                If (InStr(Session("AR"), "Rpt|MstFile|GenRpt") = 0) Then butGenRpt.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By DeptCode (2), FileNum (3), FileRef (4), FileTitle (5)
        Dim intSortIndex As Integer
        Dim strSortHeader As String

        ''Set Default Header
        dgMstList.Columns(2).HeaderText = "Reference"
        dgMstList.Columns(3).HeaderText = "File Number"
        dgMstList.Columns(4).HeaderText = "File Reference"
        dgMstList.Columns(5).HeaderText = "File Title"

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "DeptCode"
                intSortIndex = 2
                strSortHeader = "Reference"
            Case "FileNum"
                intSortIndex = 3
                strSortHeader = "File Number"
            Case "FileRef"
                intSortIndex = 4
                strSortHeader = "File Reference"
            Case "FileTitle"
                intSortIndex = 5
                strSortHeader = "File Title"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgMstList.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgMstList.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgMstList.CurrentPageIndex = 0
        dgMstList.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Private Sub dgMstList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgMstList.PageIndexChanged
        dgMstList.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub dgMstList_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgMstList.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

    Private Sub butGenRpt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butGenRpt.Click
        Try
            Dim strParameterP, strJavascript As String
            strParameterP = "MstFile_showRpt.aspx?SN=" + hdnSortName.Value + "&SO=" + hdnSortAD.Value + "&DN=" + ddlDepartment.SelectedItem.ToString

            ''Call Pop-up window to generate report
            strJavascript = "<script language=javascript>" & _
                            "window.open('" & strParameterP & "', 'MstFileListRptScreen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1');" & _
                            "</script>"
            Response.Write(strJavascript)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub ddlDepartment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlDepartment.SelectedIndexChanged
        Try
            dgMstList.CurrentPageIndex = 0
            fnPopulateRecords()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class
