#Region "Information Section"
' ****************************************************************************************************
' Description       : Add User Group
' Purpose           : Add User Group
' Author            : See Siew
' Date              : 17/01/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class usrGrp_Add
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label
    Protected WithEvents cb_module As System.Web.UI.HtmlControls.HtmlInputCheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public gStrModule As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butCheckAll.Attributes.Add("OnClick", "return checkAll()")
                Me.butUnCheckAll.Attributes.Add("OnClick", "return uncheckAll()")
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")
            Else
                gStrModule = IIf(Request.Form("cb_moduleName") = "", "", Request.Form("cb_moduleName"))
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub DisplayModule()
        Dim myReader As SqlDataReader
        Dim myReader1 As SqlDataReader
        Dim myReader2 As SqlDataReader

        Try
            Dim strOutput, strCbVal As String
            Dim intCount As Integer

            ''***Start: 1. Get Module Name 
            myReader = clsUserGroup.fnUsrGrpGetModuleFunction("", "", "M")
            While myReader.Read

                strOutput += "<TR class='TRTitleBG'><TD width=100% valign=middle colspan=5><b>" & myReader("FLD_ModuleName") & "</b></TD></TR><TR>"

                ''***Start: 2. Get Sub Module
                myReader1 = clsUserGroup.fnUsrGrpGetModuleFunction(myReader("fld_moduleValue"), "", "S")
                While myReader1.Read
                    intCount = 1
                    strOutput += "<TR><TD width='20%' align=left><FONT color=#666666 >" & myReader1("fld_subModuleName") & "</font></TD>"

                    ''***Start: 3. Get Function
                    myReader2 = clsUserGroup.fnUsrGrpGetModuleFunction(myReader("fld_moduleValue"), myReader1("fld_subModuleValue"), "F")
                    While myReader2.Read
                        strCbVal = myReader("fld_ModuleValue") & "|" & myReader1("fld_subModuleValue") & "|" & myReader2("fld_functionValue")

                        If intCount > 4 Then
                            strOutput += "</TR><TR><TD width=20% valign=middle>&nbsp;</TD>"
                            intCount = 1
                        End If

                        strOutput += "<td width='20%' align=left><input style='Border:none;'"
                        If Trim(gStrModule) <> "" And (InStr(Trim(gStrModule), strCbVal) > 0) Then strOutput += " checked "
                        strOutput += " type='checkbox' id=cb_module name=cb_moduleName value='" & strCbVal & "' runat='server'>" _
                                          & myReader2("fld_functionName") & "</td>"
                        intCount = intCount + 1
                    End While
                    myReader2.Close()

                    strOutput += "</TR><TR><TD width=20% valign=middle>&nbsp;</TD><TD colspan=4><HR></TD></TR>"
                    ''***End:  3. Get Function

                End While
                myReader1.Close()

                strOutput += "</TR><TR><TD colspan=5><BR></TD></TR>"
                ''***End:  2. Get Sub Module

            End While
            myReader.Close()
            ''***End : 1. Get Module Name 

            Response.Write(strOutput)
        Catch ex As Exception
            clsCommon.fnDataReader_Close(myReader)
            clsCommon.fnDataReader_Close(myReader1)
            clsCommon.fnDataReader_Close(myReader2)
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim intRetVal As Integer
            Dim bDuplicateRec As Boolean = False
            Dim strMsg As String

            ''Start: Check Duplicate Record
            bDuplicateRec = clsUserGroup.fnUsrGrpDuplicateUsrGrp(0, txtGroupName.Text, "I")
            ''End  : Check Duplicate Record

            ''Start: Insert Record
            If bDuplicateRec = False Then
                'Start: Added Records
                intRetVal = clsUserGroup.fnUsrGrpInsertUpdateDelete( _
                                        0, txtGroupName.Text, rdAdmin.SelectedValue, _
                                        Request.Form("cb_moduleName"), _
                                        "", Session("UsrID"), "I")
                'End  : Added Records


                ''Start: Message Notification
                If intRetVal > 0 Then
                    strMsg = "New User Group Added Successfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "New User Group created : " + txtGroupName.Text)
                Else
                    strMsg = "New User Group Added Failed."
                End If
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>alert('" + strMsg + "');</script>"
                Response.Write(strJavaScript)
                Server.Transfer("usrGrp_View.aspx")
                ''End  : Message Notification
            Else
                lblErrorMessage.Text = "User Group Name already exist. Please choose another User Group Name."
                lblErrorMessage.Visible = True
            End If
            ''End  : Insert Record
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        gStrModule = ""
        txtGroupName.Text = ""
        rdAdmin.SelectedValue = "N"
        lblErrorMessage.Visible = False
        lblErrorMessage.Text = ""
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Response.Redirect("usrGrp_View.aspx")
    End Sub

    Public Sub DisplayModuleForFTS()
        Dim myReader As SqlDataReader
        Dim myReader1 As SqlDataReader
        Dim myReader2 As SqlDataReader
        Try
            Dim strOutput, strCbVal As String
            Dim intCount As Integer

            ''***Start: 1. Get Module Name 
            myReader = clsUserGroup.fnUsrGrpGetModuleForFTSFunction("", "", "M")
            While myReader.Read

                strOutput += "<TR  class='TRTitleBG'><TD width=100% valign=middle colspan=5><b>" & myReader("FLD_ModuleName") & "</b></TD></TR><TR>"

                ''***Start: 2. Get Sub Module
                myReader1 = clsUserGroup.fnUsrGrpGetModuleForFTSFunction(myReader("fld_moduleValue"), "", "S")
                While myReader1.Read
                    intCount = 1
                    strOutput += "<TR><TD width='20%' align=left><FONT color=#666666 >" & myReader1("fld_subModuleName") & "</font></TD>"

                    ''***Start: 3. Get Function
                    myReader2 = clsUserGroup.fnUsrGrpGetModuleForFTSFunction(myReader("fld_moduleValue"), myReader1("fld_subModuleValue"), "F")
                    While myReader2.Read
                        strCbVal = myReader("fld_ModuleValue") & "|" & myReader1("fld_subModuleValue") & "|" & myReader2("fld_functionValue")

                        If intCount > 4 Then
                            strOutput += "</TR><TR><TD width=20% valign=middle>&nbsp;</TD>"
                            intCount = 1
                        End If

                        strOutput += "<td width='20%' align=left><input style='Border:none;'"
                        If Trim(gStrModule) <> "" And (InStr(Trim(gStrModule), strCbVal) > 0) Then strOutput += " checked "
                        strOutput += " type='checkbox' id=cb_module name=cb_moduleName value='" & strCbVal & "' runat='server'>" _
                                          & myReader2("fld_functionName") & "</td>"
                        intCount = intCount + 1
                    End While
                    myReader2.Close()

                    strOutput += "</TR><TR><TD width=20% valign=middle>&nbsp;</TD><TD colspan=4><HR></TD></TR>"
                    ''***End:  3. Get Function

                End While
                myReader1.Close()

                strOutput += "</TR><TR><TD colspan=5><BR></TD></TR>"
                ''***End:  2. Get Sub Module

            End While
            myReader.Close()
            ''***End : 1. Get Module Name 

            Response.Write(strOutput)
        Catch ex As Exception
            clsCommon.fnDataReader_Close(myReader)
            clsCommon.fnDataReader_Close(myReader1)
            clsCommon.fnDataReader_Close(myReader2)
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
            Throw ex
        End Try
    End Sub
End Class
