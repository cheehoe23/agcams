#Region "Information Section"
' ****************************************************************************************************
' Description       : Add User 
' Purpose           : Add User Information
' Author            : See Siew
' Date              : 25/01/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.Web.Services

#End Region

Partial Class usr_Add
    Inherits System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label
    Protected WithEvents ddlStatus As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            Me.Header.DataBind()

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")

                ''Populate Control
                fnPopulateCtrl()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            Dim dsUser As New DataSet()
            'dsUser = fnGetUserList(cbUser.List.PageSize, "", "")
            dsUser = ClsUser.fnUsrGetUsrFromADForDropDownList("AU", 10, "", "", "")
            If dsUser.Tables(0).Rows.Count > 0 Then
                ''Get User Name
                'cbUser.DataSource = dsUser
                'cbUser.DataBind()
            Else
                cbUser.Enabled = False
                ddlUserGroup.Enabled = False
                txtRemarks.Enabled = False
                butSubmit.Enabled = False
                butReset.Enabled = False
                lblErrorMessage.Text = "Not user for adding"
                lblErrorMessage.Visible = True
            End If


            ''Get User Group
            fnPopulateDropDownList(clsUserGroup.fnUsrGrpGetAllUsrGrpRdr(), ddlUserGroup, "fld_GroupID", "fld_groupName", False)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("AU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Server.Transfer("usr_Add.aspx")
    End Sub

    Private Sub ButCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButCancel.Click
        Response.Redirect("usr_View.aspx")
    End Sub

    Private Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim bDupLoginID As Boolean = False
            Dim intRetVal As Integer = 0
            Dim strMsg As String = ""

            Dim strUsrName As String
            Dim strLoginID As String
            Dim intUsrGrpID As Integer = 0
            Dim strRemarks As String = ""
            'Dim strOwner As String = ""

            ''** Get Fields value
            'strUsrName = cbUser.SelectedRowValues(1) + " (" + cbUser.SelectedRowValues(2) + ")"
            'strLoginID = cbUser.SelectedRowValues(0)
            strUsrName = cbUser.Text
            'strLoginID = hfUser.Value

            If Trim(cbUser.Text) <> "" Then
                'strOwner = cbOwner.SelectedRowValues(0)
                Dim strUsrExistF As String = "N"
                ClsUser.fnUsr_CheckUserExistADFORDDL(Trim(cbUser.Text), strLoginID, strUsrExistF)
                If strUsrExistF = "N" Then
                    lblErrorMessage.Text = "User not found. Please select an existing User."
                    lblErrorMessage.Visible = True
                    cbUser.Focus()
                    Exit Sub
                End If
            End If

            If Trim(ddlUserGroup.SelectedIndex) <= 0 Then
                lblErrorMessage.Text = "Please choose one of User Group!"
                lblErrorMessage.Visible = True
                ddlUserGroup.Focus()
                Exit Sub
            End If

            intUsrGrpID = ddlUserGroup.SelectedValue
            strRemarks = txtRemarks.Text

            ''** Check for Duplicate LoginID
            bDupLoginID = ClsUser.fnUsrDuplicateLoginID(strLoginID)

            If Not bDupLoginID Then    ''Not Duplicate Records
                ''Insert Record
                intRetVal = ClsUser.fnUsrInsertUpdateDelete( _
                                    "", strLoginID, intUsrGrpID, strRemarks, "0", Session("UsrID"), "I")

                If intRetVal > 0 Then
                    strMsg = "New User Added Successfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "New User created : " + strUsrName)

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" & strMsg & "');" & _
                                    "document.location.href='usr_View.aspx';" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    strMsg = "New User Added Failed."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If

            Else ''Duplicate Records
                lblErrorMessage.Text = "User already exist. Please choose another user."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    <WebMethod()>
    Public Shared Function GetUsersList(prefix As String) As String()
        Dim dt As DataTable = New DataTable()

        Dim items As List(Of String)
        dt = ClsUser.fnUsrGetUsrFromADForDropDownList("AU", 10, prefix, "", "").Tables(0)
        Try
            items = New List(Of String)(dt.Rows.Count)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    items.Add(String.Format("{0}~{1}", row("ADUsrName"), row("fld_ADUsrName")))
                Next row
            End If
            Return items.ToArray()
        Catch ex As Exception
            Return New String() {}
        End Try

    End Function

End Class
