<%@ Page Language="vb" AutoEventWireup="false" Codebehind="usrGrp_Edit.aspx.vb" Inherits="AMS.usrGrp_Edit" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>usrGrp_Edit</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
		<script language="javascript">
		function checkAll() {
			for (var i=0;i<document.Frm_usrGrpEdit.elements.length;i++) {
				var e = document.Frm_usrGrpEdit.elements[i];
				if (e.name == 'cb_moduleName')
					e.checked = true;
				}
				
			return false;
		}

		function uncheckAll() {
			for (var i=0;i<document.Frm_usrGrpEdit.elements.length;i++) {
				var e = document.Frm_usrGrpEdit.elements[i];
				if (e.name == 'cb_moduleName')
					e.checked = false;
				}
				
			return false;
		}
		
		function isFieldBlank(theField) {
			if (theField.value.length == 0)
				return true;
			else
				return false;
		}

		function chkFrm() {
			var foundError = false;
			var chk = false;
			
			if (!foundError && isFieldBlank(document.Frm_usrGrpEdit.txtGroupName)) {
				foundError=true
				document.Frm_usrGrpEdit.txtGroupName.focus()
				alert("Please enter User Group Name.")
			}
			
			
			for (i=0; i < document.Frm_usrGrpEdit.length; i++) {
				if (!foundError && document.Frm_usrGrpEdit.elements[i].checked && document.Frm_usrGrpEdit.elements[i].name=='cb_moduleName') {
					chk = true;
				}
			}

			if (!foundError && chk==false) {
				foundError=true
				alert("Please select at least one Access Rights.")
			}

			if (!foundError){
				var flag = false;
 				flag = window.confirm("Are you sure you want to update this user group?");
 				return flag;}
			else{
				//alert("found error")
				return false;}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Frm_usrGrpEdit" method="post" runat="server">
			<!-- Start: header -->
			<table id="TblHeader" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="TblHeader2" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>User Management : Edit User Group</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table id="TblContent" height="400" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start Main Content-->
						<table height="400" cellSpacing="1" cellPadding="0" width="100%" align="left" bgColor="silver"
							border="0">
							<tr>
								<td>
									<table height="400" cellSpacing="0" cellPadding="0" width="100%" align="left" bgColor="white"
										border="0">
										<tr>
											<td>
												<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
													<TR>
														<TD vAlign="top" align="center" width="100%"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE height="126" cellSpacing="0" cellPadding="3" width="100%" align="left" bgColor="white"
																border="0">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><B><FONT color="red">*</FONT> denotes mandatory field</B></DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" height="352">
																			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																				<TBODY>
																					<TR>
																						<TD vAlign=top width="35%" align=left><FONT color="#666666"><font color="red">*</font>User 
																								Group Name : </FONT>
																						</TD>
																						<TD width="65%"><asp:textbox id="txtGroupName" Runat="server" maxlength="100" Width="300px"></asp:textbox>
																							<br /><FONT color="#666666" size="1">(Maximum 100 characters)</FONT></FONT>
																						</TD>
																					</TR>
																					<TR>
																						<TD valign="middle" width="35%">
																						    <FONT class="DisplayTitle"><font color="red">*</font>Admin : </FONT>
																						</TD>
																						<TD width="65%">
																							<asp:radiobuttonlist id="rdAdmin" Runat="server" RepeatDirection="Horizontal">
																							    <asp:ListItem Value="Y">Yes</asp:ListItem>
																							    <asp:ListItem Value="N" Selected=True>No</asp:ListItem>
																							</asp:radiobuttonlist>
																						</TD>
																					</TR>
																					<TR>
																						<TD colSpan="2">
																							<TABLE cellSpacing="1" cellPadding="0" width="100%" align="left" bgColor="silver" border="0">
																								<tr>
																									<td bgColor="white"><FONT color="#0000CC"><strong>Module Access Rights For Asset Management System</strong></FONT></td>
																								</tr>
																								<tr>
																									<td>
																										<TABLE cellSpacing="1" cellPadding="0" width="100%" bgColor="white" border="0">
																											<!-- Start: Display Modules-->
																											<%DisplayModule%>
																											<!-- End  : Display Modules-->
                                                                                                        </TABLE>
																									</td>
																								</tr>
																							</TABLE>
																						</TD>                                                                                        
																					</TR>
                                                                                    <tr>
                                                                                    <TD colSpan="2">
																							<TABLE cellSpacing="1" cellPadding="0" width="100%" align="left" bgColor="silver" border="0">
																								<tr>
																									<td bgColor="white"><FONT color="#0000CC"><strong>Module Access Rights For File Tracking Module</strong></FONT></td>
																								</tr>
																								<tr>
																									<td>
																										<TABLE cellSpacing="1" cellPadding="0" width="100%" bgColor="white" border="0">
																											<!-- Start: Display Modules-->
																											<%DisplayModuleForFTS%>
																											<!-- End  : Display Modules-->
                                                                                                        </TABLE>
																									</td>
																								</tr>
																							</TABLE>
																						</TD>
                                                                                    </tr>
																					<TR>
																						<TD align="center" colSpan="2"><BR>
																							<asp:button id="butSubmit" Runat="server" Text="Submit"></asp:button><asp:button id="butReset" Runat="server" Text="Reset"></asp:button><asp:button id="butCheckAll" Runat="server" Text="Check All"></asp:button><asp:button id="butUnCheckAll" Runat="server" Text="UnCheck All"></asp:button><asp:button id="butCancel" Runat="server" Text="Cancel"></asp:button></TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																			<BR>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End   : Content Part -->
			<!-- Start: Hidden Field --><input id="hdnGrpID" type="hidden" runat="server"> 
			<!-- End  : Hidden Field -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
		</form>
	</body>
</HTML>
