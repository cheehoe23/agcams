#Region "Information Section"
' ****************************************************************************************************
' Description       : Edit User 
' Purpose           : Edit User Information
' Author            : See Siew
' Date              : 29/01/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class usr_Edit
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label
    Protected WithEvents ddlStatus As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butSubmit.Attributes.Add("OnClick", "return chkFrm()")
                hdnUsrID.Text = Request("UID")

                ''Populate Control
                fnPopulateCtrl()

                ''Retrieve Records 
                fnGetRecordsFromDB()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), "Y"), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False)

            ''Get User Group
            fnPopulateDropDownList(clsUserGroup.fnUsrGrpGetAllUsrGrpRdr(), ddlUserGroup, "fld_GroupID", "fld_groupName", False)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnGetRecordsFromDB()
        Dim objRdr As SqlDataReader
        Try

            objRdr = ClsUser.fnUsrGetUsrForEdit(hdnUsrID.Text)
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    objRdr.Read()
                    lblUserName.Text = CStr(objRdr("fld_usrName"))
                    txtEmail.Text = CStr(objRdr("fld_Email"))
                    ddlDepartment.SelectedValue = CStr(objRdr("fld_DeptID"))
                    ddlUserGroup.SelectedValue = objRdr("fld_UserGroup")
                    txtRemarks.Text = CStr(objRdr("fld_Remarks"))

                End If
            End If
            objRdr.Close()
        Catch ex As Exception
            clsCommon.fnDataReader_Close(objRdr)
            Throw ex
        End Try
    End Sub

    Private Sub butReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReset.Click
        Try
            ''Set Default 
            ddlUserGroup.SelectedValue = "-1"
            txtRemarks.Text = ""

            ''Retrieve Records 
            fnGetRecordsFromDB()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub ButCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButCancel.Click
        Response.Redirect("usr_View.aspx")
    End Sub

    Private Sub butSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSubmit.Click
        Try
            Dim intRetVal As Integer = 0
            Dim strMsg As String = ""

            Dim strName As String = ""
            Dim intUsrGrpID As Integer = 0
            Dim strRemarks As String = ""


            ''** Get Fields value
            strName = lblUserName.Text
            intUsrGrpID = ddlUserGroup.SelectedValue
            strRemarks = txtRemarks.Text

            ''** Insert Record
            intRetVal = ClsUser.fnUsrInsertUpdateDelete( _
                                hdnUsrID.Text, "", intUsrGrpID, strRemarks, ddlDepartment.SelectedValue, Session("UsrID"), "E")
            If intRetVal > 0 Then
                strMsg = "User update Successfully."

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Updated User : " + strName)

                ''Message Notification
                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "alert('" + strMsg + "');" & _
                                "document.location.href='usr_View.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
            Else
                strMsg = "User Updated Failed."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class
