Partial Class usr_View
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butDelUsr.Attributes.Add("OnClick", "return ConfirmDelete()")

                ''Checking For Assign Right
                If (InStr(Session("AR"), "UsrMng|user|add") = 0) Then butAddUsr.Visible = False
                If (InStr(Session("AR"), "UsrMng|user|delete") = 0) Then butDelUsr.Visible = False

                ''default Sorting
                hdnSortName.Value = "fld_usrName"
                hdnSortAD.Value = "ASC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Checking whether user not have access right to deleted user
            If (InStr(Session("AR"), "UsrMng|user|delete") = 0) Then dgUsr.Columns(6).Visible = False

            ''' Sort Header Display
            fnSortHeaderDisplay()

            ''Get User Group Records
            dgUsr.DataSource = ClsUser.fnUsrGetAllUsr(hdnSortName.Value, hdnSortAD.Value)
            dgUsr.DataBind()
            If Not dgUsr.Items.Count > 0 Then '''Not User Group Records found
                'dgUsr.Visible = False
                ddlPageSize.Enabled = False
                butDelUsr.Visible = False
                lblErrorMessage.Text = "There is no user record."
                lblErrorMessage.Visible = True
            Else
                'dgUsr.Visible = True
                ddlPageSize.Enabled = True
                If (InStr(Session("AR"), "UsrMng|user|delete") > 0) Then butDelUsr.Visible = True
                lblErrorMessage.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function fnShowEditHyperlink(ByVal strUsrID As String, ByVal strUsrName As String) As String
        Dim strRetVal As String = ""
        If (InStr(Session("AR"), "UsrMng|user|ViewAudit") > 0) Then  '' Check For Audit Access Right
            strRetVal = "<a id=hlUsrViewAudit href='usr_ViewAudit.aspx?UID=" + strUsrID + "&UN=" + strUsrName + "' target=_self >" & _
                        "<img id=imgAudit src=../images/audit.gif alt='View Audit' border=0 >" & _
                        "</a>"
        End If

        If (InStr(Session("AR"), "UsrMng|user|edit") > 0) Then  '' Check For Edit User Access Right
            strRetVal += "<a id=hlEditUsr href='usr_Edit.aspx?UID=" + strUsrID + "' target=_self >" & _
                        "<img id=imgUpdate src=../images/update.gif alt='Click for Update User.' border=0 >" & _
                        "</a>"
        End If

        Return (strRetVal)
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By fld_usrName (2), fld_LoginID (3), GrpName (4)
        Dim intSortIndex As Integer
        Dim strSortHeader As String

        ''Set Default Header
        dgUsr.Columns(2).HeaderText = "Name"
        dgUsr.Columns(3).HeaderText = "Login ID"
        dgUsr.Columns(4).HeaderText = "User Group"


        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fld_usrName"
                intSortIndex = 2
                strSortHeader = "Name"
            Case "fld_LoginID"
                intSortIndex = 3
                strSortHeader = "Login ID"
            Case "GrpName"
                intSortIndex = 4
                strSortHeader = "User Group"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgUsr.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgUsr.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Private Sub dgUsr_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgUsr.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

    Private Sub dgUsr_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgUsr.PageIndexChanged
        dgUsr.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgUsr.CurrentPageIndex = 0
        dgUsr.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Private Sub dgUsr_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgUsr.ItemDataBound
        Dim cbDelete As CheckBox
        cbDelete = CType(e.Item.FindControl("DeleteThis"), CheckBox)
        If (InStr(Session("AR"), "UsrMng|user|delete") > 0) Then  '' Check For Delete User Access Right
            If Not cbDelete Is Nothing Then
                ''Case 1: Login User Cannot deleted their own account
                If Session("UsrID") = e.Item.Cells(0).Text Then
                    cbDelete.Enabled = False
                    cbDelete.Visible = False
                End If

                ''Case 2: Administrator Cannot deleted 
                If e.Item.Cells(0).Text = "1" Then
                    cbDelete.Enabled = False
                    cbDelete.Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub butAddUsr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAddUsr.Click
        Response.Redirect("usr_Add.aspx")
    End Sub

    Private Sub butDelUsr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelUsr.Click
        Try
            Dim strMsg As String
            Dim retval As Integer
            Dim strDelUsrName As String = ""

            ''*** Start: Get User that selected for deleted
            Dim GridItem As DataGridItem
            Dim chkSelectedDel As CheckBox
            Dim strUsrIDForDel As String
            For Each GridItem In dgUsr.Items
                chkSelectedDel = CType(GridItem.Cells(6).FindControl("DeleteThis"), CheckBox)
                If chkSelectedDel.Checked Then
                    strUsrIDForDel += GridItem.Cells(0).Text & "^"
                    strDelUsrName += GridItem.Cells(2).Text & ","
                End If
            Next
            If strDelUsrName <> "" Then strDelUsrName = strDelUsrName.Substring(0, strDelUsrName.Length - 1)
            ''*** End  : Get User that selected for deleted

            ''Start: Delete selected records from database
            retval = ClsUser.fnUsrInsertUpdateDelete(strUsrIDForDel, "", "0", "", "0", Session("UsrID"), "D")
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 Then
                strMsg = "Selected record(s) deleted successfully."
                fnPopulateRecords()

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Delete user(s) : " + strDelUsrName)
            Else
                strMsg = "Selected record(s) deleted failed."
            End If
            Dim strJavaScript As String
            strJavaScript = "<script language = 'Javascript'>alert('" + strMsg + "');</script>"
            Response.Write(strJavaScript)
            ''End  : Message Notification

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class
