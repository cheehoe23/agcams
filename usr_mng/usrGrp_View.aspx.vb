#Region "Information Section"
' ****************************************************************************************************
' Description       : View User Group
' Purpose           : View/Delete User Group
' Author            : See Siew
' Date              : 17/01/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class usrGrp_View
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents UsrName As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butDelUsrGrp.Attributes.Add("OnClick", "return fnCheckDelSelect()")

                ''Checking For Assign Right
                If (InStr(Session("AR"), "UsrMng|grp|add") = 0) Then butAddUsrGrp.Visible = False
                If (InStr(Session("AR"), "UsrMng|grp|delete") = 0) Then butDelUsrGrp.Visible = False

                ''Sorting
                hdnSortName.Value = "fld_groupName"
                hdnSortAD.Value = "ASC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
            Throw ex
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            If (InStr(Session("AR"), "UsrMng|grp|delete") = 0) Then dgUsrGrp.Columns(6).Visible = False
            If hdnSortAD.Value = "ASC" Then
                dgUsrGrp.Columns(3).HeaderText = "Group Name " + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
            Else
                dgUsrGrp.Columns(3).HeaderText = "Group Name " + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
            End If

            ''Get User Group Records
            dgUsrGrp.DataSource = clsUserGroup.fnUsrGrpGetAllUsrGrp(hdnSortName.Value, hdnSortAD.Value)
            dgUsrGrp.DataBind()
            If Not dgUsrGrp.Items.Count > 0 Then ''Not User Group Records found
                dgUsrGrp.Visible = False
                ddlPageSize.Enabled = False
                butDelUsrGrp.Visible = False
                lblErrorMessage.Text = "There is no user group record."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub butAddUsrGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAddUsrGrp.Click
        Response.Redirect("usrGrp_Add.aspx")
    End Sub

    Private Sub dgUsrGrp_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgUsrGrp.PageIndexChanged
        dgUsrGrp.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgUsrGrp.CurrentPageIndex = 0
        dgUsrGrp.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Private Sub butDelUsrGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelUsrGrp.Click
        Try
            Dim strMsg As String
            Dim retval As Integer
            Dim strDelUsrGroup As String = ""

            ''*** Start: Get User Group that selected for deleted
            Dim GridItem As DataGridItem
            Dim chkSelectedDel As CheckBox
            Dim strGrpIDForDel As String = ""
            For Each GridItem In dgUsrGrp.Items
                chkSelectedDel = CType(GridItem.Cells(6).FindControl("DeleteThis"), CheckBox)
                If chkSelectedDel.Checked Then
                    strGrpIDForDel += GridItem.Cells(0).Text & "^"
                    strDelUsrGroup += GridItem.Cells(4).Text & ","
                End If
            Next
            If strDelUsrGroup <> "" Then strDelUsrGroup = strDelUsrGroup.Substring(0, strDelUsrGroup.Length - 1)
            ''*** End  : Get User Group that selected for deleted

            'lblErrorMessage.Text = "record deleted = " & strGrpIDForDel
            'lblErrorMessage.Visible = True

            ''Start: Delete selected records from database
            retval = clsUserGroup.fnUsrGrpInsertUpdateDelete(0, "", "", "", strGrpIDForDel, Session("UsrID"), "D")
            ''End  : Delete selected records from database

            ''Start: Message Notification
            If retval > 0 Then
                strMsg = "Selected record(s) deleted successfully."
                fnPopulateRecords()

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Delete user Group(s): " + strDelUsrGroup)
            Else
                strMsg = "Selected record(s) deleted failed."
            End If
            Dim strJavaScript As String
            strJavaScript = "<script language = 'Javascript'>alert('" + strMsg + "');</script>"
            Response.Write(strJavaScript)
            ''End  : Message Notification

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
            Throw ex
        End Try
    End Sub

    Private Sub dgUsrGrp_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgUsrGrp.ItemDataBound
        Dim cbDelete As CheckBox
        cbDelete = CType(e.Item.FindControl("DeleteThis"), CheckBox)
        If (InStr(Session("AR"), "UsrMng|grp|delete") > 0) Then  '' Check For Delete User Group Access Right
            If Not cbDelete Is Nothing Then
                If e.Item.Cells(2).Text = "Y" Or e.Item.Cells(0).Text = "1" Then   ''If user group assign to active user, delete check box invisible
                    cbDelete.Enabled = False
                    cbDelete.Visible = False
                End If
            End If
        End If
    End Sub

    Function fnShowEditHyperlink(ByVal strUsrGrpID As String) As String
        Dim strRetVal As String
        If (InStr(Session("AR"), "UsrMng|grp|edit") > 0) Then 'And strUsrGrpID <> 1 Then  '' Check For Edit User Group Access Right
            '    If (InStr(Session("AR"), "UsrMng|grp|edit") > 0) Then
            strRetVal = "<a id=hlEditUsrGrp href='usrGrp_Edit.aspx?GID=" + strUsrGrpID + "' target=_self >" & _
                        "<img id=imgUpdate src=../images/update.gif alt='Click for Update User Group.' border=0 >" & _
                        "</a>"
        Else
            strRetVal = ""
        End If

        Return (strRetVal)
    End Function

    Private Sub dgUsrGrp_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgUsrGrp.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub
End Class
