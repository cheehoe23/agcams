<%@ Page Language="vb" AutoEventWireup="false" Codebehind="usr_Add.aspx.vb" Inherits="AMS.usr_Add" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<%@ Register TagPrefix="fluent" Namespace="Fluent.MultiLineTextBoxValidator" Assembly="Fluent.MultiLineTextBoxValidator" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
		<title>usr_Add</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">		
        <link href="../common/CommonStyle.css" rel="stylesheet" type="text/css" />
		<script language="JavaScript" src="../Common/CommonJS.js"></script>		
		<LINK href="../common/ComboBox/styles/contactsearch/combo.css" type="text/css" rel="Stylesheet">
        <script src="../JQuery/jquery-1.6.2.min.js" type="text/javascript"></script>
        <script src="../JQuery/jquery-ui.min.js" type="text/javascript"></script>  
        <link href="../jquery-ui-1.8.16/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet"
                type="text/css" />
	    
		<script language="javascript">

		    $(document).ready(function () {
		        SearchText("cbUser", "hfUser");
		    });
		    function SearchText(obj1, obj2) {
		        $("#" + obj1).autocomplete({
		            source: function (request, response) {
		                $.ajax({
		                    url: '<%#ResolveUrl("~/usr_mng/usr_Add.aspx/GetUsersList") %>',
		                    data: "{ 'prefix': '" + request.term + "'}",
		                    dataType: "json",
		                    type: "POST",
		                    contentType: "application/json; charset=utf-8",
		                    success: function (data) {
		                        response($.map(data.d, function (item) {
		                            return {
		                                label: item.split('~')[0],
		                                val: item.split('~')[1]
		                            }
		                        }))
		                    },
		                    error: function (response) {
		                        alert(response.responseText);
		                    },
		                    failure: function (response) {
		                        alert(response.responseText);
		                    }
		                });
		            },
		            select: function (e, i) {
		                $("#" + obj2).val(i.item.val);
		            },
		            minLength: 0
		        });

		    }

		function chkFrm() {
			var foundError = false;
			
				//validate for User 
				if (!foundError && gfnIsFieldBlank(document.Frm_usrAdd.cbUserSelectedValue0)) {
		            foundError=true
		            //document.Frm_usrAdd.cbUserSelectedValue0.focus()
		            alert("User not found. Please select an existing user.")
                }
                /*
				if (!foundError && document.Frm_usrAdd.ddlUserName.value == "-1") {
					foundError=true
					document.Frm_usrAdd.ddlUserName.focus()
					alert("Please select the User Name.")
				}*/
			
				//validate for User Group
				if (!foundError && document.Frm_usrAdd.ddlUserGroup.value == "-1") {
					foundError=true
					document.Frm_usrAdd.ddlUserGroup.focus()
					alert("Please select the User Group.")
				}
			
 			if (!foundError){
 				var flag = false;
 				flag = window.confirm("Are you sure you want to add this user?");
 				return flag;
 			}
			else
				return false;
		}
		</script>
	    <style type="text/css">
            .style1
            {
                width: 30%;
            }
        </style>
	</HEAD>
	<body>
		<form id="Frm_usrAdd" method="post" runat="server">
			<!-- Start: header -->
			<table bgcolor="white" align="center" width="100%" border="0" cellspacing="0" cellpadding="0"
				ID="Table8">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" ID="Table1">
							<tr>
								<td><B>User Management : Add New User</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<!-- Start : Content Part -->
			<table bgcolor="white" align="center" height="400" width="100%" border="0" cellspacing="0"
				cellpadding="0" ID="Table2">
				<tr height="12">
					<td valign="top"><img src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td valign="top"><img src="../images/rtop.jpg"></td>
				</tr>
				<tr height="100%">
					<td valign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td valign="top" width="100%">
						<!--Start: Main Content-->
						<table align="left" bgcolor="silver" height="400" width="100%" border="0" cellspacing="1"
							cellpadding="0" ID="Table3">
							<tr>
								<td>
									<table align="left" bgcolor="white" height="400" width="100%" border="0" cellspacing="0"
										cellpadding="0" ID="Table4">
										<tr>
											<td>
												<TABLE align="center" border="0" cellPadding="0" cellSpacing="0" width="100%" ID="Table5">
													<TR>
														<TD align="center" valign="top" width="100%">
															<asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label>
															<TABLE align="left" border="0" bgcolor="white" cellPadding="3" cellSpacing="0" height="126"
																width="100%" ID="Table6">
																<TBODY>
																	<TR>
																		<TD height="4">
																			<DIV align="left"><font color="red">*</font> denotes mandatory field</DIV>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="352" valign="top">
																			<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7">
																				<TBODY align=left>
																					
																					<TR>
																						<td colspan="2">
																							
																								<TABLE border="0" cellPadding="1" cellSpacing="1" width="100%" ID="Table7a">
																									<TR>
																										<TD class="style1"><FONT class="DisplayTitle"><font color="red">*</font>Name : </FONT></TD>
																										<TD width="65%">																										    
                                                                                                            
                                                                                             <asp:TextBox ID="cbUser" runat="server" Width="300px"></asp:TextBox>																	                    
                                                                                                            
                                                                                                        </TD>
																									</TR>
																									
																									<TR>
																										<TD class="style1"><FONT class="DisplayTitle"><font color="red">*</font>User Group : </FONT></TD>
																										<TD><asp:DropDownList ID="ddlUserGroup" Runat="server"></asp:DropDownList></TD>
																									</TR>
																									
																									<TR>
																										<TD valign=top class="style1"><FONT class="DisplayTitle">Remarks : </FONT></TD>
																										<TD>
																										    <asp:TextBox id="txtRemarks" maxlength="1000" TextMode="MultiLine" Columns="40" Rows="5" Runat="server"></asp:TextBox><br />
																					                        <asp:textbox id="txtRemarksWord" Width="50px" Runat="server"></asp:textbox> character(s) Left
																					                        <fluent:multilinetextboxvalidator id="MMLValRemarks" MaxLength="1000" Runat="server" ShowCharacterCount="True" EnableClientSideRestriction="True" ShowJavascriptAlert="True" ErrorMessage="Please Keep Your Remarks to 1000 Characters." OutputControl="txtRemarksWord" ControlToValidate="txtRemarks"></fluent:multilinetextboxvalidator>
																					                    </TD>
																									</TR>
																								</TABLE>
																							
																						</td>
																					</TR>
																					<TR>
																						<TD align="center" colspan="2"><BR>
																							<asp:Button id="butSubmit" Text="Submit" Runat="Server" />&nbsp;
																							<asp:Button id="butReset" Text="Reset" Runat="Server" />&nbsp;
																							<asp:Button id="ButCancel" Text="Cancel" Runat="Server" />
																						</TD>
																					</TR>
																				</TBODY>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2"></TD>
																	</TR>
																</TBODY>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--End: Main Content-->
					</td>
					<td background="../images/bgr.jpg" valign="top"></td>
				</tr>
				<tr height="14">
					<td valign="bottom"><img src="../images/lBottom.jpg"></td>
					<td background="../images/bgB.jpg" width="100%"></td>
					<td valign="top"><img src="../images/rbottom.jpg"></td>
                    <input type="hidden" id="hfUser" runat="server"> 
				</tr>
			</table>
			<!-- End   : Content Part -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
			
		</form>
	</body>
</HTML>
