Partial Class _default
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents hdnChgPwd As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            txtErrMsg.Text = ""
            txtErrMsg.Visible = False
            Me.btnSubmit.Attributes.Add("OnClick", "return chkFrm()")

            ''Get Window Login Id
            If Request("ShowDetail") = "True" Then
                divDetails.Visible = True
            Else
                divDetails.Visible = False

                Dim arrWin As Array
                If Not Request.ServerVariables("logon_user") Is Nothing Then
                    arrWin = Request.ServerVariables("logon_user").Split("\")

                    '****** For Showing Login Details *******

                    'Dim UsrID As String
                    'UsrID = Request("loginid")
                    'If UsrID = "" And arrWin(1).ToString() = "" Then
                    If (arrWin(1).ToString() <> "") Then
                        'Response.Redirect("login.asp")
                        fnCheckLogin(arrWin(1), "")
                    Else
                        ''Checking Login --> Window Login
                        Response.Redirect("login.aspx")
                        'fnCheckLogin(UsrID, "")
                    End If

                Else
                    txtErrMsg.Text = "Login Failed. Please kindly contact your administrator."
                    txtErrMsg.Visible = True
                End If

            End If

            'lb.Text = arrWin(0) + "_" + arrWin(1)
            ''Checking Login --> Window Login
            'fnCheckLogin(Environment.UserName, "")
            'fnCheckLogin(arrWin(1), "")

        Catch ex As Exception
            txtErrMsg.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            txtErrMsg.Visible = True
        End Try
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim strLoginID As String = txtUserId.Text
            Dim strPwd As String = ""

            ''Checking Login --> Fill in Login
            fnCheckLogin(strLoginID, strPwd)
        Catch ex As Exception
            txtErrMsg.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            txtErrMsg.Visible = True
        End Try

        'Session("Name") = "Administrator"   ''' Full Name of the User
        'Session("UsrID") = 1                ''' User Id in Database
        'Session("LoginID") = "Admin"        ''' Login User Id   'UsrMng|grp|delete,&nbsp;
        'Session("AR") = "AssetMng|AddAsset|view,AssetMng|Asset|view,AssetMng|Asset|edit,AssetMng|Asset|IssAsset,AssetMng|Asset|AssetTrans,AssetMng|Asset|printResult,AssetMng|Asset|printLabel,AssetMng|Asset|viewAudit,AssetMng|TempAsset|view,AssetMng|TempAsset|update,AssetMng|CondmCtrl|view,AssetMng|CondmCtrl|condemn,AssetMng|CondmCtrl|PendNotice,AssetMng|RedunCtrl|view,AssetMng|RedunCtrl|Redun,AssetMng|RedunCtrl|print,AssetMng|PendDisposed|view,AssetMng|PendDisposed|dispose,AssetMng|PendDisposed|print,AssetMng|UsrResign|view,AssetMng|UsrResign|update,AssetMng|FloatCtrl|view,AssetMng|FloatCtrl|export,AssetMng|FloatCtrl|print,AssetMng|DisAsset|view,AssetMng|DisAsset|print,AssetMng|delAsset|view,AssetMng|delAsset|del,AssetMng|delAsset|print,ContractMng|AddContract|view,ContractMng|Contact|view,ContractMng|Contact|edit,ContractMng|Contact|delete,offlineProcess|StockTakeHis|view,offlineProcess|StockTakeHis|uploadPDA,offlineProcess|StockTakeAct|view,Rpt|DymRpt|view,Rpt|DymRpt|exportExcel,Rpt|DymRpt|exportWord,Rpt|DymRpt|addRptTpl,Rpt|DymRpt|editRptTpl,Rpt|DymRpt|delRptTpl,UsrMng|user|view,UsrMng|user|add,UsrMng|user|edit,UsrMng|user|delete,UsrMng|user|ViewAudit,UsrMng|grp|view,UsrMng|grp|add,UsrMng|grp|edit,UsrMng|grp|delete,SysSet|genrl|view,SysSet|genrl|update,SysSet|dept|view,SysSet|dept|add,SysSet|dept|edit,SysSet|dept|delete,SysSet|loct|view,SysSet|loct|add,SysSet|loct|edit,SysSet|loct|delete,SysSet|catg|view,SysSet|catg|add,SysSet|catg|edit,SysSet|catg|delete"
        ''"AssetMng|AddAsset|view,AssetMng|Asset|view,AssetMng|Asset|edit,AssetMng|Asset|IssAsset,AssetMng|Asset|AssetTrans,AssetMng|Asset|printResult,AssetMng|Asset|printLabel,AssetMng|Asset|viewAudit,AssetMng|TempAsset|view,AssetMng|TempAsset|update,AssetMng|CondmCtrl|view,AssetMng|CondmCtrl|condemn,AssetMng|CondmCtrl|PendNotice,AssetMng|RedunCtrl|view,AssetMng|RedunCtrl|Redun,AssetMng|RedunCtrl|print,AssetMng|PendDisposed|view,AssetMng|PendDisposed|dispose,AssetMng|PendDisposed|print,AssetMng|UsrResign|view,AssetMng|UsrResign|update,AssetMng|FloatCtrl|view,AssetMng|FloatCtrl|export,AssetMng|FloatCtrl|print,AssetMng|delAsset|view,AssetMng|delAsset|del,AssetMng|delAsset|print,ContractMng|AddContract|view,ContractMng|Contact|view,ContractMng|Contact|edit,ContractMng|Contact|delete,offlineProcess|StockTakeHis|view,offlineProcess|StockTakeHis|uploadPDA,offlineProcess|StockTakeAct|view,Rpt|DymRpt|view,Rpt|DymRpt|exportExcel,Rpt|DymRpt|exportWord,Rpt|DymRpt|addRptTpl,Rpt|DymRpt|editRptTpl,Rpt|DymRpt|delRptTpl,UsrMng|user|view,UsrMng|user|add,UsrMng|user|edit,UsrMng|user|delete,UsrMng|user|ViewAudit,UsrMng|grp|view,UsrMng|grp|add,UsrMng|grp|edit,UsrMng|grp|delete,SysSet|genrl|view,SysSet|genrl|update,SysSet|dept|view,SysSet|dept|add,SysSet|dept|edit,SysSet|dept|delete,SysSet|loct|view,SysSet|loct|add,SysSet|loct|edit,SysSet|loct|delete,SysSet|catg|view,SysSet|catg|add,SysSet|catg|edit,SysSet|catg|delete" '"AssetMng|AddAsset|view,AssetMng|Asset|view,AssetMng|Asset|edit,AssetMng|Asset|IssAsset,AssetMng|Asset|AssetTrans,AssetMng|Asset|printResult,AssetMng|Asset|printLabel,AssetMng|Asset|viewAudit,AssetMng|TempAsset|view,AssetMng|TempAsset|update,AssetMng|CondmCtrl|view,AssetMng|CondmCtrl|condemn,AssetMng|RedunCtrl|view,AssetMng|RedunCtrl|Redun,AssetMng|PendDisposed|view,AssetMng|PendDisposed|arc,AssetMng|UsrResign|view,AssetMng|UsrResign|update,AssetMng|FloatCtrl|view,AssetMng|FloatCtrl|export,AssetMng|FloatCtrl|print,AssetMng|delAsset|view,AssetMng|delAsset|del,ContractMng|AddContract|view,ContractMng|Contact|view,ContractMng|Contact|edit,ContractMng|Contact|delete,offlineProcess|StockTake|view,offlineProcess|StockTake|uploadPDA,offlineProcess|MoveUpdate|view,Rpt|DymRpt|view,Rpt|DymRpt|exportExcel,Rpt|DymRpt|exportWord,UsrMng|user|view,UsrMng|user|add,UsrMng|user|edit,UsrMng|user|delete,UsrMng|user|ViewAudit,UsrMng|grp|view,UsrMng|grp|add,UsrMng|grp|edit,UsrMng|grp|delete,SysSet|genrl|view,SysSet|genrl|update,SysSet|dept|view,SysSet|dept|add,SysSet|dept|edit,SysSet|dept|delete,SysSet|loct|view,SysSet|loct|add,SysSet|loct|edit,SysSet|loct|delete,SysSet|catg|view,SysSet|catg|add,SysSet|catg|edit,SysSet|catg|delete" '"AssetMng|AddAsset|view,AssetMng|Asset|view,AssetMng|TempAsset|view,AssetMng|ArcAsset|view,AssetMng|ResAsset|view,AssetMng|UsrResign|view,AssetMng|CondmCtrl|view,AssetMng|FloatCtrl|view,ContractMng|AddContract|view,ContractMng|Contact|view,offlineProcess|StockTake|view,offlineProcess|StockTake|uploadPDA,offlineProcess|MoveUpdate|view,Rpt|DymRpt|view,UsrMng|user|view,UsrMng|user|add,UsrMng|user|edit,UsrMng|user|delete,UsrMng|user|ViewAudit,UsrMng|user|resetPwd,UsrMng|grp|view,UsrMng|grp|add,UsrMng|grp|edit,UsrMng|grp|delete,SysSet|genrl|view,SysSet|genrl|update,SysSet|dept|view,SysSet|dept|add,SysSet|dept|edit,SysSet|dept|delete,SysSet|loct|view,SysSet|loct|add,SysSet|loct|edit,SysSet|loct|delete,SysSet|catg|view,SysSet|catg|add,SysSet|catg|edit,SysSet|catg|delete"
        'Session("UEmail") = "seesiew@raptech.com.sg"
        'Response.Redirect("index.aspx")
    End Sub

    Protected Sub fnCheckLogin(ByVal strLoginID As String, _
                               ByVal strPwd As String)
        Try
            Dim intUsrId As Integer = "0"
            Dim strUsrName As String = ""
            Dim strAccess As String = ""
            Dim strLSuccessF As String = ""
            Dim strAdminF As String = ""
            Dim intLoginUsrID As Integer = "0"

            clsCommon.fnLoginValidation(strLoginID, strPwd, intUsrId, strUsrName, _
                                        strAccess, strLSuccessF, strAdminF, intLoginUsrID)
            If strLSuccessF = "Y" Then ''Login Successfully
                Session("Name") = strUsrName            '' Full Name of the User
                Session("UsrID") = intUsrId             '' User Id in Database
                Session("LoginID") = strLoginID         '' Login User Id   'UsrMng|grp|delete,&nbsp;
                Session("AR") = strAccess
                Session("AdminF") = strAdminF
                Session("LoginUsersID") = intLoginUsrID

                ''**** Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "User Login")

                Response.Redirect("index.aspx")
                'Server.Transfer("index.aspx")
            Else
                txtErrMsg.Text = "Login Failed. Please kindly contact your administrator."
                txtErrMsg.Visible = True
                Response.Redirect("login.aspx")
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        txtUserId.Text = ""
        txtPwd.Text = ""
        txtErrMsg.Text = ""
        txtErrMsg.Visible = False
    End Sub
End Class
