
Partial Class login
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Me.btnLogin.Attributes.Add("OnClick", "return chkFrm()")

            txt_userID.Focus()

            If (Convert.ToString(Request.QueryString("FTSLoginID")) <> "") Then
                'Dim strJavaScript As String = ""
                'strJavaScript = "<script language = 'Javascript'>" & _
                '                "parent.location.href='/common/logout.aspx';" & _
                '                "</script>"
                'Response.Write(strJavaScript)
                txt_userID.Text = Request("FTSLoginID")
                txt_password.Text = "password"
                btnLogin_Click(sender, e)
            End If

        End If
    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Try
            Dim strLoginID As String = txt_userID.Text
            Dim strPwd As String = txt_password.Text

            ''Checking Login --> Fill in Login
            If txt_password.Text = "a" Then

                fnCheckLogin(strLoginID, strPwd)
            Else
                Me.ltMessage.Text = "You have entered an invalid Login ID and/or Password"
                Me.divMessage.Visible = True
            End If
        Catch ex As Exception
            Me.ltMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            Me.divMessage.Visible = True
        End Try
    End Sub

    Protected Sub fnCheckLogin(ByVal strLoginID As String, _
                           ByVal strPwd As String)
        Try
            Dim intUsrId As Integer = "0"
            Dim strUsrName As String = ""
            Dim strAccess As String = ""
            Dim strLSuccessF As String = ""
            Dim strAdminF As String = ""
            Dim intLoginUsrID As Integer = "0"

            clsCommon.fnLoginValidation(strLoginID, strPwd, intUsrId, strUsrName, _
                                        strAccess, strLSuccessF, strAdminF, intLoginUsrID)
            If strLSuccessF = "Y" Then ''Login Successfully
                Session("Name") = strUsrName            '' Full Name of the User
                Session("UsrID") = intUsrId             '' User Id in Database
                Session("LoginID") = strLoginID         '' Login User Id   'UsrMng|grp|delete,&nbsp;
                Session("AR") = strAccess
                Session("AdminF") = strAdminF
                Session("LoginUsersID") = intLoginUsrID
                'Session("Status") = ""

                ''**** Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "User Login")

                Response.Redirect("index.aspx")
                'Server.Transfer("index.aspx")
            Else
                Me.ltMessage.Text = "You have entered an invalid Login ID and/or Password"
                Me.divMessage.Visible = True
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

End Class
