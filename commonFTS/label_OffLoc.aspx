<%@ Register TagPrefix="neobarcode" Namespace="Neodynamic.WebControls.BarcodeProfessional" Assembly="Neodynamic.WebControls.BarcodeProfessional" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="label_OffLoc.aspx.vb" Inherits="FTS.label_OffLoc"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>label_OffLoc</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="FTSstyle.css" type="text/css" rel="Stylesheet">
		<script type="text/javascript">   
         // Print the window            
         window.print();
         
         // Close the window
         //window.close();    
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="frm_LabelOffLoc" method="post" runat="server">
			<table width="100%" border="0">
				<tr>
					<td>
						<div id="divBarcode">
							<div style="PADDING-RIGHT: 5px; PADDING-LEFT: 10px; PADDING-BOTTOM: 5px; WIDTH: 380px; PADDING-TOP: 5px; TEXT-ALIGN: center">
								<table width="100%">
									<tr>
										<td><span style="FONT-SIZE: 12pt; FONT-FAMILY: Arial"><b><asp:label id="lblOffLocName" Runat="server"></asp:label></b></span></td>
									</tr>
									<tr>
										<td><neobarcode:barcodeprofessional id="barcodeCode" runat="server" Symbology="Code128" Font-Underline="False" Font-Strikeout="False"
												Font-Size="10pt" Font-Names="Arial" Font-Italic="False" Font-Bold="False" BarHeight="0.4"></neobarcode:barcodeprofessional></td>
									</tr>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
