#Region "Information Section"
' ****************************************************************************************************
' Description       : Menu 
' Purpose           : Display Menu
' Author            : See Siew
' Date              : 14/03/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports Microsoft.Web.UI.WebControls
#End Region

Partial Class FTSmenu
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                ''Polulate Menu
                PupolateMenu()
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub PupolateMenu()

        Dim drModule, drSModule As SqlDataReader

        Dim nodeMod, nodeSMod As TreeNode
        Try

            '' 1. Get Module 
            drModule = clsCommon.FTSfnMenuGetModuleFunction("", Session("AR"), "M")
            If Not drModule Is Nothing Then
                If drModule.HasRows Then
                    While drModule.Read  '' LOOP MODULE
                        ''Add Node for Module
                        nodeMod = New TreeNode
                        'nodeMod.Text = "&nbsp;<font class='navhead'>" + CStr(drModule("fld_moduleName")) + "</font>"
                        nodeMod.Text = "&nbsp;" + CStr(drModule("fld_moduleName"))
                        TvMenu.Nodes.Add(nodeMod)

                        ''2. Get Sub-Module that belong to particular Module
                        drSModule = clsCommon.FTSfnMenuGetModuleFunction(drModule("fld_moduleValue"), Session("AR"), "S")
                        If Not drSModule Is Nothing Then
                            If drSModule.HasRows Then
                                While drSModule.Read  '' LOOP SUB MODULE
                                    ''Add Node for particular Module
                                    nodeSMod = New TreeNode
                                    nodeSMod.Text = "&nbsp;<A href='" + CStr(drSModule("fld_functionFile")) + "' target=content>"
                                    nodeSMod.Text += CStr(drSModule("fld_subModuleName")) + "</A>"
                                    nodeSMod.ImageUrl = "../images/dot.gif"
                                    'nodeSMod.SelectedStyle.Item = "FONT-SIZE: 10pt; FONT-WEIGHT: bold; COLOR:#62902E"
                                    nodeMod.Nodes.Add(nodeSMod)

                                    ''For permanent add Change Password Module
                                    If drModule("fld_moduleValue") = "UsrMng" And drSModule("fld_subModuleValue") = "grp" Then
                                        ''Add Node for Change Password Module
                                        nodeSMod = New TreeNode
                                        nodeSMod.Text = "&nbsp;<A href='../usr_mng/chgPwd.aspx?callFrm=Menu' target=content>"
                                        nodeSMod.Text += "Change Password</A>"
                                        nodeSMod.ImageUrl = "../images/dot.gif"
                                        'nodeSMod.SelectedStyle.Item = "FONT-SIZE: 10pt; FONT-WEIGHT: bold; COLOR:#62902E"
                                        nodeMod.Nodes.Add(nodeSMod)
                                    End If
                                End While  '' LOOP SUB MODULE
                            End If
                        End If
                        drSModule.Close()
                    End While   '' LOOP MODULE
                    drModule.Close()
                End If
            End If

            ''3. Additional Module
            ''Add for Online Help
            'nodeMod = New TreeNode
            'nodeMod.Text = "&nbsp;<A href='../common/UserGuide.pdf' target=_blank>User Manual</A>"
            'nodeMod.ImageUrl = "../images/dot.gif"
            'TvMenu.Nodes.Add(nodeMod)

            ''Add for Logout
            nodeMod = New TreeNode

            nodeMod.Text = "&nbsp;<A href='/common/logout.aspx?CallFrm=logout' target=_parent>Logout</A>"
            nodeMod.ImageUrl = "../images/dot.gif"
            TvMenu.Nodes.Add(nodeMod)

            ''Set Default output for TreeView
            TvMenu.ShowLines = False
            TvMenu.CssClass = "tvMenu"
        Catch ex As Exception
            clsCommon.fnDataReader_Close(drModule)
            clsCommon.fnDataReader_Close(drSModule)
            Throw ex
        End Try
    End Sub
    Protected Sub lbtnAMS_Click(sender As Object, e As EventArgs) Handles lbtnAMS.Click
        Try
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='/common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If

            Dim strJavaScript1 As String = ""
            'strJavaScript1 = "<script language = 'Javascript'>" & _
            '                "parent.location.href='/login.aspx?FTSLoginID=" + Session("LoginID") + "';" & _
            '                "</script>"
            strJavaScript1 = "<script language = 'Javascript'>" & _
                            "parent.location.href='/index.aspx';" & _
                            "</script>"
            Response.Write(strJavaScript1)

        Catch ex As Exception
        End Try
    End Sub
End Class
