/*
'*******************************************************************************************************
'Purpose  			:  Global javascript functions.
'*******************************************************************************************************
*/
//function Call Print Label function -> File Label
function gfnprint(FTitle, FNo, FBarcode, FVol, FRef){
	//alert('FTitle=' + FTitle + ',FNo=' + FNo + ',FBarcode='+ FBarcode + ',FVol='+ FVol+',FRef='+FRef)

	var PassVal;
	//''*** Start : Changes by CSS, 17 Aug 2009
	//PassVal = "FRef=" + FRef + "&FVol=" + FVol + "&FTitle=" + FTitle  + "&FBarcode=" + FBarcode + "&FNo=" + FNo;
	PassVal = "FBarcode=" + FBarcode;
	//''*** End   : Changes by CSS, 17 Aug 2009
	window.open('../common/label_file.aspx?'+PassVal, 'Print_Screen','width=420,height=250,Top=0,left=0,scrollbars=no,menubar=no,location=no');
}

//function Call Print Label function -> Officer/Location Label
function gfnPrintOffLocLabel(OLName, OLBarcode, OLType){
	var PassVal;
	PassVal = "OLName=" + OLName + "&OLBarcode=" + OLBarcode + "&OLType=" + OLType;
	window.open('../common/label_OffLoc.aspx?'+PassVal, 'Print_Screen','width=420,height=250,Top=0,left=0,scrollbars=no,menubar=no,location=no');
}


/*Start: **************************************************
Function For Date Validation (Create on: 15/07/2007)
************************************************************/
//USE TO CHECK IF DATE IS VALID
//PARAMETER 1: Date Control Name
//PARAMETER 2: Control Caption
//PARAMETER 3: 'O' if Date is a Optional Field, 'M' if Date is Mandatory
function gfnCheckDate(pobjControl, pstrCaption, pstrOptionalFlag)
{
	var ldtmValidDate;
	var lstrDateValue = pobjControl.value;

	// If the Date is optional and the user leaves it blank then 
	// let it be so and do not proceed with validating the date 
	if ((pstrOptionalFlag == 'O') && (lstrDateValue == ''))
	{
		return true; //Exit if Optional
	}
	if ((pstrOptionalFlag == 'M') && (lstrDateValue == ''))
	{
		alert(pstrCaption + " cannot be left Blank, please enter a valid date.");
		pobjControl.focus();
		return false;
	}

	if (gfnCheckNumeric(pobjControl, "/"))
	{   
	    alert("Invalid " + pstrCaption + ", " + pstrCaption + " cannot contain special characters");
		pobjControl.focus();
		return false;
	}
	if (lstrDateValue.length != 10)
	{
		alert("Invalid Date Format, Please specify date in DD/MM/YYYY format");
		pobjControl.focus();
		return false;
	}
	ldtmValidDate = gfnConvertToDate(lstrDateValue, pstrCaption);

	if (ldtmValidDate == false)
	{
		pobjControl.focus();
		return false;
	}
	else
	{
		return ldtmValidDate;
	}
}

function gfnCheckDate1(pobjControl, pstrCaption, pstrOptionalFlag)
{
	var ldtmValidDate;
	var lstrDateValue = pobjControl.value;

	// If the Date is optional and the user leaves it blank then 
	// let it be so and do not proceed with validating the date 
	if ((pstrOptionalFlag == 'O') && (lstrDateValue == ''))
	{
		return true; //Exit if Optional
	}
	if ((pstrOptionalFlag == 'M') && (lstrDateValue == ''))
	{
		alert(pstrCaption + " cannot be left Blank, please enter a valid date.");
		pobjControl.focus();
		return false;
	}

	if (gfnCheckNumeric(pobjControl, "/"))
	{   
	    alert("Date cannot contain special characters");
		pobjControl.focus();
		return false;
	}
	if (lstrDateValue.length != 10)
	{
		alert("Invalid Date Format, Please specify date in DD/MM/YYYY format");
		pobjControl.focus();
		return false;
	}
	ldtmValidDate = gfnConvertToDate(lstrDateValue, pstrCaption);

	if (ldtmValidDate == false)
	{
		pobjControl.focus();
		return false;
	}
	else
	{
		return ldtmValidDate;
	}
}


//USE TO VALIDATE AND THEN CONVERT A STRING OF DD/MM/YYYY FORMAT TO A VALID JS DATE
//Converts String To Date
function gfnConvertToDate(lstrDateValue, pstrCaption)
{
	var ldtmBaseDate;
	ldtmBaseDate=new Date(1900, 00, 01);
	// Checks for the following valid date formats:
	// DD/MM/YYYY
	// Also separates date into month, day, and year variables

	var lstrDatePattern = /^(\d{1,2})(\/)(\d{1,2})\2(\d{4})$/;
	var lstrMatchArray = lstrDateValue.match(lstrDatePattern);//CHECK FORMAT
	if (lstrMatchArray == null) 
	{
		alert("Invalid Date Format, Please specify date in DD/MM/YYYY format");
		return false;
	}

	// parse date into variables
	var day = lstrMatchArray[1];
	var month = lstrMatchArray[3];
	var year = lstrMatchArray[4];

	if (month < 1 || month > 12) // check month range
	{
		alert("Month must be between 1 and 12");
		return false;
	}
	if (day < 1 || day > 31)
	{
		alert("Day must be between 1 and 31");
		return false;
	}
	if ((month==4 || month==6 || month==9 || month==11) && day == 31)
	{
		alert("Month " + month + " does not have 31 days!");
		return false;
	}
	if (month == 2) // check for february 29th
	{
		var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
		if (day>29 || (day==29 && !isleap))
		{
			alert("February " + year + " does not have " + day + " days!"); 
			return false;
		}
	}
	var ldtmCheckDate=new Date(year, month-1, day);
	if (gfnRetDateDiff(ldtmCheckDate, ldtmBaseDate)<0)
	{
		alert("Date cannot be less than Base Date."); 
		return false;
	}
	return ldtmCheckDate; //Sucess
}

//DATE CHECK FUNCTIONS
//USE TO GET DATE DIFFERENCE IN NO OF DAYS
//PARAMETER 1: Date No 1 (Date Type)
//PARAMETER 2: Date No 2 (Date Type)
function gfnRetDateDiff(pdtmDate1, pdtmDate2)
{
	return ((pdtmDate1 - pdtmDate2)/1000/60/60/24);
}
/*End: **************************************************
Function For Date Validation (Create on: 15/07/2007)
************************************************************/

//function for Trim the string
function gfnTrimAll(sString) {
	while (sString.substring(0,1) == ' ')
	{
		sString = sString.substring(1, sString.length);
	}
	while (sString.substring(sString.length-1, sString.length) == ' ')
	{
		sString = sString.substring(0,sString.length-1);
	}
	return sString;
}

//function gfnCheckDelSelect()
function gfnCheckSelect(strErrorMsg)
{
	var lstrStatus;
	var frm = document.forms[0];			
	lstrStatus='N';
	for (i=0; i<frm.length; i++) 
	{			
		if (frm.elements[i].id.indexOf('DeleteThis') != -1 && frm.elements[i].checked == true) 
		{					
				lstrStatus='Y';
		}						
	}	
	if (lstrStatus=='N')
		{
			alert(strErrorMsg);
			return false;
		}			
	else
		{
			return true;
		}
}


//To retrieve Combo/List/Option/Check Boxes values
//PARAMETER 1: Control Name From Which the value need to be retrieved
//PARAMETER 2: Control Type Legends as used by this function to identify the control type
//RETURNS : The Values of the Passed Control, if more than 1 is Selected then these are | Seperated
function gfnGetSelectedValue()
{
	var lintICtr;
	var lstMyValues = ""; //Used for List/Check Box

	var lobjCntrlName = arguments[0];
	var lstrCntrlType = arguments[1];

	switch(lstrCntrlType)
	{
		case "C"://Combo Box
		case "L"://List Box
			if (lobjCntrlName.selected)
			{
				lstMyValues = lobjCntrlName.value;
			}
			else
			{
               			for(lintICtr=0;lintICtr < lobjCntrlName.length;lintICtr++)
               			{
               				if (lobjCntrlName[lintICtr].selected)
                				lstMyValues = lstMyValues + lobjCntrlName[lintICtr].value + "|";
               			}
        			lstMyValues = (lstMyValues == 'undefined')? "":lstMyValues.substr(0,lstMyValues.length-1);
			}
			return lstMyValues;
			break;
		case "O"://Option Button
			if (lobjCntrlName.checked)
			{
				lstMyValues = lobjCntrlName.value;
			}
			else
			{
               			for(lintICtr=0;lintICtr < lobjCntrlName.length;lintICtr++)
               			{
               				if (lobjCntrlName[lintICtr].checked)
                				lstMyValues = lobjCntrlName[lintICtr].value;
               			}
        			lstMyValues = (lstMyValues == 'undefined')? "":lstMyValues;
			}
			return lstMyValues;
			break;
		case "H"://Check Box
			if (lobjCntrlName.checked)
			{
				lstMyValues = lobjCntrlName.value;
			}
			else
			{
        			for(lintICtr=0;lintICtr < lobjCntrlName.length;lintICtr++)
        			{
        				if (lobjCntrlName[lintICtr].checked)
        					lstMyValues = lstMyValues + lobjCntrlName[lintICtr].value + "|";
               			}
        			lstMyValues = (lstMyValues == 'undefined')? "":lstMyValues.substr(0,lstMyValues.length-1);
			}
			return lstMyValues;
			break;
	}
}


/*To check that the Start Date and End Date must be 
	- greater then (>) or 
	- equal (>=) */
function gfnCheckFrmToDt(pobjControl1,pobjControl2,pstrEqualGreater)
{
	
	var frcharday, frcharmonth, frcharyear;
	var tocharday, tocharmonth, tocharyear;
	var frday, frmonth, fryear;
	var today, tomonth, toyear;
	
	frcharday 	= pobjControl1.value.charAt(0)+pobjControl1.value.charAt(1);
	frcharmonth = pobjControl1.value.charAt(3)+pobjControl1.value.charAt(4);
	frcharyear 	= pobjControl1.value.charAt(6)+ pobjControl1.value.charAt(7) + pobjControl1.value.charAt(8)+pobjControl1.value.charAt(9);
	tocharday 	= pobjControl2.value.charAt(0)+pobjControl2.value.charAt(1);
	tocharmonth = pobjControl2.value.charAt(3)+pobjControl2.value.charAt(4);
	tocharyear 	= pobjControl2.value.charAt(6)+ pobjControl2.value.charAt(7) + pobjControl2.value.charAt(8)+pobjControl2.value.charAt(9);
	
	frday 		= parseInt(frcharday,10);
	frmonth 	= parseInt(frcharmonth,10);
	fryear 		= parseInt(frcharyear,10);
	today 		= parseInt(tocharday,10);
	tomonth 	= parseInt(tocharmonth,10);
	toyear 		= parseInt(tocharyear,10);
	
	/*		alert("after get value -> From Date");
			alert(frcharday);
			alert(frcharmonth);
			alert(frcharyear);
			alert("after get value -> To Date");
			alert(tocharday);
			alert(tocharmonth);
			alert(tocharyear); */
	
	if (fryear > toyear)
	{	
		return false;
	}
	if (toyear == fryear)
	{	 
		if (frmonth > tomonth)
		{
			return false;
		}	
		if  (frmonth == tomonth)
		{		
			if (pstrEqualGreater == '>'){
		        if (frday >= today)
		        {		
					return false;
		        } 
			}
			else{
				if (frday > today)
		        {		
					return false;
		        } 
			}
		}    
	}
}

/*NUMERIC CHECK FUNCTIONS
//->USE TO ALLOW NUMERICS ONLY
//->PARAMETER 1: Numeric Control Name
//->PARAMETER 2: Special Characters to be allowed
//->PROPOSED FUTURE SIGNATURE: function gfnCheckNumeric(pobjControl, pstrAllow)*/
function gfnCheckNumeric(pobjControl, pstrAllow)
{
	//var lintNumValue = gfnRemoveComma(pobjControl.value);
	var lintNumValue = pobjControl.value;

	var lstrPattern= new RegExp("[0-9," + pstrAllow + "]", "g");
	var lstrRetArr=lintNumValue.match(lstrPattern);
	
	if (lstrRetArr==null || lstrRetArr.length!=lintNumValue.length)
	{
		// error is due to some other character		
		return true;
	}
	else
	{	
		return false;	
	}
}

/*Using modified select_deselectAll script function of my original one, from Developerfusion.com forum members - ketcapli & thombo Forum Post - [http://www.developerfusion.co.uk/forums/topic-22773]*/
function gfnSelect_deselectAll (chkVal, idVal) 
{
	var frm = document.forms[0];
	if (idVal.indexOf('DeleteThis') != -1 && chkVal == true)
	{
			var AllAreSelected = true;
			for (i=0; i<frm.length; i++) 
			{
				if (frm.elements[i].id.indexOf('DeleteThis') != -1 && frm.elements[i].checked == false)
				{ 
					AllAreSelected = false;
					break;
				} 
			} 
			if(AllAreSelected == true)
			{
				for (j=0; j<frm.length; j++) 
				{
					if (frm.elements[j].id.indexOf ('CheckAll') != -1) 
					{	
						frm.elements[j].checked = true;
						break;
					}
				}
			}
	} 
	else 
	{
		for (i=0; i<frm.length; i++) 
			{
				if (idVal.indexOf ('CheckAll') != -1) 
				{
					if(chkVal == true) 
					{
						frm.elements[i].checked = true; 
					} 
					else 
					{
						frm.elements[i].checked = false; 
					}
				} 
				else if (idVal.indexOf('DeleteThis') != -1 && frm.elements[i].checked == false) 
				{
					for (j=0; j<frm.length; j++) 
					{
						if (frm.elements[j].id.indexOf ('CheckAll') != -1) 
						{ 
							frm.elements[j].checked = false;
							break; 
						} 
					} 
				} 
			} 
	} 
} 

function gfnCheckDelSelect()
{
	var lstrStatus;
	var frm = document.forms[0];			
	lstrStatus='N';
	for (i=0; i<frm.length; i++) 
	{			
		if (frm.elements[i].id.indexOf('DeleteThis') != -1 && frm.elements[i].checked == true) 
		{					
				lstrStatus='Y';
		}						
	}	
	if (lstrStatus=='N')
		{
			alert("At least one record should be selected for deletion");
			return false;
		}			
	else
		{
			return true;
		}
}
function gfnValidatePassword(pwd1, pwd2)
{
	if 	(pwd1.value != pwd2.value) {

		return false;
	}
	return true;
}

function gfnIsFieldBlank(theField) {
	if (theField.value.length == 0)
		return true;
	else
		return false;
}
function gfnValidateEmail(obj)
{
	if ((obj.value.indexOf('@') < 1) ||
		(obj.value.indexOf('.',0) == -1) ||
		(obj.value.indexOf('.') == obj.value.length - 1) ||
		(obj.value.indexOf(' ') != -1))
	{
		return false;
	}
	else return true;
}