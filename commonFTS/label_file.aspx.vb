#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region


Partial Class label_file
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents butClose As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Me.butClose.Attributes.Add("OnClick", "return fnCloseWin()")
        Neodynamic.WebControls.BarcodeProfessional.BarcodeProfessional.LicenseOwner = "Ng Ban Loo-Standard Edition-Developer License"
        Neodynamic.WebControls.BarcodeProfessional.BarcodeProfessional.LicenseKey = "4U9TAPBCCXK8V9HNCZHD2XTGSFELFH2B4J4NKLNMPAFST8TJ6E6Q"

        ''*** Start : Comment Out by CSS, 17 Aug 2009
        'Dim strVol As String
        'If Request("FVol").Length = "1" Then
        '    strVol = "00" + Request("FVol")
        'ElseIf Request("FVol").Length = "2" Then
        '    strVol = "0" + Request("FVol")
        'Else
        '    strVol = Request("FVol")
        'End If


        'lblFileRefVol.Text = Replace(Replace(Request("FRef"), "^", " "), "*", "'") + " (" + strVol + ")"
        'lblFileTitle.Text = Replace(Replace(Replace(Request("FTitle"), "^", " "), "~", vbCrLf), "*", "'")
        'barcodeCode.Code = Request("FBarcode")
        'lblFileNo.Text = Request("FNo")
        ''*** End   : Comment Out by CSS, 17 Aug 2009

        ''*** Start : Changes by CSS, 17 Aug 2009
        Dim strVolID As Integer = Request("FBarcode").Substring(2, 7)
        Dim strSQl As String = "select    " & _
                            "	        m.fld_FileID, " & _
                            "			ltrim(rtrim(d.fld_DepartmentCode)) + m.fld_FileNoCode1 + '.' + m.fld_FileNoCode2 + '.' + m.fld_FileNoCode3 + '.' + m.fld_FileNoCode4 as fileNum, " & _
                            "			m.fld_FileTitle, isnull(m.fld_FileRef,'') as fld_FileRef,  " & _
                            "			v.fld_VolumeID, v.fld_VolumeNo, " & _
                            "			isnull(v.fld_Keyword,'') as fld_Keyword, " & _
                            "			isnull(v.fld_Remarks,'') as fld_Remarks, " & _
                            "			convert(varchar,v.fld_CreatedDt ,103)  as fld_CreatedDt     " & _
                            " 		from dbo.tbl_File_Mst m " & _
                            "			inner join dbo.tbl_File_Volume v on m.fld_FileID = v.fld_FileID " & _
                            "			inner join dbo.tbl_Department d on m.fld_FileDeptID = d.fld_DepartmentID " & _
                            "		where  v.fld_VolumeID = '" & CStr(strVolID) & "'"

        Dim objRdr As SqlDataReader
        objRdr = SqlDataAccess.clsDataAccess.ExecuteReader(cnFTSDBString, CommandType.Text, strSQl)
        If Not objRdr Is Nothing Then
            If objRdr.HasRows Then
                objRdr.Read()

                Dim strVol As String = Trim(CStr(objRdr("fld_VolumeNo")))
                If strVol.Length = "1" Then
                    strVol = "00" + strVol
                ElseIf strVol.Length = "2" Then
                    strVol = "0" + strVol
                Else
                    strVol = strVol
                End If

                lblFileRefVol.Text = objRdr("fld_FileRef") + " (" + strVol + ")"
                lblFileTitle.Text = objRdr("fld_FileTitle")
                barcodeCode.Code = mfnGetBarcode("F", CStr(strVolID))
                lblFileNo.Text = objRdr("fileNum")
               
            End If
        End If
        objRdr.Close()
        ''*** End   : Changes by CSS, 17 Aug 2009
    End Sub
End Class
