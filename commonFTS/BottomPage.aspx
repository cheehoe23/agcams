<%@ Register TagPrefix="tagFooter" TagName="footer" src="~/commonFTS/FTSfooter_cr.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="BottomPage.aspx.vb" Inherits="AMS.BottomPage"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>BottomPage</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="FTSstyle.css" type="text/css" rel="Stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="frmBottomPage" method="post" runat="server">
			<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td valign="top" align="center">
						<table width="780" height="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="3">
									<table width="100%" cellpadding="0" border="0" cellspacing="0">
										<tr>
											<td valign="top" width="1" height="1"><img src="../images/bottomleft.gif"></td>
											<td background="../images/bottommid.gif"><img src="../images/spacer.gif" height="18"></td>
											<td valign="top" width="1"><img src="../images/bottomright.gif"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" align="center" colspan="3"><tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagfooter:footer></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
