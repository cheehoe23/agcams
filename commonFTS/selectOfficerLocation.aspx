<%@ Page Language="vb" AutoEventWireup="false" Codebehind="selectOfficerLocation.aspx.vb" Inherits="AMS.selectOfficerLocation"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>selectOfficerLocation</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../common/FTSstyle.css" type="text/css" rel="Stylesheet">
		<script language="JavaScript" src="../Common/CommonJS.js"></script>
		<script language="javascript">
		function chkFrm(){
			var foundError = false;
			if (!foundError && gfnGetSelectedValue(document.frm_ChgOffLoc.rdSelectOffLoc,'O')=='')
				{
					alert("At least One Officer/Location should be selected");
					foundError = true;
				}
				
			//alert (gfnGetSelectedValue(document.frm_ChgOffLoc.rdSelectOffLoc,'O'));
		
			if (!foundError){
			    document.frm_ChgOffLoc.hdnNewOffLocID.value = gfnGetSelectedValue(document.frm_ChgOffLoc.rdSelectOffLoc, 'O');
				flag = window.confirm("You are add this Officer/Location as current movement for the file management.\nAre you sure want to add this Officer/Location?");
 				return flag;
			}
			else
				return false;
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="frm_ChgOffLoc" method="post" runat="server">
			<TABLE id="tableSubStatusAdd" cellSpacing="1" cellPadding="1" width="90%" align="center"
				bgColor="white" border="0">
				<TBODY>
					<TR>
						<TD colSpan="2"><B>Change Officer/Location</B></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></TD>
					</TR>
					<tr>
						<td colSpan="2">
							<!-- Start: Search Part -->
							<table width="100%" border="0">
								<TR>
									<TD align="left" colSpan="2">&nbsp;</TD>
								</TR>
								<TR>
									<TD align="left" bgColor="#ffff99" colSpan="2"><b>Search - Officer/Location</b></TD>
								</TR>
								<TR>
									<%--<TD vAlign="middle" width="20%"><FONT color="#666666">Type</FONT></TD>
									<TD><asp:radiobuttonlist id="rdOLType" RepeatDirection="Horizontal" AutoPostBack="True" Runat="server">
											<asp:ListItem Value="U">Officer</asp:ListItem>
											<asp:ListItem Value="L" Selected>Location</asp:ListItem>
										</asp:radiobuttonlist></TD> --%>
								<TR>
									<TD vAlign="middle" width="20%"><FONT color="#666666">Officer/Location Name</FONT></TD>
									<TD><asp:textbox id="txtOffLocName" Runat="server" MaxLength="200" Width="200"></asp:textbox></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="2"><asp:button id="butSearch" Runat="Server" 
                                            Width="60px" Text="Search"></asp:button>
										<asp:button id="butSearchReset" Runat="Server" Width="60px" Text="Reset"></asp:button>
										<asp:button id="butSearchCancel" Runat="Server" Width="60px" Text="Cancel"></asp:button></TD>
								</TR>
							</table>
							<!-- End  : Search Part -->
							<!-- Start: Search Result Part -->
							<div id="divSearchResult" runat="server">
								<table width="100%" border="0">
									<TR>
										<TD align="left" colSpan="2">&nbsp;</TD>
									</TR>
									<TR>
										<TD align="left" colSpan="2">&nbsp;</TD>
									</TR>
									<TR>
										<TD align="left" bgColor="#ffff99" colSpan="2"><b>Search Result - Officer/Location</b></TD>
									</TR>
									<TR>
										<TD vAlign="top" colSpan="2">
											<!--Datagrid for display record.--><asp:datagrid id="dgOffLoc" Runat="server" Width="100%" PagerStyle-Height="25" AlternatingItemStyle-Height="25"
												ItemStyle-Height="25" AutoGenerateColumns="False" BorderStyle="None" BorderColor="#ffffff" PagerStyle-CssClass="DGpageStyle"
												AllowPaging="True" PageSize="10" PagerStyle-HorizontalAlign="Right" PagerStyle-VerticalAlign="Middle" PagerStyle-Mode="NumericPages"
												PagerStyle-Position="Top" PagerStyle-BackColor="#a3a9cc" datakeyfield="fld_OffLocID" AlternatingItemStyle-BackColor="#e3d9ee">
												<headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
													Height="25"></headerstyle>
												<Columns>
													<asp:boundcolumn visible="false" datafield="fld_OffLocID" headertext="Officer/Location ID" ItemStyle-Height="10">
														<itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
													</asp:boundcolumn>                                                    
													<asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
														<itemstyle width="15%" cssclass="GridText" verticalalign="Middle"></itemstyle>
													</asp:boundcolumn>
													<asp:boundcolumn datafield="OffLocName" headertext="Officer/Location" ItemStyle-Height="10">
														<itemstyle width="25%" cssclass="GridText" verticalalign="Middle"></itemstyle>
													</asp:boundcolumn>                                                   
													<asp:TemplateColumn HeaderText="Select" ItemStyle-HorizontalAlign="Center" ItemStyle-Width=20%>
														<ItemTemplate>
															<%# fnShowRadioButton(DataBinder.Eval(Container.DataItem, "fld_OffLocID"))%>
														</ItemTemplate>
													</asp:TemplateColumn>
												</Columns>
											</asp:datagrid></TD>
									</TR>
									<TR>
										<TD vAlign="top" align="right" colSpan="2"><asp:button id="butSave" Runat="Server" Width="50" Text="Save"></asp:button></TD>
									</TR>
								</table>
							</div>
							<!-- End  : Search Result Part --></td>
					</tr>
				</TBODY>
			</TABLE>
			<!-- Start: Hidden Fields -->
			<input id="hdnCallFrm" type="hidden" name="hdnCallFrm" runat="server"> 
			<input id="hdnChgOffLocF" type="hidden" name="hdnChgOffLocF" runat="server">
			<input id="hdnFileID" type="hidden" name="hdnFileID" runat="server"> 
            <input id="hdnFileDetailID" type="hidden" name="hdnFileDetailID" runat="server">
			<input id="hdnOffLocID" type="hidden" name="hdnOffLocID" runat="server">
			<input id="hdnNewOffLocID" type="hidden" name="hdnNewOffLocID" runat="server">            
			<input id="hdnNewOffLocName" type="hidden" name="hdnNewOffLocName" runat="server">
			<!-- End  : Hidden Fields --></form>
		<script language="javascript">
			if (document.frm_ChgOffLoc.hdnChgOffLocF.value == 'Y')
			{
			    var openerForm = opener.document.forms.frm_fileEditFile;
				openerForm.action = "../file_mng/File_EditFile.aspx";

				window.opener.document.forms(0).hdnChgOffLocF.value = document.frm_ChgOffLoc.hdnChgOffLocF.value;
				window.opener.document.forms(0).hdnOffLocID.value = document.frm_ChgOffLoc.hdnNewOffLocID.value;
				window.opener.document.forms(0).hdnOffLocName.value = document.frm_ChgOffLoc.hdnNewOffLocName.value;
				openerForm.method = "post";
				openerForm.submit();
				window.close();	
			}
		</script>
	</body>
</HTML>
