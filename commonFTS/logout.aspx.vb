#Region "Information Section"
' ****************************************************************************************************
' Description       : Logout Page
' Purpose           : Logout Page
' Author            : See Siew
' Date              : 17/04/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class logout
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtErrMsg As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim intRetVal As Integer
            Dim strMsg As String

            ''Set Default
            lblErrorMessage.Visible = False

            '''Two case for logout --> User "logout their self", timeout
            If Request("CallFrm") = "logout" And Len(Session("UsrID")) <> 0 Then
                lblMsg.Text = "You have logout successfully."

                ''Update Logout Time
                intRetVal = clsCommon.fnLogout_UpdateUser(Session("UsrID"))
                If intRetVal > 0 Then
                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "User Logout")
                Else
                    strMsg = "Logout Time updated Failed. Please contact your administrator."
                    lblErrorMessage.Visible = strMsg
                    lblErrorMessage.Visible = True
                End If

            Else   '''Time out 
                lblMsg.Text = "Your session has expired. Please login again"
            End If

            'If remove session variable
            Session.Remove("Name")
            Session.Remove("UsrID")
            Session.Remove("LoginID")
            Session.Remove("AR")
            Session.Remove("dtSubStatus")            ''For Sub Status Adding
            Session.Remove("dsDynamicRptRec")        ''For Dynamic Report
            Session.Remove("dsMstFileListRptRec")    ''For Master File List Report
            Session.Remove("dsTrackFileRpt")         ''For Tracking Files Report
            Session.Remove("dsDelinqRpt")            ''For Delinqency Report
            Session.Abandon()

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    'If Len(Session("UsrID")) = 0 Then
    '    Session("TO") = "TO"
    '    Response.Redirect("/common/logout.aspx", False)
    '    Exit Sub
    'End If
End Class
