#Region "Information Section"
' ****************************************************************************************************
' Description       : Change Officer/Location
' Purpose           : Change Officer/Location
' Author            : See Siew
' Date              : 10/04/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Class selectOfficerLocation
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            '''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Response.Redirect("/common/logout.aspx", False)
                Exit Sub
            End If
            '''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""
            Me.butSave.Attributes.Add("OnClick", "return chkFrm()")

            If Not Page.IsPostBack Then
                ''Get Pass Value from Main screen
                hdnCallFrm.Value = Request("CFrm")
                hdnFileID.Value = Request("FID")
                hdnFileDetailID.Value = Request("FDID")
                hdnOffLocID.Value = Request("OffLocID")
                'hdnOffLocName.Value = Request("OffLocName")
                ''set Change Officer/Location to "N"
                hdnChgOffLocF.Value = "N"
                divSearchResult.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butSearchCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearchCancel.Click
        Dim strJavaScript As String
        strJavaScript = "<script language = 'Javascript'>" & _
                        "window.close();" & _
                        "</script>"
        Response.Write(strJavaScript)
    End Sub

    Private Sub butSearchReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearchReset.Click
        Try
            hdnChgOffLocF.Value = "N"
            txtOffLocName.Text = ""
            divSearchResult.Visible = False
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
        Try
            ''Set current Page to 0
            dgOffLoc.CurrentPageIndex = 0

            ''Get Records
            fnGetSearchRecord()
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnGetSearchRecord()
        Try
            ''get records
            dgOffLoc.DataSource = clsCommon.fnOffLocGetRecord(txtOffLocName.Text)
            dgOffLoc.DataBind()
            If Not dgOffLoc.Items.Count > 0 Then
                divSearchResult.Visible = False
                lblErrorMessage.Text = "There is no record(s) found."
                lblErrorMessage.Visible = True
            Else
                divSearchResult.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        Try
            Dim intRetVal As Integer
            Dim strMsg As String
            Dim strOffLocType As String = ""
            Dim strOffLocID As String = ""
            Dim strOffLocName As String = ""

            ''*** Start: Get officer/Location that selected for movement
            strOffLocID = hdnNewOffLocID.Value

            Dim GridItem As DataGridItem
            Dim chkSelected As RadioButton
            For Each GridItem In dgOffLoc.Items
                If GridItem.Cells(0).Text = strOffLocID Then
                    strOffLocType = GridItem.Cells(1).Text
                    strOffLocName = GridItem.Cells(2).Text
                    hdnNewOffLocName.Value = strOffLocName
                End If
            Next
            ''*** End  : Get officer/Location that selected for movement

            If strOffLocID <> "" Then
                ''Add File Movement record
                intRetVal = clsCommon.fnOffLocAddMovement( _
                                 hdnFileDetailID.Value, strOffLocID, _
                                    Session("LoginID"), Session("UsrID"))

                If intRetVal > 0 Then
                    strMsg = "Movement Update Successfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "Movement Updated : " + strOffLocName + " (File Numner = " + hdnFileID.Value + ")")

                    ''set movement update successfully flag
                    hdnChgOffLocF.Value = "Y"

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                    'Session("Status") = "Y"
                Else
                    strMsg = "Movement Update Failed. Please contact your administrator."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If

            Else
                lblErrorMessage.Text = "Please select a record for Movement."
                lblErrorMessage.Visible = True
            End If


        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & ex.TargetSite.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub dgOffLoc_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgOffLoc.PageIndexChanged
        dgOffLoc.CurrentPageIndex = e.NewPageIndex
        fnGetSearchRecord()
    End Sub

    Public Function fnShowRadioButton(ByVal OffLocID As String) As String
        Dim strRetVal As String

        If OffLocID = hdnOffLocID.Value Then
            strRetVal = ""
        Else
            strRetVal = "<input type=radio id='rdSelectOffLoc' name='rdSelectOffLoc' value='" & OffLocID & "' runat=server>"
        End If

        Return (strRetVal)
    End Function
   
End Class
