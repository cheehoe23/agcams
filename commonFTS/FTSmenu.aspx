<%@ Page Language="vb" AutoEventWireup="false" Codebehind="FTSmenu.aspx.vb" Inherits="AMS.FTSmenu"%>
<%@ Register TagPrefix="ie" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>menu</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="FTSstyle.css" type="text/css" rel="Stylesheet">	 
	</HEAD>
	<body MS_POSITIONING="GridLayout" bgcolor="#e8e8f4">
		<form id="Form1" method="post" runat="server">
			<!-- Start: Menu -->
            <table border="1" cellSpacing="1" cellPadding="1" align="center">
                            <tr>
                            <td align=center valign="top">
                                                <strong>Go To</strong></td>
								<td valign="top" align="left">
                                    <asp:LinkButton ID="lbtnAMS" runat="server">Asset Management Module</asp:LinkButton>
                                </td>
                            </tr>
                            </table>
			<table border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%" align="center" >
				<tr valign="top">
					<td class="sidebar" valign="top">
						<table border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%" align="center">
							
							<tr height="12" valign="top">
								<td vAlign="top" colspan="3"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></td>
							</tr>
							<tr height="12" valign="top">
								<td valign="top"><ie:treeview id="TvMenu" runat="server" SelectExpands="True" SystemImagesPath="/webctrl_client/1_0/treeimages/"
										SelectedStyle="font-family:Verdana, Helvetica, Sans-serif;" DefaultStyle="font-size:10pt;font-weight:bold;color:#615AB7;font-family:Verdana, Helvetica, Sans-serif;"
										HoverStyle="font-family:Verdana, Helvetica, Sans-serif;"></ie:treeview></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>			
			<!-- End  : Menu -->
			<!--	
			<A id="A1" href="../usr_mng/usr_View.aspx" target="content" runat="server">User</A>
			<br>
			<A href="../usr_mng/usrGrp_View.aspx" target="content" runat="server">User Group</A>
			<br>
			<A id="A2" href="../usr_mng/chgPwd.aspx?callFrm=Menu" target="content" runat="server">
				Change Password</A>
			<br>
			<A id="A4" href="../sysSetting/generalSetting.aspx" target="content" runat="server">
				General Setting</A>
			<br>
			<A id="A3" href="../sysSetting/dept_view.aspx" target="content" runat="server">Department</A>
			<br>
			<A id="A5" href="../sysSetting/location_view.aspx" target="content" runat="server">Location</A>
			<br>
			<A id="A6" href="../sysSetting/subjTopic_View.aspx" target="content" runat="server">
				Subject/(Sub)Topic</A> 
			<br>
			<br>
			<br>

			<table id="Table9" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
-->  <!-- Start: Part 2 - Logo -->
			<!--
				<tr>
					<td align="center"><img src="../images/AVAlogo.gif" align="middle"><br>
						<b>FTS</b>
						<br>
						Help Desk : <b>6760 8855</b>
					</td>
				</tr>
-->
			<!-- End  : Part 2 - Logo -->
			<!--			
			</table>
-->
		</form>
	</body>
</HTML>
