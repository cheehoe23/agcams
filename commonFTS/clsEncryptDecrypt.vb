﻿Imports System.Collections
Imports System.Collections.Generic
Imports System.Data
Imports System.Diagnostics
Imports System.IO
Imports System.Xml
Imports System.Text
Imports System.Security.Cryptography

Public Class clsEncryptDecrypt
    Public Shared key As Byte() = {}
    Public Shared IV As Byte() = {&H12, &H34, &H56, &H78, &H90, &HAB, _
     &HCD, &HEF}

    Public Shared Function EncryptText(ByVal strText As String) As String
        Return Encrypt(strText, "!#$a54?3")
    End Function

    'Decrypt the text 
    Public Shared Function DecryptText(ByVal strText As String) As String
        Return Decrypt(strText.Replace(" ", "+"), "!#$a54?3").Replace("+", " ")
    End Function

    Public Shared Function Left(param As String, length As Integer) As String
        'we start at 0 since we want to get the characters starting from the
        'left and with the specified lenght and assign it to a variable
        Dim result As String = param.Substring(0, length)
        'return the result of the operation
        Return result
    End Function

    Public Shared Function Decrypt(stringToDecrypt As String, sEncryptionKey As String) As String
        Dim inputByteArray As Byte() = New Byte(stringToDecrypt.Length) {}
        Try

            key = System.Text.Encoding.UTF8.GetBytes(Left(sEncryptionKey, 8))
            Dim des As New DESCryptoServiceProvider()
            inputByteArray = Convert.FromBase64String(stringToDecrypt)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8
            Return encoding.GetString(ms.ToArray())
        Catch e As Exception
            Return e.Message
        End Try
    End Function

    Public Shared Function Encrypt(stringToEncrypt As String, SEncryptionKey As String) As String
        Try
            key = System.Text.Encoding.UTF8.GetBytes(Left(SEncryptionKey, 8))
            Dim des As New DESCryptoServiceProvider()
            Dim inputByteArray As Byte() = Encoding.UTF8.GetBytes(stringToEncrypt)

            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write)

            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Return Convert.ToBase64String(ms.ToArray())
        Catch e As Exception
            Return e.Message
        End Try
    End Function

End Class
