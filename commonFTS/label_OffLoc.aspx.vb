Partial Class label_OffLoc
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Neodynamic.WebControls.BarcodeProfessional.BarcodeProfessional.LicenseOwner = "Ng Ban Loo-Standard Edition-Developer License"
        Neodynamic.WebControls.BarcodeProfessional.BarcodeProfessional.LicenseKey = "4U9TAPBCCXK8V9HNCZHD2XTGSFELFH2B4J4NKLNMPAFST8TJ6E6Q"



        Dim strBarcode As String
        If Request("OLType") = "L" Then
            strBarcode = FTSmodCommonFunction.mfnGetBarcode("L", Request("OLBarcode"))
        Else
            strBarcode = FTSmodCommonFunction.mfnGetBarcode("U", Request("OLBarcode"))
        End If


        lblOffLocName.Text = Replace(Request("OLName"), "^", " ")
        barcodeCode.Code = strBarcode
    End Sub

End Class
