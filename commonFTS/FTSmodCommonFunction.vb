#Region "Project Information Section"
' ****************************************************************************************************
' Description       : Common Functions.
' Purpose           : This file provides common functions accessible by other programs.
' Date			    : 25.01.2007
' ****************************************************************************************************
#End Region

#Region "Options Section"
Option Strict Off
Option Explicit On 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data             ' Imports the Data class.
Imports System.Data.SqlClient   ' Imports the SQL Client class.
Imports System.Web.Mail         ' Imports the mail class.
#End Region

Module FTSmodCommonFunction
    Public Function fnPopulateDeptTypeInRD( _
                                            ByVal objRadioButList As RadioButtonList _
                                            ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add Department Type Value in Radio Button List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 30.04.2007
        ' ****************************************************************************************************
        Try
            objRadioButList.Items.Add(New ListItem("File", "F"))
            objRadioButList.Items.Add(New ListItem("User", "U"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateUserTypeInDDL( _
                                           ByVal objDDList As DropDownList _
                                           ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add User Type Value in Drop Down List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 08.05.2007
        ' ****************************************************************************************************
        Try
            objDDList.Items.Add(New ListItem("", ""))
            objDDList.Items.Add(New ListItem("FTS User", "F"))
            objDDList.Items.Add(New ListItem("Officer", "O"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateUserTypeInRD( _
                                        ByVal objRadioButList As RadioButtonList _
                                        ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add User Type Value in Radio Button List.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 29.04.2007
        ' ****************************************************************************************************
        Try
            objRadioButList.Items.Add(New ListItem("FTS User", "F"))
            objRadioButList.Items.Add(New ListItem("Officer", "O"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Public Function mfnPrintLabel( _
    '            ByVal strFileNum As String, _
    '            ByVal strFileTitle As String, _
    '            ByVal strFileBarcode As String, _
    '            ByVal strFileRef As String) As String
    '    ' ****************************************************************************************************
    '    ' Purpose  		    : To Print Label --> File Number, File Title, File Barcode, Filr Reference
    '    ' Parameters	    : File Number, File Title, File Barcode, Filr Reference
    '    ' Returns		    : String
    '    ' Date			    : 23.04.2007
    '    ' ****************************************************************************************************
    '    Try

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    Public Function mfnGetBarcode( _
                    ByVal strType As String, _
                    ByVal strID As String) As String
        ' ****************************************************************************************************
        ' Purpose  		    : To Get Barcode for File, Location, User
        ' Parameters	    : Type ((F)ile, (L)ocation, (U)ser)
        ' Returns		    : String
        ' Date			    : 16.04.2007
        ' ****************************************************************************************************
        Try
            Dim strBarcode As String

            If strType = "F" Then
                strBarcode = "FB" + Right("0000000" + strID, 7)

            ElseIf strType = "L" Then
                strBarcode = "LB" + Right("0000000" + strID, 7)

            ElseIf strType = "U" Then
                strBarcode = "UB" + Right("0000000" + strID, 7)

            End If

            Return strBarcode
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateAndOrInDDL( _
                                        ByVal objDropDownList As DropDownList _
                                        ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add AND/OR in DropDownList
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 27.03.2007
        ' ****************************************************************************************************
        Try
            Dim intPage As Integer
            objDropDownList.Items.Add(New ListItem("And", "and"))
            objDropDownList.Items.Add(New ListItem("Or", "or"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateSecurityStatusInDDL( _
                                    ByVal objDropDownList As DropDownList _
                                    ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add Security Status Value in DropDownList.
        ' Parameters	    : objDropDownList
        ' Returns		    : Boolean
        ' Date			    : 26.03.2007
        ' ****************************************************************************************************
        Try
            Dim intPage As Integer
            objDropDownList.Items.Add(New ListItem("", "-1"))
            objDropDownList.Items.Add(New ListItem("Confidential", "2"))
            objDropDownList.Items.Add(New ListItem("Non-Confidential", "3"))
            objDropDownList.Items.Add(New ListItem("Others", "1"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function mfnGetCurrentDateForTopPage() As String
        ' ****************************************************************************************************
        ' Purpose  		    : To Get Current Date [such as : 3rd March 2007 (Saturday)]
        ' Parameters	    : -
        ' Returns		    : String
        ' ****************************************************************************************************
        Try
            Dim strCurrentDt As String
            Dim strMonth() As String = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}
            Dim strWeekDay() As String = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"}

            ''get Current date in [such as : 3rd March 2007 (Saturday)] format
            ''1. Get day
            Select Case CStr(Now.Day)
                Case "1"
                    strCurrentDt = CStr(Now.Day) + "st"
                Case "2"
                    strCurrentDt = CStr(Now.Day) + "nd"
                Case "3"
                    strCurrentDt = CStr(Now.Day) + "rd"
                Case Else
                    strCurrentDt = CStr(Now.Day) + "th"
            End Select

            ''2. Get Month
            strCurrentDt += " " + strMonth(CStr(Now.Month) - 1)

            ''3. Get Year
            strCurrentDt += " " + CStr(Now.Year)

            ''4. Get Week Day
            strCurrentDt += " (" + strWeekDay(CStr(Now.DayOfWeek)) + ")"

            Return strCurrentDt
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function mfnGetCurrentDate() As String
        ' ****************************************************************************************************
        ' Purpose  		    : To Get Current Date
        ' Parameters	    : -
        ' Returns		    : String
        ' ****************************************************************************************************
        Try
            Dim strCurrentDt As String

            ''get Current date in DD/MM/YYYY format
            If CStr(Now.Day).Length = 1 Then
                strCurrentDt = "0"
            End If
            strCurrentDt += Now.Day & "/"

            If CStr(Now.Month).Length = 1 Then
                strCurrentDt += "0"
            End If
            strCurrentDt += Now.Month & "/" & Now.Year

            Return strCurrentDt
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnUserEmailInfo( _
                                ByVal strUserName As String, _
                                ByVal strEmail As String, _
                                ByVal strLoginId As String, _
                                ByVal strPwd As String, _
                                ByVal EmailFrom As String _
                                ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To send email for user
        ' Parameters	    : strUserName, strEmail,strLoginId, strPwd
        ' Returns		    : Boolean
        ' ****************************************************************************************************
        Try
            Dim strMailBody As String
            Dim strMailSubject As String

            ''Email Subject
            strMailSubject = "New Account creation in FTS"

            ''Email Body
            strMailBody = "Dear " & strUserName & ",<br>"
            strMailBody += "Your account to File Tracking System had been created. Please login with the following information.<br>"
            strMailBody += "Login ID : " & strLoginId & "<br>"
            strMailBody += "Password: " & strPwd & "<br><br><br><br><br>"
            strMailBody += "Thank you.<br>"

            Return fnSendEmail(strEmail, "", "", strMailSubject, strMailBody, EmailFrom)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnUserEmailActiveAcc( _
                                ByVal strUserName As String, _
                                ByVal strEmail As String, _
                                ByVal strLoginId As String, _
                                ByVal strPwd As String, _
                                ByVal EmailFrom As String _
                                ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To send email for user that account have been active
        ' Parameters	    : strUserName, strEmail,strLoginId, strPwd
        ' Returns		    : Boolean
        ' ****************************************************************************************************
        Try
            Dim strMailBody As String
            Dim strMailSubject As String

            ''Email Subject
            strMailSubject = "Account had been activated in FTS"

            ''Email Body
            strMailBody = "Dear " & strUserName & ",<br>"
            'strMailBody += "Your account to File Tracking System had been activated. Please login with the following information.<br>"
            strMailBody += "You have been change from Officer to FTS User. Please login with the following information.<br>"
            strMailBody += "Login ID : " & strLoginId & "<br>"
            strMailBody += "Password: " & strPwd & "<br><br><br><br><br>"
            strMailBody += "Thank you.<br>"

            Return fnSendEmail(strEmail, "", "", strMailSubject, strMailBody, EmailFrom)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnUserEmailResetPwd( _
                                    ByVal strUserName As String, _
                                    ByVal strEmail As String, _
                                    ByVal strLoginId As String, _
                                    ByVal strPwd As String, _
                                    ByVal EmailFrom As String _
                                    ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To send email for user that Password have been reset.
        ' Parameters	    : strUserName, strEmail,strLoginId, strPwd
        ' Returns		    : Boolean
        ' ****************************************************************************************************
        Try
            Dim strMailBody As String
            Dim strMailSubject As String

            ''Email Subject
            strMailSubject = "Password had been reseted in FTS"

            ''Email Body
            strMailBody = "Dear " & strUserName & ",<br>"
            strMailBody += "Your password to File Tracking System had been reseted. Please login with the following information.<br>"
            strMailBody += "Login ID : " & strLoginId & "<br>"
            strMailBody += "Password: " & strPwd & "<br><br><br><br><br>"
            strMailBody += "Thank you.<br>"

            Return fnSendEmail(strEmail, "", "", strMailSubject, strMailBody, EmailFrom)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnUserEmailChangePwd( _
                                ByVal strUserName As String, _
                                ByVal strEmail As String, _
                                ByVal strLoginId As String, _
                                ByVal strPwd As String, _
                                ByVal EmailFrom As String _
                                ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To send email for user that Password have been changed.
        ' Parameters	    : strUserName, strEmail,strLoginId, strPwd
        ' Returns		    : Boolean
        ' ****************************************************************************************************
        Try
            Dim strMailBody As String
            Dim strMailSubject As String

            ''Email Subject
            strMailSubject = "Password had been changed in FTS"

            ''Email Body
            strMailBody = "Dear " & strUserName & ",<br>"
            strMailBody += "Your password to File Tracking System had been changed. Please login with the following information.<br>"
            strMailBody += "Login ID : " & strLoginId & "<br>"
            strMailBody += "Password: " & strPwd & "<br><br><br><br><br>"
            strMailBody += "Thank you.<br>"

            Return fnSendEmail(strEmail, "", "", strMailSubject, strMailBody, EmailFrom)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateDropDownListWithDefaultValue( _
                        ByVal objSQLDataReader As SqlDataReader, _
                        ByVal objDropDownList As DropDownList, _
                        ByVal DataValueField As String, _
                        ByVal DataTextField As String, _
                        ByVal blnDefaultSelectedValue As Boolean, _
                        ByVal strDefaultText As String, _
                        ByVal strDefaultValue As String _
                        ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To populate the drop down list with Default Value.
        ' Parameters	    : objSQLDataReader, objDropDownList,
        '                     DataValueField, DataTextField, blnDefaultSelectedValue, strDefaultValue
        ' Returns		    : Boolean
        ' ****************************************************************************************************
        Try
            If objSQLDataReader Is Nothing Then Return False
            If objSQLDataReader.HasRows Then
                With objDropDownList
                    .DataSource = objSQLDataReader
                    .DataTextField = DataTextField
                    .DataValueField = DataValueField
                    .DataBind()
                End With

                ' Add an empty value to the combo if the Default Selected Value is false.
                If Not blnDefaultSelectedValue Then
                    objDropDownList.Items.Insert(0, New ListItem(strDefaultText, strDefaultValue))
                End If
                objSQLDataReader.Close()
                Return True
            Else
                objSQLDataReader.Close()
                Return False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulateDropDownList( _
                            ByVal objSQLDataReader As SqlDataReader, _
                            ByVal objDropDownList As DropDownList, _
                            ByVal DataValueField As String, _
                            ByVal DataTextField As String, _
                            ByVal blnDefaultSelectedValue As Boolean _
                            ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To populate the drop down list.
        ' Parameters	    : objSQLDataReader, objDropDownList,
        '                     DataValueField, DataTextField, blnDefaultSelectedValue
        ' Returns		    : Boolean
        ' ****************************************************************************************************
        Try
            If objSQLDataReader Is Nothing Then Return False
            If objSQLDataReader.HasRows Then
                With objDropDownList
                    .DataSource = objSQLDataReader
                    .DataTextField = DataTextField
                    .DataValueField = DataValueField
                    .DataBind()
                End With
                ' Add an empty value to the combo if the Default Selected Value is false.
                If Not blnDefaultSelectedValue Then
                    objDropDownList.Items.Insert(0, New ListItem("", "-1"))
                End If
                objSQLDataReader.Close()
                Return True
            Else
                objSQLDataReader.Close()
                Return False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnPopulatePagesInDDL( _
                                ByVal objDropDownList As DropDownList _
                                ) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To add the Page size in the DropDownList in 10 pages.
        ' Parameters	    : objDropDownList, blnDefaultSelectedValue 
        ' Returns		    : Boolean
        ' Date			    : 25.01.2007
        ' ****************************************************************************************************
        Try
            Dim intPage As Integer
            'objDropDownList.Items.Add(New ListItem("2", "2"))
            'objDropDownList.Items.Add(New ListItem("5", "5"))

            For intPage = 10 To 50 Step 10
                objDropDownList.Items.Add(New ListItem(CStr(intPage), CStr(intPage)))
            Next
            objDropDownList.Items.Add(New ListItem("100", "100"))
            objDropDownList.Items.Add(New ListItem("All", "1000000"))
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSendEmail( _
                            ByVal EmailTo As String, _
                            ByVal EmailCC As String, _
                            ByVal EmailBCC As String, _
                            ByVal Subject As String, _
                            ByVal BodyText As String, _
                            ByVal EmailFrom As String) As Boolean
        ' ****************************************************************************************************
        ' Purpose  		    : To send email to the persons.
        ' Returns		    : Boolean
        ' Date			    : 25.01.2005
        ' ****************************************************************************************************
        Try
            Dim objMail As New MailMessage
            Dim objSMTP As Mail.SmtpMail

            ' The SMTP server IP Address.
            SmtpMail.SmtpServer = ConfigurationSettings.AppSettings("SmtpServerIP")        ''UAT / Production
            'SmtpMail.SmtpServer.Insert(0, ConfigurationSettings.AppSettings("SmtpServerIP")) ''Local

            ' The email address of the sender.
            objMail.From = ConfigurationSettings.AppSettings("EmailUserID") 'Session("uEmail") ' 'EmailFrom

            ' The email address of the recipient.
            objMail.To = EmailTo   '"seesiew@raptech.com.sg" '

            ' The email address of the CC recipient.
            objMail.Cc = EmailCC

            ' The email address of the BCC recipient.
            objMail.Bcc = EmailBCC

            ' The format of the message - it can be MailFormat.Text or MailFormat.Html 
            objMail.BodyFormat = MailFormat.Html

            ' The priority of the message  - it can be High, Normal Or Low.
            objMail.Priority = MailPriority.Normal

            ' The subject of the message 
            objMail.Subject = Subject

            ' The message text 
            objMail.Body = BodyText

            ' Send the mail.
            SmtpMail.Send(objMail)
            Return True

        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

End Module
