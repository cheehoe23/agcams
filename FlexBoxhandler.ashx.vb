﻿Imports System.Collections.Generic
Imports System.Web
Imports System.Web.Script.Serialization

Public Class FlexBoxhandler
    Implements IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "application/json"
        Dim results = New List(Of Emp)()
        'if (context.Request.QueryString["q"].Length != 0)
        '{
        '    string nametosearch = context.Request.QueryString["q"];
        '    //filter you data from db and return it in the same format as in the else section
        '}
        'else
        '{
        results.Add(New Emp(1, "Alaa", 24, "Male"))
        results.Add(New Emp(2, "Tariq", 40, "Male"))
        results.Add(New Emp(3, "Layth", 27, "Male"))
        results.Add(New Emp(4, "Whatever!", 60, "Female"))
        '}

        Dim data = New With {Key results}
        'Make sure to add:using System.Web.Script.Serialization;
        Dim json = New JavaScriptSerializer()
        context.Response.Write(json.Serialize(data))
    End Sub

    Public Class Emp
        Public Property id() As Integer
        Public Property name() As String
        Public Property Age() As Integer
        Public Property Sex() As String
        Public Sub New(ByVal empid As Integer, ByVal empname As String, ByVal age As Integer, ByVal sex As String)
            id = empid
            name = empname
            Age = age
            Sex = sex
        End Sub
    End Class

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class