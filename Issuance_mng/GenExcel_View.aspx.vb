#Region "Information Section"
' ****************************************************************************************************
' Description       : View Asset 
' Purpose           : View Asset Information
' Date              : 15/02/2008
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class GenExcel_View
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                'Response.Redirect("~/common/logout.aspx", False)
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Text = ""
            lblErrorMessage.Visible = False

            If Not Page.IsPostBack Then
                Me.butGenExcel.Attributes.Add("OnClick", "return fnConfirmSelectedRec()")

                ''Get Value pass from Search Screen
                hdnAssetIdAMS.Value = Request("AIdAMS")
                hdnAssetIdNFS.Value = Request("AIdNFS")
                hdnAssetType.Value = Request("AType")
                hdnAssetCatID.Value = Request("CatID")
                hdnAssetCatSubID.Value = Request("CatSID")
                hdnAssetStatus.Value = Request("Astatus")
                hdnPurchaseFDt.Value = Request("PFDt")
                hdnPurchaseTDt.Value = Request("PTDt")
                hdnWarExpFDt.Value = Request("WFDt")
                hdnWarExpTDt.Value = Request("WTDt")
                hdnDeptID.Value = Request("DeptID")
                hdnLocID.Value = Request("LocID")
                hdnLocSubID.Value = Request("SLocID")
                hdnOwnerID.Value = Request("OwnerID")
                hdnTempAsset.Value = Request("TempAsset")
                hdnCtrlItemF.Value = Request("CtrlItemF")

                ''default Sorting/setting
                hdnSortName.Value = "fldAssetBarcode"
                hdnSortAD.Value = "ASC"

                ''Populate Pages
                fnPopulatePagesInDDL(ddlPageSize)

                ''Display Record
                fnPopulateRecords()

                ''Checking Access Right and Function available for each module
                fnCheckAccessRightAvailable()

            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnCheckAccessRightAvailable()
        Try
            ''Checking For Assign Right
            If (InStr(Session("AR"), "IISMng|GenExcel|GenExcel") = 0) Then
                butGenExcel.Visible = False
                dgAsset.Columns(11).Visible = False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnPopulateRecords() As Boolean
        Try
            '' Sort Header Display
            fnSortHeaderDisplay()

            '' Get Records
            dgAsset.DataSource = clsIssuance.fnIssuanceGenExcel_SearchRecord( _
                                     hdnAssetIdAMS.Value, hdnAssetIdNFS.Value, _
                                     hdnAssetType.Value, hdnAssetCatID.Value, hdnAssetCatSubID.Value, _
                                     hdnAssetStatus.Value, hdnPurchaseFDt.Value, hdnPurchaseTDt.Value, _
                                     hdnWarExpFDt.Value, hdnWarExpTDt.Value, _
                                     hdnDeptID.Value, hdnLocID.Value, hdnOwnerID.Value, _
                                     hdnTempAsset.Value, hdnSortName.Value, hdnSortAD.Value, _
                                     hdnLocSubID.Value, hdnCtrlItemF.Value)
            dgAsset.DataBind()
            If Not dgAsset.Items.Count > 0 Then ''Not Records found
                ddlPageSize.Enabled = False
                butGenExcel.Enabled = False
                lblErrorMessage.Text = "There is no asset record."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnSortHeaderDisplay() As Boolean
        '' Can be Sort By :-
        ''       fldAssetBarcode (2), fld_AssetTypeStr (3), 
        ''       fld_CategoryName (4), fld_CatSubName (5), 
        ''       fld_DepartmentName (6), fld_LocationName (7), 
        ''       fld_OwnerName (8), fld_AssetStatusStr (9)

        Dim intSortIndex As Integer
        Dim strSortHeader As String = ""

        ''Set Default Header
        dgAsset.Columns(2).HeaderText = "Asset ID"
        dgAsset.Columns(3).HeaderText = "Type"
        dgAsset.Columns(4).HeaderText = "Category"
        dgAsset.Columns(5).HeaderText = "Subcategory"
        dgAsset.Columns(6).HeaderText = "Department"
        dgAsset.Columns(7).HeaderText = "Location"
        dgAsset.Columns(8).HeaderText = "Sub Location"
        dgAsset.Columns(9).HeaderText = "Assigned Owner"
        dgAsset.Columns(10).HeaderText = "Status"

        ''Check for Selected Header
        Select Case hdnSortName.Value
            Case "fldAssetBarcode"
                intSortIndex = 2
                strSortHeader = "Asset ID"
            Case "fld_AssetTypeStr"
                intSortIndex = 3
                strSortHeader = "Type"
            Case "fld_CategoryName"
                intSortIndex = 4
                strSortHeader = "Category"
            Case "fld_CatSubName"
                intSortIndex = 5
                strSortHeader = "Subcategory"
            Case "fld_DepartmentName"
                intSortIndex = 6
                strSortHeader = "Department"
            Case "fld_LocationName"
                intSortIndex = 7
                strSortHeader = "Location"
            Case "fld_LocSubName"
                intSortIndex = 8
                strSortHeader = "Sub Location"
            Case "fld_OwnerName"
                intSortIndex = 9
                strSortHeader = "Assigned Owner"
            Case "fld_AssetStatusStr"
                intSortIndex = 10
                strSortHeader = "Status"
        End Select

        If hdnSortAD.Value = "ASC" Then
            dgAsset.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/asc_select.jpg' border=0>"
        Else
            dgAsset.Columns(intSortIndex).HeaderText = strSortHeader + "&nbsp;&nbsp;<img src='../images/desc_select.jpg' border=0>"
        End If
    End Function

    Private Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        dgAsset.CurrentPageIndex = 0
        dgAsset.PageSize = ddlPageSize.SelectedItem.Value
        fnPopulateRecords()
    End Sub

    Private Sub dgAsset_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgAsset.PageIndexChanged
        dgAsset.CurrentPageIndex = e.NewPageIndex
        fnPopulateRecords()
    End Sub

    Private Sub dgAsset_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgAsset.SortCommand
        If hdnSortName.Value = e.SortExpression Then
            If hdnSortAD.Value = "ASC" Then
                hdnSortAD.Value = "Desc"
            Else
                hdnSortAD.Value = "ASC"
            End If
        Else
            hdnSortName.Value = e.SortExpression
            hdnSortAD.Value = "ASC"
        End If
        fnPopulateRecords()
    End Sub

    Protected Sub butGenExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butGenExcel.Click
        Try
            Dim strAssetIDs2 As String = ""
            Dim strMsg As String = ""

            Dim TblAsset As DataTable = fnCreateDataTableAsset()
            Dim RowAsset As DataRow
            ''Get Selected Asset IDs
            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox
            For Each GridItem In dgAsset.Items
                chkSelectedRec = CType(GridItem.Cells(11).FindControl("DeleteThis"), CheckBox)
                If chkSelectedRec.Checked Then
                    ''Add New Row to Datatable
                    RowAsset = TblAsset.NewRow()        'declaring a new row
                    RowAsset.Item("dt_AssetID") = GridItem.Cells(0).Text
                    TblAsset.Rows.Add(RowAsset)

                    strAssetIDs2 += GridItem.Cells(2).Text & ", "
                End If
            Next
            If strAssetIDs2 <> "" Then
                strAssetIDs2 = Trim(strAssetIDs2)
                strAssetIDs2 = strAssetIDs2.Substring(0, strAssetIDs2.Length - 1)

                If Len(strAssetIDs2) > 7904 Then
                    strAssetIDs2 = strAssetIDs2.Substring(0, 7904) & " ..."
                End If
            End If


            ''*** convert Data Table to XML
            Dim strAssetXML As String = ""
            Dim ds As DataSet
            ds = New DataSet         'creating a dataset
            ds.Tables.Add(TblAsset)   'assign datatable to dataset
            If Not ds Is Nothing Then
                Dim loCol As DataColumn
                'Prepare XML output from the DataSet as a string
                For Each loCol In ds.Tables(0).Columns
                    loCol.ColumnMapping = System.Data.MappingType.Attribute
                Next
                strAssetXML = ds.GetXml
            End If



            If Trim(strAssetXML) <> "" Then
                ''** Pass Asset IDs by Session
                Session("IssGenExcelAssetID") = ""
                Session("IssGenExcelAssetID") = Trim(strAssetXML)

                Dim strJavaScript As String
                strJavaScript = "<script language = 'Javascript'>" & _
                                "window.open('GenExcel_ExcelFile.aspx', 'Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1,menubar=1'); " & _
                                "</script>"
                'strJavaScript = "<script language = 'Javascript'>" & _
                '                "window.open('GenExcel_ExcelFile.aspx?AIDs=" & Trim(strAssetIDs) & "', 'Screen','width=700,height=500,Top=0,left=0,scrollbars=1,resizable=1'); " & _
                '                "</script>"
                Response.Write(strJavaScript)

                ''Insert Audit Trail
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "<font class=DisplayAudit>[Issuance Management - Generate Excel]</font><br> <b>Asset ID</b> : " & strAssetIDs2)
            Else
                strMsg = "At least one record should be selected for generate excel."
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnCreateDataTableAsset() As DataTable
        Try
            ''****1. creating a table named TblAsset
            Dim TblAsset As DataTable
            TblAsset = New DataTable("TblAsset")

            ''Column 1: Asset id
            Dim dt_AssetID As DataColumn = New DataColumn("dt_AssetID")    'declaring a column named Name
            dt_AssetID.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            TblAsset.Columns.Add(dt_AssetID)                               'adding the column to table

            ''return dataTable 
            fnCreateDataTableAsset = TblAsset
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class