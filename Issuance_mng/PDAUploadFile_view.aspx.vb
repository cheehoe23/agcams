#Region "Information Section"
' ****************************************************************************************************
' Description       : Upload PDA OUT-File 
' Purpose           : Upload PDA OUT-File for issuance process
' Author            : See Siew
' Date              : 20/06/2008
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
#End Region

Partial Public Class PDAUploadFile_view
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butUploadFile.Attributes.Add("OnClick", "return chkFrm()")

                ''Check Access Right
                If (InStr(Session("AR"), "IISMng|PDAUploadFile|Upload") = 0) Then
                    FUploadFile.Disabled = True
                    butUploadFile.Enabled = False
                End If

            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butUploadFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUploadFile.Click
        Try
            If FUploadFile.PostedFile.ContentLength <= 0 Then
                lblErrorMessage.Text = "Please browse file first."
                lblErrorMessage.Visible = True
                Exit Sub

            Else

                '' ** Save file in system
                Dim SaveLocation As String = ""
                Dim fn As String = ""
                If Not FUploadFile.PostedFile Is Nothing And FUploadFile.PostedFile.ContentLength > 0 Then
                    fn = System.IO.Path.GetFileName(FUploadFile.PostedFile.FileName)
                    SaveLocation = Server.MapPath("..\tempFile\IssuancePDAFile") & "\" & fn
                    FUploadFile.PostedFile.SaveAs(SaveLocation)
                End If

                '' ** Loop Information in Text File and get information...
                fnLoopTextFile(SaveLocation, fn)
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnLoopTextFile(ByVal SaveLocation As String, ByVal strFileName As String)
        Try
            ''Create Datatable .. Get Datatable
            Dim TblAsset As DataTable = fnCreateDataTableAsset()
            Dim RowAsset As DataRow

            Dim TblAssetMV As DataTable = fnCreateDataTableAssetMV()
            Dim RowAssetMV As DataRow

            ''Loop Record in File
            Dim MyReader As New StreamReader(SaveLocation)
            Dim strline As String = ""
            Dim strType As String = ""  '' A=Tbl Asset, M=Tbl Asset Movement
            Dim arrAsset As Array
            Dim arrAssetMv As Array
            Dim arrFirstLine As Array
            Do Until strline Is Nothing
                strline = MyReader.ReadLine()
                If Not strline Is Nothing And strline <> "" Then

                    ''*** Get Starting point for table
                    If strline = "###TBL_ASSET" Then
                        strType = "A"
                    ElseIf strline = "###TBL_ASSETMOVEMENT" Then
                        strType = "M"
                    Else
                        ''*** Get data
                        If strType = "A" Then
                            arrAsset = Split(strline, "|")

                            If arrAsset(0) <> "0" And arrAsset(5) = "F" Then
                                ''Add New Row to Datatable
                                RowAsset = TblAsset.NewRow()        'declaring a new row
                                RowAsset.Item("dt_AssetID") = arrAsset(0)
                                RowAsset.Item("dt_AMvID") = arrAsset(3)
                                RowAsset.Item("dt_ARemarks") = Trim(arrAsset(4))
                                RowAsset.Item("dt_ScanF") = arrAsset(5)
                                RowAsset.Item("dt_ModifyDt") = arrAsset(6)
                                TblAsset.Rows.Add(RowAsset)
                            End If


                        ElseIf strType = "M" Then
                            arrAssetMv = Split(strline, "|")

                            If arrAssetMv(1) <> "0" And arrAssetMv(0) <> "0" Then
                                ''Add New Row to Datatable
                                RowAssetMV = TblAssetMV.NewRow()        'declaring a new row
                                RowAssetMV.Item("dt_MAssetID") = arrAssetMv(0)
                                RowAssetMV.Item("dt_MvID") = arrAssetMv(1)
                                RowAssetMV.Item("dt_DeptID") = arrAssetMv(2)
                                RowAssetMV.Item("dt_LocID") = arrAssetMv(3)
                                RowAssetMV.Item("dt_LocSubID") = arrAssetMv(4)
                                RowAssetMV.Item("dt_OwnerID") = arrAssetMv(5)
                                RowAssetMV.Item("dt_OwnerN") = arrAssetMv(6)
                                RowAssetMV.Item("dt_CreateDt") = arrAssetMv(7)
                                TblAssetMV.Rows.Add(RowAssetMV)
                            End If

                        Else '' First Line
                            arrFirstLine = Split(strline, "|")
                            If arrFirstLine(0) <> ">>>0" Or arrFirstLine(2) <> "0" Then
                                lblErrorMessage.Text = "Wrong file was uploaded. Please make sure you upload correct file."
                                lblErrorMessage.Visible = True
                                Exit Sub
                            End If
                        End If
                    End If

                End If
            Loop
            MyReader.Close()
            MyReader.Dispose()


            ''** Get Asset in XML
            Dim ds As New DataSet
            Dim strAssetXML As String = ""
            ds = New DataSet         'creating a dataset
            ds.Tables.Add(TblAsset)   'assign datatable to dataset
            If Not ds Is Nothing Then
                Dim loCol As DataColumn
                'Prepare XML output from the DataSet as a string
                For Each loCol In ds.Tables(0).Columns
                    loCol.ColumnMapping = System.Data.MappingType.Attribute
                Next
                strAssetXML = ds.GetXml
            End If

            ''** Get Asset Movement in XML
            Dim dsMv As New DataSet
            Dim strAssetMVXML As String = ""
            dsMv = New DataSet         'creating a dataset
            dsMv.Tables.Add(TblAssetMV)   'assign datatable to dataset
            If Not dsMv Is Nothing Then
                Dim loCol2 As DataColumn
                'Prepare XML output from the DataSet as a string
                For Each loCol2 In dsMv.Tables(0).Columns
                    loCol2.ColumnMapping = System.Data.MappingType.Attribute
                Next
                strAssetMVXML = dsMv.GetXml
            End If

            ''Update Record in DB
            If strAssetXML <> "" And strAssetMVXML <> "" Then
                Dim intRetVal As Integer
                Dim strMsg As String
                Dim intIssNo As Integer = "0"
                intRetVal = clsIssuance.fnIssuancePDAUpload_UploadRecord(strAssetXML, strAssetMVXML, Session("UsrID"), intIssNo)
                If intRetVal > 0 Then
                    strMsg = "Issuance File uploaded successfully "

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Issuance file uploaded : " & strFileName)

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "document.location.href='PDAIssuance_view.aspx?CallFrm=Upload&IssNo=" & CStr(intIssNo) & "';" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    strMsg = "File upload Failed. Please contact your Administrator."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If
            Else
                lblErrorMessage.Text = "Not Issuance record(s) uploaded."
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnCreateDataTableAsset() As DataTable
        Try
            ''****1. creating a table named TblAsset
            Dim TblAsset As DataTable
            TblAsset = New DataTable("TblAsset")

            ''Column 1: Asset id
            Dim dt_AssetID As DataColumn = New DataColumn("dt_AssetID")    'declaring a column named Name
            dt_AssetID.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            TblAsset.Columns.Add(dt_AssetID)                               'adding the column to table


            ''Column 1: Mv id
            Dim dt_AMvID As DataColumn = New DataColumn("dt_AMvID")    'declaring a column named Name
            dt_AMvID.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            TblAsset.Columns.Add(dt_AMvID)

            ''Column 2: Asset Remarks
            Dim dt_ARemarks As DataColumn = New DataColumn("dt_ARemarks")      'declaring a column named Name
            dt_ARemarks.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_ARemarks)                                    'adding the column to table

            ''Column 3: Scan Indicator
            Dim dt_ScanF As DataColumn = New DataColumn("dt_ScanF")      'declaring a column named Name
            dt_ScanF.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_ScanF)                                    'adding the column to table

            ''Column 4: Modify Date
            Dim dt_ModifyDt As DataColumn = New DataColumn("dt_ModifyDt")      'declaring a column named Name
            dt_ModifyDt.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_ModifyDt)                                    'adding the column to table

            ''Set Primary Keys
            '--> Make the ID column the primary key column.
            Dim PrimaryKeyColumns(0) As DataColumn
            PrimaryKeyColumns(0) = TblAsset.Columns("dt_AssetID")
            TblAsset.PrimaryKey = PrimaryKeyColumns

            ''return dataTable 
            fnCreateDataTableAsset = TblAsset
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnCreateDataTableAssetMV() As DataTable
        Try
            ''****1. creating a table named TblAssetMV
            Dim TblAssetMV As DataTable
            TblAssetMV = New DataTable("TblAssetMV")

            ''Column 1: MAsset id
            Dim dt_MAssetID As DataColumn = New DataColumn("dt_MAssetID")    'declaring a column named Name
            dt_MAssetID.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_MAssetID)                               'adding the column to table

            ''Column 2: movement id
            Dim dt_MvID As DataColumn = New DataColumn("dt_MvID")      'declaring a column named Name
            dt_MvID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_MvID)                                    'adding the column to table

            ''Column 2: Dept id
            Dim dt_DeptID As DataColumn = New DataColumn("dt_DeptID")      'declaring a column named Name
            dt_DeptID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_DeptID)                                    'adding the column to table

            ''Column 3: Loc id
            Dim dt_LocID As DataColumn = New DataColumn("dt_LocID")      'declaring a column named Name
            dt_LocID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_LocID)                                    'adding the column to table

            ''Column 3a: Sub Loc id
            Dim dt_LocSubID As DataColumn = New DataColumn("dt_LocSubID")      'declaring a column named Name
            dt_LocSubID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_LocSubID)                                    'adding the column to table

            ''Column 4: owner ID
            Dim dt_OwnerID As DataColumn = New DataColumn("dt_OwnerID")      'declaring a column named Name
            dt_OwnerID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_OwnerID)                                    'adding the column to table

            ''Column 5: owner ID
            Dim dt_OwnerN As DataColumn = New DataColumn("dt_OwnerN")      'declaring a column named Name
            dt_OwnerN.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_OwnerN)                                    'adding the column to table

            ''Column 6: Created Date 
            Dim dt_CreateDt As DataColumn = New DataColumn("dt_CreateDt")      'declaring a column named Name
            dt_CreateDt.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_CreateDt)                                    'adding the column to table

            ''Column 7: Created By 
            Dim dt_CreateBy As DataColumn = New DataColumn("dt_CreateBy")      'declaring a column named Name
            dt_CreateBy.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAssetMV.Columns.Add(dt_CreateBy)                                    'adding the column to table

            ''Set Primary Keys
            '--> Make the ID column the primary key column.
            Dim PrimaryKeyColumns(0) As DataColumn
            'PrimaryKeyColumns(0) = TblAssetMV.Columns("dt_MAssetID")   
            PrimaryKeyColumns(0) = TblAssetMV.Columns("dt_MvID")
            TblAssetMV.PrimaryKey = PrimaryKeyColumns

            ''return dataTable 
            fnCreateDataTableAssetMV = TblAssetMV

        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class