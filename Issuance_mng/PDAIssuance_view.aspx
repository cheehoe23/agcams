<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDAIssuance_view.aspx.vb" Inherits="AMS.PDAIssuance_view" %>
<%@ Register TagPrefix="tagFooter" TagName="footer" src="../common/footer_cr.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AMS</title>
    <link href="../common/CommonStyle.css" type="text/css" rel="Stylesheet">
	<script language="JavaScript" src="../Common/CommonJS.js"></script>
	<script type="text/javascript">
	    function chkFrm(callFrm) {
			var foundError = false;
			            			
			//at least 1 asset record selected for Accept Changes/deleted
			if (!foundError && !gfnCheckSelect("At least one record should be selected.")){
				foundError=true
			}
											 
 			if (!foundError){
 			    var flag = false;
 			    if (callFrm == 'C') {
 			        flag = window.confirm("Are you sure you want to update selected Asset(s)?");
 			    }
 			    else {
 			        flag = window.confirm("Are you sure you want to delete selected Asset(s)?");
 			    }
 			     				
 				return flag;
 			}
			else
				return false;
		}
	</script>
</head>
<body>
   <form id="Frm_PDAIss" method="post" runat="server">
			<!-- Start: header -->
			<table id="Table8" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="white"
				border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr height="10">
					<td vAlign="top" background="../images/leftmid.gif">&nbsp;</td>
					<td vAlign="top" width="100%">
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><B>Issuance Management : PDA Issuance</B></td>
								<td align="right"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- End  : header -->
			<table id="Table2" height="400" cellSpacing="0" cellPadding="0" width="100%" align="center"
				bgColor="white" border="0">
				<tr height="12">
					<td vAlign="top"><IMG src="../images/ltop.jpg"></td>
					<td width="100%" background="../images/topmid.gif"></td>
					<td vAlign="top"><IMG src="../images/rtop.jpg"></td>
				</tr>
				<tr>
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td align="center"><asp:label id="lblErrorMessage" runat="server" cssclass="LabelErrorText"></asp:label></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="100%">
					<td vAlign="top" background="../images/leftmid.gif"></td>
					<td vAlign="top" width="100%">
						<!--Start Main Content-->
						<table height="400" cellSpacing="1" cellPadding="0" width="100%" align="left" border="0">
							<tr>
								<td vAlign="top" align="left">
									<asp:button id="butAccChange" Runat="server" Text="Accept Changes"></asp:button>
									<asp:button id="butDelRec" Runat="server" Text="Delete Record"></asp:button>
								</td>
							</tr>
							<tr>
								<td vAlign="top" align="center" height="400">
									<!--Datagrid for display record.-->
									<asp:datagrid id="dgIssRec" Runat="server" AlternatingItemStyle-Height="25"
                                        ItemStyle-Height="25" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
                                        BorderColor="#ffffff" datakeyfield="fld_IssDetailID" AlternatingItemStyle-BackColor="#e3d9ee">
                                        <headerstyle verticalalign="Middle" BackColor="#a3a9cc" ForeColor="White" Font-Bold="True" HorizontalAlign="Center"
                                            Height="25"></headerstyle>
                                        <Columns>
                                            <asp:boundcolumn HeaderText="fld_IssDetailID" datafield="fld_IssDetailID" Visible=false>
                                                <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn HeaderText="fld_IssAssetID" datafield="fld_IssAssetID" Visible=false>
                                                <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn HeaderText="org_DeptID" datafield="org_DeptID" Visible=false>
                                                <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn HeaderText="fld_IssDeptID" datafield="fld_IssDeptID" Visible=false>
                                                <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn HeaderText="fld_IssLocID" datafield="fld_IssLocID" Visible=false>
                                                <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn HeaderText="fld_IssOwnerID" datafield="fld_IssOwnerID" Visible=false>
                                                <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn HeaderText="fld_IssOwnerName" datafield="fld_IssOwnerName" Visible=false>
                                                <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn HeaderText="fld_IssRemarks" datafield="fld_IssRemarks" Visible=false>
                                                <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                            </asp:boundcolumn>
                                            
                                            <asp:boundcolumn datafield="SNum" headertext="S/No" ItemStyle-Height="10">
                                                <itemstyle width="5%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn datafield="fld_AssetIDAMS" headertext="Asset ID" ItemStyle-Height="10">
                                                <itemstyle width="15%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:boundcolumn headertext="Type" datafield="AssetType" >
                                                <itemstyle width="10%" cssclass="GridText" verticalalign="Middle" HorizontalAlign=Left></itemstyle>
                                            </asp:boundcolumn>
                                            <asp:templatecolumn HeaderText="Current Location Information" ItemStyle-Width="50%" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign=Left>
												<itemtemplate>
													<%#fnShowLocDetail(DataBinder.Eval(Container.DataItem, "org_DeptName"), DataBinder.Eval(Container.DataItem, "org_LocName"), DataBinder.Eval(Container.DataItem, "org_LocSubName"), DataBinder.Eval(Container.DataItem, "org_OwnerName"))%>
												</itemtemplate>
											</asp:templatecolumn>
											<asp:TemplateColumn HeaderText="(Issuance Result) Department">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlDept" runat="server"></asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="(Issuance Result) Location">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlLocation" runat="server" Width="100px" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="(Issuance Result) Sub Location">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlLocSub" runat="server" Width="100px"></asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="(Issuance Result) Assigned Owner">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlOwner" runat="server"></asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="(Issuance Result) Remarks">
                                                <ItemTemplate>
                                                    <asp:TextBox id="txtRemark" TextMode=MultiLine maxlength="1000" Columns="40" Rows="5" Runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            
                                            <asp:templatecolumn ItemStyle-Height="10" ItemStyle-Width="5%">
                                                <headertemplate>
                                                    <asp:checkbox id="CheckAll" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
                                                        runat="server" />
                                                </headertemplate>
                                                <itemtemplate>
                                                    <center>
                                                        <asp:checkbox id="DeleteThis" onclick="javascript: return gfnSelect_deselectAll (this.checked, this.id);"
                                                            runat="server" />
                                                    </center>
                                                </itemtemplate>
                                            </asp:templatecolumn>
                                            <asp:boundcolumn HeaderText="fld_IssLocSubID" datafield="fld_IssLocSubID" Visible=false>
                                                <itemstyle width="10%" cssclass="GridText" verticalalign="Middle"></itemstyle>
                                            </asp:boundcolumn>
                                        </Columns>
                                    </asp:datagrid>
								</td>
							</tr>
						</table>
						<!--End Main Content--></td>
					<td vAlign="top" background="../images/bgr.jpg"></td>
				</tr>
				<tr height="14">
					<td vAlign="bottom"><IMG src="../images/lBottom.jpg"></td>
					<td width="100%" background="../images/bgB.jpg"></td>
					<td vAlign="top"><IMG src="../images/rbottom.jpg"></td>
				</tr>
			</table>
			<!-- Start: Hidden Fields -->
			<input id="hdnIssNo" type="hidden" name="hdnIssNo" runat="server"> 
			<input id="hdnCallFrm" type="hidden" name="hdnCallFrm" runat="server">
			<!-- End  : Hidden Fields -->
			<tagFooter:footer id="Footer" runat="server" NAME="Footer"></tagFooter:footer>
		</form>
</body>
</html>
