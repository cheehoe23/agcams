'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.832
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On



'''<summary>
'''GenExcel_View class.
'''</summary>
'''<remarks>
'''Auto-generated class.
'''</remarks>
Partial Public Class GenExcel_View

    '''<summary>
    '''Frm_AssetView control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Frm_AssetView As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''lblErrorMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblErrorMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''butGenExcel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents butGenExcel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''ddlPageSize control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPageSize As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''dgAsset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dgAsset As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''hdnSortName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnSortName As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnSortAD control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnSortAD As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnAssetIdAMS control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnAssetIdAMS As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnAssetIdNFS control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnAssetIdNFS As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnAssetType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnAssetType As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnAssetCatID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnAssetCatID As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnAssetCatSubID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnAssetCatSubID As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnAssetStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnAssetStatus As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnPurchaseFDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnPurchaseFDt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnPurchaseTDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnPurchaseTDt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnWarExpFDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnWarExpFDt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnWarExpTDt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnWarExpTDt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnDeptID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnDeptID As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnLocID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnLocID As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnLocSubID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnLocSubID As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnOwnerID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnOwnerID As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnTempAsset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnTempAsset As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnCtrlItemF control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnCtrlItemF As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Footer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Footer As Global.AMS.footer_cr
End Class
