#Region "Information Section"
' ****************************************************************************************************
' Description       : Stocktake Details
' Purpose           : Stocktake Details
' Author            : See Siew
' Date              : 12/11/2007
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
#End Region

Partial Public Class UploadExcel
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butUpdateAsset.Attributes.Add("OnClick", "return chkFrm()")
                Me.butUploadResult.Attributes.Add("OnClick", "return fnViewExcelFileResult()")

                divUpload.Visible = True
                divUpdateResult.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butUpdateAsset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUpdateAsset.Click
        Try
            
            If FUploadFile.PostedFile.ContentLength <= 0 Then
                lblErrorMessage.Text = "Please browse file first."
                lblErrorMessage.Visible = True
                Exit Sub

            ElseIf System.IO.Path.GetExtension(FUploadFile.PostedFile.FileName).ToUpper <> ".XLS" Then
                lblErrorMessage.Text = "Please upload correct file. Just allow to upload excel file."
                lblErrorMessage.Visible = True
                Exit Sub

            Else

                '' ** Save file in system
                Dim SaveLocation As String = ""
                Dim fn As String = ""
                If Not FUploadFile.PostedFile Is Nothing And FUploadFile.PostedFile.ContentLength > 0 Then
                    fn = System.IO.Path.GetFileName(FUploadFile.PostedFile.FileName)
                    SaveLocation = Server.MapPath("..\tempFile\IssuanceExcelFile") & "\" & fn
                    FUploadFile.PostedFile.SaveAs(SaveLocation)
                End If

                '' ** Loop Information in Text File and get information...
                Dim strAssetIDs As String = ""
                fnLoopFile(SaveLocation, strAssetIDs)

                '' ** Insert Audit Trail
                If Len(strAssetIDs) > 8000 Then
                    strAssetIDs = strAssetIDs.Substring(0, 7904) & " ..."
                End If
                clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "<font class=DisplayAudit>[Issuance Management - Upload Excel]</font><br><b>Asset ID</b> : " + strAssetIDs)
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnLoopFile(ByVal SaveLocation As String, ByRef strAssetIDs As String)
        Try
            Dim connstring As String = ""
            Dim dr As DataRow
            Dim olecon As OleDb.OleDbConnection
            Dim olecomm As OleDb.OleDbCommand
            Dim oleadpt As OleDb.OleDbDataAdapter
            Dim ds As DataSet
            Dim x As Integer
            Dim rowcount As Integer

            Dim strAssetID As String = ""
            Dim strDesc As String = ""
            Dim strDept As String = ""
            Dim strLoc As String = ""
            Dim strLocSub As String = ""
            Dim strOwner As String = ""
            Dim strSubCat As String = ""
            Dim strRemarks As String = ""
            Dim strUpdatedF As String = ""
            Dim strReasonFailed As String = ""

            ''Create Datatable .. Get Datatable
            Dim TblAsset As DataTable = fnCreateDataTableAsset()
            Dim RowAsset As DataRow

            If File.Exists(SaveLocation) Then
                connstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & SaveLocation & ";Extended Properties=""Excel 8.0;HDR=YES;IMEX=1"""
                'connstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & SaveLocation & ";Extended Properties=""HTML Import;"""

                olecon = New OleDb.OleDbConnection
                olecon.ConnectionString = connstring
                olecomm = New OleDb.OleDbCommand
                olecomm.CommandText = "Select * from [IssMngExcelFile$]" '"Select * from [Sheet1$]"    
                olecomm.Connection = olecon
                oleadpt = New OleDb.OleDbDataAdapter(olecomm)
                ds = New DataSet

                'olecon.Open()
                oleadpt.Fill(ds) 'oleadpt.Fill(ds, "Sheet1")
                rowcount = ds.Tables(0).Rows.Count

                ''***start : checking whether user upload the correct file format (according to HEADER)
                ''***-> If incorrect file format, prompt error
                Dim strCorrectFileF As String = "Y"
                Dim intColumnCount As Integer = ds.Tables(0).Columns.Count
                If intColumnCount = "8" Then
                    Dim strColumnHeader1 As String = ds.Tables(0).Columns(0).ColumnName
                    Dim strColumnHeader2 As String = ds.Tables(0).Columns(1).ColumnName
                    Dim strColumnHeader3 As String = ds.Tables(0).Columns(2).ColumnName
                    Dim strColumnHeader4 As String = ds.Tables(0).Columns(3).ColumnName
                    Dim strColumnHeader5 As String = ds.Tables(0).Columns(4).ColumnName
                    Dim strColumnHeader6 As String = ds.Tables(0).Columns(5).ColumnName
                    Dim strColumnHeader7 As String = ds.Tables(0).Columns(6).ColumnName
                    Dim strColumnHeader8 As String = ds.Tables(0).Columns(7).ColumnName
                    If Not (strColumnHeader1 = "Asset ID" And strColumnHeader2 = "Description" And _
                            strColumnHeader3 = "Department" And strColumnHeader4 = "Location" And _
                            strColumnHeader5 = "Sub Location" And _
                            strColumnHeader6 = "Assigned Owner" And strColumnHeader7 = "Sub Category Name" And _
                            strColumnHeader8 = "Remarks") Then
                        strCorrectFileF = "N"
                    End If
                Else
                    strCorrectFileF = "N"
                End If

                If strCorrectFileF = "N" Then
                    lblErrorMessage.Text = "Incorrect File Format. Please upload correct excel file."
                    lblErrorMessage.Visible = True
                    Exit Sub
                End If
                ''***End   : checking whether user upload the correct file format (according to HEADER)

                For x = 0 To rowcount - 1
                    dr = ds.Tables(0).Rows(x)
                    strAssetID = Trim(IIf(IsDBNull(dr.Item(0)), "", dr.Item(0)))
                    strDesc = Trim(IIf(IsDBNull(dr.Item(1)), "", dr.Item(1)))
                    strDept = Trim(IIf(IsDBNull(dr.Item(2)), "", dr.Item(2)))
                    strLoc = Trim(IIf(IsDBNull(dr.Item(3)), "", dr.Item(3)))
                    strLocSub = Trim(IIf(IsDBNull(dr.Item(4)), "", dr.Item(4)))
                    strOwner = Trim(IIf(IsDBNull(dr.Item(5)), "", dr.Item(5)))
                    strSubCat = Trim(IIf(IsDBNull(dr.Item(6)), "", dr.Item(6)))
                    strRemarks = Trim(IIf(IsDBNull(dr.Item(7)), "", dr.Item(7)))
                    strUpdatedF = "S"
                    strReasonFailed = ""

                    ''*** ignore when Asset ID is blank
                    If strAssetID <> "" Then
                        ''Get Asset IDs
                        If strAssetIDs <> "" Then
                            strAssetIDs = strAssetIDs & ", " & strAssetID
                        Else
                            strAssetIDs = strAssetID
                        End If


                        ''Update Asset 
                        fnUpdateAsset(strAssetID, strDesc, strDept, strLoc, strLocSub, strOwner, strSubCat, strRemarks, strUpdatedF, strReasonFailed)

                        ''Add New Row to Datatable
                        RowAsset = TblAsset.NewRow()        'declaring a new row
                        RowAsset.Item("dt_AssetID") = strAssetID
                        RowAsset.Item("dt_Desc") = strDesc 'Replace(strDesc, Chr(10), "<br>") 'Replace(strDesc, vbCrLf, "<br>")
                        RowAsset.Item("dt_Dept") = strDept
                        RowAsset.Item("dt_Loc") = strLoc
                        RowAsset.Item("dt_LocSub") = strLocSub
                        RowAsset.Item("dt_Owner") = strOwner
                        RowAsset.Item("dt_SubCat") = strSubCat
                        RowAsset.Item("dt_Remarks") = strRemarks
                        RowAsset.Item("dt_UpdatedF") = strUpdatedF ' IIf(strUpdatedF = "F", "Failed", "Successfully")
                        RowAsset.Item("dt_ReasonFail") = strReasonFailed
                        TblAsset.Rows.Add(RowAsset)
                    End If

                Next

                ''*** convert Data Table to XML
                Dim strAssetXML As String = ""
                ds = New DataSet         'creating a dataset
                ds.Tables.Add(TblAsset)   'assign datatable to dataset
                If Not ds Is Nothing Then
                    Dim loCol As DataColumn
                    'Prepare XML output from the DataSet as a string
                    For Each loCol In ds.Tables(0).Columns
                        loCol.ColumnMapping = System.Data.MappingType.Attribute
                    Next
                    strAssetXML = ds.GetXml
                End If

                If Trim(strAssetXML) <> "" Then
                    ''*** Call SP to Update Asset , Diaplay Updated Result
                    Dim dsIssUploadExcel As New DataSet
                    dsIssUploadExcel = clsIssuance.fnIssuanceUploadExcel_UpdateAllAsset(strAssetXML, Session("UsrID"))
                    dgAsset.DataSource = dsIssUploadExcel
                    dgAsset.DataBind()
                    If Not dgAsset.Items.Count > 0 Then
                        lblErrorMessage.Text = "Asset Updated Fail. Please check your file format."
                        lblErrorMessage.Visible = True
                    Else
                        ''Set Session to blank
                        Session("dsIssUploadExcel") = ""
                        Session("dsIssUploadExcel") = dsIssUploadExcel

                        divUpload.Visible = False
                        divUpdateResult.Visible = True
                    End If

                Else
                    lblErrorMessage.Text = "Asset Updated Fail. Please check your file format."
                    lblErrorMessage.Visible = True
                End If

            Else
                lblErrorMessage.Text = "File not exist."
                lblErrorMessage.Visible = True
            End If

            'olecon.Close()
            olecon = Nothing
            olecomm = Nothing
            oleadpt = Nothing
            dr = Nothing

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fnUpdateAsset( _
                ByVal strAssetID As String, ByVal strDesc As String, ByVal strDept As String, _
                ByVal strLoc As String, ByVal strLocSub As String, ByVal strOwner As String, _
                ByVal strSubCat As String, ByVal strRemarks As String, _
                ByRef strUpdatedF As String, ByRef strReasonFailed As String)
        Try
            ''*** Check whether Asset ID is correct ***
            If Len(strAssetID) <> "13" Then
                strUpdatedF = "F"
            ElseIf strAssetID.Substring(2, 1) <> "-" Then
                strUpdatedF = "F"
            ElseIf Not IsNumeric(strAssetID.Substring(3, 10)) Then
                strUpdatedF = "F"
            End If
            If strUpdatedF = "F" Then strReasonFailed = "Invalid Asset ID."

            ''*** Check whether Description, Department, Location, Assigned Owner, SubCategory, Remarks is blank ***
            If strUpdatedF <> "F" Then
                If strDesc = "" And strDept = "" And strLoc = "" And strLocSub = "" And strOwner = "" And strSubCat = "" And strRemarks = "" Then
                    strUpdatedF = "F"
                    strReasonFailed = "Description, Department, Location, Sub Location, Assigned Owner, Sub Category and Remarks are blank."
                ElseIf strLoc = "" And strLocSub <> "" Then
                    strUpdatedF = "F"
                    strReasonFailed = "Location is blank."
                ElseIf strLoc <> "" And strLocSub = "" Then
                    strUpdatedF = "F"
                    strReasonFailed = "Sub Location is blank."
                ElseIf strDept = "" And (strLoc <> "" Or strLocSub <> "" Or strOwner <> "") Then
                    strUpdatedF = "F"
                    strReasonFailed = "Department is blank."
                ElseIf Len(strDept) > 10 Then
                    strUpdatedF = "F"
                    strReasonFailed = "Invalid Department Code"
                ElseIf Len(strLoc) > 10 Then
                    strUpdatedF = "F"
                    strReasonFailed = "Invalid Location Code"
                ElseIf Len(strLocSub) > 10 Then
                    strUpdatedF = "F"
                    strReasonFailed = "Invalid Sub Location Code"
                ElseIf Len(strSubCat) > 250 Then
                    strUpdatedF = "F"
                    strReasonFailed = "Invalid Sub Category Name"
                End If
            End If

            ''*** Update Asset record
            'If strUpdatedF <> "F" Then
            '    clsIssuance.fnIssuanceUploadExcel_UpdateAsset(strAssetID, strDesc, strDept, strLoc, strOwner, _
            '                                                  strUpdatedF, strReasonFailed, Session("UsrID"))
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function fnCreateDataTableAsset() As DataTable
        Try
            ''****1. creating a table named TblAsset
            Dim TblAsset As DataTable
            TblAsset = New DataTable("TblAsset")

            ''Column 1: Asset id
            Dim dt_AssetID As DataColumn = New DataColumn("dt_AssetID")    'declaring a column named Name
            dt_AssetID.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            TblAsset.Columns.Add(dt_AssetID)                               'adding the column to table

            ''Column 2: Description
            Dim dt_Desc As DataColumn = New DataColumn("dt_Desc")    'declaring a column named Name
            dt_Desc.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            TblAsset.Columns.Add(dt_Desc)

            ''Column 3: Department
            Dim dt_Dept As DataColumn = New DataColumn("dt_Dept")      'declaring a column named Name
            dt_Dept.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_Dept)                                    'adding the column to table

            ''Column 4: Location
            Dim dt_Loc As DataColumn = New DataColumn("dt_Loc")      'declaring a column named Name
            dt_Loc.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_Loc)                                    'adding the column to table

            ''Column 4: Sub Location
            Dim dt_LocSub As DataColumn = New DataColumn("dt_LocSub")      'declaring a column named Name
            dt_LocSub.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_LocSub)                                    'adding the column to table

            ''Column 5: Assigned Owner
            Dim dt_Owner As DataColumn = New DataColumn("dt_Owner")      'declaring a column named Name
            dt_Owner.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_Owner)                                    'adding the column to table

            ''Column 5: Sub Category
            Dim dt_SubCat As DataColumn = New DataColumn("dt_SubCat")      'declaring a column named Name
            dt_SubCat.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_SubCat)                                    'adding the column to table

            ''Column 5: Remarks
            Dim dt_Remarks As DataColumn = New DataColumn("dt_Remarks")      'declaring a column named Name
            dt_Remarks.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_Remarks)                                    'adding the column to table

            ''Column 6: Updated Flag
            Dim dt_UpdatedF As DataColumn = New DataColumn("dt_UpdatedF")      'declaring a column named Name
            dt_UpdatedF.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_UpdatedF)                                    'adding the column to table

            ''Column 7: Reason for Updated Fail
            Dim dt_ReasonFail As DataColumn = New DataColumn("dt_ReasonFail")      'declaring a column named Name
            dt_ReasonFail.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_ReasonFail)                                    'adding the column to table


            ''Set Primary Keys
            '--> Make the ID column the primary key column.
            Dim PrimaryKeyColumns(0) As DataColumn
            PrimaryKeyColumns(0) = TblAsset.Columns("dt_AssetID")
            TblAsset.PrimaryKey = PrimaryKeyColumns

            ''return dataTable 
            fnCreateDataTableAsset = TblAsset
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class