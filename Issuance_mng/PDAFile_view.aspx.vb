#Region "Information Section"
' ****************************************************************************************************
' Description       : Generate PDA IN-File 
' Purpose           : Generate PDA IN-File for issuance process
' Author            : See Siew
' Date              : 20/06/2008
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
#End Region

Partial Public Class PDAFile_view
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                divGenFile.Visible = True
                divSaveFile.Visible = False
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub butGenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butGenFile.Click
        Try
            Dim strFileName4PDA As String = ""
            Dim FileFullPath As String = ""
            Dim FileFullPathForLink As String = ""

            ''*** Get File Name
            strFileName4PDA = "IN_" & Year(Date.Today).ToString & Month(Date.Today).ToString.PadLeft(2, "0") & Day(Date.Today).ToString.PadLeft(2, "0") & Hour(Date.Now).ToString.PadLeft(2, "0") & Minute(Date.Now).ToString.PadLeft(2, "0") & Second(Date.Now).ToString.PadLeft(2, "0") & ".txt"

            ''*** Get Dile Path 
            FileFullPath = Server.MapPath("..\tempFile\IssuancePDAFile") & "\" & strFileName4PDA
            FileFullPathForLink = "..\tempFile\IssuancePDAFile" & "\" & strFileName4PDA

            ''*** Generate File for PDA
            fnGenerateFile4PDA(FileFullPath)

            ''*** Display Save Text File Part
            divGenFile.Visible = False
            divSaveFile.Visible = True
            hylFileName.Text = strFileName4PDA
            hylFileName.NavigateUrl = FileFullPathForLink

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Protected Sub fnGenerateFile4PDA(ByVal FileFullPath As String)
        Try
            '********** Beginning write text file ***********
            Dim MyWriter As New StreamWriter(FileFullPath, False)
            Dim textstring As String = ""
            Dim objRdr As SqlDataReader

            ''0.**** Details for Stocktake
            textstring = ">>>" & "0" & "|" & Session("LoginID") & "|" & "0"
            MyWriter.WriteLine(textstring)

            ''1.**** Get User From AD
            ''--> a. Header 
            textstring = "###TBL_USERFROMAD"
            MyWriter.WriteLine(textstring)

            ''--> b. Data
            objRdr = ClsUser.fnUsrGetUsrFromADDR("PU", "0", "", "0")
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        textstring = CStr(objRdr("fld_ADLoginID")) & "|"
                        textstring = textstring + fnReplaceNotAllowVal(CStr(objRdr("fld_ADUsrName")))
                        MyWriter.WriteLine(textstring)
                    End While
                End If
            End If
            objRdr.Close()


            ''2.**** Get Department
            ''--> a. Header 
            textstring = "###TBL_DEPARTMENT"
            MyWriter.WriteLine(textstring)

            ''--> b. Data
            textstring = "0" & "|" & "Issuance" & "|" & "Issuance"
            MyWriter.WriteLine(textstring)

            ''3.**** Get Location
            ''--> a. Header 
            textstring = "###TBL_LOCATION"
            MyWriter.WriteLine(textstring)

            ''--> b. Data
            objRdr = clsLocation.fnLocationGetAllRecForDDL()
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        textstring = CStr(objRdr("fld_LocationID")) & "|"
                        textstring = textstring + fnReplaceNotAllowVal(CStr(objRdr("fld_LocationCode"))) & "|"
                        textstring = textstring + fnReplaceNotAllowVal(CStr(objRdr("fld_LocationName")))
                        MyWriter.WriteLine(textstring)
                    End While
                End If
            End If
            objRdr.Close()

            ''3a.**** Get Sub Location
            ''--> a. Header 
            textstring = "###TBL_LOCATIONSUB"
            MyWriter.WriteLine(textstring)

            ''--> b. Data
            objRdr = clsLocation.fnLocation_GetLocation("0", "0", "S") 'clsLocation.fnLocationGetAllRecForDDL()
            If Not objRdr Is Nothing Then
                If objRdr.HasRows Then
                    While objRdr.Read()
                        textstring = CStr(objRdr("fld_LocSubID")) & "|"
                        textstring = textstring + CStr(objRdr("fld_LocationID")) & "|"
                        textstring = textstring + fnReplaceNotAllowVal(CStr(objRdr("fld_LocSubCode"))) & "|"
                        textstring = textstring + fnReplaceNotAllowVal(CStr(objRdr("fld_LocSubName")))
                        MyWriter.WriteLine(textstring)
                    End While
                End If
            End If
            objRdr.Close()

            ''4.**** Get Asset
            ''--> a. Header 
            textstring = "###TBL_ASSET"
            MyWriter.WriteLine(textstring)

            ''--> b. Data
            textstring = "0" & "|" & "0" & "|" & "" & "|" & "" & "|" & "0" & "|" & ""
            MyWriter.WriteLine(textstring)


            ''5.**** Get Asset Movement
            ''--> a. Header 
            textstring = "###TBL_ASSETMOVEMENT"
            MyWriter.WriteLine(textstring)

            ''--> b. Data
            textstring = "0" & "|" & "0" & "|" & "0" & "|" & "0" & "|" & "" & "|" & "" & "|" & Now() & "|" & "1"
            MyWriter.WriteLine(textstring)

            MyWriter.Close()
            MyWriter = Nothing
            '********** Ending write text file ***********
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub butBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBack.Click
        Try
            divGenFile.Visible = True
            divSaveFile.Visible = False
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Function fnReplaceNotAllowVal(ByVal strDataVal As String) As String
        Try
            Dim strRetVal As String = ""
            strRetVal = Replace(Replace(strDataVal, "'", "''"), "|", "")

            fnReplaceNotAllowVal = strRetVal
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class