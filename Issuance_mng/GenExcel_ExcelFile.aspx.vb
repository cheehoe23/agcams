Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Web.HttpResponse


Partial Public Class GenExcel_ExcelFile
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim strAssetIDs As String = Session("IssGenExcelAssetID") 'Request("AIDs")
            Session.Remove("IssGenExcelAssetID")

            ''Bind record in datagrid
            dgAsset.DataSource = clsIssuance.fnIssuanceGenExcel_GetAssetRec(strAssetIDs)
            dgAsset.DataBind()

            ''Populate Report into Excel
            Response.ContentType = "application/ms-excel"
            Response.AddHeader("Content-Disposition", "inline;filename=IssMngExcelFile.xls")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class