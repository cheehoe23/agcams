#Region "Information Section"
' ****************************************************************************************************
' Description       : Search Asset 
' Purpose           : Search Asset Information
' Date              : 15/02/2008
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
#End Region

Partial Public Class GenExcel_search
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            '********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            '********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            ''Get Owner (in-case page load again)
            'cbOwner.DataSource = fnGetUserList(cbOwner.List.PageSize, "", "")

            If Not Page.IsPostBack Then
                Me.butSearch.Attributes.Add("OnClick", "return chkFrm()")

                ''*** Populate Control
                fnPopulateCtrl()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnPopulateCtrl()
        Try
            ''Get Asset Type --> Fixed Asset, Inventory
            fnPopulateAssetTypeInDDL(ddlAssetType)

            ''Get Category
            fnPopulateDropDownList(clsCategory.fnCategoryGetCategory(), ddlAssetCat, "fld_CategoryID", "fld_CatCodeName", False)

            ''Get SubCategory (set default --> -1)
            ddlAssetSubCat.Items.Insert(0, New ListItem("", "-1"))

            ''Get Controlled Item --> Yes, No
            fnPopulateYesNoInDDL(ddlCtrlItem)

            ''Get Asset Status --> Active, InActive, Condemation, Dispose
            fnPopulateAssetStatusInDDL(ddlStatus)

            ''Get Department
            fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), Session("AdminF")), ddlDepartment, "fld_DepartmentID", "fld_DptCN", False)
            If Session("AdminF") = "N" Then ddlDepartment.Items.Remove(ddlDepartment.Items.FindByValue("-1"))

            ''Get Location
            fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLocation, "fld_LocationID", "fld_LoctCodeName", False)

            ''Get Sub Location 
            ddlLocation_SelectedIndexChanged(Nothing, Nothing)

            ' ''Get Owner
            'cbOwner.DataSource = fnGetUserList(cbOwner.List.PageSize, "", "")
            'cbOwner.DataBind()

            ''Get Temporary Asset Type --> Yes, No
            fnPopulateYesNoInDDL(ddlTempAsset)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Function fnGetUserList(ByVal intPageSize As Integer, ByVal strSearchName As String, ByVal strLastString As String) As DataSet
        Try
            Dim dsUser As New DataSet()
            dsUser = ClsUser.fnUsrGetUsrFromAD("OU", intPageSize, strSearchName, strLastString)
            fnGetUserList = dsUser
            Return dsUser
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    'Private Sub cbOwner_GetPage(ByVal sender As Object, ByVal e As Nitobi.ComboGetPageEventArgs) Handles cbOwner.GetPage
    '    Try
    '        e.NextPage = fnGetUserList(e.PageSize, e.SearchSubstring, e.LastString)
    '    Catch ex As Exception
    '        lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
    '        lblErrorMessage.Visible = True
    '    End Try
    'End Sub

    Private Sub ddlAssetCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAssetCat.SelectedIndexChanged
        Try
            If ddlAssetCat.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsCategory.fnCategoryGetAllRecDR(ddlAssetCat.SelectedValue, 0, "fld_CatSubName", "ASC"), ddlAssetSubCat, "fld_CatSubID", "fld_CatSubName", False)
            Else
                ddlAssetSubCat.Items.Clear()
                ddlAssetSubCat.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butReset.Click
        Server.Transfer("GenExcel_search.aspx")
    End Sub

    Private Sub butSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSearch.Click
        Try
            Dim dsAsset As New DataSet
            Dim strURL As String = ""

            Dim strAssetIDAMS As String = Trim(txtAssetIDAMS.Text)
            Dim strAssetIDNFS As String = Trim(txtAssetIDNFS.Text)
            Dim strAssetType As String = ddlAssetType.SelectedValue
            Dim intAssetCatID As Integer = IIf(ddlAssetCat.SelectedValue = "-1", "0", ddlAssetCat.SelectedValue)
            Dim intAssetCatSubID As Integer = IIf(ddlAssetSubCat.SelectedValue = "-1", "0", ddlAssetSubCat.SelectedValue)
            Dim strCtrlItemF As String = ddlCtrlItem.SelectedValue
            Dim strAssetStatus As String = ddlStatus.SelectedValue
            Dim strPurchaseFDt As String = txtPurchaseFrmDt.Text
            Dim strPurchaseTDt As String = txtPurchaseToDt.Text
            Dim strWarExpFDt As String = txtWarExpFrmDt.Text
            Dim strWarExpTDt As String = txtWarExpToDt.Text
            Dim intDeptID As Integer = IIf(ddlDepartment.SelectedValue = "-1", "0", ddlDepartment.SelectedValue)
            Dim intLocID As Integer = IIf(ddlLocation.SelectedValue = "-1", "0", ddlLocation.SelectedValue)
            Dim intLocSubID As Integer = IIf(ddlLocSub.SelectedValue = "-1", "0", ddlLocSub.SelectedValue)
            Dim strOwnerID As String = "" '= IIf(ddlOwner.SelectedValue = "-1", "", ddlOwner.SelectedValue)
            Dim strTempAsset As String = ddlTempAsset.SelectedValue
            Dim StrSortName As String = "fldAssetBarcode"
            Dim strSortOrder As String = "ASC"

            ''*** Get Owner
            'If Trim(cbOwner.TextValue) <> "" Then
            '    Dim strUsrExistF As String = "N"
            '    ClsUser.fnUsr_CheckUserExistAD(Trim(cbOwner.TextValue), strOwnerID, strUsrExistF)
            '    If strUsrExistF = "N" Then
            '        lblErrorMessage.Text = "Owner not found. Please select an existing Owner."
            '        lblErrorMessage.Visible = True
            '        Exit Sub
            '    End If
            'End If

            dsAsset = clsIssuance.fnIssuanceGenExcel_SearchRecord( _
                         strAssetIDAMS, strAssetIDNFS, _
                         strAssetType, intAssetCatID, intAssetCatSubID, _
                         strAssetStatus, strPurchaseFDt, strPurchaseTDt, _
                         strWarExpFDt, strWarExpTDt, _
                         intDeptID, intLocID, strOwnerID, _
                         strTempAsset, StrSortName, strSortOrder, intLocSubID, strCtrlItemF)


            If dsAsset.Tables(0).Rows.Count > 0 Then
                strURL = "GenExcel_View.aspx?AIdAMS=" + strAssetIDAMS + "&AIdNFS=" + strAssetIDNFS + _
                         "&AType=" + strAssetType + "&CatID=" + CStr(intAssetCatID) + "&CatSID=" + CStr(intAssetCatSubID) + _
                         "&Astatus=" + strAssetStatus + "&PFDt=" + strPurchaseFDt + "&PTDt=" + strPurchaseTDt + _
                         "&WFDt=" + strWarExpFDt + "&WTDt=" + strWarExpTDt + _
                         "&DeptID=" + CStr(intDeptID) + "&LocID=" + CStr(intLocID) + "&OwnerID=" + strOwnerID + _
                         "&TempAsset=" + strTempAsset + "&SLocID=" + CStr(intLocSubID) + "&CtrlItemF=" + strCtrlItemF

                Response.Redirect(strURL)
            Else
                lblErrorMessage.Visible = True
                lblErrorMessage.Text = "Record(s) not found."
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLocation.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCodeName", False)
            Else
                ddlLocSub.Items.Clear()
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class