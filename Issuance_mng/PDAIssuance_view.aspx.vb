#Region "Information Section"
' ****************************************************************************************************
' Description       : Issuance Asset
' Purpose           : Issuance Asset get from PDA
' Author            : See Siew
' Date              : 23/06/2008
' **************************************************************************************************** 
#End Region

#Region "Imports Section"
' Import the necessary classes.
Imports System.Data.SqlClient
Imports System.IO
#End Region

Partial Public Class PDAIssuance_view
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''********** Start: Check Session Time out ***********
            If Len(Session("UsrID")) = 0 Then
                Dim strJavaScript As String = ""
                strJavaScript = "<script language = 'Javascript'>" & _
                                "parent.location.href='../common/logout.aspx';" & _
                                "</script>"
                Response.Write(strJavaScript)
                Exit Sub
            End If
            ''********** End  : Check Session Time out ***********

            lblErrorMessage.Visible = False
            lblErrorMessage.Text = ""

            If Not Page.IsPostBack Then
                Me.butAccChange.Attributes.Add("OnClick", "return chkFrm('C')")
                Me.butDelRec.Attributes.Add("OnClick", "return chkFrm('D')")


                hdnIssNo.Value = Request("IssNo")
                If hdnIssNo.Value = "" Then hdnIssNo.Value = "0"
                hdnCallFrm.Value = Request("CallFrm")
                If hdnCallFrm.Value = "Upload" Then
                    ''Check Access Right
                    If (InStr(Session("AR"), "IISMng|PDAUploadFile|UpdateMv") = 0) Then
                        butAccChange.Enabled = False
                        butDelRec.Enabled = False
                    End If
                Else
                    ''Check Access Right
                    If (InStr(Session("AR"), "IISMng|PDAIss|UpdateMV") = 0) Then
                        butAccChange.Enabled = False
                        butDelRec.Enabled = False
                    End If

                End If


                ''Retrieve Records 
                fnGetRecordsFromDB()
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnGetRecordsFromDB()
        Try
            ''Get Issuance Record
            dgIssRec.DataSource = clsIssuance.fnIssuancePDA_GetRecords(hdnIssNo.Value)
            dgIssRec.DataBind()
            If Not dgIssRec.Items.Count > 0 Then
                lblErrorMessage.Text = "There is no record(s) found."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function fnShowLocDetail(ByVal strDept As String, ByVal strLoc As String, ByVal strLocSub As String, ByVal strOwner As String) As String
        Try
            Dim strRetVal As String = ""

            strRetVal = "<b>Department :</b> " & IIf(strDept = "", " - ", strDept) & "<br>" & _
                        "<b>Location :</b> " & IIf(strLoc = "", " - ", strLoc) & "<br>" & _
                        "<b>Sub Location :</b> " & IIf(strLocSub = "", " - ", strLocSub) & "<br>" & _
                        "<b>Assigned Owner :</b> " & IIf(strOwner = "", " - ", strOwner)

            Return (strRetVal)
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Function

    Private Sub dgIssRec_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgIssRec.ItemDataBound
        Try
            Dim ddlDept As DropDownList
            Dim ddlLoc As DropDownList
            Dim ddlLocSub As DropDownList
            Dim ddlOwner As DropDownList
            Dim textArea As TextBox
            Dim ChkBox As CheckBox

            ddlDept = CType(e.Item.FindControl("ddlDept"), DropDownList)
            If Not ddlDept Is Nothing Then
                fnPopulateDropDownList(clsDepartment.fnDeptGetAllRecDDL(Session("UsrID"), "Y"), ddlDept, "fld_DepartmentID", "fld_DepartmentCode", True)
                If e.Item.Cells(3).Text <> "0" Then
                    ddlDept.SelectedValue = e.Item.Cells(3).Text
                Else
                    ddlDept.SelectedValue = e.Item.Cells(2).Text
                End If
            End If

            ddlLoc = CType(e.Item.FindControl("ddlLocation"), DropDownList)
            If Not ddlLoc Is Nothing Then
                fnPopulateDropDownList(clsLocation.fnLocationGetAllRecForDDL(), ddlLoc, "fld_LocationID", "fld_LocationCode", False)
                ddlLoc.SelectedValue = e.Item.Cells(4).Text
            End If

            ddlLocSub = CType(e.Item.FindControl("ddlLocSub"), DropDownList)
            If Not ddlLocSub Is Nothing Then
                If ddlLoc.SelectedValue = "-1" Then
                    ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
                Else
                    fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLoc.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCode", False)
                    If e.Item.Cells(18).Text <> "0" Then
                        ddlLocSub.SelectedValue = e.Item.Cells(18).Text
                    End If
                End If
            End If

            ddlOwner = CType(e.Item.FindControl("ddlOwner"), DropDownList)
            If Not ddlOwner Is Nothing Then
                fnPopulateDropDownList(ClsUser.fnUsrGetUsrFromADDR("OU", "9999", "", ""), ddlOwner, "fld_ADLoginID", "fld_ADUsrName", False)
                ddlOwner.SelectedValue = e.Item.Cells(5).Text
            End If

            textArea = CType(e.Item.FindControl("txtRemark"), TextBox)
            If Not textArea Is Nothing Then
                If Trim(e.Item.Cells(7).Text) = "&nbsp;" Then
                    textArea.Text = ""
                Else
                    textArea.Text = Trim(e.Item.Cells(7).Text)
                End If
            End If


            ''Check Access Right
            If hdnCallFrm.Value = "Upload" Then
                ''Check Access Right
                If (InStr(Session("AR"), "IISMng|PDAUploadFile|UpdateMv") = 0) Then
                    If Not ddlDept Is Nothing Then ddlDept.Enabled = False
                    If Not ddlLoc Is Nothing Then ddlLoc.Enabled = False
                    If Not ddlLocSub Is Nothing Then ddlLocSub.Enabled = False
                    If Not ddlOwner Is Nothing Then ddlOwner.Enabled = False
                    If Not textArea Is Nothing Then textArea.Enabled = False

                    ChkBox = CType(e.Item.FindControl("DeleteThis"), CheckBox)
                    If Not ChkBox Is Nothing Then
                        ChkBox.Enabled = False
                    End If
                End If
            Else
                ''Check Access Right
                If (InStr(Session("AR"), "IISMng|PDAIss|UpdateMV") = 0) Then
                    If Not ddlDept Is Nothing Then ddlDept.Enabled = False
                    If Not ddlLoc Is Nothing Then ddlLoc.Enabled = False
                    If Not ddlLocSub Is Nothing Then ddlLocSub.Enabled = False
                    If Not ddlOwner Is Nothing Then ddlOwner.Enabled = False
                    If Not textArea Is Nothing Then textArea.Enabled = False

                    ChkBox = CType(e.Item.FindControl("DeleteThis"), CheckBox)
                    If Not ChkBox Is Nothing Then
                        ChkBox.Enabled = False
                    End If
                End If
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Sub butAccChange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butAccChange.Click
        Try
            ''validate records
            Dim strErrorF As String = "N"
            Dim strMsg As String = ""
            fnValSelectRecord(strErrorF, strMsg)
            If strErrorF = "Y" Then '' error found...
                lblErrorMessage.Text = strMsg
                lblErrorMessage.Visible = True
                Exit Sub
            End If

            ''Get Selected Records 
            Dim strAssetXML As String = fnGetSelectedAsset()

            ''Update Record in DB
            If strAssetXML <> "" Then
                Dim intRetVal As Integer
                intRetVal = clsIssuance.fnIssuancePDA_UpdateRec(strAssetXML, Session("UsrID"))
                If intRetVal > 0 Then
                    strMsg = "Issuance record(s) updated succesfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Issuance record(s) updated")

                    ''Get Records
                    fnGetRecordsFromDB()

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    strMsg = "Issuance record(s) update Failed. Please contact your Administrator."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If
            Else
                lblErrorMessage.Text = "At least one record should be selected for update."
                lblErrorMessage.Visible = True
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Public Sub fnValSelectRecord(ByRef strErrorF As String, ByRef strErrorMsg As String)
        Try
            strErrorF = "N"
            strErrorMsg = ""

            Dim GridItem As DataGridItem
            Dim chkSelectedRec As CheckBox
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim intRow As Integer = 0

            For Each GridItem In dgIssRec.Items
                intRow = intRow + 1

                chkSelectedRec = CType(GridItem.Cells(17).FindControl("DeleteThis"), CheckBox)
                If chkSelectedRec.Checked Then
                    ddlLocID = CType(GridItem.Cells(13).FindControl("ddlLocation"), DropDownList)
                    ddlLocSubID = CType(GridItem.Cells(14).FindControl("ddlLocSub"), DropDownList)
                    If ddlLocID.SelectedValue <> "-1" And ddlLocSubID.SelectedValue = "-1" Then
                        strErrorF = "Y"
                        strErrorMsg = "Please select Sub Location (Record " & intRow & ")."
                        Exit For
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub butDelRec_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelRec.Click
        Try
            Dim strAssetXML As String = fnGetSelectedAsset()

            ''Update Record in DB
            If strAssetXML <> "" Then
                Dim intRetVal As Integer
                Dim strMsg As String
                intRetVal = clsIssuance.fnIssuancePDA_DeleteRec(strAssetXML, Session("UsrID"))
                If intRetVal > 0 Then
                    strMsg = "Issuance record(s) deleted succesfully."

                    ''Insert Audit Trail
                    clsCommon.fnAuditInsertRec(Session("UsrID"), "0", "Issuance record(s) deleted")

                    ''Get Records
                    fnGetRecordsFromDB()

                    ''Message Notification
                    Dim strJavaScript As String
                    strJavaScript = "<script language = 'Javascript'>" & _
                                    "alert('" + strMsg + "');" & _
                                    "</script>"
                    Response.Write(strJavaScript)
                Else
                    strMsg = "Issuance record(s) delete Failed. Please contact your Administrator."
                    lblErrorMessage.Text = strMsg
                    lblErrorMessage.Visible = True
                End If
            Else
                lblErrorMessage.Text = "At least one record should be selected for delete."
                lblErrorMessage.Visible = True
            End If

        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub

    Private Function fnGetSelectedAsset() As String
        Try
            ''** Create Datatable --> Get Datatable
            Dim TblAsset As DataTable = fnCreateDataTableAsset()
            Dim RowAsset As DataRow

            ''** Get Data
            Dim GridItem As DataGridItem
            Dim chkSelected As CheckBox
            Dim ddlDeptID As DropDownList
            Dim ddlLocID As DropDownList
            Dim ddlLocSubID As DropDownList
            Dim ddlOwnerID As DropDownList
            Dim tbRemark As TextBox
            For Each GridItem In dgIssRec.Items
                chkSelected = CType(GridItem.Cells(17).FindControl("DeleteThis"), CheckBox)
                If chkSelected.Checked Then
                    ddlDeptID = CType(GridItem.Cells(12).FindControl("ddlDept"), DropDownList)
                    ddlLocID = CType(GridItem.Cells(13).FindControl("ddlLocation"), DropDownList)
                    ddlLocSubID = CType(GridItem.Cells(14).FindControl("ddlLocSub"), DropDownList)
                    ddlOwnerID = CType(GridItem.Cells(15).FindControl("ddlOwner"), DropDownList)
                    tbRemark = CType(GridItem.Cells(16).FindControl("txtRemark"), TextBox)

                    ''Add New Row to Datatable
                    RowAsset = TblAsset.NewRow()        'declaring a new row
                    RowAsset.Item("dt_IssDID") = GridItem.Cells(0).Text
                    RowAsset.Item("dt_DeptID") = IIf(ddlDeptID.SelectedValue = "-1", "0", ddlDeptID.SelectedValue)
                    RowAsset.Item("dt_LocID") = IIf(ddlLocID.SelectedValue = "-1", "0", ddlLocID.SelectedValue)
                    RowAsset.Item("dt_LocSubID") = IIf(ddlLocSubID.SelectedValue = "-1", "0", ddlLocSubID.SelectedValue)
                    RowAsset.Item("dt_OwnerID") = IIf(ddlOwnerID.SelectedValue = "-1", "", ddlOwnerID.SelectedValue)
                    RowAsset.Item("dt_ARemarks") = Trim(tbRemark.Text)
                    TblAsset.Rows.Add(RowAsset)
                End If
            Next

            ''** Get Asset in XML
            Dim ds As New DataSet
            Dim strAssetXML As String = ""
            ds = New DataSet         'creating a dataset
            ds.Tables.Add(TblAsset)   'assign datatable to dataset
            If Not ds Is Nothing Then
                Dim loCol As DataColumn
                'Prepare XML output from the DataSet as a string
                For Each loCol In ds.Tables(0).Columns
                    loCol.ColumnMapping = System.Data.MappingType.Attribute
                Next
                strAssetXML = ds.GetXml
            End If

            fnGetSelectedAsset = strAssetXML
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fnCreateDataTableAsset() As DataTable
        Try
            ''****1. creating a table named TblAsset
            Dim TblAsset As DataTable
            TblAsset = New DataTable("TblAsset")

            ''Column 1: Issuance Details id
            Dim dt_IssDID As DataColumn = New DataColumn("dt_IssDID")    'declaring a column named Name
            dt_IssDID.DataType = System.Type.GetType("System.String")    'setting the datatype for the column
            TblAsset.Columns.Add(dt_IssDID)                               'adding the column to table

            ' ''Column 2: Dept ID 
            Dim dt_DeptID As DataColumn = New DataColumn("dt_DeptID")      'declaring a column named Name
            dt_DeptID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_DeptID)                                    'adding the column to table

            ''Column 3: Loc ID 
            Dim dt_LocID As DataColumn = New DataColumn("dt_LocID")      'declaring a column named Name
            dt_LocID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_LocID)                                    'adding the column to table

            ''Column 3a: Sub Loc ID 
            Dim dt_LocSubID As DataColumn = New DataColumn("dt_LocSubID")      'declaring a column named Name
            dt_LocSubID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_LocSubID)                                    'adding the column to table

            ''Column 4: Owner ID 
            Dim dt_OwnerID As DataColumn = New DataColumn("dt_OwnerID")      'declaring a column named Name
            dt_OwnerID.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_OwnerID)

            ''Column 5: Asset Remarks
            Dim dt_ARemarks As DataColumn = New DataColumn("dt_ARemarks")      'declaring a column named Name
            dt_ARemarks.DataType = System.Type.GetType("System.String")         'setting the datatype for the column
            TblAsset.Columns.Add(dt_ARemarks)                                    'adding the column to table


            ''Set Primary Keys
            '--> Make the ID column the primary key column.
            Dim PrimaryKeyColumns(0) As DataColumn
            PrimaryKeyColumns(0) = TblAsset.Columns("dt_IssDID")
            TblAsset.PrimaryKey = PrimaryKeyColumns

            ''return dataTable 
            fnCreateDataTableAsset = TblAsset
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim ddllist As DropDownList = CType(sender, DropDownList)
            Dim cell As TableCell = CType(ddllist.Parent, TableCell)
            Dim item As DataGridItem = CType(cell.Parent, DataGridItem)
            Dim ddlLoc As DropDownList = CType(item.Cells(13).FindControl("ddlLocation"), DropDownList)
            Dim ddlLocSub As DropDownList = CType(item.Cells(14).FindControl("ddlLocSub"), DropDownList)

            'Dim strddlID As String = sender.clientid.ToString
            'Dim strddlNo As String = strddlID.Substring(Len(strddlID) - 3, 3)
            'Dim ddlLocation As DropDownList = CType(item.Cells(7).FindControl("ddlLocADD" & strddlNo), DropDownList)
            'Dim ddlLocSub As DropDownList = CType(item.Cells(7).FindControl("ddlLocSubADD" & strddlNo), DropDownList)

            If ddlLoc.SelectedValue <> "-1" Then
                fnPopulateDropDownList(clsLocation.fnLocation_GetLocation(ddlLoc.SelectedValue, "0", "S"), ddlLocSub, "fld_LocSubID", "fld_LocSubCode", False)
            Else
                ddlLocSub.Items.Clear()
                ddlLocSub.Items.Insert(0, New ListItem("", "-1"))
            End If
        Catch ex As Exception
            lblErrorMessage.Text = "Description : " & ex.Message & ControlChars.NewLine & "Source : " & Reflection.MethodBase.GetCurrentMethod.Name
            lblErrorMessage.Visible = True
        End Try
    End Sub
End Class