Public Partial Class UploadExcel_result
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''Get Today Date
            lblPrintDate.Text = mfnGetCurrentDateForTopPage()

            ''Bind record in datagrid
            dgAsset.DataSource = CType(Session("dsIssUploadExcel"), DataSet)
            dgAsset.DataBind()

            ''Set Session to blank
            'Session("dsIssUploadExcel") = ""

            ''Populate Report into Excel out
            Response.ContentType = "application/ms-excel"
            Response.AddHeader("Content-Disposition", "inline;filename=IssMngUploadExcelResult.xls")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class