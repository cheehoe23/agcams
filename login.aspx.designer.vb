'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class login

    '''<summary>
    '''frmLogin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents frmLogin As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''divMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divMessage As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ltMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ltMessage As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''txt_userID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_userID As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txt_password control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_password As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnLogin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnLogin As Global.System.Web.UI.WebControls.Button
End Class
